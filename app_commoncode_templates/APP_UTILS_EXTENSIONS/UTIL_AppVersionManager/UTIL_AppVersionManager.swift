//
//  UTIL_AppVersionManager.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 22/03/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

class AppVersionManager{
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - VERSION NUMBER
    //--------------------------------------------------------------
    //returns: 'Clarksons Platou - v 1.1'
    //note this is the BUILD number set in INFO tab of Target not VERSION field
    class func appVersionString()-> String{
        var appVersionString = "© City Of London Consulting Limited 2017"
        let bundleVersionString_ = AppVersionManager.bundleShortVersionString()
        
        appVersionString = "City Of London Consulting Limited © 2017 - v \(bundleVersionString_)"
        return appVersionString
    }
    
    //returns: '1'
    //note this is the BUILD number set in INFO tab of Target for VERSION field use bundleShortVersionString
    class func bundleVersionString()->String{
        var bundleVersionString = ""
        let stringkCFBundleVersionKey = kCFBundleVersionKey as String
        if let bundleVersion = Bundle.main.object(forInfoDictionaryKey: stringkCFBundleVersionKey) as? String{
            bundleVersionString = bundleVersion
        }
        
        return bundleVersionString
    }
    //v 1.1
    class func bundleShortVersionString()->String{
        var bundleShortVersionString = ""
        //let stringkCFBundleVersionKey = kCFBundleVersionKey as String
        if let bundleVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String{
            bundleShortVersionString = bundleVersion
        }
        
        return bundleShortVersionString
    }
}
