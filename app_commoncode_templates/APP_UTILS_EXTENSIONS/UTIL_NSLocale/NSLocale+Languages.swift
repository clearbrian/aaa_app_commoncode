//
//  NSLocale+Languages.swift
//  joyride
//
//  Created by Brian Clear on 17/05/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
extension Locale{

    //--------------------------------------------------------------
    // MARK: - DEVICE LOCALE en_GB  - not the same as device language en-GB
    // MARK: -
    //--------------------------------------------------------------

    //    print("NSLocale.currentDevice_LocaleIdentifier():\(NSLocale.currentDevice_LocaleIdentifier())")
    //    print("NSLocale.currentDevice_CountryCode():\(NSLocale.currentDevice_CountryCode())")
    //    print("NSLocale.currentDevice_LanguageCode():\(NSLocale.currentDevice_LanguageCode())")
    //    NSLocale.currentDevice_LocaleIdentifier():en_GB
    //    NSLocale.currentDevice_CountryCode():Optional("GB")
    //    NSLocale.currentDevice_LanguageCode():Optional("en")
    
    //what locale the device is currently in - the iPhone Language en + Region US = eng_US
    //note: even if you only pick French it will be set to fr_US not fr
    static func currentDevice_LocaleIdentifier() -> String{
        let locale: Locale = Locale.current
        //mem address : print("NSLocale.currentLocale():\(locale)")
        
        let localeIdentifier = locale.identifier
        //print("NSLocale.currentLocale().localeIdentifier:\(localeIdentifier)")
        return localeIdentifier
        
    }
    //en_GB - return GB
    static func currentDevice_CountryCode() -> String?{
        var currentCountryCodeForDevice_: String?
        
        currentCountryCodeForDevice_ = Locale.current.countryCodeForLocale()
        //print("NSLocale.currentLocale().localeIdentifier:\(localeIdentifier)")
        return currentCountryCodeForDevice_
        
    }
    
    //en_GB - return en
    static func currentDevice_LocaleLanguageCode() -> String?{
        var currentlanguageCodeForForDevice_: String?
        
        currentlanguageCodeForForDevice_ = Locale.current.languageCodeForLocale()
        //print("NSLocale.currentLocale().localeIdentifier:\(localeIdentifier)")
        
        return currentlanguageCodeForForDevice_
        
    }
    
    //--------------------------------------------------------------
    // MARK: - DEVICE LANGUAGE en-GB  - not the same as device local en_GB
    // MARK: -
    //--------------------------------------------------------------
    //NOTE: DEVICE LOCALE(en_GB) and LANGUAGE(en-GB) are not the same thing
    //print("NSLocale.currentDevice_LocaleIdentifier():\(NSLocale.currentDevice_LocaleIdentifier())")
    //NOTE LOCALE uses underscore
    //NSLocale.currentDevice_LocaleIdentifier():en_GB
    //print("NSLocale.currentDevice_LanguageCode():\(NSLocale.currentDevice_LanguageCode())")
    
    ////NSLocale.currentDevice_CanonicalLanguageIdentifier():en-GB
    static func currentDevice_LanguageCode() -> String{
        //let locale: NSLocale = NSLocale.currentLocale()
        //mem address : print("NSLocale.currentLocale():\(locale)")
        
        let canonicalLanguageIdentifier = Locale.canonicalLanguageIdentifier(from: Locale.current.identifier)
        //print("NSLocale.currentLocale().localeIdentifier:\(localeIdentifier)")
        return canonicalLanguageIdentifier
        
    }
    

    
    //--------------------------------------------------------------
    // MARK: - COMMENT
    // MARK: -
    //--------------------------------------------------------------

    func countryCodeForLocale() -> String?{
        let localeIdentifier = self.identifier
        
        let componentsFromLocaleIdentifierDict = Locale.components(fromIdentifier: localeIdentifier)
        //componentsFromLocaleIdentifier('en_GB'):["kCFLocaleCountryCodeKey": "GB", "kCFLocaleLanguageCodeKey": "en"]
        return componentsFromLocaleIdentifierDict["kCFLocaleCountryCodeKey"]
        
    }
    
    //"en_GB" returns "en"
    func languageCodeForLocale() -> String?{
        
        let componentsFromLocaleIdentifierDict = Locale.components(fromIdentifier: identifier)
        //componentsFromLocaleIdentifier('en_GB'):["kCFLocaleCountryCodeKey": "GB", "kCFLocaleLanguageCodeKey": "en"]
        return componentsFromLocaleIdentifierDict["kCFLocaleLanguageCodeKey"]
        
    }
    
    static func countryCodeForLocale(localeIdentifier: String) -> String?{
        
        let componentsFromLocaleIdentifierDict = Locale.components(fromIdentifier: localeIdentifier)
        //componentsFromLocaleIdentifier('en_GB'):["kCFLocaleCountryCodeKey": "GB", "kCFLocaleLanguageCodeKey": "en"]
        return componentsFromLocaleIdentifierDict["kCFLocaleCountryCodeKey"]
        
    }
    
    //"en_GB" returns "en"
    static func languageCodeForLocale(localeIdentifier: String) -> String?{
        
        let componentsFromLocaleIdentifierDict = Locale.components(fromIdentifier: localeIdentifier)
        print("componentsFromLocaleIdentifier('\(localeIdentifier)'):\(componentsFromLocaleIdentifierDict)")
        //canonicalLocaleIdentifierFromString('en_GB'):en_GB
        
        //componentsFromLocaleIdentifier('en_GB'):["kCFLocaleCountryCodeKey": "GB", "kCFLocaleLanguageCodeKey": "en"]
        //localeIdentifierFromComponents will take the DICT and generate en_GB
        return componentsFromLocaleIdentifierDict["kCFLocaleLanguageCodeKey"]
        
    }
    
    
    
    
    
    //--------------------------------------------------------------
    // MARK: - Device Locales
    // MARK: -
    //--------------------------------------------------------------

    
    //if users phone is in Enlgish and has korean keyboard installed
    //["en-GB", "ko-KR"]
    func listOfLanguagesSetUpOnDevice() -> [String] {
        
        let languageArray = Locale.preferredLanguages
        print("languageArray:\r\(languageArray)")
        //["en-GB", "ko-KR"]
        //I had korean keyboard installed
        return languageArray
    }
    
    //--------------------------------------------------------------
    // MARK: - all Locales supported on the device by iOS
    // MARK: -
    //--------------------------------------------------------------
    //Locales and Languages are NOT the same en_GB is Locale which has a language en-GB
    static func listOfAvailableLocales() -> [String]{
        let availableLocaleIdentifiersArray = Locale.availableIdentifiers
        //print("availableLocaleIdentifiersArray:\r\(availableLocaleIdentifiersArray)")
        return availableLocaleIdentifiersArray
    }
    
    static func listOfAvailableLocalesSorted() -> [String]{
        let availableLocaleIdentifiersArray = Locale.availableIdentifiers
        //print("availableLocaleIdentifiersArray:\r\(availableLocaleIdentifiersArray)")
        
        let availableLocaleIdentifiersArraySorted = availableLocaleIdentifiersArray.sorted()
        return availableLocaleIdentifiersArraySorted
    }
    
    static func dump_listOfAvailableLocalesSorted(){
        let availableLocaleIdentifiersArray = Locale.availableIdentifiers
        //print("availableLocaleIdentifiersArray:\r\(availableLocaleIdentifiersArray)")
        
        let availableLocaleIdentifiersArraySorted = availableLocaleIdentifiersArray.sorted()
        print("dump_listOfAvailableLocalesSorted:")
        for identifier in availableLocaleIdentifiersArraySorted{
            print(identifier)
        }
    }
    static func dump_listOfAvailableLocalesSortedWithDisplayNamesInEnglish(){
        let availableLocaleIdentifiersArray = Locale.availableIdentifiers
        //print("availableLocaleIdentifiersArray:\r\(availableLocaleIdentifiersArray)")
        
        let availableLocaleIdentifiersArraySorted = availableLocaleIdentifiersArray.sorted()
        print("dump_listOfAvailableLocalesSorted:")
        for identifier in availableLocaleIdentifiersArraySorted{
            if let displayNameForLocale = Locale.displayNameForLocale(displayThisLocale: identifier, inThisLanguage: "en") {
                print("\(identifier) : \(displayNameForLocale)")
            }else{
                appDelegate.log.error("[ERROR displayNameForLocale")
            }
            
        }
    }
    
    
    //--------------------------------------------------------------
    // MARK: - displayNameForLocale
    // MARK: -
    //--------------------------------------------------------------
    
    //Used to show list of languages but in the language of the device
    //get the name of locale in specifc language
    //"fr_FR" >> ""fr" ==> français (France)
    //"fr_FR" >> ""en" ==> french (France)
    
    static func displayNameForLocale(displayThisLocale: String, inThisLanguage: String) -> String?{
        var displayNameForLocale: String?
        
        //-----------------------------------------------------------------------------------
        //let frLocale = NSLocale(localeIdentifier: "fr_FR")
        //print(frLocale.displayNameForKey(NSLocaleIdentifier, value: "fr_FR")!)
        //français (France)
        //-----------------------------------------------------------------------------------

        
        //get the NSLocale for the localIDString e.g. "fr_FR" français (France)
        let inThisLanguage_locale = Locale(identifier: inThisLanguage)
        
       
        //french (France)
        if let displayNameForKey = (inThisLanguage_locale as NSLocale).displayName(forKey: NSLocale.Key.identifier, value: displayThisLocale) {
            //print("LOCALE[\(displayThisLocale)] in [\(inThisLanguage)] >> '\(displayNameForKey)'")
            displayNameForLocale = displayNameForKey
        }else{
            appDelegate.log.error("FAILED locale.displayNameForKey('\(inThisLanguage)', value: '\(inThisLanguage_locale)')")
        }
        return displayNameForLocale
    }
    
    static func dumpDisplayNameForLocale(displayThisLocale: String, inThisLanguage: String){
        //var displayThisLocale = "fr_FR"
        //var inThisLanguage = "fr_FR"
        
        if let displayNameForLocale = Locale.displayNameForLocale(displayThisLocale: displayThisLocale,
                                                                    inThisLanguage:inThisLanguage)
        {
            print("LOCALE[\(displayThisLocale)] in [\(inThisLanguage)] >> '\(displayNameForLocale)'")
            
        }else{
            
        }
    }
    
    //given array of local id - find the display name 
    //given 3 locales whats their display names in en
    //LOCALE[af] in [en] >> 'Afrikaans'
    //LOCALE[af_NA] in [en] >> 'Afrikaans (Namibia)'
    //LOCALE[af_ZA] in [en] >> 'Afrikaans (South Africa)'
    static func displayNames(_ arrayOfLocaleIds : [String], inThisLanguage: String) -> [String: String]{
        var displayNamesDict = [String: String]()
        
        for localeId in arrayOfLocaleIds{
            let displayNameInLang = displayNameForLocale(displayThisLocale: localeId, inThisLanguage: inThisLanguage)
            
            displayNamesDict[localeId] = displayNameInLang
            
        }
        return displayNamesDict
    }
    
    
    
    
    //--------------------------------------------------------------
    // MARK: - Google Place API
    // MARK: -
    //--------------------------------------------------------------
    //http://googlemaps.github.io/js-samples/map_language/map_lang.html
    static func supportedLanguagesGooglePlacesAPI() -> [String]{
        return [
        "ar",	//ARABIC
        "eu",	//BASQUE
        "bg",	//BULGARIAN
        "bn",	//BENGALI
        "ca",	//CATALAN
        "cs",	//CZECH
        "da",	//DANISH
        "de",	//GERMAN
        "el",	//GREEK
        "en",	//ENGLISH
        "en-AU",	//ENGLISH (AUSTRALIAN)      //note Locale is on format en_AU uses underline
        "en-GB",	//ENGLISH (GREAT BRITAIN)
        "es",	//SPANISH
        "fa",	//FARSI
        "fi",	//FINNISH
        "fil",	//FILIPINO
        "fr",	//FRENCH
        "gl",	//GALICIAN
        "gu",	//GUJARATI
        "hi",	//HINDI
        "hr",	//CROATIAN
        "hu",	//HUNGARIAN
        "id",	//INDONESIAN
        "it",	//ITALIAN
        "iw",	//HEBREW
        "ja",	//JAPANESE
        "kn",	//KANNADA
        "ko",	//KOREAN
        "lt",	//LITHUANIAN
        "lv",	//LATVIAN
        "ml",	//MALAYALAM
        "mr",	//MARATHI
        "nl",	//DUTCH
        "no",	//NORWEGIAN
        "pl",	//POLISH
        "pt",	//PORTUGUESE
        "pt-BR",	//PORTUGUESE (BRAZIL)
        "pt-PT",	//PORTUGUESE (PORTUGAL)
        "ro",	//ROMANIAN
        "ru",	//RUSSIAN
        "sk",	//SLOVAK
        "sl",	//SLOVENIAN
        "sr",	//SERBIAN
        "sv",	//SWEDISH
        "tl",	//TAGALOG
        "ta",	//TAMIL
        "te",	//TELUGU
        "th",	//THAI
        "tr",	//TURKISH
        "uk",	//UKRAINIAN
        "vi",	//VIETNAMESE
        "zh-CN",	//CHINESE (SIMPLIFIED)
        "zh-TW",	//CHINESE (TRADITIONAL)
        ]

    }
    
    static func displayNamesForGooglePlaces(_ inThisLanguage: String) -> [String: String]{
        var displayNamesDict = [String: String]()
        
        for localeId in self.supportedLanguagesGooglePlacesAPI(){
            let displayNameInLang = displayNameForLocale(displayThisLocale: localeId, inThisLanguage: inThisLanguage)
            
            displayNamesDict[localeId] = displayNameInLang
            
        }
        return displayNamesDict
    }
    
    
    //return the supported language or nil
    //if device local is en_GB
    //check for en_GB first
    //if no luck
    //then try en
    static func deviceLanguageSupportedByGooglePlacesApi() -> String?{
        var supportedLangCode: String?
        
        //print("NSLocale.currentDevice_LocaleIdentifier():\(NSLocale.currentDevice_LocaleIdentifier())")
        //print("NSLocale.currentDevice_CountryCode():\(NSLocale.currentDevice_CountryCode())")
        //print("NSLocale.currentDevice_LanguageCode():\(NSLocale.currentDevice_LanguageCode())")
        //NSLocale.currentDevice_LocaleIdentifier():en_GB
        //NSLocale.currentDevice_CountryCode():Optional("GB")
        //NSLocale.currentDevice_LanguageCode():Optional("en")
        
        //-----------------------------------------------------------------------------------
        //en_GB
        let currentDevice_LocaleIdentifier = Locale.currentDevice_LocaleIdentifier()
        //en
        let currentDevice_LanguageCode = Locale.currentDevice_LanguageCode()
        //-----------------------------------------------------------------------------------
        //is en_GB supported first...
        for identifierSupported in Locale.supportedLanguagesGooglePlacesAPI(){
            if currentDevice_LocaleIdentifier == identifierSupported{
                supportedLangCode = identifierSupported
            }
        }
        //-----------------------------------------------------------------------------------
        if let _ = supportedLangCode {
            //result found - no need to search for less specialised version
        }else{
            //en_GB not supported try en
            for localIdentifierSupported in Locale.supportedLanguagesGooglePlacesAPI(){
                if currentDevice_LanguageCode == localIdentifierSupported{
                    supportedLangCode = localIdentifierSupported
                }
            }
        }
        //-----------------------------------------------------------------------------------
        //can be nil
        return supportedLangCode
    
    }
    
}

/*
dump_listOfAvailableLocalesSorted:
af
af_NA
af_ZA
agq
agq_CM
ak
ak_GH
am
am_ET
ar
ar_001
ar_AE
ar_BH
ar_DJ
ar_DZ
ar_EG
ar_EH
ar_ER
ar_IL
ar_IQ
ar_JO
ar_KM
ar_KW
ar_LB
ar_LY
ar_MA
ar_MR
ar_OM
ar_PS
ar_QA
ar_SA
ar_SD
ar_SO
ar_SS
ar_SY
ar_TD
ar_TN
ar_YE
as
as_IN
asa
asa_TZ
az
az_Cyrl
az_Cyrl_AZ
az_Latn
az_Latn_AZ
bas
bas_CM
be
be_BY
bem
bem_ZM
bez
bez_TZ
bg
bg_BG
bm
bm_Latn
bm_Latn_ML
bn
bn_BD
bn_IN
bo
bo_CN
bo_IN
br
br_FR
brx
brx_IN
bs
bs_Cyrl
bs_Cyrl_BA
bs_Latn
bs_Latn_BA
ca
ca_AD
ca_ES
ca_FR
ca_IT
cgg
cgg_UG
chr
chr_US
ckb
ckb_IQ
ckb_IR
cs
cs_CZ
cy
cy_GB
da
da_DK
da_GL
dav
dav_KE
de
de_AT
de_BE
de_CH
de_DE
de_LI
de_LU
dje
dje_NE
dsb
dsb_DE
dua
dua_CM
dyo
dyo_SN
dz
dz_BT
ebu
ebu_KE
ee
ee_GH
ee_TG
el
el_CY
el_GR
en
en_001
en_150
en_AD
en_AG
en_AI
en_AL
en_AS
en_AT
en_AU
en_BA
en_BB
en_BE
en_BM
en_BS
en_BW
en_BZ
en_CA
en_CC
en_CH
en_CK
en_CM
en_CX
en_CY
en_CZ
en_DE
en_DG
en_DK
en_DM
en_EE
en_ER
en_ES
en_FI
en_FJ
en_FK
en_FM
en_FR
en_GB
en_GD
en_GG
en_GH
en_GI
en_GM
en_GR
en_GU
en_GY
en_HK
en_HR
en_HU
en_IE
en_IL
en_IM
en_IN
en_IO
en_IS
en_IT
en_JE
en_JM
en_KE
en_KI
en_KN
en_KY
en_LC
en_LR
en_LS
en_LT
en_LU
en_LV
en_ME
en_MG
en_MH
en_MO
en_MP
en_MS
en_MT
en_MU
en_MW
en_MY
en_NA
en_NF
en_NG
en_NL
en_NO
en_NR
en_NU
en_NZ
en_PG
en_PH
en_PK
en_PL
en_PN
en_PR
en_PT
en_PW
en_RO
en_RU
en_RW
en_SB
en_SC
en_SD
en_SE
en_SG
en_SH
en_SI
en_SK
en_SL
en_SS
en_SX
en_SZ
en_TC
en_TK
en_TO
en_TR
en_TT
en_TV
en_TZ
en_UG
en_UM
en_US
en_US_POSIX
en_VC
en_VG
en_VI
en_VU
en_WS
en_ZA
en_ZM
en_ZW
eo
es
es_419
es_AR
es_BO
es_CL
es_CO
es_CR
es_CU
es_DO
es_EA
es_EC
es_ES
es_GQ
es_GT
es_HN
es_IC
es_MX
es_NI
es_PA
es_PE
es_PH
es_PR
es_PY
es_SV
es_US
es_UY
es_VE
et
et_EE
eu
eu_ES
ewo
ewo_CM
fa
fa_AF
fa_IR
ff
ff_CM
ff_GN
ff_MR
ff_SN
fi
fi_FI
fil
fil_PH
fo
fo_FO
fr
fr_BE
fr_BF
fr_BI
fr_BJ
fr_BL
fr_CA
fr_CD
fr_CF
fr_CG
fr_CH
fr_CI
fr_CM
fr_DJ
fr_DZ
fr_FR
fr_GA
fr_GF
fr_GN
fr_GP
fr_GQ
fr_HT
fr_KM
fr_LU
fr_MA
fr_MC
fr_MF
fr_MG
fr_ML
fr_MQ
fr_MR
fr_MU
fr_NC
fr_NE
fr_PF
fr_PM
fr_RE
fr_RW
fr_SC
fr_SN
fr_SY
fr_TD
fr_TG
fr_TN
fr_VU
fr_WF
fr_YT
fur
fur_IT
fy
fy_NL
ga
ga_IE
gd
gd_GB
gl
gl_ES
gsw
gsw_CH
gsw_FR
gsw_LI
gu
gu_IN
guz
guz_KE
gv
gv_IM
ha
ha_Latn
ha_Latn_GH
ha_Latn_NE
ha_Latn_NG
haw
haw_US
he
he_IL
hi
hi_IN
hr
hr_BA
hr_HR
hsb
hsb_DE
hu
hu_HU
hy
hy_AM
id
id_ID
ig
ig_NG
ii
ii_CN
is
is_IS
it
it_CH
it_IT
it_SM
iu
iu_Cans
iu_Cans_CA
ja
ja_JP
jgo
jgo_CM
jmc
jmc_TZ
ka
ka_GE
kab
kab_DZ
kam
kam_KE
kde
kde_TZ
kea
kea_CV
khq
khq_ML
ki
ki_KE
kk
kk_Cyrl
kk_Cyrl_KZ
kkj
kkj_CM
kl
kl_GL
kln
kln_KE
km
km_KH
kn
kn_IN
ko
ko_KP
ko_KR
kok
kok_IN
ks
ks_Arab
ks_Arab_IN
ksb
ksb_TZ
ksf
ksf_CM
ksh
ksh_DE
kw
kw_GB
ky
ky_Cyrl
ky_Cyrl_KG
lag
lag_TZ
lb
lb_LU
lg
lg_UG
lkt
lkt_US
ln
ln_AO
ln_CD
ln_CF
ln_CG
lo
lo_LA
lt
lt_LT
lu
lu_CD
luo
luo_KE
luy
luy_KE
lv
lv_LV
mas
mas_KE
mas_TZ
mer
mer_KE
mfe
mfe_MU
mg
mg_MG
mgh
mgh_MZ
mgo
mgo_CM
mk
mk_MK
ml
ml_IN
mn
mn_Cyrl
mn_Cyrl_MN
mr
mr_IN
ms
ms_Arab
ms_Arab_BN
ms_Arab_MY
ms_Latn
ms_Latn_BN
ms_Latn_MY
ms_Latn_SG
mt
mt_MT
mua
mua_CM
my
my_MM
naq
naq_NA
nb
nb_NO
nb_SJ
nd
nd_ZW
ne
ne_IN
ne_NP
nl
nl_AW
nl_BE
nl_BQ
nl_CW
nl_NL
nl_SR
nl_SX
nmg
nmg_CM
nn
nn_NO
nnh
nnh_CM
nus
nus_SD
nyn
nyn_UG
om
om_ET
om_KE
or
or_IN
os
os_GE
os_RU
pa
pa_Arab
pa_Arab_PK
pa_Guru
pa_Guru_IN
pl
pl_PL
ps
ps_AF
pt
pt_AO
pt_BR
pt_CV
pt_GW
pt_MO
pt_MZ
pt_PT
pt_ST
pt_TL
qu
qu_BO
qu_EC
qu_PE
rm
rm_CH
rn
rn_BI
ro
ro_MD
ro_RO
rof
rof_TZ
ru
ru_BY
ru_KG
ru_KZ
ru_MD
ru_RU
ru_UA
rw
rw_RW
rwk
rwk_TZ
sah
sah_RU
saq
saq_KE
sbp
sbp_TZ
se
se_FI
se_NO
se_SE
seh
seh_MZ
ses
ses_ML
sg
sg_CF
shi
shi_Latn
shi_Latn_MA
shi_Tfng
shi_Tfng_MA
si
si_LK
sk
sk_SK
sl
sl_SI
smn
smn_FI
sn
sn_ZW
so
so_DJ
so_ET
so_KE
so_SO
sq
sq_AL
sq_MK
sq_XK
sr
sr_Cyrl
sr_Cyrl_BA
sr_Cyrl_ME
sr_Cyrl_RS
sr_Cyrl_XK
sr_Latn
sr_Latn_BA
sr_Latn_ME
sr_Latn_RS
sr_Latn_XK
sv
sv_AX
sv_FI
sv_SE
sw
sw_CD
sw_KE
sw_TZ
sw_UG
ta
ta_IN
ta_LK
ta_MY
ta_SG
te
te_IN
teo
teo_KE
teo_UG
tg
tg_Cyrl
tg_Cyrl_TJ
th
th_TH
ti
ti_ER
ti_ET
tk
tk_Latn
tk_Latn_TM
to
to_TO
tr
tr_CY
tr_TR
twq
twq_NE
tzm
tzm_Latn
tzm_Latn_MA
ug
ug_Arab
ug_Arab_CN
uk
uk_UA
ur
ur_IN
ur_PK
uz
uz_Arab
uz_Arab_AF
uz_Cyrl
uz_Cyrl_UZ
uz_Latn
uz_Latn_UZ
vai
vai_Latn
vai_Latn_LR
vai_Vaii
vai_Vaii_LR
vi
vi_VN
vun
vun_TZ
wae
wae_CH
xog
xog_UG
yav
yav_CM
yi
yi_001
yo
yo_BJ
yo_NG
zgh
zgh_MA
zh
zh_Hans
zh_Hans_CN
zh_Hans_HK
zh_Hans_MO
zh_Hans_SG
zh_Hant
zh_Hant_HK
zh_Hant_MO
zh_Hant_TW
zu
zu_ZA
*/
