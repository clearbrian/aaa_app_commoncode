//
//  MapImageUtils.swift
//  joyride
//
//  Created by Brian Clear on 11/11/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit

class MapImageUtils{
    
    
    /*
     /Users/gbxc/Documents/00_ME/00_BITBUCKET_COLC/app_apptemplatesandnotes/00_CODE/AppWithCodeTemplates_iOS/AppWithCodeTemplates_Animation/AppWithCodeTemplates_Animation/PLAYGROUNDS/CoreGraphicsMapPin.playground
     
     */
    class func textToImage(_ textTitle: String, iconColor : UIColor) -> UIImage{
        return MapImageUtils.textToImage(textTitle, iconColor: iconColor, fillColor: UIColor.white)
    }
    
    class func textToImage(_ textTitle: String, iconColor : UIColor, fillColor : UIColor) -> UIImage{
        
        
        let textTitleNSString = textTitle as NSString
        //---------------------------------------------------------------------
        let textColor: UIColor = UIColor.appColorMapLabelStroke()
        let fontTitle: UIFont = UIFont.systemFont(ofSize: 12.0)
        //---------------------------------------------------------------------
        //find the largest frame needed to display this text string at this font
        //This is the size of the inner label
        var textCGSize:CGSize = textTitleNSString.size(attributes: [NSFontAttributeName: fontTitle])
        //add extra line to handle smaller letter g drop below baseline
        textCGSize.height = textCGSize.height + 1
        //---------------------------------------------------------------------
        //DEBUG - easier to check sizes if this is set here
        //textCGSize = CGSize(width: 200.0, height: 40.0)
        //ALL SIZES SHOULD GROW OUT from textCGSize so that if you change insideBorder nothing gets cut off
        //---------------------------------------------------------------------
        let strokeBorder: CGFloat = 1.0
        //let insideBorder : CGFloat = 2.0
        //let insideBorder : CGFloat = 2.0 //ok to change this to anything - label will grow OUT not in
        
        let outsideBorder : CGFloat = 2.0 //white around outside
        let insideBorder : CGFloat = 4.0 //between square and text
        //-----------------------------------------------------------------------------------
        
        
        let squareHeight: CGFloat = textCGSize.height //width and height the same as text height
        
        let squareWidth: CGFloat = squareHeight / 2
        
        
        //should grow OUT from textCGSize
        //let widthOfUIImageReturned: CGFloat  = strokeBorder + insideBorder + sideOfIconSquare  + insideBorder + textCGSize.width + insideBorder + strokeBorder
        let widthOfUIImageReturned: CGFloat  = strokeBorder + outsideBorder + squareWidth  + insideBorder + textCGSize.width + outsideBorder + strokeBorder
        let heightOfUIImageReturned: CGFloat = strokeBorder + outsideBorder + textCGSize.height + outsideBorder + strokeBorder
        
        
        let sizeUIImageReturned:CGSize = CGSize(width: widthOfUIImageReturned,
                                                height: heightOfUIImageReturned)
        
        //---------------------------------------------------------------------
        //Trangle pointer below main icon
        //---------------------------------------------------------------------
        let trianglePathWidth : CGFloat = 10.0
        let trianglePathHeight : CGFloat = trianglePathWidth
        
        
        let sizeUIImageReturnedPlusTriangle:CGSize = CGSize(width: widthOfUIImageReturned,
                                                            height: heightOfUIImageReturned + trianglePathHeight)
        
        
        //---------------------------------------------------------------------
        //set size of returned UIImage
        //UIGraphicsBeginImageContextWithOptions(sizeUIImageReturned + trianglePathHeight, false, 0)
        UIGraphicsBeginImageContextWithOptions(sizeUIImageReturnedPlusTriangle, false, 0)
        
        var newImage: UIImage = UIImage() //will be discarded
        
        if let context = UIGraphicsGetCurrentContext(){
            //---------------------------------------------------------------------
            //-----------------------------------------------------------------------------------
            //WHOLE LABEL iS WHITE RECTANGLE
            context.setFillColor(fillColor.cgColor)
            context.fill(CGRect(origin: CGPoint(x: 0, y: 0), size: sizeUIImageReturned))
            
            //---------------------------------------------------------------------
            //SINGLE LINE BORDER
            //turned off we just as gaps around iconRect and rect we draw string into
            //---------------------------------------------------------------------
            //BORDER - starts at 0,0
            //Border is a rect of width
            // cos stroke width is 1.0
            
            //    shapeLayer.strokeColor = AppearanceManager.appColorNavbarAndTabBar.CGColor
            //    shapeLayer.fillColor = AppearanceManager.appColorTint.CGColor
            context.setStrokeColor(AppearanceManager.appColorNavbarAndTabBar.cgColor)
            
            let colorStrokeRect = AppearanceManager.appColorNavbarAndTabBar
            colorStrokeRect.setStroke()
            //trianglePath.stroke() //DRAW border
            
            context.stroke(CGRect(origin: CGPoint(x: 0.0, y: 0.0), size: sizeUIImageReturned))
            
            
            
            //-----------------------------------------------------------------------------------
            //SQUARE
            //rect stroke is 1.0 - so 1 px white border between that and icon
            let rectIconColor = CGRect(x: strokeBorder + outsideBorder,
                                       y: strokeBorder + outsideBorder,
                                       width: squareWidth,
                                       height: squareHeight)
            
            
            context.setFillColor(iconColor.cgColor)
            //CGContextSetFillColorWithColor(context, UIColor.purpleColor().CGColor)
            context.fill(rectIconColor)
            
            //------------------------------------------------------------------------------------------------
            //SET TEXT
            //------------------------------------------------------------------------------------------------
            let style = NSMutableParagraphStyle()
            //style.alignment = .left
            style.alignment = .center
            
            let attrTitle = [NSFontAttributeName:fontTitle, NSForegroundColorAttributeName:textColor, NSParagraphStyleAttributeName:style]
            
            let rectText = CGRect(x: strokeBorder + outsideBorder + squareWidth + insideBorder,
                                  y: strokeBorder + outsideBorder,
                                  width: textCGSize.width,
                                  height: textCGSize.height)
            
            //---------------------------------------------------------------------
            //Debug - show TEXT RECT - all borders should grow OUT from this
            //CGContextSetFillColorWithColor(context, UIColor.appColorCYAN().CGColor)
            //CGContextFillRect(context, rectText)
            //---------------------------------------------------------------------
            //DRAW TEXT over TEXT BACKGROUND RECT with padding LHS and RHS so its no flush at 0,0
            textTitleNSString.draw(in: rectText, withAttributes: attrTitle)
            
            
            //-----------------------------------------------------------------------------------
            //OK
            //        let aPath = UIBezierPath(ovalInRect:CGRectMake((sizeUIImageReturned.width/2.0) - (sideOfIconSquare/2.0),
            //                                                        0,
            //                                                        sideOfIconSquare, sideOfIconSquare))
            //
            //        let color = UIColor.cyanColor()
            //        color.setFill()
            //        aPath.fill() //DRAW it
            //-----------------------------------------------------------------------------------
            
            
            
            let trianglePath_topY : CGFloat = sizeUIImageReturned.height
            
            let trianglePath = UIBezierPath()
            trianglePath.move(to: CGPoint(x: (sizeUIImageReturned.width / 2.0) - trianglePathWidth / 2.0,
                                          y: trianglePath_topY))
            
            
            trianglePath.addLine(to: CGPoint(x: (sizeUIImageReturned.width / 2.0) + trianglePathWidth / 2.0,
                                             y: trianglePath_topY))
            
            trianglePath.addLine(to: CGPoint(x: (sizeUIImageReturned.width / 2.0),
                                             y: trianglePath_topY + trianglePathHeight))
            trianglePath.close()
            //-----------------------------------------------------------------------------------
            
            //let colorFill = UIColor.white
            fillColor.setFill()
            
            trianglePath.fill() //DRAW it
            
            let colorStroke = AppearanceManager.appColorNavbarAndTabBar
            colorStroke.setStroke()
            trianglePath.stroke() //DRAW border
            
            
            
            //------------------------------------------------
            //ALL DRAWING COMPLETE - RENDER IMAGE
            //------------------------------------------------
            //GENERATE THE UIImage to use as map icon
            //Must be same size for all icons on map due to bug in MapBox
            if let newImage_ = UIGraphicsGetImageFromCurrentImageContext(){
                //print("newImage['\(newImage.size)]")
                newImage = newImage_
            }else{
                MyXCodeEmojiLogger.defaultInstance.error("UIGraphicsGetImageFromCurrentImageContext() is nil")
            }
            
            
            
            
            // End the context now that we have the image we need
            UIGraphicsEndImageContext()
            //---------------------------------------------------------------------
        }else{
            MyXCodeEmojiLogger.defaultInstance.error("context is nil")
        }
        
        
        
        return newImage
    }
}
