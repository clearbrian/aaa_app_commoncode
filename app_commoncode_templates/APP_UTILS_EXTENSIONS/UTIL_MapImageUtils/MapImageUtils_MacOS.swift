//
//  MapImageUtils.swift
//  joyride
//
//  Created by Brian Clear on 11/11/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import AppKit

class MapImageUtils{
    
    
    /*
     /Users/gbxc/Documents/00_ME/00_BITBUCKET_COLC/app_apptemplatesandnotes/00_CODE/AppWithCodeTemplates_iOS/AppWithCodeTemplates_Animation/AppWithCodeTemplates_Animation/PLAYGROUNDS/CoreGraphicsMapPin.playground
     
     let imageTaxi = MapImageUtils.textToImage("TAXI RNAK", iconColor: .appColorFlat_Orange())
     
     let imageStation = MapImageUtils.textToImage("STATION", iconColor: .appColorFlat_Orange())
     */
    
    class func textToImage(_ textTitle: String, iconColor : NSColor) -> NSImage{
        return MapImageUtils.textToImage(textTitle, iconColor: iconColor, fillColor: NSColor.white)
        
    }
    
    class func textToImage(_ textTitle: String, iconColor : NSColor, fillColor : NSColor) -> NSImage{
        
        
//        let textTitleNSString = textTitle as NSString
//        //---------------------------------------------------------------------
//        let textColor: NSColor = NSColor.appColorMapLabelStroke()
//        let fontTitle: NSFont = NSFont.systemFont(ofSize: 12.0)
//        //---------------------------------------------------------------------
//        //find the largest frame needed to display this text string at this font
//        //This is the size of the inner label
//        var textCGSize:CGSize = textTitleNSString.size(withAttributes: [NSFontAttributeName: fontTitle])
//        //add extra line to handle smaller letter g drop below baseline
//        textCGSize.height = textCGSize.height + 1
//        //---------------------------------------------------------------------
//        //DEBUG - easier to check sizes if this is set here
//        //textCGSize = CGSize(width: 200.0, height: 40.0)
//        //ALL SIZES SHOULD GROW OUT from textCGSize so that if you change insideBorder nothing gets cut off
//        //---------------------------------------------------------------------
//        let strokeBorder: CGFloat = 1.0
//        //let insideBorder : CGFloat = 2.0
//        //let insideBorder : CGFloat = 2.0 //ok to change this to anything - label will grow OUT not in
//        
//        let outsideBorder : CGFloat = 2.0 //white around outside
//        let insideBorder : CGFloat = 4.0 //between square and text
//        //-----------------------------------------------------------------------------------
//        
//        
//        let squareHeight: CGFloat = textCGSize.height //width and height the same as text height
//        
//        //let squareWidth: CGFloat = squareHeight / 3
//        let squareWidth: CGFloat = squareHeight / 2
//        
//        
//        //should grow OUT from textCGSize
//        //let widthOfNSImageReturned: CGFloat  = strokeBorder + insideBorder + sideOfIconSquare  + insideBorder + textCGSize.width + insideBorder + strokeBorder
//        let widthOfNSImageReturned: CGFloat  = strokeBorder + outsideBorder + squareWidth  + insideBorder + textCGSize.width + outsideBorder + strokeBorder
//        let heightOfNSImageReturned: CGFloat = strokeBorder + outsideBorder + textCGSize.height + outsideBorder + strokeBorder
//        
//        
//        let sizeNSImageReturned:CGSize = CGSize(width: widthOfNSImageReturned,
//                                                height: heightOfNSImageReturned)
//        
//        //---------------------------------------------------------------------
//        //Trangle pointer below main icon
//        //---------------------------------------------------------------------
//        let trianglePathWidth : CGFloat = 10.0
//        let trianglePathHeight : CGFloat = trianglePathWidth
//        
//        
//        let sizeNSImageReturnedPlusTriangle:CGSize = CGSize(width: widthOfNSImageReturned,
//                                                            height: heightOfNSImageReturned + trianglePathHeight)
//        
//        
//        //---------------------------------------------------------------------
//        //set size of returned NSImage
//        //UIGraphicsBeginImageContextWithOptions(sizeNSImageReturned + trianglePathHeight, false, 0)
//        UIGraphicsBeginImageContextWithOptions(sizeNSImageReturnedPlusTriangle, false, 0)
        
        var newImage: NSImage = NSImage() //will be discarded
        
//        if let context = UIGraphicsGetCurrentContext(){
//            //---------------------------------------------------------------------
//            //-----------------------------------------------------------------------------------
//            //WHOLE LABEL iS WHITE RECTANGLE
//            context.setFillColor(NSColor.white.cgColor)
//            context.fill(CGRect(origin: CGPoint(x: 0, y: 0), size: sizeNSImageReturned))
//            
//            //---------------------------------------------------------------------
//            //SINGLE LINE BORDER
//            //turned off we just as gaps around iconRect and rect we draw string into
//            //---------------------------------------------------------------------
//            //BORDER - starts at 0,0
//            //Border is a rect of width
//            // cos stroke width is 1.0
//            
//            //    shapeLayer.strokeColor = AppearanceManager.appColorNavbarAndTabBar.CGColor
//            //    shapeLayer.fillColor = AppearanceManager.appColorTint.CGColor
//            context.setStrokeColor(AppearanceManager.appColorNavbarAndTabBar.cgColor)
//            context.stroke(CGRect(origin: CGPoint(x: 0.0, y: 0.0), size: sizeNSImageReturned))
//            
//            
//            
//            //-----------------------------------------------------------------------------------
//            //SQUARE
//            //rect stroke is 1.0 - so 1 px white border between that and icon
//            let rectIconColor = CGRect(x: strokeBorder + outsideBorder,
//                                       y: strokeBorder + outsideBorder,
//                                       width: squareWidth,
//                                       height: squareHeight)
//            
//            
//            context.setFillColor(iconColor.cgColor)
//            //CGContextSetFillColorWithColor(context, NSColor.purpleColor().CGColor)
//            context.fill(rectIconColor)
//            //------------------------------------------------------------------------------------------------
//            //SET TEXT
//            //------------------------------------------------------------------------------------------------
//            let style = NSMutableParagraphStyle()
//            //style.alignment = .left
//            style.alignment = .center
//            
//            let attrTitle = [NSFontAttributeName:fontTitle, NSForegroundColorAttributeName:textColor, NSParagraphStyleAttributeName:style]
//            
//            let rectText = CGRect(x: strokeBorder + outsideBorder + squareWidth + insideBorder,
//                                  y: strokeBorder + outsideBorder,
//                                  width: textCGSize.width,
//                                  height: textCGSize.height)
//            
//            //---------------------------------------------------------------------
//            //Debug - show TEXT RECT - all borders should grow OUT from this
//            //CGContextSetFillColorWithColor(context, NSColor.appColorCYAN().CGColor)
//            //CGContextFillRect(context, rectText)
//            //---------------------------------------------------------------------
//            //DRAW TEXT over TEXT BACKGROUND RECT with padding LHS and RHS so its no flush at 0,0
//            textTitleNSString.draw(in: rectText, withAttributes: attrTitle)
//            
//            
//            //-----------------------------------------------------------------------------------
//            //OK
//            //        let aPath = UIBezierPath(ovalInRect:CGRectMake((sizeNSImageReturned.width/2.0) - (sideOfIconSquare/2.0),
//            //                                                        0,
//            //                                                        sideOfIconSquare, sideOfIconSquare))
//            //
//            //        let color = NSColor.cyanColor()
//            //        color.setFill()
//            //        aPath.fill() //DRAW it
//            //-----------------------------------------------------------------------------------
//            
////            if includePin{
//                let trianglePath_topY : CGFloat = sizeNSImageReturned.height
//                
//                let trianglePath = NSBezierPath()
//                trianglePath.move(to: CGPoint(x: (sizeNSImageReturned.width / 2.0) - trianglePathWidth / 2.0,
//                                              y: trianglePath_topY))
//            
//                
//                trianglePath.line(to: CGPoint(x: (sizeNSImageReturned.width / 2.0) + trianglePathWidth / 2.0,
//                                                 y: trianglePath_topY))
//                
//                trianglePath.line(to: CGPoint(x: (sizeNSImageReturned.width / 2.0),
//                                                 y: trianglePath_topY + trianglePathHeight))
//                trianglePath.close()
//                //-----------------------------------------------------------------------------------
//                
//                let colorFill = NSColor.white
//                colorFill.setFill()
//                
//                trianglePath.fill() //DRAW it
//                
//            //let colorStroke = AppearanceManager.appColorNavbarAndTabBar
//                let colorStroke = NSColor.purple
//                colorStroke.setStroke()
//                trianglePath.stroke() //DRAW border
//                
//
////            }else{
////                //no pin at bottom
////            }
//            
//        appDelegate.log.error("UIGraphicsGetImageFromCurrentImageContext NOT ON MAC")
//// TODO: - MAC
////            //------------------------------------------------
////            //ALL DRAWING COMPLETE - RENDER IMAGE
////            //------------------------------------------------
////            //GENERATE THE NSImage to use as map icon
////            //Must be same size for all icons on map due to bug in MapBox
////            if let newImage_ = UIGraphicsGetImageFromCurrentImageContext(){
////                //print("newImage['\(newImage.size)]")
////                newImage = newImage_
////            }else{
////                MyXCodeEmojiLogger.defaultInstance.error("UIGraphicsGetImageFromCurrentImageContext() is nil")
////            }
////            
////            
////            
////            
////            // End the context now that we have the image we need
////            UIGraphicsEndImageContext()
////            //---------------------------------------------------------------------
//        }else{
//            MyXCodeEmojiLogger.defaultInstance.error("context is nil")
//        }
        
        
        
        return newImage
    }
}
