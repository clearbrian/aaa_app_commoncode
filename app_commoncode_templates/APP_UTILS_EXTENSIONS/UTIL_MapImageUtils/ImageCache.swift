//
//  UIImage+PrefilledDictionary.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 29/03/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
import UIKit

class ImageCache{
    
    static var imageDictionary = [String: UIImage]()
    
    static func addToDictionaryIfExists(imageName: String) -> UIImage?{
        
        if let imageFound = UIImage(named:imageName) {
            ImageCache.imageDictionary[imageName] = imageFound
            return imageFound
            
        }else{
            appDelegate.log.error("UIImage(named:'\(imageName)') is nil - addToDictionaryIfExists failed")
            return nil
        }
    }
    
    //create once, store in dict - after reuse instance in dict
    //used for map pins
    static func lazyImageForName(imageName: String) -> UIImage?{
        
        if let imageInDictionary = ImageCache.imageDictionary[imageName] {
            return imageInDictionary
        }else{
            appDelegate.log.error("PrefilledUIImages.imageDictionary[\(imageName)] is nil - create once")
            return ImageCache.addToDictionaryIfExists(imageName: imageName)
        }
    }
    
}
