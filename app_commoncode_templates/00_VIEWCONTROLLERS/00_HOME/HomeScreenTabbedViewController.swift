//
//  HomeScreenTabbedViewController.swift
//  joyride
//
//  Created by Brian Clear on 21/01/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit

class HomeScreenTabbedViewController: ParentViewController, AnimatedLogo2MenuView3Delegate {
    
    var tabBarControllerEmbedded : UITabBarController?
    
    @IBOutlet var animatedLogo2MenuView3: AnimatedLogo2MenuView3!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        animatedLogo2MenuView3.delegate = self
//        animatedLogo2MenuView3.segmentedControl.applyCustomFontForCurrentTextStyle()
        
    }
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - SEGUE
    //--------------------------------------------------------------
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == "embedTabViewController"){
            //not needed appDelegate.searchViewController lets logout to be called from any VC via delegate
            if(segue.destination.isMember(of: UITabBarController.self)){
                
                self.tabBarControllerEmbedded = (segue.destination as! UITabBarController)
                self.tabBarControllerEmbedded?.hidesBottomBarWhenPushed = true
                self.tabBarControllerEmbedded?.tabBar.isHidden = true
                
            }else{
                self.log.error("embedTabViewController not UITabBarController")
            }
        }
        else{
            self.log.error("UNHANDLED SEGUE:\(segue.identifier)")
        }
    }
    
    func segmentedControl_ValueChanged(_ animatedLogo2MenuView3: AnimatedLogo2MenuView3, segmentedControl: UISegmentedControl){
         print("\(segmentedControl.selectedSegmentIndex)")
        
        if let tabBarControllerEmbedded = self.tabBarControllerEmbedded{
            tabBarControllerEmbedded.selectedIndex = segmentedControl.selectedSegmentIndex
            //tabBarControllerEmbedded.set = segmentedControl.selectedSegmentIndex
        }else{
            self.log.error("tabBarControllerEmbedded is nil")
        }
    }
    
    //
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
