//
//  COLCCarbonTabSwipeScrollView.swift
//  joyride
//
//  Created by Brian Clear on 13/01/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
class COLCCarbonTabSwipeScrollView : CarbonTabSwipeScrollView, CarbonTabSwipeSegmentedControlDelegate{
    
    //created from Storyboard
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        //else segmented control visible off the RHS
        self.clipsToBounds = true
        
        let items = [UIImage(named: "home")!, UIImage(named: "hourglass")!, UIImage(named: "premium_badge")!, "Categories", "Top Free", "Top New Free", "Top Paid", "Top New Paid"] as [Any]
        self.setItems(items)
        print("initWithCoder")
        
        //------------------------------------------------------------------------------------------------
        //tintColor
        //------------------------------------------------------------------------------------------------
        //worng its set to clear internally
        //self.tintColor = self.window?.tintColor
        
        //use this to set color of segment images and line underneath
        //self.carbonSegmentedControl.imageSelectedColor = self.window?.tintColor
        self.carbonSegmentedControl.imageSelectedColor = UIColor.white
        //------------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------------

        //OK but use delegate
        //self.carbonSegmentedControl.addTarget(self, action: "segmentedTapped:", forControlEvents: UIControlEvents.ValueChanged)

        self.carbonSegmentedControl.delegate = self;
    }
    
    func segmentedTapped(_ carbonTabSwipeSegmentedControl: CarbonTabSwipeSegmentedControl) {
        print("segmentedTapped[carbonTabSwipeSegmentedControl.selectedSegmentIndex]:\(carbonTabSwipeSegmentedControl.selectedSegmentIndex)")
    }
    
//    - (void)carbonTabSwipeSegmentedControl:(CarbonTabSwipeSegmentedControl *)CarbonTabSwipeSegmentedControl
//    segmentedTapped:(NSInteger) selectedSegmentIndex;
    func carbonTabSwipeSegmentedControl(_ carbonTabSwipeSegmentedControl : CarbonTabSwipeSegmentedControl, segmentedTapped:NSInteger) {
        
        print("carbonTabSwipeSegmentedControl[segmentedTapped]:\(carbonTabSwipeSegmentedControl.selectedSegmentIndex)")
    }
}
