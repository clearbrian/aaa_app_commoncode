//
//  AppDelegate.swift
//  joyride
//
//  Created by Brian Clear on 29/06/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
//import FBSDKCoreKit
import Flurry_iOS_SDK

//so we can reference it from anywhere
let appDelegate = UIApplication.shared.delegate as! AppDelegate


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CurrentLocationManagerDelegate {
    
    var window: UIWindow?
    
    
    //--------------------------------------------------------------------------------------------------------------------------
    //LOGGING - Requires XCodeColors plugin
    //--------------------------------------------------------------------------------------------------------------------------
    //MUST BE DECLARED IN EACH CLASS THAT USES IT
    //let log = MyXCodeColorsLogger.defaultInstance
    
    //create seperate instance for each VC so we can turn them on/off
    //as we develop them in configureLogging()
    //let log = MySwiftyBeaverLogger()
    //let log = MyXCodeEmojiLogger.defaultInstance
    
    let log = MyXCodeEmojiLogger.defaultInstance
    
    //---------------------------------------------------------------------
    func test_Logger(){
        // add the destinations to SwiftyBeaver
        
        
        // Now let’s log!
        //self.log.verbose("not so important")  // prio 1, VERBOSE in silver
        self.log.debug("something to debug")  // prio 2, DEBUG in green
        self.log.info("a nice information")   // prio 3, INFO in blue
        self.log.warning("oh no, that won’t be good")  // prio 4, WARNING in yellow
        self.log.error("ouch, an error did occur!")  // prio 5, ERROR in red
        
        // log anything!
        //self.log.verbose(234)
        self.log.info(-123.45678)
        self.log.warning(Date())
        self.log.error(["I", "like", "logs!"])
        self.log.error(["name": "Mr Beaver", "address": "7 Beaver Lodge"])
    }
    
    
    //---------------------------------------------------------------------
    //OAuth2
    //---------------------------------------------------------------------
    let colcOAuth2Manager = COLCOAuth2Manager()
    
    //------------------------------------------------------------------------------------------------
    //OpenWithController
    //------------------------------------------------------------------------------------------------
    let openWithController = OpenWithController()
    
    
    let tflGeoJSONManager = TFLGeoJSONManager()
    
    //----------------------------------------------------------------------------------------
    //MAP
    //----------------------------------------------------------------------------------------
    //var tflTaxisRankMapViewController: TFLTaxisRankMapViewController?
    
    var tflPlaceTaxiRankResponse : TFLPlaceTaxiRankResponse?
    
   
    
    
    
    //---------------------------------------------------------------------
    //LOCATION  - CurrentLocationManager
    //---------------------------------------------------------------------
    var currentLocationManager = CurrentLocationManager()
    //currentLocationManager.delegate = self done in application:didFinish
    //also called .requestLocation
    
    //--------------------------------------------------------------
    // MARK: - CurrentLocationManagerDelegate
    // MARK: -
    //--------------------------------------------------------------
    func currentLocationUpdated(currentLocationManager: CurrentLocationManager, currentLocation: CLLocation){
        
        //---------------------------------------------------------------------
        //forward to TOP VC
        //---------------------------------------------------------------------
        //does nothing
        //        if let colcPlacePickerViewController = self.colcPlacePickerViewController{
        //            // TODO: - rename to currentLocationUpdated
        //            colcPlacePickerViewController.moveToCurrentLocation()
        //        }else{
        //            //self.log.error("colcPlacePickerViewController is nil - set in COLCPlacePickerViewController.viewDidLoad()")
        //        }
        //        //---------------------------------------------------------------------
        //        if let tripPlannerViewController = self.tripPlannerViewController{
        //            tripPlannerViewController.currentLocationUpdated(currentLocation)
        //        }else{
        //            self.log.error("appD.tripPlannerViewController is nil - set in COLCTripPlannerViewController.viewDidLoad()")
        //        }
        //---------------------------------------------------------------------
        //NOISY log.error("TODO APPD currentLocationUpdated")
        
        if let window = self.window {
            if let rootUINavigationController = window.rootViewController as? UINavigationController{
                if let topViewController = rootUINavigationController.topViewController {
                    if topViewController is CurrentLocationManagerDelegate{
                        if let topViewControllerCurrentLocationManagerDelegate = topViewController as? CurrentLocationManagerDelegate {
                            topViewControllerCurrentLocationManagerDelegate.currentLocationUpdated(currentLocationManager: currentLocationManager,
                                                                                                   currentLocation: currentLocation)
                        }else{
                            appDelegate.log.error("topViewController as? CurrentLocationManagerDelegate  is nil")
                        }
                    }else{
                        //noisyappDelegate.log.error("topViewController:[\(topViewController)] doesnt implement CurrentLocationManagerDelegate - cant forward current location")
                    }
                }else{
                    appDelegate.log.error("rootUINavigationController.topViewController is nil")
                }
            }else if let rootUITabBarController = window.rootViewController as? UITabBarController{
                if let selectedViewController = rootUITabBarController.selectedViewController {
                    
                    //UITabBarController >> UINavigationController
                    if let rootUINavigationController = selectedViewController as? UINavigationController{
                        
                        //UITabBarController >> UINavigationController >> VC<CurrentLocationManagerDelegate>
                        if let topViewController = rootUINavigationController.topViewController {
                            if topViewController is CurrentLocationManagerDelegate{
                                if let topViewControllerCurrentLocationManagerDelegate = topViewController as? CurrentLocationManagerDelegate {
                                    topViewControllerCurrentLocationManagerDelegate.currentLocationUpdated(currentLocationManager: currentLocationManager,
                                                                                                           currentLocation: currentLocation)
                                }else{
                                    appDelegate.log.error("topViewController as? CurrentLocationManagerDelegate  is nil")
                                }
                            }else{
                                //noisy
                                //appDelegate.log.error("topViewController:[\(topViewController)] doesnt implement CurrentLocationManagerDelegate - cant forward current location")
                            }
                        }else{
                            appDelegate.log.error("rootUINavigationController.topViewController is nil")
                        }
                    }else{
                        appDelegate.log.error("window.rootViewController as? UINavigationController is nil:window.rootViewController \(window.rootViewController)")
                    }
                    
                    
                }else{
                    appDelegate.log.error("rootUINavigationController.topViewController is nil")
                }
            }else{
                appDelegate.log.error("window.rootViewController as? UINavigationController is nil:window.rootViewController \(window.rootViewController)")
            }
        }else{
            appDelegate.log.error("self.window is nil")
        }
        
    }
    
    func currentLocationFailed(currentLocationManager: CurrentLocationManager, error: Error?)
    {
        self.log.error("currentLocationFailed: error:\(error) - MAP MAY BE STUCK")
    }
    
    
    
    
    //--------------------------------------------------------------
    // MARK: - APPEARANCE
    // MARK: -
    //--------------------------------------------------------------
    var appearanceManager: AppearanceManager = AppearanceManager()
    //--------------------------------------------------------------
    
//    let colcDBManager = COLCDBManager()
    
    
    //---------------------------------------------------------------------
    // TODO: - REMOVE
    var colcPlacePickerViewController : COLCPlacePickerViewController?
    
    //    var colcYelpPlacePickerViewController : COLCYelpPlacePickerViewController?
    
    //    var colcTripPlannerViewController : COLCTripPlannerViewController?
    
    //joyRIDE home screen
    //    var tripPlannerViewController : TripPlannerViewController?
    //------------------------------------------------------------------------------------------------
    //GOOGLE
    //------------------------------------------------------------------------------------------------
    //GOOGLE PLACES API
    var googlePlacesController = GooglePlacesController()
    var googleDirectionsController = GoogleDirectionsController()
    //------------------------------------------------------------------------------------------------
    //GEOCODERs
    //------------------------------------------------------------------------------------------------
    var colcGeocoder = COLCGeocoder()
    //------------------------------------------------------------------------------------------------
    
    
    //LAUNCHES OTHER TRANSIT/MAP APPS
    var colcPlaceAppLauncher = COLCPlaceAppLauncher()
    
    
    
    
    //------------------------------------------------------------------------------------------------
    //GOOGLE
    //------------------------------------------------------------------------------------------------
    //
    //https://console.developers.google.com/
    //USE FIREFOX...
    //brian.clear@cityoflondonconsulting.com   << USE FIREFOX... MAKE SURE YOURE LOGGED IN AS THIS!! NOT clearbrian@googlemail.com
    //N5!
    //https://docs.google.com/document/d/18SYp2cI-NTpjA-mK3j6USMS4vkzvCyHKoeBWz3314Yc/edit
    
    
//    //------------------------------------------------------------------------------------------------
//    //iOS key 1
//    //------------------------------------------------------------------------------------------------
//    
//    //https://console.developers.google.com/apis/credentials/key/171?project=tfl-taxi-ranks
    //used by Google SDK - geocode etc
    let googleMapsApiKey_iOS = "AIzaSyDSY9eXMUphG9oc-qfkgKDDmFrGH7N4Yl4"
//
//    //------------------------------------------------------------------------------------------------
//    //BrowserKey 1
//    //------------------------------------------------------------------------------------------------
//    //Requires Referrer -
//    //*.cityoflondonconsulting.com/*
//    // let googleMapsApiKey_Web = "AIzaSyCh_0tfBGDU7l_RHXuq5zol9amft7e3szg"
//    let googleMapsApiKey_Web =  "AIzaSyD-UQTJ4W1RgtJhamGO7fobLg1at7X1jss"
    
    //no restriction for Directions - web call
    let googleMapsApiKey_Web =  "AIzaSyBRBz99dZS2LnD3h6hFJ5_pOCiWD2JB-1o"
    
    
//
//    //------------------------------------------------------------------------------------------------
//    //SERVER KEY 1
//    //------------------------------------------------------------------------------------------------
//    //lets you add in IP but is also optional
//    //used for Directions API
//    let googleMapsApiKey_Serverkey = "AIzaSyDEZ4bn5vsRhbEawKAv274_EU74nfINHDE"
    
    
    //warning unrestricted - couldnt get iOS to w
//    let googleMapsApiKey1 = "AIzaSyBRBz99dZS2LnD3h6hFJ5_pOCiWD2JB-1o"
    
    //NOTE: if you add an API and its having problems then go to Docs site on google for that API and press GET A KEY
    // when ai added Directions I cliked on GET A KEY and it set up SERVER KEY not iOS key
    //https://developers.google.com/maps/documentation/directions/
    //---------------------------------------------------------------------
    //JAN 27 I changed it from a http website key to ios key - same string
    
    
    //-----------------------------------------------------------------------------------
    let openAppURLKit = OpenAppURLKit()
    
    //--------------------------------------------------------------
    // MARK: - docsDir
    // MARK: -
    //--------------------------------------------------------------
    
    var docsDir :String {
        get {
            let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
            return paths[0]
        }
    }
    
    //------------------------------------------------------------------------------------------------
    //SQLiteDB
    //------------------------------------------------------------------------------------------------
    //creates Documents data.db - changeable in SQLiteDB.swift
    // TODO: - SWIFT3 THOUGHT I WAS USING REALM
    //let db = SQLiteDB.sharedInstance()
    //let db = SQLiteDB.sharedInstance
    
    //----------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------
    var imageTaxi_unknown: UIImage?
    var imageTaxi_working: UIImage?
    var imageTaxi_rest_rank: UIImage?
    var imageTaxi_refreshment_rank: UIImage?

    
    var imageStation: UIImage?
    
    
   
    
    
    
    //--------------------------------------------------------------
    // MARK: - APP START
    // MARK: -
    //--------------------------------------------------------------
    //------------------------------------------------------------------------------------------------
    //MAIN
    //------------------------------------------------------------------------------------------------
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        //NOISY self.log.info("launchOptions:'\(launchOptions)'")
        self.log.info("docsDir:'\(docsDir)'")
        print("docsDir:'\(docsDir)'")
        
        //----------------------------------------------------------------------------------------
        //Map - Taxi images - get gore graphics errors if we try to add to map
        //----------------------------------------------------------------------------------------
    
        self.imageTaxi_unknown = MapImageUtils.textToImage("TAXI", iconColor: .gray)
        
        self.imageTaxi_working = MapImageUtils.textToImage("TAXI", iconColor:  .appColorFlat_Picton_Blue()) //yellow
        
        self.imageTaxi_rest_rank = MapImageUtils.textToImage("TAXI (Rest)", iconColor:AppearanceManager.appColorLight1)
        
        self.imageTaxi_refreshment_rank = MapImageUtils.textToImage("TAXI (Refresh)", iconColor: .appColorFlat_Emerald())

        
        self.imageStation = MapImageUtils.textToImage("STATION", iconColor: .appColorFlat_Amethyst())//, includePin: false)
        
        //----------------------------------------------------------------------------------------
        
        //---------------------------------------------------------------------
        //DEBUG
        //self.printSelector()
        //---------------------------------------------------------------------
        //CMMapLauncher.testopenMaps()
        
        //test_Logger()
        //---------------------------------------------------------------------
        
        //-----------------------------------------------------------------------------------
        //Device Locale - used for directions
        //-----------------------------------------------------------------------------------
        //deviceLanguage()
        find_googleSupportedLanguageCodeForDeviceLanguage()
        //-----------------------------------------------------------------------------------
        
        //-----------------------------------------------------------------------------------
        self.window?.tintColor = AppearanceManager.window_tintColor()
        //-----------------------------------------------------------------------------------
        
        //---------------------------------------------------------------------
        //Google Maps for iOS - COLC
        //https://docs.google.com/document/d/18SYp2cI-NTpjA-mK3j6USMS4vkzvCyHKoeBWz3314Yc/edit
        //-----------------------------------------------------------------------------------
        //    API KEY
        //    AIzaSyBo4nZ-NvbK3dqH6n4TjRXJ6b_OmN_R9_Y
        //    IOS APPLICATIONS
        //    com.cityoflondonconsulting.trips
        //    ACTIVATION DATE
        //    Mar 20, 2015, 10:27:00 AM
        //    ACTIVATED BY
        //    brian.clear@cityoflondonconsulting.com (you)
        //30jan - couldnt get ios app key to work had to use unrestricted one
        //NOTE: directiona api is not an iOS app call its a web call -
        // so i change api key to 
        //https://console.developers.google.com/apis/credentials/key/73?project=tfl-taxi-ranks
        //HTTP referrers (web sites) 
        // and hard coded
        //see forHTTPHeaderField: "Referer")
        
        //-----------------------------------------------------------------------------------
        //For google Place picker - any api called by the SDK but NOT Directions whic is a web call
        GMSServices.provideAPIKey(googleMapsApiKey_iOS)

        //-----------------------------------------------------------------------------------
        
        
        //------------------------------------------------------------------------------------------------
        //FLURRY
        //------------------------------------------------------------------------------------------------
        //Flurry.setDebugLogEnabled(true);
        Flurry.startSession("G92PKKMS954ZT23J4CFM");
        //Flurry.setCrashReportingEnabled(true)
        Flurry.setPulseEnabled(true)
        
        
        //------------------------------------------------------------------------------------------------
        //Google Analytics
        //------------------------------------------------------------------------------------------------
        configureGoogleAnalytics()
        
        //------------------------------------------------------------------------------------------------
        //START LOCATION
        //------------------------------------------------------------------------------------------------
        self.currentLocationManager.delegate = self
        self.currentLocationManager.requestLocation()
        //------------------------------------------------------------------------------------------------
        
        
        
        //------------------------------------------------------------------------------------------------
        //DEBUG
        //------------------------------------------------------------------------------------------------
        //ok - can show in your custom font is installed
        //bug: dragging ttf into project is buggy. XCode put UIAppFonts in generated Info.plist in IPA but I noticed it only does it for some fonts
        //so if you include custom fonts in app - add then to UIAppFonts "MYCUSTOMFONT.ttf"
        //ls /Users/gbxc/Documents/00_ME/00_BITBUCKET_COLC/joyride_for_uber_hailo/00_APPCODE/joyride/Pods/GoogleMaps/Frameworks/GoogleMaps.framework/Versions/A/Resources/GoogleMaps.bundle/GMSCoreResources.bundle/*.ttf
        
        /*
         DroidSansMerged-Regular.ttf
         Roboto-Black.ttf
         Roboto-BlackItalic.ttf
         Roboto-Bold.ttf
         Roboto-BoldItalic.ttf
         Roboto-Italic.ttf
         Roboto-Light.ttf
         Roboto-LightItalic.ttf
         Roboto-Medium.ttf
         Roboto-MediumItalic.ttf
         Roboto-Regular.ttf
         Roboto-Thin.ttf
         Roboto-ThinItalic.ttf
         RobotoCondensed-Italic.ttf
         RobotoCondensed-Regular.ttf
         Siemreap.ttf
         Tharlon-Regular.ttf
         
         */
        
        //UIFont.dumpFontsInApp()
        //        UIFont.dumpFontInInfoPlistUIAppFonts()
        //        UIFont.findFontInInfoPlistUIAppFonts("Cantarell-Regular")
        
        
        //---------------------------------------------------------------------
        //FB SDK
        //------------------------------------------------------------------------------------------------
        //https://developers.facebook.com/quickstarts/1443561855968839/?platform=ios
        //Track App Installs and App Opens
        //App Events let you measure installs on your mobile app ads, create high value audiences for targeting, and view analytics including user demographics. To log an app activation event
        //------------------------------------------------------------------------------------------------
    
        
        return true
        //return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    func configureGoogleAnalytics(){
        log.error("TODO configureGoogleAnalytics")
//        // Configure tracker from GoogleService-Info.plist.
//        var configureError:NSError?
//        GGLContext.sharedInstance().configureWithError(&configureError)
//        assert(configureError == nil, "Error configuring Google services: \(configureError)")
//        
//        // Optional: configure GAI options.
//        let gai = GAI.sharedInstance()
//        // TODO: - TURN BACK ON trackUncaughtExceptions
//        gai?.trackUncaughtExceptions = false  // report uncaught exceptions
//        gai?.logger.logLevel = GAILogLevel.warning  // remove before app release
    }
    
    
    
    
    
    //--------------------------------------------------------------
    // MARK: -  DEVICE LANGUAGE - used in Google Api calls
    // MARK: -
    //--------------------------------------------------------------
    func deviceLanguage(){
        //NOTE
        //Languages use hyphens en_GB
        //localeIdentifiers use underscore:en_GB
        let languageArray = Locale.preferredLanguages
        print("languageArray:\r\(languageArray)")
        //["en-GB", "ko-KR"]
        //I had korean keyboard installed
        
        
        
        let language = languageArray[0]
        print("language[0]:\r\(language)")
        
        let locale: Locale = Locale.current
        //mem address : print("NSLocale.currentLocale():\(locale)")
        
        let localeIdentifier = locale.identifier
        print("NSLocale.currentLocale().localeIdentifier:\(localeIdentifier)")
        
        let canonicalLocaleIdentifierFromString = Locale.canonicalIdentifier(from: localeIdentifier)
        print("canonicalLocaleIdentifierFromString('\(localeIdentifier)'):\(canonicalLocaleIdentifierFromString)")
        //canonicalLocaleIdentifierFromString('en_GB'):en_GB
        
        //-----------------------------------------------------------------------------------
        //split Locale en_GB into parts LocaleCountryCode _ languageCode
        //-----------------------------------------------------------------------------------
        let componentsFromLocaleIdentifierDict = Locale.components(fromIdentifier: localeIdentifier)
        print("componentsFromLocaleIdentifier('\(localeIdentifier)'):\(componentsFromLocaleIdentifierDict)")
        //canonicalLocaleIdentifierFromString('en_GB'):en_GB
        
        //componentsFromLocaleIdentifier('en_GB'):["kCFLocaleCountryCodeKey": "GB", "kCFLocaleLanguageCodeKey": "en"]
        //localeIdentifierFromComponents will take the DICT and generate en_GB
        
        //-----------------------------------------------------------------------------------
        //NOTE: DEVICE LOCALE(en_GB) and LANGUAGE(en-GB) are not the same thing
        //-----------------------------------------------------------------------------------
        print("NSLocale.currentDevice_LocaleIdentifier():\(Locale.currentDevice_LocaleIdentifier())")
        //NOTE LOCALE uses underscore
        //NSLocale.currentDevice_LocaleIdentifier():en_GB
        print("NSLocale.currentDevice_CountryCode():\(Locale.currentDevice_CountryCode())")
        print("NSLocale.currentDevice_LocalLanguageCode():\(Locale.currentDevice_LocaleLanguageCode())")
        
        
        print("NSLocale.currentDevice_CanonicalLanguageIdentifier():\(Locale.currentDevice_LanguageCode())")
        //NOTE LANGUAGE CODE  uses underscore
        ////NSLocale.currentDevice_CanonicalLanguageIdentifier():en-GB
        
        
        
        //NSLocale.currentDevice_CountryCode():Optional("GB")
        //NSLocale.currentDevice_LanguageCode():Optional("en")
        
        Locale.dump_listOfAvailableLocalesSortedWithDisplayNamesInEnglish()
        
        //use in api calls
        //done below in smaller func
        if let googleSupportedLanguageCodeForDeviceLanguage = Locale.deviceLanguageSupportedByGooglePlacesApi() {
            self.log.error("supportedLandCode:\(googleSupportedLanguageCodeForDeviceLanguage)")
        }else{
            self.log.error("supportedLandCode is nil")
        }
        print("")
        
        
        
        //        if let displayNameForKey = inThisLanguage_locale.displayNameForKey(NSLocaleIdentifier, value: "") {
        //            //print("LOCALE[\(displayThisLocale)] in [\(inThisLanguage)] >> '\(displayNameForKey)'")
        //            displayNameForLocale = displayNameForKey
        //        }else{
        //            self.log.error("FAILED locale.displayNameForKey('\(inThisLanguage)', value: '\(inThisLanguage_locale)')")
        //        }
        
        
        
        
        
        //    //SIMULATOR
        //    languageArray:
        //    ["en-US"]
        //    language[0]:
        //    en-US
        //    NSLocale.currentLocale().localeIdentifier country:en_US
        
        //Chinese Simplified
        //        languageArray:
        //        ["zh-Hans-US", "en-US"]
        //        language[0]:
        //        zh-Hans-US
        //        NSLocale.currentLocale().localeIdentifier country:zh-Hans_US
        
        //simpolified
        //        "zh_Hans_CN"
        //        "zh_Hans_HK"
        //        "zh_Hans_MO"
        //        "zh_Hans_SG"
        //        "zh_Hans"
        // Traditional
        //        "zh_Hant_HK"
        //        "zh_Hant_MO"
        //        "zh_Hant_TW"
        //        "zh_Hant"
        //        "zh"
        //
        //ko-KR
        
        
        //let frLocale = NSLocale(localeIdentifier: "fr_FR")
        //print(frLocale.displayNameForKey(NSLocaleIdentifier, value: "fr_FR")!)
        
        
        Locale.dumpDisplayNameForLocale(displayThisLocale: "fr_FR", inThisLanguage:"fr_FR")
        //Locale.displayNameForLocale(displayThisLocale: "fr_FR", inThisLanguage:"en")
        
        //LOCALE[fr_FR] in [fr_FR] >> 'français (France)'
        //LOCALE[fr_FR] in [en] >> 'French (France)'
        
        let availableLocaleIdentifiersArray = Locale.availableIdentifiers
        print("availableLocaleIdentifiersArray:\r\(availableLocaleIdentifiersArray)")
        
        let availableLocaleIdentifiersArraySorted = availableLocaleIdentifiersArray.sorted()
        
        //Display the English name of the locale
        for localeIdentifier in availableLocaleIdentifiersArraySorted{
            Locale.dumpDisplayNameForLocale(displayThisLocale: localeIdentifier, inThisLanguage: "en")
        }
        
    }
    
    var googleSupportedLanguageCodeForDeviceLanguage: String?
    
    func find_googleSupportedLanguageCodeForDeviceLanguage(){
        
        //NOTE
        //Languages use hyphens en_GB
        //localeIdentifiers use underscore:en_GB
        //let languageArray = Locale.preferredLanguages
        //print("languageArray:\r\(languageArray)")
        //["en-GB", "ko-KR"]
        //I had korean keyboard installed
        
        let locale: Locale = Locale.current
        //mem address : print("NSLocale.currentLocale():\(locale)")
        
        let localeIdentifier = locale.identifier
        print("NSLocale.currentLocale().localeIdentifier:\(localeIdentifier)")
        
        
        //use in api calls
        if let googleSupportedLanguageCodeForDeviceLanguage_ = Locale.deviceLanguageSupportedByGooglePlacesApi() {
            //self.log.info("googleSupportedLanguageCodeForDeviceLanguage_:\(googleSupportedLanguageCodeForDeviceLanguage_)")
            googleSupportedLanguageCodeForDeviceLanguage = googleSupportedLanguageCodeForDeviceLanguage_
            //-----------------------------------------------------------------------------------
            //DEBUG - force device language
            //not really noticable as london businesses would have name in english regardless of whose searching e.g. korean
            //the place info does have info
            //googleSupportedLanguageCodeForDeviceLanguage = "ko-KR"
            //in nearby search Google Address the 'UK' part will bein Korean
            //-----------------------------------------------------------------------------------
        }else{
            self.log.error("googleSupportedLanguageCodeForDeviceLanguage is nil")
        }
    }
    
    
    
    
    //--------------------------------------------------------------
    // MARK: - APP LIFECYCLE.. cont
    // MARK: -
    //--------------------------------------------------------------
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        //        FBSDKAppEvents.activateApp()
        
        //------------------------------------------------
        //reload top most table view to refresh dynamic types
        //------------------------------------------------
        if let topParentViewController = self.topParentViewController {
            topParentViewController.applicationDidBecomeActive_refreshTableView()
        }else{
            //noisy self.log.error("self.topParentViewController is nil - if you want to refresh the table dynamic text when app enters foreground set this")
        }
        
        
    }
    //-----------------------------------------------
    //used to call applicationDidBecomeActive_refreshTableView
    var topParentViewController : ParentViewController?
    
    
    //------------------------------------------------
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - OPEN URL
    //--------------------------------------------------------------
    //    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
    //
    //
    //        //now to do - deprecated use openURL:...options:
    //        return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
    //    }
    //
    ////    func application(app: UIApplication, openURL url: NSURL, options: [String : AnyObject]) -> Bool{
    ////        //worked but didnt show then paramter in the URL
    //////api said
    ////        //options["UIApplicationOpenURLOptionsSourceApplicationKey"] as! String,
    ////
    ////        //Called from test app
    ////        //openURL:["UIApplicationOpenURLOptionsSourceApplicationKey": com.cityoflondonconsulting.TestPPUrlScheme, "UIApplicationOpenURLOptionsOpenInPlaceKey": 0]
    ////
    ////        print("openURL:\(options)")
    ////
    ////        return FBSDKApplicationDelegate.sharedInstance().app
    ////        return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
    ////
    ////    }
    //
    //    func application(application: UIApplication, handleOpenURL url: NSURL) -> Bool{
    //
    //        self.log.info("handleOpenURL:'\(url)'")
    //        return true
    //    }
    //
    //    //api said this is deprecated but its mentioned in https://developer.apple.com/videos/play/wwdc2015-509/
    //    //dperrcation said use openURL:options but that doesnt provide the full url
    //
    ////    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool{
    ////        self.log.info("openURL:'\(url)'")
    ////        self.log.info("sourceApplication:'\(sourceApplication)'")
    //////        self.log.info("annotation:'\(annotation.cla)'")
    ////
    ////       let urlComponents = NSURLComponents(string: "\(url)")
    ////        print("urlComponents:\(urlComponents)")
    ////        print("queryItems:\(urlComponents?.queryItems)")
    ////
    //////        'placespicker://?fffff=124324'
    //////        urlComponents:Optional(<NSURLComponents 0x15d7b7630>
    //////            {scheme = placespicker,
    //////                user = (null), password = (null),
    //////                host = , port = (null),
    //////                path = ,
    //////                query = fffff=124324,
    //////                fragment = (null)})
    //////        queryItems:Optional([<NSURLQueryItem 0x15d7b5f90> {name = fffff, value = 124324}])
    ////
    ////        return true
    ////    }
    
    
    //before iOS9
    //in iOS8 if implemented will be called if not nothing happens
    //in iOS9 not called
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool{
        print("iOS8 openURL CALLED: url:[\(url)]")
        print("iOS8 openURL CALLED: sourceApplication:[\(sourceApplication)]")
        //---------------------------------------------------------------------
        // print("iOS8 openURL CALLED: annotation:[\(annotation?)]")
        //---------------------------------------------------------------------
        //    openURL CALLED: url:[joyride://oauth/callback_github?code=b880339068409e7fe923&state=8E7F9061]
        //    openURL CALLED: sourceApplication:[Optional("com.apple.mobilesafari")]
        //    openURL CALLED: annotation:[{
        //    ReferrerURL = "https:github.com/login/oauth/authorize?client_id=be45feb27af1729e6312&redirect_uri=joyride://oauth/callback_github&scope=user&state=8E7F9061&allow_signup=false";
        //    }]
        //---------------------------------------------------------------------
appDelegate.log.error("TODO FBSDKApplicationDelegate.sharedInstance OFF")
//        let urlString = "\(url)"
//        if urlString.hasPrefix("fb1083378095037687"){
//            print("App opened FB SCHEME:\(url.scheme)")
//            //http://stackoverflow.com/questions/32299271/facebook-sdk-login-never-calls-back-my-application-on-ios-9
//            return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
//        }else{
//            //foursquare oauth
//            return self.handleOpenURL(url)
//        }
        
        return self.handleOpenURL(url)
    }
    
    
    //use on above - FBSDK only gives exampl for older api
    //    //iOS9 - will be called if both openURL implemented - but in iOS8 only one above is called
    //    func application(app: UIApplication, openURL url: NSURL, options: [String : AnyObject]) -> Bool{
    //
    //        self.log.info("iOS9 openURL CALLED: url:[\(url)]")
    //        self.log.info("iOS9 openURL CALLED: options:[\(options)]")
    //
    //        let urlString = "\(url)"
    //
    //        if urlString.hasPrefix("fb"){
    //            print("App opened FB SCHEME:\(url.scheme)")
    //            //http://stackoverflow.com/questions/32299271/facebook-sdk-login-never-calls-back-my-application-on-ios-9
    //            return FBSDKApplicationDelegate.sharedInstance().application(app, openURL: url, sourceApplication: UIApplicationOpenURLOptionsSourceApplicationKey, annotation: UIApplicationOpenURLOptionsAnnotationKey)
    //        }else{
    //
    //            return self.handleOpenURL(url)
    //        }
    //
    //    }
    
    func handleOpenURL(_ url: URL) -> Bool{
        let handleOpenURL = true
        //--------------------------------------------------------------------
        //openURL CALLED: url:[joyrider://oauth/callback_github?code=41e58b05fa1a1025b317&state=8E7F9061]
        //openURL CALLED: options:[["UIApplicationOpenURLOptionsSourceApplicationKey": com.apple.mobilesafari, "UIApplicationOpenURLOptionsOpenInPlaceKey": 0]]
        //---------------------------------------------------------------------
        
        
        //todo - foursquare
        if COLCOAuth2Manager.isOAuthAppScheme(url){
            self.colcOAuth2Manager.handleOauth2CallbackURL(url)
            
        }else{
            //not FB event or joyrider://
            print("App opened with unhandled scheme:\(url.scheme)")
            //todo - foursquare
        }
        return handleOpenURL
    }
    
    
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - VERSION NUMBER
    //--------------------------------------------------------------
    //returns: 'Clarksons Platou - v 1.1'
    //note this is the BUILD number set in INFO tab of Target not VERSION field
    func appVersionString()-> String{
        var appVersionString = "© Clarksons Platou 2016"
        let bundleVersionString_ = bundleShortVersionString()
        
        appVersionString = "City Of London Consulting Limited © 2016 - v \(bundleVersionString_)"
        return appVersionString
    }
    
    //returns: '1'
    //note this is the BUILD number set in INFO tab of Target for VERSION field use bundleShortVersionString
    func bundleVersionString()->String{
        var bundleVersionString = ""
        let stringkCFBundleVersionKey = kCFBundleVersionKey as String
        if let bundleVersion = Bundle.main.object(forInfoDictionaryKey: stringkCFBundleVersionKey) as? String{
            bundleVersionString = bundleVersion
        }
        
        return bundleVersionString
    }
    //v 1.1
    func bundleShortVersionString()->String{
        var bundleShortVersionString = ""
        //let stringkCFBundleVersionKey = kCFBundleVersionKey as String
        if let bundleVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String{
            bundleShortVersionString = bundleVersion
        }
        
        return bundleShortVersionString
    }
}

