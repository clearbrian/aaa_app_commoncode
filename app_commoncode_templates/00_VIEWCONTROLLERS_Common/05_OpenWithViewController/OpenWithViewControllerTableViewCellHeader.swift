//
//  OpenWithViewControllerTableViewCellHeader
//  joyride
//
//  Created by Brian Clear on 27/05/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit 

class OpenWithViewControllerTableViewCellHeader : UITableViewCell{
    
    @IBOutlet weak var labelName: UILabel!
    
    var customFontApplied = false
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //print("COLCFacebookEventTableViewCelll init coder")
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        super.init(style: UITableViewCellStyle.subtitle, reuseIdentifier: reuseIdentifier)
        
    }
    
    func applyCustomFont(){
        if customFontApplied{
            //cell created already - BODY is replace with customfont - if we reuse it and call this again it will try and convert font again
        }else{
            
            
            self.labelName?.applyCustomFontForCurrentTextStyle()
            
            customFontApplied = true
        }
    }
}
