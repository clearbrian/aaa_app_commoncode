//
//  EditCLKPlaceAddressViewController.swift
//  joyride
//
//  Created by Brian Clear on 26/05/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit
protocol EditCLKPlaceAddressViewControllerDelegate {
    func controller(_ controller: EditCLKPlaceAddressViewController, didEditCLKPlace: CLKPlace)
}
class EditCLKPlaceAddressViewController: ParentViewController, UITextFieldDelegate  {
    
    @IBOutlet weak var textFieldNameAndAddress: UITextView!
    
    var delegate: EditCLKPlaceAddressViewControllerDelegate?
    //-----------------------------------------------------------------------------------
    var clkPlaceToEdit: CLKPlace? {
        didSet {
            print("clkPlaceToEdit - didSet: Old value: '\(String(describing: oldValue))', New value: '\(String(describing: clkPlaceToEdit))'")
            fillTextFieldAddress()
        }
    }
    //-----------------------------------------------------------------------------------
    func fillTextFieldAddress(){
        
        if let clkPlaceToEdit = self.clkPlaceToEdit {
            self.textFieldNameAndAddress?.text = "\(clkPlaceToEdit.name_formatted_address)"
            
        }else{
            //user pressed CLEAR
            self.textFieldNameAndAddress?.text = ""
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fillTextFieldAddress()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // called when 'return' key pressed. return NO to ignore.
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        return true
    }
    
    @IBAction func buttonCancel_Action(_ sender: AnyObject) {
        self.dismiss(animated: true) { () -> Void in
            
        }
    }
    
    @IBAction func buttonSave_Action(_ sender: AnyObject) {
        
        if self.textFieldNameAndAddress.text == "" {
            CLKAlertController.showAlertInVC(self, title: "Error", message: "Please enter some text")
        }else{
            //---------------------------------------------------------------------
            if let _ = self.clkPlaceToEdit {
                self.clkPlaceToEdit?.name_formatted_address = self.textFieldNameAndAddress.text
                //clear the old name as may be contact name e.g. andi Case
                //self.clkPlaceToEdit?.name = ""
            }else{
                self.log.error("self.clkPlaceToEdit is nil")
            }
            //---------------------------------------------------------------------
            
            
            self.dismiss(animated: true) { () -> Void in
                if let delegate = self.delegate {
                    if let clkPlaceToEdit = self.clkPlaceToEdit {
                        delegate.controller(self, didEditCLKPlace: clkPlaceToEdit)
                    }else{
                        self.log.error("self.clkPlaceToEdit  is nil")
                    }
                }else{
                    self.log.error("self.delegate is nil")
                }
            }
        }
    }
    
    
}
