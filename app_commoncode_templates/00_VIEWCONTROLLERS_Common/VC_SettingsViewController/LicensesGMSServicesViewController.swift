//
//  LicensesGMSServicesViewController.swift
//  joyride
//
//  Created by Brian Clear on 18/10/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps

class LicensesGMSServicesClientViewController: ParentViewController  {
    
    @IBOutlet weak var textViewLicences: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.title = "Google Maps - License"
        let gmsServicesClient_licenseText = GMSServices.openSourceLicenseInfo()
        
        self.textViewLicences.text = "\(gmsServicesClient_licenseText)"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
