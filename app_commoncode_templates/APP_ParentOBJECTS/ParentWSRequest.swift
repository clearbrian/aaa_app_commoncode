//
//  ParentWSRequest.swift
//  joyride
//
//  Created by Brian Clear on 07/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation
//import Alamofire

//Handles ANY WS request
class ParentWSRequest : ParentSwiftObject{
    
    //should be set by subclass
    var query: String?
    
//    var method: Alamofire.Method = Alamofire.Method.GET
    var parameters: [String: AnyObject] = [String: AnyObject]()
    
    override init(){
        super.init()

    }
    //only used in TFL
    var webServiceEndpoint :String?{
        get {
            //return kWEBSERVICE_ENDPOINT_GooglePlaces
            return ""
        }
    }

    
    var urlString :String? {
        get {
            
            var urlString_ :String?
            //---------------------------------------------------------------------
//            if let query = query{
//                //ENDPOINT:https://maps.googleapis.com/maps/api/place
//                //   query:                                          /details/json
//                urlString_ = "\(kWEBSERVICE_ENDPOINT_GooglePlaces)\(query)"
//                
//                //https://maps.googleapis.com/maps/api/place/details/json
//                // TODO: - cleanup
//                let _ = parametersAppendAndCheckRequiredFieldsAreSet()
//                
//            }else{
//                self.log.error("query is nil - should be set by subclass")
//            }
            //---------------------------------------------------------------------
            return urlString_
        }
    }
    //TOP MOST METHOD
    func parametersAppendAndCheckRequiredFieldsAreSet() -> Bool{
        var requiredFieldsAreSet = false
        
        //add any extra parameter though prob none at this level
        //parameters["paramname"] = "paramvalue"
        
        requiredFieldsAreSet = true
        
        return requiredFieldsAreSet
    }
    
    
    //--------------------------------------------------------------
    // MARK: - parameters
    // MARK: -
    //--------------------------------------------------------------
    
    func addToParametersIfNotNil(_ name: String, string: String?){
        
        if string == nil{
            self.log.error("param is nil:\(name)")
        }else{
            addToParametersIfNotNil(name, value: string as AnyObject)
        }
    }
    func addToParametersIfNotNil(_ name: String, value:AnyObject?){
        if value == nil{
            self.log.error("param is nil:\(name)")
        }else{
            parameters[name] = value
        }
    }
    
    
}
