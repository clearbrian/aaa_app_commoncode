//
//  ParentPickerViewController.swift
//  joyride
//
//  Created by Brian Clear on 10/11/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import MapKit
// class FBViewController: ParentViewController
import UIKit



class ParentPickerViewController: ParentViewController, MKMapViewDelegate{
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var labelNearestAddress: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //view for annotation
        mapView?.delegate = self
        
    }
    
    
    
    //TODO: - move up - made generic clkPlace
    //also used in FBVc/CalendarEvent/Contacts
    //moved in from appd
    let colcGeocoder = COLCGeocoder()
    
    func geocodeCLKPlace(clkPlace: CLKPlace){
        //------------------------------------------------------------------------------------------------
        if let clLocation = clkPlace.clLocation {
            
            //---------------------------------------------------------------------
            //REVERSE GEOCODE - START
            //---------------------------------------------------------------------
            self.colcGeocoder.reverseGeocodePlace(clkPlace: clkPlace,
                 
                 success:{ (clkPlace: CLKPlace) -> Void in
                    //OK clkFacebookEventPlace >> add geocode addreess >> clkPlace
                    self.displayGeocodedAddress_returnToDelegate(clkPlace: clkPlace)
                    
            },
                 failure:{ (error: Error?) -> Void in
                    self.log.error("[GEOCODE CURRENT LOCATION FAILED] currentLocation(\(clLocation)) error:\(String(describing: error)) ")
                    CLKAlertController.showAlertInVC(self, title: "Error getting exact address. Please check the event on Facebook", message: error?.localizedDescription)
                    
            }
            )
            //---------------------------------------------------------------------
            //REVERSE GEOCODE - END
            //---------------------------------------------------------------------
        }else{
            
            //---------------------------------------------------------------------
            //SEARCH ADDRESS STRING
            //---------------------------------------------------------------------
            //CLLocation missing from event info - try a mapkit search on the name
            /*
             place =                 {
             name = "The Old George, 379 Bethnal Green Rd, London E2 0AN";
             };
             */
            //---------------------------------------------------------------------
            self.log.error("colcFacebookEvent.clLocation is nil - lookup place.name with mapkit")

            
            // TODO: - untested all FB events have clkFacebookEventPlace.colcFacebookEvent. lat/lng set
            //            if let place = selectedCOLCFacebookEvent.place {
            //                //if no location then Place.name is usually the address
            //
            //                if let addressString: String = place.name as? String{
            //}
            //------------------------------------------------------------------------------------------------
            //MKLocalSearch
            //------------------------------------------------------------------------------------------------
            
            //if lat/lng not set then clkFacebookEventPlace.colcFacebookEvent.place is the address string
            
            //location = 	Wyndham's Theatre, London, England, United Kingdom;
            //structuredLocation = 	EKStructuredLocation <0x1706be1e0> {title = Wyndham's Theatre, London, England, United Kingdom; address = (null); geo = (null); abID = (null); routing = (null); radius = 0.000000;};

                
            if let addressString = clkPlace.name {
                
                
                //------------------------------------------------------------------------------------------------
                //forwardGeocode_MKLocalSearch START
                //------------------------------------------------------------------------------------------------
                self.colcGeocoder.forwardGeocode_MKLocalSearch(addressString: addressString,
                                                                      clkPlaceIn: clkPlace,
                                                                      
                                                                      success:{ (clkPlace: CLKPlace) -> Void in
                                                                        //-----------------------------------------------
                                                                        self.displayGeocodedAddress_returnToDelegate(clkPlace: clkPlace)
                                                                        //-----------------------------------------------
                },
                                                                      failure:{ (error: Error?) -> Void in
                                                                        //-----------------------------------------------
                                                                        self.log.error("[forwardGeocode_MKLocalSearch FB EVENT] error:\(String(describing: error))")
                                                                        
                                                                        CLKAlertController.showAlertInVC(self, title: "Error getting exact address. Please check the event on Facebook", message: error?.localizedDescription)
                                                                        //-----------------------------------------------
                }
                )//forwardGeocode_MKLocalSearch
                
                //------------------------------------------------------------------------------------------------
                //forwardGeocode_MKLocalSearch END
                //------------------------------------------------------------------------------------------------
                
                
            }else{
                appDelegate.log.error("clkFacebookEventPlace.name is nil")
                //CLKAlertController.showAlertInVC(self, title: "Error getting exact address. Please check the event on Facebook", message: "Facebook place address is empty")
                showErrorGettingAddress()
            }
            //---------------------------------------------------------------------
        }
        //------------------------------------------------------------------------------------------------
    }
    
    //should subclass - as mention Facebook or calendar
    func showErrorGettingAddress(){
        
        CLKAlertController.showAlertInVC(self, title: "SUBCLASS THIS DONT MENTION FB IN PARENT: Error getting exact address. Please check the event on Facebook", message: "Facebook place address is empty")
        
    }
// TODO: - removed to get TaxiRank building for app store - reject as Contacts framework included and needed permission entry in plist
//    //pickContactAddressReturned - string >> address
//    // TODO: - small method needed? - special case
//    func forwardAndOrReverseGeocodePlace(clkContactPlace: CLKContactPlace){
//        
//        self.colcGeocoder.forwardAndOrReverseGeocodePlace(clkContactPlace,
//                                                                 
//            success:{ (clkContactPlace: CLKContactPlace) -> Void in
//
//                //-----------------------------------------------
//                self.displayGeocodedAddress_returnToDelegate(clkPlace: clkContactPlace)
//                //-----------------------------------------------
//                
//        },
//            failure:{ (error: Error?) -> Void in
//                //-----------------------------------------------------
//                self.log.error("[GEOCODE CONTACT FAILED] error:\(String(describing: error)) ")
//                CLKAlertController.showAlertInVC(self, title: "Error", message: "Error finding exact location for that address." )
//                //-----------------------------------------------------
//        })
//    }



    func displayGeocodedAddress_returnToDelegate(clkPlace: CLKPlace){
        if let clkGeocodedAddressesCollection = clkPlace.clkGeocodedAddressesCollection {
            
            if let bestOrFirstCLKAddress = clkGeocodedAddressesCollection.bestOrFirstCLKAddress {
                
                //---------------------------------------------------------------------
                //store geocoded Address with place that has CLLocation address geocoded from
                //self.clkGeocodedAddress = bestOrFirstCLKAddress
                appDelegate.log.debug("[GEOCODE FB OK] bestOrFirstCLKAddress:\(bestOrFirstCLKAddress)")
                
                //---------------------------------------------------------------------
                //THE GEOCODED ADDRESS - geocoded from the lat/lng for the FBEvent.location lat/lng
                self.labelNearestAddress?.text = clkPlace.formatted_address_geocoded
                self.labelNearestAddress?.textColor = UIColor.purple
                //---------------------------------------------------------------------
            }else{
                appDelegate.log.error("clkGeocodedAddressesResponse.bestOrFirstCLKAddress is nil")
                
            }
            
            showCollectionOnMap(clkPlace:clkPlace)
            //---------------------------------------------------------------------
            self.returnToDelegate(clkPlace: clkPlace)
            //---------------------------------------------------------------------
        }else{
            appDelegate.log.error("clkPlace is nil")
        }
    }
    
    //override - some subclasses dont have address
    func showCollectionOnMap(clkPlace: CLKPlace){
        
        //self.mapView?.showbestOrFirstFromCollectionOnMap(clkGeocodedAddressesCollection: clkGeocodedAddressesCollection)appDelegate.log.error("clkPlace.clkGeocodedAddressesCollection is nil")

        appDelegate.log.error("SUBCLASS showCollectionOnMap")

    }
    
    // TODO: - make generic
    func returnToDelegate(clkPlace: CLKPlace){
        self.log.error("MUST SUBCLASS returnToDelegate")
    }
    
    //--------------------------------------------------------------
    // MARK: - MKMapViewDelegate
    // MARK: -
    //--------------------------------------------------------------

    public func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?{
        
        var mkAnnotationViewReturned : MKAnnotationView?
        
        //------------------------------------------------------------------------------------------------
        //classes that IMPLEMENT MKAnnotation not subclass
        //------------------------------------------------------------------------------------------------

        if (annotation is MKUserLocation) {
            //if annotation is not an MKPointAnnotation (eg. MKUserLocation),
            //return nil so map draws default view for it (eg. blue dot)...
            mkAnnotationViewReturned = nil
            
        }
        else if annotation is COLCMKAnnotation
        {
            //------------------------------------------------------------------------------------------------
            //COLCMKAnnotation implements MKAnnotation - easier to show title
            //------------------------------------------------------------------------------------------------

            
            if let colcMKAnnotation: COLCMKAnnotation = annotation as? COLCMKAnnotation {
                //------------------------------------------------------------------------------------------------
                let reuseId = "COLCMKAnnotationIdentifier"
                //DONT REUSE - we only ever have one pin
                //adding COntact >> Andi Case twice - second time added Pin not
//                    if let mkAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId){
//                        //we are re-using a view, update its annotation reference...
//                        mkAnnotationView.annotation = annotation
//                        
//                    }else{
                    //------------------------------------------------------------------------------------------------
                    //NEW
                    //------------------------------------------------------------------------------------------------
                    
                    let mkAnnotationViewNew = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
                    //---------------------------------------------------------------------
                    //dont use title clashes with VC.title
                    var pinTitle = "Apple Location"
                    
                    //MKAnnotation is a protocol
                    
                    //---------------------------------------------------------------------
                    //PIN TITLE - get from Annotation.title
                    //---------------------------------------------------------------------
                    //    if let annotationTitleForPin = annotation.title {
                    //        pinTitle = annotationTitleForPin!  WEIRD think its cos MKAnnotation is a protocol
                    //    }else{
                    //        appDelegate.log.error("annotation.title is nil - using default")
                    //    }
                    //---------------------------------------------------------------------
                    //the title is in the wrapper class colcMKAnnotation.title not in the CLPlacemark.title as it requires addressDictionary
                    if let annotationTitleForPin = colcMKAnnotation.title {
                        pinTitle = annotationTitleForPin
                    }else{
                        appDelegate.log.error("annotation.title is nil - using default")
                    }
                    //---------------------------------------------------------------------
                    //---------------------------------------------------------------------
                    
                    //pinTitle = "very long title\rwithcarriage return"
                    let annotationImage = MapImageUtils.textToImage(pinTitle, iconColor: UIColor.appColorFlat_Emerald())
                    //mkAnnotationViewNew.image = UIImage(named:"bar.png")
                    
                    mkAnnotationViewNew.image = annotationImage
                    //---------------------------------------------------------------------
                    mkAnnotationViewNew.canShowCallout = false
                    
                    mkAnnotationViewReturned = mkAnnotationViewNew
                    //---------------------------------------------------------------------
                    
//                }
                //------------------------------------------------------------------------------------------------
                
            }
            else
            {
                appDelegate.log.error("MAP PIN CANT DRAW  UNKNOWN annotation class USE annotation as? COLCMKAnnotation")
            }
        }
        else if annotation is MKPlacemark
        {
            self.log.error("MAP PIN - dont use MKPlacemark use colcMKAnnotation - easier to set title")
            
            if let _: MKPlacemark = annotation as? MKPlacemark {
             
                //------------------------------------------------------------------------------------------------
                //    let reuseId = "MKAnnotationViewAddress"
                //    
                //    if let mkAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId){
                //        //we are re-using a view, update its annotation reference...
                //        mkAnnotationView.annotation = annotation
                //        
                //        
                //    }else{
                //        //------------------------------------------------------------------------------------------------
                //        //NEW
                //        //------------------------------------------------------------------------------------------------
                //        let mkAnnotationViewNew = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
                //        //---------------------------------------------------------------------
                //        //dont use title clashes with VC.title
                //        var pinTitle = "Apple Location"
                //        
                //        //MKAnnotation is a protocol
                //      
                //        self.log.error("MAP PIN - dont use MKPlacemark use colcMKAnnotation - easier to set title")
                //        //---------------------------------------------------------------------
                //        //PIN TITLE - get from Annotation.title
                //        //---------------------------------------------------------------------
                //        //    if let annotationTitleForPin = annotation.title {
                //        //        pinTitle = annotationTitleForPin!  WEIRD think its cos MKAnnotation is a protocol
                //        //    }else{
                //        //        appDelegate.log.error("annotation.title is nil - using default")
                //        //    }
                //        //---------------------------------------------------------------------
                //        if let annotationTitleForPin = mkPlacemark.title {
                //            pinTitle = annotationTitleForPin
                //        }else{
                //            appDelegate.log.error("annotation.title is nil - using default")
                //        }
                //        //---------------------------------------------------------------------
                //        //---------------------------------------------------------------------
                //        let annotationImage = MapImageUtils.textToImage(pinTitle, iconColor: UIColor.appColorFlat_Emerald())
                //        //mkAnnotationViewNew.image = UIImage(named:"bar.png")
                //    
                //        mkAnnotationViewNew.image = annotationImage
                //        //---------------------------------------------------------------------
                //        mkAnnotationViewNew.canShowCallout = false
                //    
                //        mkAnnotationViewReturned = mkAnnotationViewNew
                //        //---------------------------------------------------------------------
                //
                //    }
                //------------------------------------------------------------------------------------------------

            }
            else
            {
                appDelegate.log.error("MAP PIN CANT DRAW  UNKNOWN annotation class USE annotation as? COLCMKAnnotation")
            }
        }
        else {
            appDelegate.log.error("UNKNOWN implementation of protocol MKAnnotation")
        }
        
        return mkAnnotationViewReturned
    }
}
