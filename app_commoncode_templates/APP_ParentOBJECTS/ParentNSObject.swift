//
//  ParentNSObject.swift
//  SandPforiOS
//
//  Created by Brian Clear on 17/06/2015.
//  Copyright (c) 2015 CLARKSONS RESEARCH SERVICES LIMITED. All rights reserved.
//

import Foundation

//Parent class for NSObject subclasses e.g. 
//needed a paretn class to manage logging - on instance per VC/Controller(Swift)/Controller(NSObject)

//To use NSXMLParserDelegate your class must also conform to NSObjectProtocol so has to be subclass of NSObject
//Error
//class LoginController:  NSXMLParserDelegate{
//ERROR: Type 'LoginController' does not conform to protocol 'NSObjectProtocol'

//class LoginController: NSObject, NSXMLParserDelegate{
//So Parent class has to be NSObject

class ParentNSObject: NSObject{
    //------------------------------------------------
    //LOGGING - Requires XCodeColors plugin
    //MUST BE DECLARED IN EACH CLASS THAT USES IT
    
    //let log = MyXCodeColorsLogger.defaultInstance
    let log = MyXCodeEmojiLogger.defaultInstance
    
    //create seperate instance for each VC so we can turn them on/off as we develop them
    //let log = MySwiftyBeaverLogger()
    //------------------------------------------------
    
    //SUB CLASS MUST CALL super.init else loggin not configured
    //FOR NSObject use override
    override init() {
        super.init()
        
        configureLogging()
    }
    
    //
    func configureLogging(){
        //log = MyXCodeColorsLogger is declared above in parent
        
        //------------------------------------------------
        //LOG LEVEL
        //------------------------------------------------
        //case Verbose = 1 << SHOW EVERYTHING
        //case Debug
        //case Info
        //case Warning
        //case Error    << RELEASE
        //case None     << ALL OFF
        //------------------------------------------------
//        self.log.outputLogLevel = .Verbose_5
        //------------------------------------------------
        //[fg220,50,47;[Error] This is Error Log [;[fg88,110,117;[AppDelegate.swift:34] testLogging() 2015-06-09 11:14:15.811[;
        
        //------------------------------------------------
        //LOG FORMAT
        //------------------------------------------------
//        self.log.showFileInfo = true
//        self.log.showDate = true
//        self.log.showLogLevel = true
//        self.log.showFunctionName = true
    }
}
