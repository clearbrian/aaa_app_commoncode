//
//  ParentMappable.swift
//  joyride
//
//  Created by Brian Clear on 09/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation
//used to be cocoapods but i added custom code so copied objectmapper pod into code
//import ObjectMapperr

class ParentMappable : Mappable {
    
    //parent calls mapping() which should call subclassed versions
    required init?(map: Map){
        mapping(map: map)
        
// TODO: - SWIFT3 - had to use swift3 from github so lost mu changes - still in project see Finder
//        if map.findUnMappedFields() > 0{
//            self.log.error("UNMAPPED JSON KEYS[\(self)]:\(map.JSONDictionaryUnMapped)")
//        }
    }
    
    func mapping(map: Map){
        self.log.error("should subclass")
    }
    
    //------------------------------------------------
    //LOGGING - Requires XCodeColors plugin
    //MUST BE DECLARED IN EACH CLASS THAT USES IT
    
    //let log = MyXCodeColorsLogger.defaultInstance
    let log = MyXCodeEmojiLogger.defaultInstance
    
    
    //create seperate instance for each VC so we can turn them on/off as we develop them
    //let log = MySwiftyBeaverLogger()
    //------------------------------------------------
    
    //SUB CLASS MUST CALL super.init else loggin not configured
    init() {
        //super.init()
        
        configureLogging()
    }
    
    func configureLogging(){
        //log = MyXCodeColorsLogger is declared above @ UIApplicationMain
        
        //------------------------------------------------
        //LOG LEVEL
        //------------------------------------------------
        //case Verbose = 1 << SHOW EVERYTHING
        //case Debug
        //case Info
        //case Warning
        //case Error    << RELEASE
        //case None     << ALL OFF
        //------------------------------------------------
        //        self.log.outputLogLevel = .Verbose_5
        //------------------------------------------------
        //[fg220,50,47;[Error] This is Error Log [;[fg88,110,117;[AppDelegate.swift:34] testLogging() 2015-06-09 11:14:15.811[;
        
        //------------------------------------------------
        //LOG FORMAT
        //------------------------------------------------
        //        self.log.showFileInfo = true
        //        self.log.showDate = true
        //        self.log.showLogLevel = true
        //        self.log.showFunctionName = true
    }
}
