//
//  UIColor+Clarksons.swift
//  SandPforiOS
//
//  Created by Brian Clear on 10/06/2015.
//  Copyright (c) 2015 CLARKSONS RESEARCH SERVICES LIMITED. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    
    public class func appBaseColor() -> UIColor { 
        
        //return UIColor(red: 26.0/255.0,  green:36.0/255.0,  blue:41.0/255.0,  alpha:1.0)
        return UIColor.appColorCYAN()
    }
    
    public class func appColorMIDNIGHTBLUE() -> UIColor { 
        //clarksons base color - Midnight blue from website
        //http://site.clarksons.com/images/background.png
        return UIColor(red: 26.00/255.0,  green:36.00/255.0,  blue:41.00/255.0,  alpha:1.00)
    }
//    public class func appColorCYAN() -> UIColor { 
//        return UIColor(red: 29.00/255.0,  green:181.00/255.0,  blue:204.00/255.0,  alpha:1.00)
//    }
    public class func appColorCYAN() -> UIColor {
        return UIColor(red: 55.00/255.0,  green:142.00/255.0,  blue:218.00/255.0,  alpha:1.00)
    }

    public class func appColorYELLOW() -> UIColor {
        return UIColor(red: 208.00/255.0,  green:197.00/255.0,  blue:42.00/255.0,  alpha:1.00)
    }
    
    public class func appColorREDFLAT() -> UIColor { 
        return UIColor(red: 207.00/255.0,  green:28.00/255.0,  blue:29.00/255.0,  alpha:1.00)
    }
    
    public class func appColorGREENLIGHT() -> UIColor { 
        return UIColor(red: 88.00/255.0,  green:169.00/255.0,  blue:45.00/255.0,  alpha:1.00)
    }
    
    public class func appColorGREEN() -> UIColor { 
        return UIColor(red: 68.00/255.0,  green:170.00/255.0,  blue:66.00/255.0,  alpha:1.00)
    }
    public class func appColorORANGE() -> UIColor { 
        return UIColor(red: 252.00/255.0,  green:108.00/255.0,  blue:33.00/255.0,  alpha:1.00)
    }
    public class func appColorPURPLE() -> UIColor { 
        return UIColor(red: 246.00/255.0,  green:26.00/255.0,  blue:162.00/255.0,  alpha:1.00)
    }
    
    //taken from Clarksons annual report - not on official site but
    public class func appColorBLUEDARK() -> UIColor { 
        return UIColor(red: 3.00/255.0,  green:49.00/255.0,  blue:76.00/255.0,  alpha:1.00)
    }
    public class func appColorBLUELIGHT() -> UIColor { 
        return UIColor(red: 8.00/255.0,  green:81.00/255.0,  blue:111.00/255.0,  alpha:1.00)
    }
    public class func appColorBLUEGREEN() -> UIColor { 
        return UIColor(red: 19.00/255.0,  green:133.00/255.0,  blue:151.00/255.0,  alpha:1.00)
    }
    
    public class func appColorGray() -> UIColor {
        return UIColor(red: 226.00/255.0,  green:228.00/255.0,  blue:229.00/255.0,  alpha:1.00)
    }
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - https://flatuicolors.com/
    //--------------------------------------------------------------

    public class func appColorFlatPinkRed() -> UIColor {
        return UIColor(red: 255.00/255.0,  green:38.00/255.0,  blue:82.00/255.0,  alpha:1.00)
    }
    
    public class func appColorFlat_Turquoise() -> UIColor {
        return UIColor(red: 26.00/255.0,  green:188.00/255.0,  blue:156.00/255.0,  alpha:1.00)
    }
    
    public class func appColorFlat_Purple_SEANCE() -> UIColor {
        return UIColor(red: 154.0/255.0,  green:18.0/255.0,  blue:179.0/255.0,  alpha:1.00)
    }

    
    //Emerald
    public class func appColorFlat_Emerald() -> UIColor {
        return UIColor(red: 46.00/255.0,  green:204.00/255.0,  blue:113.00/255.0,  alpha:1.00)
    }

    
    //PETER RIVER
    //rgb(52, 152, 219)
    public class func appColorFlat_Peter_River() -> UIColor {
        return UIColor(red: 52.00/255.0,  green:152.00/255.0,  blue:219.00/255.0,  alpha:1.00)
    }
    
    //AMETHYST/purple
    //rgb(155, 89, 182)
    public class func appColorFlat_Amethyst() -> UIColor {
        return UIColor(red: 155.00/255.0,  green:89.00/255.0,  blue:182.00/255.0,  alpha:1.00)
    }
    
    //WET ASPHALT/grey
    //rgb(52, 73, 94)
    public class func appColorFlat_WetAsphalt() -> UIColor {
        return UIColor(red: 52.00/255.0,  green:73.00/255.0,  blue:94.00/255.0,  alpha:1.00)
    }
    //CARROT
    //rgb(230, 126, 34)
    public class func appColorFlat_Carrot() -> UIColor {
        return UIColor(red: 230.00/255.0,  green:126.00/255.0,  blue:34.00/255.0,  alpha:1.00)
    }
    
    
    //pink red
    //rgb(231, 76, 60)
    public class func appColorFlat_Alizarin_Red() -> UIColor {
        return UIColor(red: 231.00/255.0,  green:76.00/255.0,  blue:60.00/255.0,  alpha:1.00)
    }
    
    //concrete
    //rgb(127, 140, 141)
    public class func appColorFlat_Concrete() -> UIColor {
        return UIColor(red: 127.00/255.0,  green:140.00/255.0,  blue:141.00/255.0,  alpha:1.00)
    }
    
    //orange
    //rgb(243, 156, 18)
    public class func appColorFlat_Orange() -> UIColor {
        return UIColor(red: 243.00/255.0,  green:156.00/255.0,  blue:18.00/255.0,  alpha:1.00)
    }
    //rgb(241, 196, 15)
    public class func appColorFlat_Sunflower() -> UIColor {
        return UIColor(red: 241.00/255.0,  green:196.00/255.0,  blue:15.00/255.0,  alpha:1.00)
    }
    //pomegranate
    //rgb(192, 57, 43)
    public class func appColorFlat_Pomegranate() -> UIColor {
        return UIColor(red: 192.00/255.0,  green:57.00/255.0,  blue:43.00/255.0,  alpha:1.00)
    }
    
    
    //http://www.flatuicolorpicker.com/
//    MEDIUM PURPLE
//    BF55EC
//    191, 85, 236
    public class func appColorFlat_Medium_Purple() -> UIColor {
        return UIColor(red: 191.00/255.0,  green:85.00/255.0,  blue:236.00/255.0,  alpha:1.00)
    }
//    PICTON BLUE
//    22A7F0
//    34, 167, 240
    public class func appColorFlat_Picton_Blue() -> UIColor {
        return UIColor(red: 34.00/255.0,  green:167.00/255.0,  blue:240.00/255.0,  alpha:1.00)
    }
    
//    GOSSIP
//    87D37C
//    135, 211, 124
    public class func appColorFlat_Gossip() -> UIColor {
        return UIColor(red: 135.00/255.0,  green:211.00/255.0,  blue:124.00/255.0,  alpha:1.00)
    }
    
//    BUTTERCUP
//    F39C12
//    243, 156, 18
    public class func appColorFlat_Buttercup() -> UIColor {
        return UIColor(red: 243.00/255.0,  green:156.00/255.0,  blue:18.00/255.0,  alpha:1.00)
    }
    
//    SUNSET ORANGE
//    F64747
//    246, 71, 71
    
    public class func appColorFlat_SunsetOrange() -> UIColor {
        return UIColor.rgb(246, green:71, blue:71)
    }

    public class func appColorFlat_YellowOrange_California() -> UIColor {
        return UIColor.rgb(248, green:148, blue:6)
    }
    
    
    public class func rgb(_ red:Int, green:Int, blue:Int) -> UIColor {
        return UIColor(red: CGFloat(red)/255.0,  green:CGFloat(green)/255.0,  blue:CGFloat(blue)/255.0,  alpha:1.00)
    }
    
    //--------------------------------------------------------------
    // MARK: - GoogleMapUtils
    // MARK: -
    //--------------------------------------------------------------

    public class func appColorMapLabelStroke() -> UIColor {
        return UIColor.rgb(5, green:24, blue:76)
        
    }
    public class func appColorMapLabelFill() -> UIColor {
        return UIColor.rgb(240, green:237, blue:239)
        
    }
    //--------------------------------------------------------------

}
