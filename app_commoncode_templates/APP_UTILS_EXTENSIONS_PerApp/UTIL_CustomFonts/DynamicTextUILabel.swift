//
//  DynamicTextUILabel.swift
//  test_customfontrobottogooglemaps
//
//  Created by Brian Clear on 23/02/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit
open class DynamicTextUILabel : UILabel{
    
    let log = MyXCodeEmojiLogger.defaultInstance
    
    // MARK: - not used but work
    //
//    @IBInspectable public var animationType: String?
//    @IBInspectable public var autoRun: Bool = true
//    @IBInspectable public var duration: Double = Double.NaN
    
    
    // MARK: - Lifecycle
    open override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        configInspectableProperties()
    }
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        configInspectableProperties()
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        self.log.debug("DynamicTextUILabel layoutSubviews START - apply custom font to UILabel subview ------------------------")
        //works but I built extention to UILabel instead
//        self.applyCustomFontForCurrentTextStyle()
        
        configAfterLayoutSubviews()
        
        self.log.debug("DynamicTextUILabel layoutSubviews END------------------------")
    }
    
    // MARK: - Private
    fileprivate func configInspectableProperties() {
//        configAnimatableProperties()
//        configLeftImage()
    }
    
    fileprivate func configAfterLayoutSubviews() {
//        configBorder()
    }
}
