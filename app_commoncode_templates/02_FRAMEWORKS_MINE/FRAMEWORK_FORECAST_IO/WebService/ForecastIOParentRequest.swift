//
//  ForecastIOParentRequest.swift
//  joyride
//
//  Created by Brian Clear on 13/04/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation


class ForecastIOParentRequest : ParentWSRequest{
    
    //https://api.forecast.io/forecast/fe29b3a4003dc1b0c33828fc8732244c/51.5080776,-0.0717707
    let webServiceEndpoint_ForecastIO:String = "https://api.forecast.io/forecast/fe29b3a4003dc1b0c33828fc8732244c"
    
    
    //should be set by subclass
    
    override init(){
        super.init()
        
    }
    
    override var urlString :String? {
        get {
            var urlString_ :String?
            
            if let query = query{
                
                urlString_ = "\(self.webServiceEndpoint_ForecastIO)\(query)"
                // TODO: - cleanup
               let _ = parametersAppendAndCheckRequiredFieldsAreSet()
                
            }else{
                self.log.error("query is nil - should be set by subclass")
            }
            
            return urlString_
        }
    }
    
    override func parametersAppendAndCheckRequiredFieldsAreSet() -> Bool{
        var requiredFieldsAreSet = false
        
        requiredFieldsAreSet = super.parametersAppendAndCheckRequiredFieldsAreSet()
        if requiredFieldsAreSet{
            
            //DEBUG - comment out "key" to trigger "REQUEST_DENIED"
            //status = "REQUEST_DENIED";
            
            //ADD THE GOOGLE API KEYS - stored in Delegate
            //Two key WEB and IOS - I use WEB
            //parameters["key"] = appDelegate.googleMapsApiKey_Web
            //parameters["key"] = appDelegate.googleMapsApiKey_iOS
            //press GET A KEY on API docs created server key not ios key
            //SWIFT 3 parameters["key"] = appDelegate.googleMapsApiKey_Serverkey
            //parameters["key"] = appDelegate.googleMapsApiKey_Serverkey as AnyObject
//            parameters["key"] = appDelegate.googleMapsApiKey as AnyObject
            
            
            requiredFieldsAreSet = true
        }else{
            //required param missing in super class
        }
        
        return requiredFieldsAreSet
    }
}
