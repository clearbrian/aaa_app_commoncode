//
//  ForecastIOResponseDailyData.swift
//  joyride
//
//  Created by Brian Clear on 13/04/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation


class ForecastIOResponseDailyData: ParentMappable {
    
    var time: NSNumber? // 1460502000,
    var summary: NSNumber? // "Light rain starting in the evening.",
    var icon: NSNumber? // "rain",
    var sunriseTime: NSNumber? // 1460524152,
    var sunsetTime: NSNumber? // 1460573664,
    var moonPhase: NSNumber? // 0.22,
    var precipIntensity: NSNumber? // 0.0034,
    var precipIntensityMax: NSNumber? // 0.0184,
    var precipIntensityMaxTime: NSNumber? // 1460581200,
    var precipProbability: NSNumber? // 0.59,
    var precipType: NSNumber? // "rain",
    var temperatureMin: NSNumber? // 39.68,
    var temperatureMinTime: NSNumber? // 1460520000,
    var temperatureMax: NSNumber? // 62.43,
    var temperatureMaxTime: NSNumber? // 1460559600,
    var apparentTemperatureMin: NSNumber? // 37.97,
    var apparentTemperatureMinTime: NSNumber? // 1460527200,
    var apparentTemperatureMax: NSNumber? // 62.43,
    var apparentTemperatureMaxTime: NSNumber? // 1460559600,
    var dewPoint: NSNumber? // 37.98,
    var humidity: NSNumber? // 0.63,
    var windSpeed: NSNumber? // 3.48,
    var windBearing: NSNumber? // 238,
    var visibility: NSNumber? // 10,
    var cloudCover: NSNumber? // 0.34,
    var pressure: NSNumber? // 1007.99,
    var ozone: NSNumber? // 391.76
    
    
    
    //---------------------------------------------------------------------
    // root calls mapping()
    //    required init?(_ map: Map){
    //        mapping(map: map)
    //    }
    //---------------------------------------------------------------------
    
    //https://github.com/Hearst-DD/ObjectMapper
    override func mapping(map: Map){
        
        time <- map["time"]
        summary <- map["summary"]
        icon <- map["icon"]
        sunriseTime <- map["sunriseTime"]
        sunsetTime <- map["sunsetTime"]
        moonPhase <- map["moonPhase"]
        precipIntensity <- map["precipIntensity"]
        precipIntensityMax <- map["precipIntensityMax"]
        precipIntensityMaxTime <- map["precipIntensityMaxTime"]
        precipProbability <- map["precipProbability"]
        precipType <- map["precipType"]
        temperatureMin <- map["temperatureMin"]
        temperatureMinTime <- map["temperatureMinTime"]
        temperatureMax <- map["temperatureMax"]
        temperatureMaxTime <- map["temperatureMaxTime"]
        apparentTemperatureMin <- map["apparentTemperatureMin"]
        apparentTemperatureMinTime <- map["apparentTemperatureMinTime"]
        apparentTemperatureMax <- map["apparentTemperatureMax"]
        apparentTemperatureMaxTime <- map["apparentTemperatureMaxTime"]
        dewPoint <- map["dewPoint"]
        humidity <- map["humidity"]
        windSpeed <- map["windSpeed"]
        windBearing <- map["windBearing"]
        visibility <- map["visibility"]
        cloudCover <- map["cloudCover"]
        pressure <- map["pressure"]
        ozone <- map["ozone"]
    }
    
}

//{
//    time: 1460502000,
//    summary: "Light rain starting in the evening.",
//    icon: "rain",
//    sunriseTime: 1460524152,
//    sunsetTime: 1460573664,
//    moonPhase: 0.22,
//    precipIntensity: 0.0034,
//    precipIntensityMax: 0.0184,
//    precipIntensityMaxTime: 1460581200,
//    precipProbability: 0.59,
//    precipType: "rain",
//    temperatureMin: 39.68,
//    temperatureMinTime: 1460520000,
//    temperatureMax: 62.43,
//    temperatureMaxTime: 1460559600,
//    apparentTemperatureMin: 37.97,
//    apparentTemperatureMinTime: 1460527200,
//    apparentTemperatureMax: 62.43,
//    apparentTemperatureMaxTime: 1460559600,
//    dewPoint: 37.98,
//    humidity: 0.63,
//    windSpeed: 3.48,
//    windBearing: 238,
//    visibility: 10,
//    cloudCover: 0.34,
//    pressure: 1007.99,
//    ozone: 391.76
//},
