//
//  LyftKit.swift
//  joyride
//
//  Created by Brian Clear on 16/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation
// TODO: - Remove
//remove
//
//class LyftKit : ParentSwiftObject{
//    
//    /*
//     =======================================================================================================
//     =======================================================================================================
//
//     https://developer.lyft.com/docs/deeplinking
//     
//     https://docs.google.com/document/d/1Gn3Fo5-jWkGZumJLlEgK1nAizxCCMm5T0X4qTF7yeLE/edit
//     
//     Client secret
//     dj5FKag_VcaHX-cUfWuU7JmGEgS3wZEc
//     clientID
//     gadHhTlp1TO7
//
//     =======================================================================================================
//     =======================================================================================================
//     SUPPORTED DEEPLINKS
//     Link to the Lyft app map screen
//     The following links will open the app and place the user on the map screen.
//     
//     lyft://
//     
//     Setting the ride type
//     lyft://ridetype?id=<ride_type>
//     lyft://ridetype?id=<ride_type>&partner=<client_id>
//     Any ride type returned by the /ridetypes API may be used as the ride type parameter in a deeplink.
//     
//     Ride Type	Description
//     lyft	Standard Lyft
//     lyft_line	Lyft Line
//     lyft_plus	Lyft Plus
//     Specifying a pickup and destination location for Lyft rides
//     You can choose to specify the pickup and/or destination location for Lyft rides. If no pickup location is specified the user’s current location will be assumed.
//     //---------------------------------------------------------------------
//     lyft://ridetype?id=lyft&pickup[latitude]=37.764728&pickup[longitude]=-122.422999&destination[latitude]=37.7763592&destination[longitude]=-122.4242038
//     //---------------------------------------------------------------------
//     lyft://ridetype?id=lyft
//     &pickup[latitude]=37.764728
//     &pickup[longitude]=-122.422999
//     &destination[latitude]=37.7763592
//     &destination[longitude]=-122.4242038
//     
//     //---------------------------------------------------------------------
//     Adding a promo code
//     Deeplinks can be used to add a promo code to a user's account. This link will land the user on the payments screen with the promo code my_custom_promo_code pre-populated for the user and ready to be submitted.
//     
//     lyft://payment?credits=<my_custom_promo_code>
//     
//     Contact partnerships@lyft.com to get access to promo codes to distribute to your users.
//     =======================================================================================================
//     =======================================================================================================
//     */
//    //---------------------------------------------------------------------
//    //can be passed in Authorization header but never got it to work
//    let kLYFT_CLIENT_ID:String = "gadHhTlp1TO7"
//    
//    //USE IN API
//    //let kLYFT_CLIENT_ID_ENCODED:String = "dj5FKag_VcaHX-cUfWuU7JmGEgS3wZEc"
//    //---------------------------------------------------------------------
//    
//    static let openAppConfig = OpenAppConfigDeepLinking(openWithType : OpenWithType.OpenWithType_Lyft,
//                                              appSchemeString     : "lyft://",                   //make sure you add this to Info.plist LSApplicationQueriesSchemes
//                                              appDeepLinkURLString: "",                          //required if OpenAndDeepLink
//                                              appWebsiteURLString : "http://lyft.com",
//                                              appStoreIdUInt      : 529379082) // 1111503668,//,
//    
//    
//    
//    //--------------------------------------------------------------
//    // MARK: -
//    // MARK: - Deep Linking
//    //--------------------------------------------------------------
//    
//    /*
//     //---------------------------------------------------------------------
//     lyft://ridetype?id=lyft&pickup[latitude]=37.764728&pickup[longitude]=-122.422999&destination[latitude]=37.7763592&destination[longitude]=-122.4242038
//     //---------------------------------------------------------------------
//     lyft://ridetype?id=lyft
//     &pickup[latitude]=37.764728&pickup[longitude]=-122.422999
//     &destination[latitude]=37.7763592&destination[longitude]=-122.4242038
//     //---------------------------------------------------------------------
//     */
//    
//    func deepLinkingURLString(lyftPlacePickup lyftPlacePickup:LyftPlace, lyftPlaceDestination:LyftPlace) -> String{
//        
//        //Uber is installed - open the app
//        var urlString = ""
//        //================================================================================================
//        //lyftapp://ridetype?id=lyft
//        urlString = urlString + "\(LyftKit.openAppConfig.appSchemeString)ridetype?id=lyft"
//        
//        //---------------------------------------------------------------------
//        //&pickup[latitude]=37.764728&pickup[longitude]=-122.422999
//        //&destination[latitude]=37.7763592&destination[longitude]=-122.4242038
//        //---------------------------------------------------------------------
//        urlString = urlString + lyftPlacePickup.paramsForLyftPlace
//        urlString = urlString + lyftPlaceDestination.paramsForLyftPlace
//        //---------------------------------------------------------------------
//        //partner=<client_id>
//        urlString = urlString + "&partner=\(kLYFT_CLIENT_ID)"
//        //================================================================================================
//
//        //---------------------------------------------------------------------
//        //DEBUG
//        //---------------------------------------------------------------------
//        self.log.debug("urlString:\(urlString)")
//        //DEBUG
//        let lines = urlString.componentsSeparatedByString("&")
//        for line in lines{
//            self.log.debug("\(line)")
//        }
//        
//        return urlString
//    }
//    
//    func openTripInLyft(clkPlaceStart clkPlaceStart: CLKPlace?,
//                                      clkPlaceEnd: CLKPlace?,
//                                      presentingViewController: UIViewController)
//    {
//        var lyftPlacePickup : LyftPlace?
//        var lyftPlaceDestination : LyftPlace?
//        
//        //------------------------------------------------------------------------------------------------
//        //START
//        //------------------------------------------------------------------------------------------------
//        if let clkPlaceStart = clkPlaceStart{
//            lyftPlacePickup = LyftPlace.convert_CLKPlace_To_LyftPlace(clkPlaceStart, lyftPlaceType : .Pickup)
//            
//        }else{
//            self.log.error("clkPlaceStart is nil")
//        }
//        //------------------------------------------------------------------------------------------------
//        //DESTINATION
//        //------------------------------------------------------------------------------------------------
//        
//        if let clkPlaceEnd = clkPlaceEnd{
//            lyftPlaceDestination = LyftPlace.convert_CLKPlace_To_LyftPlace(clkPlaceEnd, lyftPlaceType : .Destination)
//        }else{
//            self.log.error("self.clkPlaceEnd is nil")
//        }
//        
//        //------------------------------------------------------------------------------------------------
//        //OPEN in Lyft
//        //------------------------------------------------------------------------------------------------
//
//        if let lyftPlacePickup_ = lyftPlacePickup{
//            if let lyftPlaceDestination_ = lyftPlaceDestination{
//                //------------------------------------------------------------------------------------------------
//                self.openTripInLyft(lyftPlacePickup: lyftPlacePickup_,
//                               lyftPlaceDestination: lyftPlaceDestination_,
//                           presentingViewController: presentingViewController)
//                //------------------------------------------------------------------------------------------------
//                
//            }else{
//                self.log.error("lyftPlaceDestination is nil")
//            }
//        }else{
//            self.log.error("lyftPlacePickup is nil 3331")
//        }
//    }
//
//    func openTripInLyft(lyftPlacePickup lyftPlacePickup:LyftPlace,
//                                        lyftPlaceDestination:LyftPlace,
//                                        presentingViewController: UIViewController)
//    {
//        
//        //-----------------------------------------------------------------------------------
//        //  <a href="https://geo.itunes.apple.com/us/app/lyft-taxi-app-alternative/id529379082?mt=8">Lyft - Taxi App Alternative - Lyft, Inc.</a>
//        //-----------------------------------------------------------------------------------
//        let appDeepLinkURLString = self.deepLinkingURLString(lyftPlacePickup: lyftPlacePickup, lyftPlaceDestination: lyftPlaceDestination)
//        
//        if appDeepLinkURLString == ""{
//            self.log.error("error: appDeepLinkURLString is ''")
//            
//        }else{
//            
//            //--------------------------------------------------------------------------
//            //Deep Link url not blank
//            //--------------------------------------------------------------------------
//
//            LyftKit.openAppConfig.appDeepLinkURLString = appDeepLinkURLString
//            
//            appDelegate.openAppURLKit.openApp(openAppConfig: LyftKit.openAppConfig,
//                                   presentingViewController: presentingViewController,
//                
//                success:{() -> Void in
//                    self.log.debug("openApp: success")
//                },
//                failure:{(errorMessage) -> Void in
//                    CLKAlertController.showAlertInVC(presentingViewController, title: "Error opening application", message: errorMessage)
//                    //should be ok but not used if app if not foudn in search then pointless opening itunes web page
//                    //appDelegate.openAppURLKit.openiTunesPageInSafari(openAppConfig: openAppConfig, presentingViewController: presentingViewController)
//
//                }
//            )
// 
//        }
//        //-----------------------------------------------------------------------------------
//    }
//
//}
