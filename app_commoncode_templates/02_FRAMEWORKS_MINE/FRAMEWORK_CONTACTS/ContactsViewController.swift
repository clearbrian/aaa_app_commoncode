//
//  ContactsViewController.swift
//  joyride
//
//  Created by Brian Clear on 14/10/2015.
//  Copyright © 2015 City Of London Consulting Limited. All rights reserved.
//

import UIKit
import AddressBookUI
import MapKit
import Contacts


//--------------------------------------------------------------
// MARK: -
// MARK: - PROTOCOL
//--------------------------------------------------------------
protocol ContactsViewControllerDelegate {
    func pickContactAddressReturned(_ contactsViewController: ContactsViewController, clkPlacesPickerResultPlace: CLKPlacesPickerResultPlace)
    func contactsViewControllerCancelled(_ contactsViewController: ContactsViewController)
}

//must be NSObject to handle ABPeoplePickerNavigationControllerDelegate
@available(iOS 9.0, *)
class ContactsViewController: ParentPickerViewController, ContactsFrameworkManagerDelegate {
    
    var contactsManager = ContactsFrameworkManager()
    
//    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var textViewFormattedAddress: UITextView!
    // TODO: - CLEANUP after test
//    var selectedCLKContactPlace : CLKContactPlace?
    
    var delegate: ContactsViewControllerDelegate?
    
    @IBOutlet weak var buttonFindContacts: UIButton!
    
    @IBOutlet weak var buttonCancel: UIButton!
    
    
    
    //--------------------------------------------------------------
    // MARK: - viewDidLoad
    // MARK: -
    //--------------------------------------------------------------

    override func viewDidLoad() {
        super.viewDidLoad()
         
         //self.view.backgroundColor = AppearanceManager.appColorLight0
        
        //styleButton(self.buttonFindContacts)
        //styleButton(self.buttonCancel)
        
         buttonFindContacts.layer.cornerRadius = 4.0
        
    }
    func styleButton(_ button:UIButton){
        
        button.backgroundColor = AppearanceManager.appColorNavbarAndTabBar
        button.setTitleColor(AppearanceManager.appColorTintInternal, for: UIControlState())
        
        button.layer.borderColor = UIColor.appColorCYAN().cgColor
        button.layer.borderWidth = 1.0
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonPickAddress_Action(_ sender: AnyObject) {
        
        //internal Contacts picker uses delegate so this is wrapper for it
        self.contactsManager.delegate = self
        self.contactsManager.pickContactAddress(presentingViewController:self)
    }
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - ContactsFrameworkManagerDelegate
    //--------------------------------------------------------------
    
    func pickContactAddressReturned(_ contactsFrameworkManager: ContactsFrameworkManager, contactPropertyAddress: CNContactProperty){
        
        let colcContact = COLCContact(contactPropertyAddress:contactPropertyAddress)
        
        let selectedCLKContactPlace = CLKContactPlace(colcContact: colcContact)
        
       // TODO: - forwardAndOrReverseGeocodePlace removed to get TaxiRank building for app store - reject as Contacts framework included and needed permission entry in plist
        appDelegate.log.error("TODO: - forwardAndOrReverseGeocodePlace removed to get TaxiRank building for app store - reject as Contacts framework included and needed permission entry in plist")
        //in parent
//        self.forwardAndOrReverseGeocodePlace(clkContactPlace: selectedCLKContactPlace)
    }
    
    
    //should subclass - as mention Facebook or calendar
    override func showErrorGettingAddress(){
        //SUBCLASSED THIS DONT MENTION FB IN PARENT:
        CLKAlertController.showAlertInVC(self, title: "Error getting exact address. Please check the event on Contacts", message: "Contact place address is empty")
    }
    
    override func showCollectionOnMap(clkPlace: CLKPlace){
        
        if let clkGeocodedAddressesCollection = clkPlace.clkGeocodedAddressesCollection {

            self.mapView.showbestOrFirstFromCollectionOnMap(clkGeocodedAddressesCollection: clkGeocodedAddressesCollection)


        }else{
            appDelegate.log.error("clkPlace.clkGeocodedAddressesCollection is nil")
        }
    
    }
    
    override func returnToDelegate(clkPlace: CLKPlace){
        if let delegate = self.delegate {
            
            //---------------------------------------------------------------------
            // TODO: - needed? just mone MAINNAME into CLKPLace
            let clkPlacesPickerResultPlace = CLKPlacesPickerResultPlace()
            clkPlacesPickerResultPlace.clkPlaceSelected = clkPlace

            delegate.pickContactAddressReturned(self, clkPlacesPickerResultPlace: clkPlacesPickerResultPlace)
            //---------------------------------------------------------------------
            
        }else{
            self.log.error("self.delegate is nil")
        }
        
    }
    
    //---------------------------------------------------------------------
    func pickContactAddressFailed(_ contactsFrameworkManager: ContactsFrameworkManager, errorMessage: String){
        CLKAlertController.showAlertInVC(self, title: "Error accessing Contacts", message: errorMessage)
    }
    
    func contactPickerDidCancel(_ contactsFrameworkManager: ContactsFrameworkManager){
        self.log.debug("contactPickerDidCancel")
        
    }
// TODO: - CLEANUP after test
//    func showForwardGeocodeError(errorMessage: String){
//        CLKAlertController.showAlertInVC(self, title: "Error", message: "Error finding exact location for that address. Please edit the address in Contacts" )
//    }
    


    @IBAction func buttonCancel_Action(_ sender: AnyObject) {
        //self.navigationController?.popViewControllerAnimated(true)
        
        self.dismiss(animated: true, completion: {
            
            //------------------------------------------------
            if let delegate_ = self.delegate {
                //containerView is hidden by the delegate
                delegate_.contactsViewControllerCancelled(self)
            }else{
                self.log.error("self.delegate is nil")
            }
            //------------------------------------------------
        })
        
    }
}
