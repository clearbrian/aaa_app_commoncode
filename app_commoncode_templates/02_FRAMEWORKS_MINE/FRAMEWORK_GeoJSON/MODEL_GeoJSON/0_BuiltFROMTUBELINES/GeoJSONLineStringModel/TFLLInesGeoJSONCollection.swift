//
//  TFLLInesGeoJSONCollection.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 27/01/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

enum TFLNetworkType: String{
    
    case unknown = "UNKNOWN"
    case crossrail = "Crossrail"
    case dlr = "DLR"
    case emirates_air_line = "Emirates Air Line"
    case overground = "Overground"
    case tramlink = "Tramlink"
    case tube = "Tube"
    
    static func type(for typeString: String) -> TFLNetworkType{
        switch typeString{
        
        case "Crossrail":
            return .crossrail
            
        case "DLR":
            return .dlr
            
        case "Emirates Air Line":
            return .emirates_air_line
            
        case "Overground":
            return .overground
            
        case "Tramlink":
            return .tramlink
            
        case "Tube":
            return .tube
            
        default:
            return .unknown
        }
    }

}

class TFLNetwork{
    
    var arrayLines = [TFLLine]()
    
    var tflNetworkType: TFLNetworkType = .unknown
    
}

enum TFLLineType: String{

    case unknown = "UNKNOWN_TFLLineType"
    case bakerloo = "Bakerloo"
    case central = "Central"
    case circle = "Circle"
    case crossrail = "Crossrail"
    case district = "District"
    case dlr = "DLR"
    case east_london = "East London"
    case emirates_air_line = "Emirates Air Line"
    case hammersmith_and_city = "Hammersmith & City"
    case jubilee = "Jubilee"
    case london_overground = "London Overground"
    case metropolitan = "Metropolitan"
    case northern = "Northern"
    case piccadilly = "Piccadilly"
    case tfl_rail = "TfL Rail"
    case tramlink = "Tramlink"
    case victoria = "Victoria"
    case waterloo_and_city = "Waterloo & City"
    
    static func tflLineType(for nameString:String) ->TFLLineType{
        switch nameString{
            
        case "Bakerloo":
            return .bakerloo
            
        case "Central":
            return .central
            
        case "Circle":
            return .circle
            
        case "Crossrail":
            return .crossrail
            
        case "District":
            return .district
            
        case  "DLR":
            return .dlr
            
        case "East London":
            return .east_london
            
        case "Emirates Air Line":
            return .emirates_air_line
            
        case "Hammersmith & City":
            return .hammersmith_and_city
            
        case "Jubilee":
            return .jubilee
            
        case "London Overground":
            return .london_overground
            
        case "Metropolitan":
            return .metropolitan
            
        case "Northern":
            return .northern
            
        case "Piccadilly":
            return .piccadilly
            
        case "TfL Rail":
            return .tfl_rail
            
        case "Tramlink":
            return .tramlink
            
        case "Victoria":
            return .victoria
            
        case "Waterloo & City":
            return .waterloo_and_city
        default:
            return .unknown
        }
    }
}
class TFLLine{
    
}



extension GeoJSONLineStringFeatureCollection{
    
    
//    var tflNetworksArray = [TFLNetwork]()
    
    
    /*
     {
     "type": "FeatureCollection",
     "features": [
     {
     "geometry": {
     "type": "LineString",
     "coordinates": [
     [
     -0.059409291146068,
     51.52391213822392
     ],
     ...
     [
     -0.057584287095381,
     51.532416916418214
     ]
     ]
     },
     "type": "Feature",
     "properties": {
     "lines": [
     {
     "opened": 2015,
     "end_sid": "910GBTHNLGR",
     "colour": "#EE7C0E",
     "network": "Overground",
     "start_sid": "910GCAMHTH",
     "last_wd": 0.0103,
     "name": "London Overground"
     }
     ],
     "id": "LeaValleyLine1"
     }
     },
     */

    func buildTFLNetwork(){
        
        if let features : [GeoJSONLineStringFeature] = self.features {
            //----------------------------------------------------------------------------------------
            //Each Feature is a sub line on a TFL network - eg Edgeware branch of Northern Line
            //----------------------------------------------------------------------------------------
            for geoJSONLineStringFeature in features{
                    
                var lineIdString_ = ""
                
                //----------------------------------------------------------------------------------------
                //Feature : properties
                //----------------------------------------------------------------------------------------
                if let geoJSONLineStringFeature: GeoJSONLineStringProperty = geoJSONLineStringFeature.properties {
                    
                    if let id = geoJSONLineStringFeature.id {
                        lineIdString_ = id
                    }else{
                        appDelegate.log.error("geoJSONPointProperty.name is nil")
                    }
                    
                    //------------------------------------------------------------------------------------------------
                    //
                    //    "properties": {
                    //    "lines": [
                    //    {
                    //    "opened": 2015,
                    //    "end_sid": "910GBTHNLGR",
                    //    "colour": "#EE7C0E",
                    //    "network": "Overground",
                    //    "start_sid": "910GCAMHTH",
                    //    "last_wd": 0.0103,
                    //    "name": "London Overground"
                    //    }
                    //    ],
                    //    "id": "LeaValleyLine1"
                    //    }
                    //-----------------------------------------------------------------------------------------------
                    var linesCountAll_ = 0
                    

                    if let lines = geoJSONLineStringFeature.lines {
                        linesCountAll_ = lines.count
                        
                        if geoJSONLineStringFeature.arrayActiveLines.count == 0{
                            
                            //Crossrail : AbbeyWoodSpur - 1 line entry but not active yet
                            //OK NOISY print("INFO: arrayActiveLines.count == 0 >>> LINE:id:\(lineIdString_) linesCountAll_:\(linesCountAll_) arrayActiveLines.count:\(geoJSONLineStringFeature.arrayActiveLines.count)")

                        }else if geoJSONLineStringFeature.arrayActiveLines.count == 1{
                            
                            //beware - BatterseaParkSpur cant find
                            //print("INFO: arrayActiveLines.count == 1 >>> LINE:id:\(lineIdString_) linesCountAll_:\(linesCountAll_) arrayActiveLines.count:\(geoJSONLineStringFeature.arrayActiveLines.count)")

                        }else if geoJSONLineStringFeature.arrayActiveLines.count > 1{
                            if geoJSONLineStringFeature.isSharedActiveBranch{
                                //line.name is not the same for all lines - so shared line
                                //print("NOTE: isSharedActiveBranch:TRUE >>> LINE:id:\(lineIdString_) linesCountAll_:\(linesCountAll_) arrayActiveLines.count:\(geoJSONLineStringFeature.arrayActiveLines.count)")
                            }else{
                                
                                //print("NOTE: isSharedActiveBranch:FALSE >>> LINE:id:\(lineIdString_) linesCountAll_:\(linesCountAll_) arrayActiveLines.count:\(geoJSONLineStringFeature.arrayActiveLines.count)")

                            }
                        }
                        else
                        {
                            print("ERROR: arrayActiveLines.count < 0 >>> LINE:id:\(lineIdString_) linesCountAll_:\(linesCountAll_) arrayActiveLines.count:\(geoJSONLineStringFeature.arrayActiveLines.count)")
                        }

                    }else{
                        appDelegate.log.error("geoJSONPointProperty.lines is nil")
                    }
                    //----------------------------------------------------------------------------------------
                   
                }else{
                    appDelegate.log.error("geoJSONPointFeature.properties is nil")
                }
                
//                //------------------------------------------------------------------------------------------------
//                //geometry
//                //------------------------------------------------------------------------------------------------
//                //"geometry": {
//                //    "type": "LineString",
//                //      "coordinates": [
//                //-------------------------------------------------------------------
//                //var arrayCLLocationCoordinate2D = [CLLocationCoordinate2D]()
//                let path = GMSMutablePath()
//                if let geometry = geoJSONLineStringFeature.geometry {
//                    
//                    if let coordinatesArrayOfArray:[[Float]] = geometry.coordinates{
//                        
//                        for coordinates:[Float] in coordinatesArrayOfArray{
//                            //------------------------------------------------------------------------------------------------
//                            if coordinates.count == 2{
//                                //ITS LNG/LAT
//                                let latitude = coordinates[1]
//                                let longitude = coordinates[0]
//                                //let clLocation = CLLocation( latitude: CLLocationDegrees(latitude),
//                                //                                longitude: CLLocationDegrees(longitude))
//                                
//                                
//                                
//                                let location = CLLocationCoordinate2D(latitude: CLLocationDegrees(latitude),
//                                                                      longitude: CLLocationDegrees(longitude))
//                                path.add(location)
//                                //location is ok
//                                //let marker = GMSMarker(position: clLocation.coordinate)
//                                
//                                //marker.title = "UNKNOWN"
//                                
//                                //   //------------------------------------------------------------------------------------------------
//                                //   //store the TFLTaxiRank with the marker
//                                //   marker.userData = geoJSONPointFeature
//                                //   //------------------------------------------------------------------------------------------------
//                                //   if let imagePin = UIImage.init(named: "Roundel") {
//                                //       marker.icon = imagePin
//                                //   }else{
//                                //       //appDelegate.log.error("UIImage.init(named: imageName:'\(imageName)') is nil")
//                                //       marker.icon = nil
//                                //   }
//                                //   marker.map = self.gmsMapView
//                                
//                                //print("\(lineIdString_),\(latitude),\(longitude)")
//                                
//                                //arrayCLLocationCoordinate2D.append(clLocation)
//                                //------------------------------------------------------------------------------------------------
//                            }else{
//                                appDelegate.log.error("coordinates.count == 2 FAILED:\(coordinates.count)")
//                            }
//                            //------------------------------------------------------------------------------------------------
//                            
//                        }
//                    }else{
//                        appDelegate.log.error("geometry.coordinates is nil")
//                    }
//                }else{
//                    appDelegate.log.error("geoJSONPointFeature.geometry  is nil")
//                }
                
                

                
//OK
//                    let gmsPolyline = GMSPolyline(path: path)
//                    gmsPolyline.strokeWidth = 3.0
//                    gmsPolyline.strokeColor = .purple
//                    gmsPolyline.map = self.gmsMapView
                
                //------------------------------------------------------------------------------------------------
                
            }//for
        }else{
            appDelegate.log.error("tflStationsPointCollection.features is nil")
        }

    }
    
    
}
