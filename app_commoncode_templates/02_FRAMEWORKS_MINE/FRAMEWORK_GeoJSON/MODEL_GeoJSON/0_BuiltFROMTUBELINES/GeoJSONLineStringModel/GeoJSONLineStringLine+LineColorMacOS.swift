//
//  GeoJSONLineStringLine+LineColorMacOS.swift
//  mac_app_applemaps_tfl
//
//  Created by Brian Clear on 11/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
import AppKit

extension GeoJSONLineStringLine{
    var colourSafe : NSColor{
        var colourSafe_  : NSColor = .black
        if let colour_ = self.colour {
            
            colourSafe_ = NSColor.hexStringToNSColor(colour_)
            
        }else{
            appDelegate.log.error("self.colour is nil - cant get line color")
        }
        return colourSafe_
    }
}
