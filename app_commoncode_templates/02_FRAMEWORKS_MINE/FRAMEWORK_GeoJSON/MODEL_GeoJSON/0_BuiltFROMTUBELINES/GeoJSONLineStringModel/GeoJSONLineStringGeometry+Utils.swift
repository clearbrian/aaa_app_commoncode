//
//	GeoJSONLineStringGeometry.swift
//
//	Create by Brian Clear on 26/1/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
//import ObjectMapper
import GoogleMaps
import CoreLocation


extension GeoJSONLineStringGeometry{
    
    var arrayCLLocationCoordinate2D: [CLLocationCoordinate2D]{
        
        var arrayCLLocationCoordinate2D = [CLLocationCoordinate2D]()
        
        //------------------------------------------------------------------------------------------------
        //geometry
        //------------------------------------------------------------------------------------------------
        //"geometry": {
        //    "type": "LineString",
        //      "coordinates": [
        //-------------------------------------------------------------------
        if let coordinatesArrayOfArray:[[Float]] = self.coordinates{
            
            for coordinates:[Float] in coordinatesArrayOfArray{
                
                //--------------------------------------------------
                if coordinates.count == 2{
                    //----------------------------------------------
                    //ITS LNG/LAT
                    let latitude = coordinates[1]
                    let longitude = coordinates[0]
                    //------------------------------------------------------------------------------------------------
                    let clLocationCoordinate2D = CLLocationCoordinate2D(latitude: CLLocationDegrees(latitude),
                                                                        longitude: CLLocationDegrees(longitude))
                    
                    arrayCLLocationCoordinate2D.append(clLocationCoordinate2D)
                    //------------------------------------------------------------------------------------------------
                }else{
                    appDelegate.log.error("coordinates.count == 2 FAILED:\(coordinates.count)")
                }
                //------------------------------------------------------------------------------------------------
            }
        }else{
            appDelegate.log.error("geometry.coordinates is nil")
        }
        return arrayCLLocationCoordinate2D
    }
    
}
