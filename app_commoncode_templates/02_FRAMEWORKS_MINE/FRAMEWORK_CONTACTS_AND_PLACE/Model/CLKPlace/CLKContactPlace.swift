//
//  CLKContactPlace.swift
//  joyride
//
//  Created by Brian Clear on 18/10/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
// TODO: - COMMENTED out as app store demanded I add plist permisssion message for Contacts framework
//import Contacts
import MapKit

class CLKContactPlace : CLKPlace{
    
    var colcContact : COLCContact
    
    init(colcContact: COLCContact) {
        
        self.colcContact = colcContact
        
        //set clLocation and name to nil d
        super.init(clLocation:nil, name: nil, clkPlaceWrapperType: .geocodedContact)
        
    }
    

    override var name :String? {
        get {
            //---------------------------------------------------------
            var nameReturned :String?
            // TODO: - changed by SET - eg Edit Name
            
            
//            //dont call super getter just check name_ to see if change by edit - if not then get name from contact
//            //if let _name = super.name {
//            if let _name = super._name {
//                    nameReturned = _name
//            }else{
//                //------------------------------------------------------------------------------------------------
//                //GET FROM INTERNAL TYPE
//                //------------------------------------------------------------------------------------------------
//                nameReturned = self.colcContact.name
//            }
            
            
            
            nameReturned = self.colcContact.name
            //------------------------------------------------------------------------------------------------
            return nameReturned
            //------------------------------------------------------------------------------------------------
        }
        set {
            appDelegate.log.error("TODO 56456 EDIT CHANGED NAME? oo just address")
            // TODO:                _name = newValue
        }
    }
    
    //the name to display - ususally just returns self.name but here in Contact can be 
    //PERSONAL CONTACT - Andi Case > dont display name
    //Business CONTACT - Andi Case/Clarksons > just display org name
    
    override var formatted_name :String? {
        get {
            //---------------------------------------------------------
            var formatted_nameReturned :String?
            //in base class just returns name
            //formatted_nameReturned = self.name
            
            //---------------------------------------------------------------------
            if let orgName = self.colcContact.orgName {
                //Business address
                formatted_nameReturned = orgName
            }else{
                //personal contact - may have name but dont show it the uber driver doesnt care
                formatted_nameReturned = nil
            }
            
            
            //------------------------------------------------------------------------------------------------
            return formatted_nameReturned
            //------------------------------------------------------------------------------------------------
        }
    }
    
    
    
    

    
//// TODO: - CLEANUP after test - one in CLKPlace gets the correct geocoded result
//    override var name_formatted_address :String {
//        get {
//            //---------------------------------------------------------
//            var name_formatted_addressReturned :String = ""
//            name_formatted_addressReturned = "todogeocodedContact/name_formatted_address"
//            
////            //set by Edit screen
////            if let _name_formatted_address = self._name_formatted_address {
////                name_formatted_addressReturned = _name_formatted_address
////            }else{
////                //                //------------------------------------------------
////                //                //local var not set by edit - formulate from other fields
////                //                //------------------------------------------------
////                //                //special case - only show name if business
////                //                if self.clkPlaceWrapperType == .geocodedContact{
////                //                    //---------------------------------------------------------------------
////                //                    //CONTACT
////                //                    //---------------------------------------------------------------------
////                //                    if let colcContact = self.colcContact {
////                //
////                //                        if "" != colcContact.name_for_contact {
////                //
////                //                            if let formatted_address = self.formatted_address {
////                //                                 name_formatted_addressReturned = "\(colcContact.name_for_contact), \(formatted_address)"
////                //
////                //                            }else{
////                //                                appDelegate.log.error("self.formatted_address is nil")
////                //                            }
////                //
////                //                        }else{
////                //                            //Contact Name and ORG both blank use first line of address
////                //                            //appDelegate.log.debug("colcContact.name_for_contact is ''")
////                //                            //use first line or addesss
////                //                            if let formatted_address = self.formatted_address {
////                //                                name_formatted_addressReturned = "\(formatted_address)"
////                //
////                //                            }else{
////                //                                appDelegate.log.error("self.formatted_address is nil")
////                //                            }
////                //                        }
////                //                    }else{
////                //                        appDelegate.log.error("self.EXPR is nil")
////                //                    }
////                //                    //---------------------------------------------------------------------
////                //                }
////                //                else{
////                //                    //---------------------------------------------------------------------
////                //                    //NON CONTACT
////                //                    //---------------------------------------------------------------------
////                if let name = self.name{
////                    if let formatted_address = self.formatted_address{
////                        
////                        if self.clkPlaceWrapperType == .geocodedCalendarEvent{
////                            name_formatted_addressReturned = "\(formatted_address)"
////                        }
////                        else  if self.clkPlaceWrapperType == .colcFacebookEvent{
////                            
////                            if let formatted_address = self.formatted_address{
////                                name_formatted_addressReturned = "\(name), \(formatted_address)"
////                            }else{
////                                //both nil
////                                name_formatted_addressReturned = "\(name)"
////                            }
////                        }else{
////                            //when you edit an address name is set to ""
////                            if name == ""{
////                                name_formatted_addressReturned = "\(formatted_address)"
////                            }else{
////                                name_formatted_addressReturned = "\(name), \(formatted_address)"
////                            }
////                            
////                        }
////                        
////                    }else{
////                        name_formatted_addressReturned = "\(name)"
////                    }
////                }else{
////                    if let formatted_address = self.formatted_address{
////                        name_formatted_addressReturned = "\(formatted_address)"
////                    }else{
////                        //both nil
////                        name_formatted_addressReturned = ""
////                    }
////                }
////                //                    //---------------------------------------------------------------------
////                //                }
////                //                //-----------------------------------------------------------------------------------
////            }
//            //------------------------------------------------------------------------------------------------
//            return name_formatted_addressReturned
//            //------------------------------------------------------------------------------------------------
//        }
//        set {
//            //store in private var
//            //_name_formatted_address = name_formatted_address
//            //_name_formatted_address = newValue
//            //EDIT SCREEN
//            // TODO: - EDIT contact name/address in edit screen
//            appDelegate.log.error("TODO: - EDIT contact name/address in edit screen")
//            super.name_formatted_address = newValue
//        }
//    }
    
    
    
    
    
    
    //------------------------------------------------------------------------------------------------
    //clLocation CLLocation
    //------------------------------------------------------------------------------------------------
    
    override var clLocation :CLLocation? {
        get {
            //---------------------------------------------------------
            var clLocationReturned :CLLocation?
            
            //---------------------------------------------------------------------
            //    appDelegate.log.error("FB clLocation self.googleGeocodedGMSAddress is nil - do i need to subclass this")
            //    clLocationReturned = nil
            //    if let googleGeocodedGMSAddress = self.googleGeocodedGMSAddress   {
            //        clLocationReturned = CLLocation.init(latitude: googleGeocodedGMSAddress.coordinate.latitude, longitude: googleGeocodedGMSAddress.coordinate.latitude)
            //        
            //    }else{
            //        appDelegate.log.error("[COLCFacebookEvent_GeoCodeLocation] self.googleGeocodedGMSAddress is nil")
            //    }
            //---------------------------------------------------------------------
            if let clkGeocodedAddressesCollection = self.clkGeocodedAddressesCollection {
                if let bestOrFirstCLKAddress: CLKAddress = clkGeocodedAddressesCollection.bestOrFirstCLKAddress {
                    if let clLocation = bestOrFirstCLKAddress.clLocation {
                        clLocationReturned = clLocation
                        
                    }else{
                        appDelegate.log.error("bestOrFirstCLKAddress.clLocation is nil")
                    }
                }else{
                    appDelegate.log.error("clkGeocodedAddressesCollection.bestOrFirstCLKAddress is nil")
                }
            }else{
                appDelegate.log.error("self.clkGeocodedAddressesCollection is nil")
            }
            //------------------------------------------------------------------------------------------------
            return clLocationReturned
            //------------------------------------------------------------------------------------------------
        }

    }
}
