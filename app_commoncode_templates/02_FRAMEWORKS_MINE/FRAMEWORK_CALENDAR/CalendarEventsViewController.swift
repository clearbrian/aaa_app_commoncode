//
//  CalendarEventsViewController.swift
//  joyride
//
//  Created by Brian Clear on 05/09/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//
import GoogleMaps
//http://stackoverflow.com/questions/24177204/swift-compiler-error-use-of-undeclared-type-ekeventstore
import EventKit
import EventKitUI

import UIKit

import MapKit

protocol CalendarEventsViewControllerDelegate {
    func controllerDidSelectCalendarEvent(_ calendarEventsViewController: CalendarEventsViewController, clkPlacesPickerResultPlace: CLKPlacesPickerResultPlace)
    func calendarEventsViewControllerCancelled(_ calendarEventsViewController: CalendarEventsViewController)
}


/*
 See my bitbucket improvement version of apple EKEvent sample
 /Users/gbxc/Documents/00_ME/00_BITBUCKET_COLC/app_test_ios_eventkit_calendar/SimpleEKDemo/Classes/DefaultEventsListAndEditViewController.m
 
 */
class CalendarEventsViewController: ParentPickerViewController, UITableViewDataSource, UITableViewDelegate, EventKitFrameworkManagerDelegate, RangeDatePickerControllerDelegate, EKCalendarChooserDelegate
{
    
    var eventKitFrameworkManager = EventKitFrameworkManager()
    var delegate: CalendarEventsViewControllerDelegate?
    
    
    @IBOutlet weak var tableView: UITableView!

    @IBOutlet weak var buttonRefresh: UIButton!
    @IBOutlet weak var buttonDates: UIButton!
    @IBOutlet weak var buttonCalendar: UIButton!

    @IBOutlet weak var labelDates: UILabel!
    

    //--------------------------------------------------------------
    // MARK: - UI
    // MARK: -
    //--------------------------------------------------------------

    func preferredContentSizeChanged(_ notification: Notification){
        self.tableView.invalidateIntrinsicContentSize()
        self.tableView.reloadData()
        
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //------------------------------------------------------------------------------------------------
        //to call applicationDidBecomeActive_refreshTableView
        //preferredContentSizeChanged only works sometimes
        appDelegate.topParentViewController = self

        //------------------------------------------------------------------------------------------------
        //DYNAMIC TEXT and TABLE ROW HEIGHT
        //------------------------------------------------------------------------------------------------
        ////can done in DataSource methods
        //self.tableView.estimatedRowHeight = 170.0
        ////also done in heightForRowAtIndexPath
        //self.tableView.rowHeight = UITableViewAutomaticDimension;
        
        //also done in heightForRowAtIndexPath
        //self.tableView.rowHeight = 130.0
        
        //        self.tableView.sectionHeaderHeight = UITableViewAutomaticDimension;
        //        self.tableView.estimatedSectionHeaderHeight = 80.0f;
        
        //------------------------------------------------------------------------------------
        self.buttonRefresh?.layer.cornerRadius = 4.0
        self.buttonDates?.layer.cornerRadius = 4.0
        self.buttonCalendar?.layer.cornerRadius = 4.0
        
        //self.mapView?.layer.cornerRadius = 4.0
        self.mapView?.layer.borderColor = AppearanceManager.appColorDark1.cgColor
        self.mapView?.layer.borderWidth = 2.0
        //---------------------------------------------------------------------
        updateDatesLabel()
        //------------------------------------------------------------------------------------
        //asks permission first time
        refreshEvents()
        
        
        
        
    }
    
    //viewDidLoad and Refresh button
    func refreshEvents(){
        //internal Calendars picker uses delegate so this is wrapper for it
        self.eventKitFrameworkManager.delegate = self
        self.eventKitFrameworkManager.getEvents(presentingViewController:self)
    }
    
    //dynamic type - if changed in iOS setting need to reload the table
    override func applicationDidBecomeActive_refreshTableView(){
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - TABLEVIEW
    //--------------------------------------------------------------
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat{
        return 170.0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return UITableViewAutomaticDimension
        //        return 170.0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return arrayCOLCCalendarEvent.count
        return self.eventKitFrameworkManager.eventsList.count
    }
    
    //--------------------------------------------------------------
    // MARK: - TABLEVIEW: CELL FOR ROW
    //--------------------------------------------------------------

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cellIdentifier = "COLCCalendarEventTableViewCell"
        
        let colcCalendarEventTableViewCell : COLCCalendarEventTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! COLCCalendarEventTableViewCell
        
        colcCalendarEventTableViewCell.applyCustomFont()
        //------------------------------------------------
        let ekEvent : EKEvent = self.eventKitFrameworkManager.eventsList[(indexPath as NSIndexPath).row]
        //------------------------------------------------
        //        colcCalendarEventTableViewCell.labelId?.text = "..."
        //        if let id = ekEvent.title as? String{
        //            colcCalendarEventTableViewCell.labelId?.text = id
        //            
        //        }else{
        //            self.log.error("colcCalendarEvent.id as? String is nil")
        //        }
        //------------------------------------------------
        colcCalendarEventTableViewCell.labelName?.text = "..."
        colcCalendarEventTableViewCell.labelName?.text = ekEvent.title

        //------------------------------------------------
        colcCalendarEventTableViewCell.labelPlace?.text = "..."
        
        
        //------------------------------------------------------------------------------------------------
        /*
        EKEvent <0x14d11bf50>
            {
                EKEvent <0x14d11bf50>
                    {	 title = 		Jridebc;
                        location = 	Brian's Home
                        15 Royal Mint Street
                        London
                        E1 8LG
                        United Kingdom;
                        calendar = 	EKCalendar <0x14cdece10> {title = Work; type = CalDAV; allowsModify = YES; color = #CC73E1;};
                        alarms = 		(null);
                        URL = 			(null);
                        lastModified = 2016-11-09 15:18:10 +0000;
                        startTimeZone = 	Europe/London (GMT) offset 0;
                        startTimeZone = 	Europe/London (GMT) offset 0
                };
                location = 	Brian's Home
                15 Royal Mint Street
                London
                E1 8LG
                United Kingdom;
                structuredLocation = 	EKStructuredLocation <0x14d4408b0> {title = Brian's Home; address = 15 Royal Mint Street
                    London
                    E1 8LG
                    United Kingdom; geo = <+51.51012600,-0.07133700> +/- 0.00m (speed -1.00 mps / course -1.00) @ 09/11/2016, 18:22:49 Greenwich Mean Time; abID = ab://Brian's%20Home; routing = (null); radius = 0.000000;};
                    startDate = 	2016-11-09 15:00:00 +0000; 
                    endDate = 		2016-11-09 16:00:00 +0000; 
                    allDay = 		0; 
                    floating = 	0; 
                    recurrence = 	(null); 
                    attendees = 	(null); 
                    travelTime = 	(null); 
                    startLocation = 	(null);
                };
         //------------------------------------------------------------------------------------------------

        */
        
        let noLocationString = "No location set for this event"
        
        //if structuredLocation set then location is set to  structuredLocation.title
        if let location = ekEvent.location{
            if location == ""{
                colcCalendarEventTableViewCell.labelPlace?.text = noLocationString
                colcCalendarEventTableViewCell.labelPlace?.textColor = AppearanceManager.appColorLight_ErrorColor
                
                
            }else{
                colcCalendarEventTableViewCell.labelPlace?.text = location
                colcCalendarEventTableViewCell.labelPlace?.textColor = AppearanceManager.appColorDark1
            }
            
        }else{
            self.log.error("colcCalendarEvent.place is nil")
            colcCalendarEventTableViewCell.labelPlace?.text = noLocationString
            colcCalendarEventTableViewCell.labelPlace?.textColor = AppearanceManager.appColorLight_ErrorColor
        }
        
        //------------------------------------------------
        
        
        //------------------------------------------------
        //"2016-05-03T00:00:00"
        //if event is TODAY it may be in the past if its at 9am and time is 4pm
        //needs to be non optional
        var dateToCompare = Date() //NOW - so anytime during the day
        if let todayAtMidnight = RangeDatePickerController.dateAtMidnight(Date()){
            dateToCompare = todayAtMidnight
        }
        
        if let dateString = dateStringInDisplayFormat(ekEvent.startDate) as? String {

            if ekEvent.startDate.compare(dateToCompare) == ComparisonResult.orderedDescending {
                colcCalendarEventTableViewCell.labelStartTime?.text = "Start: \(dateString) [Upcoming]"
                colcCalendarEventTableViewCell.contentView.backgroundColor = UIColor.white
            }else{
                colcCalendarEventTableViewCell.labelStartTime?.text = "Start: \(dateString) [Past]"
                colcCalendarEventTableViewCell.contentView.backgroundColor = AppearanceManager.appColorLight_DeSelectedColor
            }
        
        }else{
            colcCalendarEventTableViewCell.labelStartTime?.text = ""
        }
        
        //------------------------------------------------
        if let dateString =  dateStringInDisplayFormat(ekEvent.endDate) as? String {
            
            if ekEvent.endDate.compare(dateToCompare) == ComparisonResult.orderedDescending {
                colcCalendarEventTableViewCell.labelEndTime?.text = "End: \(dateString) [Upcoming]"
                colcCalendarEventTableViewCell.contentView.backgroundColor = UIColor.white
            }else{
                colcCalendarEventTableViewCell.labelEndTime?.text = "End: \(dateString) [Past]"
                colcCalendarEventTableViewCell.contentView.backgroundColor = AppearanceManager.appColorLight_DeSelectedColor
            }
            
        }else{
            colcCalendarEventTableViewCell.labelStartTime?.text = ""
        }
        //------------------------------------------------
        colcCalendarEventTableViewCell.labelDescription?.text = "Calendar: [\(ekEvent.calendar.source.title)] \(ekEvent.calendar.title)"
        
        return colcCalendarEventTableViewCell
    }
    
    //--------------------------------------------------------------
    // MARK: - TABLEVIEW: didSelectRowAt
    //--------------------------------------------------------------
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let ekEvent : EKEvent = self.eventKitFrameworkManager.eventsList[(indexPath as NSIndexPath).row]
        self.log.info("\(ekEvent)")

        self.didSelectEKEvent(ekEvent)
    }
    
    //--------------------------------------------------------------
    // MARK: - didSelectEKEvent
    // MARK: -
    //--------------------------------------------------------------
    func didSelectEKEvent(_ ekEvent: EKEvent){
        
        
        //iOS9*
        //structuredLocation: EKStructuredLocation?
        self.log.info("\(ekEvent.structuredLocation)")
        //---------------------------------------------------------------------
        
        
        let clkCalendarEKEventPlace = CLKCalendarEKEventPlace(ekEvent:ekEvent)
        // EKStructuredLocation = ekEvent.structuredLocation ay be set but address can be minimal and we need to convert it to a CLKAddress wuicker to reversegeocode it
        geocodeCLKPlace(clkPlace:clkCalendarEKEventPlace)
    }
    
    
    //should subclass - as mention Facebook or calendar
    override func showErrorGettingAddress(){
        //SUBCLASSED THIS DONT MENTION FB IN PARENT:
        CLKAlertController.showAlertInVC(self, title: "Error getting exact address. Please check the event on Calendar", message: "Calendar place address is empty")
    }
    
    override func showCollectionOnMap(clkPlace: CLKPlace){
        
        if let clkGeocodedAddressesCollection = clkPlace.clkGeocodedAddressesCollection {
// TODO: - change this to take clkPlace
            self.mapView.showbestOrFirstFromCollectionOnMap(clkGeocodedAddressesCollection: clkGeocodedAddressesCollection)
            
        }else{
            appDelegate.log.error("clkPlace.clkGeocodedAddressesCollection is nil")
        }
    }
    
    override func returnToDelegate(clkPlace: CLKPlace){
        if let delegate = self.delegate{
            
            //------------------------------------------------
            let clkPlacesPickerResultPlace = CLKPlacesPickerResultPlace()
            clkPlacesPickerResultPlace.clkPlaceSelected = clkPlace
            
            delegate.controllerDidSelectCalendarEvent(self, clkPlacesPickerResultPlace: clkPlacesPickerResultPlace)
            //------------------------------------------------
            
        }else{
            self.log.error("delegate is nil")
        }
    }



    //--------------------------------------------------------------
    // MARK: - EVENT DATE
    // MARK: -
    //--------------------------------------------------------------
// TODO: - move to utility
    func dateStringInDisplayFormat(_ dateIN: Date) -> NSString?{
        
        var dateStringReturned : NSString? = nil
        
        let dateFormatterIN = DateFormatter()
        
        //"Mon 11 Feb 2016"
        dateFormatterIN.dateFormat = "E dd MMM yyyy HH:mm"
        
        // TODO: - Fixtures do we need to support timeZone
        //dateFormatterIN.timeZone = self.userTimeZone()
        dateStringReturned = dateFormatterIN.string(from: dateIN) as NSString?
        
        return dateStringReturned
        
    }
    
    @IBAction func buttonCancel_Action(_ sender: AnyObject) {
        
        //self.navigationController?.popViewControllerAnimated(true)
        
        self.dismiss(animated: true, completion: {
            
            //------------------------------------------------
            if let delegate_ = self.delegate {
                //containerView is hidden by the delegate
                delegate_.calendarEventsViewControllerCancelled(self)
            }else{
                self.log.error("self.delegate is nil")
            }
            //------------------------------------------------
        })
    }
    
    
    func updateDatesLabel(){
        self.labelDates.text = self.dateRangeAsString()
    }
    
    //--------------------------------------------------------------
    // MARK: - EventKitFrameworkManagerDelegate
    // MARK: -
    //--------------------------------------------------------------
    func getEventsReturned(_ eventKitFrameworkManager: EventKitFrameworkManager){
        
        //self.labelDates.text = self.dateRangeAsString()
        updateDatesLabel()
        self.tableView.reloadData()
        
        if self.eventKitFrameworkManager.eventsList.count == 0{
        
            CLKAlertController.showAlertInVC(self, title: "No events found", message: "No events were found in this Calendar between\r[\(self.dateRangeAsString())]. Try changing calendars or date range")
        }
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func getEventsReturnedWithError(_ eventKitFrameworkManager: EventKitFrameworkManager, failedWithErrorMessage: String){
        //instructions to get to Settings are in iOS errors
        
        updateDatesLabel()
        
        //"Access to Calendars is denied. The user has previously denied access.\rYou may change it in Settings > Privacy > Calendars"
        CLKAlertController.showAlertInVC(self, title: "Error accessing Calendar", message: "\(failedWithErrorMessage)")
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    //--------------------------------------------------------------
    // MARK: - BUTTON REFRESH
    // MARK: -
    //--------------------------------------------------------------

    @IBAction func buttonRefresh_Action(_ sender: AnyObject) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        clearMap()
        
        self.refreshEvents()
        
    }
    
    //refresh - and on new search
    func clearMap(){
        DispatchQueue.main.async{
            self.mapView.removeAnnotations(self.mapView.annotations)
        }

    }
    
    @IBAction func buttonDates_Action(_ sender: AnyObject) {
        self.performSegue(withIdentifier: segueCalendarEventsViewControllerTORangeDatePickerController, sender: nil)
    }
    
    //--------------------------------------------------------------
    // MARK: - BUTTON 'CALENDARS' - Open EKCalendarChooser
    // MARK: -
    //--------------------------------------------------------------
    @IBAction func buttonCalendars_Action(_ sender: AnyObject) {
        
        DispatchQueue.main.async{
            
            let chooser = EKCalendarChooser(selectionStyle: .multiple, displayStyle: .allCalendars, entityType: .event, eventStore: self.eventKitFrameworkManager.eventStore)
            chooser.modalPresentationStyle = .currentContext
            //these are in nav bar
            chooser.showsDoneButton = true
            chooser.showsCancelButton = true
            chooser.delegate = self
            
            if let selectedCalendarArray = self.eventKitFrameworkManager.selectedCalendarArray {
                //                let setCals =
                //                
                //                Printing description of selectedCalendarArray:
                //                expression produced error: error: /var/folders/gs/1drgtxxj63d5431c5vqp5klm0000gn/T/./lldb/36741/expr1.swift:1:77: error: use of undeclared type 'EventKit'
                //                Swift._DebuggerSupport.stringForPrintObject(Swift.UnsafePointer<Swift.Array<EventKit.EKCalendar>>(bitPattern: 0x1155bc3d0)!.pointee)
                //
                                //if let selectedCalendersSet = NSSet(array: selectedCalendarArray) as? NSSet<EKCalendar>{
                                    
                //                let selectedCalendersSet = NSSet(array: selectedCalendarArray)
                //                chooser.selectedCalendars = selectedCalendersSet as! Set<EKCalendar>
                //let selectedCalendersSet = NSSet(array: selectedCalendarArray)
                chooser.selectedCalendars = Set(selectedCalendarArray)
               
    
            }else{
                self.log.error("self.eventKitFrameworkManager.selectedCalendarArray is nil")
            }
            
            let navigationController = UINavigationController(rootViewController: chooser)
            
            //self.navigationController?.pushViewController(chooser, animated: true)
            self.present(navigationController, animated: true) {
                //visible
            }
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - BUTTON 'CALENDARS' - EKCalendarChooserDelegate
    //--------------------------------------------------------------

    func calendarChooserSelectionDidChange(_ calendarChooser: EKCalendarChooser){
        print("TODO: calendarChooserSelectionDidChange")
        //        self.dismissViewControllerAnimated(true) {
        //            
        //        }
    }
    
    func calendarChooserDidFinish(_ calendarChooser: EKCalendarChooser){
        print("TODO: calendarChooserDidFinish")
        self.dismiss(animated: true) {
            self.eventKitFrameworkManager.selectedCalendarArray = Array(calendarChooser.selectedCalendars)
            // TODO: - is this needed we never call it
            //self.eventKitFrameworkManager.fetchEvents()
            self.refreshEvents()
            self.tableView.reloadData()
        }
    }

    func calendarChooserDidCancel(_ calendarChooser: EKCalendarChooser){
        print("TODO: calendarChooserDidCancel")
        self.dismiss(animated: true) {
            
        }
    }
    

    
    //--------------------------------------------------------------
    // MARK: - DATES - RangeDatePickerControllerDelegate
    // MARK: -
    //--------------------------------------------------------------
    func datesUpdated(_ rangeDatePickerController: RangeDatePickerController, dateFrom: Date, dateTo: Date){
        self.eventKitFrameworkManager.startDate = dateFrom
        self.eventKitFrameworkManager.endDate = dateTo
        self.refreshEvents()
    }
    
    
    //--------------------------------------------------------------
    // MARK: - DATE - FORMAT
    // MARK: -
    //--------------------------------------------------------------
    func dateRangeAsString() -> String{
        return "\(self.formatDate(self.eventKitFrameworkManager.startDate as Date)) - \(self.formatDate(self.eventKitFrameworkManager.endDate as Date))"

    }
    
    func formatDate(_ dateIN: Date) -> NSString{
        
        var dateStringReturned : NSString = ""
        
        let dateFormatterIN = DateFormatter()
        
        //"Mon 11 Feb 2016"
        dateFormatterIN.dateFormat = "E dd MMM yyyy"
        
        // TODO: - Fixtures do we need to support timeZone
        //dateFormatterIN.timeZone = self.userTimeZone()
        dateStringReturned = dateFormatterIN.string(from: dateIN) as NSString
        
        return dateStringReturned
        
    }
    
    //--------------------------------------------------------------
    // MARK: - SEGUE
    // MARK: -
    //--------------------------------------------------------------
    
    let segueCalendarEventsViewControllerTORangeDatePickerController = "segueCalendarEventsViewControllerTORangeDatePickerController"
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == segueCalendarEventsViewControllerTORangeDatePickerController){
            
            if(segue.destination.isMember(of: RangeDatePickerController.self)){
                
                let rangeDatePickerController = (segue.destination as! RangeDatePickerController)
                rangeDatePickerController.delegate = self
                rangeDatePickerController.dateFrom = self.eventKitFrameworkManager.startDate
                rangeDatePickerController.dateTo = self.eventKitFrameworkManager.endDate
                
            }else{
                print("ERROR:not AddTripViewController")
            }
        }
        else{
            print("UNHANDLED SEGUE:\(segue.identifier)")
        }
    }
}
