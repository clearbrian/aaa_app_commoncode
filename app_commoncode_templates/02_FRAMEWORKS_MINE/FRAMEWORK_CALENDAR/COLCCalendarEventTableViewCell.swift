//
//  COLCCalendarEventTableViewCell.swift
//  joyride
//
//  Created by Brian Clear on 05/09/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit
class COLCCalendarEventTableViewCell : UITableViewCell{
    
    @IBOutlet weak var labelId: UILabel!
    @IBOutlet weak var labelName: UILabel!
    
    @IBOutlet weak var labelPlace: UILabel!
    @IBOutlet weak var labelRecurrance: UILabel!
    
    @IBOutlet weak var labelStartTime: UILabel!
    @IBOutlet weak var labelEndTime: UILabel!
    
    @IBOutlet weak var labelDescription: UILabel!
    
    var customFontApplied = false
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //print("COLCCalendarEventTableViewCelll init coder")
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        super.init(style: UITableViewCellStyle.subtitle, reuseIdentifier: reuseIdentifier)
        
    }
    
    func applyCustomFont(){
        if customFontApplied{
            //cell created already - BODY is replace with customfont - if we reuse it and call this again it will try and convert font again
        }else{
            
            self.labelId?.applyCustomFontForCurrentTextStyle()
            self.labelName?.applyCustomFontForCurrentTextStyle()
            self.labelPlace?.applyCustomFontForCurrentTextStyle()
            self.labelRecurrance?.applyCustomFontForCurrentTextStyle()
            self.labelStartTime?.applyCustomFontForCurrentTextStyle()
            self.labelEndTime?.applyCustomFontForCurrentTextStyle()
            self.labelDescription?.applyCustomFontForCurrentTextStyle()
            
            customFontApplied = true
        }
    }
}
