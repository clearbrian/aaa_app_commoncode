//
//  CalendarAuthorizer.swift
//  SharedCode
//
//  Created by Vandad on 7/9/15.
//  Copyright © 2015 Pixolity. All rights reserved.
//

import Foundation
import EventKit

//--------------------------------------------------------------
// MARK: - ERROR MESSAGE
// MARK: -
//--------------------------------------------------------------

let kCalendar_AuthErrorMessage_NotDetermined_NotGranted = "User has previously refused access to Calendars. If this was in error you may change it in Settings > Privacy > Calendars"

/* The application is not authorized to access contact data.
 *  The user cannot change this application’s status, possibly due to active restrictions such as parental controls being in place. */
let kCalendar_AuthErrorMessage_Restricted = "Access to Calendars is restricted. The user cannot change this access, possibly due to restrictions such as parental controls. Please check \rSettings > Privacy > Calendars\ror\rSettings > General > Restrictions > Calendars"

/*! The user explicitly denied access to contact data for the application. */
let kCalendar_AuthErrorMessage_Denied =  "The user has previously denied access to Calendars.\rIf this was in error you may change it in Settings > Privacy > Calendars"

//--------------------------------------------------------------

open class CalendarAuthorizer{
    
    open let eventStore = EKEventStore()
    
    let log = MyXCodeEmojiLogger.defaultInstance
    
    var currentCNAuthorizationStatus: EKAuthorizationStatus = .notDetermined
    
    open func authorizeCalendarsWithCompletionHandler(_ completionHandler: @escaping (_ succeeded: Bool, _ errorMessage : String?) -> Void){
        
        self.currentCNAuthorizationStatus = EKEventStore.authorizationStatus(for: .event)

        switch self.currentCNAuthorizationStatus{
        case .authorized:
            completionHandler(true, nil)
            
        case .notDetermined:
            //Request it
            self.requestCalendarAccess{succeeded, errorMessageReq in
                
                if succeeded{
                    //------------------------------------------------------------------------------------------------
                    //CONTACT ACCESS AUTHORIZED - Show Picker
                    //------------------------------------------------------------------------------------------------
                    //user granted access in alert
                    completionHandler(true, nil)
                    
                } else {
                    //------------------------------------------------------------------------------------------------
                    //CONTACT ACCESS NOT AUTHORIZED
                    //------------------------------------------------------------------------------------------------
                    
                    if let errorMessageReq = errorMessageReq{
                        
                        completionHandler(false, errorMessageReq)
                        
                    }else{
                        self.log.error("errorMessage is nil but succeeded is false")
                        
                        completionHandler(false, kCalendar_AuthErrorMessage_NotDetermined_NotGranted)
                    }
                }
            }
        case .restricted:
            //print(".Restricted")
            completionHandler(false, kCalendar_AuthErrorMessage_Restricted)
            
        case .denied:
            //print(".Denied")
            //Request it again incase it was DENIED but then user switches to Settings and turns it back on then back to the app
            self.requestCalendarAccess{succeeded, errorMessageReq in
                
                if succeeded{
                    //------------------------------------------------------------------------------------------------
                    //CONTACT ACCESS AUTHORIZED - Show Picker
                    //------------------------------------------------------------------------------------------------
                    //user granted access in alert
                    completionHandler(true, nil)
                    
                } else {
                    //------------------------------------------------------------------------------------------------
                    //CONTACT ACCESS NOT AUTHORIZED
                    //------------------------------------------------------------------------------------------------
                    
                    if let errorMessageReq = errorMessageReq{
                        
                        completionHandler(false, errorMessageReq)
                        
                    }else{
                        self.log.error("errorMessage is nil but succeeded is false")
                        
                        completionHandler(false, kCalendar_AuthErrorMessage_NotDetermined_NotGranted)
                    }
                }
            }
        
        }
    }
    
    //need to do this EVERY time in case access was DENIED then user goes into settings and changes switch to on in Provacy > Calendars or in Restrictions
    func requestCalendarAccess(_ completionHandler: @escaping (_ succeeded: Bool, _ errorMessage : String?) -> Void){
        

        
        self.eventStore.requestAccess(to: .event) { (granted: Bool, error: Error?) in
            if (granted) {
                print("permission granted for calendar")
                completionHandler(true, nil)
                
            }
            else {
                if let error = error{
                    completionHandler(false, error.localizedDescription)
                    
                }else{
                    //---------------------------------------------------------------------
                    self.log.error("errorMessage is nil but succeeded is false")
                    //completionHandler(succeeded: false, errorMessage: kCalendar_AuthErrorMessage_NotDetermined_NotGranted)
                    //---------------------------------------------------------------------
                    //WHEN User has turned off permission then goes into settings and turns it on you need to request access again 
                    //First time its
                    switch self.currentCNAuthorizationStatus{
                    case .authorized:
                        print(".Authorized")
                        completionHandler(true, nil)
                    case .notDetermined:
                        print(".NotDetermined")
                        completionHandler(false, kCalendar_AuthErrorMessage_NotDetermined_NotGranted)
                        
                    case .restricted:
                        print(".Restricted")
                        completionHandler( false, kCalendar_AuthErrorMessage_Restricted)
                        
                    case .denied:
                        print(".Denied")
                        completionHandler(false, kCalendar_AuthErrorMessage_Denied)
                    }
                    //---------------------------------------------------------------------
                }
            }
        }
    }
}
