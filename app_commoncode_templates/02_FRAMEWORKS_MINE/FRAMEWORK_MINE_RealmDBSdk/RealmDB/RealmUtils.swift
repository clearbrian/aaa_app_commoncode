//
//  RealmUtils.swift
//  TestRealDB
//
//  Created by Brian Clear on 18/02/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import RealmSwift

class RealmUtils{
    let log = MyXCodeEmojiLogger.defaultInstance
    
    //when running test code handy to wipe the whole db everytime we start the app
    class func cleanInstallDeleteDB(){
        //upgraded n broke 
        print("cleanInstallDeleteDB path error")
//        if let path = Realm.Configuration.defaultConfiguration.path {
//            do {
//                try NSFileManager.defaultManager().removeItemAtPath(path)
//                self.log.info("Clean Install : deleted [\(path)]")
//            } catch {
//                self.log.info("Failed to delete default.realm - ok if first install")
//            }
//        }else{
//            self.log.info("Realm.Configuration.defaultConfiguration.path is nil")
//        }
    }
    
    class func deleteAllData() {
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
        }
    }
    
    class func dumpRealmPaths(){
        //path to the default.realm
        appDelegate.log.info("========= REALM DB ===========")
        appDelegate.log.info("realmDBFullPath:\r\(RealmUtils.realmDBFullPath())")
        appDelegate.log.info("")
        appDelegate.log.debug("realmDBDirectoryFullPath:\r\(RealmUtils.realmDBDirectoryFullPath())")
        appDelegate.log.info("")
        appDelegate.log.info("documentsPath:\r\(RealmUtils.documentsPath())")
        appDelegate.log.info("==============================")

    }
    
    class func realmDBFullPath() -> String{
        var realmDBFullPath = "nil"
        
        //print(Realm.Configuration.defaultConfiguration.path!)
        
//        if let path = Realm.Configuration.defaultConfiguration.path {
//             //let pathNSString = path as NSString
//            
//            ///Users/gbxc/Library/Developer/CoreSimulator/Devices/7DF14D0C-7867-4F9B-B86C-99F166B671F1/data/Containers/Data/Application/67D495B0-4296-45A2-A837-9FC741EDE54B/Documents/default.realm
//            
//            //print(pathNSString.stringByDeletingLastPathComponent)
//            realmDBFullPath = path
//            
//        }else{
//            self.log.info("Realm.Configuration.defaultConfiguration.path is nil")
//        }
        
        //-------------------------------------------------------------------
        //v 1.0*
        //Realm.Configuration.defaultConfiguration.fileURL
        if let fileURL = Realm.Configuration.defaultConfiguration.fileURL {
            //let pathNSString = path as NSString
            
            ///Users/gbxc/Library/Developer/CoreSimulator/Devices/7DF14D0C-7867-4F9B-B86C-99F166B671F1/data/Containers/Data/Application/67D495B0-4296-45A2-A837-9FC741EDE54B/Documents/default.realm
            
            //print(pathNSString.stringByDeletingLastPathComponent)
            realmDBFullPath = "\(fileURL)"
            
        }else{
            appDelegate.log.info("Realm.Configuration.defaultConfiguration.path is nil")
        }
        
        
        return realmDBFullPath
    }
    
    class func realmDBDirectoryFullPath() -> String{
        var realmDBDirectoryFullPath = "nil"
        
        //print(Realm.Configuration.defaultConfiguration.path!)
        
//        if let path = Realm.Configuration.defaultConfiguration.path {
//            
//            ///Users/gbxc/Library/Developer/CoreSimulator/Devices/7DF14D0C-7867-4F9B-B86C-99F166B671F1/data/Containers/Data/Application/67D495B0-4296-45A2-A837-9FC741EDE54B/Documents/default.realm
//            
//            let pathNSString = path as NSString
//            realmDBDirectoryFullPath = pathNSString.stringByDeletingLastPathComponent
//            
//        }else{
//            self.log.info("Realm.Configuration.defaultConfiguration.path is nil")
//        }
        if let fileURL = Realm.Configuration.defaultConfiguration.fileURL {
            //let pathNSString = path as NSString
            
            ///Users/gbxc/Library/Developer/CoreSimulator/Devices/7DF14D0C-7867-4F9B-B86C-99F166B671F1/data/Containers/Data/Application/67D495B0-4296-45A2-A837-9FC741EDE54B/Documents/default.realm
            
            //print(pathNSString.stringByDeletingLastPathComponent)
            realmDBDirectoryFullPath = "\(fileURL)"
            
        }else{
             MyXCodeEmojiLogger.defaultInstance.error("Realm.Configuration.defaultConfiguration.path is nil")
        }
        return realmDBDirectoryFullPath
    }
    
    //By default default.realm is stored in app /Documents
    class func documentsPath() -> String{
        
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        //self.log.info("documentsPath:\r\(documentsPath)")
        return documentsPath
    }
}
