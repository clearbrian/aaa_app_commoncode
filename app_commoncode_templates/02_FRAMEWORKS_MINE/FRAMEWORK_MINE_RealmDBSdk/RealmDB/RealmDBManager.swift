//
//  RealmDBManager.swift
//  TestRealDB
//
//  Created by Brian Clear on 18/02/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import RealmSwift
// TODO: - swift3
enum RealmDBResultType {
    ///case Success(anyObject: AnyObject)
    case success
    
    //swift3
    case error(errorType: RealmDBError)
    
}
//classes with RealmDb.Error
enum RealmDBError: Swift.Error {
    case unexpectedTypeReturned(expectedType:String, returnedType:String)
    case errorThrownByRealm(errorString:String)
}


open class RealmDBManager{
    let log = MyXCodeEmojiLogger.defaultInstance
    var dbOpenedOk = false
    var dbOpenedNSError : NSError?
    
    fileprivate var realm_ : Realm?
    
    //non optional getter - dont call this if try Realm() failed below
    open var realm : Realm{
        return realm_!
    }
    
    
    public init(){
        
        //During Dev - wipe the db everytime to change the object model classes
        //doesnt work need to delete the app in simulator
        //RealmUtils.cleanInstallDeleteDB()
        //RealmUtils.deleteAllData()
        
        
        //check for API changes and migrate data manually
        //RealmDBManager.migrate()
        
        //Realm DB
        do {
            self.realm_ = try Realm()
            dbOpenedOk = true
        }
        catch let error as NSError {
            print("REALM DB ERROR: createRealm exception:\(error)");
            dbOpenedOk = false
            dbOpenedNSError = error
        }
    }
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - SAVE
    // MARK: -
    //--------------------------------------------------------------
    func save(_ object: Object,
          completion: (_ resultType: RealmDBResultType) -> Void){
        //e.g.hypeDocument.showNextScene(hypeDocument.kSceneTransitionCrossfade)
        self.realm.beginWrite()
        self.realm.add(object)
        
        do {
            try realm.commitWrite()
            self.log.info("commitWrite OK")
            completion(RealmDBResultType.success)
        }
        catch let nserror as NSError {
            dumpRealmNSError(nserror)
            
            // see extension NSError below
            let errorType = nserror.errorType
            let resultType = RealmDBResultType.error(errorType: errorType)
            
            completion(resultType)
        }
    }
    
    
//    func returnAllRealmPlace(RealmPlace: RealmPlace){
//        
//        
//        return self.realm.objects(object.self)
////        //e.g.hypeDocument.showNextScene(hypeDocument.kSceneTransitionCrossfade)
////        self.realm.beginWrite()
////        self.realm.add(object)
////        
////        do {
////            try realm.commitWrite()
////            self.log.info("commitWrite OK")
////            completion(resultType: RealmDBResultType.Success)
////        }
////        catch let nserror as NSError {
////            dumpRealmNSError(nserror)
////            
////            // see extension NSError below
////            let errorType = nserror.errorType
////            let resultType = RealmDBResultType.Error(errorType: errorType)
////            
////            completion(resultType: resultType)
////        }
//    }
    
    
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - UPDATE
    // MARK: -
    //--------------------------------------------------------------

    func update(updateBlock: @escaping () -> Void,
                 completion: (_ resultType: RealmDBResultType) -> Void
        )
    {
        //---------------------------------------------------------------------
        do {
            try realm.write {
                //Update is done but setting properties on an object whilst in a write block
                updateBlock()
            }
            self.log.info("delete OK")
            completion(RealmDBResultType.success)
        }
        catch let nserror as NSError {
            dumpRealmNSError(nserror)
            
            // see extension NSError below
            let errorType = nserror.errorType
            let resultType = RealmDBResultType.error(errorType: errorType)
            
            completion(resultType)
        }
        
        //---------------------------------------------------------------------
    }
    
    
    
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - DELETE
    // MARK: -
    //--------------------------------------------------------------

    func delete(_ object: Object,
            completion: (_ resultType: RealmDBResultType) -> Void)
    {
        do {
            try realm.write {
                realm.delete(object)
            }
            self.log.info("delete OK")
            completion(RealmDBResultType.success)
        }
        catch let nserror as NSError {
            dumpRealmNSError(nserror)
            
            // see extension NSError below
            let errorType = nserror.errorType
            let resultType = RealmDBResultType.error(errorType: errorType)
            
            completion(resultType)
        }
    }
    
    
    
    
    
    
    func dumpRealmNSError(_ nsError: NSError){
        self.log.info(" createRealm exception:\(nsError)");
        self.log.error("error.domain:\(nsError.domain)");
        self.log.error("error.code:\(nsError.code)");
        self.log.error("error.userInfo:\(nsError.userInfo)");
        self.log.error("error.localizedDescription:\(nsError.localizedDescription)");
       
    }
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - STARTUP
    //--------------------------------------------------------------
    
    
    //https://realm.io/docs/swift/latest/#migrations
    //https://github.com/realm/realm-cocoa/blob/master/examples/ios/swift-2.1.1/Migration/AppDelegate.swift
    open class func migrate() {
        let config = Realm.Configuration(
            // Set the new schema version. This must be greater than the previously used
            // version (if you've never set a schema version before, the version is 0).
            schemaVersion: 1,
            
            // Set the block which will be called automatically when opening a Realm with
            // a schema version lower than the one set above
            migrationBlock: { migration, oldSchemaVersion in
                if (oldSchemaVersion < 1) {
                   
                    // Nothing to do!
                    // Realm will automatically detect new properties and removed properties
                    // And will update the schema on disk automatically
                }
            }
        )
        Realm.Configuration.defaultConfiguration = config
    }

}

extension NSError {
    var errorType : RealmDBError{
        // self.log.info(" createRealm exception:\(error)");
        MyXCodeEmojiLogger.defaultInstance.error("error.domain:\(self.domain)");
        MyXCodeEmojiLogger.defaultInstance.error("error.code:\(self.code)");
        MyXCodeEmojiLogger.defaultInstance.error("error.userInfo:\(self.userInfo)");
        MyXCodeEmojiLogger.defaultInstance.error("error.localizedDescription:\(self.localizedDescription)");
        // TODO: - HANDLE DOMAIN ERRORS
        let errorString = self.localizedDescription
        let errorType = RealmDBError.errorThrownByRealm(errorString: errorString)
        return errorType
    }
}


