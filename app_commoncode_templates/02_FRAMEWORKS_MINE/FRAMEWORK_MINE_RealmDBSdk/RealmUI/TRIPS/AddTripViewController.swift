//
//  AddTripViewController.swift
//  TestRealDB
//
//  Created by Brian Clear on 18/02/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit


public enum ChoosePlaceType : String {
    case Start = "Start"
    case Destination = "Destination"
}

protocol AddTripViewControllerDelegate {
    func controllerDidAddTrip(_ controller: AddTripViewController, realmTrip: RealmTrip)
}

class AddTripViewController: ParentViewController, SelectPlaceViewControllerDelegate {
    
    var delegate: AddTripViewControllerDelegate?
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var textFieldTripName: UITextField!
    
    @IBOutlet weak var textFieldNameStart: UITextField!
    @IBOutlet weak var textViewAddressStart: UITextView!
    @IBOutlet weak var textFieldPlaceIdStart: UITextField!
    @IBOutlet weak var textFieldLatStart: UITextField!
    @IBOutlet weak var textFieldLngStart: UITextField!
    
    @IBOutlet weak var textFieldNameDestination: UITextField!
    @IBOutlet weak var textViewAddressDestination: UITextView!
    @IBOutlet weak var textFieldPlaceIdDestination: UITextField!
    @IBOutlet weak var textFieldLatDestination: UITextField!
    @IBOutlet weak var textFieldLngDestination: UITextField!
    
    
    var trip = RealmTrip()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool){
        super.viewDidAppear(animated)
        print("viewDidAppear")

        
        //contentView.width is now set to width of phone in Landscape OR Portrait
        //you ALWAYS need this regardless of what frame/constraints you set on contentView else nothing will scroll
        
        self.scrollView.contentSize = CGSize(width: self.contentView.frame.width,
                                            height: self.contentView.frame.height)
        

    }
    
    var selectedChoosePlaceType :ChoosePlaceType?
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - CHOOSE START/DESTINATION
    // MARK: -
    //--------------------------------------------------------------

    let ksegueAddTripViewControllerTOSelectPlaceViewController = "segueAddTripViewControllerTOSelectPlaceViewController"
    
    @IBAction func buttonChooseStart_Action(_ sender: AnyObject) {
        self.selectedChoosePlaceType = .Start
        self.performSegue(withIdentifier: ksegueAddTripViewControllerTOSelectPlaceViewController, sender: nil)
    }
    
    @IBAction func buttonChooseDestination_Action(_ sender: AnyObject) {
        self.selectedChoosePlaceType = .Destination
        self.performSegue(withIdentifier: ksegueAddTripViewControllerTOSelectPlaceViewController, sender: nil)
    }
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - prepareForSegue
    //--------------------------------------------------------------
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        if(segue.identifier == ksegueAddTripViewControllerTOSelectPlaceViewController){
            
            if(segue.destination.isMember(of: SelectPlaceViewController.self)){
                let selectPlaceViewController = (segue.destination as! SelectPlaceViewController)
                selectPlaceViewController.delegate = self
                
            }else{
                self.log.error("not SelectPlaceViewController")
            }
        }
        else{
            self.log.info("UNHANDLED SEGUE:\(segue.identifier)")
        }
    }
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - SelectPlaceViewControllerDelegate
    // MARK: -
    //--------------------------------------------------------------

    
    func controllerDidSelectPlace(_ selectPlaceViewController: SelectPlaceViewController, realmPlace: RealmPlace){
        if let selectedChoosePlaceType = self.selectedChoosePlaceType {
            displayAndStoreRealmPlace(realmPlace, choosePlaceType: selectedChoosePlaceType)
            
        }else{
            self.log.error("self.selectedChoosePlaceType is nil")
        }
    }
    
    func displayAndStoreRealmPlace(_ realmPlace: RealmPlace, choosePlaceType: ChoosePlaceType){
       
        switch choosePlaceType{
        case .Start:
            
             self.trip.realmPlaceStart = realmPlace
            
            self.textFieldNameStart.text = realmPlace.name
            self.textViewAddressStart.text = realmPlace.address
            self.textFieldPlaceIdStart.text = realmPlace.place_id
            self.textFieldLatStart.text = "\(realmPlace.latitude)"
            self.textFieldLngStart.text = "\(realmPlace.longitude)"
            
        case .Destination:
            
            self.trip.realmPlaceDestination = realmPlace
            
            self.textFieldNameDestination.text = realmPlace.name
            self.textViewAddressDestination.text = realmPlace.address
            self.textFieldPlaceIdDestination.text = realmPlace.place_id
            self.textFieldLatDestination.text = "\(realmPlace.latitude)"
            self.textFieldLngDestination.text = "\(realmPlace.longitude)"
            
        }
    }
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - SAVE TRIP
    // MARK: -
    //--------------------------------------------------------------
    @IBAction func buttonSave_Action(_ sender: AnyObject) {

        self.trip.name = textFieldTripName.text!
        
        appDelegate.colcDBManager.realmDBManager.save(self.trip){
            (resultType: RealmDBResultType) -> Void in
            
            //-----------------------------------------------------------
            switch resultType {
            case .success:
                //------------------------------------------------------------------------------------------------
                //SUCCESS
                //------------------------------------------------------------------------------------------------
                
                self.log.info("Trip saved ok")
                
                if let navigationController = self.navigationController {
                    navigationController.popViewController(animated: true)
                    self.tellDelegateDidAddTrip()
                    
                }else{
                    //view was presented not pushed
                    self.dismiss(animated: true, completion: {
                        self.tellDelegateDidAddTrip()
                    })
                }
                //---------------------------------------------------------------------
            case .error(let errorType):
                //------------------------------------------------------------------------------------------------
                //ERROR
                //------------------------------------------------------------------------------------------------
                self.log.error("\(errorType)")
            }
            //-----------------------------------------------------------
        }
    }
    
    func tellDelegateDidAddTrip(){
        if let delegate = self.delegate{
            
            //------------------------------------------------
            delegate.controllerDidAddTrip(self, realmTrip: self.trip)
            //------------------------------------------------
            
        }else{
            self.log.error("delegate is nil")
        }
    }
    
}
