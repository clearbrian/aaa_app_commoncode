//
//  TripsViewController.swift
//  TestRealDB
//
//  Created by Brian Clear on 18/02/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class TripsViewController: ParentViewController, AddTripViewControllerDelegate {

    
    @IBOutlet weak var tableView: UITableView!
    
    let realm = try! Realm()
    lazy var realmTripsArray: Results<RealmTrip> = {
        self.realm.objects(RealmTrip.self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.log.info("realmTripsArray.count:\(realmTripsArray.count)")
   
    }

    func reloadDataAndTable(){
        realmTripsArray = self.realm.objects(RealmTrip.self)
        self.tableView.reloadData()
    }
    
    //------------------------------------------------
    // MARK: UITableViewDataSource
    //------------------------------------------------
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return realmTripsArray.count;
    }

    
    func dateStringLocalized(_ dateIN: Date) -> String{
        
        //if we cant format return unformatted
        var dateStringReturned = "\(dateIN)"
        
        let dateFormatterIN = DateFormatter()
        
        //"11 Feb 2016"
        //dateFormatterIN.dateFormat = "dd/MMM yyyy"
        //localized better
        dateFormatterIN.dateStyle = .medium
        dateFormatterIN.timeStyle = .medium
        
        // TODO: - Fixtures do we need to support timeZone
        //dateFormatterIN.timeZone = self.userTimeZone()
        dateStringReturned = dateFormatterIN.string(from: dateIN)
        
        return dateStringReturned
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell{
        
        let cellIdentifier = "TripsTableViewCell"
        
        var cell = UITableViewCell()
        
        
        
        if let tripsTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? TripsTableViewCell{
            
            let realmTrip: RealmTrip = realmTripsArray[indexPath.row]
            //-----------------------------------------------------------------------------------
            tripsTableViewCell.labelName.text = realmTrip.name
            tripsTableViewCell.labelDateTime.text = dateStringLocalized(realmTrip.dateTime as Date)
            //-----------------------------------------------------------------------------------
            
            if let openWithType: OpenWithType =  OpenWithType.openWithTypeForRawValue( realmTrip.openWithTypeId) {
                tripsTableViewCell.labelOpenWithTypeName.text = openWithType.name()
            }else{
                self.log.error("openWithTypeForRawValue is nil for realmTrip.openWithTypeId:\(realmTrip.openWithTypeId)")
                tripsTableViewCell.labelOpenWithTypeName.text = "Unknown"
            }
            //-----------------------------------------------------------------------------------
           
            if let realmPlaceStart = realmTrip.realmPlaceStart {
                tripsTableViewCell.labelPlaceStart.text = "\(realmPlaceStart.name) - \(realmPlaceStart.address)"
            }else{
                self.log.error("realmTrip.realmPlaceStart is nil")
            }
            //-----------------------------------------------------------------------------------
            if let realmPlaceDestination = realmTrip.realmPlaceDestination {
                tripsTableViewCell.labelPlaceDestination.text = "\(realmPlaceDestination.name) - \(realmPlaceDestination.address)"
            }else{
                self.log.error("realmTrip.realmPlaceDestination is nil")
            }
            //-----------------------------------------------------------------------------------
           
       
//swipe to delete
//            if self.sortType == .SortType_InsertionOrder || self.sortType == .SortType_Name{
//                if let arrayRealmPlaces = self.arrayRealmPlaces {
//                    //------------------------------------------------
//                    realmPlace = arrayRealmPlaces[indexPath.row]
//                    //------------------------------------------------
//                }else{
//                    self.log.error("self.arrayRealmPlaces is nil")
//                    realmPlace = nil
//                }
//            }else if self.sortType == .SortType_Nearest {
//                if let arrayRealmPlacesSortedByDistance = self.arrayRealmPlacesSortedByDistance {
//                    //------------------------------------------------
//                    realmPlace = arrayRealmPlacesSortedByDistance[indexPath.row]
//                    //------------------------------------------------
//                }else{
//                    self.log.error("self.arrayRealmPlacesSortedByDistance is nil")
//                    realmPlace = nil
//                }
//            }else{
//                self.log.error("UNHANDLED sortType is nil")
//            }
//            
//            //------------------------------------------------
//            //display realmPlace
//            //------------------------------------------------
//            if let realmPlace = realmPlace {
//                //-----------------------------------------------------------------------------------
//                //dont convert to CLKPlace - done when you tap on a row - better as get Detail called
//                selectPlaceTableViewCell.labelName?.text = realmPlace.name
//                selectPlaceTableViewCell.labelAddress?.text = realmPlace.address
//                //-----------------------------------------------------------------------------------
//                let clkPlaceWrapperType = CLKPlaceWrapperType.clkPlaceWrapperTypeForRaw(realmPlace.clkPlaceWrapperTypeId)
//                
//                selectPlaceTableViewCell.labelType?.text = "[\(clkPlaceWrapperType.rawValue)] \(clkPlaceWrapperType.nameReadable())"
//                //-----------------------------------------------------------------------------------
//                // TODO: - COMMENT
//                selectPlaceTableViewCell.labelLocality?.text = "Locality: todo in SaveToTrip" //realmPlace.address << clkPlace.locality
//                //------------------------------------------------
//            }else{
//                self.log.error("self.arrayRealmPlaces is nil")
//                
//                //dont convert to CLKPlace - done when you tap on a row - better as get Detail called
//                selectPlaceTableViewCell.labelName?.text = ""
//                selectPlaceTableViewCell.labelAddress?.text = ""
//                selectPlaceTableViewCell.labelType?.text = ""
//                selectPlaceTableViewCell.labelLocality?.text = ""
//                //------------------------------------------------
//            }
//            
            
            cell = tripsTableViewCell
            
        }else{
            self.log.info("cellIdentifier[\(cellIdentifier)] not found")
        }
        
        return cell

    }
    
    //------------------------------------------------
    // MARK: UITableViewDelegate
    //------------------------------------------------
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath){
        let realmTrip = realmTripsArray[indexPath.row]
        
        self.log.info("didSelectRowAtIndexPath:ROW:\(indexPath.row) Trip:\(realmTrip.name)")
        
    }

    //--------------------------------------------------------------
    // MARK: -
    // MARK: - ADD NEW TRIP
    //--------------------------------------------------------------
    let ksegueTripsTOAddTrip = "segueTripsTOAddTrip"
    
    @IBAction func buttonAdd_Action(_ sender: AnyObject) {
        
        performSegue(withIdentifier: ksegueTripsTOAddTrip, sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == ksegueTripsTOAddTrip){

            if(segue.destination.isMember(of: AddTripViewController.self)){
                
                let addTripViewController = (segue.destination as! AddTripViewController)
                addTripViewController.delegate = self

            }else{
                self.log.error("not AddTripViewController")
            }
        }
        else{
            self.log.info("UNHANDLED SEGUE:\(segue.identifier)")
        }
    }
    
    func controllerDidAddTrip(_ controller: AddTripViewController, realmTrip: RealmTrip){
        reloadDataAndTable()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
