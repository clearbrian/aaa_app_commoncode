//
//  SavedTripsUINavigationController.swift
//  joyride
//
//  Created by Brian Clear on 01/03/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit

class SavedTripsUINavigationController: UINavigationController  {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
