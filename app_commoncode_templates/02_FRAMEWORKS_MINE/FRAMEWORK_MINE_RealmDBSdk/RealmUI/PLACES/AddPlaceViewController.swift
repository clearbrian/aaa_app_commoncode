//
//  AddPlaceViewController.swift
//  TestRealDB
//
//  Created by Brian Clear on 18/02/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation

import UIKit

protocol AddPlaceViewControllerDelegate {
    func didAddItem(_ controller: AddPlaceViewController)
}

class AddPlaceViewController: ParentViewController  {
    
    var delegate: AddPlaceViewControllerDelegate?
    
    @IBOutlet weak var textFieldName: UITextField!
    
    @IBOutlet weak var textViewAddress: UITextView!
    
    @IBOutlet weak var textFieldPlaceId: UITextField!
    
    @IBOutlet weak var textFieldLat: UITextField!
    
    @IBOutlet weak var textFieldLng: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //UITextView has no border - add one so it matches UITextField
        self.textViewAddress.layer.borderColor = UIColor(red: 210/255, green: 235/255, blue: 231/255, alpha: 1.0).cgColor
        self.textViewAddress.layer.borderWidth = 1.0
        self.textViewAddress.layer.cornerRadius = 5.0
    }
    
    @IBAction func buttonSave_Action(_ sender: AnyObject) {
        
        let realmPlace = RealmPlace()
        
        realmPlace.name = self.textFieldName.text!
        realmPlace.address = self.textViewAddress.text!
        realmPlace.place_id = self.textFieldPlaceId.text!
        realmPlace.latitude =  (self.textFieldLat.text! as NSString).doubleValue
        realmPlace.longitude = (self.textFieldLng.text! as NSString).doubleValue
        
        appDelegate.colcDBManager.realmDBManager.save(realmPlace){
            (resultType: RealmDBResultType) -> Void in
            
            //-----------------------------------------------------------
            switch resultType {
            case .success:
                //------------------------------------------------------------------------------------------------
                //SUCCESS
                //------------------------------------------------------------------------------------------------

                self.log.info("place saved ok")
                
                if let navigationController = self.navigationController {
                    navigationController.popViewController(animated: true)
                    self.tellDelegateItemAdded()
                   
                }else{
                    //view was presented not pushed
                    self.dismiss(animated: true, completion: {
                        self.tellDelegateItemAdded()
                    })
                }
                //---------------------------------------------------------------------
            case .error(let errorType):
                //------------------------------------------------------------------------------------------------
                //ERROR
                //------------------------------------------------------------------------------------------------
                self.log.error("\(errorType)")
            }
            //-----------------------------------------------------------
        }
    }
    
    func tellDelegateItemAdded(){
        if let delegate = self.delegate{
            
            //------------------------------------------------
            delegate.didAddItem(self)
            //------------------------------------------------
            
        }else{
            self.log.error("delegate is nil")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
