//
//  EditPlaceViewController.swift
//  TestRealDB
//
//  Created by Brian Clear on 26/02/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit


protocol EditPlaceViewControllerDelegate {
    func didEditItem(_ controller: EditPlaceViewController)
}


class EditPlaceViewController: ParentViewController {
    var selectedPlaceToEdit : RealmPlace?
    
    var delegate: EditPlaceViewControllerDelegate?
    
    //------------------------------------------------------------------------------------------------
    @IBOutlet weak var textFieldName: UITextField!
    @IBOutlet weak var textViewAddress: UITextView!
    @IBOutlet weak var textFieldPlaceId: UITextField!
    @IBOutlet weak var textFieldLat: UITextField!
    @IBOutlet weak var textFieldLng: UITextField!
    //------------------------------------------------------------------------------------------------

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //UITextView has no border - add one so it matches UITextField
        self.textViewAddress.layer.borderColor = UIColor(red: 210/255, green: 235/255, blue: 231/255, alpha: 1.0).cgColor
        self.textViewAddress.layer.borderWidth = 1.0
        self.textViewAddress.layer.cornerRadius = 5.0
        
        // Do any additional setup after loading the view, typically from a nib.
        if let selectedPlaceToEdit = self.selectedPlaceToEdit{
            fillVCFieldsFromPlace(selectedPlaceToEdit)
        }else{
           self.log.error("selectedPlaceToEdit is nil")
        }
    }
    
    func fillVCFieldsFromPlace(_ place:RealmPlace){
        
        self.textFieldName.text = place.name
        self.textViewAddress.text = place.address
        self.textFieldPlaceId.text = place.place_id
        self.textFieldLat.text = "\(place.latitude)"
        self.textFieldLng.text = "\(place.longitude)"
    }
    
    @IBAction func buttonSave_Action(_ sender: AnyObject) {
        if let place = self.selectedPlaceToEdit {
            //BASIC VERSION
            //    do {
            //        //Update - just set Objects properties while in write block
            //        try realm.write {
            //            place.name = self.textFieldName.text!
            //            place.address = self.textViewAddress.text!
            //            place.placeId = self.textFieldPlaceId.text!
            //            place.latitude =  (self.textFieldLat.text! as NSString).doubleValue
            //            place.longitude = (self.textFieldLng.text! as NSString).doubleValue
            //        }
            //        self.log.info("commitWrite OK")
            //        completion(resultType: RealmDBResultType.Success)
            //    }
            //    catch let nserror as NSError {
            //        dumpRealmNSError(nserror)
            //        
            //        // see extension NSError below
            //        let errorType = nserror.errorType
            //        let resultType = RealmDBResultType.Error(errorType: errorType)
            //        
            //        completion(resultType: resultType)
            //    }
            //------------------------------------------------------------------------------------------------
            //V2 -
            //------------------------------------------------------------------------------------------------

            appDelegate.colcDBManager.realmDBManager.update(
                updateBlock:{()->Void in
                        place.name = self.textFieldName.text!
                        place.address = self.textViewAddress.text!
                        place.place_id = self.textFieldPlaceId.text!
                        place.latitude =  (self.textFieldLat.text! as NSString).doubleValue
                        place.longitude = (self.textFieldLng.text! as NSString).doubleValue
                
                },
                completion:{(resultType: RealmDBResultType) -> Void in
                    
                    //------------------------------------------------------------------------------------------------
                    switch resultType {
                    case .success:
                        //---------------------------------------------------------
                        //SUCCESS
                        //---------------------------------------------------------
                        
                        self.log.info("place UPDATED ok")
                        
                        if let navigationController = self.navigationController {
                            navigationController.popViewController(animated: true)
                            self.tellDelegateItemEdited()
                            
                        }else{
                            //view was presented not pushed
                            self.dismiss(animated: true, completion: {
                                self.tellDelegateItemEdited()
                            })
                        }
                        //---------------------------------------------------------
                    case .error(let errorType):
                        //---------------------------------------------------------
                        //ERROR
                        //---------------------------------------------------------
                        self.log.error("\(errorType)")
                    }
                    //------------------------------------------------------------------------------------------------
                }
            )//update
            //------------------------------------------------------------------------------------------------

        }else{
            self.log.error("self.selectedPlaceToEdit is nil")
        }
    }

    func tellDelegateItemEdited(){
        if let delegate = self.delegate{
            
            //------------------------------------------------
            delegate.didEditItem(self)
            //------------------------------------------------
            
        }else{
            self.log.error("delegate is nil")
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
