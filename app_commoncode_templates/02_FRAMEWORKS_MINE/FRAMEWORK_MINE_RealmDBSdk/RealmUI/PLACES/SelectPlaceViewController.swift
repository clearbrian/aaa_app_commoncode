//
//  SelectPlaceViewController.swift
//  TestRealDB
//
//  Created by Brian Clear on 26/02/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.

import Foundation
import UIKit
import RealmSwift
//used to be pod but was Swift 2.3 and dev never updated it - so manually converted it myself
//import GeoQueries

protocol SelectPlaceViewControllerDelegate {
    func controllerDidSelectPlace(_ selectPlaceViewController: SelectPlaceViewController, realmPlace: RealmPlace)
}

//Int is SegmentIndex
enum SortType: Int{
    
    case sortType_InsertionOrder = 0
    case sortType_Name = 1
    case sortType_Nearest = 2
    
}
class SelectPlaceViewController: ParentViewController, UITableViewDataSource, UITableViewDelegate {
    
    var delegate: SelectPlaceViewControllerDelegate?
    
    @IBOutlet weak var tableView: UITableView!
    
    var sortType: SortType = .sortType_InsertionOrder
    
    let realm = try! Realm()
//    lazy var arrayRealmPlaces: Results<RealmPlace> = {
//        switch
//        self.realm.objects(RealmPlace)
//    }()
    //-----------------------------------------------------------------------------------
    //couldnt figure out how to convert from [RealmPlace] to Results<RealmPlace>
    var arrayRealmPlaces: Results<RealmPlace>?
    
    var arrayRealmPlacesSortedByDistance:[RealmPlace]? // = [RealmPlace]()
   //-----------------------------------------------------------------------------------
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    @IBOutlet weak var buttonEditDeleteCancel: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //-----------------------------------------------------------------------------------
        self.segmentedControl.applyCustomFontForCurrentTextStyle()
        self.buttonEdit.applyCustomFontForCurrentTextStyle()
        //-----------------------------------------------------------------------------------
        //DYNAMIC TEXT and TABLE ROW HEIGHT
        //-----------------------------------------------------------------------------------
        self.tableView.estimatedRowHeight = 95.0
        //also done in heightForRowAtIndexPath
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        //self.tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        //self.tableView.estimatedSectionHeaderHeight = 80.0
        //------------------------------------------------
        
        
       
        //------------------------------------------------------------------------------------------------
        //Add Edit button to Nav bar
        //NOT NEEDED - if canEditRowAtIndexPath returns true then user can swipe to Delete
        //        let barButtonItem_Edit = UIBarButtonItem(title: "Edit", style: .Plain, target: self, action:"buttonEdit_Action:")
        //        self.navigationItem.rightBarButtonItem = barButtonItem_Edit
        //------------------------------------------------------------------------------------------------
        
        
        
        reloadDataAndTable()
    }
    
    //------------------------------------------------------------------------------------------------
    //TABLE
    //-----------------------------------------------------------------------------------------------
    
    func reloadDataAndTable(){
        
        switch self.sortType{
        case .sortType_InsertionOrder:
            arrayRealmPlaces = self.realm.objects(RealmPlace.self)
            //put most recent at the top
            //had to add date to RealmPlace as sort requires a property
            if let arrayRealmPlaces = self.arrayRealmPlaces {
            
                self.arrayRealmPlaces = arrayRealmPlaces.sorted(byProperty: "timestampCreated", ascending: false);
                
            }else{
                appDelegate.log.error("self.arrayRealmPlaces is nil")
            }
            
            self.log.info("arrayRealmPlaces:\(arrayRealmPlaces?.count)")
            
        case .sortType_Name:
            arrayRealmPlaces = self.realm.objects(RealmPlace.self).sorted(byProperty: "name")
            self.log.info("arrayRealmPlaces:\(arrayRealmPlaces?.count)")
            
        case .sortType_Nearest:
            //arrayRealmPlaces = self.realm.objects(RealmPlace)
            let radius = 450000.0 // 450 km
            //---------------------------------------------------------------------
            // TODO: - put findNearby into a UTIL class - its in 3 places
    
            if let currentLocation = appDelegate.currentLocationManager.currentUserPlace .clLocation {
                
                do {
                    arrayRealmPlacesSortedByDistance = try Realm().findNearby(RealmPlace.self,
                                                                                     origin: currentLocation.coordinate,
                                                                                     radius: radius,
                                                                              sortAscending: true,
                                                                                latitudeKey: "latitude",
                                                                               longitudeKey: "longitude" )
                    
                    self.log.info("arrayRealmPlacesSortedByDistance:\(arrayRealmPlacesSortedByDistance?.count)")
                    
                } catch let error {
                    self.log.error("findNearby failed: \(error)")
                    arrayRealmPlacesSortedByDistance = nil
                }
                
            }else{
                self.log.error("currentUserPlace.clLocation is nil")
            }
     
            //---------------------------------------------------------------------
        }
        //-----------------------------------------------------------------------------------
        self.tableView.reloadData()
    }
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - UITableViewDataSource
    // MARK: -
    //--------------------------------------------------------------
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        var numberOfRowsInSection = 0
        
        if self.sortType == .sortType_InsertionOrder || self.sortType == .sortType_Name{
            if let arrayRealmPlaces = self.arrayRealmPlaces {
                numberOfRowsInSection = arrayRealmPlaces.count
                
            }else{
                self.log.error("self.arrayRealmPlaces is nil")
                numberOfRowsInSection = 0
            }
        }else if self.sortType == .sortType_Nearest {
            if let arrayRealmPlaces = self.arrayRealmPlaces {
                numberOfRowsInSection = arrayRealmPlaces.count
                
            }else{
                self.log.error("self.arrayRealmPlaces is nil")
                numberOfRowsInSection = 0
            }
        }else{
            self.log.error("UNHANDLED sortType is nil")
        }
        return numberOfRowsInSection
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cellIdentifier = "SelectPlaceTableViewCell"
        
        var cell = UITableViewCell()

        
        if let selectPlaceTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? SelectPlaceTableViewCell{

            var realmPlace : RealmPlace?
            
            if self.sortType == .sortType_InsertionOrder || self.sortType == .sortType_Name{
                if let arrayRealmPlaces = self.arrayRealmPlaces {
                    //------------------------------------------------
                    realmPlace = arrayRealmPlaces[indexPath.row]
                    //------------------------------------------------
                }else{
                    self.log.error("self.arrayRealmPlaces is nil")
                     realmPlace = nil
                }
            }else if self.sortType == .sortType_Nearest {
                if let arrayRealmPlacesSortedByDistance = self.arrayRealmPlacesSortedByDistance {
                    //------------------------------------------------
                    realmPlace = arrayRealmPlacesSortedByDistance[(indexPath as NSIndexPath).row]
                    //------------------------------------------------
                }else{
                    self.log.error("self.arrayRealmPlacesSortedByDistance is nil")
                    realmPlace = nil
                }
            }else{
                self.log.error("UNHANDLED sortType is nil")
            }
            
            //------------------------------------------------
            //display realmPlace
            //------------------------------------------------
            if let realmPlace = realmPlace {
                //-----------------------------------------------------------------------------------
                //dont convert to CLKPlace - done when you tap on a row - better as get Detail called
                selectPlaceTableViewCell.labelName?.text = realmPlace.name
                selectPlaceTableViewCell.labelAddress?.text = realmPlace.address
               
                // TODO: - add USE STREET to Recent?
                print("realmPlace.nearestStreetAddress:'\(realmPlace.addressNearest)'")
                //selectPlaceTableViewCell.labelAddress?.text = realmPlace.nearestStreetAddress
                //-----------------------------------------------------------------------------------
                let clkPlaceWrapperType = CLKPlaceWrapperType.clkPlaceWrapperTypeForRaw(realmPlace.clkPlaceWrapperTypeId)
                
//                selectPlaceTableViewCell.labelType?.text = "[\(clkPlaceWrapperType.rawValue)] \(clkPlaceWrapperType.nameReadable())"
                 selectPlaceTableViewCell.labelType?.text = "\(clkPlaceWrapperType.nameReadable())"
                //-----------------------------------------------------------------------------------
                // TODO: - COMMENT
                selectPlaceTableViewCell.labelLocality?.text = "Locality: \(realmPlace.locality)" //realmPlace.address << clkPlace.locality
                //------------------------------------------------
            }else{
                self.log.error("self.arrayRealmPlaces is nil")
                
                //dont convert to CLKPlace - done when you tap on a row - better as get Detail called
                selectPlaceTableViewCell.labelName?.text = ""
                selectPlaceTableViewCell.labelAddress?.text = ""
                selectPlaceTableViewCell.labelType?.text = ""
                selectPlaceTableViewCell.labelLocality?.text = ""
                //------------------------------------------------
            }

            
            cell = selectPlaceTableViewCell
            
        }else{
            self.log.info("cellIdentifier[\(cellIdentifier)] not found")
        }
        
        return cell
    }
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - didSelectRowAtIndexPath
    // MARK: -
    //--------------------------------------------------------------
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        
        if let arrayRealmPlaces = self.arrayRealmPlaces {
            //------------------------------------------------
            let selectedPlace: RealmPlace = arrayRealmPlaces[indexPath.row]
            
            self.log.info("didSelectRowAtIndexPath:ROW:\((indexPath as NSIndexPath).row) Place:\(selectedPlace.name)")
            
            self.controllerDidSelectPlace(selectedPlace)
            //------------------------------------------------
        }else{
            self.log.error("self.arrayRealmPlaces is nil")

        }
        
        var realmPlace : RealmPlace?
        
        if self.sortType == .sortType_InsertionOrder || self.sortType == .sortType_Name{
            if let arrayRealmPlaces = self.arrayRealmPlaces {
                //------------------------------------------------
                realmPlace = arrayRealmPlaces[indexPath.row]
                //------------------------------------------------
            }else{
                self.log.error("self.arrayRealmPlaces is nil")
                realmPlace = nil
            }
        }else if self.sortType == .sortType_Nearest {
            if let arrayRealmPlacesSortedByDistance = self.arrayRealmPlacesSortedByDistance {
                //------------------------------------------------
                realmPlace = arrayRealmPlacesSortedByDistance[(indexPath as NSIndexPath).row]
                //------------------------------------------------
            }else{
                self.log.error("self.arrayRealmPlacesSortedByDistance is nil")
                realmPlace = nil
            }
        }else{
            self.log.error("UNHANDLED sortType is nil")
        }
        
        if let realmPlace = realmPlace {
            //-----------------------------------------------------------------------------------
            self.log.info("didSelectRowAtIndexPath:ROW:\((indexPath as NSIndexPath).row) Place:\(realmPlace.name)")
            
            self.controllerDidSelectPlace(realmPlace)
            //------------------------------------------------
        }else{
            self.log.error("realmPlace is nil")
        }
        
    }
    
    func controllerDidSelectPlace(_ realmPlace: RealmPlace){
        if let delegate = self.delegate{
            
            //------------------------------------------------
            delegate.controllerDidSelectPlace(self, realmPlace: realmPlace)
            //------------------------------------------------
            
        }else{
            self.log.error("delegate is nil")
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - Edit Table
    // MARK: -
    //--------------------------------------------------------------
    //doesnt work turned of gesture on outer container
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool{
        //default is to allow EDITING on all rows
        let canEditRow = true
        
        //        //DISABLE EDITING for first row
        //        //Tapping EDIT will not show Edit controls for the first row
        //        if indexPath.row == 0 {
        //            canEditRow = false
        //        }
        return canEditRow
    }
    
    @IBOutlet weak var buttonEdit: UIButton!
    
    @IBAction func buttonEdit_Action(_ sender: AnyObject) {
        
        //------------------------------------------------------------------------------------------------
        //toggle .editing mode
        //This will show red DELETE ICON on LHS of each row - doesnt show move
        //To show MOVE need to orerride delegate
        //------------------------------------------------------------------------------------------------
        //three ways to do it
        //SHOWS DELETE INSTANTLY
        //if subclass of UITableViewController
        //self.editing = !self.editing
        
        //if tableView is an ivar OR subclass of UITableViewController
        //self.tableView.editing = !self.tableView.editing
        
        //SHOW DELETE BUTTON WITH ANIMATION
        //wont toggle!
        //self.setEditing(true, animated: true)
        
        //        This is not the same as Show DELETE on swipe
        //        http://stackoverflow.com/questions/3309484/uitableviewcell-show-delete-button-on-swipe
        
        //TOGGLE DELETE
        //if parent is UIViewController
        //let editingNew = !self.editing
        
        let editingNew = !self.tableView.isEditing
        
        
        self.tableView.setEditing(editingNew, animated: true)
        
        if editingNew{
            DispatchQueue.main.async{
                self.buttonEdit.setTitle("Cancel", for: UIControlState())
            }
        }else{
            DispatchQueue.main.async{
                self.buttonEdit.setTitle("Delete", for: UIControlState())
            }
        }
        
    }
    
    //--------------------------------------------------------------
    // MARK: - Lifecycle
    // MARK: -
    //--------------------------------------------------------------

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonCancel_Action(_ sender: AnyObject) {
        
        //self.navigationController?.popViewControllerAnimated(true)
        
        self.dismiss(animated: true, completion: {
            
            //------------------------------------------------
//            if let delegate_ = self.delegate {
//                //containerView is hidden by the delegate
//                delegate_.fbViewControllerCancelled(self)
//            }else{
//                self.log.error("self.delegate is nil")
//            }
            //------------------------------------------------
        })
    }
    
    @IBAction func segmentedControlSort_ValueChanged(_ segmentedControlSort: UISegmentedControl) {
        switch segmentedControlSort.selectedSegmentIndex{
        case 0:
            self.sortType = .sortType_InsertionOrder
            
        case 1:
            self.sortType = .sortType_Name
            
        case 2:
            self.sortType = .sortType_Nearest
            
        default:
            self.log.error("UNHANDLED segmentedControlSort:\(segmentedControlSort.selectedSegmentIndex)")
            self.sortType = .sortType_InsertionOrder
        }
        self.reloadDataAndTable()
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath){
        
        switch editingStyle{
        case .none:
            print("editingStyle:None forRowAtIndexPath:\((indexPath as NSIndexPath).row)")
        case .delete:
            //---------------------------------------------------------------------
            //DELETE
            //---------------------------------------------------------------------
            print("editingStyle:Delete forRowAtIndexPath:\((indexPath as NSIndexPath).row)")
            //---------------------------------------------------------------------
            //DELETE INSTANTLY
            //OK
            //tableData.removeAtIndex(indexPath.row)
            //tableView.reloadData()
            
            //---------------------------------------------------------------------
            //DELETE WITH ANIMATION
            //Remove from backing data - data source should be edited first before editing table with animation as table is reloaded and DataSource delegates are called again
//            tableData.removeAtIndex(indexPath.row)
//            
//            var indexPathsToDeleteArray = [NSIndexPath]()
//            indexPathsToDeleteArray.append(indexPath)
//            

//            //---------------------------------------------------------------------
            
            
            if let arrayRealmPlaces = self.arrayRealmPlaces {
                //------------------------------------------------
                let realmPlace = arrayRealmPlaces[indexPath.row]
                self.log.info("DELETE:\(realmPlace.name)")

                try! realm.write {
                    self.realm.delete(realmPlace)
                }
                //-----------------------------------------------------------------------------------
                //doesnt work the reloadDataAndTable() causes it to flash
//                var indexPathsToDeleteArray = [NSIndexPath]()
//                indexPathsToDeleteArray.append(indexPath)
//                self.tableView.beginUpdates()
//                //UITableViewRowAnimation
//                self.tableView.deleteRowsAtIndexPaths(indexPathsToDeleteArray, withRowAnimation:.Left)
//                self.tableView.endUpdates()
                
                self.reloadDataAndTable()
                //------------------------------------------------
            }else{
                self.log.error("self.arrayRealmPlaces is nil")
            }
            
        case .insert:
            print("editingStyle:Insert forRowAtIndexPath:\((indexPath as NSIndexPath).row)")
        }
    }
}
