//
//  PlacesViewController.swift
//  TestRealDB
//
//  Created by Brian Clear on 18/02/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift



class PlacesViewController: ParentViewController, UITableViewDataSource, UITableViewDelegate, AddPlaceViewControllerDelegate, EditPlaceViewControllerDelegate  {

    @IBOutlet weak var tableView: UITableView!
    
    let realm = try! Realm()
    lazy var places: Results<RealmPlace> = {
        self.realm.objects(RealmPlace.self)
    }()
    
    //EDIT
    var selectedPlaceToEdit : RealmPlace?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.log.info("places.count:\(places.count)")
        //------------------------------------------------------------------------------------------------
        //Add Edit button to Nav bar
        //NOT NEEDED - if canEditRowAtIndexPath returns true then user can swipe to Delete
//        let barButtonItem_Edit = UIBarButtonItem(title: "Edit", style: .Plain, target: self, action:"buttonEdit_Action:")
//        self.navigationItem.rightBarButtonItem = barButtonItem_Edit
        //------------------------------------------------------------------------------------------------

        reloadDataAndTable()
    }
    //not needed
    func buttonEdit_Action(_ sender: UIBarButtonItem) {
        let editingNew = !self.tableView.isEditing
        self.tableView.setEditing(editingNew, animated: true)
    }
    
    
    //------------------------------------------------------------------------------------------------
    //TABLE
    //------------------------------------------------------------------------------------------------

    
    func reloadDataAndTable(){
        places = self.realm.objects(RealmPlace.self)
        self.tableView.reloadData()
    }
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - UITableViewDataSource
    // MARK: -
    //--------------------------------------------------------------
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return places.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cellIdentifier = "PlacesTableViewCell"
        
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as UITableViewCell!
        
        if cell == nil {
            cell = UITableViewCell(style:.default, reuseIdentifier:cellIdentifier)
        }
        
        let realmPlace : RealmPlace = places[indexPath.row]
        
        cell?.textLabel!.text = realmPlace.name
        
        let clkPlaceWrapperType: CLKPlaceWrapperType = CLKPlaceWrapperType.clkPlaceWrapperTypeForRaw(realmPlace.clkPlaceWrapperTypeId)
        cell?.detailTextLabel!.text = clkPlaceWrapperType.nameReadable()
        
        return cell!
    }
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - didSelectRowAtIndexPath
    // MARK: -
    //--------------------------------------------------------------
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        self.selectedPlaceToEdit = places[indexPath.row]
        
        self.log.info("didSelectRowAtIndexPath:ROW:\((indexPath as NSIndexPath).row) Place:\(self.selectedPlaceToEdit?.name)")
        
        performSegue(withIdentifier: kseguePlacesTOEditPlaceViewController, sender: nil)
    }
    
    
    
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - EDIT - UITableViewDataSource - funcs related to editing/move/delete
    //--------------------------------------------------------------
    //Note methds to control EDITING are in both UITableViewDataSource/UITableViewDelegate
    
    //THIS SHOWS/HIDES Delete button
    //CONTROL EDITING FOR 1 or more rows
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool{
        
        //Called for EVERY ROW when table reloaded
        //default is to allow EDITING on all rows
        
        
        //---------------------------------------------------------------------
        //    var canEditRow = true
        //
        //    //DISABLE EDITING for first row
        //    //Tapping EDIT will not show Edit controls for the first row
        //    if indexPath.row == 0 {
        //        canEditRow = false
        //    }
        //    return canEditRow
        //---------------------------------------------------------------------
        //ALL ROWS DELETABLE
        return true
    }
    
    //------------------------------------------------------------------------------------------------
    //When user hits edit then DELETE on the row tableView(_:commitEditingStyle:forRowAtIndexPath:) called
    //------------------------------------------------------------------------------------------------
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath){
        
        switch editingStyle{
        case .none:
            self.log.info("editingStyle:None forRowAtIndexPath:\((indexPath as NSIndexPath).row)")
        case .delete:
            //---------------------------------------------------------------------
            //DELETE
            //---------------------------------------------------------------------
            self.log.info("editingStyle:Delete forRowAtIndexPath:\((indexPath as NSIndexPath).row)")
            //---------------------------------------------------------------------
            //DELETE INSTANTLY
            //OK
            //tableData.removeAtIndex(indexPath.row)
            //tableView.reloadData()
            
            //---------------------------------------------------------------------
            //DELETE WITH ANIMATION
            //Remove from backing data - data source should be edited first before editing table with animation as table is reloaded and DataSource delegates are called again
            //tableData.removeAtIndex(indexPath.row)
            
            let place = places[indexPath.row]
            
            appDelegate.colcDBManager.realmDBManager.delete(place){
                (resultType: RealmDBResultType) -> Void in
                
                //-----------------------------------------------------------
                switch resultType {
                case .success:
                    //------------------------
                    //SUCCESS
                    //------------------------
                    
                    self.log.info("place deleted ok")
                    
                    var indexPathsToDeleteArray = [NSIndexPath]()
                    indexPathsToDeleteArray.append(indexPath as NSIndexPath)
                    
                    tableView.beginUpdates()
                    //UITableViewRowAnimation
                    //SWIFT3
                    tableView.deleteRows(at: indexPathsToDeleteArray as [IndexPath], with:.automatic)
                    tableView.endUpdates()
                    
                    //-------------------------
                case .error(let errorType):
                    //-------------------------
                    //ERROR
                    //-------------------------
                    self.log.error("\(errorType)")
                }
                //-----------------------------------------------------------
            }
            //---------------------------------------------------------------------
        case .insert:
            self.log.info("editingStyle:Insert forRowAtIndexPath:\((indexPath as NSIndexPath).row)")
        }
    }
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - ADD NEW TRIP
    //--------------------------------------------------------------
    let kseguePlacesTOAddPlaceViewController = "seguePlacesTOAddPlaceViewController"
    let kseguePlacesTOEditPlaceViewController = "seguePlacesTOEditPlaceViewController"
    //---------------------------------------------------------------------
    @IBAction func buttonAdd_Action(_ sender: AnyObject) {
        performSegue(withIdentifier: kseguePlacesTOAddPlaceViewController, sender: nil)
    }
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - AddPlaceViewControllerDelegate
    // MARK: -
    //--------------------------------------------------------------
    
    func didAddItem(_ controller: AddPlaceViewController){
        reloadDataAndTable()
    }
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - EditPlaceViewControllerDelegate
    // MARK: -
    //--------------------------------------------------------------
    func didEditItem(_ controller: EditPlaceViewController){
        reloadDataAndTable()
    }
    
    //------------------------------------------------------------------------------------------------
    //SEGUE
    //------------------------------------------------------------------------------------------------

    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - prepareForSegue
    //--------------------------------------------------------------
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        if(segue.identifier == kseguePlacesTOAddPlaceViewController){
            
            if(segue.destination.isMember(of: AddPlaceViewController.self)){
                let addPlaceViewController = (segue.destination as! AddPlaceViewController)
                addPlaceViewController.delegate = self
                
            }else{
                self.log.error("not AddPlaceViewController")
            }
        }
        else if(segue.identifier == kseguePlacesTOEditPlaceViewController){
            
            if(segue.destination.isMember(of: EditPlaceViewController.self)){
                let editPlaceViewController = (segue.destination as! EditPlaceViewController)
                editPlaceViewController.delegate = self
                editPlaceViewController.selectedPlaceToEdit = self.selectedPlaceToEdit
                
            }else{
                self.log.error("not EditPlaceViewController")
            }
        }
        else{
            self.log.info("UNHANDLED SEGUE:\(segue.identifier)")
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
