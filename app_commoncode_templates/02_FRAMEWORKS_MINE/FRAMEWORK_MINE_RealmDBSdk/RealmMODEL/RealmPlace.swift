//
//  Place.swift
//  TestRealDB
//
//  Created by Brian Clear on 18/02/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import RealmSwift

class RealmPlace : Object {
    
    dynamic var name = ""
    
    //business address OR geocode address
    dynamic var address = ""
    
    //geocode address in case business address crap
    dynamic var addressNearest = ""
    
    dynamic var locality = "" //needed by uber
    dynamic var place_id = ""
    dynamic var latitude = 0.0
    dynamic var longitude = 0.0
    
    dynamic var timestampCreated = NSDate()
    
    dynamic var clkPlaceWrapperTypeId = 0 //CLKPlaceWrapperType.rawValue
    
    
}

