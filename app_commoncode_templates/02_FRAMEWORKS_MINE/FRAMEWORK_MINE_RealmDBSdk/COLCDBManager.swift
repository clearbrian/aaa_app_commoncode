//
//  DBManager.swift
//  joyride
//
//  Created by Brian Clear on 01/03/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import Realm

enum DBResultType {
    case success
    case error(errorType: Error)
}

enum DBError: Error {
    case unexpectedTypeReturned(expectedType:String, returnedType:String)
    case errorThrownByDB(errorString:String)
    case errorCreatingRealmPlace()
}

class COLCDBManager {
    //RealmDB
    let realmDBManager = RealmDBManager()

    let log = MyXCodeEmojiLogger.defaultInstance
    
    func saveCLKPlace(_ clkPlace : CLKPlace,
        completion: @escaping (_ dbResultType: DBResultType) -> Void){
        //---------------------------------------------------------------------
        if let realmPlace = clkPlace.realmPlace{
            
            realmDBManager.save(realmPlace){
                (resultType: RealmDBResultType) -> Void in
                
                //-----------------------------------------------------------
                switch resultType {
                case .success:
                    //------------------------------------------------------------------------------------------------
                    //SUCCESS
                    //------------------------------------------------------------------------------------------------
                    
                    self.log.info("place saved ok")
                    
                    //                if let navigationController = self.navigationController {
                    //                    navigationController.popViewControllerAnimated(true)
                    //                    self.tellDelegateItemAdded()
                    //
                    //                }else{
                    //                    //view was presented not pushed
                    //                    self.dismissViewControllerAnimated(true, completion: {
                    //                        self.tellDelegateItemAdded()
                    //                    })
                    //                }
                    //---------------------------------------------------------------------
                    
                    completion( DBResultType.success)
                    
                case .error(let errorType):
                    //------------------------------------------------------------------------------------------------
                    //ERROR
                    //------------------------------------------------------------------------------------------------
                    self.log.error("\(errorType)")
                    
                    //SWIFT3
                    let dbResultType = DBResultType.error(errorType: errorType as Error)
                    completion( dbResultType)
                }
                //-----------------------------------------------------------
            }
        }else{
            self.log.error("clkPlace.realmPlace returned nil")
            
            let dbResultType = DBResultType.error(errorType: DBError.errorCreatingRealmPlace())
            completion( dbResultType)
        }
    }
    
    //Check if place is already in DB
    func findRealmPlaceInDb(_ realmPlace: RealmPlace) -> RealmPlace?{
        //if we return nil here then we must add trip to db
        var realmPlaceReturned: RealmPlace?
        

        //---------------------------------------------------------------------
        //v1 filter cant handle dingle quotes in addres
//        var filterString = "name = '\(realmPlace.name)' "
//        filterString = filterString + "AND address = '\(realmPlace.address)' "
//        filterString = filterString + "AND locality = '\(realmPlace.locality)' "
//        filterString = filterString + "AND place_id = '\(realmPlace.place_id)' "
//        filterString = filterString + "AND clkPlaceWrapperTypeId = \(realmPlace.clkPlaceWrapperTypeId) "
//        
//        //------------------------------------------------
//        //remove lat, lng - was too unique
//        //------------------------------------------------
//        //exception sting expected double - remove quotes
//        
//        //            filterString = filterString + "AND latitude = '\(realmPlaceStart.latitude)' "
//        //            filterString = filterString + "AND longitude = '\(realmPlaceStart.longitude)' "
//        
//        //            filterString = filterString + "AND latitude = \(realmPlaceStart.latitude) "
//        //            filterString = filterString + "AND longitude = \(realmPlaceStart.longitude) "
//        //-----------------------------------------------------------------------------------
//        //BUG:  '8-14 Cooper's Row, London EC3N 2BQ, UK' middle ' causing exception
//        self.log.info("filterString:\r \(filterString)")
//        filterString = filterString.stringByReplacingOccurrencesOfString("'", withString: "\\'")
//        self.log.info("filterString:\r \(filterString)")
        
        
        //---------------------------------------------------------------------
        //BUG 32-33 Cooper's Row  >> address = '32-33 Cooper\'s Row' "
        //need to insert \' for the address only not the whole filter
        //CORRECT: address = '32-33 Cooper\'s Row' 
        //WRONG    address = \'32-33 Cooper\'s Row\'
        //---------------------------------------------------------------------
        let escaped_name = realmPlace.name.replacingOccurrences(of: "'", with: "\\'")
        let escaped_address = realmPlace.address.replacingOccurrences(of: "'", with: "\\'")
        let escaped_locality = realmPlace.locality.replacingOccurrences(of: "'", with: "\\'")
        
        var filterString = "name = '\(escaped_name)' "
        filterString = filterString + "AND address = '\(escaped_address)' "
        filterString = filterString + "AND locality = '\(escaped_locality)' "
        filterString = filterString + "AND place_id = '\(realmPlace.place_id)' "
        filterString = filterString + "AND clkPlaceWrapperTypeId = \(realmPlace.clkPlaceWrapperTypeId) "
        self.log.info("filterString:\r \(filterString)")
        
        //-----------------------------------------------------------------------------------
        //                do {
        //                    //Update - just set Objects properties while in write block
        //                    try realm.write {
        //                        place.name = self.textFieldName.text!
        //                        place.address = self.textViewAddress.text!
        //                        place.placeId = self.textFieldPlaceId.text!
        //                        place.latitude =  (self.textFieldLat.text! as NSString).doubleValue
        //                        place.longitude = (self.textFieldLng.text! as NSString).doubleValue
        //                    }
        //                    self.log.info("commitWrite OK")
        //                    completion(resultType: RealmDBResultType.Success)
        //                }
        //                catch let nserror as NSError {
        //                    dumpRealmNSError(nserror)
        //
        //                    // see extension NSError below
        //                    let errorType = nserror.errorType
        //                    let resultType = RealmDBResultType.Error(errorType: errorType)
        //
        //                    completion(resultType: resultType)
        //                }
        
        //-----------------------------------------------------------------------------------
        let filteredPlaces = realmDBManager.realm.objects(RealmPlace.self).filter(filterString)
        self.log.info("filteredPlaces:\(filteredPlaces.count)")
        
        if filteredPlaces.count == 0{
            self.log.info("NOT FOUND IN DB - use new place - name = '\(realmPlace.name)'")
            //return nil so we know we must add it
            realmPlaceReturned = nil
            
        }else if filteredPlaces.count == 1{
            //exact item found in db (not includeding lat/lng) - use this instead
            
            //note we dont check lat,lng so should replace the found PLACE.lat PLACE.lng with new lat/lng
            //or check the distance
            
            realmPlaceReturned = filteredPlaces[0]
            
        }else{
            //count > 1
            print("")
            self.log.info("\(filteredPlaces.count) FOUND IN DB - use FIRST place - name = '\(realmPlace.name)'")
            realmPlaceReturned = filteredPlaces[0]
        }
        
        return realmPlaceReturned

    }
    
    
    func findInDB_RealmTrip(_ realmTrip: RealmTrip) -> RealmTrip?{
        //if we return nil here then we must add trip to db
        var realmTripReturned: RealmTrip?
        
        let filterString = "name = '\(realmTrip.name)' "
        //        filterString = filterString + "AND address = '\(realmTrip.address)' "
        
        //        filterString = filterString + "AND address = '\(realmTrip.address)' "
        //        filterString = filterString + "AND locality = '\(realmTrip.locality)' "
        //        filterString = filterString + "AND place_id = '\(realmTrip.place_id)' "
        //        filterString = filterString + "AND clkTripWrapperTypeId = \(realmTrip.clkTripWrapperTypeId) "
        
        //------------------------------------------------
        //remove lat, lng - was too unique
        //------------------------------------------------
        //exception sting expected double - remove quotes
        
        //            filterString = filterString + "AND latitude = '\(realmTripStart.latitude)' "
        //            filterString = filterString + "AND longitude = '\(realmTripStart.longitude)' "
        
        //            filterString = filterString + "AND latitude = \(realmTripStart.latitude) "
        //            filterString = filterString + "AND longitude = \(realmTripStart.longitude) "
        //-----------------------------------------------------------------------------------
        self.log.info("findInDB_RealmTrip: filterString:\r \(filterString)")
        //-----------------------------------------------------------------------------------
        //                do {
        //                    //Update - just set Objects properties while in write block
        //                    try realm.write {
        //                        place.name = self.textFieldName.text!
        //                        place.address = self.textViewAddress.text!
        //                        place.placeId = self.textFieldTripId.text!
        //                        place.latitude =  (self.textFieldLat.text! as NSString).doubleValue
        //                        place.longitude = (self.textFieldLng.text! as NSString).doubleValue
        //                    }
        //                    self.log.info("commitWrite OK")
        //                    completion(resultType: RealmDBResultType.Success)
        //                }
        //                catch let nserror as NSError {
        //                    dumpRealmNSError(nserror)
        //
        //                    // see extension NSError below
        //                    let errorType = nserror.errorType
        //                    let resultType = RealmDBResultType.Error(errorType: errorType)
        //
        //                    completion(resultType: resultType)
        //                }
        
        //-----------------------------------------------------------------------------------
        let filteredTrips = realmDBManager.realm.objects(RealmTrip.self).filter(filterString)
        self.log.info("filteredTrips:\(filteredTrips.count)")
        
        if filteredTrips.count == 0{
            self.log.info("RealmTrip NOT FOUND IN DB - use new place - name = '\(realmTrip.name)'")
            //return nil so we know we must add it
            realmTripReturned = nil
            
        }else if filteredTrips.count == 1{
            //exact item found in db (not includeding lat/lng) - use this instead
            
            //note we dont check lat,lng so should replace the found PLACE.lat PLACE.lng with new lat/lng
            //or check the distance
            
            realmTripReturned = filteredTrips[0]
            
        }else{
            //count > 1
            print("")
            self.log.info("\(filteredTrips.count) FOUND IN DB - use FIRST place - name = '\(realmTrip.name)'")
            realmTripReturned = filteredTrips[0]
        }
        
        return realmTripReturned
        
    }
    
    func saveCLKTrip(_ clkTrip : CLKTrip,
        completion: @escaping (_ dbResultType: DBResultType) -> Void){
            
        //RealmDBObject
        let realmTrip = RealmTrip()
        
        //------------------------------------------------
        // TODO: - let user add trip name
        //            if let name = clkTrip. {
        //                realmTrip.name = "TRIP NAME"
        //            }else{
        //                self.log.error("clkTrip.name is nil")
        //            }
        //-----------------------------------------------------------------------------------
        realmTrip.name = clkTrip.name
        realmTrip.openWithTypeId = clkTrip.openWithTypeId
        realmTrip.dateTime = clkTrip.dateTime

        //overridden getter
        realmTrip.realmPlaceStart = clkTrip.clkPlaceStart.realmPlace
        realmTrip.realmPlaceDestination = clkTrip.clkPlaceEnd.realmPlace
        
        //-----------------------------------------------------------------------------------
        //Check place not in DB already
        //-----------------------------------------------------------------------------------
        //find in DB
        //if found use the DB instance
        //if not there then use new one
        
//        var mustSaveToDb_Start = false
//        var mustSaveToDb_Destination = false
        
        if let realmPlaceStart = realmTrip.realmPlaceStart {
            if let realmPlaceInDB_Start = findRealmPlaceInDb(realmPlaceStart){
                //mustSaveToDb_Start = false
                realmTrip.realmPlaceStart = realmPlaceInDB_Start
                
            }else{
                self.log.error("findRealmPlaceInDb(realmPlaceStart) not in DB must add it")
                //mustSaveToDb_Start = true
            }
        }else{
            self.log.error("realmTrip.realmPlaceStart is nil")
        }
        //-----------------------------------------------------------------------------------

        if let realmPlaceDestination = realmTrip.realmPlaceDestination {
            if let realmPlaceInDB_Destination = findRealmPlaceInDb(realmPlaceDestination){
                //mustSaveToDb_Destination = false
                realmTrip.realmPlaceDestination = realmPlaceInDB_Destination
                
            }else{
                self.log.error("findRealmPlaceInDb(realmPlaceDestination) not in DB must add it")
                //mustSaveToDb_Destination = true
            }
        }else{
            self.log.error("realmTrip.realmPlaceDestination is nil")
        }
        //-----------------------------------------------------------------------------------

        
        realmDBManager.save(realmTrip){
            (resultType: RealmDBResultType) -> Void in
            
            //-----------------------------------------------------------
            switch resultType {
            case .success:
                //------------------------------------------------------------------------------------------------
                //SUCCESS
                //------------------------------------------------------------------------------------------------
                
                self.log.info("trip saved ok")
                
                completion(DBResultType.success)
                
            case .error(let errorType):
                //------------------------------------------------------------------------------------------------
                //ERROR
                //------------------------------------------------------------------------------------------------
                self.log.error("\(errorType)")
                //SWIFT 3
                let dbResultType = DBResultType.error(errorType: errorType as Error)
                
                completion(dbResultType)
            }
            //-----------------------------------------------------------
        }

    }
    
    //start and end are not in db so save start/end as trip
    func saveRealmTripToDB(_ realmTrip: RealmTrip){
        
        
    }
}
