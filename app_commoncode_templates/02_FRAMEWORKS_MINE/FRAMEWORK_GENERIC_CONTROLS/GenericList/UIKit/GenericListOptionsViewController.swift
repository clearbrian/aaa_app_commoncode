//
//  GenericListOptionsViewController.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 17/03/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
import UIKit


protocol GenericListOptionsViewControllerDelegate {
    func genericListOptionsViewController(genericListOptionsViewController: GenericListOptionsViewController, didSelectRowAt indexPath: IndexPath)
}

class GenericListOptionsViewController : ParentViewController, UITableViewDelegate, UITableViewDataSource  {
    
    var delegate: GenericListOptionsViewControllerDelegate?
    
    @IBOutlet weak var tableView: UITableView!
    
    var selectedIndexPath : IndexPath?
    
    //--------------------------------------------------------------
    // MARK: - dictionaryTableData
    // MARK: -
    //--------------------------------------------------------------
    
    //Any is String
   
    var dictionaryTableData: [String:[String]] = [
        "Section 0":["Row 0.0", "Row 0.1"],
        "Section 1":["Row 1.0", "Row 1.1"],
        "Section 2":["Row 2.0", "Row 2.2"]
    ]

    
    //--------------------------------------------------------------
    // MARK: - viewDidLoad
    // MARK: -
    //--------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureNavBar()
        
    }
    
    //--------------------------------------------------------------
    // MARK: - KEYS
    // MARK: -
    //--------------------------------------------------------------
    var keysArrayUnsorted : Array<String>{
        let keysArrayUnsorted = Array(dictionaryTableData.keys)
        return keysArrayUnsorted
    }
    
    //needed because dictionaryTableData.keys.array is not sorted alphabetically by insertion order
    var keysArraySorted : Array<String>{
        let keysArraySorted = self.keysArrayUnsorted.sorted { (string0, string1) -> Bool in
            if string0.compare(string1, options: .numeric) == .orderedAscending{
                return true
                
            }else{
                return false
            }
        }
        return keysArraySorted
    }
    
    //------------------------------------------------
    // MARK: retrieveSortedKeyForSection
    //------------------------------------------------
    //Sort the keys array before accessing them by section
    //must be done everywhere we get key_section
    func retrieveSortedKeyForSection(_ section: Int) ->String{
        //String
        let key_section : String = self.keysArraySorted[section]
        return key_section
    }
    
    
    
    //--------------------------------------------------------------
    // MARK: - SECTIONS
    // MARK: -
    //--------------------------------------------------------------
    public func numberOfSections(in tableView: UITableView) -> Int {
        let numberOfSections_ = self.keysArrayUnsorted.count
        return numberOfSections_
    }
    
    
    //section header - nil to hide if ungrouped
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?{
        
        //---------------------------------------------------------
        //KEY in dictionaryTableData_Grouped in subclass
        //---------------------------------------------------------
        let key_section = self.retrieveSortedKeyForSection(section)
        return key_section
    }
    
    //--------------------------------------------------------------
    // MARK: - ROWS
    // MARK: -
    //--------------------------------------------------------------
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        var numberOfRowsInSection_ = 0
        
        let key_section = retrieveSortedKeyForSection(section)
        
        if let rowDataArray_ = dictionaryTableData[key_section] {
            numberOfRowsInSection_ = rowDataArray_.count
        }else{
            appDelegate.log.error("111 dictionaryTableData[key_section:\(key_section)] is nil or not Array<String> ")
        }
        
        return numberOfRowsInSection_
    }
    
    
    func rowDataArrayForSection (_ section: Int) -> Array<Any>{
        
        var rowDataArray_ : Array<Any> = []
        
        let key_section = retrieveSortedKeyForSection(section)
        
        //Array
        if let rowDataArray = dictionaryTableData[key_section]{
            rowDataArray_ = rowDataArray
            
        }else{
            appDelegate.log.error("222 dictionaryTableData[key_section:\(key_section)] is nil")
        }
        return rowDataArray_
    }


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        
        let cellIdentifier = "GenericListOptionsTableViewCell"
        
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as UITableViewCell!
        if cell == nil {
            cell = UITableViewCell(style:.default, reuseIdentifier:cellIdentifier)
        }
        
        let rowDataArrayForSection = self.rowDataArrayForSection(indexPath.section)
        
        if let rowText = rowDataArrayForSection[indexPath.row] as? String{
            cell?.textLabel!.text = rowText
            
        }else{
            cell?.textLabel!.text = "ERROR"
        }
        
        return cell!
    }
    
    //------------------------------------------------
    // MARK: UITableViewDelegate
    //------------------------------------------------
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        var rowText_ = ""
        
        let rowDataArrayForSection = self.rowDataArrayForSection(indexPath.section)
        
        if let rowText = rowDataArrayForSection[indexPath.row] as? String{
            rowText_ = rowText
            
        }else{
            rowText_ = "ERROR"
        }
        //moved to save
//        if let delegate = self.delegate{
//            
//            //------------------------------------------------
//            //errorIN may be nil
//            delegate.genericListOptionsViewController(genericListOptionsViewController: self, didSelectRowAt: indexPath)
//            //------------------------------------------------
//            
//        }else{
//            appDelegate.log.error("delegate is nil")
//        }
        
        
        print("didSelectRowAtIndexPath:ROW:\(indexPath.row) VALUE:\(rowText_)")
        
        
        self.selectedIndexPath = indexPath
        
     
        
    }
    
    
    //--------------------------------------------------------------
    // MARK: - Navigation Bar
    // MARK: -
    //--------------------------------------------------------------
    //SUBLCASS
    func configureNavBar() {
        //----------------------------------------------------------------------------------------
        //Menu
        //----------------------------------------------------------------------------------------
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back",
                                                           style: .plain,
                                                           target: self,
                                                           action: #selector(GenericListOptionsViewController.leftBarButtonItem_Action))
        //----------------------------------------------------------------------------------------
        //Map
        //----------------------------------------------------------------------------------------
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save",
                                                            style: .plain,
                                                            target: self,
                                                            action: #selector(GenericListOptionsViewController.rightBarButtonItem_Action))
        //----------------------------------------------------------------------------------------
    }
    
    func leftBarButtonItem_Action() {
        self.dismiss(animated: true, completion: {
            
        })
        
    }
    
    func rightBarButtonItem_Action() {
        self.dismiss(animated: true, completion: {
            
            //------------------------------------------------
            if let selectedIndexPath = self.selectedIndexPath {
                if let delegate = self.delegate{
                    
                    //------------------------------------------------
                    //errorIN may be nil
                    delegate.genericListOptionsViewController(genericListOptionsViewController: self, didSelectRowAt: selectedIndexPath)
                    //------------------------------------------------
                    
                }else{
                    appDelegate.log.error("delegate is nil")
                }
            }else{
                appDelegate.log.error("self.selectedIndexPath is nil")
            }
            //------------------------------------------------
        })
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
