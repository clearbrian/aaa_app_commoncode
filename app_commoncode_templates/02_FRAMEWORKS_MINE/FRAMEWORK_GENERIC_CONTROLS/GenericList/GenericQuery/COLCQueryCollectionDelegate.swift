//
//  COLCQueryCollectionDelegate.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 15/03/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

//--------------------------------------------------------------
// MARK: - COLCQueryCollection
// MARK: -
//--------------------------------------------------------------
protocol COLCQueryCollectionDelegate {
    func colcQuery_HasChanged(colcQuery: COLCQuery, inCOLCQueryCollection: COLCQueryCollection)
}
