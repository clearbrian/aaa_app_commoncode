//
//  COLCSort.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 15/03/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

/*
 
 /Users/gbxc/Documents/00_ME/00_BITBUCKET_COLC/app_apptemplatesandnotes/00_CODE/AppWithCodeTemplates_iOS_Swift3/PLAYGROUNDS/Sort_Object_MultipleProperties.playground

 */

class COLCSort{
    
    //shown in SORT picker
    var title: String
    
    //Name of property to sort on
    var propertyName: String
    
    var isSorted = true     //False = COLCSortNone
    var isAscending = true  //False = SORT DESC
    var isCaseSensitive = true //False = SORT DESC
    
    
    
    //only used in COLCSortByString - done in COLCSort cos it knows the param name commonName etc
    func arrayFirstLetters(sortedArray: [COLCQueryable]) -> [String]?{
        //SUBCLASS TO ADD
        return nil
    }
    
    //----------------------------------------------------------------------------------------
    //init
    //----------------------------------------------------------------------------------------
    init(title           : String,
         propertyName    : String,
         isSorted        : Bool,
         isAscending     : Bool,
         isCaseSensitive : Bool)
    {
        self.title = title
        self.propertyName = propertyName
        self.isSorted = isSorted
        self.isAscending = isAscending
        self.isCaseSensitive = isCaseSensitive
    }

    convenience init(title           : String,
                     propertyName    : String)
    {
        self.init(title:title, propertyName: propertyName, isSorted: true, isAscending: true, isCaseSensitive: false)
    }

    //--------------------------------------------------------------
    // MARK: - sortResults
    // MARK: -
    //--------------------------------------------------------------
    ////protocol COLCQueryable : COLCSortable, CoreLocatable
    func sortResults(arrayToSort: [COLCQueryable]) -> [COLCQueryable]{

        let arraySorted = arrayToSort
        appDelegate.log.error("sortResults - UNSORTED - use subclass to define type of sort: default sort should be SortByCommonName")
        return arraySorted
    }
    
    func sortResults(arrayToSort: [COLCQueryable], filterByFirstLetter firstLetter: String) -> [COLCQueryable]{
        
        appDelegate.log.error("arrayToSort: filterByFirstLetter - should only be in COLCSortByString")
        return [COLCQueryable]()
    }
}





// TODO: - 16mar

//    write sortByName
//    or sort by propertyName
//
//    subclass
//
//    COLCSortByStringProperty(propName) > TFLPlace. stringFor(proName) > %0.propString
//    COLCSortByBoolProperty
//COLCSortByIntProperty
//    COLCSortByIntProperty

//colcQuery_isAvailable default sort should be sort by name
//how doe we sort by name/address
//or group by a..z or group by borough

/*
 OPTIONS TABLEVC MODAL - diff sections
 
 SEARCH
 NEAREST
 LOCATION
 IS AVAIL
 
 
 SORT
 A..z   tap row >> z..A
 number of spaces
 
 GROUP BY
 Borough
 
 Type : Rest/Rank
 
 // TODO: - COLCSortSting? - also sort by first letter
 
 */
