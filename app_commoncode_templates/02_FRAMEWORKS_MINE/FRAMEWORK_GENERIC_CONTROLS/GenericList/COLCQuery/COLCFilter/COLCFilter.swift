//
//  COLCFilter.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 15/03/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

//--------------------------------------------------------------
// MARK: - COLCFilter
// MARK: -
//--------------------------------------------------------------
//cant handle AND / OR for multiple filters
//for AND set ALL to required
//CustomStringConvertible


class COLCFilter: COLCFilterOrGroupParent, CustomStringConvertible{
    var propertyName : String
    
    //the value you filter against is in sub class
    //var searchString: String >> COLCFilterString
    //var searchBool: Bool     >> COLCFilerBool
    
    init(propertyName: String) {
        self.propertyName = propertyName
        
        super.init()
    }
    
    func filterMatch(colcQueryable: COLCQueryable) -> Bool{
        appDelegate.log.error("SUBCLASS COLCFilter - filterMatch")
        
        //----------------------------------------------------------------------------------------
        //SAMPLE CODE for subclass
        //----------------------------------------------------------------------------------------
        //if let value = colcQueryable.value(forPropertyName: self.propertyName) {
        //
        //}else{
        //    appDelegate.log.error("olcQueryable.value(forPropertyName: colcFilter.propertyName\(propertyName) is nil")
        //}
        
        //comparison value varied
        //if let valueString = colcQueryable.value(forPropertyName: self.propertyName) as? Bool {}
        //if let valueString = colcQueryable.value(forPropertyName: self.propertyName) as? String {}
        
        return false
    }
    
    var description: String {
        var description_ = "**** \(type(of: self)) *****"
        description_ = description_ + "propertyName:\(propertyName)]"
        return description_
    }
}

// TODO: - Filter by CLLocation AND Radius
