//
//  COLCFilterGroupType.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 15/03/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

enum COLCFilterGroupType{
    case and
    case or
}
