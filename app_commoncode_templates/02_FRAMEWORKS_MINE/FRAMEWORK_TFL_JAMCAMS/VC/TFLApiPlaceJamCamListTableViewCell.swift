//
//  TFLApiPlaceJamCamListTableViewCell.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 13/01/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
import UIKit

class TFLApiPlaceJamCamListTableViewCell : UITableViewCell{
    
    @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var labelDetail0: UILabel!
    @IBOutlet weak var labelDetail1: UILabel!
    
    @IBOutlet weak var labelSubDetail0: UILabel!
    @IBOutlet weak var labelSubDetail1: UILabel!
    
    
    var customFontApplied = false
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //print("PlacesTableViewCell init coder")
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        super.init(style: UITableViewCellStyle.subtitle, reuseIdentifier: reuseIdentifier)
        
    }
    
    func setup(){
        
    }
    
    override func draw(_ rect: CGRect) {
        
        //print("drawRect")
        //didnt work here only for cells off screen
        //applyCustomFont()
    }
    
    func applyCustomFont(){
        if customFontApplied{
            //cell created already - BODY is replace with customfont - if we reuse it and call this again it will try and convert font again
        }else{
            self.labelTitle?.applyCustomFontForCurrentTextStyle()
            self.labelDetail0?.applyCustomFontForCurrentTextStyle()
            self.labelDetail1?.applyCustomFontForCurrentTextStyle()
            
            self.labelSubDetail0?.applyCustomFontForCurrentTextStyle()
            self.labelSubDetail1?.applyCustomFontForCurrentTextStyle()

            customFontApplied = true
        }
    }
}
