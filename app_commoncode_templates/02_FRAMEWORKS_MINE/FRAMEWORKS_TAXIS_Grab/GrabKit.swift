//
//  GrabKit.swift
//  joyride
//
//  Created by Brian Clear on 16/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation

class OpenAppConfigGrab: OpenAppConfig{
    
}

/*
 https://developer.grab.com/docs/deeplinking
 
 https://docs.google.com/document/d/1Gn3Fo5-jWkGZumJLlEgK1nAizxCCMm5T0X4qTF7yeLE/edit
 //make sure you add this to Info.plist LSApplicationQueriesSchemes
 <key>CFBundleURLSchemes</key>
 <array>
 <string>grabtaxi</string>
 </array>
 */

class GrabKit : ParentSwiftObject{
    //https://geo.itunes.apple.com/en/app/id647268330?mt=8
    //https://itunes.apple.com/gb/app/grab-grabtaxi/id647268330?mt=8
    //-----------------------------------------------------------------------------------
    static let openAppConfig = OpenAppConfigClipboard(openWithType : OpenWithType.openWithType_Grab,
                                      appSchemeString     : "grabtaxi://",
                                      appWebsiteURLString : "http://www.grab.com/",
                                      appStoreIdUInt      : 647268330)
    //-----------------------------------------------------------------------------------
    
}
