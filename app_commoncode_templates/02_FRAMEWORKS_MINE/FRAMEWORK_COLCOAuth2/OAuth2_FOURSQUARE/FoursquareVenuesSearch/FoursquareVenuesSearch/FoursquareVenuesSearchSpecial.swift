//
//	FoursquareVenuesSearchSpecial.swift
//
//	Create by Brian Clear on 19/7/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class FoursquareVenuesSearchSpecial : ParentMappable{

	var count : Int?
	var items : [String]?



	override func mapping(map: Map)
	{
		count <- map["count"]
		items <- map["items"]
		
	}
	var description: String
	{
		var description_ = "**** \(type(of: self)) *****\r"
		description_ = description_ + "count: \(count)\r"
		description_ = description_ + "items: \(items)\r"
		return description_
	}

}
