//
//	FoursquareVenuesSearchVenue.swift
//
//	Create by Brian Clear on 19/7/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class FoursquareVenuesSearchVenue : ParentMappable{

	var allowMenuUrlEdit : Bool?
	var categories : [FoursquareVenuesSearchCategory]?
	var contact : FoursquareVenuesSearchContact?
	var hasMenu : Bool?
	var hasPerk : Bool?
	var hereNow : FoursquareVenuesSearchHereNow?
	var id : String?
	var location : FoursquareVenuesSearchLocation?
	var menu : FoursquareVenuesSearchMenu?
	var name : String?
	var referralId : String?
	var specials : FoursquareVenuesSearchSpecial?
	var stats : FoursquareVenuesSearchStat?
	var storeId : String?
	var url : String?
	var venueChains : [String]?
	var verified : Bool?



	override func mapping(map: Map)
	{
		allowMenuUrlEdit <- map["allowMenuUrlEdit"]
		categories <- map["categories"]
		contact <- map["contact"]
		hasMenu <- map["hasMenu"]
		hasPerk <- map["hasPerk"]
		hereNow <- map["hereNow"]
		id <- map["id"]
		location <- map["location"]
		menu <- map["menu"]
		name <- map["name"]
		referralId <- map["referralId"]
		specials <- map["specials"]
		stats <- map["stats"]
		storeId <- map["storeId"]
		url <- map["url"]
		venueChains <- map["venueChains"]
		verified <- map["verified"]
		
	}
	var description: String
	{
		var description_ = "**** \(type(of: self)) *****\r"
		description_ = description_ + "allowMenuUrlEdit: \(allowMenuUrlEdit)\r"
		description_ = description_ + "categories: \(categories)\r"
		description_ = description_ + "contact: \(contact)\r"
		description_ = description_ + "hasMenu: \(hasMenu)\r"
		description_ = description_ + "hasPerk: \(hasPerk)\r"
		description_ = description_ + "hereNow: \(hereNow)\r"
		description_ = description_ + "id: \(id)\r"
		description_ = description_ + "location: \(location)\r"
		description_ = description_ + "menu: \(menu)\r"
		description_ = description_ + "name: \(name)\r"
		description_ = description_ + "referralId: \(referralId)\r"
		description_ = description_ + "specials: \(specials)\r"
		description_ = description_ + "stats: \(stats)\r"
		description_ = description_ + "storeId: \(storeId)\r"
		description_ = description_ + "url: \(url)\r"
		description_ = description_ + "venueChains: \(venueChains)\r"
		description_ = description_ + "verified: \(verified)\r"
		return description_
	}

}
