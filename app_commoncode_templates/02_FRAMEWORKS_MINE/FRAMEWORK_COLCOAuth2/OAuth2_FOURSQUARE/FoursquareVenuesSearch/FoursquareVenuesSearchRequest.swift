//
//  FoursquareVenuesSearchRequest.swift
//  joyride
//
//  Created by Brian Clear on 19/07/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation

// https://developer.foursquare.com/docs/venues/search


class FoursquareVenuesSearchRequest : FoursquareAccessTokenGetRequest{
    
    override init(access_token : String) {
        
        //FoursquareAccessTokenGetRequest set GETS and oauth_token=...
        super.init(access_token: access_token)
    

        //---------------------------------------------------------------------
        self.webServiceEndpoint = "https://api.foursquare.com/v2/venues/search"
        
        
//        //---------------------------------------------------------------------
//        if let currentLocation = appDelegate.currentLocation {
//            //add ll=
//            // TODO: - add ll
//        }else{
//            self.log.error("appDelegate.currentLocation is nil")
//        }
        
    }
}
