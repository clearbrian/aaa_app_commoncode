//
//  OAuth2AccessTokenFoursquareRequest.swift
//  joyride
//
//  Created by Brian Clear on 08/07/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation


class OAuth2AccessTokenFoursquareRequest : OAuth2ParentWSRequest{
    
    override init() {
        super.init()

        //---------------------------------------------------------------------
        //Foursquare is always a GET so body not important
        //self.headers["Content-Type"] = "application/x-www-form-urlencoded; charset=utf-8"
        
        //response is always json
        //self.headers["Accept"] = "application/json"
    }
}
