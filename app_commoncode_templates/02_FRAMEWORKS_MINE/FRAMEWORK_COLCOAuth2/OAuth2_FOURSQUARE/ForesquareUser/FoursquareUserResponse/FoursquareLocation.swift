//
//	FoursquareLocation.swift
//
//	Create by Brian Clear on 8/7/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class FoursquareLocation : ParentMappable{

	var address : String?
	var cc : String?
	var city : String?
	var country : String?
	var formattedAddress : [String]?
	var lat : Float?
	var lng : Float?
	var postalCode : String?
	var state : String?



	override func mapping(map: Map)
	{
		address <- map["address"]
		cc <- map["cc"]
		city <- map["city"]
		country <- map["country"]
		formattedAddress <- map["formattedAddress"]
		lat <- map["lat"]
		lng <- map["lng"]
		postalCode <- map["postalCode"]
		state <- map["state"]
		
	}
	var description: String
	{
		var description_ = "**** \(type(of: self)) *****\r"
		description_ = description_ + "address: \(address)\r"
		description_ = description_ + "cc: \(cc)\r"
		description_ = description_ + "city: \(city)\r"
		description_ = description_ + "country: \(country)\r"
		description_ = description_ + "formattedAddress: \(formattedAddress)\r"
		description_ = description_ + "lat: \(lat)\r"
		description_ = description_ + "lng: \(lng)\r"
		description_ = description_ + "postalCode: \(postalCode)\r"
		description_ = description_ + "state: \(state)\r"
		return description_
	}

}
