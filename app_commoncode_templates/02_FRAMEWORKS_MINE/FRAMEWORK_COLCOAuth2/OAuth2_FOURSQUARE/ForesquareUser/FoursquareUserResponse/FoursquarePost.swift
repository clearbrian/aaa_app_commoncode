//
//	FoursquarePost.swift
//
//	Create by Brian Clear on 8/7/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class FoursquarePost : ParentMappable{

	var count : Int?
	var textCount : Int?



	override func mapping(map: Map)
	{
		count <- map["count"]
		textCount <- map["textCount"]
		
	}
	var description: String
	{
		var description_ = "**** \(type(of: self)) *****\r"
		description_ = description_ + "count: \(count)\r"
		description_ = description_ + "textCount: \(textCount)\r"
		return description_
	}

}
