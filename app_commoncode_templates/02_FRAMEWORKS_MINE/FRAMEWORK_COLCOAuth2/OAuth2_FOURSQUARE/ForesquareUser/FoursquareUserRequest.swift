//
//  FoursquareUserRequest.swift
//  joyride
//
//  Created by Brian Clear on 08/07/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation

// https://api.foursquare.com/v2/users/self?oauth_token=ZPCT1JPZB3RQXM0Q3M5BAALVYNPASFK22IW40CVO0OBX5NQN&v=20160708


class FoursquareUserRequest : FoursquareAccessTokenGetRequest{
    
    override init(access_token : String) {
        
        //FoursquareAccessTokenGetRequest set GETS and oauth_token=...
        super.init(access_token: access_token)
        
        self.webServiceEndpoint = "https://api.foursquare.com/v2/users/self"


    }
}
