//
//	GithubUserRepoPermission.swift
//
//	Create by Brian Clear on 6/7/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GithubUserRepoPermission : ParentMappable{
    
    var admin : Bool?
    var pull : Bool?
    var push : Bool?
    
    
    
    override func mapping(map: Map)
    {
        admin <- map["admin"]
        pull <- map["pull"]
        push <- map["push"]
        
    }
    var description: String
    {
        var description_ = "**** \(type(of: self)) *****\r"
        description_ = description_ + "admin: \(admin)\r"
        description_ = description_ + "pull: \(pull)\r"
        description_ = description_ + "push: \(push)\r"
        return description_
    }
    
}
