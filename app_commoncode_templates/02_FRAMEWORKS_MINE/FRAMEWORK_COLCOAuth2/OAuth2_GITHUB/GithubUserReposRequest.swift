//
//  OAuth2GithubUserReposRequest.swift
//  joyride
//
//  Created by Brian Clear on 06/07/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation

//GET https://api.github.com/user/repos?access_token=5ec032eb388224913bb50e8baf9318045cf3657e
class GithubUserReposRequest : OAuth2ParentAccessTokenWSRequest{
    
    override init(access_token : String) {
        super.init(access_token: access_token)
        
        self.webServiceEndpoint = "https://api.github.com/user/repos"
        
    }
}
