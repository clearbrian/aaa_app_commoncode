//
//  GithubUser.swift
//  joyride
//
//  Created by Brian Clear on 06/07/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

//
//	GithubUser.swift
//
//	Create by Brian Clear on 6/7/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GithubUser : ParentMappable{
    
    var avatarUrl : String?
    var bio : String?
    var blog : String?
    var collaborators : Int?
    var company : String?
    var createdAt : String?
    var diskUsage : Int?
    var email : String?
    var eventsUrl : String?
    var followers : Int?
    var followersUrl : String?
    var following : Int?
    var followingUrl : String?
    var gistsUrl : String?
    var gravatarId : String?
    var hireable : String?
    var htmlUrl : String?
    var id : Int?
    var location : String?
    var login : String?
    var name : String?
    var organizationsUrl : String?
    var ownedPrivateRepos : Int?
    var plan : GithubPlan?
    var privateGists : Int?
    var publicGists : Int?
    var publicRepos : Int?
    var receivedEventsUrl : String?
    var reposUrl : String?
    var siteAdmin : Bool?
    var starredUrl : String?
    var subscriptionsUrl : String?
    var totalPrivateRepos : Int?
    var type : String?
    var updatedAt : String?
    var url : String?
    
    
    
    override func mapping(map: Map)
    {
        avatarUrl <- map["avatar_url"]
        bio <- map["bio"]
        blog <- map["blog"]
        collaborators <- map["collaborators"]
        company <- map["company"]
        createdAt <- map["created_at"]
        diskUsage <- map["disk_usage"]
        email <- map["email"]
        eventsUrl <- map["events_url"]
        followers <- map["followers"]
        followersUrl <- map["followers_url"]
        following <- map["following"]
        followingUrl <- map["following_url"]
        gistsUrl <- map["gists_url"]
        gravatarId <- map["gravatar_id"]
        hireable <- map["hireable"]
        htmlUrl <- map["html_url"]
        id <- map["id"]
        location <- map["location"]
        login <- map["login"]
        name <- map["name"]
        organizationsUrl <- map["organizations_url"]
        ownedPrivateRepos <- map["owned_private_repos"]
        plan <- map["plan"]
        privateGists <- map["private_gists"]
        publicGists <- map["public_gists"]
        publicRepos <- map["public_repos"]
        receivedEventsUrl <- map["received_events_url"]
        reposUrl <- map["repos_url"]
        siteAdmin <- map["site_admin"]
        starredUrl <- map["starred_url"]
        subscriptionsUrl <- map["subscriptions_url"]
        totalPrivateRepos <- map["total_private_repos"]
        type <- map["type"]
        updatedAt <- map["updated_at"]
        url <- map["url"]
        
    }
    var description: String
    {
        var description_ = "**** \(type(of: self)) *****\r"
        description_ = description_ + "avatarUrl: \(avatarUrl)\r"
        description_ = description_ + "bio: \(bio)\r"
        description_ = description_ + "blog: \(blog)\r"
        description_ = description_ + "collaborators: \(collaborators)\r"
        description_ = description_ + "company: \(company)\r"
        description_ = description_ + "createdAt: \(createdAt)\r"
        description_ = description_ + "diskUsage: \(diskUsage)\r"
        description_ = description_ + "email: \(email)\r"
        description_ = description_ + "eventsUrl: \(eventsUrl)\r"
        description_ = description_ + "followers: \(followers)\r"
        description_ = description_ + "followersUrl: \(followersUrl)\r"
        description_ = description_ + "following: \(following)\r"
        description_ = description_ + "followingUrl: \(followingUrl)\r"
        description_ = description_ + "gistsUrl: \(gistsUrl)\r"
        description_ = description_ + "gravatarId: \(gravatarId)\r"
        description_ = description_ + "hireable: \(hireable)\r"
        description_ = description_ + "htmlUrl: \(htmlUrl)\r"
        description_ = description_ + "id: \(id)\r"
        description_ = description_ + "location: \(location)\r"
        description_ = description_ + "login: \(login)\r"
        description_ = description_ + "name: \(name)\r"
        description_ = description_ + "organizationsUrl: \(organizationsUrl)\r"
        description_ = description_ + "ownedPrivateRepos: \(ownedPrivateRepos)\r"
        description_ = description_ + "plan: \(plan)\r"
        description_ = description_ + "privateGists: \(privateGists)\r"
        description_ = description_ + "publicGists: \(publicGists)\r"
        description_ = description_ + "publicRepos: \(publicRepos)\r"
        description_ = description_ + "receivedEventsUrl: \(receivedEventsUrl)\r"
        description_ = description_ + "reposUrl: \(reposUrl)\r"
        description_ = description_ + "siteAdmin: \(siteAdmin)\r"
        description_ = description_ + "starredUrl: \(starredUrl)\r"
        description_ = description_ + "subscriptionsUrl: \(subscriptionsUrl)\r"
        description_ = description_ + "totalPrivateRepos: \(totalPrivateRepos)\r"
        description_ = description_ + "type: \(type)\r"
        description_ = description_ + "updatedAt: \(updatedAt)\r"
        description_ = description_ + "url: \(url)\r"
        return description_
    }
    
}
