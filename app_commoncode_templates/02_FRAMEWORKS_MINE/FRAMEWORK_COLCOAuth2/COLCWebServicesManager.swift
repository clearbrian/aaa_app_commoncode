//
//  COLCWebServicesManager.swift
//  colcoauth2
//
//  Created by Brian Clear on 06/07/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit


enum COLCHTTPMethod : String {
    case Get = "GET"
    case Post = "POST"
}

class COLCWebServicesManager {
    let log = MyXCodeEmojiLogger.defaultInstance
    func callOAuthWS(_ oauth2ParentWSRequest: OAuth2ParentWSRequest,
                            parsedResponse: @escaping (_ parsedObject: Any, _ statusCode: Int) -> Void,
                                   failure: @escaping (_ error: Error?) -> Void
        )
    {
        //---------------------------------------------------------------------
        if let urlString = oauth2ParentWSRequest.urlString{
            
            // TODO: - should be done in urlString getter
            //---------------------------------------------------------------------
            //PARAMETERS - should be done in urlString getter
            //---------------------------------------------------------------------
            //https://arc.test.clarksons.com/api/1/0/Search/vesselAisLocations?q=æ&take=5
            //---------------------------------------------------------------------
            //            let parameters: [String: AnyObject]? = oauth2ParentWSRequest.parameters
            //            
            //            var urlStringFinal = "\(urlString)?"
            //            
            //            if let parameters = parameters{
            //                for parameter in parameters{
            //                    self.log.info("parameter.0:\(parameter.0)")
            //                    self.log.info("parameter.1:\(parameter.1)")
            //                    let key = parameter.0
            //                    let value = parameter.1
            //                    urlStringFinal = "\(urlStringFinal)\(key)=\(value)&"
            //                }
            //            }else{
            //                self.log.error("parameters is nil")
            //            }
            //            let urlwithPercentEscapes = urlStringFinal.stringByAddingPercentEncodingWithAllowedCharacters( NSCharacterSet.URLQueryAllowedCharacterSet())
            
            
            //---------------------------------------------------------------------
            let urlStringwithPercentEscapes = urlString.addingPercentEncoding( withAllowedCharacters: CharacterSet.urlQueryAllowed)
            //---------------------------------------------------------------------
            if let urlStringwithPercentEscapes = urlStringwithPercentEscapes{
                
                
                //key=
                //app_key
                if urlString.contains("key"){
                      self.log.highlight("CALLING URL [BEFORE]:\r[REDACTED]")
                }else{
                      self.log.highlight("CALLING URL [BEFORE]:\r[\(urlString)]")
                }
              
                
                if let url: URL = URL(string: urlStringwithPercentEscapes){
                    
                    //self.log.debug("URL ESCAPED:[\(urlStringwithPercentEscapes)]")
                    //------------------------------------------------------------------------------------------------
                    let request: NSMutableURLRequest = NSMutableURLRequest(url: url)

                    //---------------------------------------------------------------------
                    //"GET"/"POST"
                    request.httpMethod = oauth2ParentWSRequest.colcHTTPMethod.rawValue
                    //---------------------------------------------------------------------
                    switch oauth2ParentWSRequest.colcHTTPMethod{
                    case .Get:
                        //---------------------------------------------------------------------
                        //GET
                        //---------------------------------------------------------------------
                        self.log.info("HTTP METHOD: \(oauth2ParentWSRequest.colcHTTPMethod.rawValue)")
                        
                    case .Post:
                        //---------------------------------------------------------------------
                        //POST
                        //---------------------------------------------------------------------
                        self.log.info("HTTP METHOD: \(oauth2ParentWSRequest.colcHTTPMethod.rawValue)")
                        
                        //-----------------------------------------------------
                        //BODY
                        //-----------------------------------------------------
                        if let bodyString = oauth2ParentWSRequest.bodyString {
                            
                            self.log.debug("====== BODY ======")
                            print(bodyString)
                            print("bodyString.length:\(bodyString.length)")
                            self.log.debug("====== BODY ======")

                            
                            let bodyData = bodyString.data(using: String.Encoding.utf8)
                            request.httpBody=bodyData

                        }else{
                            self.log.error("POST but oauth2ParentWSRequest.bodyString is nil")
                        }
                    }

                    //---------------------------------------------------------------------
                    //was in sample but not sure its needed
                    //request.timeoutInterval = 60
                    //---------------------------------------------------------------------
                   
                    
                    //---------------------------------------------------------------------
                    //HEADERS
                    //---------------------------------------------------------------------
                    //add header for all ws calls

                    //request.setValue("http://www.cityoflondonconsulting.com", forHTTPHeaderField: "Referer")
                    
                    //    //---------------------------------------------------------------------
                    //    //This is format of the BODY SENT
                    //    //---------------------------------------------------------------------
                    //    //application/x-www-form-urlencoded; charset=utf-8
                    //    //client_id=be45feb27af1729e6312&client_secret=bbf4bde86a4a3fa193b821cc4a6c610079c306d2&oauth_redirect_uri=joyrider://oauth/callback_github&state=8E7F9061
                    //    //---------------------------------------------------------------------
                    //    request.setValue("application/json;charset=UTF-8", forHTTPHeaderField: "Content-Type")
                    //
                    //    //This is format of the RESPONSE RETURNED
                    //    //doent work with github
                    //    //request.setValue("application/json;charset=UTF-8", forHTTPHeaderField: "Accept")
                    //    request.setValue("application/json", forHTTPHeaderField: "Accept")
                    //    //------------------------------------------------
                    
                    let headersKeysArray = Array(oauth2ParentWSRequest.headers.keys)
                    
                    for key in headersKeysArray{
                        if let value = oauth2ParentWSRequest.headers[key]{
                            self.log.info("ADD HEADER [\(key)]:[\(value)]")
                            request.setValue(value, forHTTPHeaderField: key)
                            
                        }else{
                            self.log.error("oauth2ParentWSRequest.headers[\(key)] is nil")
                        }
                    }
                    
                    //------------------------------------------------------------------------------------------------
                    //CALL WEBSERVICE
                    //------------------------------------------------------------------------------------------------
                    //SWIFT2.0
                    let session = URLSession.shared
                    let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                        //---------------------------------------------------------------------
                        //RESPONSE RETURNED
                        //---------------------------------------------------------------------
                        DispatchQueue.main.async{
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        }
                        
                        if let error = error{
                            failure(error)
                        }else{
                            //---------------------------------------------------------------------
                            //NO ERROR
                            //---------------------------------------------------------------------
                            if let data = data{
                                
                                if let httpResponse = response as? HTTPURLResponse{
                                    let requestUrlString = "\(request.url)"
                                    //app_key=
                                    //key=
                                    if requestUrlString.contains("key"){
                                        print("====== [2. STATUS CODE:[\(httpResponse.statusCode)] URL REDACTED=====")
                                    }else{
                                        print("====== [3. STATUS CODE:[\(httpResponse.statusCode)] \(request.url)=====")
                                    }
                                    
                                    
                                    //------------------------------------------------------------------------------------------------
                                    if let dataStringResponse = NSString(data:data, encoding:String.Encoding.utf8.rawValue){
                                        print("====== [RESPONSE \(request.url)=====")
                                        //NOISY
                                        //print("dataStringResponse:")
                                        //print(dataStringResponse)
                                        //print("dataStringResponse.length:\(dataStringResponse.length)")
                                        print("================")
                                        
                                        //print(JSONPrint.prettyPrint(bodystring))
                                        //---------------------------------------------------------------------
                                        //github if Accept is 'xml'
                                        //error=bad_verification_code&error_description=The+code+passed+is+incorrect+or+expired.&error_uri=https%3A%2F%2Fdeveloper.github.com%2Fv3%2Foauth%2F%23bad-verification-code
                                        //"Accept"] = "application/json"
                                        //{"error":"bad_verification_code","error_description":"The code passed is incorrect or expired.","error_uri":"https://developer.github.com/v3/oauth/#bad-verification-code"}
                                        
                                        //------------------------------------------------------------------------------------------------
                                        //Response Header Fields
                                        //------------------------------------------------------------------------------------------------
                                        //    print("====== [RESPONSE: allHeaderFields =====")
                                        //    print("httpResponse.allHeaderFields:\(httpResponse.allHeaderFields)")
                                        //    print("================")
                                        
                                        //------------------------------------------------------------------------------------------------

                                        
                                        //---------------------------------------------------------------------
                                        //For OAuth a call can fail and return valid json
                                        //so return the parsedObject and a statusCode and let the github or foursquare handle the error/success as codes can vary
                                        //---------------------------------------------------------------------
                                        
                                        do {
                                            //SWIFT3let parsedObject: Any = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments)
                                            let parsedObject: Any = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments)
                                            
                                            //---------------------------------------------------------------------
                                            //NOTE - could be a 200 and error in the returned json - handled externally
                                            //{"error":"bad_verification_code","error_description":"The code passed is incorrect or expired.","error_uri":"https://developer.github.com/v3/oauth/#bad-verification-code"}
                                            //---------------------------------------------------------------------
                                            
                                            //so return ALL on main thread
                                            DispatchQueue.main.async{
                                                //success( parsedObject)
                                                parsedResponse( parsedObject, httpResponse.statusCode)
                                            
                                            }
                                            
                                            //---------------------------------------------------------------------
                                            
                                        } catch let error {
                                            //---------------------------------------------------------------------
                                            //invalid json return as NSError
                                            //---------------------------------------------------------------------
                                            failure(error)
                                        }
                                        
                                    }else{
                                        //ERROR
                                        failure(NSError.errorWithDescription("No Data", code: httpResponse.statusCode))
                                    }
                                    //------------------------------------------------------------------------------------------------
                                    
                                }else{
                                    self.log.error("response as? NSHTTPURLResponse is nil")
                                    failure(NSError.errorWithDescription("NSHTTPURLResponse is nil", code: 65756))
                                }
                                
                            }else{
                                failure(NSError.errorWithDescription("reponse data is nil", code: 65756))
                            }
                        }
                    }) 
                    //CALL THE URL
                    DispatchQueue.main.async{
                        UIApplication.shared.isNetworkActivityIndicatorVisible = true
                    }
                    
                    task.resume()
                    //------------------------------------------------------------------------------------------------
                    //------------------------------------------------------------------------------------------------
                    
                }else{
                    self.log.error("URL FAILED")
                    failure(NSError.errorWithDescription("invalid url String", code: 5778))
                }
            }else{
                self.log.error("urlwithPercentEscapes is nil")
                failure(NSError.errorWithDescription("urlwithPercentEscapes is nil", code: 5778))
            }
            
        }else{
            self.log.error("webServiceRequest.urlString is nil - did you subclass it")
            //            failure(error: NSError(type:.Arc_APIError_QueryString_Throwing_Error))
            failure(NSError.errorWithDescription("webServiceRequest.urlString is nil - did you subclass it", code: 6356346))
        }
    }
}
