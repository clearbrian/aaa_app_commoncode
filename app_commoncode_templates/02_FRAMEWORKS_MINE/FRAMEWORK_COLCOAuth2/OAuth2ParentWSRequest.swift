//
//  OAuth2ParentWSRequest.swift
//  joyride
//
//  Created by Brian Clear on 06/07/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation


//Handles ANY WS request
class OAuth2ParentWSRequest : ParentSwiftObject{
    
    //should be set by subclass
    //https://github.com/login/oauth/access_token
    var webServiceEndpoint: String?
    
    var query: String?

    var colcHTTPMethod: COLCHTTPMethod = .Get
    
    var bodyString: String?

    var parameters: [String: AnyObject] = [String: AnyObject]()
    
    var headers: [String: String] = [String: String]()
    
    override init(){
        super.init()
        
    }
    
    var urlString :String? {
        get {
            
            var urlString_ :String?
            //---------------------------------------------------------------------
            if let webServiceEndpoint = self.webServiceEndpoint {
                
                if let query = query{

                    urlString_ = "\(webServiceEndpoint)?\(query)"
                    
                }else{
                    //---------------------------------------------------------------------
                    //query is nil
                    //---------------------------------------------------------------------
                    switch self.colcHTTPMethod{
                    case .Get:
                        self.log.error("query is nil - GET so more likely an error")
                    case .Post:
                        self.log.info("query is nil - POST so probably not an error")
                    }
                    //---------------------------------------------------------------------
                    //NO ?query=
                    urlString_ = "\(webServiceEndpoint)"
                    //---------------------------------------------------------------------
                }
            }else{
                self.log.error("self.webServiceEndpoint is nil")
            }
            //---------------------------------------------------------------------
            return urlString_
        }
        set {
            print("[urlString: set]\(newValue)")
        }
    }
}
