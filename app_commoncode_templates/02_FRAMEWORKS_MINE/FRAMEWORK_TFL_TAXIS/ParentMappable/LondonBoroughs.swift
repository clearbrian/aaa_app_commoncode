//
//  LondonBoroughs.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 19/01/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
/*
 used in 
 RANK
 PROP,Address,Borough,Kensington and Chelsea
 
 //https://en.wikipedia.org/wiki/List_of_London_boroughs
 Barking and Dagenham
 Barnet
 Bexley
 Brent
 Bromley
 Camden
 City of London     missing form wikipedia but in /TaxiRank json
 Croydon
 Ealing
 Enfield
 Greenwich
 Hackney
 Hammersmith and Fulham
 Haringey
 Harrow
 Havering
 Hillingdon
 Hounslow
 Islington
 Kensington and Chelsea
 Kingston upon Thames
 Lambeth
 Lewisham
 Merton
 Newham
 Redbridge
 Richmond Upon Thames
 Southwark
 Sutton
 Tower Hamlets
 Waltham Forest
 Wandsworth
 Westminster
 
 
 "Barking and Dagenham"
 "Barnet"
 "Bexley"
 "Brent"
 "Bromley"
 "Camden"
 "City of London"
 "Croydon"
 "Ealing"
 "Enfield"
 "Greenwich"
 "Hackney"
 "Hammersmith and Fulham"
 "Haringey"
 "Harrow"
 "Havering"
 "Hillingdon"
 "Hounslow"
 "Islington"
 "Kensington and Chelsea"
 "Kingston upon Thames"
 "Lambeth"
 "Lewisham"
 "Merton"
 "Newham"
 "Redbridge"
 "Richmond Upon Thames"
 "Southwark"
 "Sutton"
 "Tower Hamlets"
 "Waltham Forest"
 "Wandsworth"
 "Westminster"
 
 
 https://docs.google.com/spreadsheets/d/1snItXx1Rv_KKpGfXei1G8f98rokrvANQvCMP4fzrMa8/edit#gid=1132259057
 
*/

enum LondonBorough: String{
    //string in json not known
    case unknown = "UNKNOWN"
    
    case barking_and_dagenham = "Barking and Dagenham"
    case barnet = "Barnet"
    case bexley = "Bexley"
    case brent = "Brent"
    case bromley = "Bromley"
    case camden = "Camden"
    case city_of_london = "City of London"
    case croydon = "Croydon"
    case ealing = "Ealing"
    case enfield = "Enfield"
    case greenwich = "Greenwich"
    case hackney = "Hackney"
    case hammersmith_and_fulham = "Hammersmith and Fulham"
    case haringey = "Haringey"
    case harrow = "Harrow"
    case havering = "Havering"
    case hillingdon = "Hillingdon"
    case hounslow = "Hounslow"
    case Islington = "Islington"
    case kensington_and_chelsea = "Kensington and Chelsea"
    case kingston_upon_thames = "Kingston upon Thames"
    case lambeth = "Lambeth"
    case lewisham = "Lewisham"
    case merton = "Merton"
    case newham = "Newham"
    case redbridge = "Redbridge"
    case richmond_upon_thames = "Richmond Upon Thames"
    case southwark = "Southwark"
    case sutton = "Sutton"
    case tower_hamlets = "Tower Hamlets"
    case waltham_forest = "Waltham Forest"
    case wandsworth = "Wandsworth"
    case westminster = "Westminster"
    
    func londonBoroughForString(boroughString: String) -> LondonBorough{
        switch boroughString{
        case "Barking and Dagenham":
            return .barking_and_dagenham
        case "Barnet":
            return .barnet
        case "Bexley":
            return .bexley
        case "Brent":
            return .brent
        case "Bromley":
            return .bromley
        case "Camden":
            return .camden
        case "City of London":
            return .city_of_london
        case "Croydon":
            return .croydon
        case "Ealing":
            return .ealing
        case "Enfield":
            return .enfield
        case "Greenwich":
            return .greenwich
        case "Hackney":
            return .hackney
        case "Hammersmith and Fulham":
            return .hammersmith_and_fulham
        case "Haringey":
            return .haringey
        case "Harrow":
            return .harrow
        case "Havering":
            return .havering
        case "Hillingdon":
            return .hillingdon
        case "Hounslow":
            return .hounslow
        case "Islington":
            return .Islington
        case "Kensington and Chelsea":
            return .kensington_and_chelsea
        case "Kingston upon Thames":
            return .kingston_upon_thames
        case "Lambeth":
            return .lambeth
        case "Lewisham":
            return .lewisham
        case "Merton":
            return .merton
        case "Newham":
            return .newham
        case "Redbridge":
            return .redbridge
        case "Richmond Upon Thames":
            return .richmond_upon_thames
        case "Southwark":
            return .southwark
        case "Sutton":
            return .sutton
        case "Tower Hamlets":
            return .tower_hamlets
        case "Waltham Forest":
            return .waltham_forest
        case "Wandsworth":
            return .wandsworth
        case "Westminster":
            return .westminster
        default:
            return .unknown
        }
    }
}
