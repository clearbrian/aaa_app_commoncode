//
//  TFLAdditionalPropertyCategory_TaxiRank.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 13/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

//TFLAdditionalProperty:[category:Address - key:Borough - value:Tower Hamlets]
//TFLAdditionalProperty:[category:Description - key:NumberOfSpaces - value:3]
//TFLAdditionalProperty:[category:Description - key:OperationDays - value:Mon - Sun]
//TFLAdditionalProperty:[category:Description - key:OperationTimes - value:24 hours]
//TFLAdditionalProperty:[category:Description - key:RankType - value:Working]


enum TFLAdditionalPropertyCategory_TaxiRank: String{
    case Unknown = "Unknown"
    case Address = "Address"
    case Description = "Description"
    
    
    func displayString() -> String{
        switch self{
        case .Unknown:
            return "Unknown"
            
        case .Address:
            return "Address"
            
        case .Description:
            return "Description"
            
        }
    }
    
    //"Address" >> TFLAdditionalPropertyCategory.Address
    static func categoryForCategoryString(categoryString: String?) -> TFLAdditionalPropertyCategory_TaxiRank{
        if let categoryString = categoryString {
            switch categoryString{
            case "Address":
                return .Address
                
            case "Description":
                return .Description
                
            default:
                return .Unknown
            }
        }else{
            appDelegate.log.error("categoryString is nil")
            return .Unknown
        }
    }
}
