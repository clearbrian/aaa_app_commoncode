//
//  TFLAdditionalPropertyKey.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 13/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

//https://docs.google.com/spreadsheets/d/1ZD7Oty83tidOD-spUpJsMFRFphzVFIY9hqpyef6BrnY/edit#gid=0

enum TFLAdditionalPropertyKey_TaxiRank: String{
    
    case unknownPropKey = "Unknown"
    case boroughPropKey = "Borough"
    case numberOfSpacesPropKey = "NumberOfSpaces"
    case operationDaysPropKey = "OperationDays"
    case operationTimesPropKey = "OperationTimes"
    case rankTypePropKey = "RankType"
    case stationAtcoCodePropKey = "StationAtcoCode" //stationAtcoCode,910GSRUISLP - TaxiRank_3569,PROP,Address,borough,Hillingdon
    
    func displayString() -> String{
        switch self{
        case .unknownPropKey:
            return "Unknown"
            
        case .boroughPropKey:
            return "borough"
            
        case .numberOfSpacesPropKey:
            return "Number Of Spaces"
            
        case .operationDaysPropKey:
            return "Operation Days"
            
        case .operationTimesPropKey:
            return "Operation Times"
            
        case .rankTypePropKey:
            return "Rank Type"
            
        case .stationAtcoCodePropKey:
            return "Station Atco Code"
            
        }
    }
    
    //"rankType" >> TFLAdditionalPropertyKey.rankType
    static func keyForKeyString(keyString: String?) -> TFLAdditionalPropertyKey_TaxiRank{
        
        if let keyString = keyString {
            switch keyString{
                
            case "Borough":
                return .boroughPropKey
                
            case "NumberOfSpaces":
                return .numberOfSpacesPropKey
                
            case "OperationDays":
                return .operationDaysPropKey
                
            case "OperationTimes":
                return .operationTimesPropKey
                
            case "RankType":
                return .operationTimesPropKey
                
            case "StationAtcoCode":
                return .stationAtcoCodePropKey
                
            default:
                return .unknownPropKey
            }
        }else{
            appDelegate.log.error("keyString is nil")
            return .unknownPropKey
        }
    }
}
