//
//  TFLApiPlaceTaxiRankVersionInfo.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 11/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

struct TFLApiPlaceTaxiRankVersionInfo{
    
    var rankIdJSON : String
    var rankIdUnversioned : String
    var version : Int
    var hasVersions : Bool
    
    init (     rankIdJSON : String,
               rankIdUnversioned : String,
               version : Int,
               hasVersions : Bool)
    {
        self.rankIdJSON = rankIdJSON
        self.rankIdUnversioned = rankIdUnversioned
        self.version = version
        self.hasVersions = hasVersions
        
    }
}
