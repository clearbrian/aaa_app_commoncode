//
//  CLKGeocodedAddressesCollection.swift
//  joyride
//
//  Created by Brian Clear on 26/10/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
//--------------------------------------------------------------
// MARK: - Single address string search returns multiple forward geocoded("e1 8lg" >> "CLPlacemark")
// MARK: -
//--------------------------------------------------------------
class CLKGeocodedAddressesCollection: ParentSwiftObject{
    
    
    //Ewings beflast has no name only postcode - Apple forward geocoding will return lat/lng but may not improve address
    var reverseGeocodeRecommended: Bool{
        var reverseGeocodeRecommended_ = false
        
        if let bestOrFirstCLKAddress = self.bestOrFirstCLKAddress {
            reverseGeocodeRecommended_ = bestOrFirstCLKAddress.reverseGeocodeRecommended
            
        }else{
            appDelegate.log.error("self.bestOrFirstCLKAddress is nil >> reverseGeocodeRecommended_ FALSE")
        }
        
        return reverseGeocodeRecommended_
        
    }
    
    var firstAddress: CLKAddress?{
        appDelegate.log.error("firstAddress is nil - should subclass")
        return nil
    }
    
    
    var bestOrFirstCLKAddress: CLKAddress?{
        appDelegate.log.error("bestOrFirstCLKAddress is nil - should subclass")
        return nil
    }
}
