//
//  COLCGoogleDirectionsUIManager.swift
//  joyride
//
//  Created by Brian Clear on 26/01/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import GoogleMaps

protocol COLCGoogleDirectionsUIManagerDelegate {
    func colcGoogleDirectionsUIManager(_ colcGoogleDirectionsUIManager: COLCGoogleDirectionsUIManager, clkGoogleDirectionsResponse : CLKGoogleDirectionsResponse )
}

//off loads all code to do with calling Google Directions from the UIViewController
class COLCGoogleDirectionsUIManager {
    
    var delegate: COLCGoogleDirectionsUIManagerDelegate?
    
    let log = MyXCodeEmojiLogger.defaultInstance
    var gmsMapView: GMSMapView?
    var parentViewController: UIViewController?

    var tripGMSPolyline : GMSPolyline?
    
    init(gmsMapView: GMSMapView, parentViewController: UIViewController) {
        
        //to draw directions on map
        self.gmsMapView = gmsMapView
        //to show alerts
        self.parentViewController = parentViewController
        
    }

    var googleDirectionsSearchRunning = false
    
    func callWS_GoogleDirections()
    {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        if self.googleDirectionsSearchRunning {
            self.log.error("googleDirectionsSearchRunning is TRUE - dont run again")
        }else{
            googleDirectionsSearchRunning = true
            
            
            //------------------------------------------------------------------------------------------------
            //CLKGoogleDirectionsRequest
            //------------------------------------------------------------------------------------------------
            
            
            //------------------------------------------------------------------------------------------------
            //ADDRESS TO ADDRESS
            //------------------------------------------------------------------------------------------------
            
            //self.getDirections(addressStringFrom: "London", addressStringTo: "Liverpool")
            //self.getDirections(addressStringFrom: "E1 8LG", addressStringTo: "Liverpool")
            //self.getDirections(addressStringFrom: "E1 8LG", addressStringTo: "New York") //ZERO_RESULTS
            
            
            //------------------------------------------------------------------------------------------------
            //PLACEID TO PLACEID
            //------------------------------------------------------------------------------------------------
            //LONDON TO CORK
            //self.getDirections(placeIdFrom: "ChIJdd4hrwug2EcRmSrV3Vo6llI", placeIdTo: "ChIJYbm-kQiQREgR0MUxl6nHAAo")
            //------------------------------------------------------------------------------------------------
            //LATLNG to LATLNG
            //------------------------------------------------------------------------------------------------
            
            //50.7195341,-3.538888
            //50.8011214,-3.4370039
            //OK
            //self.getDirections( latFrom: NSNumber(double: 50.7195341), lngFrom: NSNumber(double:-3.538888),
            //                  latTo: NSNumber(double: 50.8011214), lngTo: NSNumber(double: -3.4370039))
            
            //OK - TUPLE VERSION
            //self.getDirections(latFrom:(50.7195341, -3.538888),
            //                     latTo:(50.8011214, -3.4370039))
            //---------------------------------------------------------------------
            //EAST LONDON /EALING - checkign traffic
            //    self.getDirections(latFrom:(51.5119723,-0.3730202),
            //                     latTo:(51.4058183,-0.0103344))
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - getDirections
    // MARK: -
    //--------------------------------------------------------------

    
    func getDirections(clkPlaceStart: CLKPlace,
                       clkPlaceEnd: CLKPlace,
                       clkGoogleDirectionsTravelMode: CLKGoogleDirectionsTravelMode = .walking,
        
                       success: @escaping (_ clkGoogleDirectionsResponse: CLKGoogleDirectionsResponse?) -> Void,
                       failure: @escaping (_ error: Error?) -> Void)
    {

        if let clkGoogleDirectionsRequest:CLKGoogleDirectionsRequest = CLKGoogleDirectionsRequest(clkPlaceStart: clkPlaceStart, clkPlaceEnd: clkPlaceEnd)
        {
            //----------------------------------------------------------------------------------------
            //
            //----------------------------------------------------------------------------------------
            
            // TODO: - let use choose walk/tube
            //NOTE defaulting to "transit/driving"
            //----------------------------------------------------------------------------------------
            
            //1 leg n steps
            //https://maps.googleapis.com/maps/api/directions/json?language=en-GB&origin=51.50835708148136,-0.07180972857748918&destination=51.47121047973633,-0.489874005317688&mode=walking&key=AIzaSyBRBz99dZS2LnD3h6hFJ5_pOCiWD2JB-1o&
            //----------------------------------------------------------------------------------------
            //clkGoogleDirectionsRequest.mode = CLKGoogleDirectionsTravelMode.Walking.rawValue
            //clkGoogleDirectionsRequest.transit_mode = nil

            //clkGoogleDirectionsRequest.mode = CLKGoogleDirectionsTravelMode.Transit.rawValue
            //clkGoogleDirectionsRequest.transit_mode = nil
            
            
            clkGoogleDirectionsRequest.mode = clkGoogleDirectionsTravelMode.rawValue
            clkGoogleDirectionsRequest.transit_mode = nil

            //----------------------------------------------------------------------------------------
            self.callCLKGoogleDirectionsRequest(clkGoogleDirectionsRequest,
                                                success:{(clkGoogleDirectionsResponse: CLKGoogleDirectionsResponse?) -> Void in
                                                    
                                                    //---------------------------------------------------------------------
                                                    if let clkGoogleDirectionsResponse = clkGoogleDirectionsResponse {
                                                        self.handleCLKGoogleDirectionsResponse(clkGoogleDirectionsResponse)
                                                    }else{
                                                        self.log.error("clkGoogleDirectionsResponse is nil")
                                                    }
                                                    //---------------------------------------------------------------------
                                                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                                    //---------------------------------------------------------------------
                },
                                                failure:{(error) -> Void in
                                                    self.log.error("error:\(String(describing: error))")
                                                    if let nserror = error {
                                                        self.handleError(nserror)
                                                        
                                                    }else{
                                                        self.log.error("error is nil")
                                                    }
                                                    
                                                    self.googleDirectionsSearchRunning = false
                                                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
)
            
        }else{
            self.log.error("CLKGoogleDirectionsLocation init failed")
        }
        
    }
    
    func getDirections(
        addressStringFrom: String,
        addressStringTo: String,
        success: @escaping (_ clkGoogleDirectionsResponse: CLKGoogleDirectionsResponse?) -> Void,
        failure: @escaping (_ error: Error?) -> Void)
    {
        //---------------------------------------------------------------------
        //ADDRESS TO ADDRESSS
        if let clkGoogleDirectionsRequest:CLKGoogleDirectionsRequest
            = CLKGoogleDirectionsRequest(addressStringFrom: addressStringFrom, addressStringTo: addressStringTo)
        {
            
            self.callCLKGoogleDirectionsRequest(clkGoogleDirectionsRequest,
                                                success:{(clkGoogleDirectionsResponse: CLKGoogleDirectionsResponse?) -> Void in
                                                    
                                                    //                                                    //---------------------------------------------------------------------
                                                    //                                                    if let clkGoogleDirectionsResponse = clkGoogleDirectionsResponse {
                                                    //                                                        self.handleCLKGoogleDirectionsResponse(clkGoogleDirectionsResponse)
                                                    //                                                    }else{
                                                    //                                                        self.log.error("clkGoogleDirectionsResponse is nil")
                                                    //                                                    }
                                                    //                                                    //---------------------------------------------------------------------
                                                    //                                                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                                    //                                                    //---------------------------------------------------------------------
                },
                                                failure:{(error) -> Void in
                                                    //                                                    self.log.error("error:\(error)")
                                                    //                                                    if let nserror = error {
                                                    //                                                        self.handleError(nserror)
                                                    //                                                        
                                                    //                                                    }else{
                                                    //                                                        self.log.error("error is nil")
                                                    //                                                    }
                                                    //                                                    
                                                    //                                                    self.googleDirectionsSearchRunning = false
                                                    //                                                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
            })
            
        }else{
            self.log.error("CLKGoogleDirectionsLocation init failed")
        }
        //---------------------------------------------------------------------
    }
    
    func getDirections( placeIdFrom: String,
                        placeIdTo: String,
                        success: @escaping (_ clkGoogleDirectionsResponse: CLKGoogleDirectionsResponse?) -> Void,
                        failure: @escaping (_ error: Error?) -> Void)
    {
        //---------------------------------------------------------------------
        //ADDRESS TO ADDRESSS
        if let clkGoogleDirectionsRequest:CLKGoogleDirectionsRequest
            = CLKGoogleDirectionsRequest(placeIdFrom: placeIdFrom,
                                           placeIdTo: placeIdTo)
        {
            self.callCLKGoogleDirectionsRequest(clkGoogleDirectionsRequest,
                                                success:{(clkGoogleDirectionsResponse: CLKGoogleDirectionsResponse?) -> Void in
                                                    
                                                    //                                                    //---------------------------------------------------------------------
                                                    //                                                    if let clkGoogleDirectionsResponse = clkGoogleDirectionsResponse {
                                                    //                                                        self.handleCLKGoogleDirectionsResponse(clkGoogleDirectionsResponse)
                                                    //                                                    }else{
                                                    //                                                        self.log.error("clkGoogleDirectionsResponse is nil")
                                                    //                                                    }
                                                    //                                                    //---------------------------------------------------------------------
                                                    //                                                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                                    //                                                    //---------------------------------------------------------------------
                },
                                                failure:{(error) -> Void in
                                                    //                                                    self.log.error("error:\(error)")
                                                    //                                                    if let nserror = error {
                                                    //                                                        self.handleError(nserror)
                                                    //                                                        
                                                    //                                                    }else{
                                                    //                                                        self.log.error("error is nil")
                                                    //                                                    }
                                                    //                                                    
                                                    //                                                    self.googleDirectionsSearchRunning = false
                                                    //                                                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
            })
            
        }else{
            self.log.error("CLKGoogleDirectionsLocation init failed")
        }
        //---------------------------------------------------------------------
    }
    
    func getDirections(latFrom:(lat:Double,lng:Double),
                       latTo:(lat:Double,lng:Double),
                       success: @escaping (_ clkGoogleDirectionsResponse: CLKGoogleDirectionsResponse?) -> Void,
                       failure: @escaping (_ error: Error?) -> Void)
    {
        
        self.getDirections( latFrom: NSNumber(value: latFrom.lat),
                            lngFrom: NSNumber(value: latFrom.lng),
                            latTo: NSNumber(value: latTo.lat),
                            lngTo: NSNumber(value: latTo.lng),
                            success:{(clkGoogleDirectionsResponse: CLKGoogleDirectionsResponse?) -> Void in
                                
                                //                                                    //---------------------------------------------------------------------
                                //                                                    if let clkGoogleDirectionsResponse = clkGoogleDirectionsResponse {
                                //                                                        self.handleCLKGoogleDirectionsResponse(clkGoogleDirectionsResponse)
                                //                                                    }else{
                                //                                                        self.log.error("clkGoogleDirectionsResponse is nil")
                                //                                                    }
                                //                                                    //---------------------------------------------------------------------
                                //                                                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                //                                                    //---------------------------------------------------------------------
            },
                            failure:{(error) -> Void in
                                //                                                    self.log.error("error:\(error)")
                                //                                                    if let nserror = error {
                                //                                                        self.handleError(nserror)
                                //
                                //                                                    }else{
                                //                                                        self.log.error("error is nil")
                                //                                                    }
                                //
                                //                                                    self.googleDirectionsSearchRunning = false
                                //                                                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
        })
    }
    
    
    
    func getDirections(
        latFrom: NSNumber, lngFrom: NSNumber,
        latTo: NSNumber, lngTo: NSNumber,
        success: @escaping (_ clkGoogleDirectionsResponse: CLKGoogleDirectionsResponse?) -> Void,
        failure: @escaping (_ error: Error?) -> Void)
    {
        //---------------------------------------------------------------------
        //ADDRESS TO ADDRESSS
        if let clkGoogleDirectionsRequest:CLKGoogleDirectionsRequest
            = CLKGoogleDirectionsRequest(latFrom: latFrom, lngFrom: lngFrom,
                latTo: latTo, lngTo:lngTo)
        {
            self.callCLKGoogleDirectionsRequest(clkGoogleDirectionsRequest,
                                                success:{(clkGoogleDirectionsResponse: CLKGoogleDirectionsResponse?) -> Void in
                                                    
                                                    //                                                    //---------------------------------------------------------------------
                                                    //                                                    if let clkGoogleDirectionsResponse = clkGoogleDirectionsResponse {
                                                    //                                                        self.handleCLKGoogleDirectionsResponse(clkGoogleDirectionsResponse)
                                                    //                                                    }else{
                                                    //                                                        self.log.error("clkGoogleDirectionsResponse is nil")
                                                    //                                                    }
                                                    //                                                    //---------------------------------------------------------------------
                                                    //                                                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                                    //                                                    //---------------------------------------------------------------------
                },
                                                failure:{(error) -> Void in
                                                    //                                                    self.log.error("error:\(error)")
                                                    //                                                    if let nserror = error {
                                                    //                                                        self.handleError(nserror)
                                                    //                                                        
                                                    //                                                    }else{
                                                    //                                                        self.log.error("error is nil")
                                                    //                                                    }
                                                    //                                                    
                                                    //                                                    self.googleDirectionsSearchRunning = false
                                                    //                                                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
            })
            
        }else{
            self.log.error("CLKGoogleDirectionsLocation init failed")
        }
        //---------------------------------------------------------------------
    }
    
    
    var googleDirectionsController = GoogleDirectionsController()
    
    func callCLKGoogleDirectionsRequest(_ clkGoogleDirectionsRequest : CLKGoogleDirectionsRequest,
                                        success: @escaping (_ clkGoogleDirectionsResponse: CLKGoogleDirectionsResponse?) -> Void,
                                        failure: @escaping (_ error: Error?) -> Void){
        //------------------------------------------------------------------------------------------------
        self.googleDirectionsController.findGoogleDirections(clkGoogleDirectionsRequest,
            success:{(clkGoogleDirectionsResponse: CLKGoogleDirectionsResponse?) -> Void in
                
                //---------------------------------------------------------------------
                if let clkGoogleDirectionsResponse = clkGoogleDirectionsResponse {
                    self.handleCLKGoogleDirectionsResponse(clkGoogleDirectionsResponse)
                }else{
                    self.log.error("clkGoogleDirectionsResponse is nil")
                }
                //---------------------------------------------------------------------
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                //---------------------------------------------------------------------
            },
            failure:{(error) -> Void in
                self.log.error("error:\(String(describing: error))")
                if let nserror = error {
                    self.handleError(nserror)
                    
                }else{
                    self.log.error("error is nil")
                }
                
                self.googleDirectionsSearchRunning = false
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
        )
        
        //------------------------------------------------------------------------------------------------
    }
    
    //--------------------------------------------------------------
    // MARK: - RESPONSE
    // MARK: -
    //--------------------------------------------------------------

    func handleCLKGoogleDirectionsResponse(_ clkGoogleDirectionsResponse: CLKGoogleDirectionsResponse){
        if let routes = clkGoogleDirectionsResponse.routes{
            
            for clkGoogleDirectionRoute : CLKGoogleDirectionRoute in routes{
                //---------------------------------------------------------------------
                //ROUTE
                //---------------------------------------------------------------------
                /*
                "legs" : [
                {
                    "distance" : {
                         "text" : "6.1 km",
                         "value" : 6141
                    },
                    "duration" : {
                    "text" : "19 mins",
                    "value" : 1141
                    },
                    "end_address" : "2A Woolridge Way, Loddiges Rd, London E9 6PP, UK",
                    "end_location" : {
                    "lat" : 51.54244869999999,
                    "lng" : -0.049196
                    },
                    "start_address" : "14 E Smithfield, London E1W 1AP, UK",
                    "start_location" : {
                    "lat" : 51.50841639999999,
                    "lng" : -0.0718461
                },
                 
                 v1
                 //one leg
                 https://maps.googleapis.com/maps/api/directions/json?language=en-GB&origin=51.50834635291438,-0.07175035301466454&destination=51.47121047973633,-0.489874005317688&key=AIzaSyBRBz99dZS2LnD3h6hFJ5_pOCiWD2JB-1o&transit_mode=driving&
                 
                 v2
                 //mode=walking
                 //nearest taxi
                 https://maps.googleapis.com/maps/api/directions/json?language=en-GB&origin=51.50835708148136,-0.07180972857748918&destination=51.50901794433594,-0.07325699925422668&mode=walking&key=AIzaSyBRBz99dZS2LnD3h6hFJ5_pOCiWD2JB-1o&
                 
                 mode=walking - only 1 leg with n steps as you walk the whole way
                 https://maps.googleapis.com/maps/api/directions/json?language=en-GB&origin=51.50835708148136,-0.07180972857748918&destination=51.47121047973633,-0.489874005317688&mode=walking&key=AIzaSyBRBz99dZS2LnD3h6hFJ5_pOCiWD2JB-1o&
                 
            */
        
        
        
                if let legs: [CLKGoogleLeg] = clkGoogleDirectionRoute.legs{
                    if legs.count > 0{
                        
                        if legs.count != 1{
                            appDelegate.log.error("legs.count != 1")
                            
                        }else{
                            //----------------------------------------------------------------------------------------
                            //FIRST LOG ONLY
                            //----------------------------------------------------------------------------------------
                            // TODO: - Support multiple legs
                            let legFirst: CLKGoogleLeg = legs[0]
                            //---------------------------------------------------------------------
                            // distance: CLKGoogleMeasurement
                            //---------------------------------------------------------------------
                            if let distance: CLKGoogleMeasurement = legFirst.distance {
                                //----------------------------------------------------------------------------------------
                                self.log.debug("distance[distanceMetricSafe]:\(distance.distanceMetricSafe)")
                                self.log.debug("distance[distanceMilesSafe]:\(distance.distanceMilesSafe)")
                                
                                
                            }else{
                                appDelegate.log.error("legFirst.distance is nil - cant get trip info")
                            }
                            //---------------------------------------------------------------------
                            // distance: CLKGoogleMeasurement
                            //---------------------------------------------------------------------
                            if let duration: CLKGoogleMeasurement = legFirst.duration {
                                
                                if let text = duration.text {
                                    self.log.debug("duration[text]:\(text)")
                                }else{
                                    appDelegate.log.error("duration.text is nil")
                                }
                                
                                if let value = duration.value {
                                    self.log.debug("duration[secs]:\(value)")
                                }else{
                                    appDelegate.log.error("distance.text is nil")
                                }
                                
                            }else{
                                appDelegate.log.error("legFirst.duration is nil - cant get trip info")
                            }
                            //----------------------------------------------------------------------------------------
                            //FIRST LEG ONLY - END
                            //----------------------------------------------------------------------------------------
                        }
                        
                    }else{
                        appDelegate.log.error("clkGoogleDirectionRoute.legs. count not > 0 - cant get trip info")
                    }
                }else{
                    appDelegate.log.error("clkGoogleDirectionRoute.legs is nil - cant get trip info")
                }
                //---------------------------------------------------------------------
                //POLYLINE - each step has their own polyline
                //---------------------------------------------------------------------
                if let overview_polyline: CLKGooglePolyline = clkGoogleDirectionRoute.overview_polyline{
                    if let polyline:Polyline = overview_polyline.polyline {
                        
                        if let coordinates : [CLLocationCoordinate2D] = polyline.coordinates {
                            print("\(coordinates.count)")
                            
                            let path = GMSMutablePath()
                            
                            for location : CLLocationCoordinate2D in coordinates {
                                path.add(location)
                            }
                            
                            //remove old trip line
                            if let tripGMSPolyline = self.tripGMSPolyline{
                                
                                tripGMSPolyline.map = nil
                                self.tripGMSPolyline = nil
                            }else{
                                
                            }
                            //add new line
                            self.tripGMSPolyline = GMSPolyline(path: path)
                            self.tripGMSPolyline?.strokeWidth = 3.0
                            self.tripGMSPolyline?.strokeColor = AppearanceManager.appColorNavbarAndTabBar
                            self.tripGMSPolyline?.map = self.gmsMapView
                            
                        }else{
                            self.log.error("polyline.coordinates is nil")
                        }
                    }else{
                        self.log.error("overview_polyline.polyline is nil")
                    }
                }else{
                    self.log.error("clkGooglePlaceResult.name is nil")
                }
                //---------------------------------------------------------------------
                //Route Bounds - zoom map
                //---------------------------------------------------------------------
                
                if let bounds: CLKGooglePlaceViewport = clkGoogleDirectionRoute.bounds{
                    if let bounds_gmsCoordinateBounds = bounds.gmsCoordinateBounds {
                        if let gmsMapView = self.gmsMapView {
                            
                            //space at bottom for the MY LOCATION button
                            if let gmsCameraPosition = gmsMapView.camera(for: bounds_gmsCoordinateBounds, insets: UIEdgeInsetsMake(50, 50, 200, 50)) {
                                gmsMapView.animate(to: gmsCameraPosition)
                            }else{
                                appDelegate.log.error("gmsCameraPosition is nil")
                            }

                        }else{
                            self.log.error("self.gmsMapView is nil")
                        }
                    }else{
                        self.log.error("bounds.gmsCoordinateBounds is nil")
                    }
                }else{
                    self.log.error("clkGooglePlaceResult.name is nil")
                }
                
                //---------------------------------------------------------------------
            }//for
        }else{
            self.log.error("clkGoogleDirectionsResponse.result is nil - OK if /api/directions use .results")
        }
        
        //----------------------------------------------------------------------------------------
        //Return to UI - to fill labels
        //----------------------------------------------------------------------------------------
        if let delegate = self.delegate {
            delegate.colcGoogleDirectionsUIManager(self, clkGoogleDirectionsResponse: clkGoogleDirectionsResponse)
        }else{
            appDelegate.log.error("self.<#EXPR#> is nil")
        }
    }
    
    func handleError(_ error: Error){
        var showErrorAlert = true
        switch error{
            
        case AppError.googlePlacesResponseStatus_ZERO_RESULTS_DISTANCE:
            self.log.debug("status: ZERO_RESULTS")
            //------------------------------------------------------------------------------------------------
            //DONT SHOW ERROR ALERT FOR ZERO RESULTS
            showErrorAlert = false
            
            //                        //remove all results
            //                        self.nearbySearch_CLKGooglePlaceResultsArray = [CLKGooglePlaceResult]()
            //                        self.tableViewPlacesResults.reloadData()
            //
            //                        //------------------------------------------------------------------------------------------------
            //                        self.labelAddress.text = ""
            //                        //------------------------------------------------------------------------------------------------
            
            //------------------------------------------------------------------------------------------------
            
        case AppError.googlePlacesResponseStatus_OVER_QUERY_LIMIT:
            self.log.debug("status: OVER_QUERY_LIMIT")
            
        case AppError.googlePlacesResponseStatus_REQUEST_DENIED:
            self.log.debug("status: REQUEST_DENIED")
            
        case AppError.googlePlacesResponseStatus_INVALID_REQUEST:
            
            self.log.debug("status: INVALID_REQUEST")
            
        default:
            self.log.error("UNKNOWN error: \(error)")
        }
        
        if showErrorAlert{
            if let parentViewController = self.parentViewController {
                CLKAlertController.showAlertInVCWithCallback(parentViewController, title: "Error", message: error.localizedDescription)
                    { () -> Void in
                        self.log.debug("Ok tapped = alert closed appDelegate.doLogout()")
                }
            }else{
                self.log.error("parentViewController is nil")
            }
        }
    }
    
    //if user clears Start or End remove the line in between
    func removeTripFromMap(){
        
        if let tripGMSPolyline = self.tripGMSPolyline {
            tripGMSPolyline.map = nil
        }else{
            self.log.error("self.tripGMSPolyline is nil")
        }
    }

}
