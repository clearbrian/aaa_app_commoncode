//
//  CLKGoogleDirectionRoute.swift
//  joyride
//
//  Created by Brian Clear on 25/01/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation



class CLKGoogleDirectionRoute: ParentMappable {
    
    var summary:String?
    
    var overview_polyline: CLKGooglePolyline?
    var waypoint_order: [AnyObject]? //empty array in testing [] so left at AnyObject
    var bounds:CLKGooglePlaceViewport?    //used in Places api as well
    var copyrights:String?
    
    // TODO: - test if start end in different countries
    var warnings: [AnyObject]? //empty array in testing [] so left at AnyObject - may be if start and end in different countries
    
    var legs: [CLKGoogleLeg]?
    
    
    // root calls mapping()
    //    required init?(_ map: Map){
    //        mapping(map: map)
    //    }
    
    override func mapping(map: Map){
        summary <- map["summary"]
        overview_polyline <- map["overview_polyline"]
        waypoint_order <- map["waypoint_order"]
        bounds <- map["bounds"]
        copyrights <- map["copyrights"]
        warnings <- map["warnings"]
        legs <- map["legs"]
    }
}
/*
"geometry" : {
"location" : {
"lat" : 51.509664,
"lng" : -0.086253
}
},
*/
