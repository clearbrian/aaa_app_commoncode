//
//  CLKGoogleDuration.swift
//  joyride
//
//  Created by Brian Clear on 25/01/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation

extension CLKGoogleMeasurement{
    
    //km or m
    var distanceMetricSafe: String{
        var distanceMetricSafe_ : String = ""
        //---------------------------------------------------------
        //google shows 0.9 km - I want to say meters if its less than 1000m
        if let metersNumber: NSNumber = self.value {

            let metersDouble = metersNumber.doubleValue
            
            if metersDouble < 1000{
                distanceMetricSafe_  = "\(metersDouble) m"
            }else{
                let kmDouble  = metersDouble/1000.0
                
                let kmNumber = NSNumber.init(value: kmDouble)
                
                let formatter = NumberFormatter()
                formatter.numberStyle = .decimal
                formatter.minimumFractionDigits = 2
                formatter.maximumFractionDigits = 2
                
                if let kmString = formatter.string(from: kmNumber){
                    distanceMetricSafe_ = "\(kmString) km"
                    
                }else{
                    appDelegate.log.error("format is nil - could not format: kmNumber:\(kmNumber)")
                }
            }

        }else{
            appDelegate.log.error("distance.text is nil")
        }
        //---------------------------------------------------------
        return distanceMetricSafe_
    }
    
    var distanceMilesSafe: String{
        var distanceMilesSafe : String = ""
        //--------------------------------------------------------
        
        if let distanceValueMeters: NSNumber = self.value {
            //self.log.debug("distance[meters]:\(value)")
            
            if let milesNumber = CLKDistanceConvertor.metersToMilesString2DecPlaces(distanceValueMeters: distanceValueMeters) {
                distanceMilesSafe = "\(milesNumber) miles"
            }else{
                appDelegate.log.error("CLKDistanceConvertor.metersToMilesString2DecPlaces is nil")
            }
            
        }else{
            appDelegate.log.error("distance. value is nil")
        }
        //---------------------------------------------------------
        return distanceMilesSafe
    }

    //--------------------------------------------------------------
    // MARK: - miles
    // MARK: -
    //--------------------------------------------------------------

    /*
     "distance": {
     
     "text": "35.4 km",
     "value": 35422
     
     },
     
     */
    
    var miles: NSNumber?{
        var miles_ : NSNumber? = nil
        
        //value is meters 35422
        if let distanceValueMeters: NSNumber = self.value {
            //self.log.debug("distance[meters]:\(value)")
            
            if let milesNumber = CLKDistanceConvertor.metersToMiles(distanceValueMeters: distanceValueMeters) {
                miles_ = milesNumber
            }else{
                appDelegate.log.error("CLKGoogleMeasurement.metersToMiles is nil")
            }

        }else{
            appDelegate.log.error("distance. value is nil")
        }
        return miles_
    }
    
    
    
}

class CLKGoogleMeasurement: ParentMappable {
    
    var value: NSNumber?
    var text: String?
    
    // root calls mapping()
    //    required init?(_ map: Map){
    //        mapping(map: map)
    //    }
    
    override func mapping(map: Map){
        value <- map["value"]
        text <- map["text"]
    }
}
