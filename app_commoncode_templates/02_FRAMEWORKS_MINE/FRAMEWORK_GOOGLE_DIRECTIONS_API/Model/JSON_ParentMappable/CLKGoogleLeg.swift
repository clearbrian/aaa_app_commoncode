//
//  CLKGoogleLeg.swift
//  joyride
//
//  Created by Brian Clear on 25/01/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation


class CLKGoogleLeg: ParentMappable {
   
    var via_waypoint: [AnyObject]? //empty array in testing [] so left at AnyObject
    var duration: CLKGoogleMeasurement?
    var points:String?
    
    var start_location: CLKGooglePlaceLocation?
    var end_location: CLKGooglePlaceLocation?
    var distance: CLKGoogleMeasurement?
    
    var start_address:String?
    var end_address:String?
    
    var steps: [CLKGoogleDistanceStep]?
    var traffic_speed_entry: [AnyObject]?
    
    
    // root calls mapping()
    //    required init?(_ map: Map){
    //        mapping(map: map)
    //    }
    
    override func mapping(map: Map){
        via_waypoint <- map["via_waypoint"]
        duration <- map["duration"]
        points <- map["points"]
        
        start_location <- map["start_location"]
        end_location <- map["end_location"]
        
        distance <- map["distance"]
        
        start_address <- map["start_address"]
        end_address <- map["end_address"]
        
        steps <- map["steps"]
        traffic_speed_entry <- map["traffic_speed_entry"]
        
    }
}

