//
//  CLKGoogleDirectionsParentRequest.swift
//  joyride
//
//  Created by Brian Clear on 22/01/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation

//
//  CLKPlacesWSQuery.swift
//  joyride
//
//  Created by Brian Clear on 03/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation
//import Alamofire



class CLKGoogleDirectionsParentRequest : ParentWSRequest{
    
    
    //------------------------------------------------------------------------------------------------
    //404 - let kWEBSERVICE_ENDPOINT_GooglePlaces:String = "https://maxxxps.googleapis.com/maps/api/place"
    //404 - let kWEBSERVICE_ENDPOINT_GooglePlaces:String = "https://maps.googleapis.com/maps/api/placeXXX"
    
    //---------------------------------------------------------------------
    //e.g.
    //https://maps.googleapis.com/maps/api/place/details/output?parameters
    //------------------------------------------------------------------------------------------------
    
    
    //let webServiceEndpoint_GoogleDirections:String = "https://maps.googleapis.com/maps/api/directions"
    //directions/json is added in CLKGoogleDirectionsRequest
    let webServiceEndpoint_GoogleDirections:String = "https://maps.googleapis.com/maps/api"
    
    
    //should be set by subclass
    
    override init(){
        super.init()
        
    }
    
    var language: String? {
        var language_: String?
        
        if let googleSupportedLanguageCodeForDeviceLanguage = GoogleApi.find_googleSupportedLanguageCodeForDeviceLanguage(){
            
            language_ = googleSupportedLanguageCodeForDeviceLanguage
            
        }else{
            self.log.error("googleSupportedLanguageCodeForDeviceLanguage is nil - should be set in appD")
        }
        
        return language_
    }
    
    override var urlString :String? {
        get {
            var urlString_ :String?
            
            if let query = query{
                
                urlString_ = "\(self.webServiceEndpoint_GoogleDirections)\(query)"
                
                let _ = parametersAppendAndCheckRequiredFieldsAreSet()
                
            }else{
                self.log.error("query is nil - should be set by subclass")
            }
            
            return urlString_
        }
    }
    
    override func parametersAppendAndCheckRequiredFieldsAreSet() -> Bool{
        var requiredFieldsAreSet = false
        
        requiredFieldsAreSet = super.parametersAppendAndCheckRequiredFieldsAreSet()
        if requiredFieldsAreSet{
            
            //DEBUG - comment out "key" to trigger "REQUEST_DENIED"
            //status = "REQUEST_DENIED";
            
            //ADD THE GOOGLE API KEYS - stored in Delegate
            //Two key WEB and IOS - I use WEB
            //parameters["key"] = GoogleApiKeys.googleMapsApiKey_Web
            //parameters["key"] = GoogleApiKeys.googleMapsApiKey_iOS
            //press GET A KEY on API docs created server key not ios key
            
            //SWIFT3 
            //parameters["key"] = GoogleApiKeys.googleMapsApiKey_Serverkey
            //parameters["key"] = GoogleApiKeys.googleMapsApiKey_Serverkey as AnyObject
            parameters["key"] = GoogleApiKeys.googleMapsApiKey_Web as AnyObject
            
            
            requiredFieldsAreSet = true
        }else{
            //required param missing in super class
        }
        
        return requiredFieldsAreSet
    }
}
