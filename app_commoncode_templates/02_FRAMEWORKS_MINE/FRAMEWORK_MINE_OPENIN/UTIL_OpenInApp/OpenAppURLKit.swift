//
//  OpenInAppKit.swift
//  joyride
//
//  Created by Brian Clear on 12/05/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit
import StoreKit
import SafariServices
//--------------------------------------------------------------
// MARK: - OpenAppURLKit
// MARK: -
//--------------------------------------------------------------

// TODO: - REMOVE THIS use subclasses instead
class OpenAppURLKit: ParentNSObject, SKStoreProductViewControllerDelegate{
    
    // TODO: - remove failure: should show message
    func openAppNoClosures(_ openAppConfig: OpenAppConfig,
                                presentingViewController: UIViewController)
    {
        self.openApp(openAppConfig: openAppConfig,
                                          presentingViewController: presentingViewController,
                                          
                                          success:{() -> Void in
                                            self.log.debug("openApp: success")
            },
                                          failure:{(errorMessage) -> Void in
                                            CLKAlertController.showAlertInVC(presentingViewController, title: "Error opening application", message: errorMessage)
                                            //should be ok but not used if app if not foudn in search then pointless opening itunes web page
                                            //appDelegate.openAppURLKit.openiTunesPageInSafari(openAppConfig: openAppConfig, presentingViewController: presentingViewController)
                                            
            }
        )
    }
    
    
    func openApp(openAppConfig: OpenAppConfig,
        presentingViewController: UIViewController,
        success: () -> Void,
        failure: (_ errorMessage : String) -> Void
        )
    {
        
        // TODO: - REMOVE THIS WHOLE CLASS use subclasses instead
        switch openAppConfig {
            
        case let openAppConfigDeepLinking as OpenAppConfigDeepLinking:
            //------------------------------------------------
            //DEEP LINKING
            //------------------------------------------------
            open_OpenAppConfigOtherApp(openAppConfigDeepLinking,
                                          presentingViewController: presentingViewController,
                                          
                                          success:{() -> Void in
                                            self.log.debug("openAppConfigDeepLinking: success")
                },
                                          failure:{(errorMessage) -> Void in
                                            CLKAlertController.showAlertInVC(presentingViewController, title: "Error opening application", message: errorMessage)
                                            //should be ok but not used if app if not foudn in search then pointless opening itunes web page
                                            //appDelegate.openAppURLKit.openiTunesPageInSafari(openAppConfig: openAppConfig, presentingViewController: presentingViewController)
                                            
                }
            )
            
            
        case let openAppConfigClipboard as OpenAppConfigClipboard:
            //------------------------------------------------
            //CLIP BOARD
            //------------------------------------------------
//            open_OpenAppConfigClipboard(openAppConfigClipboard,
//                                          presentingViewController: presentingViewController,
//                                          
//                                          success:{() -> Void in
//                                            self.log.debug("openAppConfigDeepLinking: success")
//                },
//                                          failure:{(errorMessage) -> Void in
//                                            CLKAlertController.showAlertInVC(presentingViewController, title: "Error opening application", message: errorMessage)
//                                            //should be ok but not used if app if not foudn in search then pointless opening itunes web page
//                                            //appDelegate.openAppURLKit.openiTunesPageInSafari(openAppConfig: openAppConfig, presentingViewController: presentingViewController)
//                                            
//                }
//            )
            open_OpenAppConfigOtherApp(openAppConfigClipboard,
                                        presentingViewController: presentingViewController,
                                        
                                        success:{() -> Void in
                                            self.log.debug("openAppConfigDeepLinking: success")
                },
                                        failure:{(errorMessage) -> Void in
                                            CLKAlertController.showAlertInVC(presentingViewController, title: "Error opening application", message: errorMessage)
                                            //should be ok but not used if app if not foudn in search then pointless opening itunes web page
                                            //appDelegate.openAppURLKit.openiTunesPageInSafari(openAppConfig: openAppConfig, presentingViewController: presentingViewController)
                                            
                }
            )
            
        case let openAppConfigCMMapLauncher as OpenAppConfigCMMapLauncher:
            //------------------------------------------------
            //CMMapLauncher
            //------------------------------------------------
            open_OpenAppConfigOtherApp(openAppConfigCMMapLauncher,
                                       presentingViewController: presentingViewController,
                                       
                                       success:{() -> Void in
                                        self.log.debug("open_OpenAppConfigOtherApp: success")
                },
                                       failure:{(errorMessage) -> Void in
                                        CLKAlertController.showAlertInVC(presentingViewController, title: "Error opening application", message: errorMessage)
                                        //should be ok but not used if app if not foudn in search then pointless opening itunes web page
                                        //appDelegate.openAppURLKit.openiTunesPageInSafari(openAppConfig: openAppConfig, presentingViewController: presentingViewController)
                                        
                }
            )
            
        case let openAppConfigSafari as OpenAppConfigSafari:
            
            //------------------------------------------------
            //WEBSITE
            //------------------------------------------------
            
            open_OpenAppConfigWebsite(openAppConfigSafari,
                                       presentingViewController: presentingViewController,
                                       
                                       success:{() -> Void in
                                        self.log.debug("open_OpenAppConfigWebsite: success")
                },
                                       failure:{(errorMessage) -> Void in
                                        CLKAlertController.showAlertInVC(presentingViewController, title: "Error opening application", message: errorMessage)
                                        //should be ok but not used if app if not foudn in search then pointless opening itunes web page
                                        //appDelegate.openAppURLKit.openiTunesPageInSafari(openAppConfig: openAppConfig, presentingViewController: presentingViewController)
                                        
                }
            )
            
        default:
            self.log.error("ERROR - openAppConfig UNHANLDED SUBCLASS \(openAppConfig)")
        }

    }
    
    //--------------------------------------------------------------
    // MARK: - <#COMMENT#>
    // MARK: -
    //--------------------------------------------------------------
    func open_OpenAppConfigClipboard(_ openAppConfigClipboard: OpenAppConfigClipboard,
                                   presentingViewController: UIViewController,
                                   success: () -> Void,
                                   failure: (_ errorMessage : String) -> Void
        )
    {
        self.log.debug("TODO open_OpenAppConfigWebsite OpenAppConfigClipboard")
    }
    
    
    
    func open_OpenAppConfigWebsite(_ openAppConfigWebsite: OpenAppConfigWebsite,
                                   presentingViewController: UIViewController,
                                   success: () -> Void,
                                   failure: (_ errorMessage : String) -> Void
        )
    {
        self.log.debug("TODO open_OpenAppConfigWebsite OpenAppConfigSafari")
    }
    
    
    func open_OpenAppConfigOtherApp(_ openAppConfigOtherApp: OpenAppConfigOtherApp,
                                       presentingViewController: UIViewController,
                                       success: @escaping () -> Void,
                                       failure: @escaping (_ errorMessage : String) -> Void
        )
    {
        if openAppConfigOtherApp.appSchemeString == "" {
            failure("openAppConfigOtherApp.appSchemeString == ''")
            
        }else{
            //------------------------------------------------
            //Scheme is not blank
            //------------------------------------------------
            self.log.debug("[open_OpenAppConfigOtherApp] OPEN APP:'\(openAppConfigOtherApp.appSchemeString)'")
            
            if let appSchemeURL = URL(string:openAppConfigOtherApp.appSchemeString){
                
                if (UIApplication.shared.canOpenURL(appSchemeURL))
                {
                    
                    //let urlStringToOpen: String?

                    //---------------------------------------------------------------------
                    //App is installed - open the app using deeplinking
                    //---------------------------------------------------------------------
                    // TODO: - swift3 untested
                    if let urlStringToOpen = openAppConfigOtherApp.urlStringToOpen {
                        
                        if let url = URL(string:urlStringToOpen){
                            UIApplication.shared.openURL(url)
                            success()
                            
                        }else{
                            self.log.error("url is nil - not valid:")
                            failure("appDeepLinkURLString is invalid")
                        }
                    }else{
                        self.log.error("urlStringToOpen is nil")
                    }
                    
                }
                else
                {
                    //-----------------------------------------------------------------------------------
                    //APP is not installed - ask user would they like to search app store
                    //------------------------------------------------
                    
                    CLKAlertController.showAlertInVCWithCancelAndOkHandler(presentingViewController,
                                                                           title: "App \(openAppConfigOtherApp.appName) is not installed",
                                                                           buttonTitleDefault: "Yes",
                                                                           buttonTitleCancel: "No",
                                                                           message: "Would you like to search for it on the App Store?"
                    ){ () -> Void in
                        //user pressed OK to search
                        //------------------------------------------------
                        //Search for app id in Store
                        //------------------------------------------------
                        self.searchInAppStore(openAppConfigOtherApp, presentingViewController: presentingViewController,
                                              success:{() -> Void in
                                                self.log.debug("searchIdInAppStore: success")
                                                success()
                                                
                            },
                                              failure:{(errorMessage) -> Void in
                                                failure(errorMessage)
                            },
                                              failureSearchInAppStore:{(errorMessage) -> Void in
                                                
                                                //-----------------------------------------------------------------------------------
                                                //APP id not foudn in Store - fallback to website
                                                //------------------------------------------------
                                                
                                                CLKAlertController.showAlertInVCWithCancelAndOkHandler(presentingViewController,
                                                    title: "App \(openAppConfigOtherApp.appName) not found in App Store",
                                                    buttonTitleDefault: "Yes",
                                                    buttonTitleCancel: "No",
                                                    message: "Would you like to open the app website in Safari?"
                                                ){ () -> Void in
                                                    //user pressed OK to search
                                                    //------------------------------------------------
                                                    //Search for app id in Store
                                                    //------------------------------------------------
                                                    //self.openiTunesPageInSafari(openAppConfig: openAppConfig, presentingViewController: presentingViewController)
                                                    
                                                    //self.openAppWebsitePageInExternalSafari(openAppConfigOtherApp: openAppConfigOtherApp, presentingViewController: presentingViewController)
                                                    
                                                    //Safari but stays in app
                                                    //TO TEST set appStoreIdUInt to 1 - not to 0
                                                    self.openAppWebsitePageInSFSafariViewController(openAppConfigOtherApp: openAppConfigOtherApp, presentingViewController: presentingViewController)
                                                    
                                                    //-----------------------------------------------------------------------------------
                                                }//showAlertInVCWithCancelAndOkHandler
                            }
                            
                            
                        )//searchInAppStore
                        //-----------------------------------------------------------------------------------
                    }//showAlertInVCWithCancelAndOkHandler
                    //-----------------------------------------------------------------------------------
                }//if
            }else{
                failure("appSchemeURL is nil - openAppConfig.appSchemeString:'\(openAppConfigOtherApp.appSchemeString)' not valid")
            }
        }
    }
    
    
    
    
    //--------------------------------------------------------------
    // MARK: - StoreKit
    // MARK: -
    //--------------------------------------------------------------

    var storeProductViewController : SKStoreProductViewController?
    
    func searchInAppStore(_ openAppConfigOtherApp: OpenAppConfigOtherApp,
                        presentingViewController: UIViewController,
        success: @escaping () -> Void,
        failure: (_ errorMessage : String) -> Void,
        failureSearchInAppStore: @escaping (_ errorMessage : String) -> Void
        )
    {
        if openAppConfigOtherApp.appStoreIdUInt > 0{
            self.storeProductViewController = SKStoreProductViewController()
            
            if let storeProductViewController = self.storeProductViewController {
                
                storeProductViewController.delegate = self
                //-----------------------------------------------------------------------------------
                storeProductViewController.loadProduct(
                    withParameters: [SKStoreProductParameterITunesItemIdentifier: NSNumber(value: openAppConfigOtherApp.appStoreIdUInt as UInt)],
                    
                    completionBlock: {
                        (result: Bool, error: Error?) -> Void in
                        
                        //-----------------------------------------------------------------------------------
                        //if app not released yet then user will see blank screen - when they hit Cancel they get error domain 0
                        //-----------------------------------------------------------------------------------
                        if let error = error {
                            failureSearchInAppStore("error:[\(error)]")
                            
                        }else{
                            //no error
                            if result == false{
                                print("TODO fall back to iTunes url")
                                //self.handle(iTunesURLString)
                                failureSearchInAppStore("error:[result == false]")
                            }else{
                                self.log.error("storeProductViewController. loadProductWithParameters: result is true")
                                success()

                            }
                        }
                        //move down
                        //self.storeProductViewController = nil
                        //-----------------------------------------------------------------------------------
                    }
                )
                //-----------------------------------------------------------------------------------
                presentingViewController.present(storeProductViewController, animated: true, completion: nil)
                //... productViewControllerDidFinish
                //-----------------------------------------------------------------------------------
                
            }else{
                failure("self.storeProductViewController is nil")
            }
        }else{
            failure("[OpenAppConfig error] appStoreIdUInt > 0 FAILED")
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - SKStoreProductViewControllerDelegate
    // MARK: -
    //--------------------------------------------------------------
    func productViewControllerDidFinish(_ storeProductViewController: SKStoreProductViewController) {
        

        switch UIApplication.shared.applicationState{
        case .active:
            self.log.error("productViewControllerDidFinish called and .Active")
            storeProductViewController.dismiss(animated: true, completion: nil)
            
        case .inactive:
            self.log.error("productViewControllerDidFinish called and .Inactive")
            
        case .background:
            //if user hits Store button the the full App Store app is opened and we enter the background
            self.log.error("productViewControllerDidFinish called and .Background")
           
        }
    }
    
    
    //not used but should work - if id not in itunes search then opening page is pointless
    func openiTunesPageInSafari(openAppConfigOtherApp: OpenAppConfigOtherApp,
                               presentingViewController: UIViewController)
    {
        //needed? if app is installed but wrong
        // //------------------------------------------------
        // //Not installed open website
        // //------------------------------------------------
        if openAppConfigOtherApp.iTunesURLString == ""{
            CLKAlertController.showAlertInVC(presentingViewController, title: "Error opening application", message: "openAppConfigOtherApp.iTunesURLString is blank")
        }else{
            
            if let iTunesURLString = openAppConfigOtherApp.iTunesURLString {
                
                if let iTunesURL = URL(string:iTunesURLString) {
                    UIApplication.shared.openURL(iTunesURL)
                }else{
                    //invalid website URL
                    self.log.error("NSURL to urlBackupWebsite is nil - '\(String(describing: openAppConfigOtherApp.iTunesURLString))' not valid")
                }
            }else{
                self.log.error("openAppConfig.iTunesURLString is nil - did you get the appStoreIdUInt")
            }
            
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - OPEN SAFARI
    // MARK: -
    //--------------------------------------------------------------

     //TO TEST set appStoreIdUInt to 1 - not to 0
    func openAppWebsitePageInExternalSafari(openAppConfigOtherApp: OpenAppConfigOtherApp,
                                              presentingViewController: UIViewController)
    {
        //needed? if app is installed but wrong
        // //------------------------------------------------
        // //Not installed open website
        // //------------------------------------------------
        if openAppConfigOtherApp.iTunesURLString == ""{
            CLKAlertController.showAlertInVC(presentingViewController, title: "Error opening application", message: "openAppConfigOtherApp.iTunesURLString is blank")
        }else{
            if let appWebsiteURLStringURL = URL(string:openAppConfigOtherApp.appWebsiteURLString) {
                UIApplication.shared.openURL(appWebsiteURLStringURL)
            }else{
                //invalid website URL
                self.log.error("NSURL to urlBackupWebsite is nil - '\(openAppConfigOtherApp.appWebsiteURLString)' not valid")
            }
        }
    }
    
   
    //--------------------------------------------------------------
    // MARK: - OPens Safari while staying in the app
    // MARK: -
    //--------------------------------------------------------------
    //TO TEST set appStoreIdUInt to 1 - not to 0
    func openAppWebsitePageInSFSafariViewController(openAppConfigOtherApp: OpenAppConfigOtherApp,
                                                          presentingViewController: UIViewController)
    {
        //needed? if app is installed but wrong
        // //------------------------------------------------
        // //Not installed open website
        // //------------------------------------------------
        if openAppConfigOtherApp.iTunesURLString == ""{
            CLKAlertController.showAlertInVC(presentingViewController, title: "Error opening application", message: "openAppConfigOtherApp.iTunesURLString is blank")
        }else{
            if let appWebsiteURLStringURL = URL(string:openAppConfigOtherApp.appWebsiteURLString) {
                //UIApplication.sharedApplication().openURL(appWebsiteURLStringURL)
                
                
                let svc = SFSafariViewController(url: appWebsiteURLStringURL)
                presentingViewController.present(svc, animated: true, completion: nil)
                
            }else{
                //invalid website URL
                self.log.error("NSURL to urlBackupWebsite is nil - '\(openAppConfigOtherApp.appWebsiteURLString)' not valid")
            }
        }
    }
}


extension SKStoreProductViewController{
    
    open override var preferredStatusBarStyle : UIStatusBarStyle {
        //should be set in
        //ParentTableViewController
        //ParentViewController
        //extension UINavigationController
        //return .LightContent
        //return AppearanceManager.preferredStatusBarStyle
        return .lightContent
    }
}
