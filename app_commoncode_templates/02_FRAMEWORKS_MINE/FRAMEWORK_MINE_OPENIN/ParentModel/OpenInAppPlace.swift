//
//  OpenInAppPlace.swift
//  joyride
//
//  Created by Brian Clear on 06/06/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
//UberPlace/LyftPlace/HailoPlace
class OpenInAppPlace: ParentSwiftObject{
    
   
    
    
    
//    static func nameForCLKPlace(_ clkPlace: CLKPlace, nameToShowFallback: String) -> String{
//        var nameToShow = ""
//        //------------------------------------------------------------------------------------------------
//        //NAME
//        //------------------------------------------------------------------------------------------------
//        //if geocode then name not set
//        //.GeoCodeLocation name is blank so put on in then overwrite if it is set
//        
//        
//        refactor this into - .display_name()
//        
//        
//        if let name = clkPlace.name{
//            nameToShow = name
//        }else{
//            
//            if let formatted_address_first_line = clkPlace.formatted_address_first_line {
//                nameToShow = formatted_address_first_line
//            }else{
//                print("ERROR:clkPlace.name & clkPlace.formatted_address_first_line is nil - could not get formatted_address[0]")
//                //"PICKUP"
//                //"DESTINATION"
//                nameToShow = nameToShowFallback
//                
//            }
//        }
//        return nameToShow
//    }
    
    static func localityForCLKPlace(_ clkPlace: CLKPlace) -> String?{
        //------------------------------------------------------------------------------------------------
        //LOCALITY
        //------------------------------------------------------------------------------------------------
        // TODO: - test this in unknown locality
        var locality : String?
        
        //---------------------------------------------------------------------
        //1. use locality for the place picked - trip could be abroad and nor current location
        //---------------------------------------------------------------------
        if let clkPlace_locality = clkPlace.locality {
            locality = clkPlace_locality
            
        }else{
            print("ERROR: clkPlace.locality is nil - trying current location")
            
            //---------------------------------------------------------------------
            //2. fall back to device location > GMSAddress.locality
            //---------------------------------------------------------------------
            if let currentLocation_locality = appDelegate.currentLocationManager.currentLocation_locality {
                locality = currentLocation_locality
                
            }else{
                // TODO: - if user has location turned off then they need to enter it manually
                appDelegate.log.error("appDelegate.currentLocation_locality is nil")
                
            }
        }
        return locality
    }
}
