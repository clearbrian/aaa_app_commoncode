//
//  OpenAppConfigSafari.swift
//  joyride
//
//  Created by Brian Clear on 27/05/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit
class OpenAppConfigSafari: OpenAppConfigWebsite{

    //------------------------------------------------------------------------------------------------
    //init
    //------------------------------------------------------------------------------------------------
    init(openWithType:OpenWithType, appWebsiteURLString: String)
    {
        super.init(openWithType: openWithType, appWebsiteURLString:appWebsiteURLString, openWithSource: .OpenWithSource_Safari)

    }
    override func open(_ presentingViewController: UIViewController,
                       clkPlaceStart: CLKPlace,
                       clkPlaceEnd: CLKPlace,
                       openWithTravelMode: OpenWithTravelMode,
                       success: () -> Void,
                       failure: (_ errorMessage : String) -> Void){
        self.log.error("TODO OpenAppConfigSafari")
    }
}
