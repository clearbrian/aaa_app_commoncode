//
//  OpenWithTypes.swift
//  joyride
//
//  Created by Brian Clear on 27/05/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
// TODO: - change to OpenWithApp
enum OpenWithType: Int{
    
    //use Int so we can store type in db safer than a string
    case openWithType_Uber = 0	//"Uber"
    case openWithType_Lyft = 1	//"Lyft"
    case openWithType_AddisonLee = 2	//"Addison Lee"
    case openWithType_KuaidiDache = 3	//"Kuaidi Dache (一号专车)"
    case openWithType_DidiChuxing = 4	//"Didi Chuxing (滴滴出行)"
    
    case openWithType_Curb = 5	//"Curb"
    case openWithType_BlaBlaCar = 6	//"BlaBlaCar" //only uses city name
    
    case openWithType_Hailo = 7	//"Hailo"
    case openWithType_Gett = 8	//"Gett"
    case openWithType_Grab = 9	//"Grab"
    case openWithType_OlaCabs = 10	//"OlaCabs"
    
    
    case openWithType_AppleMaps = 11	//"Apple Maps"
    case openWithType_GoogleMaps = 12	//"Google Maps"
    
    case openWithType_CityMapper = 13	//"CityMapper "
    case openWithType_Transit = 14	//"Transit"
    case openWithType_TFL = 15	//"Transport for London (TFL)"
    case openWithType_Waze = 16	//"Waze"
    case openWithType_Navigon = 17	//"Navigon"
    // TODO: - FUTURE: tomtom/kabbbee
    //kabbbee
    //yandexnavi
    //travelline
    //moveit
    //yandexmaps
    //yandexnavi
    
    func name() -> String {
        switch self{
        case .openWithType_Uber:
            return "Uber"
        case .openWithType_Lyft:
            return "Lyft"
        case .openWithType_AddisonLee:
            return "Addison Lee"
        case .openWithType_KuaidiDache:
            return "Kuaidi Dache (一号专车)"
        case .openWithType_DidiChuxing:
            return "Didi Chuxing (滴滴出行)"
            
        case .openWithType_Curb:
            return "Curb"
        case .openWithType_BlaBlaCar:
            return "BlaBlaCar" //only uses city name
            
        case .openWithType_Hailo:
            return "Hailo"
        case .openWithType_Gett:
            return "Gett"
        case .openWithType_Grab:
            return "Grab"
        case .openWithType_OlaCabs:
            return "OlaCabs"
            
        case .openWithType_AppleMaps:
            return "Apple Maps"
        case .openWithType_GoogleMaps:
            return "Google Maps"
            
        case .openWithType_CityMapper:
            return "CityMapper "
        case .openWithType_Transit:
            return "Transit"
        case .openWithType_TFL:
            return "Transport for London (TFL)"
        case .openWithType_Waze:
            return "Waze"
        case .openWithType_Navigon:
            return "Navigon"
            
        }
    }
    
    func openInDescription() -> String {
        switch self{
        case .openWithType_Uber:
            return "Open your trip in Uber"
        case .openWithType_Lyft:
            return "Open your trip in Lyft"
        case .openWithType_AddisonLee:
            return "Open your trip in Addison Lee"
        case .openWithType_KuaidiDache:
            return "Open your trip in Kuaidi Dache (一号专车)"
        case .openWithType_DidiChuxing:
            return "Open your trip in Didi Chuxing (滴滴出行)"
            
        case .openWithType_Curb:
            return "Open your trip in Curb"
        case .openWithType_BlaBlaCar:
            return "Open your trip in BlaBlaCar" //only uses city name
            
        case .openWithType_Hailo:
            return "Open your trip in Hailo"
        case .openWithType_Gett:
            return "Open your trip in Gett"
        case .openWithType_Grab:
            return "Open your trip in Grab"
        case .openWithType_OlaCabs:
            return "Open your trip in OlaCabs"
            
        case .openWithType_AppleMaps:
            return "See route and traffic"
            
        case .openWithType_GoogleMaps:
            return "See route with traffic in Google Maps"
            
        case .openWithType_CityMapper:
            return "See nearby public transit"
            
        case .openWithType_Transit:
            return "See nearby public transit"
            
        case .openWithType_TFL:
            return "See nearby public transit"
            
        case .openWithType_Waze:
            return "Show best route in Waze"
            
        case .openWithType_Navigon:
            return "Show best route in Navigon"
            
        }
    }
    
    
    
    
    func openAppConfig() -> OpenAppConfig{
        
        switch self{
        case .openWithType_Uber:
            return OpenAppConfigDeepLinkingUber()
            
        case .openWithType_Lyft:
            return OpenAppConfigDeepLinkingLyft()
            
        case .openWithType_AddisonLee:
            return AddisonLeeKit.openAppConfig
            
        case .openWithType_KuaidiDache:
            return KuaidiDacheKit.openAppConfig
            
        case .openWithType_DidiChuxing:
            return DidiChuxingKit.openAppConfig
            
        case .openWithType_Curb:
            return CurbKit.openAppConfig
            
        case .openWithType_BlaBlaCar:
            return BlaBlaCarKit.openAppConfig
            
        case .openWithType_Hailo:
            return OpenAppConfigDeepLinkingHailo()
            
        case .openWithType_Gett:
            return GettTaxiKit.openAppConfig
            
        case .openWithType_Grab:
            return GrabKit.openAppConfig
            
        case .openWithType_OlaCabs:
            return OlaCabsKit.openAppConfig
            
            
        case .openWithType_AppleMaps:
            return AppleMapsKit.openAppConfig
            
        case .openWithType_GoogleMaps:
            return GoogleMapsKit.openAppConfig

        case .openWithType_CityMapper:
            return CityMapperKit.openAppConfig

        case .openWithType_Transit:
            return TransitKit.openAppConfig

        case .openWithType_TFL:
            return TFLKit.openAppConfig

        case .openWithType_Waze:
            return WazeKit.openAppConfig

            
        case .openWithType_Navigon:
            return NavigonKit.openAppConfig
            // TODO: - FUTURE: tomtom/kabbbee etc
            //kabbbee
            //yandexnavi
            //travelline
            //moveit
        }
    }
    
    //convert Int in DB to OpenWithType
    static func openWithTypeForRawValue(_ rawValueToFind: Int) -> OpenWithType?{
        var openWithTypeReturned: OpenWithType?
        switch rawValueToFind{
        case 0:
            openWithTypeReturned = .openWithType_Uber
        case 1:
            openWithTypeReturned = .openWithType_Lyft
        case 2:
            openWithTypeReturned = .openWithType_AddisonLee
        case 3:
            openWithTypeReturned = .openWithType_KuaidiDache
        case 4:
            openWithTypeReturned = .openWithType_DidiChuxing
            
        case 5:
            openWithTypeReturned = .openWithType_Curb
        case 6:
            openWithTypeReturned = .openWithType_BlaBlaCar
            
        case 7:
            openWithTypeReturned = .openWithType_Hailo
        case 8:
            openWithTypeReturned = .openWithType_Gett
        case 9:
            openWithTypeReturned = .openWithType_Grab
        case 10:
            openWithTypeReturned = .openWithType_OlaCabs
            
            
        case 11:
            openWithTypeReturned = .openWithType_AppleMaps
        case 12:
            openWithTypeReturned = .openWithType_GoogleMaps
            
        case 13:
            openWithTypeReturned = .openWithType_CityMapper
        case 14:
            openWithTypeReturned = .openWithType_Transit
        case 15:
            openWithTypeReturned = .openWithType_TFL
        case 16:
            openWithTypeReturned = .openWithType_Waze
        case 17:
            openWithTypeReturned = .openWithType_Navigon
        default:
            appDelegate.log.error("openWithTypeForRawValue : UNHANDLED rawValueToFind:\(rawValueToFind)")
            openWithTypeReturned = nil
        }
        return openWithTypeReturned
    }
    
    
    
}



// TODO: - change to OpenWithMethod
enum OpenWithSource: String{
    case OpenWithSource_Clipboard = "Uses Clipboard"
    case OpenWithSource_DeepLinking = "Open trip in app"
    case OpenWithSource_CMMapLauncher = "Show route, directions or traffic ..."
    case OpenWithSource_Safari = "Open In Safari"
}

enum OpenWithAppGroup: String{
    case OpenWithAppGroup_RideSharing = "Taxis and Rides"
    case OpenWithAppGroup_Maps = "Open route in other apps..."
    case OpenWithAppGroup_Transit = "Transit - Get Directions"
    
}



//enum CMMapApp{
//    case CMMapAppAddissonLee         // AddissonLee
//    case CMMapAppAppleMaps  // Preinstalled Apple Maps
//    case CMMapAppCitymapper // Citymapper
//    case CMMapAppGoogleMaps // Standalone Google Maps App
//    case CMMapAppNavigon    // Navigon
//    case CMMapAppTheTransitApp  // The Transit App
//    case CMMapAppWaze           // Waze
//    case CMMapAppYandex         // Yandex Navigator
//    
//    // TODO: - add here-route
//    //https://github.com/Parklyapp/MapLauncher/blob/master/Source/Maps.swift
//}




//--------------------------------------------------------------
// MARK: - AddisonLee - OpenAppConfigClipboard
// MARK: -
//--------------------------------------------------------------
class AddisonLeeKit : ParentSwiftObject{
    //------------------------------------------------------------------------------------------------
    //https://itunes.apple.com/gb/app/addison-lee-mini-cab-taxi/id718312937?mt=8
    //------------------------------------------------------------------------------------------------
    static let openAppConfig = OpenAppConfigClipboard(openWithType : OpenWithType.openWithType_AddisonLee,
                                                      appSchemeString     : "addlee://",
                                                      appWebsiteURLString : "https://www.addisonlee.com",
                                                      appStoreIdUInt      : 718312937)
}
//-----------------------------------------------------------------------------------

//class AddisonLeeKit : ParentSwiftObject{
//
//    //https://itunes.apple.com/gb/app/addison-lee-mini-cab-taxi/id718312937?mt=8
//    static let openAppConfig = OpenAppConfigCMMapLauncher(openWithType : OpenWithType.OpenWithType_AddisonLee,
//                                                      appSchemeString : "addlee://",
//                                                      appWebsiteURLString : "https://www.addisonlee.com",
//                                                      appStoreIdUInt      : 718312937,
//                                                      cmMapApp:.CMMapAppAddissonLee)
//
//}

//-----------------------------------------------------------------------------------





//--------------------------------------------------------------
// MARK: - CMMapAppKit
// MARK: -
//--------------------------------------------------------------

class AppleMapsKit : ParentSwiftObject{
    //-----------------------------------------------------------------------------------
    
    static let appSchemeStringForApp = CMMapLauncher.urlPrefixForMapApp(.cmMapAppAppleMaps)
    //https://itunes.apple.com/gb/app/addison-lee-mini-cab-taxi/id718312937?mt=8
    static let openAppConfig = OpenAppConfigCMMapLauncher(openWithType : OpenWithType.openWithType_AppleMaps,
                                                          appSchemeString : appSchemeStringForApp,
                                                          appWebsiteURLString : "https://www.apple.com/ios/maps/",
                                                          appStoreIdUInt      : 718312937,
                                                          cmMapApp:.cmMapAppAppleMaps)
    //-----------------------------------------------------------------------------------
}

class CityMapperKit : ParentSwiftObject{
    //-----------------------------------------------------------------------------------
    
    static let appSchemeStringForApp = CMMapLauncher.urlPrefixForMapApp(.cmMapAppCitymapper)
    //https://itunes.apple.com/gb/app/citymapper-transport-app-live/id469463298?mt=8
    static let openAppConfig = OpenAppConfigCMMapLauncher(openWithType : OpenWithType.openWithType_CityMapper,
                                                          appSchemeString : appSchemeStringForApp,
                                                          appWebsiteURLString : "https://www.addisonlee.com",
                                                          appStoreIdUInt      : 469463298,
                                                          cmMapApp:.cmMapAppCitymapper)
    //-----------------------------------------------------------------------------------
}

class GoogleMapsKit : ParentSwiftObject{
    
    static let appSchemeStringForApp = CMMapLauncher.urlPrefixForMapApp(.cmMapAppGoogleMaps)
    //https://itunes.apple.com/gb/app/google-maps-real-time-navigation/id585027354?mt=8
    static let openAppConfig = OpenAppConfigCMMapLauncher(openWithType : OpenWithType.openWithType_GoogleMaps,
                                                          appSchemeString : appSchemeStringForApp,
                                                          appWebsiteURLString : "https://maps.google.com",
                                                          appStoreIdUInt      : 585027354,
                                                          cmMapApp:.cmMapAppGoogleMaps)

}
//-----------------------------------------------------------------------------------

class NavigonKit : ParentSwiftObject{
    
    static let appSchemeStringForApp = CMMapLauncher.urlPrefixForMapApp(.cmMapAppNavigon)
    //https://itunes.apple.com/gb/developer/garmin-wuerzburg-gmbh/id320198400
    static let openAppConfig = OpenAppConfigCMMapLauncher(openWithType : OpenWithType.openWithType_Navigon,
                                                          appSchemeString : appSchemeStringForApp,
                                                          appWebsiteURLString : "http://www.navigon.com/iphone",
                                                          appStoreIdUInt      : 320198400,
                                                          cmMapApp:.cmMapAppNavigon)
}

//-----------------------------------------------------------------------------------

class TransitKit : ParentSwiftObject{
    
    static let appSchemeStringForApp = CMMapLauncher.urlPrefixForMapApp(.cmMapAppTheTransitApp)
    //https://itunes.apple.com/gb/app/transit-app-live-countdowns/id498151501?mt=8
    //http://thetransitapp.com/
    static let openAppConfig = OpenAppConfigCMMapLauncher(openWithType : OpenWithType.openWithType_Transit,
                                                          appSchemeString : appSchemeStringForApp,
                                                          appWebsiteURLString : "http://thetransitapp.com/",
                                                          appStoreIdUInt      : 498151501,
                                                          cmMapApp:.cmMapAppTheTransitApp)

}
//-----------------------------------------------------------------------------------

class WazeKit : ParentSwiftObject{
    //-----------------------------------------------------------------------------------
    
    static let appSchemeStringForApp = CMMapLauncher.urlPrefixForMapApp(.cmMapAppWaze)
    //https://itunes.apple.com/gb/app/waze-gps-navigation-maps-social/id323229106?mt=8
    static let openAppConfig = OpenAppConfigCMMapLauncher(openWithType : OpenWithType.openWithType_Waze,
                                                          appSchemeString : appSchemeStringForApp,
                                                          appWebsiteURLString : "http://www.waze.com/",
                                                          appStoreIdUInt      : 323229106,
                                                          cmMapApp:.cmMapAppWaze)
    //-----------------------------------------------------------------------------------
}
//class YandexKit : ParentSwiftObject{
//    //-----------------------------------------------------------------------------------
//    
//    static let appSchemeStringForApp = CMMapLauncher.urlPrefixForMapApp(.CMMapAppYandex)
//    //https://itunes.apple.com/gb/app/addison-lee-mini-cab-taxi/id718312937?mt=8
//    static let openAppConfig = OpenAppConfigCMMapLauncher(openWithType : OpenWithType.OpenWithType_Waze,
//                                                          appSchemeString : appSchemeStringForApp,
//                                                          appWebsiteURLString : "https://www.addisonlee.com",
//                                                          appStoreIdUInt      : 718312937,
//                                                          cmMapApp:.CMMapAppYandex)
//    //-----------------------------------------------------------------------------------
//}
// TODO: - FUTURE: tomtom
//kabbbee
//yandexnavi
//travelline
//moveit
//kakao - korean





//--------------------------------------------------------------
// MARK: - TFL - OpenAppConfigSafari
// MARK: -
//--------------------------------------------------------------
class TFLKit : ParentSwiftObject{
    
    static let openAppConfig = OpenAppConfigSafariTFL(openWithType : OpenWithType.openWithType_TFL, appWebsiteURLString: "http://www.tfl.com")

}





