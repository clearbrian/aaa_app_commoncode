//
//  COLCFacebookResultEvents.swift
//  joyride
//
//  Created by Brian Clear on 07/03/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import CoreLocation
class COLCFacebookEvent: ParentMappable {
    
    //[end_time, id, start_time, rsvp_status, description, place, name]
    
    var id: NSString?
    var name: NSString?
    var place: COLCFacebookPlace?
    var description: NSString?
    var rsvp_status: NSString?
    var start_time: NSString?
    var end_time: NSString?
    
    
    override func mapping(map: Map){
        
        id <- map["id"]
        name <- map["name"]
        place <- map["place"]
        description <- map["description"]
        rsvp_status <- map["rsvp_status"]
        start_time <- map["start_time"]
        end_time <- map["end_time"]
        
    }
    
    var clLocation : CLLocation?{
        var clLocation :CLLocation?
        
        if let place = place{
            //COLCFacebookPlace
            // var location: COLCFacebookLocation?
            
            if let place_clLocation = place.clLocation{
                clLocation = place_clLocation
                
            }else{
                self.log.error("clPlacemark.location is nil")
            }
        }else{
            self.log.error("place is nil - 333")
        }
        return clLocation
    }
    
    var formattedAddress : String{
        var formattedAddress_ = ""
        if let place = self.place{
            formattedAddress_ = place.formattedAddress
        }else{
            self.log.error("self.place is nil")
        }
        return formattedAddress_;
    }
    
}
