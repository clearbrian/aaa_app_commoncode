//
//  COLCFacebookPlace.swift
//  joyride
//
//  Created by Brian Clear on 08/03/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//


import Foundation
import CoreLocation

class COLCFacebookPlace: ParentMappable {
    
    //[end_time, id, start_time, rsvp_status, description, place, name]
    
    var id: NSString?
    var location: COLCFacebookLocation?
    var name: NSString?
    
    override func mapping(map: Map){
        
        id <- map["id"]
        location <- map["location"]
        name <- map["name"]
        
    }
    
    var clLocation : CLLocation?{
        var clLocation :CLLocation?
        
        if let location = location{
            if let clLocationInner = location.clLocation{
                clLocation = clLocationInner
            }else{
                self.log.error("COLCFacebookPlace location is nil - 5551")
            }
        }else{
            self.log.error("COLCFacebookPlace location is nil - 5552")
        }
        return clLocation
    }
    
    var formattedAddress : String{
        var formattedAddress_ = ""
        if let location = self.location{
            formattedAddress_ = location.formattedAddress
        }else{
            self.log.error("location is nil")
        }
        return formattedAddress_;
    }
    
}

/*
place =                 {
    id = 9189327788;
    location =                     {
        city = London;
        country = "United Kingdom";
        latitude = "51.4860659";
        longitude = "-0.121945";
        street = "372 Kennington Lane";
        zip = "SE11 5HY";
    };
    name = "Vauxhall Tavern";
};
*/
