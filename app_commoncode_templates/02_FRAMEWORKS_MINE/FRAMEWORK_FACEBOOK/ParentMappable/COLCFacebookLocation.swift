//
//  COLCFacebookLocation.swift
//  joyride
//
//  Created by Brian Clear on 08/03/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import CoreLocation

class COLCFacebookLocation: ParentMappable {
    
    //[end_time, id, start_time, rsvp_status, description, place, name]
    
    var city: NSString?
    var country: NSString?
    var latitude: NSNumber?
    var longitude: NSNumber?
    var zip: NSString?
    var street: NSString?
    
    override func mapping(map: Map){
        
        city <- map["city"]
        country <- map["country"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        zip <- map["zip"]
        street <- map["street"]
        
    }
    
    var clLocation : CLLocation?{
        var clLocation :CLLocation?
        
        if let latitude = latitude{
            if let longitude = longitude{
                clLocation = CLLocation(latitude: latitude.doubleValue, longitude: longitude.doubleValue)
            }else{
                self.log.error("COLCFacebookLocation longitude is nil - 343")
            }
        }else{
            self.log.error("COLCFacebookLocation latitude is nil - 555")
        }
        return clLocation
    }
    
    var formattedAddress : String{
        var formattedAddress_ = ""
        
        //------------------------------------------------
        //some locations can be very basic -  e.g Bristol, United Kingdom - need to geocode lat/lng later -
        //------------------------------------------------
        if let street = self.street {
            

            if let city = self.city {
                if let zip = self.zip {
                    if let country = self.country {
                        formattedAddress_ = "\(street), \(city), \(zip), \(country)"
                        
                    }else{
//                        self.log.error("self.country is nil")
                        formattedAddress_ = "\(street),\(city), \(zip)"
                    }
                }else{
//                    self.log.error("self.zip is nil")
                    if let country = self.country {
                        formattedAddress_ = "\(street), \(city), \(country)"
                        
                    }else{
//                        self.log.error("self.country is nil")
                        formattedAddress_ = "\(street), \(city)"
                    }
                }
            }else{
//                self.log.error("self.city is nil")
                if let zip = self.zip {
                    if let country = self.country {
                        formattedAddress_ = "\(street), \(zip), \(country)"
                        
                    }else{
//                        self.log.error("self.country is nil")
                        formattedAddress_ = "\(street), \(zip)"
                    }
                }else{
//                    self.log.error("self.zip is nil")
                    if let country = self.country {
                        formattedAddress_ = "\(street), \(country)"
                        
                    }else{
//                        self.log.error("self.country is nil")
                        
                        if let city = self.city {
                            formattedAddress_ = "\(street), \(city)"
                            
                        }else{
//                            self.log.error("self.city is nil")
                            formattedAddress_ = ""
                        }
                    }
                }
            }
            //------------------------------------------------
        }else{
            //self.log.error("self.street is nil")
            if let city = self.city {
                if let zip = self.zip {
                    if let country = self.country {
                        formattedAddress_ = "\(city), \(zip), \(country)"
                        
                    }else{
                        //self.log.error("self.country is nil")
                        formattedAddress_ = "\(city), \(zip)"
                    }
                }else{
                    //self.log.error("self.zip is nil")
                    if let country = self.country {
                        formattedAddress_ = "\(city), \(country)"
                        
                    }else{
                        //self.log.error("self.country is nil")
                        formattedAddress_ = "\(city)"
                    }
                }
            }else{
                //self.log.error("self.city is nil")
                if let zip = self.zip {
                    if let country = self.country {
                        formattedAddress_ = "\(zip), \(country)"
                        
                    }else{
                        self.log.error("self.country is nil")
                        formattedAddress_ = "\(zip)"
                    }
                }else{
                    //self.log.error("self.zip is nil")
                    if let country = self.country {
                        formattedAddress_ = "\(country)"
                        
                    }else{
                        //self.log.error("self.country is nil")
                        
                        if let city = self.city {
                            formattedAddress_ = "\(city)"
                            
                        }else{
                            //self.log.error("self.city is nil")
                            formattedAddress_ = ""
                        }
                    }
                }
            }
        }
        

        return formattedAddress_;
    }
    
}

/*
location =         {
    city = Manchester;
    country = "United Kingdom";
    latitude = "53.4772987";
    longitude = "-2.2364399";
    zip = M1;
};
*/
