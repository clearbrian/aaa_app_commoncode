//
//  CLKAppleGeocoder.swift
//  joyride
//
//  Created by Brian Clear on 18/10/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit

class  CLKAppleGeocoder: ParentSwiftObject{
    
    
    func forwardGeocode_MKLocalSearch(_ addressString: String,
                                              success: @escaping (_ clkGeocodedAddressesAppleCollection: CLKGeocodedAddressesAppleCollection) -> Void,
                                              failure: @escaping (_ error: Error?) -> Void
        )
    {
        
        //------------------------------------------------------------------------------------------------
        //MKLocalSearchRequest
        //---------------------------------------------------------------------
        let request = MKLocalSearchRequest()
        //---------------------------------------------------------------------
        //request.naturalLanguageQuery = "15 Royal Mint Street London, E1 8LG"
        request.naturalLanguageQuery = addressString
        //---------------------------------------------------------------------
        //request.region = mapView.region
        //---------------------------------------------------------------------
        let search = MKLocalSearch(request: request)
        //------------------------------------------------------------------------------------------------
        
        search.start { response, error in
            if let response = response{
                //------------------------------------------------------------------------------------------------
                //MKLocal Search OK
                //------------------------------------------------------------------------------------------------
                
                //---------------------------------------------------------------------
                //     item:<MKMapItem: 0x1459f64f0> {
                //     isCurrentLocation = 0;
                //     name = "15 Royal Mint Street";
                //     placemark = "15 Royal Mint Street, 15 Royal Mint Street, London, E1 8LG, England @ <+51.51012620,-0.07133710> +/- 0.00m, region CLCircularRegion (identifier:'<+51.51012620,-0.07133710> radius 49.91', center:<+51.51012620,-0.07133710>, radius:49.91m)";
                //     timeZone = "Europe/London (BST) offset 3600 (Daylight)";
                //     }
                //---------------------------------------------------------------------
                
                let clkGeocodedAddressesAppleCollection = CLKGeocodedAddressesAppleCollection()
                
                for mapItem: MKMapItem in response.mapItems {
                    
                    // Display the received items
                    print("mapItem:\(mapItem)")
                    
                    
                    let clkAddressApple = CLKAddressAppleMapKit(mapItem: mapItem)
                    clkGeocodedAddressesAppleCollection.clkAddressAppleArray.append(clkAddressApple)
                    
                }
                success(clkGeocodedAddressesAppleCollection)
                //------------------------------------------------------------------------------------------------

            }else{
                //------------------------------------------------------------------------------------------------
                //MKLocal Search ERROR
                //------------------------------------------------------------------------------------------------
                
                appDelegate.log.error("There was an error searching for: \(String(describing: request.naturalLanguageQuery)) error: \(String(describing: error))")
                if let error = error {
                    print("arrayCLPlacemark is nil: error :\(error)")
                    failure(error)
                    
                }else{
                    appDelegate.log.error("Apple Local Search - failed")
                    failure(NSError.errorWithMessage("Apple Local Search - failed", code: 2233))
                    
                }
            }
        }
    }
    
    
    
    func forwardGeocodeAddressDictionary(_ dictionary: [AnyHashable: Any],
                                              success: @escaping (_ clkGeocodedAddressesAppleCollection: CLKGeocodedAddressesAppleCollection) -> Void,
                                              failure: @escaping (_ error: Error?) -> Void
        )
    {
        //---------------------------------------------------------------------
        //Uses Case: Contact > address > CNPostalAddress >> Dictionary
        //---------------------------------------------------------------------
        let coder = CLGeocoder()
    
        //---------------------------------------------------------------------
        //geocodeAddressDictionary:
        //      You can obtain an address dictionary from an ABPerson by retrieving the kABPersonAddressProperty property.
        //      Alternately, one can be constructed using the kABPersonAddress* keys defined in <AddressBook/ABPerson.h>.
        
        //      note: self.colcContact.addressDictionary uses CNPostalAddress*Key which seems to work as AddressBook deprecated
        //---------------------------------------------------------------------
        //Click on calendar location can return multiples - display on map
        coder.geocodeAddressDictionary(dictionary, completionHandler: { (arrayCLPlacemarks: [CLPlacemark]?, error) -> Void in
            
            if let arrayCLPlacemarks = arrayCLPlacemarks{
                //------------------------------------------------------------------------------------------------
                if arrayCLPlacemarks.isEmpty
                {
                    appDelegate.log.error("arrayCLPlacemark : \(String(describing: error))")
                }
                //------------------------------------------------------------------------------------------------
                appDelegate.log.debug("arrayCLPlacemark :\(arrayCLPlacemarks)")
                appDelegate.log.debug("arrayCLPlacemark.count :\(arrayCLPlacemarks.count)")
                //------------------------------------------------------------------------------------------------

                let clkGeocodedAddressesAppleCollection = CLKGeocodedAddressesAppleCollection()
                for clPlacemark: CLPlacemark in arrayCLPlacemarks{

                    print(clPlacemark)
                    //Commodity Quay, Commodity Quay, London, E1W, England @ <+51.50852580,-0.07217820> +/- 100.00m, region CLCircularRegion (identifier:'<+51.50832210,-0.07233215> radius 25.06', center:<+51.50832210,-0.07233215>, radius:25.06m)
                    
                    let clkAddressApple = CLKAddressApple(clPlacemark: clPlacemark)
                    clkGeocodedAddressesAppleCollection.clkAddressAppleArray.append(clkAddressApple)
                    
                    
                }
                success(clkGeocodedAddressesAppleCollection)
                
            }else{
                //---------------------------------------------------------------------
                //arrayCLPlacemarks is nil
                //---------------------------------------------------------------------
                if let error = error {
                    print("arrayCLPlacemark is nil: error :\(error)")
                    failure(error)
                }else{
                    appDelegate.log.error("arrayCLPlacemark is nil: error :nil")
                    failure(NSError.errorWithMessage("Apple forward geocode failed", code: 7687232))
                }
            }
            
        });
    }
    
    
    //---------------------------------------------------------------------
    // TODO: - reverseGeocode for Apple
    //    //call Apple Geocoder - return all results - may want user to choose one if multiple results
    //    func reverseGeocode(coordinate: CLLocationCoordinate2D,
    //                        success: @escaping (_ clkGeocodedAddressesResponse: CLKGeocodedAddressesResponse) -> Void,
    //                        failure: @escaping (_ error: Error?) -> Void
    //
    //        )
    //    {
    //        //------------------------------------------------------------------------------------------------
    //        let gmsGeocoder = GMSGeocoder()
    //        //------------------------------------------------------------------------------------------------
    //        //CLLocation
    //        //GMSReverseGeocodeResponse
    //        //------------------------------------------------------------------------------------------------
    //        gmsGeocoder.reverseGeocodeCoordinate(coordinate){ response , error in
    //
    //            if error != nil {
    //                self.log.error("error:\(error)")
    //
    //                failure(error as NSError?)
    //            }
    //            else
    //            {
    //                //---------------------------------------------------------------------
    //            }//geocoder no error
    //        }//geocoder - block
    //        //------------------------------------------------------------------------------------------------
    //    }
}
