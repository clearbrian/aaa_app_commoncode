//
//  COLCGeocoder.swift
//  joyride
//
//  Created by Brian Clear on 04/11/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
class COLCGeocoder{
    fileprivate var clkGoogleGeocoder = CLKGoogleGeocoder()
    fileprivate var clkAppleGeocoder = CLKAppleGeocoder()
    
    
    //--------------------------------------------------------------
    // MARK: - REVERSE GEOCODE
    // MARK: -
    //--------------------------------------------------------------
    var isReverseGeocodeInProgress = false

    func reverseGeocodePlace(clkPlace:CLKPlace,
                        success: @escaping (_ clkPlace: CLKPlace) -> Void,
                        failure: @escaping (_ error: Error?) -> Void)
    {
        if isReverseGeocodeInProgress{
            //noisy appDelegate.log.error("isReverseGeocodeInProgress - SKIP")
        }else{
            if let clLocation = clkPlace.clLocation {
                if clLocation.isValid{
                    //------------------------------------------
                    self.isReverseGeocodeInProgress = true
                    
                    //------------------------------------------------------------------------------------------------
                    //reverseGeocode START
                    //------------------------------------------------------------------------------------------------
                    self.clkGoogleGeocoder.reverseGeocode(clLocation,
                        success:{ (clkGeocodedAddressesGoogleCollection: CLKGeocodedAddressesGoogleCollection) -> Void in

                            self.isReverseGeocodeInProgress = false
                            clkPlace.clkGeocodedAddressesCollection = clkGeocodedAddressesGoogleCollection
                            success(clkPlace)

                        },
                        failure:{ (error: Error?) -> Void in
                            //---------------------------------------------------------------------
                            //appDelegate.log.error("[GEOCODE CURRENT LOCATION FAILED] currentLocation(\(currentLocation)) error:\(error)")
                            //---------------------------------------------------------------------
                            self.isReverseGeocodeInProgress = false
                            failure(error)
                            //---------------------------------------------------------------------
                        }
                    )
                    //------------------------------------------------------------------------------------------------
                    //reverseGeocode END
                    //------------------------------------------------------------------------------------------------
                    
                }else{
                    appDelegate.log.error("clLocation.isValid failed")
                    let errorLocation = NSError.errorWithMessage("Invalid CLLocation:\(clLocation)", code: 6765798)
                    failure(errorLocation)
                }
            }else{
                appDelegate.log.error("clkPlace.clLocation is nil")
            }
        }
        
        
    }
    
    //--------------------------------------------------------------
    // MARK: - forward geocode - Address string to CLKAddress collection
    // MARK: -
    //--------------------------------------------------------------
    
    //Address String > Address/lat/lng
    //not 100% accurate - postcode returns lat/lng but not street address may require
    
    func forwardGeocode_MKLocalSearch(addressString: String,
                                         clkPlaceIn: CLKPlace,
                                            success: @escaping (_ clkPlace: CLKPlace) -> Void,
                                          failure: @escaping (_ error: Error?) -> Void
        )
    {
        //prevent multiple calls
        if isForwardGeocodeInProgress{
            appDelegate.log.debug("isForwardGeocodeInProgress - skip geocode till current call returns")
            
        }else{


// TODO: - CLEANUP after test
//            if let addressDictionary = clkFacebookEventPlace.colcContact.addressDictionary {
                //---------------------------------------------------------------------
                //print(addressDictionary)
                self.isForwardGeocodeInProgress = true
                
                //---------------------------------------------------------------------
                self.clkAppleGeocoder.forwardGeocode_MKLocalSearch(addressString,
                                                                      success:
                    {(clkGeocodedAddressesAppleCollection: CLKGeocodedAddressesAppleCollection) -> Void in
                        
                        self.isForwardGeocodeInProgress = false
                        
                        //---------------------------------------------------------------------
                        //IMPORTANT - store geocode address with CLKPLace.addressDictionary it was forward geocoded from else address doesnt appear properly
                        //---------------------------------------------------------------------
                        //store the collection on the result in this CLKPlace
                        //important all address come from this
                        
                        //
                        clkPlaceIn.clkGeocodedAddressesCollection = clkGeocodedAddressesAppleCollection
                        
                        
                        //---------------------------------------------------------------------
                        //FORWARD GEOCODE  - SUCCESS
                        //---------------------------------------------------------------------
                        if clkGeocodedAddressesAppleCollection.reverseGeocodeRecommended{
                            appDelegate.log.error("reverseGeocodeRecommended TRUE")
                            //---------------------------------------------------------------------
                            success(clkPlaceIn)
                            //---------------------------------------------------------------------
                            
                        }else{
                            appDelegate.log.error("reverseGeocodeRecommended FALSE - use address from forwardGeocode")
                            success(clkPlaceIn)
                        }
                        
                },
              failure:{ (error: Error?) -> Void in
                //---------------------------------------------------------------------
                //appDelegate.log.error("[GEOCODE CURRENT LOCATION FAILED] currentLocation(\(currentLocation)) error:\(error)")
                
                self.isForwardGeocodeInProgress = false
                failure(error)
                //---------------------------------------------------------------------
                })
                //---------------------------------------------------------------------
//            }else{
//                appDelegate.log.error("self.colcContact.addressDictionary is nil")
//                failure(NSError.appError(.geocode_forward_apple_failed))
//            }
            
        }//isForwardGeocodeInProgress
    }
    
    //--------------------------------------------------------------
    // MARK: - FORWARD GEOCODE - address string to Place
    // MARK: -
    //--------------------------------------------------------------
    var isForwardGeocodeInProgress = false
    
    
    //TODO: - forwardAndOrReverseGeocodePlace removed to get TaxiRank building for app store - reject as Contacts framework included and needed permission entry in plist
    //uses
//    func forwardAndOrReverseGeocodePlace(_ clkContactPlace: CLKContactPlace,
//                                                 success: @escaping (_ clkContactPlace: CLKContactPlace) -> Void,
//                                                 failure: @escaping (_ error: Error?) -> Void
//        )
//    {
//        //prevent multiple calls
//        if isForwardGeocodeInProgress{
//            appDelegate.log.debug("isForwardGeocodeInProgress - skip geocode till current call returns")
//            
//        }else{
//            //Address Forward geocode rquires a dictionary in specific format -
//            if let addressDictionary = clkContactPlace.colcContact.addressDictionary {
//                //---------------------------------------------------------------------
//                //print(addressDictionary)
//                self.isForwardGeocodeInProgress = true
//
//                //---------------------------------------------------------------------
//                self.clkAppleGeocoder.forwardGeocodeAddressDictionary(addressDictionary,
//                    success:
//                    {(clkGeocodedAddressesAppleCollection: CLKGeocodedAddressesAppleCollection) -> Void in
//                        
//                        self.isForwardGeocodeInProgress = false
//                        
//                        //---------------------------------------------------------------------
//                        //IMPORTANT - store geocode address with CLKPLace.addressDictionary it was forward geocoded from else address doesnt appear properly
//                        //---------------------------------------------------------------------
//                        //store the collection on the result in this CLKPlace
//                        //important all address come from this
//                        clkContactPlace.clkGeocodedAddressesCollection = clkGeocodedAddressesAppleCollection
//                        
//                        
//                        
//                        //---------------------------------------------------------------------
//                        //FORWARD GEOCODE  - SUCCESS
//                        //---------------------------------------------------------------------
//                        if clkGeocodedAddressesAppleCollection.reverseGeocodeRecommended{
//                            appDelegate.log.error("reverseGeocodeRecommended TRUE")
//                            
//                                //---------------------------------------------------------------------
//                                //REVERSE GEOCODE - START
//                                //---------------------------------------------------------------------
//                                self.reverseGeocodePlace(clkPlace: clkContactPlace,
//                                     success:{(clkPlace: CLKPlace) -> Void in
//                                        //---------------------------------------------------------------------
//                                        //GEOCODE OK
//                                        //---------------------------------------------------------------------
//                                        
//                                        if let clkContactPlace = clkPlace as? CLKContactPlace {
//                                            //---------------------------------------------------------------------
//                                            if let clkGeocodedAddressesCollection = clkPlace.clkGeocodedAddressesCollection {
//                                                if let bestOrFirstCLKAddress = clkGeocodedAddressesCollection.bestOrFirstCLKAddress {
//                                                    // TODO: - remove use currentUserPlace.clkGeocodedAddressesCollection.bestOrFirstCLKAddress
//                                                    //required ad Uber requires locality
//                                                    print("bestOrFirstCLKAddress:\(bestOrFirstCLKAddress)")
//                                                    
//                                                    success(clkContactPlace)
//                                                    
//                                                }else{
//                                                    appDelegate.log.error("clkGeocodedAddressesResponse.bestOrFirstCLKAddress is nil")
//                                                }
//                                                //---------------------------------------------------------------------
//                                            }else{
//                                                appDelegate.log.error("clkPlace.clkGeocodedAddressesCollection is nil")
//                                            }
//                                            //---------------------------------------------------------------------
//                                        }else{
//                                            appDelegate.log.error("clkPlace as? CLKContactPlace is nil")
//                                        }
//                                    },
//                                    failure:{ (error: Error?) -> Void in
//                                        appDelegate.log.error("[GEOCODE CURRENT LOCATION FAILED] error:\(String(describing: error))  - use address from forwardGeocode")
//                                        //failure(error)
//                                        
//                                        //use address from forwardGeocode
//                                        success(clkContactPlace)
//                                    }
//                                )
//                                //---------------------------------------------------------------------
//                                //REVERSE GEOCODE - END
//                                //---------------------------------------------------------------------
//                            
//                        }else{
//                            appDelegate.log.error("reverseGeocodeRecommended FALSE - use address from forwardGeocode")
//                            success(clkContactPlace)
//                        }
//                        
//                    },
//                    failure:{ (error: Error?) -> Void in
//                        //---------------------------------------------------------------------
//                        //appDelegate.log.error("[GEOCODE CURRENT LOCATION FAILED] currentLocation(\(currentLocation)) error:\(error)")
//
//                        self.isForwardGeocodeInProgress = false
//                        failure(error)
//                        //---------------------------------------------------------------------
//                })
//                //---------------------------------------------------------------------
//            }else{
//                appDelegate.log.error("self.colcContact.addressDictionary is nil")
//                failure(NSError.appError(.geocode_forward_apple_failed))
//            }
//            
//        }//isForwardGeocodeInProgress
//    }
    
// TODO: - remove to get TaxiRank building for app store - reject as Contacts framework included and needed permission entry in plist
//    //uses
//    func forwardAndOrReverseGeocodePlace1(_ clkContactPlace: CLKContactPlace,
//                                         success: @escaping (_ clkContactPlace: CLKContactPlace) -> Void,
//                                         failure: @escaping (_ error: Error?) -> Void
//        )
//    {
//        //prevent multiple calls
//        if isForwardGeocodeInProgress{
//            appDelegate.log.debug("isForwardGeocodeInProgress - skip geocode till current call returns")
//            
//        }else{
//            //Address Forward geocode rquires a dictionary in specific format -
//            if let addressDictionary = clkContactPlace.colcContact.addressDictionary {
//                //---------------------------------------------------------------------
//                //print(addressDictionary)
//                self.isForwardGeocodeInProgress = true
//                
//                //---------------------------------------------------------------------
//                self.clkAppleGeocoder.forwardGeocodeAddressDictionary(addressDictionary,
//                                                                      success:
//                    {(clkGeocodedAddressesAppleCollection: CLKGeocodedAddressesAppleCollection) -> Void in
//                        
//                        self.isForwardGeocodeInProgress = false
//                        
//                        //---------------------------------------------------------------------
//                        //IMPORTANT - store geocode address with CLKPLace.addressDictionary it was forward geocoded from else address doesnt appear properly
//                        //---------------------------------------------------------------------
//                        //store the collection on the result in this CLKPlace
//                        //important all address come from this
//                        clkContactPlace.clkGeocodedAddressesCollection = clkGeocodedAddressesAppleCollection
//                        
//                        
//                        
//                        //---------------------------------------------------------------------
//                        //FORWARD GEOCODE  - SUCCESS
//                        //---------------------------------------------------------------------
//                        if clkGeocodedAddressesAppleCollection.reverseGeocodeRecommended{
//                            appDelegate.log.error("reverseGeocodeRecommended TRUE")
//                            
//                            //---------------------------------------------------------------------
//                            //REVERSE GEOCODE - START
//                            //---------------------------------------------------------------------
//                            self.reverseGeocodePlace(clkPlace: clkContactPlace,
//                                                     success:{(clkPlace: CLKPlace) -> Void in
//                                                        //---------------------------------------------------------------------
//                                                        //GEOCODE OK
//                                                        //---------------------------------------------------------------------
//                                                        
//                                                        if let clkContactPlace = clkPlace as? CLKContactPlace {
//                                                            //---------------------------------------------------------------------
//                                                            if let clkGeocodedAddressesCollection = clkPlace.clkGeocodedAddressesCollection {
//                                                                if let bestOrFirstCLKAddress = clkGeocodedAddressesCollection.bestOrFirstCLKAddress {
//                                                                    // TODO: - remove use currentUserPlace.clkGeocodedAddressesCollection.bestOrFirstCLKAddress
//                                                                    //required ad Uber requires locality
//                                                                    print("bestOrFirstCLKAddress:\(bestOrFirstCLKAddress)")
//                                                                    
//                                                                    success(clkContactPlace)
//                                                                    
//                                                                }else{
//                                                                    appDelegate.log.error("clkGeocodedAddressesResponse.bestOrFirstCLKAddress is nil")
//                                                                }
//                                                                //---------------------------------------------------------------------
//                                                            }else{
//                                                                appDelegate.log.error("clkPlace.clkGeocodedAddressesCollection is nil")
//                                                            }
//                                                            //---------------------------------------------------------------------
//                                                        }else{
//                                                            appDelegate.log.error("clkPlace as? CLKContactPlace is nil")
//                                                        }
//                            },
//                                                     failure:{ (error: Error?) -> Void in
//                                                        appDelegate.log.error("[GEOCODE CURRENT LOCATION FAILED] error:\(String(describing: error))  - use address from forwardGeocode")
//                                                        //failure(error)
//                                                        
//                                                        //use address from forwardGeocode
//                                                        success(clkContactPlace)
//                            }
//                            )
//                            //---------------------------------------------------------------------
//                            //REVERSE GEOCODE - END
//                            //---------------------------------------------------------------------
//                            
//                        }else{
//                            appDelegate.log.error("reverseGeocodeRecommended FALSE - use address from forwardGeocode")
//                            success(clkContactPlace)
//                        }
//                        
//                },
//                                                                      failure:{ (error: Error?) -> Void in
//                                                                        //---------------------------------------------------------------------
//                                                                        //appDelegate.log.error("[GEOCODE CURRENT LOCATION FAILED] currentLocation(\(currentLocation)) error:\(error)")
//                                                                        
//                                                                        self.isForwardGeocodeInProgress = false
//                                                                        failure(error)
//                                                                        //---------------------------------------------------------------------
//                })
//                //---------------------------------------------------------------------
//            }else{
//                appDelegate.log.error("self.colcContact.addressDictionary is nil")
//                failure(NSError.appError(.geocode_forward_apple_failed))
//            }
//            
//        }//isForwardGeocodeInProgress
//    }
    
    
    

}
