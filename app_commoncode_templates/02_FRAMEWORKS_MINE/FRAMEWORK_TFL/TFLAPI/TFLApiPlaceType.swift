//
//  TFLPlaceType.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 13/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

/*
 
 



 
 //----------------------------------------------------------------------------------------
 //APPL CabWise
 //----------------------------------------------------------------------------------------
     //https://tfl.gov.uk/corporate/terms-and-conditions/cabwise
     //457
     case cabwise
     
     //    "category": "Identifier",
     //    "category": "Address",
     //    "category": "Contact",
     //    "category": "Resource",
     //    "category": "Payments",
     //    "category": "Time",
     //    "category": "Counter",
     //    "category": "Type",
     
    Competition: Kabbee
 
 
 

 
 //----------------------------------------------------------------------------------------
 //META area info
 //----------------------------------------------------------------------------------------
 //----------------------------------------------------------------------------------------
 case areaForIntensification
 
    //https://tfl.gov.uk/info-for/urban-planning-and-construction/planning-with-webcat
    //https://tfl.gov.uk/info-for/urban-planning-and-construction/planning-with-webcat/glossary
  case opportunityAreas
 
 //case censusOutputAreas //valid type - DO NOT CALL - is 15 mb o
    case censusSuperOutputAreas
    case centralActivityZone
    case boroughs
        //just name / x/y , pop, and LSOA
    case wards
 
 //----------------------------------------------------------------------------------------


 
 PARKING - http://content.tfl.gov.uk/tfl-taxi-ranks-booklet.pdf
    case onStreetMeteredBay
    case carPark
    ----
    case taxiRank
 
 TRAFFIC
    case variableMessageSign
    case jamCam

 
    case redLightCam
    case speedCam
 
    case redLightAndSpeedCam
        "value": "Combined Red and Speed",  NOT .redLightCam  and .speedCam
        https://www.google.co.uk/maps/@51.5137951,-0.249121,3a,75y,123.67h,94.96t/data=!3m6!1e1!3m4!1shrQ_wfZjlE3HILjAPqDIHQ!2e0!7i13312!8i6656
 
    London Safety Camera Partnership
    https://www.cityoflondon.police.uk/advice-and-support/safer-roads/Pages/Safety-Camera-Partnership.aspx
 
 
 PARKING - COACH
    case coachBay
    case coachPark
    case otherCoachParking
 

 
 case oysterTicketShop



 
 WATER - put into one app - would anyone use it - need to show all on a map
     http://canalplan.eu/gazetteer/ff2o
     case waterfreightBridge
     case waterfreightDock
     case waterfreightJetty
     case waterfreightLock
     case waterfreightOther_Access_Point
     case waterfreightTunnel
     case waterfreightWharf
     
 

 
 
 //----------------------------------------------------------------------------------------
 //APP TFL NEARBY TRANSPORT
 //----------------------------------------------------------------------------------------
 APP IDEA 
    place with not type and lat/lng - shows nearby transport
    TFL - NEARBY app
    might need departures times for /StopPoint api
 
 case carClub
         https://tfl.gov.uk/info-for/urban-planning-and-construction/transport-assessment-guide/guidance-by-transport-type/car-clubs
         
         "value": "City Car Club",
         https://www.citycarclub.co.uk/locations/london-car-hire/
         
         "value": "E-car Club",
         "value": "http://www.e-carclub.org/",
         
         "value": "Hertz 24/7",
         "value": "http://www.hertz247.com/BQ/en-GB/Home",
         
         "value": "Zipcar",
         "value": "http://www.zipcar.co.uk",

 //----------------------------------------------------------------------------------------
 case bikePoint
    //use bike api but maybe NEARBY app for transport
 
 case cyclePark
    bike parks at stations
 //----------------------------------------------------------------------------------------
 
 
 */

//--------------------------------------------------------------
// MARK: - TFLApiPlaceType
// MARK: -
//--------------------------------------------------------------

enum TFLApiPlaceType: Int{
    case unknown = 0
    
    
    case areaForIntensification
    case bikePoint
    case boroughs
    case cabwise
    case carClub
    case carPark
    //case censusOutputAreas //valid type - DO NOT CALL - is 15 mb o
    case censusSuperOutputAreas
    case centralActivityZone
    case coachBay
    case coachPark
    case cyclePark
    case jamCam
    case onStreetMeteredBay
    case opportunityAreas
    case otherCoachParking
    case oysterTicketShop
    case redLightAndSpeedCam
    case redLightCam
    case speedCam
    case taxiRank
    case variableMessageSign
    case wards
    case waterfreightBridge
    case waterfreightDock
    case waterfreightJetty
    case waterfreightLock
    case waterfreightOther_Access_Point
    case waterfreightTunnel
    case waterfreightWharf
        
    
    //--------------------------------------------------------------
    // MARK: - CustomStringConvertible - not needed enum of String ouputs itself
    // MARK: -
    //--------------------------------------------------------------
    //enum of string used in URL
    //if you want desc use .title
    //    public var description: String {
    //
    //        
    //    }
    
    //--------------------------------------------------------------
    // MARK: - Generic VCs
    // MARK: -
    //--------------------------------------------------------------
    //title for VCs
    public var title: String {
        return TFLApiPlaceType.displayStringForType(self)
        
    }
    
    public var apiStringForType: String {
        return TFLApiPlaceType.apiEndpointString(self)
        
    }
    
    
    //--------------------------------------------------------------
    // MARK: - colcQueryCollectionForType TFLApiPlaceType >> COLCQueryCollection
    // MARK: -
    //--------------------------------------------------------------

//    public var colcQueryCollectionForType: COLCQueryCollection {
//        return TFLApiPlaceType.colcQueryCollectionForType(self)
//    }
    
    
        
    static func colcQueryCollectionForType(_ type: TFLApiPlaceType) -> COLCQueryCollection{
        switch type{

//        case .unknown :
//            return "UNKNOWN"
//            
//        case .areaForIntensification :
//            return "AreaForIntensification"
//        case .bikePoint :
//            return "BikePoint"
//        case .boroughs :
//            return "Boroughs"
//        case .cabwise :
//            return "Cabwise"
//        case .carClub :
//            return "CarClub"
//        case .carPark :
//            return "CarPark"
//            
//            //case .censusOutputAreas :
//            //    return "Census Output Areas"
//            
//        case .censusSuperOutputAreas :
//            return "CensusSuperOutputAreas"
//        case .centralActivityZone :
//            return "CentralActivityZone"
//        case .coachBay :
//            return "CoachBay"
//        case .coachPark :
//            return "CoachPark"
//        case .cyclePark :
//            return "CyclePark"
            
        case .jamCam :
            return TFLApiPlaceJamCam.colcQueryCollectionForType //TFLApiPlaceCOLCQueryCollectionJamCam
            
//        case .onStreetMeteredBay :
//            return "OnStreetMeteredBay"
//        case .opportunityAreas :
//            return "OpportunityAreas"
//        case .otherCoachParking :
//            return "OtherCoachParking"
//        case .oysterTicketShop :
//            return "OysterTicketShop"
//        case .redLightAndSpeedCam :
//            return "RedLightAndSpeedCam"
//        case .redLightCam :
//            return "RedLightCam"
//        case .speedCam :
//            return "SpeedCam"
            
        case .taxiRank :
            return  TFLApiPlaceTaxiRank.colcQueryCollectionForType //TFLApiPlaceCOLCQueryCollectionTaxiRank()
            
//        case .variableMessageSign :
//            return "VariableMessageSign"
//        case .wards :
//            return "Wards"
//        case .waterfreightBridge :
//            return "WaterfreightBridge"
//        case .waterfreightDock :
//            return "WaterfreightDock"
//        case .waterfreightJetty :
//            return "WaterfreightJetty"
//        case .waterfreightLock :
//            return "WaterfreightLock"
//        case .waterfreightOther_Access_Point :
//            return "WaterfreightOtherAccessPoint"
//        case .waterfreightTunnel :
//            return "WaterfreightTunnel"
//        case .waterfreightWharf :
//            return "Waterfreight Wharf"
            
        default:
            return  TFLApiPlace.colcQueryCollectionForType //TFLApiPlaceCOLCQueryCollection()

        }
    }
    
    //--------------------------------------------------------------
    // MARK: - QUERY param
    // MARK: -
    //--------------------------------------------------------------

    //cant use desc - as enum now lower case
    static func apiEndpointString(_ type: TFLApiPlaceType?) -> String{
        if let type = type {
            
            switch type{
            //----------------------------------------------------------------------------------------
            case .unknown :
                return "UNKNOWN"
                
            case .areaForIntensification :
                return "AreaForIntensification"
            case .bikePoint :
                return "BikePoint"
            case .boroughs :
                return "Boroughs"
            case .cabwise :
                return "Cabwise"
            case .carClub :
                return "CarClub"
            case .carPark :
                return "CarPark"
                
                //case .censusOutputAreas :
                //    return "Census Output Areas"
                
            case .censusSuperOutputAreas :
                return "CensusSuperOutputAreas"
            case .centralActivityZone :
                return "CentralActivityZone"
            case .coachBay :
                return "CoachBay"
            case .coachPark :
                return "CoachPark"
            case .cyclePark :
                return "CyclePark"
            case .jamCam :
                return "JamCam"
            case .onStreetMeteredBay :
                return "OnStreetMeteredBay"
            case .opportunityAreas :
                return "OpportunityAreas"
            case .otherCoachParking :
                return "OtherCoachParking"
            case .oysterTicketShop :
                return "OysterTicketShop"
            case .redLightAndSpeedCam :
                return "RedLightAndSpeedCam"
            case .redLightCam :
                return "RedLightCam"
            case .speedCam :
                return "SpeedCam"
            case .taxiRank :
                return "TaxiRank"
            case .variableMessageSign :
                return "VariableMessageSign"
            case .wards :
                return "Wards"
            case .waterfreightBridge :
                return "WaterfreightBridge"
            case .waterfreightDock :
                return "WaterfreightDock"
            case .waterfreightJetty :
                return "WaterfreightJetty"
            case .waterfreightLock :
                return "WaterfreightLock"
            case .waterfreightOther_Access_Point :
                return "WaterfreightOtherAccessPoint"
            case .waterfreightTunnel :
                return "WaterfreightTunnel"
            case .waterfreightWharf :
                return "Waterfreight Wharf"
                
                
            }
            //----------------------------------------------------------------------------------------
        }else{
            appDelegate.log.error("type is nil - not found")
            return "ERROR TYPE"
        }
    }
    
    
    
    //--------------------------------------------------------------
    // MARK: - TFLApiPlaceType <> String
    // MARK: -
    //--------------------------------------------------------------

    static func typeForString(_ value: String?) -> TFLApiPlaceType?{
        if let value = value {
            //----------------------------------------------------------------------------------------
            switch value{
            case "AreaForIntensification" :
                return .areaForIntensification
                
            case "BikePoint" :
                return .bikePoint
                
            case "Boroughs" :
                return .boroughs
                
            case "Cabwise" :
                return .cabwise
                
            case "CarClub" :
                return .carClub
                
            case "CarPark" :
                return .carPark
                
            case "CensusSuperOutputAreas" :
                return .censusSuperOutputAreas
                
            case "CentralActivityZone" :
                return .centralActivityZone
                
            case "CoachBay" :
                return .coachBay
                
            case "CoachPark" :
                return .coachPark
                
            case "CyclePark" :
                return .cyclePark
                
            case "JamCam" :
                return .jamCam
                
            case "OnStreetMeteredBay" :
                return .onStreetMeteredBay
                
            case "OpportunityAreas" :
                return .opportunityAreas
                
            case "OtherCoachParking" :
                return .otherCoachParking
                
            case "OysterTicketShop" :
                return .oysterTicketShop
                
            case "RedLightAndSpeedCam" :
                return .redLightAndSpeedCam
                
            case "RedLightCam" :
                return .redLightCam
                
            case "SpeedCam" :
                return .speedCam
                
            case "TaxiRank" :
                return .taxiRank
                
            case "VariableMessageSign" :
                return .variableMessageSign
                
            case "Wards" :
                return .wards
                
            case "WaterfreightBridge" :
                return .waterfreightBridge
                
            case "WaterfreightDock" :
                return .waterfreightDock
                
            case "WaterfreightJetty" :
                return .waterfreightJetty
                
            case "WaterfreightLock" :
                return .waterfreightLock
                
            case "WaterfreightOther_Access_Point" :
                return .waterfreightOther_Access_Point
                
            case "WaterfreightTunnel" :
                return .waterfreightTunnel
                
            case "WaterfreightWharf" :
                return .waterfreightWharf
                
            default:
                return .unknown
            }
            //-------------------------------------------------------------------
            
        }else{
            appDelegate.log.error("type value is nil - not found")
            return nil
        }
    }
    
    //PLURAL - shown in titles
    static func displayStringForType(_ type: TFLApiPlaceType?) -> String{
        if let type = type {
            
            switch type{
            //----------------------------------------------------------------------------------------
            case .unknown :
                return "UNKNOWN"
                
            case .areaForIntensification :
                return "Areas For Intensification"
            case .bikePoint :
                return "Bike Points"
            case .boroughs :
                return "Boroughs"
            case .cabwise :
                return "Cabwise"
            case .carClub :
                return "Car Clubs"
            case .carPark :
                return "Car Parks"
                
            //case .censusOutputAreas :
            //    return "Census Output Areas"
                
            case .censusSuperOutputAreas :
                return "Census Super Output Areas"
            case .centralActivityZone :
                return "Central Activity Zones"
            case .coachBay :
                return "Coach Bays"
            case .coachPark :
                return "Coach Parks"
            case .cyclePark :
                return "Cycle Parks"
            case .jamCam :
                return "Jam Cams"
            case .onStreetMeteredBay :
                return "OnStreet Metered Bays"
            case .opportunityAreas :
                return "Opportunity Areas"
            case .otherCoachParking :
                return "Other Coach Parking"
            case .oysterTicketShop :
                return "Oyster Ticket Shops"
            case .redLightAndSpeedCam :
                return "Red Light And Speed Cams"
            case .redLightCam :
                return "Red Light Cams"
            case .speedCam :
                return "Speed Cams"
            case .taxiRank :
                return "Taxi Ranks"
            case .variableMessageSign :
                return "Variable Message Signs"
            case .wards :
                return "Wards"
            case .waterfreightBridge :
                return "Waterfreight Bridges"
            case .waterfreightDock :
                return "Waterfreight Docks"
            case .waterfreightJetty :
                return "Waterfreight Jettys"
            case .waterfreightLock :
                return "Waterfreight Locks"
            case .waterfreightOther_Access_Point :
                return "Waterfreight Other Access Points"
            case .waterfreightTunnel :
                return "Waterfreight Tunnels"
            case .waterfreightWharf :
                return "Waterfreight Wharfs"
            //----------------------------------------------------------------------------------------
            
            }
        }else{
            appDelegate.log.error("json value is nil - not found")
            return "ERROR TYPE"
        }
    }
    
    
    
    
    //--------------------------------------------------------------
    // MARK: - CELL
    // MARK: -
    //--------------------------------------------------------------
    
    static func cellIdentifierForType(_ type: TFLApiPlaceType?) -> String{
        
        var cellIdentifierForType = "GenericListTableViewCell"
        if let type = type {
            
            switch type{
            //----------------------------------------------------------------------------------------
                //            case .unknown :
                //                cellIdentifierForType = "UNKNOWN"
                //
                //            case .areaForIntensification :
                //                cellIdentifierForType = "Area For Intensification"
                //            case .bikePoint :
                //                cellIdentifierForType = "Bike Point"
                //            case .boroughs :
                //                cellIdentifierForType = "Boroughs"
                //            case .cabwise :
                //                cellIdentifierForType = "Cabwise"
                //            case .carClub :
                //                cellIdentifierForType = "Car Club"
                //            case .carPark :
                //                cellIdentifierForType = "Car Park"
                //
                //                //case .censusOutputAreas :
                //                //    cellIdentifierForType = "Census Output Areas"
                //
                //            case .censusSuperOutputAreas :
                //                cellIdentifierForType = "Census Super Output Areas"
                //            case .centralActivityZone :
                //                cellIdentifierForType = "Central Activity Zone"
                //            case .coachBay :
                //                cellIdentifierForType = "Coach Bay"
                //            case .coachPark :
                //                cellIdentifierForType = "Coach Park"
                //            case .cyclePark :
                //                cellIdentifierForType = "Cycle Park"
                
            case .jamCam :
                cellIdentifierForType = "GenericListTableViewCell_Detail0Only"
                
                //            case .onStreetMeteredBay :
                //                cellIdentifierForType = "OnStreet Metered Bay"
                //            case .opportunityAreas :
                //                cellIdentifierForType = "Opportunity Areas"
                //            case .otherCoachParking :
                //                cellIdentifierForType = "Other Coach Parking"
                //            case .oysterTicketShop :
                //                cellIdentifierForType = "Oyster Ticket Shop"
                //            case .redLightAndSpeedCam :
                //                cellIdentifierForType = "Red Light And Speed Cam"
                //            case .redLightCam :
                //                cellIdentifierForType = "Red Light Cam"
                //            case .speedCam :
                //                cellIdentifierForType = "Speed Cam"
                //            case .taxiRank :
                //                cellIdentifierForType = "Taxi Rank"
                //            case .variableMessageSign :
                //                cellIdentifierForType = "Variable Message Sign"
                //            case .wards :
                //                cellIdentifierForType = "Wards"
                //            case .waterfreightBridge :
                //                cellIdentifierForType = "Waterfreight Bridge"
                //            case .waterfreightDock :
                //                cellIdentifierForType = "Waterfreight Dock"
                //            case .waterfreightJetty :
                //                cellIdentifierForType = "Waterfreight Jetty"
                //            case .waterfreightLock :
                //                cellIdentifierForType = "Waterfreight Lock"
                //            case .waterfreightOther_Access_Point :
                //                cellIdentifierForType = "Waterfreight Other Access Point"
                //            case .waterfreightTunnel :
                //                cellIdentifierForType = "WaterfreightTunnel"
                //            case .waterfreightWharf :
            //                cellIdentifierForType = "Waterfreight Wharf"
            default:
                cellIdentifierForType = "GenericListTableViewCell"
                
                
            }
        }else{
            //appDelegate.log.error("json value is nil - not found")
        }
        return cellIdentifierForType
    }
    
    
//    static func TypeForInt(_ rawValueInt: Int) -> TFLApiPlaceType{
//        
////        for rawValueIntInEnum in TFLApiPlaceType.unknown.rawValue ... TFLApiPlaceType.waterfreightWharf.rawValue {
////            // Do something useful
////            
////            let ghg = TFLApiPlaceType.typeForString(<#T##value: String?##String?#>)
////        }
//    }
}
