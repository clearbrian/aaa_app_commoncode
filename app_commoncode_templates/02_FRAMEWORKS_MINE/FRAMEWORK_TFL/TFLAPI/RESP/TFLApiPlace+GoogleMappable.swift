//
//  TFLApiPlace+GoogleMappable.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 14/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
import CoreLocation
import GoogleMaps
/*
 //MapsPinnable includes CoreLocatable
 protocol GoogleMappable: MapPinnable
 
 subclass should implement MapPinnable and leave GoogleMappable in here as you can subclass something in an extension
 
 */
extension TFLApiPlace : GoogleMappable{
    
    //GoogleMappable - the TFLApiPlace subclass 
    var gmsMarker : GMSMarker? {
        
        if let clLocation = self.clLocation{
            
            //location is ok
            let marker = GMSMarker(position: clLocation.coordinate)
            
            //------------------------------------------------------------------------------------------------
            //uses TFLApiPlace >> MapPinnable >> markerTitle()/markerSnippet()
            //------------------------------------------------------------------------------------------------

            //TITLE  - if text is in image then turn off - other wise set it so when you tap on pin the label pops up
            marker.title = self.markerTitle()
            marker.snippet = self.markerSnippet()
            
            //------------------------------------------------------------------------------------------------
            //IMAGE - also used in icon on table so it matches the map
            //marker.icon = MapImageUtils.textToImage(pinText, iconColor: .appColorBLUEGREEN())
            //the image should be in the center - so when we tap on result in table - it moves the map to the center of the pin image
            marker.groundAnchor = CGPoint(x: 0.5,y: 0.5)
            //note jamcam offset higher
            //----------------------------------------------------------------------------------------
            //PIN
            //-------------------------------------------------------------------------------------
            //uses TFLApiPlace >> MapPinnable >> markerImageName overidded by TFLApiPlace subclass e.g TaxiRank
            //the image
            //----------------------------------------------------------------------------------------
            if let markerImage = self.markerImage {
                marker.icon = markerImage
            }else{
                appDelegate.log.error("markerImage is nil")
                marker.icon = nil
            }
            //----------------------------------------------------------------------------------------
            //PIN - END
            //----------------------------------------------------------------------------------------
            
            //------------------------------------------------------------------------------------------------
            //Z index - taxi ranks - should stay on top
            //------------------------------------------------------------------------------------------------
            marker.zIndex = 999
            
            //------------------------------------------------------------------------------------------------
            //store the TFLTaxiRank with the marker
            marker.userData = self
            //------------------------------------------------------------------------------------------------
            return marker
            
        }else{
            appDelegate.log.error("location is nil cant create GMSMarker for TFLTaxiRank")
            return nil
        }
    }
}


