//
//  TFLApiPlaceAdditionalProperty.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 13/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation


//--------------------------------------------------------------
// MARK: - TFLApiPlaceAdditionalProperty
// MARK: -
//--------------------------------------------------------------

class TFLApiPlaceAdditionalProperty : ParentMappable{
    
    var type : String?
    var category : String?
    var key : String?
    var modified : String?
    var sourceSystemKey : String?
    var value : String?
    
    
    init(type : String,
         category : String,
         key : String,
         modified : String,
         sourceSystemKey : String,
         value : String
        )
    {
        super.init()
        self.type = type
        self.category = category
        self.key = key
        self.modified = modified
        self.sourceSystemKey = sourceSystemKey
        self.value = value
        
    }
    
    required init?(map: Map){

        super.init(map: map)

    }
    
//    tflApiPlaceAdditionalPropertyBorough.type = "Tfl.Api.Presentation.Entities.AdditionalProperties, Tfl.Api.Presentation.Entities"
//    tflApiPlaceAdditionalPropertyBorough.category = "Address"
//    tflApiPlaceAdditionalPropertyBorough.key = "Borough"
//    tflApiPlaceAdditionalPropertyBorough.sourceSystemKey = "3207"
//    tflApiPlaceAdditionalPropertyBorough.value = "Chelsea"
//    tflApiPlaceAdditionalPropertyBorough.modified = "2017-02-15T14:20:59.16Z"
    
    init(category : String,
         key : String,
         sourceSystemKey : String,
         value : String
        )
    {
        super.init()
        self.type = "Tfl.Api.Presentation.Entities.AdditionalProperties, Tfl.Api.Presentation.Entities"
        self.category = category
        self.key = key
        self.modified = "2017-02-15T14:20:59.16Z"
        self.sourceSystemKey = sourceSystemKey
        self.value = value
        
    }
    
    
    
    
// TODO: - TODO
    //----------------------------------------------------------------------------------------
//    var tflAdditionalPropertyCategory: TFLApiPlaceAdditionalPropertyCategory{
//        return TFLAdditionalPropertyCategory.categoryForCategoryString(categoryString: self.category)
//    }
    //----------------------------------------------------------------------------------------
    
    
    //	class func newInstance(map: Map) -> Mappable?{
    //		return TFLAdditionalProperty()
    //	}
    //	required init?(map: Map){}
    //	private override init(){}
    
    override func mapping(map: Map)
    {
        type <- map["$type"]
        category <- map["category"]
        key <- map["key"]
        modified <- map["modified"]
        sourceSystemKey <- map["sourceSystemKey"]
        value <- map["value"]
        
    }
    
}
