//
//  TFLApiPlaceResponse.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 20/01/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation


class TFLApiPlaceResponse: ParentGenericListViewDataSource{

    
    //the objects in the json
    //some are versioned
    var tflApiPlaceArrayProcessed = [TFLApiPlace](){
        didSet {
            // TODO: - CLEANUP after test
            // TODO: - need a default - show all
            //WRONG taxi query also called for
            //requery( colcQuery: TFLApiPlaceTaxiRank.colcQuery_WorkingRanks())
            //... tflApiPlaceArrayProcessed is ivar in TFLAPiResponse - requery triggered when GeneVC.genericRespon.didSet
        }
    }

    
    
    //--------------------------------------------------------------
    // MARK: - RUN QUERY
    // MARK: -
    //--------------------------------------------------------------
    // TODO: - put this method IN COLCQUERY
    override func requery(colcQuery: COLCQuery){
        let startDate = Date()
        
        //result and count stay in colcQuery - so store it
        self.colcQuery = colcQuery
        
        appDelegate.log.debug("**** REQUERY START ****")
        //---------------------------------------------------------
        //RUN THE QUERY:
        //---------------------------------------------------------
        colcQuery.requery(arrayCOLCQueryable: self.tflApiPlaceArrayProcessed)
        //---------------------------------------------------------
        
        
        let endDate = Date()
        let seconds = endDate.timeIntervalSince(startDate)

        
        appDelegate.log.debug("**** REQUERY ENDED [seconds:\(seconds)] ****")

    }
    
    //--------------------------------------------------------------
    // MARK: - TOTAL RESULTS COUNT > label
    // MARK: -
    //--------------------------------------------------------------

    //displayed in label - its the filtered results - ungrouped array count
    override var totalsCount: Int {
        if let colcQuery = self.colcQuery {
            return colcQuery.resultsFilteredCount
        }else{
            appDelegate.log.error("self.<#EXPR#> is nil")
            return 0
        }
    }
    
    override var dictionaryTableData : Dictionary<String, Array<Any>>{
        
        if let colcQuery = self.colcQuery {
            return colcQuery.dictionaryTableData_Result
        }else{
            appDelegate.log.error("self.colcQuery is nil - dictionaryTableData")
            
            return Dictionary<String, Array<Any>>()
        }
    }
}

