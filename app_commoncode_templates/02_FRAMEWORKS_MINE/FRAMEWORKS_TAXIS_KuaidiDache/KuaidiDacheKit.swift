//
//  KuaidiDacheKit.swift
//  joyride
//
//  Created by Brian Clear on 16/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation

//-----------------------------------------------------------------------------------
// https://developer.lyft.com/docs/deeplinking
// https://docs.google.com/document/d/1Gn3Fo5-jWkGZumJLlEgK1nAizxCCMm5T0X4qTF7yeLE/edit
//-----------------------------------------------------------------------------------
//     make sure you add this to Info.plist LSApplicationQueriesSchemes
//    <key>CFBundleURLSchemes</key>
//    <array>
//    <string>fb1624443627822246</string>
//    </array>
//-----------------------------------------------------------------------------------
//  https://geo.itunes.apple.com/en/app/id789653936?mt=8
//-----------------------------------------------------------------------------------

class KuaidiDacheKit : ParentSwiftObject{
    //-----------------------------------------------------------------------------------
    static let openAppConfig = OpenAppConfigClipboard(openWithType : OpenWithType.openWithType_KuaidiDache,
                                      appSchemeString     : "fb1624443627822246://",
                                      appWebsiteURLString : "http://www.vvipone.com/en/",
                                      appStoreIdUInt      : 789653936)
    //-----------------------------------------------------------------------------------
    
}
