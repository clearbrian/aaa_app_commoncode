//
//  CLKGooglePlaceSearchResponse
//  joyride
//
//  Created by Brian Clear on 02/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//
import CoreLocation
class CLKGooglePlaceSearchResponse: ParentMappable {
    
    //------------------------------------------------
    //place/details has 1 result
    //------------------------------------------------
    
    //[html_attributions, result, status]
    var html_attributions: [AnyObject]?
    var result:CLKGooglePlaceResult?
    var status:String?
    
    //------------------------------------------------
    //nearbysearch has n results
    //------------------------------------------------
    var results: [CLKGooglePlaceResult]?
    
    var next_page_token:String?
    //---------------------------------------------------------------------
    // root calls mapping()
    //    required init?(_ map: Map){
    //        mapping(map: map)
    //    }
    //---------------------------------------------------------------------
    //in this bitbucket
    //GooglePlaces_place_detail.cje
    
    //https://github.com/Hearst-DD/ObjectMapper
    override func mapping(map: Map){
        
//        var mappedCountBEFORE: Int = map.mappedCount
 
        //  /api/place/details/
        html_attributions <- map["html_attributions"]
        result <- map["result"]
        status <- map["status"]
        
        //api/place/nearbysearch
        next_page_token <- map["next_page_token"]
        results <- map["results"]
    }
    
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - METHODS REQUIRED BY UBER API
    //--------------------------------------------------------------
    /*
    uber://?client_id=YOUR_CLIENT_ID
    &action=setPickup
    
    &pickup[latitude]=37.775818
    &pickup[longitude]=-122.418028
    &pickup[nickname]=UberHQ
    &pickup[formatted_address]=1455 Market St, San Francisco, CA 94103
    
    &dropoff[latitude]=37.802374
    &dropoff[longitude]=-122.405818
    &dropoff[nickname]=Coit Tower
    &dropoff[formatted_address]=1 Telegraph Hill Blvd, San Francisco, CA 94133
    &product_id=a1111c8c-c720-46c3-8534-2fcdd730040d
    */

    var lat: Double? {
        get {
            var lat_ : Double? = nil
            if let lat = result?.geometry?.location?.lat{
                lat_ = lat.doubleValue
            }else{
                self.log.error("result?.geometry?.location?.lat is nil")
            }
            return lat_
        }
        set {
            
        }
    }
    var lng: Double? {
        get {
            var lng_ : Double? = nil
            if let lng = result?.geometry?.location?.lng{
                lng_ = lng.doubleValue
            }else{
                self.log.error("result?.geometry?.location?.lng is nil")
            }
            return lng_
        }
        set {
            
        }
    }
    
    func latitude() -> String {
        var latitude_ = ""
        if let lat = result?.geometry?.location?.lat{
            latitude_ = "\(lat)"
        }else{
            self.log.error("result?.geometry?.location?.lat is nil")
        }
        return latitude_
    }
    func longitude() -> String {
        var longitude_ = ""
        if let lng = result?.geometry?.location?.lng{
            longitude_ = "\(lng)"
        }else{
            self.log.error("result?.geometry?.location?.lng is nil")
        }
        return longitude_
    }
    
    func locationCoordinate2D() -> CLLocationCoordinate2D? {
        var locationCoordinate2D : CLLocationCoordinate2D? = nil
        if let lat = result?.geometry?.location?.lat{
            if let lng = result?.geometry?.location?.lng{
                locationCoordinate2D = CLLocationCoordinate2D(latitude: lat.doubleValue, longitude: lng.doubleValue)
                
            }else{
                self.log.error("result?.geometry?.location?.lng is nil")
            }
        }else{
            self.log.error("result?.geometry?.location?.lat is nil")
        }
        return locationCoordinate2D
    }
    
    
    func name() -> String {
        var name_ = ""
        if let name = result?.name{
            name_ = name
        }else{
            self.log.error("result?.name is nil")
        }
        return name_
    }
    
    func formatted_address() -> String {
        var formatted_address_ = ""

        if let formatted_address = result?.formatted_address{
            formatted_address_ = formatted_address
        }else{
            self.log.error("result?.formatted_address is nil")
        }

        return formatted_address_
    }
    func formatted_name_address() -> String {
        var formatted_name_address = ""
        
        if let name = result?.name{
            
            if let formatted_address = result?.formatted_address{
                formatted_name_address = "\(name), \(formatted_address)"
                
            }else{
                self.log.error("result?.formatted_address is nil")
                formatted_name_address = "\(name), ADDRESS NOT FOUND"
            }
            
        }else{
            self.log.error("result?.name is nil")
            
            if let formatted_address = result?.formatted_address{
                formatted_name_address = "NAME NOT FOUND, \(formatted_address)"
                
            }else{
                self.log.error("result?.formatted_address is nil")
                formatted_name_address = "NAME NOT FOUND, ADDRESS NOT FOUND"
            }
        }
        
        return formatted_name_address
    }
    
    
}
/*
stringJson:
{
    "html_attributions" : [],
    "result" : {
        "address_components" : [
        {
        "long_name" : "30",
        "short_name" : "30",
        "types" : [ "street_number" ]
        },
        {
        "long_name" : "Fish Street Hill",
        "short_name" : "Fish St Hill",
        "types" : [ "route" ]
        },
        {
        "long_name" : "London",
        "short_name" : "London",
        "types" : [ "postal_town" ]
        },
        {
        "long_name" : "United Kingdom",
        "short_name" : "GB",
        "types" : [ "country", "political" ]
        },
        {
        "long_name" : "EC3R 6DN",
        "short_name" : "EC3R 6DN",
        "types" : [ "postal_code" ]
        }
        ],
        "adr_address" : "Monument, \u003cspan class=\"street-address\"\u003e30 Fish Street Hill\u003c/span\u003e, \u003cspan class=\"locality\"\u003eLondon\u003c/span\u003e \u003cspan class=\"postal-code\"\u003eEC3R 6DN\u003c/span\u003e, \u003cspan class=\"country-name\"\u003eUnited Kingdom\u003c/span\u003e",
        "formatted_address" : "Monument, 30 Fish Street Hill, London EC3R 6DN, United Kingdom",
        "formatted_phone_number" : "020 3371 3349",
        "geometry" : {
            "location" : {
            "lat" : 51.509664,
            "lng" : -0.086253
            }
        },
        "icon" : "http://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png",
        "id" : "6b63b757489eecbc16b0a73d9e69b667a6e3da94",
        "international_phone_number" : "+44 20 3371 3349",
        "name" : "American Golf",
        "opening_hours" : {
            "open_now" : true,
            "periods" : [
            {
            "close" : {
            "day" : 0,
            "time" : "1600"
            },
            "open" : {
            "day" : 0,
            "time" : "1000"
            }
            },
            {
            "close" : {
            "day" : 1,
            "time" : "2000"
            },
            "open" : {
            "day" : 1,
            "time" : "0800"
            }
            },
            {
            "close" : {
            "day" : 2,
            "time" : "2000"
            },
            "open" : {
            "day" : 2,
            "time" : "0800"
            }
            },
            {
            "close" : {
            "day" : 3,
            "time" : "2000"
            },
            "open" : {
            "day" : 3,
            "time" : "0800"
            }
            },
            {
            "close" : {
            "day" : 4,
            "time" : "2000"
            },
            "open" : {
            "day" : 4,
            "time" : "0800"
            }
            },
            {
            "close" : {
            "day" : 5,
            "time" : "2000"
            },
            "open" : {
            "day" : 5,
            "time" : "0800"
            }
            },
            {
            "close" : {
            "day" : 6,
            "time" : "1700"
            },
            "open" : {
            "day" : 6,
            "time" : "0900"
            }
            }
            ],
            "weekday_text" : [
            "Monday: 8:00 am – 8:00 pm",
            "Tuesday: 8:00 am – 8:00 pm",
            "Wednesday: 8:00 am – 8:00 pm",
            "Thursday: 8:00 am – 8:00 pm",
            "Friday: 8:00 am – 8:00 pm",
            "Saturday: 9:00 am – 5:00 pm",
            "Sunday: 10:00 am – 4:00 pm"
            ]
        },
        "place_id" : "ChIJZeLN3lMDdkgR5Zx5syC5NgI",
        "reference" : "CmRgAAAAWMBa7BDU_GuD3x1X-tCfGlHXv4Shz72L8PTwGeHMFqKQmz9NJbpop4ImEwR8fKAhF0QeodfNXzxWHTUIR1KSClWOXlZ4LIZbr0mtzT61W6S7wIDrPwlDOqaOIvJb0dADEhBxf2XNmBkd1miexCAMxLlNGhTLFLZY06Sn7VL-ItN7wEGCKxIweg",
        "scope" : "GOOGLE",
        "types" : [ "store", "establishment" ],
        "url" : "https://plus.google.com/116012597509910198766/about?hl=en-US",
        "utc_offset" : 60,
        "vicinity" : "Monument, 30 Fish Street Hill, London",
        "website" : "http://www.americangolf.co.uk/"
    },
    "status" : "OK"
}

jsonResultNSDictionary.allKeys:
[html_attributions, result, status]
done!
done!
result:[id, website, address_components, vicinity, formatted_address, international_phone_number, url, adr_address, scope, formatted_phone_number, geometry, icon, place_id, opening_hours, reference, types, name, utc_offset]
(lldb)

*/
