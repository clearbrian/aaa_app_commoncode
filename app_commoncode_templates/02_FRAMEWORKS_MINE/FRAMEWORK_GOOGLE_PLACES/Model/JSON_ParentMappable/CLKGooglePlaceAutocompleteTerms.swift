//
//  CLKGooglePlaceAutocompleteTerms.swift
//  joyride
//
//  Created by Brian Clear on 31/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//
import UIKit

class CLKGooglePlaceAutocompleteTerms: ParentMappable {
    

    var offset :NSNumber?
    var value :String?
    
    //---------------------------------------------------------------------
    
    // root calls mapping()
    //    required init?(_ map: Map){
    //        mapping(map: map)
    //    }
    
    override func mapping(map: Map){

        offset <- map["offset"]
        value <- map["value"]
    }
}

/*
"terms" : [

{

"offset" : 0,

"value" : "CAU Kingston"

},

{

"offset" : 14,

"value" : "Riverside Walk"

},

{

"offset" : 30,

"value" : "Kingston upon Thames"

},

{

"offset" : 52,

"value" : "United Kingdom"

}

],
*/
