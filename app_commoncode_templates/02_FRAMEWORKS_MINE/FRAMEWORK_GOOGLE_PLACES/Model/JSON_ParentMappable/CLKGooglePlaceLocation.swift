//
//  CLKGooglePlaceLocation.swift
//  joyride
//
//  Created by Brian Clear on 03/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//
import UIKit
import CoreLocation
class CLKGooglePlaceLocation: ParentMappable, CustomStringConvertible {
    
    var lat:NSNumber?
    var lng:NSNumber?

// root calls mapping()
//    required init?(_ map: Map){
//        mapping(map: map)
//    }

    override func mapping(map: Map){
        lat <- map["lat"]
        lng <- map["lng"]
    }
    
    var description : String {
        var description_ = "CLKGooglePlaceLocation:"
        
        if let lat = self.lat {
            if let lng = self.lng {
               description_ = "https://www.google.co.uk/maps/@\(lat.doubleValue),\(lng.doubleValue),14z"
                
            }else{
                self.log.error("self.lng is nil 3423")
            }
        }else{
            self.log.error("self.lat is nil 5453")
        }
        
        return description_
    }

    var cllocationCoordinate2D : CLLocationCoordinate2D?{
        get{
            var cllocationCoordinate2D : CLLocationCoordinate2D?
            if let lat = self.lat {
                if let lng = self.lng {
                    cllocationCoordinate2D =  CLLocationCoordinate2DMake(lat.doubleValue, lng.doubleValue)
                    
                }else{
                    self.log.error("self.lng is nil 3423")
                }
            }else{
                self.log.error("self.lat is nil 5453")
            }
            return cllocationCoordinate2D
        }
    }
}
/*
"geometry" : {
"location" : {
            "lat" : 51.509664,
            "lng" : -0.086253
}
},
*/
