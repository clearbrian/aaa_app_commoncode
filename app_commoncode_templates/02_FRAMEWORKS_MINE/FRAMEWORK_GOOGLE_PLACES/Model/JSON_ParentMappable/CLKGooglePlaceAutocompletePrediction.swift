//
//  CLKGooglePlaceAutocompleteMatchedSubstring.swift
//  joyride
//
//  Created by Brian Clear on 31/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation



class CLKGooglePlaceAutocompletePrediction : ParentMappable {
    
    var description :String?
    //var prediction_description :String?

    var id :String?
    var matched_substrings :[CLKGooglePlaceAutocompleteMatchedSubstring]?
    var place_id :String?
    var reference :String?
    var terms : [CLKGooglePlaceAutocompleteTerms]?


    
    
    //------------------------------------------------------------------------------------------------
    //TYPES - from json to enum
    //------------------------------------------------------------------------------------------------
    //var types : [String]?
    //one main type is needed for the pin color so use the top one
    //set in clkGooglePlacesTypeArray.didSet
    var clkGooglePlacesTypeMainType : CLKGooglePlacesType?
    
    //set when type didSet called
    var showPlaceInResults = true
    
    //set when type didSet called
    var clkGooglePlacesTypeArray : [CLKGooglePlacesType]?{
        
        //First this
        willSet {
            //print("Old value is \(clkGooglePlacesTypeArray), new value is \(newValue)")
        }
        
        //value is set
        
        //Finaly this
        didSet {
            //print("Old value is \(oldValue), new value is \(clkGooglePlacesTypeArray)")
            if let clkGooglePlacesTypeArray = clkGooglePlacesTypeArray{
                self.clkGooglePlacesTypeMainType = clkGooglePlacesTypeArray[0]
            }else{
                self.log.error("clkGooglePlacesTypeArray is nil - in didSet")
                self.clkGooglePlacesTypeMainType = nil
            }
        }
    }
    var clkGooglePlacesTypeArrayString: String {
        get {
            var clkGooglePlacesTypeArrayStringReturned = ""
            
            if let clkGooglePlacesTypeArray = self.clkGooglePlacesTypeArray{
                for clkGooglePlacesType : CLKGooglePlacesType in clkGooglePlacesTypeArray{
                    if clkGooglePlacesTypeArrayStringReturned == ""{
                        // empty string - just add
                        clkGooglePlacesTypeArrayStringReturned = "\(clkGooglePlacesType.googlePlacesType.rawValue)"
                    }else{
                        //append
                        clkGooglePlacesTypeArrayStringReturned = "\(clkGooglePlacesTypeArrayStringReturned), \(clkGooglePlacesType.googlePlacesType.rawValue)"
                    }
                }
            }else{
                self.log.error("self.clkGooglePlacesTypeArray is nil")
            }
            
            return clkGooglePlacesTypeArrayStringReturned
        }
        //set {
        //    //self = CGRectMake(newValue, self.minY, self.width, self.height)
        //}
    }
    
    //---------------------------------------------------------------------
    //if type array only contains types we dont want to see then hide it
    //if ANY are true then return true
    func canShowPlaceInResults(_ clkGooglePlacesTypeArrayReturned : [CLKGooglePlacesType]) -> Bool{
        var showPlaceInResults_ = false
        for clkGooglePlacesType in clkGooglePlacesTypeArrayReturned{
            if clkGooglePlacesType.isOKToShowTypeName{
                //at least one type is ok to show
                showPlaceInResults_ = true
                break
                
            }else{
                //dont set to false
            }
        }
        return showPlaceInResults_
    }
    //---------------------------------------------------------------------
    
    var googlePlacesController = GooglePlacesController()
    
    
    var types : [String]? {
        
        //First this
        willSet {
            //print("Old value is \(types), new value is \(newValue)")
        }
        
        //value is set
        
        //Finally this
        didSet {
            //print("Old value is \(oldValue), new value is \(types)")
            if let types = types{
                if types.count > 0{
                    if let clkGooglePlacesTypeArrayReturned : [CLKGooglePlacesType] = self.googlePlacesController.clkGooglePlacesTypesManager.finalTypesForGooglePlaceTypes(types){
                        self.clkGooglePlacesTypeArray = clkGooglePlacesTypeArrayReturned
                        
                        
                        //["neighborhood", "political"] - hide in results on map and table
                        self.showPlaceInResults = self.canShowPlaceInResults(clkGooglePlacesTypeArrayReturned)
                        
                        
                    }else{
                        self.log.error("CLKGooglePlacesType is nil for \(String(describing: self.place_id)):")
                    }
                }else{
                    self.log.error("types.count > 0 failed")
                }
            }else{
                self.log.error("self.types is nil")
            }
        }
    }
    
    
    
    
    
    
    
    //---------------------------------------------------------------------
    
    // root calls mapping()
    //    required init?(_ map: Map){
    //        mapping(map: map)
    //    }
    
    override func mapping(map: Map){
        //prediction_description <- map["description"]
        description <- map["description"]
        id <- map["id"]
        matched_substrings <- map["matched_substrings"]
        place_id <- map["place_id"]
        reference <- map["reference"]
        terms <- map["terms"]
        types <- map["types"]
        
    }
}



/*

{
    
    "description" : "CAU Blackheath, Royal Parade, London, United Kingdom",
    
    "id" : "33f1262464c817a6c0314662eab00bcf9739c51d",
    
    "matched_substrings" : [
    
    {
    
    "length" : 3,
    
    "offset" : 0
    
    }
    
    ],
    
    "place_id" : "ChIJF23SZtGp2EcRgtUJp5ZS6Ts",
    
    "reference" : "ClRCAAAAPUjSJvC-RYCyX7gLv8wsvZOWTu5dAMf5lqoHrtGPv1bIkMLxRMA9vAbUCdX6291qKmYeo8nXgJplPqSvdxhvaJYDwQuRYiuWs4OlLeVHQ2ISEAMh7CXKNwJdZJYqdsqDgOkaFFqGm21PZQqjMzHr7OlBoq1idoVX",
    
    "terms" : [
    
    {
    
    "offset" : 0,
    
    "value" : "CAU Blackheath"
    
    },
    
    {
    
    "offset" : 16,
    
    "value" : "Royal Parade"
    
    },
    
    {
    
    "offset" : 30,
    
    "value" : "London"
    
    },
    
    {
    
    "offset" : 38,
    
    "value" : "United Kingdom"
    
    }
    
    ],
    
    "types" : [ "establishment" ]
    
},
*/
