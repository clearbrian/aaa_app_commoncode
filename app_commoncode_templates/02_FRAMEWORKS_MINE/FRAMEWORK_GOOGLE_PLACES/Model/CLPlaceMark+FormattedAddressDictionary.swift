//
//  CLPlaceMark+FormattedAddressDictionary.swift
//  joyride
//
//  Created by Brian Clear on 04/11/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import CoreLocation

extension CLPlacemark{
    
    //safer way to get the address as a string - getting street/locality etc can produce
    //CLPlacemark > addressDictionary["FormattedAddressLines"]
    var formattedAddressLines: [String]?{
        
        var formattedAddressLines_: [String]?
        
        //---------------------------------------------------------------------
        if let addressDictionary = self.addressDictionary{
            
            //---------------------------------------------------------------------
            //Contacts uses new framework but ios8 contacts uses C api keys
            /*
             ▿ [8] : 2 elements
             - .0 : FormattedAddressLines
             ▿ .1 : 4 elements
             - [0] : 18 East Smithfield { ... }
             - [1] : London { ... }
             - [2] : E1W 1AP { ... }
             - [3] : England { ... }
             
             */
            if let formattedAddressArray = addressDictionary["FormattedAddressLines"] as? [String]{
                formattedAddressLines_ = formattedAddressArray
                
            }else{
                appDelegate.log.error("key:'FormattedAddressLines' returned nil - 46376457")
            }
            
        }else{
            appDelegate.log.error("dictionaryAddress is nil")
        }
        
        return formattedAddressLines_
    }
    
    
    var formattedAddressString: String?{
        
        var formattedAddressString_: String?
        
        //---------------------------------------------------------------------
        if let addressDictionary = self.addressDictionary{
            
            //non optional so easier to append
            var formattedAddress_ = ""
            //---------------------------------------------------------------------
            //Contacts uses new framework but ios8 contacts uses C api keys
            /*
             ▿ [8] : 2 elements
             - .0 : FormattedAddressLines
             ▿ .1 : 4 elements
             - [0] : 18 East Smithfield { ... }
             - [1] : London { ... }
             - [2] : E1W 1AP { ... }
             - [3] : England { ... }
             
             */
            if let formattedAddressArray = addressDictionary["FormattedAddressLines"]{
                let addressString = "\(formattedAddressArray)"
                
                formattedAddress_ = addressString.replacingOccurrences(of: "    ", with:" ")
                
                formattedAddress_ = formattedAddress_.replacingOccurrences(of: "\"", with:"")
                formattedAddress_ = formattedAddress_.replacingOccurrences(of: "(", with:"")
                formattedAddress_ = formattedAddress_.replacingOccurrences(of: ")", with:"")
                formattedAddress_ = formattedAddress_.replacingOccurrences(of: "\n", with:"")
                
            }else{
                appDelegate.log.error("key:'FormattedAddressLines' returned nil - 46376457")
            }
            
            if formattedAddress_ == ""{
                formattedAddressString_ = nil
            }else{
                formattedAddressString_ = formattedAddress_
            }
            
        }else{
            appDelegate.log.error("dictionaryAddress is nil")
        }
        
        
        return formattedAddressString_
    }
}
