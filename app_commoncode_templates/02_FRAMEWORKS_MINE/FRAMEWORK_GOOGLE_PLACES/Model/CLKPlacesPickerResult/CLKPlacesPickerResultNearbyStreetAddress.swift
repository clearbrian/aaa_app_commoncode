//
//  CLKPlacesPickerResultNearbyStreetAddress.swift
//  joyride
//
//  Created by Brian Clear on 24/10/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation

//--------------------------------------------------------------
// MARK: - level 2. CLKPlacesPickerResultPlace: CLKPlacesPickerResult
//--------------------------------------------------------------
class CLKPlacesPickerResultNearbyStreetAddress : CLKPlacesPickerResult{
    
    //map address
    var clkLocationPlaceSelected: CLKLocationPlace?
    
    //To LocationPicker
    override var clkPlaceSelectedReturned: CLKPlace?{
        return self.clkLocationPlaceSelected
    }
    
    
    override var hasOtherAddress: Bool{
        return false
    }
    
    //---------------------------------------------------------------------
    
    override var name :String?{
        var name_: String?
        
        if let clkLocationPlaceSelected = self.clkLocationPlaceSelected {
            name_ = clkLocationPlaceSelected.name
            
        }else{
            appDelegate.log.error("self.clkPlaceSelected is nil - cant set name_formatted_address")
        }
        return name_
    }
    
    // TODO: - OK
    override var formatted_address_main :String?{
        var formatted_address_main_ :String = ""
        
        if let clkLocationPlaceSelected = self.clkLocationPlaceSelected {
            if let formatted_address = clkLocationPlaceSelected.formatted_address {
                formatted_address_main_ = formatted_address
                
            }else{
                appDelegate.log.error("clkLocationPlaceSelected.formatted_address is nil")
            }
            
        }else{
            appDelegate.log.error("self.clkPlaceSelected is nil - cant set name_formatted_address")
        }
        
        return formatted_address_main_
    }
    
    override var formatted_address_geocoded :String?{
        let formatted_address_geocoded_ :String = "formatted_address_geocoded 6666"
        // TODO: - CLEANUP after not needed
        //        if let clkPlaceSelected = self.clkPlaceSelected {
        //            //            if clkPlaceSelected.isGeocoded {
        //            //                if let formatted_address = clkPlaceSelected.formatted_address {
        //            //                    formatted_address_ = formatted_address
        //            //
        //            //                }else{
        //            //                    appDelegate.log.error("sclkPlaceSelected.formatted_address is nil")
        //            //                }
        //            //
        //            //            }else{
        //            //                formatted_address_ = "NOT GEOCODED YET"
        //            //                appDelegate.log.error("useGeocodedAddress is YES but NOT GEOCODED YET isGeocoded is false")
        //            //            }
        //
        //            if let formatted_address = clkPlaceSelected.formatted_address {
        //                formatted_address_geocoded_ = formatted_address
        //
        //            }else{
        //                appDelegate.log.error("clkPlaceSelected.formatted_address is nil")
        //            }
        //        }else{
        //            appDelegate.log.error("self.clkPlaceSelected is nil - cant set name_formatted_address")
        //        }
        
        return formatted_address_geocoded_
    }
    
    
    
    //    override var name_formatted_address :String{
    //        var name_formatted_address_ :String = ""
    //
    //// TODO: - cleanup used self.name self.formatted_address
    ////        if let clkLocationPlaceSelected = self.clkLocationPlaceSelected {
    ////            if useGeocodedAddress{
    ////
    ////                if let clkLocationPlaceSelected = self.clkLocationPlaceSelected {
    ////
    ////                    let name = self.name
    ////                    let formatted_address = clkLocationPlaceSelected.formatted_address
    ////
    ////
    ////
    ////                }else{
    ////                    name_formatted_address_ = "ERROR useGeocodedAddress true but clkLocationPlaceSelected nil"
    ////                    appDelegate.log.error("[ERROR useGeocodedAddress true but clkLocationPlaceSelected nil")
    ////                }
    ////
    ////            }else{
    ////                //dont geocode use name/address from clkPlaceSelected
    ////                name_formatted_address_  = clkLocationPlaceSelected.name_formatted_address
    ////
    ////            }
    ////        }else{
    ////            appDelegate.log.error("self.clkLocationPlaceSelected is nil - cant set name_formatted_address")
    ////        }
    //
    //        if let name = self.name {
    //            if let formatted_address = self.formatted_address {
    //                name_formatted_address_ = "\(name), \(formatted_address)"
    //            }else{
    //                appDelegate.log.error("self.formatted_address is nil")
    //                name_formatted_address_ = "\(name)"
    //            }
    //        }else{
    //            if let formatted_address = self.formatted_address {
    //                name_formatted_address_ = "\(formatted_address)"
    //            }else{
    //                appDelegate.log.error("self.formatted_address is nil")
    //                name_formatted_address_ = ""
    //            }
    //        }
    //
    //        return name_formatted_address_
    //    }
    
    
    
    //--------------------------------------------------------------
    // EXTERNAL
    //--------------------------------------------------------------
    override var mainStringNAME : String{
        if let name = self.name {
            return name
        }else{
            appDelegate.log.error("self.name is nil - mainStringNAME >> ''")
            return " "
        }
    }
    // TODO: - code is exact same in subclass?
    override var mainStringADDRESS : String{
        if let formatted_address_main = self.formatted_address_main {
            return formatted_address_main
        }else{
            appDelegate.log.error("self.formatted_address_main is nil - mainStringADDRESS >> ''")
            return " "
        }
    }
    // TODO: - needed? anywhere where the name diff when geocoded?
    override var geoCodedStringNAME : String{
        return self.mainStringNAME
    }
    
    override var geoCodedStringADDRESS : String{
        var formatted_address_geocoded_ :String = ""
        
        //        if let clkLocationPlaceSelected = self.clkLocationPlaceSelected {
        //
        //            if clkLocationPlaceSelected.isGeocoded {
        //
        //                if let formatted_address = clkLocationPlaceSelected.formatted_address {
        //                    formatted_address_ = formatted_address
        //                }else{
        //                    appDelegate.log.error("clkLocationPlaceSelected.formatted_address is nil")
        //                }
        //            }else{
        //                formatted_address_ = "NOT GEOCODED YET"
        //                appDelegate.log.error("useGeocodedAddress is YES but NOT GEOCODED YET isGeocoded is false")
        //            }
        //
        //        }else{
        //            appDelegate.log.error("self.clkPlaceSelected is nil - cant set name_formatted_address")
        //        }
        //---------------------------------------------------------------------
        if let formatted_address_geocoded = self.formatted_address_geocoded {
            formatted_address_geocoded_ = formatted_address_geocoded
        }else{
            appDelegate.log.error("formatted_address_geocoded is nil - geoCodedStringADDRESS >> ''")
            return " "
        }
        //---------------------------------------------------------------------
        return formatted_address_geocoded_
    }
}
