//
//  CLKAddressAppleMapKit.swift
//  joyride
//
//  Created by Brian Clear on 09/11/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import MapKit

//MKLocalSearch returns array of MKMapItem
class CLKAddressAppleMapKit: CLKAddressApple{
    
    fileprivate var mkMapItem: MKMapItem?
    
    
    init(mapItem: MKMapItem) {
        super.init(clPlacemark:mapItem.placemark)
        //super.init()
        
        self.mkMapItem = mapItem
    }
    
    
    //mapItem.placemark is subclass MKPlacemark : CLPlacemark

    override var clPlacemark: CLPlacemark? {
        get {
            if let mkMapItem = self.mkMapItem {
                return mkMapItem.placemark
                
            }else{
                appDelegate.log.error("self.mapItem is nil >> clPlacemark is nil")
                return nil
                
            }
        }
        set {
            super.clPlacemark = clPlacemark
        }
    }

    

}
