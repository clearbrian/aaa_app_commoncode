//
//  CLKGeocodedAddressesResponse.swift
//  joyride
//
//  Created by Brian Clear on 26/10/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation

//--------------------------------------------------------------
// MARK: - ADDRESS COLLECTION - Apple
// MARK: -
//--------------------------------------------------------------

class CLKGeocodedAddressesAppleCollection: CLKGeocodedAddressesCollection{
    
    //wrapped for GMSAddress
    var clkAddressAppleArray = [CLKAddressApple]()
    
    override var firstAddress: CLKAddressApple?{
        var firstAddress_: CLKAddressApple? = nil
        
        //at least 1
        if clkAddressAppleArray.count > 0 {
            firstAddress_ = clkAddressAppleArray[0]
            
        }else{
            appDelegate.log.error("clkAddressAppleArray.count > 0 FAILED: addresses.count:\(clkAddressAppleArray.count)")
            
        }
        
        return firstAddress_
    }
    
    
    override var bestOrFirstCLKAddress: CLKAddressApple?{
        var bestOrFirstCLKAddress: CLKAddressApple? = nil
        
        for clkAddressApple : CLKAddressApple in clkAddressAppleArray {
            
            //NOISY self.log.debug("address for currentLocation:\(address)")
            
            //find FIRST address which has 'thoroughfare' / street
            //if has a street so most likely to be full address - though not 100% accurate
            //if let _ = gmsAddress.thoroughfare {
            if let _ = clkAddressApple.street {
                
                
                //---------------------------------------------------------------------
                /*
                 //has a street so most likely to be full address - though not 100% accurate
                 GMSAddress {
                 coordinate: (51.511054, -0.041803)
                 lines: St Paul's Ave, London E14 8DL, UK
                 thoroughfare: St Paul's Ave
                 locality: London
                 postalCode: E14 8DL
                 country: United Kingdom
                 }
                 */
                //---------------------------------------------------------------------
                
                bestOrFirstCLKAddress = clkAddressApple
                break;
                
            }else{
                //noisy self.log.error("Has no address.thoroughfare - skip")
            }
            
        }//for
        
        //---------------------------------------------------------------------
        // Check two - if address with thorougfare not find fall back to the first GMSAddress
        //---------------------------------------------------------------------
        if let _ = bestOrFirstCLKAddress {
            //address with throrough found - skip
        }else{
            self.log.debug("no GMSAddress with address.thoroughfare - using .firstResult() ")
            
            if clkAddressAppleArray.count > 0{
                bestOrFirstCLKAddress = clkAddressAppleArray[0]
            }else{
                self.log.error("clkAddressAppleArray.count > 0 FAILED:\(clkAddressAppleArray.count)")
            }
        }//check 2 - use firstResult
        //-----------------------------------------------------------------------------------
        return bestOrFirstCLKAddress
    }
}
