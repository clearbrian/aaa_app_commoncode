//
//  CLKGoogleGeocoder.swift
//  joyride
//
//  Created by Brian Clear on 18/10/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import GoogleMaps
class  CLKGoogleGeocoder: ParentSwiftObject{
    
    //--------------------------------------------------------------
    // MARK: - overloaded method CLLocation >> reverseGeocode(coordinate)
    // MARK: -
    //--------------------------------------------------------------

    //CLLocation > CLLocationCoordinate2D
    func reverseGeocode(_ coordinate: CLLocationCoordinate2D,
                                  success: @escaping (_ clkGeocodedAddressesGoogleCollection: CLKGeocodedAddressesGoogleCollection) -> Void,
                                  failure: @escaping (_ error: Error?) -> Void
        
        )
    {
        let clLocation = CLLocation.init(latitude: coordinate.latitude, longitude: coordinate.longitude)
        
        //------------------------------------------------
        self.reverseGeocode(clLocation,
                            
                            success:{(clkGeocodedAddressesResponse) -> Void in
                                //-------------------------------------
                                success(clkGeocodedAddressesResponse)
                                //-------------------------------------
                            },
                            failure:{(error: Error?) -> Void in
                                //-------------------------------------
                                failure(error)
                                //-------------------------------------
                            }
        )
        //------------------------------------------------
        
    }
    
    //--------------------------------------------------------------
    // MARK: - CALL reverseGeocode
    // MARK: -
    //--------------------------------------------------------------
    //call Google Geocoder - return all results - may want user to choose one if multiple results
    func reverseGeocode(_ clLocation:CLLocation,
                        success: @escaping (_ clkGeocodedAddressesGoogleCollection: CLKGeocodedAddressesGoogleCollection) -> Void,
                        failure: @escaping (_ error: Error?) -> Void
        
        )
    {
        if clLocation.isValid{
        
            //------------------------------------------------------------------------------------------------
            let gmsGeocoder = GMSGeocoder()
            //------------------------------------------------------------------------------------------------
            //CLLocation
            //GMSReverseGeocodeResponse
            //------------------------------------------------------------------------------------------------
            gmsGeocoder.reverseGeocodeCoordinate(clLocation.coordinate){ response , error in
                
                if error != nil {
                    self.log.error("error:\(String(describing: error))")
                    //---------------------------------------------------------------------
                    //north atlantic
                    //2015-07-21 17:18:28.493 [TripPickerViewController.swift:112] [openPlacesPicker] [bg255,255,255;[fg220,50,47;[Error_2] error:Error Domain=NSURLErrorDomain Code=-1001 "The request timed out." UserInfo=0x1704e4300 {NSUnderlyingError=0x174856da0 "The request timed out.", NSErrorFailingURLStringKey=https://clients4.google.com/glm/mmap, NSErrorFailingURLKey=https://clients4.google.com/glm/mmap, NSLocalizedDescription=The request timed out.} [;
                    //---------------------------------------------------------------------
                    failure(error as NSError?)
                }
                else
                {
                    // TODO: - each result has a type use ROOFTOP not APPROXIMATION
                    // see http://noc.to/geodecode#51.5101572,-0.0729212
                    
                    
                    //---------------------------------------------------------------------
                    //GMSReverseGeocodeResponse
                    //first result may not be the most accurate
                    /*
                     Optional([GMSAddress {
                     coordinate: (51.511094, -0.041829)
                     lines: , London, UK
                     locality: London
                     administrativeArea: Greater London
                     country: United Kingdom
                     }
                     , GMSAddress {
                     coordinate: (51.511094, -0.041829)
                     lines: St Paul's Ave, London E14 8DL, UK
                     thoroughfare: St Paul's Ave
                     locality: London
                     postalCode: E14 8DL
                     country: United Kingdom
                     }
                     , GMSAddress {
                     coordinate: (51.511094, -0.041829)
                     lines: , London E14, UK
                     locality: London
                     postalCode: E14
                     country: United Kingdom
                     }
                     , GMSAddress {
                     coordinate: (51.511094, -0.041829)
                     lines: London Borough of Tower Hamlets, Greater London, UK
                     administrativeArea: Greater London
                     country: United Kingdom
                     }
                     , GMSAddress {
                     coordinate: (51.511094, -0.041829)
                     lines: , London, UK
                     locality: London
                     country: United Kingdom
                     }
                     , GMSAddress {
                     coordinate: (51.511094, -0.041829)
                     lines: , Greater London, UK
                     administrativeArea: Greater London
                     country: United Kingdom
                     }
                     , GMSAddress {
                     coordinate: (51.511094, -0.041829)
                     lines: United Kingdom
                     country: United Kingdom
                     }
                     ])
                     
                     */
                    /*
                     [GoogleGeocoder.swift:147 reverseGeocode(coordinate:success:failure:)] gmsReverseGeocodeResponse.results():Optional([GMSAddress {
                     coordinate: (51.509161, -0.073020)
                     lines: 2 Royal Mint Court, London EC3N, UK
                     thoroughfare: 2 Royal Mint Court
                     locality: London
                     postalCode: EC3N
                     country: United Kingdom
                     }
                     , GMSAddress {
                     coordinate: (51.509161, -0.073020)
                     lines: , London, UK
                     locality: London
                     administrativeArea: Greater London
                     country: United Kingdom
                     }
                     , GMSAddress {
                     coordinate: (51.509161, -0.073020)
                     lines: Royal Mint Court, London EC3N 4HJ, UK
                     thoroughfare: Royal Mint Court
                     locality: London
                     postalCode: EC3N 4HJ
                     country: United Kingdom
                     }
                     , GMSAddress {
                     coordinate: (51.509161, -0.073020)
                     lines: , London EC3N, UK
                     locality: London
                     postalCode: EC3N
                     country: United Kingdom
                     }
                     , GMSAddress {
                     coordinate: (51.509161, -0.073020)
                     lines: London Borough of Tower Hamlets, UK
                     administrativeArea: Greater London
                     country: United Kingdom
                     }
                     , GMSAddress {
                     coordinate: (51.509161, -0.073020)
                     lines: , London, UK
                     locality: London
                     country: United Kingdom
                     }
                     , GMSAddress {
                     coordinate: (51.509161, -0.073020)
                     lines: , Greater London, UK
                     administrativeArea: Greater London
                     country: United Kingdom
                     }
                     ])
                     
                     //---------------------------------------------------------------------
                     
                     [GoogleGeocoder.swift:147 reverseGeocode(coordinate:success:failure:)] gmsReverseGeocodeResponse.results():Optional([GMSAddress {
                     coordinate: (51.374769, 1.258349)
                     lines: Saint Nicholas-at-Wade, Birchington CT7 0NB, UK
                     subLocality: Saint Nicholas-at-Wade
                     locality: Birchington
                     postalCode: CT7 0NB
                     country: United Kingdom
                     }
                     , GMSAddress {
                     coordinate: (51.374769, 1.258349)
                     lines: , Birchington CT7, UK
                     locality: Birchington
                     country: United Kingdom
                     }
                     , GMSAddress {
                     coordinate: (51.374769, 1.258349)
                     lines: CT7, UK
                     postalCode: CT7
                     country: United Kingdom
                     }
                     , GMSAddress {
                     coordinate: (51.374769, 1.258349)
                     lines: Thanet District, UK
                     administrativeArea: Kent
                     country: United Kingdom
                     }
                     , GMSAddress {
                     coordinate: (51.374769, 1.258349)
                     lines: , Kent, UK
                     administrativeArea: Kent
                     country: United Kingdom
                     }
                     , GMSAddress {
                     coordinate: (51.374769, 1.258349)
                     lines: United Kingdom
                     country: United Kingdom
                     }
                     , GMSAddress {
                     coordinate: (51.374769, 1.258349)
                     lines: Unnamed Road, Birchington CT7 0QR, UK
                     thoroughfare: Unnamed Road
                     locality: Birchington
                     postalCode: CT7 0QR
                     country: United Kingdom
                     }
                     ])
                     
                     */
                    
                    
                    
                    
                    //---------------------------------------------------------------------
                    if let gmsReverseGeocodeResponse = response {
                        //DEBUG - see comments above
                        //self.log.debug("gmsReverseGeocodeResponse.results():\(gmsReverseGeocodeResponse.results())")
                        
                        let arrayAnyObjects : [AnyObject]? = gmsReverseGeocodeResponse.results()
                        
                        if let arrayAnyObjects = arrayAnyObjects {
                            
                            //------------------------------------------------------------------------------------------------
                            //[GMSAddress]
                            //------------------------------------------------------------------------------------------------
                            if let arrayGMSAddress = arrayAnyObjects as? [GMSAddress] {
                                //--------------------------------------------------------
                                //[GMSAddress] >> [CLKAddressGoogle] >> CLKGeocodedAddressesGoogleCollection
                                //--------------------------------------------------------
                                let clkGeocodedAddressesGoogleCollection = CLKGeocodedAddressesGoogleCollection()
                                
                                for gmsAddress : GMSAddress in arrayGMSAddress {
                                    
                                    let clkAddressGoogle = CLKAddressGoogle(gmsAddress:gmsAddress)
                                    clkGeocodedAddressesGoogleCollection.clkAddressGoogleArray.append(clkAddressGoogle)
                                    
                                }//for
                                
                                //let clkGoogleGeocoderResult =  CLKGoogleGeocoderResult(clkAddressGoogle: clkAddressGoogle)
                                success(clkGeocodedAddressesGoogleCollection)
                               
                            }else{
                                appDelegate.log.error("arrayAnyObjects as? [GMSAddress] is nil")
                                 failure(NSError.genericError("arrayAnyObjects as? [GMSAddress] is nil"))
                            }
                        }else{
                            self.log.error("gmsReverseGeocodeResponse.results() is nil")
                            failure(NSError.genericError("gmsReverseGeocodeResponse.results() is nil"))
                        }
                    }else{
                        self.log.error("response : GMSReverseGeocodeResponse is nil")
                        failure(NSError.genericError("GMSReverseGeocodeResponse is nil"))
                    }
                    //---------------------------------------------------------------------
                }//geocoder no error
            }//geocoder - block
            //------------------------------------------------------------------------------------------------
        }else{
            self.log.error("clLocation.isValid failed")
            let errorLocation = NSError.errorWithMessage("Invalid CLLocation:\(clLocation)", code: 6765798)
            failure(errorLocation)
        }
    }
}

// TODO: - CLEANUP after test//                        //------------------------------------------------------------------------------------------------
//                        for address : GMSAddress in (arrayAnyObjects as! [GMSAddress]) {
//
//                            //reset for each
//                            addressString_ = ""
//
//                            //NOISY self.log.debug("address for currentLocation:\(address)")
//
//                            if let _ = address.thoroughfare {
//                                //---------------------------------------------------------------------
//                                /*
//                                 //has a street so most likely to be full address - though not 100% accurate
//                                 , GMSAddress {
//                                 coordinate: (51.511054, -0.041803)
//                                 lines: St Paul's Ave, London E14 8DL, UK
//                                 thoroughfare: St Paul's Ave
//                                 locality: London
//                                 postalCode: E14 8DL
//                                 country: United Kingdom
//                                 }
//                                 */
//
//                                //---------------------------------------------------------------------
//                                //self.textViewAddressStart.text = "\(address.lines)"
//                                //address.lines is array so "\(address.lines)" sticks [] [around it]
//
//                                if let lines = address.lines{
//
//                                    for iCount in 0 ..< lines.count{
//                                        addressString_ = addressString_ + "\(lines[iCount])"
//
//                                        if iCount == lines.count - 1{
//                                            addressString_ = addressString_ + ""
//                                        }else{
//                                            addressString_ = addressString_ + ", "
//                                        }
//
//                                    }
//                                    // TODO: - Royal Mint Court - first address with thorough may not be the best
//                                    //foudn address escape - may be others with thourough
//
//                                    //-----------------------------------------------------
//                                    gmsAddressUsed = address
//                                    //-----------------------------------------------------
//                                    break
//                                }else{
//                                    self.log.error("address.lines is nil")
//                                }
//                            }else{
//                                self.log.error("Has no address.thoroughfare - skip")
//                            }
//                        }//for
//
//                        //check if any address with thoroughfare found if not use first
//                        if addressString_ == ""{
//                            //USE THE FIRST
//                            if let gmsAddress : GMSAddress = gmsReverseGeocodeResponse.firstResult() {
//                                //---------------------------------------------------------------------
//                                //NOISY self.log.debug("address for currentLocation:\(address)")
//                                //self.textViewAddressStart.text = "\(address.lines)"
//                                //address.lines is array so "\(address.lines)" sticks [] [around it]
//                                addressString_ = ""
//                                //---------------------------------------------------------------------
//                                if let lines = gmsAddress.lines{
//                                    for iCount in 0..<lines.count{
//                                        //first line can be empty
//                                        //lines: , Salisbury, Wiltshire SP4 7DE, UK
//
//                                        if lines[iCount].trim() == ""{
//                                            self.log.debug("first line blank skipping")
//                                        }else{
//                                            //something in the line
//                                            addressString_ = addressString_ + "\(lines[iCount])"
//
//                                            if iCount == lines.count - 1{
//                                                addressString_ = addressString_ + ""
//                                            }else{
//                                                addressString_ = addressString_ + ", "
//                                            }
//                                        }
//                                    }
//                                }else{
//                                    self.log.error("address.lines is nil")
//                                }
//                                //-----------------------------------------------------------------------------------
//
//                                let clkGoogleGeocoderResult =  CLKGoogleGeocoderResult(addressString: addressString_, googleGeocodedGMSAddress: gmsAddress)
//
//
//
//
//
//
//                                success(clkGoogleGeocoderResult)
//                                //-----------------------------------------------------------------------------------
//
//                            }else{
//                                self.log.error("address is nil")
//                                failure(NSError.genericError("address is nil"))
//                            }
//                        }else{
//                            //thoroughfare found - address with street found
//                            if let gmsAddressUsed = gmsAddressUsed {
//                                //---------------------------------------------------------------------
//                                //success("\(addressString_)", gmsAddressUsed)
//                                let clkGoogleGeocoderResult =  CLKGoogleGeocoderResult(addressString: addressString_, googleGeocodedGMSAddress: gmsAddressUsed)
//                                success(clkGoogleGeocoderResult)
//                                //---------------------------------------------------------------------
//                            }else{
//                                self.log.error("addressUsed is nil")
//                            }
//                        }
//                        //------------------------------------------------------------------------------------------------
//
