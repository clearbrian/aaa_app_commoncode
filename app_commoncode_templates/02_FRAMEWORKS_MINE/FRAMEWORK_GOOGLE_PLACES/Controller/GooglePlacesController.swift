//
//  GooglePlacesController.swift
//  joyride
//
//  Created by Brian Clear on 24/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import GoogleMaps

class GooglePlacesController : ParentSwiftObject{
    
    let googlePlacesWSController = GooglePlacesWSController()
    let clkGooglePlacesTypesManager = CLKGooglePlacesTypesManager()
    
    static let defaultSearchRadius : Double = 75.0
    
    override init(){
        super.init()
        
    }
    
    
    func doNearbySearch(_ location: CLLocation,
                          radius:Double?,
                         success: @escaping (_ results: [CLKGooglePlaceResult]?) -> Void,
                         failure: @escaping (_ error: Error?) -> Void
        )
    {
        
        //------------------------------------------------------------------------------------------------
        //CLKPlacesNearbySearchRequest
        //------------------------------------------------------------------------------------------------

        let clkPlacesNearbySearchRequest:CLKPlacesNearbySearchRequest = CLKPlacesNearbySearchRequest()
        clkPlacesNearbySearchRequest.location = "\(location.coordinate.latitude),\(location.coordinate.longitude)"
        //https://developers.google.com/maps/faq#using-google-maps-apis
        //clkPlacesNearbySearchRequest.language = "fr"
        //clkPlacesNearbySearchRequest.language = "ar"//arabic
        //clkPlacesNearbySearchRequest.language = "zh-CN"//arabic
        
        
        if let radius = radius{
            clkPlacesNearbySearchRequest.radius = radius
        }else{
            self.log.error("radius is nil - default to \(GooglePlacesController.defaultSearchRadius)")
            //default set internally - comment in to change from default
            //clkPlacesNearbySearchRequest.radius = GooglePlacesController.defaultSearchRadius
        }
        
        //---------------------------------------------------------------------
        self.googlePlacesWSController.get_place_nearbysearch(clkPlacesNearbySearchRequest,
            success:{
                (clkPlaceSearchResponse: CLKGooglePlaceSearchResponse?)->Void in
                //---------------------------------------------------------------------
                if let clkPlaceSearchResponse = clkPlaceSearchResponse{
                    self.log.debug("clkPlacesNearbySearchRequest returned")
                    
                    //Two types
                    //clkPlaceSearchResponse.result
                    //clkPlaceSearchResponse.results
                    
                    //---------------------------------------------------------------------
                    //Place/details - // /api/place/details >> clkPlaceSearchResponse.result
                    if let _ = clkPlaceSearchResponse.result{
                        
                    }else{
                        self.log.error("clkPlaceSearchResponse.result is nil - OK if /api/place/nearbysearch use .results")
                    }
                    //---------------------------------------------------------------------
                    // /api/place/nearbysearch >> clkPlaceSearchResponse.results
                    if let results = clkPlaceSearchResponse.results{
                        self.log.debug("clkPlaceSearchResponse.results:\(results.count) PLACES FOUND")
                        //OK but VC has better output
                        //    for clkGooglePlaceResult in results{
                        //        if let name = clkGooglePlaceResult.name{
                        //            self.log.debug("PLACE:\(name)")
                        //        }else{
                        //            self.log.error("clkGooglePlaceResult.name is nil")
                        //        }
                        //    }
                        //---------------------------------------------------------------------
                        //return to CALLER
                        success(results)
                        //---------------------------------------------------------------------
                    }else{
                        self.log.error("clkPlaceSearchResponse.results is nil - OK if /api/place/details use .result")
                    }
                }else{
                    self.log.error("clkPlaceSearchResponse is nil or not CLKGooglePlaceSearchResponse")
                }
                //---------------------------------------------------------------------
                
            },
            failure:{
                (error) -> Void in
//                self.log.error("error:\(error)")
//                
//                CLKAlertController.showAlertInVCWithCallback(self, title: "Error", message: error?.localizedDescription)
//                    { () -> Void in
//                        self.log.debug("Ok tapped = alert closed appDelegate.doLogout()")
//                }
                return failure(error)
                
            }
        )
        //---------------------------------------------------------------------
    }

    func doPlaceAutoComplete(_ input:String,
        location: CLLocation,
        radius:Double?,
        success: @escaping (_ predictions: [CLKGooglePlaceAutocompletePrediction]?) -> Void,
        failure: @escaping (_ error: Error?) -> Void
        )
    {
        
        //------------------------------------------------------------------------------------------------
        //CLKPlacesNearbySearchRequest
        //------------------------------------------------------------------------------------------------
        
        let clkPlacesAutoCompleteRequest:CLKPlacesAutoCompleteRequest = CLKPlacesAutoCompleteRequest()
        //---------------------------------------------------------------------
        clkPlacesAutoCompleteRequest.input = input
        //---------------------------------------------------------------------
        clkPlacesAutoCompleteRequest.location = "\(location.coordinate.latitude),\(location.coordinate.longitude)"
        //---------------------------------------------------------------------
        if let radius = radius{
            clkPlacesAutoCompleteRequest.radius = radius
        }else{
            self.log.error("radius is nil - default to \(GooglePlacesController.defaultSearchRadius)")
            //default set internally - comment in to change from default
            //clkPlacesNearbySearchRequest.radius = GooglePlacesController.defaultSearchRadius
        }
        
        //---------------------------------------------------------------------
    
        self.googlePlacesWSController.get_place_autocomplete(clkPlacesAutoCompleteRequest,
            success:{
                (clkPlaceAutocompleteResponse: CLKGooglePlaceAutocompleteResponse?)->Void in
                //---------------------------------------------------------------------
                if let clkPlaceAutocompleteResponse = clkPlaceAutocompleteResponse{
                    self.log.debug("get_place_autocomplete returned")
                    
                
                    //---------------------------------------------------------------------
                    if let predictions = clkPlaceAutocompleteResponse.predictions{
                        //self.log.debug("clkPlaceAutocompleteResponse.predictions:\(predictions.count) PLACES FOUND")
                        //---------------------------------------------------------------------
                        //return to CALLER
                        success(predictions)
                        //---------------------------------------------------------------------
                    }else{
                        self.log.error("clkPlaceSearchResponse.results is nil - OK if /api/place/details use .result")
                    }
                }else{
                    self.log.error("clkPlaceSearchResponse is nil or not CLKGooglePlaceSearchResponse")
                }
                //---------------------------------------------------------------------
                
            },
            failure:{
                (error) -> Void in
                //                self.log.error("error:\(error)")
                //
                //                CLKAlertController.showAlertInVCWithCallback(self, title: "Error", message: error?.localizedDescription)
                //                    { () -> Void in
                //                        self.log.debug("Ok tapped = alert closed appDelegate.doLogout()")
                //                }
                
//                to check - should this just be
                failure(error)
//                not
//                return failure(error)
//                its a closure
//                same as success(...)
                
            }
        )
        //---------------------------------------------------------------------
    }
    func doQueryAutoComplete(_ input:String,
        location: CLLocation,
        radius:Double?,
        success: @escaping (_ predictions: [CLKGooglePlaceAutocompletePrediction]?) -> Void,
        failure: @escaping (_ error: Error?) -> Void
        )
    {
        
        //------------------------------------------------------------------------------------------------
        //CLKPlacesNearbySearchRequest
        //------------------------------------------------------------------------------------------------
        
        let clkPlacesQueryAutoCompleteRequest:CLKPlacesQueryAutoCompleteRequest = CLKPlacesQueryAutoCompleteRequest()
        //---------------------------------------------------------------------
        clkPlacesQueryAutoCompleteRequest.input = input
        //---------------------------------------------------------------------
        clkPlacesQueryAutoCompleteRequest.location = "\(location.coordinate.latitude),\(location.coordinate.longitude)"
        //---------------------------------------------------------------------
        if let radius = radius{
            clkPlacesQueryAutoCompleteRequest.radius = radius
        }else{
            self.log.error("radius is nil - default to \(GooglePlacesController.defaultSearchRadius)")
            //default set internally - comment in to change from default
            //clkPlacesNearbySearchRequest.radius = GooglePlacesController.defaultSearchRadius
        }
        
        //---------------------------------------------------------------------
        self.googlePlacesWSController.get_query_autocomplete(clkPlacesQueryAutoCompleteRequest,
            success:{
                (clkPlaceAutocompleteResponse: CLKGooglePlaceAutocompleteResponse?)->Void in
                //---------------------------------------------------------------------
                if let clkPlaceAutocompleteResponse = clkPlaceAutocompleteResponse{
                    self.log.debug("clkPlacesQueryAutoCompleteRequest returned")
                    
                    
                    //---------------------------------------------------------------------
                    if let predictions = clkPlaceAutocompleteResponse.predictions{
                        //self.log.debug("clkPlaceAutocompleteResponse.predictions:\(predictions.count) PLACES FOUND")
                        //---------------------------------------------------------------------
                        //return to CALLER
                        success(predictions)
                        //---------------------------------------------------------------------
                    }else{
                        self.log.error("clkPlaceSearchResponse.results is nil - OK if /api/place/details use .result")
                    }
                }else{
                    self.log.error("clkPlaceSearchResponse is nil or not CLKGooglePlaceSearchResponse")
                }
                //---------------------------------------------------------------------
                
            },
            failure:{
                (error) -> Void in
                //                self.log.error("error:\(error)")
                //
                //                CLKAlertController.showAlertInVCWithCallback(self, title: "Error", message: error?.localizedDescription)
                //                    { () -> Void in
                //                        self.log.debug("Ok tapped = alert closed appDelegate.doLogout()")
                //                }
                return failure(error)
                
            }
        )
        //---------------------------------------------------------------------
    }
}
