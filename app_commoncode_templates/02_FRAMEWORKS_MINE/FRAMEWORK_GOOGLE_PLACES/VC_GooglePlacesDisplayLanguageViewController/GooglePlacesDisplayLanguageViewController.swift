//
//  GooglePlacesDisplayLanguageViewController.swift
//  joyride
//
//  Created by Brian Clear on 17/05/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit
class GooglePlacesDisplayLanguageViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
       // let supportedLanguagesGooglePlacesAPIArray =  Locale.displayNamesForGooglePlaces("en")
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //https://developers.google.com/maps/faq#using-google-maps-apis
    var arrayTableData: [String] = [];
    
    //------------------------------------------------
    // MARK: UITableViewDataSource
    //------------------------------------------------
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return arrayTableData.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        
        let cellIdentifier = "FixturesSearchDataTableViewCell"
        
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as UITableViewCell!
        if cell == nil {
            cell = UITableViewCell(style:.default, reuseIdentifier:cellIdentifier)
        }
        let rowText = self.arrayTableData[(indexPath as NSIndexPath).row]
        cell?.textLabel!.text = rowText
        
        return cell!
    }
    
    //------------------------------------------------
    // MARK: UITableViewDelegate
    //------------------------------------------------
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let rowText = self.arrayTableData[(indexPath as NSIndexPath).row]
        print("didSelectRowAtIndexPath:ROW:\((indexPath as NSIndexPath).row) VALUE:\(rowText)")
    }
}
