//
//  MapIconView.swift
//  joyride
//
//  Created by Brian Clear on 18/08/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation

//
//  PlayPauseIconView.swift
//  HeySwift
//
//  Created by Andy VanWagoner on 6/16/14.
//  Copyright (c) 2014 Andy VanWagoner. All rights reserved.
//

import UIKit

class MapIconView: UIView {
    var label : UILabel?
    
    let log = MyXCodeEmojiLogger.defaultInstance
    
    override init(frame: CGRect) {
        self.stringToDisplay = "."
        self.fillColor = UIColor.appColorCYAN()
        
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.clear
        self.addLabel()

    }
    //Added to IB
    required init?(coder aDecoder: NSCoder) {
        
        self.stringToDisplay = "."
        self.fillColor = UIColor.appColorCYAN()
        super.init(coder: aDecoder)
        
        self.addLabel()
        
    }
    
    func addLabel(){
        //UILabel
        self.label = UILabel(frame: self.bounds)
        self.label?.minimumScaleFactor = 0.4
        self.label?.textAlignment = .center
        //label?.backgroundColor = UIColor.cyanColor()
        self.label?.text = self.stringToDisplay
        self.label?.font = self.label?.font.withSize(10.0)
        //let stringSize = self.stringToDisplay.sizeWithAttributes(attributes)
        
        //CGSize lLabelSize = self.stringToDisplay.sizeWithFont( self.label?.font, forWidth:self.label?.frame.size.width lineBreakMode:factLabel.lineBreakMode];
        

        self.addSubview(label!)
    }
    
    var stringToDisplay : String {
        didSet {
            label?.text = stringToDisplay
            setNeedsDisplay()
        }
    }
    var fillColor : UIColor {
        didSet {
            //force redraw
            setNeedsDisplay()
        }
    }
    
    override func draw(_ rect: CGRect) {
        
        //------------------------------------------------
        //DRAW CIRCLE
        //------------------------------------------------
        if let ctx : CGContext = UIGraphicsGetCurrentContext(){
            //let locationOfTouch : CGPoint = CGPoint(x: self.bounds.size.height/2.0,
//                y: self.bounds.size.width/2.0)
            ctx.saveGState()
            
            ctx.setLineWidth(1)
            ctx.setStrokeColor(red: 1, green: 0, blue: 0, alpha: 1)//red
            //func CGContextAddArc(c: CGContext!, x: CGFloat, y: CGFloat, radius: CGFloat, startAngle: CGFloat, endAngle: CGFloat, clockwise: Int32)
            
            //        let c: CGContext! = ctx
//            let x: CGFloat = locationOfTouch.x
//            let y: CGFloat = locationOfTouch.y
//            let radius: CGFloat = (self.bounds.width-5) / 2.0
//            let startAngle: CGFloat = 0.0
//            let endAngle: CGFloat =  CGFloat(M_PI*2)
//            let clockwise: Int32 = 1
            
print("TODO SWIFT3:CGContextAddArc")
////            CGContextAddArc(ctx,
////                locationOfTouch.x,
////                locationOfTouch.y,
////                radius,
////                startAngle,
////                endAngle,
////                clockwise)
//            AddArc(locationOfTouch.x,
//                            locationOfTouch.y,
//                            radius,
//                            startAngle,
//                            endAngle,
//                            clockwise)
//            ctx.strokePath()
            
            //------------------------------------------------
        }else{
            self.log.error("UIGraphicsGetCurrentContext() is nil")
        }

    }
    func viewAsImage() -> UIImage?{
        var returnedImage : UIImage? = nil;
        
        let iconWidth: CGFloat = 24
        let iconHeight: CGFloat = 24
        
        //let returnedRect_bounds =  CGRectMake(0, 0, iconWidth, iconHeight)
        
        
        //------------------------------------------------
        //DRAW CIRCLE
        //------------------------------------------------
        UIGraphicsBeginImageContextWithOptions(CGSize(width: iconWidth, height: iconHeight), false, 0.0)
        
//        var ctx : CGContextRef = UIGraphicsGetCurrentContext()!
        
//            var centerPoint : CGPoint = CGPointMake(returnedRect_bounds.size.height/2.0,
//                returnedRect_bounds.size.width/2.0)
//            CGContextSaveGState(ctx)
//            
//            CGContextSetLineWidth(ctx,1)
//            CGContextSetRGBStrokeColor(ctx, 1, 0, 0, 1)//red
//            //func CGContextAddArc(c: CGContext!, x: CGFloat, y: CGFloat, radius: CGFloat, startAngle: CGFloat, endAngle: CGFloat, clockwise: Int32)
//            
//            var c: CGContext! = ctx
//            var x: CGFloat = centerPoint.x
//            var y: CGFloat = centerPoint.y
//            
//            var radius: CGFloat = (returnedRect_bounds.width-5) / 2.0
//            
//            var startAngle: CGFloat = 0.0
//            var endAngle: CGFloat =  CGFloat(M_PI*2)
//            var clockwise: Int32 = 1
//            
//            
//            CGContextAddArc(ctx,
//                centerPoint.x,
//                centerPoint.y,
//                radius,
//                startAngle,
//                endAngle,
//                clockwise)
//            CGContextStrokePath(ctx)
//            
//            CGContextRestoreGState(ctx)
        returnedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        //------------------------------------------------
        return returnedImage
        
    }
}
