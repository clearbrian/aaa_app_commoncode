//
//  GooglePlacesWSController.swift
//  joyride
//
//  Created by Brian Clear on 03/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation

//import Alamofire
import CoreLocation

//OK indicates that no errors occurred; the place was successfully detected and at least one result was returned.
//ZERO_RESULTS indicates that the search was successful but returned no results. This may occur if the search was passed a latlng in a remote location.
//OVER_QUERY_LIMIT indicates that you are over your quota.
//REQUEST_DENIED indicates that your request was denied, generally because of lack of an invalid key parameter.
//INVALID_REQUEST generally indicates that a required query parameter (location or radius) is missing.

enum GooglePlacesResponseStatus : String {
    case OK = "OK"
    case ZERO_RESULTS = "ZERO_RESULTS"
    case OVER_QUERY_LIMIT = "OVER_QUERY_LIMIT"
    case REQUEST_DENIED = "REQUEST_DENIED"
    case INVALID_REQUEST = "INVALID_REQUEST"
    
}



//https://developers.google.com/places/webservice/search
class GooglePlacesWSController: ParentWSController{
    
    //with photos
    //ChIJyWEHuEmuEmsRm9hTkapTCrk
    
//    // Place Search
//    https://developers.google.com/places/webservice/search
//        // Nearby Search Requests
//        https://maps.googleapis.com/maps/api/place/nearbysearch/output?parameters
//        
//        Text Search Requests
//        https://maps.googleapis.com/maps/api/place/textsearch/output?parameters
//        
//        Radar Search Requests
//        https://maps.googleapis.com/maps/api/place/radarsearch/output?parameters
//    ======================
//    Place Details
//    https://developers.google.com/places/webservice/details
//    
//            Place Details Requests
//            https://maps.googleapis.com/maps/api/place/details/output?parameters
//            
                //---------------------------------------------------------------------
//              Place Add
//              https://developers.google.com/places/webservice/add-place
//            
//                  POST
//                  Add a place
//                  https://maps.googleapis.com/maps/api/place/add/json?key=API_KEY
//                  //POST details in body
//            
//                  Delete a place
//                  https://maps.googleapis.com/maps/api/place/delete/json?key=API_KEY
//    ======================
//    Place Photos
//            https://developers.google.com/places/webservice/photos
//            Place Photo Requests
//            https://maps.googleapis.com/maps/api/place/photo?parameters
//            https://github.com/MoZhouqi/PhotoBrowser
//    ======================
//    Autocomplete
//            Place Autocomplete
//            https://maps.googleapis.com/maps/api/place/autocomplete/output?parameters
//            
//            Query Autocomplete - "pizza near New York"
//            https://maps.googleapis.com/maps/api/place/queryautocomplete/output?parameters
    
    
    // "place_id" = ChIJZeLN3lMDdkgR5Zx5syC5NgI;
    func get_place_detail(_ placeId: String,
                              success: @escaping (_ clkPlaceSearchResponse: CLKGooglePlaceSearchResponse?) -> Void,
                              failure: @escaping (_ error: Error?) -> Void
            )
    {
        //https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyCh_0tfBGDU7l_RHXuq5zol9amft7e3szg&placeid=ChIJN1t_tDeuEmsRUsoyG83frY4) [;
        
        //---------------------------------------------------------------------
        let clkPlacesPlaceDetailsRequest:CLKPlacesPlaceDetailsRequest = CLKPlacesPlaceDetailsRequest()
        clkPlacesPlaceDetailsRequest.placeid = placeId
        //---------------------------------------------------------------------
        //CALL THE WS
        //---------------------------------------------------------------------
        //if let urlString =  clkPlacesPlaceDetailsRequest.urlString{
        if let _ = clkPlacesPlaceDetailsRequest.urlString{
            
            //NOISY self.log.debug("urlString:\r\(urlString)")
            //---------------------------------------------------------------------
            self.callWS(
                clkPlacesPlaceDetailsRequest,
                
                success:{
                    (parsedObject: Any?)->Void in
                        //---------------------------------------------------------------------
                        //response handler takes CLKGooglePlaceSearchResponse? so use
                        //if let clkPlaceSearchResponse : CLKGooglePlaceSearchResponse? =
                        //instead of
                        //if let clkPlaceSearchResponse =
                        //as this is a CLKGooglePlaceSearchResponse not CLKGooglePlaceSearchResponse? so response(clkPlaceSearchResponse) wont take it
                        if let clkPlaceSearchResponse : CLKGooglePlaceSearchResponse = Mapper<CLKGooglePlaceSearchResponse>().map(JSONObject:parsedObject){
                            
                            self.log.debug("clkPlacesPlaceDetailsRequest returned")
                            //---------------------------------------------------------------------
                            //check response status
                            if let status = clkPlaceSearchResponse.status{
                                switch status{
                                case GooglePlacesResponseStatus.OK.rawValue:
                                    self.log.debug("status: OK")
                                    //---------------------------------------------------------------------
                                    //RESPONSE OK
                                    //---------------------------------------------------------------------
                                    success(clkPlaceSearchResponse)
                                    //---------------------------------------------------------------------
                                case GooglePlacesResponseStatus.ZERO_RESULTS.rawValue:
                                    self.log.debug("status: ZERO_RESULTS")
                                    return failure(NSError.appError(.googlePlacesResponseStatus_ZERO_RESULTS_PLACES))
                                    //return failure(NSError.appError(.GooglePlacesResponseStatus_ZERO_RESULTS_DISTANCE))
                                    
                                case GooglePlacesResponseStatus.OVER_QUERY_LIMIT.rawValue:
                                    self.log.debug("status: OVER_QUERY_LIMIT")
                                    return failure(NSError.appError(.googlePlacesResponseStatus_OVER_QUERY_LIMIT))
                                    
                                case GooglePlacesResponseStatus.REQUEST_DENIED.rawValue:
                                    self.log.debug("status: REQUEST_DENIED")
                                    return failure(NSError.appError(.googlePlacesResponseStatus_REQUEST_DENIED))
                                    
                                case GooglePlacesResponseStatus.INVALID_REQUEST.rawValue:
                                    
                                    self.log.debug("status: INVALID_REQUEST")
                                    return failure(NSError.appError(.googlePlacesResponseStatus_INVALID_REQUEST))
                                default:
                                    //HANDLE ALL ERRORS - if you need to handle specific ones such as NO RESULT can comment out here but probably should handle in delegate/caller
                                    //"ZERO_RESULTS"
                                    //"OVER_QUERY_LIMIT"
                                    //"REQUEST_DENIED"  //problem with key
                                    //"INVALID_REQUEST"
                                    self.log.debug("status: \(status)")
                                    return failure(NSError.googlePlacesAPIStatusError(status))
                                }
                                
                            }else{
                                self.log.error("clkPlaceSearchResponse.status is nil")
                                return failure(NSError.unexpectedResponseObject("clkPlaceSearchResponse.status is nil"))
                            }

                            //------------------------------------------------------------------------------------------------

                        }else{
                            self.log.error("clkPlaceSearchResponse is nil or not CLKGooglePlaceSearchResponse")
                        }
                    },
                failure:{
                    (error) -> Void in
                    //NO WS ERROR but may be status errors see success:
                    self.log.error("error:\(error)")
                    failure(error)
                }
            )
            //---------------------------------------------------------------------
        }else{
            self.log.error("urlString is nil - 16787876")
        }
    }
    
    func get_place_nearbysearch(_ clkPlacesNearbySearchRequest: CLKPlacesNearbySearchRequest,
        success: @escaping (_ clkPlaceSearchResponse: CLKGooglePlaceSearchResponse?) -> Void,
        failure: @escaping (_ error: Error?) -> Void
        )
    {
        //https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-33.8670522,151.1957362&radius=500&types=food&name=cruise&key=AIzaSyCh_0tfBGDU7l_RHXuq5zol9amft7e3szg

        //---------------------------------------------------------------------
        //CALL THE WS
        //---------------------------------------------------------------------
        //if let urlString =  clkPlacesNearbySearchRequest.urlString{
        if let _ = clkPlacesNearbySearchRequest.urlString{
                
            //NOISY self.log.debug("urlString:\r\(urlString)")
            //---------------------------------------------------------------------
            self.callWS(
                clkPlacesNearbySearchRequest,
                
                success:{
                    (parsedObject: Any?)->Void in
                    //------------------------------------------------------------------------------------------------
                    //---------------------------------------------------------------------
                    //response handler takes CLKGooglePlaceSearchResponse? so use
                    //if let clkPlaceSearchResponse : CLKGooglePlaceSearchResponse? =
                    //instead of
                    //if let clkPlaceSearchResponse =
                    //as this is a CLKGooglePlaceSearchResponse not CLKGooglePlaceSearchResponse? so response(clkPlaceSearchResponse) wont take it
                    if let clkPlaceSearchResponse : CLKGooglePlaceSearchResponse = Mapper<CLKGooglePlaceSearchResponse>().map(JSONObject:parsedObject){
                        //---------------------------------------------------------------------
                        self.log.debug("clkPlacesNearbySearchRequest returned")
                        //---------------------------------------------------------------------
                        //check response status
                        if let status = clkPlaceSearchResponse.status{
                            switch status{
                            case GooglePlacesResponseStatus.OK.rawValue:
                                self.log.debug("status: OK")
                                //---------------------------------------------------------------------
                                //RESPONSE OK
                                //---------------------------------------------------------------------
                                success(clkPlaceSearchResponse)
                                //---------------------------------------------------------------------
                                // TODO: - HANDLE EACH OF THESE PROPERLY
                            case GooglePlacesResponseStatus.ZERO_RESULTS.rawValue:
                                self.log.debug("status: ZERO_RESULTS")
                                return failure(NSError.appError(.googlePlacesResponseStatus_ZERO_RESULTS_PLACES))
                                //return failure(NSError.appError(.GooglePlacesResponseStatus_ZERO_RESULTS_DISTANCE))
                                
                            case GooglePlacesResponseStatus.OVER_QUERY_LIMIT.rawValue:
                                self.log.debug("status: OVER_QUERY_LIMIT")
                                return failure(NSError.appError(.googlePlacesResponseStatus_OVER_QUERY_LIMIT))

                            case GooglePlacesResponseStatus.REQUEST_DENIED.rawValue:
                                self.log.debug("status: REQUEST_DENIED")
                                return failure(NSError.appError(.googlePlacesResponseStatus_REQUEST_DENIED))
   
                            case GooglePlacesResponseStatus.INVALID_REQUEST.rawValue:

                                self.log.debug("status: INVALID_REQUEST")
                                return failure(NSError.appError(.googlePlacesResponseStatus_INVALID_REQUEST))
                                
                            default:
                                //HANDLE ALL ERRORS - if you need to handle specific ones such as NO RESULT can comment out here but probably should handle in delegate/caller
                                self.log.error("UNKNOWN status: \(status)")
                                return failure(NSError.googlePlacesAPIStatusError(status))
                            }
                            
                        }else{
                            self.log.error("clkPlaceSearchResponse.status is nil")
                            return failure(NSError.unexpectedResponseObject("clkPlaceSearchResponse.status is nil"))
                        }
            
                        //------------------------------------------------------------------------------------------------
                        
                    }else{
                        self.log.error("clkPlaceSearchResponse is nil or not CLKGooglePlaceSearchResponse")
                    }
                    //------------------------------------------------------------------------------------------------

                },
                failure:{
                    (error) -> Void in
                    //NO WS ERROR but may be status errors see success:
                    self.log.error("error:\(error)")
                    failure(error)
                }
            )
            //---------------------------------------------------------------------
        }else{
            self.log.error("urlString is nil - 16787876")
        }
    }
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - Autocomplete
    //--------------------------------------------------------------
    //https://developers.google.com/places/webservice/autocomplete
    //    Autocomplete
    //            Place Autocomplete
    //            https://maps.googleapis.com/maps/api/place/autocomplete/output?parameters
    //
    //            Query Autocomplete - "pizza near New York"
    //            https://maps.googleapis.com/maps/api/place/queryautocomplete/output?parameters
    
    func get_place_autocomplete(_ clkPlacesAutoCompleteRequest: CLKPlacesAutoCompleteRequest,
                                                          success: @escaping (_ clkPlaceAutocompleteResponse: CLKGooglePlaceAutocompleteResponse?) -> Void,
                                                          failure: @escaping (_ error: Error?) -> Void
        )
    {
        //https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-33.8670522,151.1957362&radius=500&types=food&name=cruise&key=AIzaSyCh_0tfBGDU7l_RHXuq5zol9amft7e3szg
        
        //---------------------------------------------------------------------
        //CALL THE WS
        //---------------------------------------------------------------------
        //if let urlString =  clkPlacesAutoCompleteRequest.urlString{
        if let _ =  clkPlacesAutoCompleteRequest.urlString{
            
            //NOISY self.log.debug("urlString:\r\(urlString)")
            //---------------------------------------------------------------------
            self.callWS(
                clkPlacesAutoCompleteRequest,
                
                success:{
                    (parsedObject: Any?)->Void in
                    //------------------------------------------------------------------------------------------------
                    //---------------------------------------------------------------------
                    //response handler takes CLKGooglePlaceSearchResponse? so use
                    //if let clkPlaceSearchResponse : CLKGooglePlaceSearchResponse? =
                    //instead of
                    //if let clkPlaceSearchResponse =
                    //as this is a CLKGooglePlaceSearchResponse not CLKGooglePlaceSearchResponse? so response(clkPlaceSearchResponse) wont take it

                    if let clkPlaceAutocompleteResponse : CLKGooglePlaceAutocompleteResponse = Mapper<CLKGooglePlaceAutocompleteResponse>().map(JSONObject:parsedObject){
                        //---------------------------------------------------------------------
                        self.log.debug("MAPPER clkPlacesAutoCompleteRequest returned")
                        //---------------------------------------------------------------------
                        //check response status
                        if let status = clkPlaceAutocompleteResponse.status{
                            switch status{
                            case GooglePlacesResponseStatus.OK.rawValue:
                                self.log.debug("status: OK")
                                //---------------------------------------------------------------------
                                //RESPONSE OK
                                //---------------------------------------------------------------------
                                success(clkPlaceAutocompleteResponse)
                                //---------------------------------------------------------------------
                            case GooglePlacesResponseStatus.ZERO_RESULTS.rawValue:
                                self.log.debug("status: ZERO_RESULTS")
                                return failure(NSError.appError(.googlePlacesResponseStatus_ZERO_RESULTS_PLACES))
                                //return failure(NSError.appError(.GooglePlacesResponseStatus_ZERO_RESULTS_DISTANCE))
                                
                            case GooglePlacesResponseStatus.OVER_QUERY_LIMIT.rawValue:
                                self.log.debug("status: OVER_QUERY_LIMIT")
                                return failure(NSError.appError(.googlePlacesResponseStatus_OVER_QUERY_LIMIT))
                                
                            case GooglePlacesResponseStatus.REQUEST_DENIED.rawValue:
                                self.log.debug("status: REQUEST_DENIED")
                                return failure(NSError.appError(.googlePlacesResponseStatus_REQUEST_DENIED))
                                
                            case GooglePlacesResponseStatus.INVALID_REQUEST.rawValue:
                                self.log.debug("status: INVALID_REQUEST")
                                return failure(NSError.appError(.googlePlacesResponseStatus_INVALID_REQUEST))
                                
                            default:
                                //HANDLE ALL ERRORS - if you need to handle specific ones such as NO RESULT can comment out here but probably should handle in delegate/caller
                                //"ZERO_RESULTS"
                                //"OVER_QUERY_LIMIT"
                                //"REQUEST_DENIED"  //problem with key
                                //"INVALID_REQUEST"
                                self.log.debug("status: \(status)")
                                return failure(NSError.googlePlacesAPIStatusError(status))
                            }
                            
                        }else{
                            self.log.error("clkPlaceAutocompleteResponse.status is nil")
                            return failure(NSError.unexpectedResponseObject("clkPlaceAutocompleteResponse.status is nil"))
                        }
                        
                        //------------------------------------------------------------------------------------------------
                        
                    }else{
                        self.log.error("clkPlaceAutocompleteResponse is nil or not CLKGooglePlaceAutocompleteResponse")
                    }
                    //---------------------------------------------------------------------
                },
                failure:{
                    (error) -> Void in
                    //NO WS ERROR but may be status errors see success:
                    self.log.error("error:\(error)")
                    failure(error)
                }
            )
            //---------------------------------------------------------------------
        }else{
            self.log.error("urlString is nil - 16787876")
        }
    }
    //            Query Autocomplete - "pizza near New York"
    //            https://maps.googleapis.com/maps/api/place/queryautocomplete/output?parameters
    
    func get_query_autocomplete(_ clkPlacesQueryAutoCompleteRequest: CLKPlacesQueryAutoCompleteRequest,
        success: @escaping (_ clkPlaceAutocompleteResponse: CLKGooglePlaceAutocompleteResponse?) -> Void,
        failure: @escaping (_ error: Error?) -> Void
        )
    {
        //https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-33.8670522,151.1957362&radius=500&types=food&name=cruise&key=AIzaSyCh_0tfBGDU7l_RHXuq5zol9amft7e3szg
        
        //---------------------------------------------------------------------
        //CALL THE WS
        //---------------------------------------------------------------------
        //if let urlString =  clkPlacesQueryAutoCompleteRequest.urlString{
        if let _ =  clkPlacesQueryAutoCompleteRequest.urlString{
            
            //NOISY self.log.debug("urlString:\r\(urlString)")
            //---------------------------------------------------------------------
            self.callWS(
                clkPlacesQueryAutoCompleteRequest,
                
                success:{
                    (parsedObject: Any?)->Void in
                    //------------------------------------------------------------------------------------------------
                    //---------------------------------------------------------------------
                    //response handler takes CLKGooglePlaceSearchResponse? so use
                    //if let clkPlaceSearchResponse : CLKGooglePlaceSearchResponse? =
                    //instead of
                    //if let clkPlaceSearchResponse =
                    //as this is a CLKGooglePlaceSearchResponse not CLKGooglePlaceSearchResponse? so response(clkPlaceSearchResponse) wont take it
                    
                    if let clkPlaceAutocompleteResponse : CLKGooglePlaceAutocompleteResponse = Mapper<CLKGooglePlaceAutocompleteResponse>().map(JSONObject:parsedObject){
                        //---------------------------------------------------------------------
                        self.log.debug("clkPlacesAutoCompleteRequest returned 4345")
                        //---------------------------------------------------------------------
                        //check response status
                        if let status = clkPlaceAutocompleteResponse.status{
                            switch status{
                            case GooglePlacesResponseStatus.OK.rawValue:
                                self.log.debug("status: OK")
                                //---------------------------------------------------------------------
                                //RESPONSE OK
                                //---------------------------------------------------------------------
                                success(clkPlaceAutocompleteResponse)
                                //---------------------------------------------------------------------
                            case GooglePlacesResponseStatus.ZERO_RESULTS.rawValue:
                                self.log.debug("status: ZERO_RESULTS")
                                return failure(NSError.appError(.googlePlacesResponseStatus_ZERO_RESULTS_PLACES))
                                //return failure(NSError.appError(.GooglePlacesResponseStatus_ZERO_RESULTS_DISTANCE))
                                
                            case GooglePlacesResponseStatus.OVER_QUERY_LIMIT.rawValue:
                                self.log.debug("status: OVER_QUERY_LIMIT")
                                return failure(NSError.appError(.googlePlacesResponseStatus_OVER_QUERY_LIMIT))
                                
                            case GooglePlacesResponseStatus.REQUEST_DENIED.rawValue:
                                self.log.debug("status: REQUEST_DENIED")
                                return failure(NSError.appError(.googlePlacesResponseStatus_REQUEST_DENIED))
                                
                            case GooglePlacesResponseStatus.INVALID_REQUEST.rawValue:
                                
                                self.log.debug("status: INVALID_REQUEST")
                                return failure(NSError.appError(.googlePlacesResponseStatus_INVALID_REQUEST))
                            default:
                                //HANDLE ALL ERRORS - if you need to handle specific ones such as NO RESULT can comment out here but probably should handle in delegate/caller
                                //"ZERO_RESULTS"
                                //"OVER_QUERY_LIMIT"
                                //"REQUEST_DENIED"  //problem with key
                                //"INVALID_REQUEST"
                                self.log.debug("status: \(status)")
                                return failure(NSError.googlePlacesAPIStatusError(status))
                            }
                            
                        }else{
                            self.log.error("clkPlaceAutocompleteResponse.status is nil")
                            return failure(NSError.unexpectedResponseObject("clkPlaceAutocompleteResponse.status is nil"))
                        }
                        
                        //------------------------------------------------------------------------------------------------
                        
                    }else{
                        self.log.error("clkPlaceAutocompleteResponse is nil or not CLKGooglePlaceAutocompleteResponse")
                    }
                    //---------------------------------------------------------------------
                },
                failure:{
                    (error) -> Void in
                    //NO WS ERROR but may be status errors see success:
                    self.log.error("error:\(error)")
                    failure(error)
                }
            )
            //---------------------------------------------------------------------
        }else{
            self.log.error("urlString is nil - 16787876")
        }
    }
}
