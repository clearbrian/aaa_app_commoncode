//
//  CoreLocatable.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 17/01/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
import CoreLocation

protocol CoreLocatable{
    var clLocation: CLLocation? {get}
    
    func distanceTo(otherLocation: CLLocation) -> CLLocationDistance
    func distanceToCurrentLocation() -> CLLocationDistance
    
    //var distanceToCurrentLocationFormatted: String
    //class func sortByNearest(arrayUnsorted:[TFLApiPlace]) -> [TFLApiPlace]
    //class func sortByNearest(arrayUnsorted:[CoreLocatable]) -> [CoreLocatable]
}
