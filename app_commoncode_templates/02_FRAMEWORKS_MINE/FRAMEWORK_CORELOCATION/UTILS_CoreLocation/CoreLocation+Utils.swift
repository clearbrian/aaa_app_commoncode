//
//  CoreLocation+Utils.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 17/01/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
import CoreLocation



extension CLLocation{

    
    //--------------------------------------------------------------
    // MARK: - Init with possible optionals
    // MARK: -
    //--------------------------------------------------------------
    class func locationWith(latFloat : Float?, lonFloat : Float?) -> CLLocation?{
        var locationReturned: CLLocation? = nil
        
        if let latFloat = latFloat {
            if let lonFloat = lonFloat {
                
                let latDouble = Double(latFloat)
                let lonDouble = Double(lonFloat)
                
                locationReturned = CLLocation.init(latitude: latDouble, longitude: lonDouble)
                
            }else{
                appDelegate.log.error("self.lon is nil")
            }
        }else{
            appDelegate.log.error("self.lat is nil")
        }
        return locationReturned
    }
    
    class func locationWith(latDouble : Double?, lonDouble : Double?) -> CLLocation?{
        var locationReturned: CLLocation? = nil
        
        if let latDouble = latDouble {
            if let lonDouble = lonDouble {
                
                locationReturned = CLLocation.init(latitude: latDouble, longitude: lonDouble)
                
            }else{
                appDelegate.log.error("lonDouble is nil")
            }
        }else{
            appDelegate.log.error("latDouble is nil")
        }
        return locationReturned
    }
    
    
    //--------------------------------------------------------------
    // MARK: - validCLLocation
    // MARK: -
    //--------------------------------------------------------------

    var isValid: Bool{
        var locationIsValid = false
        if self.coordinate.latitude == 0.0{
            if self.coordinate.longitude == 0.0{
                //technically correct but probably not the correct current location if theyre not on the equater at 0.0
                MyXCodeEmojiLogger.defaultInstance.error("location is 0.0,0.0")
                locationIsValid = false
            }else{
                //ok
                locationIsValid = true
            }
        }else{
            //ok
            locationIsValid = true
        }
        return locationIsValid
    }
    
    
    //--------------------------------------------------------------
    // MARK: - Format for display - 2 decimal places
    // MARK: -
    //--------------------------------------------------------------
    
    //51.87638745638746, 0.34873628 >> "51.87, FormattedLatLng.swift"
    func formattedLatLng() -> String{
        
        let latString = COLCFormatter.formatToTwoDecimalPlaces(doubleValue: self.coordinate.latitude)
        let lngString = COLCFormatter.formatToTwoDecimalPlaces(doubleValue: self.coordinate.longitude)
        
        //clLocation.coordinate.latitude, clLocation.coordinate.longitude
        return "\(latString),\(lngString)"
        
    }
    
   
}
