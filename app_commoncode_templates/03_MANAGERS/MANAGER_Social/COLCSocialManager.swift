//
//  COLCSocialManager.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 24/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
import Social

class COLCSocialManager{
    
    var viewController: UIViewController?
    
    //--------------------------------------------------------------
    // MARK: - UIActivityViewController
    // MARK: -
    //--------------------------------------------------------------

    //COLCSocialManager.showShareActivityViewController(fromVC: aViewController)
    class func showShareActivityViewController(fromVC vc: UIViewController){
        
        var activityItems = [Any]()
        
        //----------------------------------------------------------------------------------------
        //TEXT
        //----------------------------------------------------------------------------------------
        //dont share link IN text FB app will only show link expanded - no text
        //let textToShare = "TAXI RANKS LONDON. New iOS app for tourists and cabbies. Check it out its free. http://buff.ly/2kNMhyU"
        //let textToShare = "TAXI RANKS LONDON. New iOS app for tourists and cabbies. Check it out its free."
       //----------------------------------------------------------------------------------------
        //OK - note fb doesnt let you share text will only show link - but 
        //Fb messenger displays the image weridly
        //let textToShare = "TAXI RANKS LONDON. New iOS app for tourists and cabbies. Check it out its free. http://bit.ly/taxirankslondon"
        //----------------------------------------------------------------------------------------
        
        activityItems.append(AppConfig.COLCSocialManager_textToShare)
        
        //----------------------------------------------------------------------------------------
        //URL - just put the url in the text fb will only show link
        //----------------------------------------------------------------------------------------
        //dont share url string directly - FB only shows that link
        
        // //let linkToShare = "http://buff.ly/2kNMhyU"
        // let linkToShare = "http://bit.ly/taxirankslondon"
        // 
        // if let linkToShareURL = NSURL(string: linkToShare) {
        //     activityItems.append(linkToShareURL)
        // }else{
        //     appDelegate.log.error("linkToShare is nil")
        // }
        
        //----------------------------------------------------------------------------------------
        //IMAGE
        //----------------------------------------------------------------------------------------
        let imageShareIconName =  "SHARE_Icon-60"
        
        if let imageShareIcon = UIImage.init(named:imageShareIconName) {
             activityItems.append(imageShareIcon)
        }else{
            appDelegate.log.error("imageShareIconName is nil:\(imageShareIconName)")
        }
        
        //----------------------------------------------------------------------------------------
        //SHARE
        //----------------------------------------------------------------------------------------

        if activityItems.count > 0{
            let activityViewController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
            
            activityViewController.popoverPresentationController?.sourceView = vc.view // so that iPads won't crash
            
            // exclude some activity types from the list (optional)
            //activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            
            let buttonTintBackup = UIButton.appearance().tintColor
            
            UIButton.appearance().tintColor = UIColor.red
            
            // present the view controller
            vc.present(activityViewController, animated: true) {
                UIButton.appearance().tintColor = buttonTintBackup
            }
        }else{
            appDelegate.log.error("activityItems.count > 0{")
        }
       
    }
    
    
    //--------------------------------------------------------------
    // MARK: - Social.framework - use UIActivityViewController instead
    // MARK: -
    //--------------------------------------------------------------

    //Social.framework - dont use only 4 - use showShareActivityViewController
    class func showShareActionSheet(vc: UIViewController){
       var arrayUIAlertActions = [UIAlertAction]()
        
        //----------------------------------------------------------------------------------------
        //Twitter
        //----------------------------------------------------------------------------------------
        if let alertAction = COLCSocialManager.addActionForServiceType(SLServiceTypeTwitter, slServiceTypeName:"Twitter",  viewController: vc){
            arrayUIAlertActions.append(alertAction)
            
        }else{
            appDelegate.log.error("addActionForServiceType(SLServiceTypeTwitter) is nil")
        }
        //----------------------------------------------------------------------------------------
        //SLServiceTypeFacebook
        //----------------------------------------------------------------------------------------
        if let alertAction = COLCSocialManager.addActionForServiceType(SLServiceTypeFacebook, slServiceTypeName:"Facebook",  viewController: vc){
            arrayUIAlertActions.append(alertAction)
            
        }else{
            appDelegate.log.error("addActionForServiceType(SLServiceTypeFacebook) is nil")
        }
        //----------------------------------------------------------------------------------------
        //SLServiceTypeSinaWeibo
        //----------------------------------------------------------------------------------------
        if let alertAction = COLCSocialManager.addActionForServiceType(SLServiceTypeSinaWeibo, slServiceTypeName:"Sina Weibo",  viewController: vc){
            arrayUIAlertActions.append(alertAction)
            
        }else{
            appDelegate.log.error("addActionForServiceType(SLServiceTypeSinaWeibo) is nil")
        }
        //----------------------------------------------------------------------------------------
        //SLServiceTypeTencentWeibo
        //----------------------------------------------------------------------------------------
        if let alertAction = COLCSocialManager.addActionForServiceType(SLServiceTypeTencentWeibo, slServiceTypeName:"Tencent Weibo", viewController: vc){
            arrayUIAlertActions.append(alertAction)
            
        }else{
            appDelegate.log.error("addActionForServiceType(SLServiceTypeTencentWeibo) is nil")
        }
     
        if arrayUIAlertActions.count > 0{
            CLKAlertController.showSheetWithActions(vc: vc,
                                                    title: "Share",
                                                    message: "Share on social networks",
                                                    alertActions: arrayUIAlertActions)
        }else{
            appDelegate.log.error("arrayUIAlertActions.count == 0")
        }
    }

    class func addActionForServiceType(_ slServiceType: String, slServiceTypeName: String, viewController: UIViewController) -> UIAlertAction?{
        
        let alertActionServiceType = UIAlertAction(title: slServiceType, style: UIAlertActionStyle.default) { (alertAction) -> Void in
            //----------------------------------------------------------------------------------------
            //TAP ON ALERT ITEM - OPENS SHARE popup
            //----------------------------------------------------------------------------------------
            if SLComposeViewController.isAvailable(forServiceType: slServiceType) {
                
                if let slComposeViewController  = SLComposeViewController(forServiceType: slServiceType){
                    slComposeViewController.setInitialText("TEXT TO POST")
                    
                    viewController.present(slComposeViewController, animated: true, completion: nil)
                }else{
                    appDelegate.log.error("alertActionForSLServiceTypeFacebook is nil")
                }
                
                
            } else {
                
                CLKAlertController.showAlertInVC(viewController, title: "Not logged into \(slServiceTypeName)", message: "Please log into \(slServiceTypeName) in iOS Settings")
            }
            //----------------------------------------------------------------------------------------
        }
        
        return alertActionServiceType
    
    }
}
