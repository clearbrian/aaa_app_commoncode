//
//  SettingsTableViewController
//  joyride
//
//  Created by Brian Clear on 19/05/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit
import SafariServices

//static tablevc
class SettingsTableViewController: ParentTableViewController, SFSafariViewControllerDelegate  {
    
    let emailManager = EmailManager()
    
    @IBOutlet weak var labelVersion: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.labelVersion?.text = AppVersionManager.appVersionString()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch (indexPath as NSIndexPath).section{
        case 0:
            //---------------------------------------------------------
            switch (indexPath as NSIndexPath).row{
            case 0:
                //----------------------------------------------------------------------------------------
                tableView.deselectRow(at: indexPath, animated: true)
                self.emailManager.sendEmailFromVC(self)
                //----------------------------------------------------------------------------------------
            case 1:
                //----------------------------------------------------------------------------------------
                //TWITTER
                tableView.deselectRow(at: indexPath, animated: true)
                self.openTwitterInSafari()
                //----------------------------------------------------------------------------------------
            case 2:
                //----------------------------------------------------------------------------------------
                //RATE US
                tableView.deselectRow(at: indexPath, animated: true)
                //self.openAppIniTunesInSafari()
                //self.openAppInITunes()
                self.openReviewManagerOrFallback()
                
                //----------------------------------------------------------------------------------------
            case 3:
                //SHARE
                tableView.deselectRow(at: indexPath, animated: true)
                COLCSocialManager.showShareActivityViewController(fromVC: self)
                //----------------------------------------------------------------------------------------
            default:
                logger.error("UNHANDLED didSelectRowAtIndexPath \((indexPath as NSIndexPath).row)")
            }
            //---------------------------------------------------------
        case 1:
            switch (indexPath as NSIndexPath).row{
            case 0:
                //PRIVACY POLICY
                self.openPrivacyPolicyInSafari()
            default:
                logger.error("UNHANDLED didSelectRowAtIndexPath \((indexPath as NSIndexPath).row)")
            }
        case 2:
            switch (indexPath as NSIndexPath).row{
            case 0:
                print("Libraries Uses: TFL")
                openTFLInSafari()
            case 1:
                print("Libraries Uses: Google Places")
            case 2:
                print("Libraries Uses: Google Maps")
            case 3:
                print("Libraries Uses: Google Analytics")
                openGoogleAnalyticsInSafari()
                
            case 4:
                print("Libraries Uses: Flurry")
                openFlurryLicenseInSafari()
                
                //            case 3:
                //                print("Libraries Uses: RealmDb")
            //                openRealmDBInSafari()
            default:
                logger.error("UNHANDLED didSelectRowAtIndexPath \((indexPath as NSIndexPath).row)")
            }
        case 3:
            
            switch (indexPath as NSIndexPath).row{
            case 0:
                //version
                print("user tapped on version")
                
            default:
                logger.error("UNHANDLED didSelectRowAtIndexPath \((indexPath as NSIndexPath).row)")
            }
        default:
            logger.error("UNHANDLED didSelectRowAtIndexPath \((indexPath as NSIndexPath).section)")
        }
        
    }
    
    func openTwitterInSafari(){
        let urlString = "https://twitter.com/colcltd"
        self.openURLStringInSafari(urlString)
        
    }
    
    func openPrivacyPolicyInSafari(){
        let urlString = "http://www.cityoflondonconsulting.com/privacypolicy.html"
        self.openURLStringInSafari(urlString)
        
    }
    
    func openTFLInSafari(){
        let urlString = "https://tfl.gov.uk/corporate/terms-and-conditions/transport-data-service"
        
        self.openURLStringInSafari(urlString)
        
    }
    
    func openGoogleAnalyticsInSafari(){
        //let urlString = "https://www.google.com/analytics/#/mobile-apps?modal_active=none"
        let urlString = "https://cocoapods.org/pods/GoogleAnalytics"
        
        self.openURLStringInSafari(urlString)
        
    }
    
    func openRealmDBInSafari(){
        let urlString = "https://www.realm.io"
        self.openURLStringInSafari(urlString)
        
    }
    
    func openFlurryLicenseInSafari(){
        let urlString = "https://developer.yahoo.com/analytics/"
        self.openURLStringInSafari(urlString)
        
    }
    
    //--------------------------------------------------------------
    // MARK: - ITUNES
    // MARK: -
    //--------------------------------------------------------------
    let iTunesURLString    = "https://geo.itunes.apple.com/us/app/id1197382484?mt=8"
    let iTunesAppURLString = "itms://geo.itunes.apple.com/us/app/id1197382484?mt=8"
    
    func openAppIniTunesInSafari(){
        //Will ask user if they want to redirect to iTunes
        
        self.openURLStringInSafari(iTunesURLString)
        
    }
    func openAppInITunes(){
        
        //add to LSApplicationQueriesSchemes
        //else you get The specified URL has an unsupported scheme. Only HTTP and HTTPS URLs are supported - WRONG THIS WAS CAUSED BY SPACE in urlString
        if let appSchemeURL = URL(string:"itms-apps://"){
            
            if (UIApplication.shared.canOpenURL(appSchemeURL))
            {
                //https://itunes.apple.com/gb/app/seanet/id1084735244?mt=8
                //OK
                //let urlString = "itms://geo.itunes.apple.com/us/app/lyft-taxi-app-alternative/id529379082?mt=8"
                //Ok just iTunes Id
                //oklet urlString = "itms://geo.itunes.apple.com/us/app/id529379082?mt=8"
                
                //OK http://phobos.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=529379082
                //OK    https://itunes.apple.com/app/lyft-taxi-app-alternative/id529379082?mt=8
                //OK http://phobos.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=529379082
                //NO itms-apps://phobos.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=529379082
                
                
                //https://affiliate.itunes.apple.com/resources/documentation/itunes-store-web-service-search-api/
                //let urlString = "itms://geo.itunes.apple.com/us/app/id1111503668?mt=8"
                
                
                logger.debug("OPENING urlString:\(iTunesAppURLString)")
                if let appURL = URL(string:iTunesAppURLString) {
                    UIApplication.shared.openURL(appURL)
                }else{
                    logger.error("urlHailoApp is nil - iTunesAppURLString not valid:[\(iTunesAppURLString)]")
                }
            }else{
                logger.error("canOpenURL 'itms://' FAILED")
            }
        }else{
            logger.error("'itms://' url is nil")
        }
    }
    
    /*
     10.3 - 
     https://developer.apple.com/reference/storekit/skstorereviewcontroller/2851536-requestreview
     if app in dev mode Submit button wont be enabled
     */
    func openReviewManagerOrFallback(){
        /* Notes from Apple on behavior of iOS 10.3's prompt:
         1. On debug builds the iOS 10.3 prompt always shows
         2. It won't show on TestFlight builds. (Should cause App Store fallback every time)
         3. There are rules as to when it will be shown to App Store users, and it can be disabled globally by a user (hence the need for fallback).
         
         Note from developer:
         1. Please don't use this to auto-prompt / send people to the App Store. It's terribly user hostile. This was developed for if you need a "rate app" button in your app, and where the button not doing anything looks like a glitch.
         */
        
        // Obviously this demo app isn't on the App Store, so we'll use the Apple Store app as the App Store link fallback for example purposes.
        ReviewManager.shared.promptForReview(appId: "\(AppConfig.AppStore_appid)")
    }
    
    
    
    func openURLStringInSafari(_ urlString: String){
        
        let urlwithPercentEscapes = urlString.addingPercentEncoding( withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        if let urlStringwithPercentEscapes = urlwithPercentEscapes{
            if let url = URL(string: urlStringwithPercentEscapes) {
                
                //-----------------------------------------------------------------------------------
                //SafariController button use app tint but has white border so can be hard to see
                // AppearanceManager.appColorNavbarAndTabBar
                // AppearanceManager.appColorTint
                //appDelegate.window?.tintColor = UIColor.redColor()
                
                //appDelegate.window?.tintColor = AppearanceManager.appColorNavbarAndTabBar
                //... change back in  afariViewController: safariViewControllerDidFini
                //-----------------------------------------------------------------------------------
                let safariViewController = SFSafariViewController(url: url)
                
                safariViewController.delegate = self
                self.present(safariViewController, animated: true, completion: nil)
                appDelegate.window?.tintColor = AppearanceManager.appColorNavbarAndTabBar
            }else{
                logger.error("url is nil for urlString:\r\(urlString)\r\(urlStringwithPercentEscapes)")
            }
        }else{
            logger.error("urlwithPercentEscapes is nil")
        }
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController){
        logger.debug("safariViewControllerDidFinish")
    }
    
    
}
