//
//  COLCPlacePickerViewController.swift
//  joyride
//
//  Created by Brian Clear on 24/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import GoogleMaps
import CoreLocation


//--------------------------------------------------------------
// MARK: - func to sort results
// MARK: -
//--------------------------------------------------------------
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}




enum PlaceResultsSort{
    case sortByGoogleWSResults          // Sort in order returned from WS
    case sortByNearest      // As you move map results are sorted/ numbered so nearest always at the top
    case sortByName                     // Sort by Name of business - user may know name but not exact location
}



//--------------------------------------------------------------
// MARK: - COLCPlacePickerViewControllerDelegate
// MARK: -
//--------------------------------------------------------------

protocol COLCPlacePickerViewControllerDelegate {
    
    func colcPlacePickerViewController_clkPlacesPickerResult(_ colcPlacePickerViewController: COLCPlacePickerViewController, clkPlacesPickerResult: CLKPlacesPickerResult)
    
    func colcPlacePickerViewControllerCancelled(_ colcPlacePickerViewController: COLCPlacePickerViewController)
    
}


//--------------------------------------------------------------
// MARK: -
// MARK: - IMPLEMENTATION
// MARK: -
//--------------------------------------------------------------

class COLCPlacePickerViewController: ParentViewController, GMSMapViewDelegate, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    
    var delegate: COLCPlacePickerViewControllerDelegate?
    //beware ParentViewController also has  Apple @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var mapView: GMSMapView!
    
    //delegate may send current position and map jumps back
    var userDidChangeCameraPosition : Bool = false
    
    //------------------------------------------------------------------------------------------------
    //flags to prevent multiple searches
    var nearbySearchRunning : Bool = false
    var placeAutoCompleteRunning : Bool = false
    var queryAutoCompleteRunning : Bool = false
    //------------------------------------------------------------------------------------------------

    var mapIsIdleTimer : Timer?
    var timerTypeAndSearch: Timer?
    
    
    var gmsCircle : GMSCircle?
    
    //The results as returned by WS
    var nearbySearch_CLKGooglePlaceResultsArrayUnSorted: [CLKGooglePlaceResult]?
    
    //The results to be shown on map/table with various sorts applied
    var nearbySearch_CLKGooglePlaceResultsArray: [CLKGooglePlaceResult]?
    
    var placeAutoComplete_CLKGooglePlaceAutocompletePredictionArray: [CLKGooglePlaceAutocompletePrediction]?
    var queryAutoComplete_CLKGooglePlaceAutocompletePredictionArray: [CLKGooglePlaceAutocompletePrediction]?

    
    
    @IBOutlet weak var viewWrapperSearchField: UIView!
    
    @IBOutlet weak var tableViewSearch: UITableView!
    @IBOutlet weak var tableViewPlacesResults: UITableView!
    
    @IBOutlet weak var textFieldAddSearch: UITextField!
    
    @IBOutlet weak var viewAddNewSearchResults: UIView!
    @IBOutlet weak var viewAddNewSearchResults_BottomConstraint: NSLayoutConstraint!
    
    //will be same as appDelegate.currentLocation unless user moves map

    var currentMapLocation: CLLocation?
    //to stop ws called too much get dist in between
    var prevMapLocation: CLLocation?
    //default : show result sorted by distance to center of the map
    var currentPlaceResultsSort : PlaceResultsSort = .sortByNearest
    
    var mapMovedOkToCallWS = true

    //--------------------------------------------------------------
    // MARK: - OUTLETS
    // MARK: -
    //--------------------------------------------------------------

    @IBOutlet weak var nsLayoutConstraint_Wrapper_Search_textField_height: NSLayoutConstraint!
    

    @IBOutlet weak var nsLayoutConstraint_Wrapper_TextAddress_height: NSLayoutConstraint!
    
    
    @IBOutlet weak var sonarView: SonarView!
    
    @IBOutlet weak var labelNearestAddress: UILabel!
    
    //the dot beside nearest street address
    @IBOutlet weak var imageViewNearestResult: UIImageView!
    
    //------------------------------------------------------------------------------------------------
    //clkPlaceSelected
    //------------------------------------------------------------------------------------------------
    //THE SELECTED ITEM RETURNED FROM THIS SCREEN - nil denotes not picked
    //beware this VC is now embedded so may copy the reference and sets START/END to same point
    //optional because we want to be able to nil it to show currentLocation
    
    //dont defautl may be set from outside to EDIT a CLKPLace
    //var clkPlaceSelected : CLKPlace? = CLKPlace()
    //-----------------------------------------------------------------------------------
    
    // TODO: - needed?
    
    //user can tap on START/EDIT and it will edit
    //passed in from TripPlannerVc > LocationPicker >> COLCCarbonTabSwipeNavigationViewControllerInputs >> colcPlacePickerViewController
//    var clkPlaceSelected: CLKPlace?{
//        willSet {
//            print("[clkPlaceSelected]3 willSet: Current value: '\(clkPlaceSelected)', New value: '\(newValue)'")
//        }
//        //Finally this
//        didSet {
//            //print("[clkPlaceSelected]4 didSet: Old value: '\(oldValue)', clkPlaceSelected['\(clkPlaceSelected?.name_formatted_address)']")
//            moveMapToCLKPlaceSelected()
//        }
//    }
    
    var clkGooglePlaceSelected: CLKGooglePlace?{
        willSet {
            print("[clkGooglePlaceSelected willSet: Current value: '\(String(describing: clkGooglePlaceSelected))', New value: '\(String(describing: newValue))'")
        }
        //Finally this
        didSet {
            //print("[clkPlaceSelected]4 didSet: Old value: '\(oldValue)', clkPlaceSelected['\(clkPlaceSelected?.name_formatted_address)']")
            moveMapToCLKPlaceSelected()
        }
    }
    
    

    
    //if nil then just show current location
    func showPlacesPickerWithCurrentLocation(){
        self.showPlacesPicker(nil)
    }
    
    func showPlacesPicker(_ clkPlaceToShow : CLKPlace?){
        //ok if nil
        // TODO: - needed?
logger.error("ERROR showPlacesPicker todo")
//        //but screen is reused  - make sure that when you get the start and end youre not using the same clkPlaceSelected reference value else START/END on Trip VC point to same thing
//        if let clkPlaceToShow = clkPlaceToShow {
//           self.clkPlaceSelected = clkPlaceToShow
//        }else{
//            //dont leave it at nil
//            self.clkPlaceSelected = CLKPlace()
//        }
    }
    
    
    //------------------------------------------------------------------------------------------------
    // MARK: -
    // MARK: - IMPLEMENTATION
    // MARK: -
    //------------------------------------------------------------------------------------------------
    override func viewDidLoad() {
        
        super.viewDidLoad()
        //------------------------------------------------------------------------------------------------
        //logger.debug("========== COLCPlacePickerViewController: viewDidLoad()")
        //------------------------------------------------------------------------------------------------

        self.labelNearestAddress.applyCustomFontForCurrentTextStyle()
         self.labelNearestAddress.textColor = UIColor.purple
        //------------------------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------
        //DYNAMIC TEXT and TABLE ROW HEIGHT
        //-----------------------------------------------------------------------------------
        self.tableViewPlacesResults.estimatedRowHeight = 80.0
        //also done in heightForRowAtIndexPath
        self.tableViewPlacesResults.rowHeight = UITableView.automaticDimension;
        
        //self.tableView.sectionHeaderHeight = UITableView.automaticDimension;
        //self.tableView.estimatedSectionHeaderHeight = 80.0f;
        //------------------------------------------------------------------------------------------------
        
        // TODO: - EMBEDDED IN CONTAINER HIDE SEARCH AND USE buttom
        //BROKEN        self.nsLayoutConstraint_Wrapper_Search_textField_height.constant = 0.0;
        //BROKEN        self.nsLayoutConstraint_Wrapper_TextAddress_height.constant = 0.0;

        //------------------------------------------------------------------------------------------------
        self.viewAddNewSearchResults.isHidden = true
        //---------------------------------------------------------------------
        textFieldAddSearch.delegate = self
        
        //textFieldAddSearch.addTarget(self, action: "textFieldDidChange:", forControlEvents:.ValueChanged)
         subscribeToKeyboardNotifications()
        //---------------------------------------------------------------------
       
        
        //------------------------------------------------------------------------------------------------
        //test_gmsPlacesClient_autocompleteQuery()
        //---------------------------------------------------------------------
        //to get current location updates
        appDelegate.colcPlacePickerViewController = self
        //---------------------------------------------------------------------
        //MAPVIEW
        //---------------------------------------------------------------------
        self.mapView.delegate = self
        self.mapView.isMyLocationEnabled = true
        self.mapView.isBuildingsEnabled = true
        self.mapView.isTrafficEnabled = false
        
        self.mapView.settings.compassButton = true;
        self.mapView.settings.myLocationButton = true;
        self.mapView.settings.indoorPicker = true;
        
        //needed for when user taps My location button twice it rotate in direction they are facing
        self.mapView.settings.rotateGestures = true;
        
        
        // TODO: - needed?
        //-----------------------------------------------------------------------------------
        //        if let _ = appDelegate.currentLocation{
        //            moveToCurrentLocation()
        //            //move will call geocode
        ////            if let _ = appDelegate.currentLocation_googleGeocodedGMSAddress{
        ////                logger.error("appDelegate.currentLocation_googleGeocodedGMSAddress is set already")
        ////            }else{
        ////                logger.error("appDelegate.currentLocation.currentLocation_googleGeocodedGMSAddress is not set yet - call geocodeLocation()")
        ////                self.geocodeLocation(currentLocation)
        ////            }
        //            
        //        }else{
        //            logger.error("appDelegate.currentLocation is not set yet")
        //        }
        //-----------------------------------------------------------------------------------
//        logger.debug("LOC: [viewDidLoad:] about to call  moveToCurrentLocation()")
//        
//        moveToCurrentLocation()
        //-----------------------------------------------------------------------------------
        //APPEARANCE
        //-----------------------------------------------------------------------------------
        
        self.view.backgroundColor = AppearanceManager.appColorNavbarAndTabBar
        
        //self.viewWrapperSearchField.backgroundColor = UIToolbar.appearance().backgroundColor
        self.viewWrapperSearchField.backgroundColor = AppearanceManager.appColorDark1
       
        //-----------------------------------------------------------------------------------
        //        //self.buttonSearch.tintColor = AppearanceManager.appColorTint
        //        self.buttonSearch.setTitleColor(AppearanceManager.appColorTint, forState: .Normal)
        //        
        //        self.buttonSearch.applyCustomFontForCurrentTextStyle()
        //        
        //        
        //        self.buttonCancel.setTitleColor(AppearanceManager.appColorTint, forState: .Normal)
        //        
        //        self.buttonCancel.applyCustomFontForCurrentTextStyle()
        //-----------------------------------------------------------------------------------
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
         self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        //req here and not internaly as center point not set so animation can appear at position set in IB i think

        self.sonarView.startAnimation()
        
        moveToCurrentLocation()
    }
    
    //when we edit the TO/FROW the didSet may be called AFTER viewWillAppear
    //so call in setter clkPlaceSelected.didSet
    func moveMapToCLKPlaceSelected(){
        if let clkGooglePlaceSelected = self.clkGooglePlaceSelected {
            if let clLocation = clkGooglePlaceSelected.clLocation {
                //if device gets current location - dont move map
                self.userDidChangeCameraPosition = true
                self.moveMapToCLLocation(clLocation)
                
            }else{
                logger.error("clkPlaceSelected.clLocation is nil")
            }
        }else{
            logger.error("self.clkPlaceSelected is nil")
        }
    }
    
    
    //caused weird userinfo mapping crash
    //    override func viewDidAppear(animated: Bool) {
    //        super.viewDidAppear(animated)
    //        // Do any additional setup after loading the view, typically from a nib.
    //
    //    }
    
    

    //--------------------------------------------------------------
    // MARK: -
    // MARK: - UITextFieldDelegate
    //--------------------------------------------------------------
    //To trigger event when user types in fields
    //right click in IB and choose EditingChanged >> textField_EditingChanged
    
    @IBAction func textField_EditingChanged(_ textField: UITextField) {
        //if more than one search
        if(textField == self.textFieldAddSearch){
            
            //logger.error("self.textFieldAddSearch: '\(self.textFieldAddSearch.text)'")
            
            if textField.text == ""{
                clearResultsAndTablePREDICTIONS()
            }else{
                if let timerTypeAndSearch = self.timerTypeAndSearch{
                    //wvery time you type a letter - reset the time - by deleteing the old one and start new one
                    timerTypeAndSearch.invalidate()
                }else{
                    //first time - only need to create timer
                }
                
                createTimer(textField.text!)
            }
        }else{
            logger.error("textFieldDidBeginEditing: unhandled textfield")
        }
    }
    
    let kuserInfo_SearchText = "searchText"
    func createTimer(_ searchText: String){
        self.timerTypeAndSearch = Timer.scheduledTimer(timeInterval: 0.5,
                                                                         target: self,
                                                                         selector: #selector(COLCPlacePickerViewController.timerTypeAndSearch_fired(_:)),
                                                                         userInfo: [kuserInfo_SearchText :searchText],
                                                                         repeats: false)
    }
    //user has stoped typing in search box for long enough time to say they wish to search
    @objc func timerTypeAndSearch_fired(_ timer:Timer) {
        
        if let userInfo = timer.userInfo as? Dictionary<String, AnyObject>{
            if let textField_text:String = userInfo[kuserInfo_SearchText] as? String{
                
                //timer.invalidate()
                callJSONWebservices_placeAutoComplete(textField_text)
                
            }else{
                logger.error("userInfo is nil")
            }
        }else{
            logger.error("userInfo is nil")
        }
    }
    
    //when user deletes all text in search field table should be cleared
    func clearResultsAndTablePREDICTIONS(){
        
        self.placeAutoComplete_CLKGooglePlaceAutocompletePredictionArray = [CLKGooglePlaceAutocompletePrediction]()
        self.tableViewSearch.reloadData()
    }
    
    //--------------------------------------------------------------
    // MARK: - UITextField
    // MARK: -
    //--------------------------------------------------------------

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool{
        //logger.debug("textFieldShouldBeginEditing")
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        //logger.debug("textFieldDidBeginEditing")
        
        //USER TAPPED IN SEARCH FIELD
        //        self.fadeInSearch()
    }
    //---------------------------------------------------------------------
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool{
        //logger.debug("textFieldShouldEndEditing")
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        //logger.debug("textFieldDidEndEditing")
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        //logger.debug("shouldChangeCharactersInRange")
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool{
        //logger.debug("textFieldShouldClear")
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField)-> Bool{
        //self.closeSearchHideKeyboard()
        //triggers keyboardWillHide: which also fades out view
        self.textFieldAddSearch.resignFirstResponder()
        return false
    }
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - KEYBORAD
    //--------------------------------------------------------------
    fileprivate func subscribeToKeyboardNotifications() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(COLCPlacePickerViewController.keyboardWillShow(_:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(COLCPlacePickerViewController.keyboardWillHide(_:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
    }
    
    fileprivate func unsubscribeFromKeyboardNotifications() {
        
        NotificationCenter.default.removeObserver(self,
                                                  name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self,
                                                  name: UIResponder.keyboardWillHideNotification,
                                                  object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        
        if let notification_userInfo = (notification as NSNotification).userInfo {

            //---------------------------------------------------------------------
            //KEYBOARD HEIGHT
            //---------------------------------------------------------------------
            //OK let heightKeyboard = (notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            //OK let heightKeyboard1 = (notification_userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue

            if let value = notification_userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
                let heightKeyboard = value.cgRectValue.height

                //---------------------------------------------------------------------
                //DURATION
                //---------------------------------------------------------------------

                if let durationNSValue = notification_userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber {
                    let durationDouble = durationNSValue.doubleValue

                        //logger.debug("keyboardWillShow: durationDouble:\(durationDouble)")
                        self.viewAddNewSearchResults.alpha = 0.0
                        self.viewAddNewSearchResults.isHidden = false

                    UIView.animate(withDuration: durationDouble, delay:0.0, options: UIView.AnimationOptions(),
                                       animations: {
                                        //self.view.frame = CGRectMake(0, 0, Geo.width(), Geo.height() - height)
                                        self.viewAddNewSearchResults_BottomConstraint.constant = heightKeyboard - (50 + 44);
                                        self.viewAddNewSearchResults.alpha = 1.0
                            },
                                       completion: {finished in
                                        //self.textFieldAddSearch.text = ""
                            }
                        )
                    
                }else{
                    logger.error("notification_userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber is nil")
                }
            }else{
                logger.error("notification_userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue is nil")
            }
            
        }else{
            logger.error("(notification as NSNotification).userInfo is nil")
        }
    }

    
    @objc func keyboardWillHide(_ notification: Notification) {
        //---------------------------------------------------------------------
        if let notification_userInfo = (notification as NSNotification).userInfo {
            
            //---------------------------------------------------------------------
            //KEYBOARD HEIGHT
            //---------------------------------------------------------------------
            //OK let heightKeyboard = (notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            //OK let heightKeyboard1 = (notification_userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            
            if let value = notification_userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
                //height not require we just set constraint back to 0
                let _ = value.cgRectValue.height
                
                //---------------------------------------------------------------------
                //DURATION
                //---------------------------------------------------------------------
                
                if let durationNSValue = notification_userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber {
                    let durationDouble = durationNSValue.doubleValue
                    logger.debug("keyboardWillHide: duration:\(durationDouble)")
                    
                    UIView.animate(withDuration: durationDouble, delay:0.0, options: UIView.AnimationOptions(),
                                   animations: {
                                    self.viewAddNewSearchResults_BottomConstraint.constant = 0;
                                    self.viewAddNewSearchResults.alpha = 0.0
                        },
                                   completion: {finished in
                                    self.textFieldAddSearch.text = ""
                        }
                    )
                    
                }else{
                    logger.error("notification_userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber is nil")
                }
            }else{
                logger.error("notification_userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue is nil")
            }
            
        }else{
            logger.error("(notification as NSNotification).userInfo is nil")
        }
    }
    
    @IBAction func buttonCancel_Action(_ sender: AnyObject) {
        //self.navigationController?.popViewControllerAnimated(true)
        
        self.dismiss(animated: true, completion: {
            
            //------------------------------------------------
            if let delegate_ = self.delegate {
                //containerView is hidden by the delegate
                delegate_.colcPlacePickerViewControllerCancelled(self)
            }else{
                logger.error("self.delegate is nil")
            }
            //------------------------------------------------
        })
        
    }

    @IBAction func buttonSearch_Action(_ sender: AnyObject) {
        
        // TODO: - add timer
        logger.error("self.textFieldAddSearch: '\(String(describing: self.textFieldAddSearch.text))'")
        if self.textFieldAddSearch.text == ""{
            
        }else{
            if let text = self.textFieldAddSearch.text {
                callJSONWebservices_placeAutoComplete(text)
            }else{
                logger.error("self.textFieldAddSearch.text is nil")
            }
        }
    }
    
    
    
    //called from search button and when typing paused timer triggers
    func callJSONWebservices_placeAutoComplete(_ searchText: String){
    
        //---------------------------------------------------------------------
        DispatchQueue.global(qos: .background).async {
            //---------------------------------------------------------------------
            //BACKGROUND
            //---------------------------------------------------------------------
            //---------------------------------------------------------------------
            if searchText.trim() == ""{
                logger.error("searchText is just a space - skipping")
            }else{
                if let currentMapLocation = self.currentMapLocation{
                    
                    //clear table
                    self.placeAutoComplete_CLKGooglePlaceAutocompletePredictionArray = [CLKGooglePlaceAutocompletePrediction]()
                    self.tableViewSearch.reloadData()
                    
                    
                    // TODO: - set radius to where map is zoomed to

                    self.placeAutoComplete(searchText, location: currentMapLocation)
                    
                    
                }else{
                    logger.error("self.currentMapLocation is nil")
                }
            }
        }
        //---------------------------------------------------------------------
    }
    

    //--------------------------------------------------------------
    // MARK: -
    // MARK: - LOCATION/MAP
    //--------------------------------------------------------------
    
    //nearbysearch is triggered AFTER move finished in idleAtCameraPosition:
    func moveToCurrentLocation(){
        if self.userDidChangeCameraPosition{
            //NOISY  
            //logger.debug("moveToCurrentLocation - userDidChangeCameraPosition is true DONT move to new currentLocation")
        }else{
            //NOISY 
            //logger.debug("moveToCurrentLocation - userDidChangeCameraPosition is false DO move to new currentLocation")
            //map at default so ok to move to current location

            if let currentLocation = appDelegate.currentLocationManager.currentUserPlace.clLocation {
                //NOISY: logger.debug("LOC: [moveToCurrentLocation:]about to call  moveMapToCLLocation(currentLocation:\(currentLocation))")
                moveMapToCLLocation(currentLocation)
                
            }else{
                logger.error("currentUserPlace.clLocation is nil")
            }

        }
    }
    
    //called every time a new device location is recieved - UNLESS user manually moves the map then thats disabled
    //called when you tap on a row in the table
    func moveMapToCLLocation(_ clLocation : CLLocation){
        //------------------------------------------------------------------------------------------------
        if clLocation.isValid{
            //-----------------------------------------------------------------------------------
            //zoom in if very far out - when app starts can be a zoom 4.0
            var zoom: Float = self.mapView.camera.zoom
            if self.mapView.camera.zoom < 17{
                zoom = 17
            }
             //-----------------------------------------------------------------------------------
            let cameraPosition = GMSCameraPosition.camera(withLatitude: clLocation.coordinate.latitude, longitude:clLocation.coordinate.longitude, zoom:zoom)
            //var mapView = GMSMapView.mapWithFrame(CGRectZero, camera:camera)
            //NOISY logger.warning("LOC: animateToCameraPosition( cameraWithLatitude:[\(clLocation.coordinate.latitude),\(clLocation.coordinate.longitude)")
            self.mapView.animate(to: cameraPosition)
            //triggered AFTER move complete in idleAtCameraPosition:
            //self.nearbySearch(currentLocation)
            //-----------------------------------------------------------------------------------
        }else{
             logger.debug("LOC: [moveMapToCLLocation(clLocation:\(clLocation))] clLocation.isValid is false - dont call animateToCameraPosition")
        }
   
    }
    
    
    //pass it nil for name else it will display it in Nearest address e.g. "Nearest Address, 15 Royal Mint st..." which we dont want
    var clkLocationPlaceMapCenter : CLKLocationPlace = CLKLocationPlace()
    
    //------------------------------------------------------------------------------------------------
    //GEOCODERs
    //------------------------------------------------------------------------------------------------
    var colcGeocoder = COLCGeocoder()
    
    
    func geocodeCenterOfMapLocation(_ currentLocation: CLLocation){
        //---------------------------------------------------------------------
        //locations will come in over and over. if less 10 meters will geocode to address but not if one in progress
        //---------------------------------------------------------------------



        //---------------------------------------------------------------------
        //
        //---------------------------------------------------------------------
        if currentLocation.isValid{
            clkLocationPlaceMapCenter.updateLocation(clLocation: currentLocation)
            
            //------------------------------------------------------------------------------------------------
            //CALL REVERSE GEOCODE - alway if you user this methof
            //------------------------------------------------------------------------------------------------
            self.colcGeocoder.reverseGeocodePlace(clkPlace: clkLocationPlaceMapCenter,
                                                         success:{(clkPlace: CLKPlace) -> Void in
                                                            
                                                            
                                                            //---------------------------------------------------------------------
                                                            //GEOCODE OK
                                                            //---------------------------------------------------------------------
                                                            if let formatted_address = self.clkLocationPlaceMapCenter.formatted_address{
                                                                //---------------------------------------------------------------------
                                                                //both not nil
                                                                self.labelNearestAddress?.text = "\(formatted_address)"
                                                                
                                                                self.imageViewNearestResult?.image = mapPinImage("", fillColor: AppearanceManager.appColorTintInternal,
                                                                                                                 textColor: UIColor.white)
                                                                
                                                                //---------------------------------------------------------------------
                                                            }else{
                                                                // TODO: - test bus stop
                                                                logger.error("formatted_address is nil - can happen for bus stops")
                                                                
                                                                self.labelNearestAddress?.text = "Retrieving address..."
                                                            }
                                                            //---------------------------------------------------------------------
            },
                                                         failure:{ (error: Error?) -> Void in
                                                                logger.error("[GEOCODE CURRENT LOCATION FAILED] currentLocation(\(currentLocation)) error:\(String(describing: error)) ")
            }
            )
            //------------------------------------------------------------------------------------------------
            
        }else{
            logger.debug("clLocation.isValid is false - skip geocode of new location")
        }
        
    }
    
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - WS: NEARBY SEARCH - FIND BUSINESSES
    //--------------------------------------------------------------
    func nearbySearch(_ location: CLLocation){
        self.nearbySearch(location, radius:GooglePlacesController.defaultSearchRadius)
    }
    func nearbySearch(_ location: CLLocation, radius:Double){
        
        //UIApplication.shared.isNetworkActivityIndicatorVisible = true
        logger.debug("[ nearbySearch ] ")
        if self.nearbySearchRunning{
            logger.error("nearbySearchRunning TRUE - do nothin")
        }else{
            self.nearbySearchRunning = true
            
            self.mapView.clear()

            //------------------------------------------------------------------------------------------------
            //blue unfille circle showing readius of search
            //------------------------------------------------------------------------------------------------
            let gmsCircle = GMSCircle(position: location.coordinate, radius: radius)
            //gmsCircle.strokeColor = UIColor.appBaseColor()
            gmsCircle.strokeColor = AppearanceManager.appColorNavbarAndTabBar.withAlphaComponent(0.5)
            gmsCircle.map = self.mapView;
            //------------------------------------------------------------------------------------------------
            callWS_NearbySearch(location, radius:radius)
            //---------------------------------------------------------------------
        }
    
    }
    
    func applySortToResult(){
        
        //get the unsorted results returned by the WS
        //should be set in callWS_NearbySearch
        if let nearbySearch_CLKGooglePlaceResultsArrayUnSorted = self.nearbySearch_CLKGooglePlaceResultsArrayUnSorted{
            
            //------------------------------------------------------------------------------------------------
            switch self.currentPlaceResultsSort{
            case .sortByGoogleWSResults:
                // Sort in order returned from WS
                self.nearbySearch_CLKGooglePlaceResultsArray = nearbySearch_CLKGooglePlaceResultsArrayUnSorted
                
            case .sortByNearest:
                // As you move map results are sorted/ numbered so nearest always at the top
                //-------------------------------------------------------------------------------------
                //SORT BY DIST TO MAP CENTER
                //Note you must have called updateDistanceToMapCenter a
                //-------------------------------------------------------------------------------------
                let sortedByDistArray = nearbySearch_CLKGooglePlaceResultsArrayUnSorted.sorted { $0.distanceToMapCenter < $1.distanceToMapCenter }
                self.nearbySearch_CLKGooglePlaceResultsArray = sortedByDistArray
                
            case .sortByName:
                // Sort by Name of business - user may know name but not exact location
                let sortedByNameArray = nearbySearch_CLKGooglePlaceResultsArrayUnSorted.sorted { $0.name < $1.name }
                self.nearbySearch_CLKGooglePlaceResultsArray = sortedByNameArray
                
            }
            //------------------------------------------------------------------------------------------------

        }else{
            logger.error("self.nearbySearch_CLKGooglePlaceResultsArrayUnSorted is nil - should have been set in callWS_NearbySearch()")
        }
    }
    
    
    
    //--------------------------------------------------------------
    // MARK: - PLACES
    // MARK: -
    //--------------------------------------------------------------
    //moved in from APPD
    var googlePlacesController = GooglePlacesController()
    
    func callWS_NearbySearch(_ location: CLLocation,
                               radius:Double?)
    {
        if location.isValid{
            //------------------------------------------------
            self.googlePlacesController.doNearbySearch(location,
                                                              radius:radius,
                                                              success:{(results: [CLKGooglePlaceResult]?) -> Void in
                                                                
                                                                //---------------------------------------------------------------------
                                                                if let results = results{
                                                                    //NOISY logger.debug("clkPlaceSearchResponse.results:\(results.count) PLACES FOUND")
                                                                    
                                                                    
                                                                    
                                                                    var count = 1
                                                                    //logger.debug("======================================================================")
                                                                    
                                                                    //-------------------------------------------------------------------------------------
                                                                    //UPDATE dist to map center
                                                                    //-------------------------------------------------------------------------------------
                                                                    for clkGooglePlaceResult in results{
                                                                        
                                                                        //-------------------------------------------------------------------
                                                                        //CLKResult/Dist to map center
                                                                        //-------------------------------------------------------------------
                                                                        //store internally as sort method doesnt take parameter
                                                                        clkGooglePlaceResult.updateDistanceToMapCenter(mapCenterLocation: location)
                                                                        
                                                                        //logger.debug("\(clkGooglePlaceResult)")
                                                                        
                                                                    }
                                                                    //logger.debug("======================================================================")
                                                                    
                                                                    //-------------------------------------------------------------------------------------
                                                                    //STORE IN GLOBAL ARRAY SO Table/Map can be filled from it
                                                                    //-------------------------------------------------------------------------------------
                                                                    //Store unsorted result - so we can add remove sorts later
                                                                    self.nearbySearch_CLKGooglePlaceResultsArrayUnSorted = results
                                                                    
                                                                    self.applySortToResult()
                                                                    
                                                                    //-------------------------------------------------------------------------------------
                                                                    //PLACE ON MAP USING SORTED RESULTS
                                                                    //-------------------------------------------------------------------------------------
                                                                    if let nearbySearch_CLKGooglePlaceResultsArray = self.nearbySearch_CLKGooglePlaceResultsArray{
                                                                        for clkGooglePlaceResult in nearbySearch_CLKGooglePlaceResultsArray{
                                                                            clkGooglePlaceResult.addToMap(self.mapView, pinText:"\(count)")
                                                                            count += 1
                                                                        }
                                                                        
                                                                    }else{
                                                                        //NOISY logger.error("self.nearbySearch_CLKGooglePlaceResultsArray is nil")
                                                                    }
                                                                }else{
                                                                    //moved where theres no businesses
                                                                    logger.error("results is nil")
                                                                    
                                                                    self.nearbySearch_CLKGooglePlaceResultsArrayUnSorted = [CLKGooglePlaceResult]()
                                                                }
                                                                
                                                                
                                                                //-------------------------------------------------------------------------------------
                                                                //FILL TABLE/CLEAR table if not results
                                                                //-------------------------------------------------------------------------------------
                                                                self.tableViewPlacesResults.reloadData()
                                                                //scroll to top as results cleared
                                                                self.tableViewPlacesResults.setContentOffset(CGPoint.zero, animated:true)
                                                                
                                                                
                                                                
                                                                //---------------------------------------------------------------------
                                                                self.nearbySearchRunning = false
                                                                //UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                                                
                                                                self.sonarView.stopAnimation()
                },
                                                              failure:{(error) -> Void in
                                                                logger.error("error:\(String(describing: error))")
                                                                self.sonarView.stopAnimation()
                                                                
                                                                var showErrorAlert = true
                                                                if let error = error {
                                                                    switch error{
                                                                        
                                                                    case AppError.googlePlacesResponseStatus_ZERO_RESULTS_PLACES:
                                                                        logger.debug("status: ZERO_RESULTS")
                                                                        //DONT SHOW ERROR ALERT FOR ZERO RESULTS
                                                                        showErrorAlert = false
                                                                        
                                                                    case AppError.googlePlacesResponseStatus_ZERO_RESULTS_DISTANCE:
                                                                        logger.debug("status: GooglePlacesResponseStatus_ZERO_RESULTS_DISTANCE - WRONG RESULT use GooglePlacesResponseStatus_ZERO_RESULTS_PLACES")
                                                                        
                                                                    case AppError.googlePlacesResponseStatus_OVER_QUERY_LIMIT:
                                                                        logger.debug("status: OVER_QUERY_LIMIT")
                                                                        
                                                                    case AppError.googlePlacesResponseStatus_REQUEST_DENIED:
                                                                        logger.debug("status: REQUEST_DENIED")
                                                                        
                                                                    case AppError.googlePlacesResponseStatus_INVALID_REQUEST:
                                                                        
                                                                        logger.debug("status: INVALID_REQUEST")
                                                                        
                                                                    default:
                                                                        logger.error("UNKNOWN error: \(error)")
                                                                    }
                                                                    //------------------------------------------------------------------------------------------------
                                                                    
    
                                                                    //remove all results
                                                                    self.nearbySearch_CLKGooglePlaceResultsArray = [CLKGooglePlaceResult]()
                                                                    self.tableViewPlacesResults.reloadData()
                                                                    //------------------------------------------------------------------------------------------------
                                                                    // self.labelAddress.text = ""
                                                                    //------------------------------------------------------------------------------------------------

                                                                    
                                                                }else{
                                                                    logger.error("error is nil")
                                                                }
                                                                
                                                                
                                                                if showErrorAlert{
                                                                    CLKAlertController.showAlertInVCWithCallback(self, title: "Error", message: error?.localizedDescription)
                                                                    { () -> Void in
                                                                        logger.debug("Ok tapped = alert closed appDelegate.doLogout()")
                                                                    }
                                                                }
                                                                
                                                                self.nearbySearchRunning = false
                                                                //UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
            )
            //------------------------------------------------
        }else{
            logger.error("location.isValid is false - doNearbySearch cancelled")
            //reset or else map is never filled even if you manually move the map
            self.nearbySearchRunning = false
        }
    }
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - WS: placeAutoComplete SEARCH
    // MARK: -
    //--------------------------------------------------------------
    func placeAutoComplete(_ input:String, location: CLLocation){
        self.placeAutoComplete(input, location: location, radius:GooglePlacesController.defaultSearchRadius)
    }
    
    func placeAutoComplete(_ input:String, location: CLLocation, radius:Double){
        
        //UIApplication.shared.isNetworkActivityIndicatorVisible = true

        logger.debug("[ placeAutoComplete ] ")
        
        if self.placeAutoCompleteRunning{
            logger.error("placeAutoCompleteRunning TRUE - do nothin")
        }else{
            self.placeAutoCompleteRunning = true

            //OK
            //callWS_PlaceAutoComplete("CAU",location:location, radius:radius)
            // callWS_PlaceAutoComplete("The Slug and Lettuce",location:location, radius:radius)
            //callWS_PlaceAutoComplete("The Slug and Lettuce",location:location, radius:200.0)
            
            callWS_PlaceAutoComplete(input,location:location, radius:radius)
            //---------------------------------------------------------------------
        }
    }
    //--------------------------------------------------------------
    // MARK: - PLACES
    // MARK: -
    //--------------------------------------------------------------


    
    func callWS_PlaceAutoComplete(_ input:String,
        location: CLLocation,
        radius:Double?){
            //---------------------------------------------------------------------
            self.googlePlacesController.doPlaceAutoComplete(input,
                location:location,
                radius:radius,
                success:{(predictions: [CLKGooglePlaceAutocompletePrediction]?) -> Void in
                    
                    //---------------------------------------------------------------------
                    if let predictions = predictions{
                        //logger.debug("clkPlaceSearchResponse.results:\(predictions.count) PLACES FOUND")
                        
                        //---------------------------------------------------------------------
                        for clkPlaceAutocompletePrediction : CLKGooglePlaceAutocompletePrediction in predictions{
                            if let _ = clkPlaceAutocompletePrediction.description{
                                //logger.debug("[GOOGLE PLACE SEACH: RESULT]:\(description)")
                            }else{
                                logger.error("clkPlaceAutocompletePrediction.description is nil")
                            }
                        }
                        //---------------------------------------------------------------------
                        self.placeAutoComplete_CLKGooglePlaceAutocompletePredictionArray = predictions
                        self.tableViewSearch.reloadData()

                        //---------------------------------------------------------------------
                    }else{
                        logger.error("results is nil")
                    }
                    //---------------------------------------------------------------------
                    self.placeAutoCompleteRunning = false
                    //UIApplication.shared.isNetworkActivityIndicatorVisible = false
                },
                failure:{(error) -> Void in
                    logger.error("error:\(String(describing: error))")
                    
                    CLKAlertController.showAlertInVCWithCallback(self, title: "Error", message: error?.localizedDescription)
                        { () -> Void in
                            logger.debug("Ok tapped = alert closed appDelegate.doLogout()")
                    }
                    self.placeAutoCompleteRunning = false
                    //UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
            )
            //---------------------------------------------------------------------
    }
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - WS: queryAutoComplete SEARCH
    //--------------------------------------------------------------
    func queryAutoComplete(_ input:String, location: CLLocation){
        self.queryAutoComplete(input, location:location, radius:GooglePlacesController.defaultSearchRadius)
    }
    func queryAutoComplete(_ input:String, location: CLLocation, radius:Double){
        
        //UIApplication.shared.isNetworkActivityIndicatorVisible = true

        logger.debug("[ queryAutoComplete ] ")
        
        if self.queryAutoCompleteRunning{
            logger.error("queryAutoComplete TRUE - do nothin")
        }else{
            self.queryAutoCompleteRunning = true
            
            //self.mapView.clear()
            
            //OK
            callWS_QueryAutoComplete(input,location:location, radius:radius)
            //OK
            //callWS_QueryAutoComplete("Pizza in London",location:location, radius:radius)
            
            //Not great
            //callWS_QueryAutoComplete("Pub in Wapping",location:location, radius:radius)
            //---------------------------------------------------------------------
        }
        
    }
    
    //--------------------------------------------------------------
    // MARK: - GOOGLE PLACES
    // MARK: -
    //--------------------------------------------------------------

    
    func callWS_QueryAutoComplete(_ input:String,
        location: CLLocation,
        radius:Double?){
            //---------------------------------------------------------------------
            self.googlePlacesController.doQueryAutoComplete(input,
                location:location,
                radius:radius,
                success:{(predictions: [CLKGooglePlaceAutocompletePrediction]?) -> Void in
                    
                    //---------------------------------------------------------------------
                    if let predictions = predictions{
                        //NOISY logger.debug("clkPlaceSearchResponse.results:\(predictions.count) PLACES FOUND")
                        
                        //---------------------------------------------------------------------
                        for clkPlaceAutocompletePrediction : CLKGooglePlaceAutocompletePrediction in predictions{
                            if let description = clkPlaceAutocompletePrediction.description{
                                logger.debug("description:\(description)")
                            }else{
                                logger.error("clkPlaceAutocompletePrediction.description is nil")
                            }
                        }
                        //---------------------------------------------------------------------
                        // TODO: - beware need to share tableView with 3 searches
                        self.queryAutoComplete_CLKGooglePlaceAutocompletePredictionArray = predictions
                        self.tableViewSearch.reloadData()
                    }else{
                        logger.error("results is nil")
                    }
                    //---------------------------------------------------------------------
                    self.queryAutoCompleteRunning = false
                    //---------------------------------------------------------------------
                    //UIApplication.shared.isNetworkActivityIndicatorVisible = false
                },
                failure:{(error) -> Void in
                    logger.error("error:\(String(describing: error))")
                    
                    CLKAlertController.showAlertInVCWithCallback(self, title: "Error", message: error?.localizedDescription)
                        { () -> Void in
                            logger.debug("Ok tapped = alert closed appDelegate.doLogout()")
                    }
                    self.queryAutoCompleteRunning = false
                    //UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
            )
            //---------------------------------------------------------------------
    }
    
    func callWS_get_place_detail(_ placeIdString: String){
        //---------------------------------------------------------------------
        self.googlePlacesController.googlePlacesWSController.get_place_detail(placeIdString,
            success:{
                (clkPlaceSearchResponse: CLKGooglePlaceSearchResponse?)->Void in
                //---------------------------------------------------------------------
                if let clkPlaceSearchResponse = clkPlaceSearchResponse{
                    logger.debug("[callWS_get_place_detail] clkPlaceSearchResponse returned")
                    
             
                    if let result = clkPlaceSearchResponse.result {
                        if let geometry = result.geometry {
                            if let location = geometry.location {
                                if let lat = location.lat {
                                    if let lng = location.lng {
                                        
                                        let cameraPosition = GMSCameraPosition.camera(withLatitude: lat.doubleValue, longitude: lng.doubleValue, zoom:16)
                                        //var mapView = GMSMapView.mapWithFrame(CGRectZero, camera:camera)
                                        
                                        //open app/dont move map/do search:'paris'/ dont move map/ next loc jumps back to paris
                                        //this blocks the device current loc from moving the map
                                        self.userDidChangeCameraPosition = true
                                        self.mapView.animate(to: cameraPosition)
                                        
                                        
                                    }else{
                                        logger.error("clkPlaceSearchResponse.result.geometry.location.lng is nil")
                                    }
                                }else{
                                    logger.error("clkPlaceSearchResponse.result.geometry.location.lat is nil")
                                }
                            }else{
                                logger.error("clkPlaceSearchResponse.result.geometry.location is nil")
                            }
                        }else{
                            logger.error("clkPlaceSearchResponse.result.geometry is nil")
                        }
                    }else{
                        logger.error("clkPlaceSearchResponse.result is nil")
                    }
                    
                }else{
                    logger.error("clkPlaceSearchResponse is nil or not CLKGooglePlaceSearchResponse")
                }
                //---------------------------------------------------------------------
            },
            failure:{
                (error) -> Void in
                logger.error("error:\(String(describing: error))")
                
                CLKAlertController.showAlertInVCWithCallback(self, title: "Error", message: error?.localizedDescription)
                    { () -> Void in
                        logger.debug("Ok tapped = alert closed appDelegate.doLogout()")
                }
            }
        )
        //---------------------------------------------------------------------
    }
    
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - GMSMapViewDelegate
    //--------------------------------------------------------------
    
    //Called before the camera on the map changes, either due to a gesture, animation (e.g., by a user tapping on the “My Location” button)
    //or by being updated explicitly via the camera or a zero-length animation on layer.
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool){
        
        
        if gesture{
            //OK NOISY - logger.debug("willMove gesture:\(gesture) self.userDidChangeCameraPosition = true")
            self.userDidChangeCameraPosition = true
            //if user moves map with their hand then should recall ws
            self.mapMovedOkToCallWS = true
        }else{
            
            //or map moving because device location sending notification (and user hasnt manually moved the map to block this)
            ////USER TAPPED ON MYLOCATION BUTTON resets userDidChangeCameraPosition
            //OK NOISY - logger.debug("willMove gesture:\(gesture) - map moved because device location notif sent or user tapped MY LOC button")
        }
        
    }
    /**
    * Called repeatedly during any animations or gestures on the map (or once, if
    * the camera is explicitly set). This may not be called for all intermediate
    * camera positions. It is always called for the final position of an animation
    * or gesture.
    */
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition){
        
//        logger.debug("didChangeCameraPosition")
        //delegate may send current position and map jumps back, need flag to prevent this
        //
        
    }
    
    func clLocationForCLLocationCoordinate2D(_ clLocationCoordinate2D : CLLocationCoordinate2D) -> CLLocation{
        let location: CLLocation = CLLocation(latitude: clLocationCoordinate2D.latitude, longitude: clLocationCoordinate2D.longitude)
        return location
    }
    /**
    * Called when the map becomes idle, after any outstanding gestures or
    * animations have completed (or after the camera has been explicitly set).
    */
    
    //--------------------------------------------------------------
    // MARK: - MOVE MAP - CALL GOOGLE NEARBY and GEOLOCATE
    // MARK: -
    //--------------------------------------------------------------

    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition){
        logger.warning("LOC: [idleAtCameraPosition] START")

        logger.debug("idleAtCameraPosition: position:\(position.target)")
        
        
        //------------------------------------------------------------------------------------------------
        //UPDATE CURRENT
        //------------------------------------------------------------------------------------------------
        //position.target: CLLocationCoordinate2D
        let location: CLLocation = clLocationForCLLocationCoordinate2D(position.target)
        
        if location.isValid == false{
            logger.warning("LOC: [idleAtCameraPosition] location.isValid is FALSE - map pointing at blue equator - move it - calling moveToCurrentLocation")
            self.moveToCurrentLocation()
        }else{
            logger.warning("LOC: [idleAtCameraPosition] location.isValid - continue")

            //------------------------------------------------
            //                position.bearing        //CLLocationDirection :Double
            //                position.target         //CLLocationCoordinate2D
            //                position.viewingAngle   //Double
            //                position.zoom           //Float
            
            //OK just not needed
            //        if let mapView_projection = self.mapView.projection{
            //            let gmsVisibleRegion = mapView_projection.visibleRegion()
            //
            ////            /** Bottom left corner of the camera. */
            ////            var nearLeft: CLLocationCoordinate2D
            ////
            ////            /** Bottom right corner of the camera. */
            ////            var nearRight: CLLocationCoordinate2D
            ////
            ////            /** Far left corner of the camera. */
            ////            var farLeft: CLLocationCoordinate2D
            ////
            ////            /** Far right corner of the camera. */
            ////            var farRight: CLLocationCoordinate2D
            //
            //
            //            logger.debug("  farLeft:\(gmsVisibleRegion.farLeft.latitude), \(gmsVisibleRegion.farLeft.longitude)")
            //            logger.debug(" farRight:\(gmsVisibleRegion.farRight.latitude), \(gmsVisibleRegion.farRight.longitude)")
            //            logger.debug(" nearLeft:\(gmsVisibleRegion.nearLeft.latitude), \(gmsVisibleRegion.nearLeft.longitude)")
            //            logger.debug("nearRight:\(gmsVisibleRegion.nearRight.latitude), \(gmsVisibleRegion.nearRight.longitude)")
            //            logger.debug("")
            //        }else{
            //            logger.error("self.mapView.projectionis nil")
            //        }
            //------------------------------------------------------------------------------------------------
            //PREVENT TOO MUCH WS CALLS if map only moves small bit
            //------------------------------------------------------------------------------------------------
            var mapMovedEnoughToCallWS = false
            //shut up warning
            print(mapMovedEnoughToCallWS)
            //is this first location retrieved? if not then backup current
            if let currentMapLocation = self.currentMapLocation{
                //---------------------------------------------
                //prev and current set
                //---------------------------------------------
                //Backup current before updating it below
                self.prevMapLocation = currentMapLocation
                
            }else{
                logger.warning("LOC: [idleAtCameraPosition] currentMapLocation is nil - map first appearing idleAtCameraPosition")
            }
            
            
            //------------------------------------------------------------------------------------------------
            //UPDATE CURRENT
            //------------------------------------------------------------------------------------------------
            //position.target: CLLocationCoordinate2D
           
            //            if location.isValid{
            //                logger.warning("LOC: [idleAtCameraPosition] location.isValid use as currentMapLocation")
            //            }else{
            //                
            //                if let currentLocation = appDelegate.currentLocation {
            //                    logger.warning("LOC: [idleAtCameraPosition] location.isValid FALSE - using currentLocation:[\(currentLocation.coordinate.latitude),\(currentLocation.coordinate.longitude)")
            //                    location = currentLocation
            //                }else{
            //                    logger.warning("LOC: [idleAtCameraPosition] location.isValid FALSE - appDelegate.currentLocation is nil - location may be invalid")
            //                }
            //                
            //            }
            
            //needed for querysearch / place search - where to center your search from
            self.currentMapLocation = location
            
            //------------------------------------------------------------------------------------------------
            //if dist between prev and current large enough refresh the results
            //------------------------------------------------------------------------------------------------
            
            if let prevMapLocation = self.prevMapLocation{
                if let currentMapLocation = self.currentMapLocation{
                    //------------------------------------------------------------------------------------------------
                    //GET dist between the PREV /CURRENT map locations
                    //------------------------------------------------------------------------------------------------
                    
                    //FROM = e.g. MAP CENTER
                    let from: CLLocationCoordinate2D =  CLLocationCoordinate2D(latitude: prevMapLocation.coordinate.latitude, longitude: prevMapLocation.coordinate.longitude)
                    //print("from:\(from)")
                    
                    //TO = Place result location SELF
                    let to: CLLocationCoordinate2D =  CLLocationCoordinate2D(latitude: currentMapLocation.coordinate.latitude, longitude: currentMapLocation.coordinate.longitude)
                    //print("to:\(from)")
                    //GoogleMaps function - returns meters
                    let distanceMetersDouble : CLLocationDistance =  GMSGeometryDistance(from, to)
                    
                    
                    // TODO: - WHEN ZOOMED IN THIS CAN FAIL TO BE TRIGGERED
                    let maxDist = 10.0
                    if distanceMetersDouble > maxDist{
                        //logger.debug("distanceMetersDouble:\(distanceMetersDouble) GREATER THAN \(maxDist)m - call WS")
                        mapMovedEnoughToCallWS = true
                    }else{
                        //logger.debug("distanceMetersDouble:\(distanceMetersDouble) LESS THAN \(maxDist)m - dont call WS")
                        mapMovedEnoughToCallWS = false
                    }
                    
                    //------------------------------------------------------------------------------------------------
                    
                }else{
                    logger.debug("currentMapLocation is nil - ok to update map")
                    mapMovedEnoughToCallWS = true
                }
            }else{
                logger.debug("prevMapLocation is nil - ok to update map")
                mapMovedEnoughToCallWS = true
            }
            //-----------------------------------------------------------------------------------
            if mapMovedOkToCallWS{
                //update Map and Table
                
                self.sonarView.startAnimation()
                logger.warning("LOC: [idleAtCameraPosition] CALL nearbySearch()")
                self.nearbySearch(location)
                //-----------------------------------------------------------------------------------
                //move down - always get
                ///Convert lat/lng to address
                //self.geocodeLocation(location.coordinate)
                //-----------------------------------------------------------------------------------
            }else{
                logger.warning("LOC: [idleAtCameraPosition] mapMovedOkToCallWS is FALSE - DONT CALL NEARBY SEARCH OR GEOCODE LOCATION")
            }
            
            //------------------------------------------------
            //GEOCODE LOCATION
            //------------------------------------------------
            ///Convert lat/lng to address
            self.geocodeCenterOfMapLocation(location)

            //------------------------------------------------
        }

    }
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - TAP/PRESS at COOORD
    //--------------------------------------------------------------
    /**
    * Called after a tap gesture at a particular coordinate, but only if a marker
    * was not tapped.  This is called before deselecting any currently selected
    * marker (the implicit action for tapping on the map).
    */
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D){
        logger.debug("mapView: didTapAtCoordinate")
    }
    /**
    * Called after a long-press gesture at a particular coordinate.
    *
    * @param mapView The map view that was pressed.
    * @param coordinate The location that was pressed.
    */
    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D){
        logger.debug("mapView: didLongPressAtCoordinate")
    }
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - SCROLL TABLE WHEN USER TAPS ON PIN
    //--------------------------------------------------------------
    
    private func findRowIndexForCLKGooglePlaceResult(_ clkGooglePlaceResult: CLKGooglePlaceResult) -> Int{
        var indexItemFoundAt : Int = -1
        
        var indexCount : Int = 0
        if let nearbySearch_CLKGooglePlaceResultsArray = self.nearbySearch_CLKGooglePlaceResultsArray{
            
            for clkGooglePlaceResultInArray in nearbySearch_CLKGooglePlaceResultsArray{
                
                if let clkGooglePlaceResultInArray_place_id = clkGooglePlaceResultInArray.place_id{
                    if let clkGooglePlaceResult_place_id = clkGooglePlaceResult.place_id{
                        //------------------------------------------------------------------------------------------------
                        if clkGooglePlaceResultInArray_place_id == clkGooglePlaceResult_place_id{
                            //item found
                            indexItemFoundAt = indexCount
                            break
                        }else{
                            
                        }
                        //------------------------------------------------------------------------------------------------
                    }else{
                        logger.error("clkGooglePlaceResult.place_id is nil")
                    }
                }else{
                    logger.error("clkGooglePlaceResultInArray.place_id is nil")
                }
                indexCount += 1
            }
        }else{
            //NOISY logger.error("self.nearbySearch_CLKGooglePlaceResultsArray is nil")
        }
        return indexItemFoundAt
    }
    
    private func scrollToRowForCLKGooglePlaceResult(_ clkGooglePlaceResult: CLKGooglePlaceResult){
        
        let row = self.findRowIndexForCLKGooglePlaceResult(clkGooglePlaceResult)
        if row == -1{
            logger.error("scrollToRowForCLKGooglePlaceResult FAILED - row is -1 clkGooglePlaceResult not found")
           
        }else{
            //move foudn row to TOP of the tabelView
             self.tableViewPlacesResults.scrollToRow(at: IndexPath(row: row, section: 0), at: .top, animated: true)
        }
        
        
        
    }
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - TAPPING
    //--------------------------------------------------------------
    /**
    * Called after a marker has been tapped.
    *
    * @param mapView The map view that was pressed.
    * @param marker The marker that was pressed.
    * @return YES if this delegate handled the tap event, which prevents the map
    *         from performing its default selection behavior, and NO if the map
    *         should continue with its default selection behavior.
    */
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool{
        
        logger.debug("didTapMarker:\(String(describing: marker.title))")
        
        if let clkGooglePlaceResult = marker.userData as? CLKGooglePlaceResult{
            
            //------------------------------------------------------------------------------------------------
            //SCROLL table to the row related to the pin
            //------------------------------------------------------------------------------------------------

            scrollToRowForCLKGooglePlaceResult(clkGooglePlaceResult)
            
            //move the map to the pin but map delegate idleAtCameraPosition should call the nearby WS else all the pin numbers/results change
            self.mapMovedOkToCallWS = false
            //needed else if you on MY LOC button then a pin it will jump back to MY LOC when next device location received
            self.userDidChangeCameraPosition = true
            
            //user tapped on pin
            self.userSelectedResultOnMapOrTable(clkGooglePlaceResult)
            
            
        }else{
            logger.error("marker.userData is nil or not CLKGooglePlaceResult")
        }
        //map handles tap
        return true
    }
    
    
    //user tapped on pin
    //or
    //user tapped on row on table
    func userSelectedResultOnMapOrTable(_ clkGooglePlaceResult: CLKGooglePlaceResult){
        //------------------------------------------------------------------------------------------------
        //Get name/address (address maybe blank)
        //------------------------------------------------------------------------------------------------
        
        let clkGooglePlace = CLKGooglePlace(clkGooglePlaceResult:clkGooglePlaceResult)

        //------------------------------------------------------------------------------------------------
        //GET FULL ADDRESS - requires another ws call
        //------------------------------------------------------------------------------------------------
        self.clkGooglePlaceSelected = clkGooglePlace
        
        clkGooglePlace.get_place_detail(clkGooglePlaceResult,
            success:{(clkGooglePlaceResultWithDetail: CLKGooglePlaceResult)->Void in
                
                //---------------------------------------------------------------------
                logger.debug("clkPlaceSearchResponse returned - with better address - store in clkPlaceSelected:[clkGooglePlaceResultWithDetail.formatted_address:\(String(describing: clkGooglePlaceResultWithDetail.formatted_address))]")
                //-----------------------------------------------------------------------------------
                //if we cant get improved address as user if they want to use GEOCODE address
                //Issue: could still be crap address after getting detail
                if let formatted_address = clkGooglePlace.formatted_address {
                    logger.debug("formatted_address AFTER get Detail:\(formatted_address)")
                    
                }else{
                    //may never happed
                    //"London UK" >> get Details >> "E1 8lg London UK"
                    logger.error("clkGooglePlaceResultWithDetail.formatted_address is nil: getting more detailed address failed - can happen for bus stops")
                    
                    CLKAlertController.showAlertInVCWithCallback(self, title: "Error", message: "Could not get detailed address for this location")
                    { () -> Void in
                        logger.debug("Ok tapped")
                    }
                }
                //-----------------------------------------------------------------------------------
                self.returnCLKPlaceSelectedThroughDelegate()
                //UIApplication.shared.isNetworkActivityIndicatorVisible = false
            },
            failure:{(error) -> Void in
                
                logger.error("error:\(String(describing: error))")
                
                CLKAlertController.showAlertInVCWithCallback(self, title: "Error", message: error?.localizedDescription)
                    { () -> Void in
                        logger.debug("Ok tapped = alert closed appDelegate.doLogout()")
                }
                //UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
        )
        
        
        
        //------------------------------------------------------------------------------------------------
        //MOVE MAP TO PIN LOCATION
        //------------------------------------------------------------------------------------------------
        if let clLocation = clkGooglePlaceResult.clLocation{
            self.moveMapToCLLocation(clLocation)
        }else{
            logger.error("clkGooglePlaceResult.clLocation is nil")
        }
    }
    

    
    //--------------------------------------------------------------
    // MARK: - GMSMapViewDelegate
    // MARK: -
    //--------------------------------------------------------------

    /**
    * Called after a marker's info window has been tapped.
    */
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker){
        logger.debug("mapView: didTapInfoWindowOfMarker")
    }
    /**
    * Called after an overlay has been tapped.
    * This method is not called for taps on markers.
    *
    * @param mapView The map view that was pressed.
    * @param overlay The overlay that was pressed.
    */
    func mapView(_ mapView: GMSMapView, didTap overlay: GMSOverlay){
        logger.debug("mapView: didTapOverlay")
    }
    /**
    * Called when a marker is about to become selected, and provides an optional
    * custom info window to use for that marker if this method returns a UIView.
    * If you change this view after this method is called, those changes will not
    * necessarily be reflected in the rendered version.
    *
    * The returned UIView must not have bounds greater than 500 points on either
    * dimension.  As there is only one info window shown at any time, the returned
    * view may be reused between other info windows.
    *
    * Removing the marker from the map or changing the map's selected marker during
    * this call results in undefined behavior.
    *
    * @return The custom info window for the specified marker, or nil for default
    */
//    func mapView(mapView: GMSMapView!, markerInfoWindow marker: GMSMarker!) -> UIView!
    
    
    /**
    * Called when mapView:markerInfoWindow: returns nil. If this method returns a
    * view, it will be placed within the default info window frame. If this method
    * returns nil, then the default rendering will be used instead.
    *
    * @param mapView The map view that was pressed.
    * @param marker The marker that was pressed.
    * @return The custom view to disaply as contents in the info window, or null to
    * use the default content rendering instead
    */
//    func mapView(mapView: GMSMapView!, markerInfoContents marker: GMSMarker!) -> UIView!
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - DRAGGING
    //--------------------------------------------------------------
    /**
    * Called when dragging has been initiated on a marker.
    */
    func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker){
        logger.debug("mapView: didBeginDraggingMarker")
    }
    /**
    * Called after dragging of a marker ended.
    */
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker){
        logger.debug("mapView: didEndDraggingMarker")
    }
    /**
    * Called while a marker is dragged.
    */
    func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker){
        logger.debug("mapView: didDragMarker")
    }
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - TAP LOCATION ICON
    //--------------------------------------------------------------
    /**
    * Called when the My Location button is tapped.
    *
    * @return YES if the listener has consumed the event (i.e., the default behavior should not occur),
    *         NO otherwise (i.e., the default behavior should occur). The default behavior is for the
    *         camera to move such that it is centered on the user location.
    */
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool{
        logger.debug("didTapMyLocationButtonForMapView")
        //the default behavior should occur. move to location
        
        //when app starts it will move the map to currentLocation over and over
        //unless user manually moves the map
        //tapping on the userlocation should reset this so map will follow user as they move
        self.userDidChangeCameraPosition = false
        
        return false
    }
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - TABLEVIEW
    //--------------------------------------------------------------
    
    //------------------------------------------------
    // MARK: -
    // MARK: UITableViewDataSource
    //------------------------------------------------
    // MARK: SECTIONS
    //------------------------------------------------
    //    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    //        
    //        var numberOfSectionsInTableView = 1
    //        
    //        if tableViewPlacesResults == self.tableViewPlacesResults{
    ////            numberOfSectionsInTableView = self.arrayTableData.count
    ////            logger.debug("numberOfSectionsInTableView: tableViewPlacesResults: \(numberOfSectionsInTableView)")
    //        }
    //        else if tableView == self.tableViewSearch{
    ////            numberOfSectionsInTableView =  dictionaryTableDataADD.keys.array.count;
    ////            logger.debug("numberOfSectionsInTableView: tableViewSearch: \(numberOfSectionsInTableView)")
    //        }
    //        else{
    //            logger.error("UNHANDLED TABLEVIEW")
    //        }
    //        
    //        return numberOfSectionsInTableView;
    //    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numberOfRowsInSection = 0 
    
        //------------------------------------------------------------------------------------------------
        if tableView == self.tableViewPlacesResults{
            
            if let nearbySearch_CLKGooglePlaceResultsArray = self.nearbySearch_CLKGooglePlaceResultsArray{
                numberOfRowsInSection = nearbySearch_CLKGooglePlaceResultsArray.count
            }else{
                //NOISY logger.error("self.nearbySearch_CLKGooglePlaceResultsArray is nil")
            }
            
        }
        else if tableView == self.tableViewSearch
        {
            
            if let placeAutoComplete_CLKGooglePlaceAutocompletePredictionArray = self.placeAutoComplete_CLKGooglePlaceAutocompletePredictionArray{
            //                if 0 == placeAutoComplete_CLKGooglePlaceAutocompletePredictionArray.count{
            //                    numberOfRowsInSection = 0 //TODO ADDRESS in ROW 0
            //                }else{
            //                   numberOfRowsInSection = placeAutoComplete_CLKGooglePlaceAutocompletePredictionArray.count + 1 //ADDRESS in ROW 0
            //                    numberOfRowsInSection = placeAutoComplete_CLKGooglePlaceAutocompletePredictionArray.count + 1 //ADDRESS in ROW 0
            //                }
                numberOfRowsInSection = placeAutoComplete_CLKGooglePlaceAutocompletePredictionArray.count
                
                
            }else{
                //NOISY logger.error("self.placeAutoComplete_CLKGooglePlaceAutocompletePredictionArray is nil")
                numberOfRowsInSection = 1
            }
        }
        else{
            logger.error("UNHANDLED TABLEVIEW")
        }

        //------------------------------------------------------------------------------------------------

        return numberOfRowsInSection
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = UITableViewCell()
        if tableView == self.tableViewPlacesResults{
            
            //------------------------------------------------------------------------------------------------
            let placesTableViewCell : PlacesTableViewCell = self.tableViewPlacesResults.dequeueReusableCell(withIdentifier: "PlacesTableViewCell", for: indexPath) as! PlacesTableViewCell
            
            placesTableViewCell.applyCustomFont()
            //---------------------------------------------------------------------
            //RESET as cell is reused but not all data set both times
            placesTableViewCell.labelTitle.text = ""
            placesTableViewCell.labelDetail.text = ""
            placesTableViewCell.labelSubDetail.text = ""
            
            //  placesTableViewCell.labelTitle?.applyCustomFontForCurrentTextStyle()
            //  placesTableViewCell.labelDetail?.applyCustomFontForCurrentTextStyle()
            //  placesTableViewCell.labelSubDetail?.applyCustomFontForCurrentTextStyle()
        
            
            //dont set the UIImageView to nil - crashes the other side
            placesTableViewCell.imageViewPlaceResult.image = nil
            placesTableViewCell.labelDistanceMeters.text = ""
            //--------------------------------------------------------------------
            //------------------------------------------------------------------------------------------------
            placesTableViewCell.contentView.backgroundColor = UIColor.white
            
            
            if let nearbySearch_CLKGooglePlaceResultsArray = self.nearbySearch_CLKGooglePlaceResultsArray{
                let clkGooglePlaceResult : CLKGooglePlaceResult = nearbySearch_CLKGooglePlaceResultsArray[(indexPath as NSIndexPath).row]
                //placesTableViewCell.textLabel?.text = clkGooglePlaceResult.name
                //placesTableViewCell.detailTextLabel?.text = "\(clkGooglePlaceResult.types)"
                placesTableViewCell.labelTitle?.text = clkGooglePlaceResult.name
                
                //------------------------------------------------------------------------------------------------
                //ADDRESS
                //------------------------------------------------------------------------------------------------
                //use "vicinity" : "East Smithfield, London"
                //formatted_address can be nil
                //------------------------------------------------------------------------------------------------
                placesTableViewCell.labelDetail?.text = clkGooglePlaceResult.vicinity
                
                //------------------------------------------------------------------------------------------------
                //TYPE
                //------------------------------------------------------------------------------------------------
                let locationTypeName = clkGooglePlaceResult.clkGooglePlacesTypeArrayString
                //placesTableViewCell.labelDetail?.text = "[\(locationTypeName)]:\(clkGooglePlaceResult.types)"
                placesTableViewCell.labelSubDetail?.text = "\(locationTypeName)"
                //------------------------------------------------------------------------------------------------
                //------------------------------------------------------------------------------------------------
                //DISTANCE to map center
                //------------------------------------------------------------------------------------------------
                
                if clkGooglePlaceResult.distanceToMapCenter == GoogleMapsDistance.NotFound{
                    placesTableViewCell.labelDistanceMeters?.text = ""
                }else{
                    //------------------------------------------------------------------------------------------------
                    //2 DECIMAL PLACES
                    //------------------------------------------------------------------------------------------------
                    let numberFormatter            = NumberFormatter()
                    numberFormatter.numberStyle    = NumberFormatter.Style.decimal
                    numberFormatter.minimumFractionDigits = 2
                    numberFormatter.maximumFractionDigits = 2
                    
                    if let distanceToMapCenterString = numberFormatter.string(from: NSNumber(value: clkGooglePlaceResult.distanceToMapCenter)){
                        placesTableViewCell.labelDistanceMeters?.text = "\(distanceToMapCenterString) m"
                    }else{
                        placesTableViewCell.labelDistanceMeters?.text = " "
                    }
                }
                
                //------------------------------------------------------------------------------------------------
                //MAP PIN - number
                //------------------------------------------------------------------------------------------------
                
                //should never happen - made red so we can see issues
                
                if let clkGooglePlacesTypeMainType = clkGooglePlaceResult.clkGooglePlacesTypeMainType{
                    
                    if clkGooglePlacesTypeMainType.isOKToShowTypeName{
                        placesTableViewCell.labelTitle?.backgroundColor = UIColor.clear
                    }else{
                        //placesTableViewCell.labelTitle?.backgroundColor = UIColor.redColor()
                        placesTableViewCell.labelTitle?.backgroundColor = UIColor.clear
                        logger.error("SHOULD NOT SHOW THIS TYPE")
                    }
                    
                }else{
                    logger.error("clkGooglePlaceResult.clkGooglePlacesTypeMainType is nil")
                    
                }
                
                //------------------------------------------------------------------------------------------------
                //MAP PIN NUMBER
                //------------------------------------------------------------------------------------------------
                //sort is done by sorting the whole array after ws returns
                let pinText = "\(((indexPath as NSIndexPath).row) + 1)" // + 1 so it starts at 1
                
                //placesTableViewCell.imageViewPlaceResult.image = mapPinImage(pinText, fillColor: clkGooglePlaceResult.colorForType(), textColor: UIColor.whiteColor())
                
                
                placesTableViewCell.imageViewPlaceResult.image = clkGooglePlaceResult.mapPinImageWithText(pinText)

                
                
                
            }else{
                //NOISY logger.error("self.nearbySearch_CLKGooglePlaceResultsArray is nil")
            }
            cell = placesTableViewCell

        }
        else if tableView == self.tableViewSearch
        {
            let predictionTableViewCell : PredictionTableViewCell = self.tableViewSearch.dequeueReusableCell(withIdentifier: "PredictionTableViewCell", for: indexPath) as! PredictionTableViewCell

            if let placeAutoComplete_CLKGooglePlaceAutocompletePredictionArray = self.placeAutoComplete_CLKGooglePlaceAutocompletePredictionArray{
                
                let clkPlaceAutocompletePrediction : CLKGooglePlaceAutocompletePrediction = placeAutoComplete_CLKGooglePlaceAutocompletePredictionArray[(indexPath as NSIndexPath).row]
                
                predictionTableViewCell.labelTitle?.text = clkPlaceAutocompletePrediction.description
                
                //------------------------------------------------------------------------------------------------
                //TYPE
                //------------------------------------------------------------------------------------------------
                var locationTypeName = clkPlaceAutocompletePrediction.clkGooglePlacesTypeArrayString
                //remove "Geocode"
                locationTypeName = locationTypeName.replace("Geocode", with: "Place")
                
        
                //placesTableViewCell.labelDetail?.text = "[\(locationTypeName)]:\(clkGooglePlaceResult.types)"
                predictionTableViewCell.labelDetail?.text = "\(locationTypeName)"
                //------------------------------------------------------------------------------------------------

            }else{
                logger.error("self.placeAutoComplete_CLKGooglePlaceAutocompletePredictionArray is nil")
            }
            cell = predictionTableViewCell
        }
        else{
            logger.error("UNHANDLED TABLEVIEW")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        var heightForRow : CGFloat = 72.0
        
        if tableView == self.tableViewPlacesResults{
            //heightForRow = 72.0
            heightForRow = UITableView.automaticDimension
        }
        else if tableView == self.tableViewSearch{
            heightForRow = 58
            
        }
        else{
            logger.error("UNHANDLED TABLEVIEW")
        }
        
        return heightForRow
    }
    
    
    //--------------------------------------------------------------
    // MARK: - TABLE DID SELECT ROW
    // MARK: -
    //--------------------------------------------------------------

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if tableView == self.tableViewPlacesResults{
            if let nearbySearch_CLKGooglePlaceResultsArray = self.nearbySearch_CLKGooglePlaceResultsArray{
                let clkGooglePlaceResult : CLKGooglePlaceResult = nearbySearch_CLKGooglePlaceResultsArray[(indexPath as NSIndexPath).row]
                logger.debug("ROW TAPPED: clkGooglePlaceResult:\(clkGooglePlaceResult) LOC:clkGooglePlaceResult:\(String(describing: clkGooglePlaceResult.geometry?.location?.lat)), \(String(describing: clkGooglePlaceResult.geometry?.location?.lng))")
                
                //tap on row should move to pin but not call ws again
                self.mapMovedOkToCallWS = false
                
                //Screen appears - dont move map, tap on business in table, map moved to the pin but
                //when location received it jumps back to users position 
                //this stops this
                //if user taps a business in table, map will move to business and all new iphone loc dont move the map
                self.userDidChangeCameraPosition = true
                //get the place detail
                self.userSelectedResultOnMapOrTable(clkGooglePlaceResult)
                
            }else{
                //NOISY logger.error("self.nearbySearch_CLKGooglePlaceResultsArray is nil")
            }

        }
        else if tableView == self.tableViewSearch{
            //hide keyboard and search results
            self.textFieldAddSearch.resignFirstResponder()
            if let placeAutoComplete_CLKGooglePlaceAutocompletePredictionArray = self.placeAutoComplete_CLKGooglePlaceAutocompletePredictionArray{
                
                
                
//                let clkPlaceAutocompletePrediction : CLKGooglePlaceAutocompletePrediction = placeAutoComplete_CLKGooglePlaceAutocompletePredictionArray[indexPath.row - 1]

                let clkPlaceAutocompletePrediction : CLKGooglePlaceAutocompletePrediction = placeAutoComplete_CLKGooglePlaceAutocompletePredictionArray[(indexPath as NSIndexPath).row]
                if let place_id = clkPlaceAutocompletePrediction.place_id{
                    self.callWS_get_place_detail(place_id)
                }else{
                    logger.error("clkPlaceAutocompletePrediction.place_id is nil")
                }
                
            }else{
                logger.error("self.placeAutoComplete_CLKGooglePlaceAutocompletePredictionArray is nil")
            }

        }
        else{
            logger.error("UNHANDLED TABLEVIEW")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - USE AND RETURN ADDRESS
    //--------------------------------------------------------------
    
    @IBAction func buttonUsePlace_Action(_ sender: AnyObject) {
        
        logger.debug("buttonUsePlace_Action ")
        
       returnCLKPlaceSelectedThroughDelegate()
    }
    
    
    func returnCLKPlaceSelectedThroughDelegate(){
        if let clkGooglePlaceSelected = self.clkGooglePlaceSelected {
            
            let clkPlacesPickerResultNearbyGooglePlace = CLKPlacesPickerResultNearbyGooglePlace()
            clkPlacesPickerResultNearbyGooglePlace.clkGooglePlaceSelected = clkGooglePlaceSelected
            clkPlacesPickerResultNearbyGooglePlace.clkLocationPlaceMapCenter = self.clkLocationPlaceMapCenter
            returnThroughDelegate(clkPlacesPickerResultNearbyGooglePlace)
            
        }else{
            logger.error("returnCLKPlaceSelectedThroughDelegate self.delegate is nil")
            
        }
    }
    
    
    @IBAction func buttonUseNearestAddress_Action(_ sender: AnyObject) {

        //---------------------------------------------------------------------
        //may not be geocoded yet - failed often on iOS10
        if self.clkLocationPlaceMapCenter.clLocationIsValid{
            
            //---------------------------------------------------------------------
            self.clkGooglePlaceSelected = nil
            
            //---------------------------------------------------------------------
            let clkPlacesPickerResultNearbyStreetAddress = CLKPlacesPickerResultNearbyStreetAddress()
            clkPlacesPickerResultNearbyStreetAddress.clkLocationPlaceSelected = self.clkLocationPlaceMapCenter
            
            //---------------------------------------------------------------------
            returnThroughDelegate(clkPlacesPickerResultNearbyStreetAddress)
            //---------------------------------------------------------------------
        }else{
            logger.error("self.clkLocationPlaceMapCenter.clLocationIsValid is false - dont return to LocationVC")
        }
        //---------------------------------------------------------------------
    }
    
    func returnThroughDelegate(_ clkPlacesPickerResult: CLKPlacesPickerResult){
        
        if let delegate = self.delegate {
            delegate.colcPlacePickerViewController_clkPlacesPickerResult(self, clkPlacesPickerResult: clkPlacesPickerResult)
            
        }else{
            logger.error("clkPlaceSelected is nil")
        }
    }
    
    
    
    func showHelpAlert(){
        CLKAlertController.showAlertInVC(self, title: "Choose Nearby Business or Street Address", message: "The map and list show all businesses near the center of the map.\r Tap a business on the map or the list to use its business address.\rNEAREST STREET ADDRESS\rOr tap on Nearest Street Address to be picked up at the street address closest to the map center.\rMOVE MAP\rMove the map or pinch-to-zoom to find other businesses nearby or worldwide.\rUSE STREET\rClick on 'Use Street' to combine the business name / street address to be picked up outside a business.\rGOOGLE SEARCH\rUse the search box to type any worldwide location or business name/address to perform a Google Maps search. (Provide as much info as you can of the name and/or address for a more accurate result)")
    }
    @IBAction func buttonShowHelp_Action(_ sender: AnyObject) {
        showHelpAlert()
        
    }
}
