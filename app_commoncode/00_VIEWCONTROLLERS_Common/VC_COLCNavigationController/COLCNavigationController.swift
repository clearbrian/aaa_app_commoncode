//
//  COLCNavigationController.swift
//  joyride
//
//  Created by Brian Clear on 27/11/2015.
//  Copyright © 2015 City Of London Consulting Limited. All rights reserved.
//
import UIKit

class COLCNavigationController: UINavigationController{
    //i think all tabs in the TABController must have same bith at this Nav controllers
    override var preferredStatusBarStyle : UIStatusBarStyle {
        //return .Default
        return AppearanceManager.preferredStatusBarStyle
    }
}
