//
//  LocationPickerViewController
//  joyride
//
//  Created by Brian Clear on 14/01/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import GoogleMaps
protocol LocationPickerViewControllerDelegate {

    func locationPickerViewController(_ locationPickerViewController: LocationPickerViewController, clkPlacesPickerResult: CLKPlacesPickerResult)
    
    func locationPickerViewControllerCancelled(_ locationPickerViewController: LocationPickerViewController)
    
}
// TODO: - COCOAPODS
class LocationPickerViewController: ParentViewController{
    
}
//class LocationPickerViewController: ParentViewController,
//    COLCPlacePickerViewControllerDelegate,
//    ContactsViewControllerDelegate,
//    SelectPlaceViewControllerDelegate,
//    FBViewControllerDelegate,
//    COLCCarbonTabSwipeNavigationViewControllerInputsDelegate,
//    CalendarEventsViewControllerDelegate
//{
//
//    var colcCarbonTabSwipeNavigationViewControllerInputs : COLCCarbonTabSwipeNavigationViewControllerInputs?
//    //---------------------------------------------------------------------
//    var delegate: LocationPickerViewControllerDelegate?{
//        didSet {
//            print(" Old:[\(oldValue)], New:\(delegate)")
//
//        }
//    }
//    //---------------------------------------------------------------------
//    @IBOutlet weak var view_labelAddress: UIView!
//    @IBOutlet weak var labelAddress: UILabel!
//
//    @IBOutlet weak var buttonUseAddress: UIButton!
//
//    //--------------------------------------------------------------
//    // MARK: - switchUseStreet
//    // MARK: -
//    //--------------------------------------------------------------
//    @IBOutlet weak var viewUseStreet: UIView!
//    @IBOutlet weak var switchUseStreet: UISwitch!
//
//    @IBAction func switchUseStreet_ValueChanged(_ sender: AnyObject) {
//
//        updateNameAddressFromResult()
//    }
//
//
//
//    //-----------------------------------------------------------------------------------
//    //user can tap on START/EDIT and it will edit
//    //passed in from TripPlannerVc > LocationPicker >> COLCCarbonTabSwipeNavigationViewControllerInputs >> colcPlacePickerViewController
//
//    // TODO: - remove?
//    var clkPlaceSelected: CLKPlace?{
//
//        //---------------------------------------------------------------------
//        if let clkPlacesPickerResult = self.clkPlacesPickerResult {
//            if let clkPlaceSelectedReturned = clkPlacesPickerResult.clkPlaceSelectedReturned {
//                return clkPlaceSelectedReturned
//            }else{
//                logger.error("clkPlacesPickerResult.clkPlaceSelectedReturned is nil")
//                return nil
//            }
//        }else{
//            logger.error("self.clkPlacesPickerResult is nil")
//            return nil
//        }
//        //---------------------------------------------------------------------
//    }
//
//
//    //--------------------------------------------------------------
//    // MARK: - CLKPlacesPickerResult
//    // MARK: -
//    //--------------------------------------------------------------
//
//    var clkPlacesPickerResult: CLKPlacesPickerResult?{
//        didSet {
//            //noisy print("[clkPlaceSelected]7 didSet: Old value: '\(oldValue)', clkPlaceSelected['\(clkPlaceSelected?.name_formatted_address)']")
//
//            //---------------------------------------------------------------------
//            showHideSwitch()
//            updateNameAddressFromResult()
//            //---------------------------------------------------------------------
//        }
//    }
//
//
//    func showHideSwitch(){
//        if let clkPlacesPickerResult = self.clkPlacesPickerResult {
//            if clkPlacesPickerResult.hasOtherAddress{
//                //self.switchUseStreet.isOn =
//                show_viewUseStreet()
//            }else{
//                hide_viewUseStreet()
//                self.switchUseStreet.isOn = false
//
//            }
//        }else{
//            hide_viewUseStreet()
//            self.switchUseStreet.isOn = false
//        }
//    }
//
//    func updateNameAddressFromResult(){
//
//        if let clkPlacesPickerResult = self.clkPlacesPickerResult {
//            //---------------------------------------------------------------------
//            //Called in LocationPickerVC and TripPlanner VC
//            self.labelAddress.attributedText = clkPlacesPickerResult.attributedString_name_formatted_address(useGeocoded: self.switchUseStreet.isOn)
//            //---------------------------------------------------------------------
//        }else{
//            logger.error("self.clkPlacesPickerResult is nil")
//        }
//    }
//
//    func attributedString(nameString: String, addressString: String) -> NSAttributedString{
//        let nameCommaString = "\(nameString), "
//        let nameCommaAttributedString = NSAttributedString(string: nameCommaString)
//
//        //---------------------------------------------------------------------
//        var attributesArray = [String : NSObject]()
//        attributesArray[NSForegroundColorAttributeName] = UIColor.purple
//        //---------------------------------------------------------------------
//        let addressAttributedString = NSAttributedString(string: addressString, attributes: attributesArray)
//
//        //---------------------------------------------------------------------
//        let finalAttributedString = NSMutableAttributedString(attributedString: nameCommaAttributedString)
//        finalAttributedString.append(addressAttributedString)
//        //---------------------------------------------------------------------
//        return finalAttributedString
//    }
//
//
//    //--------------------------------------------------------------
//    // MARK: - viewDidLoad
//    // MARK: -
//    //--------------------------------------------------------------
//
//    override func viewDidLoad() {
//
//        super.viewDidLoad()
//
//        self.view_labelAddress.layer.cornerRadius = 4.0
//
//        //---------------------------------------------------------------------
//
//        //self.buttonUseAddress.applyCustomFontForCurrentTextStyle()
//        //self.buttonUseAddressGeocoded.applyCustomFontForCurrentTextStyle()
//        //---------------------------------------------------------------------
//        self.updateLabelsForTab(0)
//        //---------------------------------------------------------------------
//        //moved up to all parents
//        //to blend in behind the status bar
//        self.view.backgroundColor = AppearanceManager.appColorNavbarAndTabBar
//        //---------------------------------------------------------------------
//
//        if let colcCarbonTabSwipeNavigationViewControllerInputs = self.colcCarbonTabSwipeNavigationViewControllerInputs{
//
//           //------------------------------------------------------------------------------------------------
//            //SWIPE style
//            //------------------------------------------------------------------------------------------------
//            //self.labelAddress.applyCustomFontForCurrentTextStyle()
//            colcCarbonTabSwipeNavigationViewControllerInputs.carbonSegmentedControl?.applyCustomFontForCurrentTextStyle()
//            //------------------------------------------------------------------------------------------------
//            ////see PLACEPICKER_TABCOLOR : SELECTED COLOR WHEN YOU TAP ON TAB
//            //seg control is transparent for the colcCarbonTabSwipeNavigationViewControllerInputs
//            //can use same style as all other seg controls
//            //see PLACEPICKER_TABCOLOR : SELECTED
//            colcCarbonTabSwipeNavigationViewControllerInputs.carbonSegmentedControl?.setTitleTextAttributes([NSForegroundColorAttributeName: AppearanceManager.appColorLight_SelectedColor],
//                                                                                                                  for: .selected)
//            //------------------------------------------------------------------------------------------------
//            colcCarbonTabSwipeNavigationViewControllerInputs.delegate_ContainerVC = self
//            //------------------------------------------------------------------------------------------------
//            //PASS Selected item DOWN
//            //------------------------------------------------------------------------------------------------
//            if let clkPlaceSelected = self.clkPlaceSelected {
//                colcCarbonTabSwipeNavigationViewControllerInputs.clkPlaceSelected = clkPlaceSelected
//
//            }else{
//                logger.error("self.clkPlaceSelected is nil 888")
//            }
//            //---------------------------------------------------------------------
//
//        }else{
//            logger.error("colcCarbonTabSwipeNavigationViewControllerInputs is nil")
//        }
//        //------------------------------------------------------------------------------------------------
//    }
//
//
//
//    //--------------------------------------------------------------
//    // MARK: - TAB 1 - NEARBY - COLCPlacePickerViewControllerDelegate
//    // MARK: -
//    //--------------------------------------------------------------
//
//    // TODO: - have one delegate for all tabs
//    func colcPlacePickerViewController_clkPlacesPickerResult(_ colcPlacePickerViewController: COLCPlacePickerViewController, clkPlacesPickerResult: CLKPlacesPickerResult){
//
//        self.clkPlacesPickerResult = clkPlacesPickerResult
//
//    }
//    //cant cancel its in swipe nav
//    func colcPlacePickerViewControllerCancelled(_ colcPlacePickerViewController: COLCPlacePickerViewController){
//        logger.info("colcPlacePickerViewControllerCancelled:)")
//
//    }
//
//    //--------------------------------------------------------------
//    // MARK: - - TAB 2 CONTACTS - ContactsViewControllerDelegate
//    // MARK: -
//    //--------------------------------------------------------------
//    func pickContactAddressReturned(_ contactsViewController: ContactsViewController, clkPlacesPickerResultPlace: CLKPlacesPickerResultPlace){
//
//        self.clkPlacesPickerResult = clkPlacesPickerResultPlace
//
//    }
//
//    func contactsViewControllerCancelled(_ contactsViewController: ContactsViewController){
//        print("contactsViewControllerCancelled:")
//    }
//
//    //--------------------------------------------------------------
//    // MARK: - TAB : CALENDAR - CalendarEventsViewControllerDelegate
//    // MARK: -
//    //--------------------------------------------------------------
//    func controllerDidSelectCalendarEvent(_ calendarEventsViewController: CalendarEventsViewController, clkPlacesPickerResultPlace: CLKPlacesPickerResultPlace){
//
//        self.clkPlacesPickerResult = clkPlacesPickerResultPlace
//
//    }
//    func calendarEventsViewControllerCancelled(_ calendarEventsViewController: CalendarEventsViewController){
//        print("calendarEventsViewControllerCancelled:")
//    }
//
//    //------------------------------------------------------------------------------------------------
//    // MARK: - TAB 4: FB EVENTS : FBViewControllerDelegate
//    // MARK:
//    //------------------------------------------------------------------------------------------------
//    func fbViewControllerSelectFacebookEvent(_ fbViewController: FBViewController, clkPlacesPickerResultPlace: CLKPlacesPickerResultPlace)
//    {
//        self.clkPlacesPickerResult = clkPlacesPickerResultPlace
//
//    }
//
//    func fbViewControllerCancelled(_ fbViewController: FBViewController){
//        print("fbViewControllerCancelled")
//    }
//
//
//
//    //--------------------------------------------------------------
//    // MARK: -
//    // MARK: - CALL OTHER SCREENS
//    //--------------------------------------------------------------
//
//    @IBAction func buttonCancel_Action(_ sender: AnyObject) {
//        self.dismiss(animated: true, completion: {
//            //------------------------------------------------
//            if let delegate = self.delegate {
//
//                delegate.locationPickerViewControllerCancelled(self)
//
//            }else{
//                logger.error("self.delegate_COLCPlacePickerViewControllerDelegate is nil 77657")
//            }
//            //------------------------------------------------
//        })
//    }
//
//    @IBAction func barButtonItemEdit_Action(_ sender: AnyObject) {
//
//        var placePicked = false
//
//        if let _ = self.clkPlaceSelected {
//            placePicked = true
//        }
//        else{
//            logger.error("self.clkPlaceSelected/self.colcContactSelected are nil - unhandled type")
//        }
//        if placePicked{
//            self.performSegue(withIdentifier: ksegueTOLocationEditViewController, sender: nil)
//
//        }else{
//             logger.error("no type selected")
//        }
//
//    }
//
//    //--------------------------------------------------------------
//    // MARK: -
//    // MARK: - EXIT - RETURN TO DELEGATE
//    //--------------------------------------------------------------
//    @IBAction func buttonUse_Action(_ sender: AnyObject) {
//        //------------------------------------------------
//        if let delegate = self.delegate {
//            //---------------------------------------------------------------------
//            if let clkPlacesPickerResult = self.clkPlacesPickerResult {
//
//                delegate.locationPickerViewController(self, clkPlacesPickerResult: clkPlacesPickerResult)
//                self.dismiss(animated: true, completion: {})
//            }
//            else{
//                logger.error("self.clkPlacesPickerResult is nil")
//            }
//            //---------------------------------------------------------------------
//        }else{
//            logger.error("self.delegate_COLCPlacePickerViewControllerDelegate is nil 333")
//        }
//        //------------------------------------------------
//    }
//
//    func showHelpAlert(){
//          CLKAlertController.showAlertInVC(self, title: "Please choose an address", message: "Choose an address from the sources shown.\rSwipe the tabs at the top to see other sources.\rThe address to be sent to Uber will appear in the field at the bottom of the screen.\rPress 'Use' again to return the chosen address to Start or Destination field in the Trip Planner")
//    }
//
//    //--------------------------------------------------------------
//    // MARK: - USER CHANGED TAB - COLCCarbonTabSwipeNavigationViewControllerInputsDelegate
//    // MARK: -
//    //--------------------------------------------------------------
//    func willMoveAtIndex(_ colcCarbonTabSwipeNavigationViewControllerInputs: COLCCarbonTabSwipeNavigationViewControllerInputs, index: UInt){
//       self.updateLabelsForTab(index)
//    }
//
//    //use " " not "" else Dynamic height not set right
//    let dynamicHeightBlank = " "
//
//    //called in viewDidLoad and willMoveAtIndex
//    func updateLabelsForTab(_ index:UInt)
//    {
//        //---------------------------------------------------------------------
//        //use " " not "" else Dynamic height not set right
//        //---------------------------------------------------------------------
//
//        self.labelAddress.text = "Move map or choose from list..."
//        //---------------------------------------------------------------------
//        //turn it off in NEARBY then on in FB then back to nearby should reset to off else you see blank address
//        self.switchUseStreet.isOn = false
//
//
//        //---------------------------------------------------------------------
//        switch index{
//        case 0:
//            //--------------------------------------------------------
//            self.labelAddress.text = "Move map or choose from list..."
//
//        case 1:
//            self.labelAddress.text = "Tap the button above to view your contacts"
//            //---------------------------------------------------------------------
//            hide_viewUseStreet()
//            //---------------------------------------------------------------------
//            //when you hide turn off so it uses clkSelected else can be blank in text field
//            self.switchUseStreet.isOn = false
//            //---------------------------------------------------------------------
//        case 2:
//            print("willMoveAtIndex:\(index) - FB EVENTS")
//             self.labelAddress.text = dynamicHeightBlank
//
//            //---------------------------------------------------------------------
//            show_viewUseStreet()
//
//            //---------------------------------------------------------------------
//            //when you hide turn off so it uses clkSelected else can be blank in text field
//            self.switchUseStreet.isOn = false
//            //---------------------------------------------------------------------
//        case 3:
//            print("willMoveAtIndex:\(index) - RECENT PLACES")
//            self.labelAddress.text = dynamicHeightBlank
//            hide_viewUseStreet()
//
//        case 4:
//            print("willMoveAtIndex:\(index) - CALENDAR EVENTS")
//            //---------------------------------------------------------------------
//            self.labelAddress.text = dynamicHeightBlank
//            //---------------------------------------------------------------------
//            hide_viewUseStreet()
//
//        default:
//            print("willMoveAtIndex:\(index) - ERROR UNHANDLED EVENTS")
//        }
//    }
//
//
//
//    func show_viewUseStreet(){
//        self.viewUseStreet.isHidden = false
//    }
//    func hide_viewUseStreet(){
//        self.viewUseStreet.isHidden = true
//    }
//
//    let ksegueTOLocationEditViewController = "segueTOLocationEditViewController"
//    //--------------------------------------------------------------
//    // MARK: -
//    // MARK: - SEGUE
//    //--------------------------------------------------------------
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//
//        if(segue.identifier == "embedCOLCCarbonTabSwipeNavigationViewControllerInputs"){
//            //------------------------------------------------------------------------------------------------
//            //not needed appDelegate.searchViewController lets logout to be called from any VC via delegate
//
//            if segue.destination.isMember(of: COLCCarbonTabSwipeNavigationViewControllerInputs.self){
//                self.colcCarbonTabSwipeNavigationViewControllerInputs = (segue.destination as! COLCCarbonTabSwipeNavigationViewControllerInputs)
//
//                //PLACES PICKER
//                self.colcCarbonTabSwipeNavigationViewControllerInputs?.delegate_COLCPlacePickerViewControllerDelegate = self
//
//                //CONTACTS
//                self.colcCarbonTabSwipeNavigationViewControllerInputs?.delegate_ContactsViewControllerDelegate = self
//
//                //CALENDAR
//                self.colcCarbonTabSwipeNavigationViewControllerInputs?.delegate_CalendarEventsViewControllerDelegate = self
//
//                //SAVE PLACES
//                self.colcCarbonTabSwipeNavigationViewControllerInputs?.delegate_SelectPlaceViewControllerDelegate = self
//
//                //FB
//                self.colcCarbonTabSwipeNavigationViewControllerInputs?.delegate_FBViewControllerDelegate = self
//
//            }else{
//                logger.error("VC in swgue 'embedCalculatorMenuTableViewController' is not of type COLCCarbonTabSwipeNavigationViewControllerInputs")
//            }
//            //------------------------------------------------------------------------------------------------
//        }
//        else{
//            logger.error("UNHANDLED SEGUE:\(segue.identifier)")
//        }
//    }
//
//
//    //--------------------------------------------------------------
//    // MARK: - TAB 3 : RECENT PLACES - SelectPlaceViewControllerDelegate
//    // MARK: -
//    //--------------------------------------------------------------
//    func controllerDidSelectPlace(_ selectPlaceViewController: SelectPlaceViewController, realmPlace: RealmPlace){
//        print("controllerDidSelectPlace:\(realmPlace)")
//        //---------------------------------------------------------------------
//        //RealmPlace - need to convert to CLKPlace or one of its subclasses
//        //---------------------------------------------------------------------
//        //     controllerDidSelectPlace:Place {
//        //     name = Get Licensed;
//        //     address = Tower Bridge Business Center, 46-48 E Smithfield, Bank E1W 1AW, United Kingdom;
//        //     place_id = ChIJZa-dB6tydkgR-ct8ncb30vA;
//        //     latitude = 51.50871000000001;
//        //     longitude = -0.06937600000000001;
//        //     }
//        //------------------------------------------------------------------------------------------------
//        //is this needed - wasnt it done when we saved to db
//        if realmPlace.place_id != ""{
//
//            logger.error("TODO: CONVERT REALM to CLKPlace subclass then reassign clkPlace.clkGooglePlaceResult")
//            // TODO: - convert any DB back to a CLKPlace subclass and use that
//            //            self.get_place_detail(realmPlace,
//            //                                  success:{
//            //                                    (clkGooglePlaceResultWithDetail: CLKGooglePlaceResult)->Void in
//            //                                    //---------------------------------------------------------------------
//            //
//            //                                    logger.info("clkPlaceSearchResponse returned - with better address - store in clkPlaceSelected:[clkGooglePlaceResultWithDetail.formatted_address:\(clkGooglePlaceResultWithDetail.formatted_address)]")
//            //                                    //-----------------------------------------------------------------------------------
//            //                                    //if we cant get improved address as user if they want to use GEOCODE address
//            //                                    //Issue: could still be crap address after getting detail
//            //                                    //---------------------------------------------------------------------
//            //                                    if let formatted_address = clkGooglePlaceResultWithDetail.formatted_address {
//            //                                        logger.info("formatted_address AFTER get Detail:\(formatted_address)")
//            //                                    }else{
//            //                                        //---------------------------------------------------------------------
//            //                                        //may never happed
//            //                                        //"London UK" >> get Details >> "E1 8lg London UK"
//            //                                        //---------------------------------------------------------------------
//            //                                        logger.error("clkGooglePlaceResultWithDetail.formatted_address is nil: getting more detailed address failed - can happen for bus stops")
//            //                                        //---------------------------------------------------------------------
//            //                                        CLKAlertController.showAlertInVCWithCallback(self, title: "Error", message: "Could not get detailed address for this location")
//            //                                        { () -> Void in
//            //                                            logger.info("Ok tapped")
//            //                                        }
//            //                                    }
//            //
//            //
//            //                                    //---------------------------------------------------------------------
//            //                                    let clkPlace = CLKPlace()
//            //                                    clkPlace.clkPlaceWrapperType = .savedPlace_CLKGooglePlaceResult
//            //                                    clkPlace.clkGooglePlaceResult = clkGooglePlaceResultWithDetail
//            //                                    self.clkPlaceSelected = clkPlace
//            //                                    //--------------------------------------------------------
//            //                                    self.clkPlaceSelectedGeocodeLocation = nil
//            //                                    //---------------------------------------------------------------------
//            //                                    //UIApplication.shared.isNetworkActivityIndicatorVisible = false
//            //                                    //---------------------------------------------------------------------
//            //                },
//            //                failure:{(error) -> Void in
//            //                        //---------------------------------------------------------------------
//            //                        logger.error("error:\(error)")
//            //
//            //                        CLKAlertController.showAlertInVCWithCallback(self, title: "Error", message: error?.localizedDescription)
//            //                        { () -> Void in
//            //                            logger.info("Ok tapped = alert closed appDelegate.doLogout()")
//            //                        }
//            //                        //UIApplication.shared.isNetworkActivityIndicatorVisible = false
//            //                        //---------------------------------------------------------------------
//            //                }
//            //            )
//
//        }else{
//            //------------------------------------------------
//            //realmPlace.place_id IS "" e.g. GeocodedLocation
//            //------------------------------------------------
//
//            //            self.clkPlaceSelected = CLKPlace(realmPlace:realmPlace)
//            //            // TODO: - shoudl be rebuilt from DB
//            //            self.clkPlaceSelected?.clkPlaceWrapperType = .CLKPlace_Default//.SavedPlace_CLKGooglePlaceResult
//            //            //self.display_clkPlaceSelected()
//            //---------------------------------------------------------------------
//            //            let clkPlaceFromDB = CLKPlace(realmPlace:realmPlace)
//            //            // TODO: - WRONG - type is loaded from db?
//            //            clkPlaceFromDB.clkPlaceWrapperType = .clkPlace_Default//.SavedPlace_CLKGooglePlaceResult
//            //            self.clkPlaceSelected = clkPlaceFromDB
//            //// TODO: - CLEANUP after test
//            ////            self.clkPlaceSelectedGeocodeLocation = nil
//            logger.error("LOAD realm db into CLKPlace is off 398798792")
//
//        }
//        //------------------------------------------------------------------------------------------------
//        //---------------------------------------------------------------------
//        let clkPlacesPickerResultPlace = CLKPlacesPickerResultPlace()
//        let clkPlaceFromDB = CLKPlace(realmPlace:realmPlace)
//        clkPlacesPickerResultPlace.clkPlaceSelected = clkPlaceFromDB
//
//
//        //delegate.fbViewControllerSelectFacebookEvent(self, clkPlacesPickerResultPlace:clkPlacesPickerResultPlace)
//        //---------------------------------------------------------------------
//
//
//        //            // TODO: - WRONG - type is loaded from db?
//        //            clkPlaceFromDB.clkPlaceWrapperType = .clkPlace_Default//.SavedPlace_CLKGooglePlaceResult
//        //self.clkPlaceSelected = clkPlaceFromDB
//
//        self.clkPlacesPickerResult = clkPlacesPickerResultPlace
//
//
//    }
//
//    // TODO: - MOVE INTO RealmDB class?
//    //--------------------------------------------------------------
//    // MARK: - get_place_detail - realmPlace
//    // MARK: -
//    //--------------------------------------------------------------
//    //takes CLKGooglePlaceResult returns same result but address is better
//    func get_place_detail(_ realmPlace: RealmPlace,
//                          success: @escaping (_ clkGooglePlaceResultWithDetail: CLKGooglePlaceResult) -> Void,
//                          failure: (_ error: Error?) -> Void
//        )
//    {
//        // TODO: - do inside CLKGooglePlace
//        if realmPlace.place_id != ""
//        {
//            self.get_place_detail(realmPlace.place_id,
//                                  success:{
//                                    (clkPlaceSearchResponse: CLKGooglePlaceSearchResponse?)->Void in
//                                    //---------------------------------------------------------------------
//                                    if let clkPlaceSearchResponse = clkPlaceSearchResponse{
//                                        logger.info("clkPlaceSearchResponse returned")
//
//                                        if let clkGooglePlaceResultReturned = clkPlaceSearchResponse.result{
//                                            success(clkGooglePlaceResultReturned)
//
//                                        }
//                                        else if let results = clkPlaceSearchResponse.results{
//                                            logger.error("more than one result foudn for get_place_detail:\(results)")
//                                        }
//                                        else
//                                        {
//                                            logger.error("clkPlaceSearchResponse.result/clkPlaceSearchResponse.results is nil")
//                                        }
//                                    }else{
//                                        logger.error("clkPlaceSearchResponse is nil or not CLKGooglePlaceSearchResponse")
//                                    }
//                                    //---------------------------------------------------------------------
//                                    //UIApplication.shared.isNetworkActivityIndicatorVisible = false
//                },
//                                  failure:{
//                                    (error) -> Void in
//                                    //------------------------------------------------------------------------------------------------
//                                    logger.error("error:\(error)")
//
//                                    CLKAlertController.showAlertInVCWithCallback(self, title: "Error", message: error?.localizedDescription)
//                                    { () -> Void in
//                                        logger.info("Ok tapped = alert closed appDelegate.doLogout()")
//                                    }
//                                    //UIApplication.shared.isNetworkActivityIndicatorVisible = false
//                                    //------------------------------------------------------------------------------------------------
//
//                }
//            )
//        }else{
//            logger.error("realmPlace.place_id is ''")
//        }
//    }
//    //--------------------------------------------------------------
//    // MARK: - get_place_detail - placeIdString
//    // MARK: -
//    //--------------------------------------------------------------
//    func get_place_detail(_ placeIdString: String,
//                          success: @escaping (_ clkPlaceSearchResponse: CLKGooglePlaceSearchResponse?) -> Void,
//                          failure: @escaping (_ error: Error?) -> Void){
//
//        //UIApplication.shared.isNetworkActivityIndicatorVisible = true
//        //---------------------------------------------------------------------
//        appDelegate.googlePlacesController.googlePlacesWSController.get_place_detail(placeIdString,
//                                                                                     success:{
//                                                                                        (clkPlaceSearchResponse: CLKGooglePlaceSearchResponse?)->Void in
//                                                                                        //---------------------------------------------------------------------
//                                                                                        if let clkPlaceSearchResponse = clkPlaceSearchResponse{
//                                                                                            logger.info("clkPlaceSearchResponse returned")
//
//                                                                                            // success(clkPlaceSearchResponse: clkPlaceSearchResponse)
//                                                                                            success( clkPlaceSearchResponse)
//
//                                                                                        }else{
//                                                                                            logger.error("clkPlaceSearchResponse is nil or not CLKGooglePlaceSearchResponse")
//                                                                                            // TODO: - CREATE ERROR
//                                                                                            //failure(error: nil)
//                                                                                            failure(nil)
//                                                                                        }
//                                                                                        //---------------------------------------------------------------------
//                                                                                        //UIApplication.shared.isNetworkActivityIndicatorVisible = false
//            },
//                                                                                     failure:{
//                                                                                        (error) -> Void in
//                                                                                        logger.error("error:\(error)")
//
//                                                                                        CLKAlertController.showAlertInVCWithCallback(self, title: "Error", message: error?.localizedDescription)
//                                                                                        { () -> Void in
//                                                                                            logger.info("Ok tapped = alert closed appDelegate.doLogout()")
//                                                                                        }
//                                                                                        //UIApplication.shared.isNetworkActivityIndicatorVisible = false
//            }
//        )
//        //---------------------------------------------------------------------
//    }
//
//
//
//
//    //--------------------------------------------------------------
//    // MARK: -
//    // MARK: - LIFECYCLE
//    //--------------------------------------------------------------
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//
//
//
//
//
//
//}

