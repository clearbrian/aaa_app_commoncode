//
//  HomeScreenViewController.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 17/01/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
import UIKit
class HomeScreenViewController: ParentViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    let segueHomeScreenViewControllerTOGenericListViewController = "segueHomeScreenViewControllerTOGenericListViewController"
    @IBAction func buttonTaxiRankList_Action(_ sender: Any) {
        performSegue(withIdentifier: segueHomeScreenViewControllerTOGenericListViewController, sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
//        if(segue.identifier == segueHomeScreenViewControllerTOGenericListViewController){
//            
//            if(segue.destination.isMember(of: GenericListViewController.self)){
//                
//                let genericListViewController = (segue.destination as! GenericListViewController)
//                
////                rangeDatePickerController.delegate = self
////                rangeDatePickerController.dateFrom = self.eventKitFrameworkManager.startDate
////                rangeDatePickerController.dateTo = self.eventKitFrameworkManager.endDate
//                
//            }else{
//                print("ERROR:not AddTripViewController")
//            }
//        }
//        else{
//            print("UNHANDLED SEGUE:\(segue.identifier)")
//        }
    }
    
}
