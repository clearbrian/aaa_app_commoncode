//
//  TripPlannerViewController.swift
//  joyride
//
//  Created by Brian Clear on 21/01/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit
//import SafariServices
import GoogleMaps

let ksegueTOLocationPickerViewController = "segueTripPlannerViewControllerTOLocationPickerViewController"
let ksegueTOOpenWithViewController = "segueTripPlannerViewControllerTOOpenWithViewController"

class TripPlannerViewController: ParentViewController, LocationPickerViewControllerDelegate, EditCLKPlaceAddressViewControllerDelegate {
    
    //inti in viewDidLoad AFTER mapView and this VC fully init'd
    var colcGoogleDirectionsUIManager : COLCGoogleDirectionsUIManager?
    //---------------------------------------------------------------------
    @IBOutlet weak var labelAddressStart: UILabel!
    @IBOutlet weak var labelAddressEnd: UILabel!
    //---------------------------------------------------------------------
    @IBOutlet weak var mapView: GMSMapView!
    //---------------------------------------------------------------------
    @IBOutlet weak var viewButtonUber: UIView!
    @IBOutlet weak var buttonOpenIn: UIButton!
    //---------------------------------------------------------------------
    //when picker returns this is Start or End
    var currentPlaceType: PlaceType = .Start
    
    
    var markerStart : GMSMarkerPop?
    var markerEnd : GMSMarkerPop?
    

    @IBOutlet weak var viewFROM: UIView!
    @IBOutlet weak var viewTO: UIView!
    
    @IBOutlet weak var viewSWAP: UIView!
   
    
    //------------------------------------------------------------------------------------------------
    @IBOutlet weak var viewFROMTO: UIView!
    
    var userDidChangeCameraPosition : Bool = false

    //---------------------------------------------------------------------
    //hidden buttons over START/DEST fields
    @IBOutlet weak var buttonStart: UIButton!
    @IBOutlet weak var buttonDestination: UIButton!
    //---------------------------------------------------------------------
    // TODO: -  whats this for?
    var clkPlaceToEdit: CLKPlace?
    
    //---------------------------------------------------------------------
    
    //Start/End locations
    var clkPlaceStart: CLKPlace? {
        //---------------------------------------------------------------------
        if let clkPlacesPickerResultStart = self.clkPlacesPickerResultStart {
            if let clkPlaceSelectedReturned = clkPlacesPickerResultStart.clkPlaceSelectedReturned {
                return clkPlaceSelectedReturned
            }else{
                logger.error("clkPlacesPickerResultStart.clkPlaceSelectedReturned is nil")
                return nil
            }
        }else{
            logger.error("self.clkPlacesPickerResultStart is nil")
            return nil
        }
        //---------------------------------------------------------------------
    }
    
    var clkPlacesPickerResultStart: CLKPlacesPickerResult?{
        didSet {
            
            MyXCodeEmojiLogger.defaultInstance.info("clkPlacesPickerResultStart didSet: Old value: '\(oldValue)', New value: '\(clkPlacesPickerResultStart)'")
            
            if let clkPlacesPickerResultStart = clkPlacesPickerResultStart {
                
                //stop map jumping if new device location sent to this screen
                userDidChangeCameraPosition = true
                
                //---------------------------------------------------------------------
                self.labelAddressStart?.text = "\(clkPlacesPickerResultStart.name_formatted_address)"
                //---------------------------------------------------------------------
                //-------------------
                //ADD PIN TO START
                //-------------------
                if let clkPlaceSelectedReturned = clkPlacesPickerResultStart.clkPlaceSelectedReturned {
                    clkPlaceSelectedReturned.placeType = PlaceType.Start
                    self.markerStart = addPointToToMapForPlace(clkPlaceSelectedReturned, marker : self.markerStart, fallbackNameString: "Start")
                    
                }else{
                    logger.error("clkPlacesPickerResultStart.clkPlaceSelectedReturned is nil - cant add pin")
                }
                
                //---------------------------------------------------------------------
                self.zoomToPins()
                //------------------------------------------
                //self.showHideButtons()
                //------------------------------------------
                
            }else{
                logger.error("didSet: clkPlacesPickerResultStart is nil")
                self.labelAddressStart?.text = "Tap to set start location"
                //-----------------------------------------------------------------------------------
                if let markerStart = self.markerStart {
                    markerStart.map = nil
                    
                }else{
                    //noisy ok at start
                    //logger.error("self.markerStart is nil")
                }
                //-----------------------------------------------------------------------------------
            }
            
            showOrHideDirectionsOverlayFromWS()
        }
    }
    
    var clkPlaceEnd: CLKPlace? {
        
        if let clkPlacesPickerResultEnd = self.clkPlacesPickerResultEnd {
            if let clkPlaceSelectedReturned = clkPlacesPickerResultEnd.clkPlaceSelectedReturned {
                return clkPlaceSelectedReturned
            }else{
                logger.error("clkPlacesPickerResultEnd.clkPlaceSelectedReturned is nil")
                return nil
            }
        }else{
            logger.error("self.clkPlacesPickerResultEnd is nil")
            return nil
        }
    }
    
    var clkPlacesPickerResultEnd: CLKPlacesPickerResult?{
        didSet {
            
            MyXCodeEmojiLogger.defaultInstance.info("clkPlacesPickerResultEnd didSet: Old value: '\(oldValue)', New value: '\(clkPlacesPickerResultEnd)'")
            
            if let clkPlacesPickerResultEnd = clkPlacesPickerResultEnd {
                
                //stop map jumping if new device location sent to this screen
                userDidChangeCameraPosition = true
                
                //---------------------------------------------------------------------
                self.labelAddressEnd?.text = "\(clkPlacesPickerResultEnd.name_formatted_address)"
                //---------------------------------------------------------------------
                //-------------------
                //ADD PIN TO START
                //-------------------
                if let clkPlaceSelectedReturned = clkPlacesPickerResultEnd.clkPlaceSelectedReturned {
                    
                    clkPlaceSelectedReturned.placeType = PlaceType.End
                    self.markerEnd = addPointToToMapForPlace(clkPlaceSelectedReturned, marker : self.markerEnd, fallbackNameString: "End")
                    
                }else{
                    logger.error("clkPlacesPickerResultEnd.clkPlaceSelectedReturned is nil")
                }
                
                //---------------------------------------------------------------------
                self.zoomToPins()
                //------------------------------------------
                //self.showHideButtons()
                //------------------------------------------
                
            }else{
                logger.error("didSet: clkPlacesPickerResultEnd is nil")
                self.labelAddressEnd?.text = "Tap to set end location"
                //-----------------------------------------------------------------------------------
                if let markerEnd = self.markerEnd {
                    markerEnd.map = nil
                    
                }else{
                    //noisy ok at end
                    //logger.error("self.markerEnd is nil")
                }
                //-----------------------------------------------------------------------------------
            }
            
            showOrHideDirectionsOverlayFromWS()
        }
    }
    
    

    //--------------------------------------------------------------
    // MARK: -
    // MARK: - IMPLEMENTATION
    //--------------------------------------------------------------
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        //self.buttonSwap.hidden = true
        
        //self.view.backgroundColor = AppearanceManager.appColorNavbarAndTabBar
        
        self.viewFROM.layer.cornerRadius = 4.0
        self.viewTO.layer.cornerRadius = 4.0
        self.labelAddressStart?.layer.cornerRadius = 4.0
        self.labelAddressEnd?.layer.cornerRadius = 4.0
        
        
        //-------------------------------------------------------------------
        
        self.buttonOpenIn.layer.cornerRadius = 4.0
        
        //self.buttonOpenIn.backgroundColor = AppearanceManager.appColorLight1
        //self.buttonOpenIn.backgroundColor = AppearanceManager.appColorNavbarAndTabBar
        //self.buttonOpenIn.titleLabel?.textColor = AppearanceManager.appColorNavbarAndTabBar
        //self.buttonOpenIn.tintColor = AppearanceManager.appColorNavbarAndTabBar
        //-----------------------------------------
        //SHADOW
        //make shadow same as google location shadow
        
        self.buttonOpenIn.layer.shadowColor = UIColor.gray.cgColor
        //self.buttonOpenIn.layer.shadowColor = AppearanceManager.appColorNavbarAndTabBar.CGColor
        self.buttonOpenIn.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.buttonOpenIn.layer.shadowRadius = 2.0
        self.buttonOpenIn.layer.shadowOpacity = 0.5
        //-------------------------------------------------------------------
        //viewButtonUber
        self.viewButtonUber.layer.cornerRadius = 4.0

        self.viewButtonUber.layer.shadowColor = UIColor.gray.cgColor
        //self.viewButtonUber.layer.shadowColor = AppearanceManager.appColorNavbarAndTabBar.CGColor
        self.viewButtonUber.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.viewButtonUber.layer.shadowRadius = 2.0
        self.viewButtonUber.layer.shadowOpacity = 1.0
        //-------------------------------------------------------------------
        
        //debug show hidden buttons over to/from labels
//        self.buttonStart.layer.borderWidth = 1.0
//        self.buttonStart.layer.borderColor = UIColor.redColor().CGColor
//        
//        self.buttonDestination.layer.borderWidth = 1.0
//        self.buttonDestination.layer.borderColor = UIColor.redColor().CGColor
        
        
        self.viewFROM.layer.borderWidth = 4.0
        //self.viewFROM.layer.borderColor = AppearanceManager.appColorNavbarAndTabBar.CGColor
        self.viewFROM.layer.borderColor = UIColor.white.cgColor
        self.viewFROM.layer.cornerRadius = 4.0
        //self.viewFROM.clipsToBounds = true
        
        self.viewTO.layer.borderWidth = 4.0
        //self.viewTO.layer.borderColor = AppearanceManager.appColorNavbarAndTabBar.CGColor
        self.viewTO.layer.borderColor = UIColor.white.cgColor
        self.viewTO.layer.cornerRadius = 4.0
        //self.viewTO.clipsToBounds = true
       
        //clip the shadow to BELOW the boxes
        //viewFROMTO.clipsToBounds = true
        addShadowToView(self.viewFROM)
        addShadowToView(self.viewTO)
        
//        addShadowToView(self.labelAddressStart)
//        addShadowToView(self.labelAddressEnd)
        
        //---------------------------------------------------------------------
        //init only after map and this VC fully init'd
        //---------------------------------------------------------------------
        if let mapView = self.mapView {
            self.colcGoogleDirectionsUIManager = COLCGoogleDirectionsUIManager(gmsMapView: mapView, parentViewController: self)
        }else{
            logger.error("self.mapView is nil - cant init COLCGoogleDirectionsUIManager")
        }
        //------------------------------------------------------------------------------------------------
        //UIAppearance
        //------------------------------------------------------------------------------------------------
        
        //self.buttonOpenIn.applyCustomFontForCurrentTextStyle()
        self.labelAddressStart?.applyCustomFontForCurrentTextStyle()
        self.labelAddressEnd?.applyCustomFontForCurrentTextStyle()
        
        //------------------------------------------------------------------------------------------------
        //MAP
        //------------------------------------------------------------------------------------------------
        //show Blue Dot
        mapView.isMyLocationEnabled = true
        //compass
        mapView.settings.compassButton = true
        //TAp to move to my location
        mapView.settings.myLocationButton = true 
     
        //------------------------------------------------------------------------------------------------
        //to get current location updates forwarded to this screen
// TODO: - CCOCAPODS REFACTOR  OK TO COMMENT BACK IN      appDelegate.tripPlannerViewController = self
        //------------------------------------------------------------------------------------------------
        
        // TODO: - is this whats clearing the map when we switch apps
        resetTrip()
        
    
        //---------------------------------------------------------------------
        //CENTER ON CURRENT LOCATION
        //---------------------------------------------------------------------
        if let currentLocation = appDelegate.currentLocationManager.currentUserPlace.clLocation {
            self.currentLocationUpdated(currentLocation)
            
        }else{
            logger.error("currentUserPlace.clLocation is nil - CANT MOVE TripPlanner map")
        }

        //---------------------------------------------------------------------
        
    }
    
    //--------------------------------------------------------------
    // MARK: - RESET TRIP
    // MARK: -
    //--------------------------------------------------------------
    
    func resetTrip(){

        self.clkPlacesPickerResultStart = nil
        self.clkPlacesPickerResultEnd = nil

        self.mapView.clear()
    }
    
    func addShadowToView(_ view : UIView){
        //.layer.shadowColor = UIColor.grayColor().CGColor
        view.layer.shadowColor = AppearanceManager.appColorNavbarAndTabBar.cgColor
        
        //view.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view.layer.shadowRadius = 5.0
        view.layer.shadowOpacity = 0.5
    }
    
    //--------------------------------------------------------------
    // MARK: - ZOOM TO LOCATION
    // MARK: -
    //--------------------------------------------------------------
    func currentLocationUpdated(_ currentLocation : CLLocation){
        if self.userDidChangeCameraPosition{
            //NOISY logger.debug("moveToCurrentLocation - userDidChangeCameraPosition is true DONT move to new currentLocation")
        }else{
            //NOISY logger.debug("moveToCurrentLocation - userDidChangeCameraPosition is false DO move to new currentLocation")
            //map at default so ok to move to current location

            moveMapToCLLocation(currentLocation)
        }
    }
    
    func moveMapToCLLocation(_ clLocation : CLLocation){
        //------------------------------------------------------------------------------------------------
        
        //zoom both maps to current location but Start should be more zoomed in than dest
        let zoomedStart: Float = 15
        
        let cameraPositionStart = GMSCameraPosition.camera(withLatitude: clLocation.coordinate.latitude, longitude:clLocation.coordinate.longitude, zoom:zoomedStart)
        
        self.mapView.animate(to: cameraPositionStart)
        
        //triggered AFTER move complete in idleAtCameraPosition:
        //self.nearbySearch(currentLocation)
    }

    
    //--------------------------------------------------------------
    // MARK: - ZOOM TO PINS
    // MARK: -
    //--------------------------------------------------------------
    func zoomToPins(){
        if let markerStart = self.markerStart {
            //self.markerStart is set
            
            if let markerEnd = self.markerEnd {
                //BOTH SET - zoom to bounds
                
                //START
                var pins_gmsCoordinateBounds = GMSCoordinateBounds()
                //ADD AND RETURN END
                
                pins_gmsCoordinateBounds = pins_gmsCoordinateBounds.includingCoordinate(markerStart.position)
                pins_gmsCoordinateBounds = pins_gmsCoordinateBounds.includingCoordinate(markerEnd.position)

                if let gmsCameraPosition = self.mapView.camera(for: pins_gmsCoordinateBounds,
                    insets: UIEdgeInsetsMake(230, 100, 100, 100))
                {
                    self.mapView.animate(to: gmsCameraPosition)
                    
                }else{
                    logger.error("gmsCameraPosition is nil")
                }

            }else{
                logger.error("self.markerEnd is nil")
                //ONLY START SET - zoom to start
                //self.mapView.animateToLocation(markerStart.position)
                let cameraUpdate =  GMSCameraUpdate.setTarget(markerStart.position, zoom: 14)
                self.mapView.animate(with: cameraUpdate)
            }
        }else{
            //self.markerStart is nil
            
            if let markerEnd = self.markerEnd {
                //ONLY END SET - zoom to end - should not happen but added for completeness
                //self.mapView.animateToLocation(markerEnd.position)
                let cameraUpdate =  GMSCameraUpdate.setTarget(markerEnd.position, zoom: 14)
                self.mapView.animate(with: cameraUpdate)
            }else{
                //START and end are nil do nothing
            }
        }
    }
    
    func showOrHideDirectionsOverlayFromWS(){
        if let colcGoogleDirectionsUIManager = self.colcGoogleDirectionsUIManager {
            
            if let clkPlaceStart = self.clkPlaceStart {
                if let clkPlaceEnd = self.clkPlaceEnd {

                    if clkPlaceStart === clkPlaceEnd{
                        logger.error("origin and destination are same - can happen when you swap")
                    }else{
                        //------------------------------------------------------------------------------------------------
                        colcGoogleDirectionsUIManager.getDirections(clkPlaceStart: clkPlaceStart,
                                                                    clkPlaceEnd: clkPlaceEnd,
                                                                    success:{(clkGoogleDirectionsResponse: CLKGoogleDirectionsResponse?) -> Void in
                                                                        //---------------------------------------------------------------------
                                                                        if let clkGoogleDirectionsResponse = clkGoogleDirectionsResponse {
                                                                            colcGoogleDirectionsUIManager.handleCLKGoogleDirectionsResponse(clkGoogleDirectionsResponse)
                                                                        }else{
                                                                            logger.error("clkGoogleDirectionsResponse is nil")
                                                                        }
                                                                        //---------------------------------------------------------------------
                                                                        //UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                                                        //---------------------------------------------------------------------
                            },
                                                                    failure:{(error) -> Void in
                                                                        
                                                                        logger.error("error:\(error)")
                                                                        if let nserror = error {
                                                                            colcGoogleDirectionsUIManager.handleError(nserror)
                    
                                                                        }else{
                                                                            logger.error("error is nil")
                                                                        }
                    
                                                                        //self.googleDirectionsSearchRunning = false
                                                                        //UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        })
                        //------------------------------------------------------------------------------------------------

                    }
                }else{
                    //clkPlaceEnd is nil - do nothing till both retrieved
                    colcGoogleDirectionsUIManager.removeTripFromMap()
                }
            }else{
                //clkPlaceStart is nil - do nothing till both retrieved
                colcGoogleDirectionsUIManager.removeTripFromMap()
            }
        }else{
            logger.error("self.colcGoogleDirectionsUIManager is nil")
        }
    }
    
    func addPointToToMapForPlace(_ clkPlace: CLKPlace, marker : GMSMarker?, fallbackNameString: String) -> GMSMarkerPop?{
        var gmsMarkerPop_ : GMSMarkerPop?
        
        //---------------------------------------------------------------------
        //DISCONNECT PREV MARKER FROM MAP - if set
        //---------------------------------------------------------------------
        //remove previous marker from map, if set
        if let marker = marker {
            if let _ = marker.map {
                //disconnect from map - will be overitten outside
                marker.map = nil
            }else{
                //self.markerStart.map is nil - so nothing to disconnect
                logger.info("self.markerStart.map is nil - so nothing to disconnect")
            }
        }else{
            logger.warning("self.markerStart - ok if new")
        }
        
        //-----------------------------------------------------------------------------------
        //source varies depending on type
        if let clLocation = clkPlace.clLocation {

            
            //---------------------------------------------------------------------
            //ADD NEW MARKER TO MAP
            //---------------------------------------------------------------------
            gmsMarkerPop_ = GMSMarkerPop(position: clLocation.coordinate)
            if let gmsMarkerPop_ = gmsMarkerPop_ {

                //---------------------------------------------------------------------
                //MARKER - TITLE - nearby address has name = nil so we dont show "15 Royal Mint St, 15 Royal Mint St, London"
                //---------------------------------------------------------------------
                if let name = clkPlace.name {
                    gmsMarkerPop_.title = name
                    
                }else if let formatted_address_first_line = clkPlace.formatted_address_first_line {
                    gmsMarkerPop_.title = formatted_address_first_line
                    
                }else{
                    //fallback to Start/End
                    logger.error("clkPlace.name is nil - display fallbackNameString on maker:\(fallbackNameString)")
                    gmsMarkerPop_.title = fallbackNameString
                }
                
                //-----------------------------------------------------
                //connect to map
                //set in subclass gmsMarkerPop_
                //marker_.appearAnimation = marker_.kGMSMarkerAnimationNone
                
                
                //    shapeLayer.strokeColor = AppearanceManager.appColorNavbarAndTabBar.CGColor
                //    shapeLayer.fillColor = AppearanceManager.appColorTint.CGColor
                //-----------------------------------------------------
                
                let image = GoogleMapUtils.imageForMarker(gmsMarkerPop_, clkPlace: clkPlace)
                gmsMarkerPop_.icon = image
                //weird bug I set this then I commented it out and it was all over the shop  - check the image for icon doesnt have clear border
                //OK centered
                //gmsMarkerPop_.groundAnchor = CGPoint(x: 0.5, y: 0.5)
                //gmsMarkerPop_.groundAnchor = CGPoint(x: 0.0, y: 0.5)
                
                //alway leave x at 0.5 then image has a clear border around it text
                //label is just above the point
                gmsMarkerPop_.groundAnchor = CGPoint(x: 0.5, y: 1.0)
                
                //-------------------------------------------------------------------
                gmsMarkerPop_.map = self.mapView
                //-------------------------------------------------------------------
                
            }else{
                logger.error("marker_ is nil")
            }
        }else{
            logger.error("clkPlaceStart.gmsPlace  is nil")
        }
        return gmsMarkerPop_
    }
    
    
    //--------------------------------------------------------------
    // MARK: - TAP on Start/End
    // MARK: -
    //--------------------------------------------------------------

    @IBAction func buttonStart_Action(_ sender: AnyObject) {
        self.currentPlaceType = .Start
        self.performSegue(withIdentifier: ksegueTOLocationPickerViewController, sender: nil)
        
    }
    
    @IBAction func buttonDestination_Action(_ sender: AnyObject) {
        self.currentPlaceType = .End
        self.performSegue(withIdentifier: ksegueTOLocationPickerViewController, sender: nil)
    }
    
    @IBAction func buttonOpenIn_Action(_ sender: AnyObject) {
        self.performSegue(withIdentifier: ksegueTOOpenWithViewController, sender: nil)
        
    }
    
    @IBAction func buttonOpenInUber_Action(_ sender: AnyObject) {

        if let clkPlaceStart = self.clkPlaceStart{
            if let clkPlaceEnd = self.clkPlaceEnd{
 print("ERROR  appDelegate.openWithController commented out works trying to get cocoapods build working")
//                appDelegate.openWithController.open(OpenWithType.openWithType_Uber.openAppConfig(),
//                                                    clkPlaceStart: clkPlaceStart,
//                                                    clkPlaceEnd: clkPlaceEnd,
//                                                    presentingViewController: self)
                
            }else{
                logger.info("clkPlaceEnd is nil - OK for Uber but need to test all other app so moved to FUTURE")
                // TODO: - FUTURE - END is optional for uber but mandatory for others - need to test each
                //                appDelegate.openWithController.open(OpenWithType.OpenWithType_Uber.openAppConfig(),
                //                                                    clkPlaceStart: clkPlaceStart,
                //                                                    clkPlaceEnd: nil,
                //                                                    presentingViewController: self)
                
                 CLKAlertController.showAlertInVC(self, title: "Destination location missing", message: "Please choose a Destination location by tapping on the field.")
            }
        }else{
            logger.error("clkPlaceStart is nil")
            CLKAlertController.showAlertInVC(self, title: "Start location missing", message: "Please choose a Start location by tapping on the field.")
        }
    }

    func swapStartAndEnd(){
        //---------------------------------------------------------------------
        //    let tempCLKPlaceStart = self.clkPlaceStart
        //    self.clkPlaceStart = self.clkPlaceEnd
        //    self.clkPlaceEnd = tempCLKPlaceStart
        //---------------------------------------------------------------------
        let tempCLKPlacesPickerResultStart = self.clkPlacesPickerResultStart
        self.clkPlacesPickerResultStart = self.clkPlacesPickerResultEnd
        self.clkPlacesPickerResultEnd = tempCLKPlacesPickerResultStart
        //---------------------------------------------------------------------
    }

    
    //--------------------------------------------------------------
    // MARK: - OPTIONS
    // MARK: -
    //--------------------------------------------------------------

    @IBOutlet weak var buttonOptionsStart: UIButton!
    @IBAction func buttonOptionsStart_Action(_ sender: AnyObject) {
        openOptions(.Start)
    }
    
    @IBOutlet weak var buttonOptionsEnd: UIButton!
    @IBAction func buttonOptionsEnd_Action(_ sender: AnyObject) {
        openOptions(.End)
    }
    
    //--------------------------------------------------------------
    // MARK: - OPTIONS - Share
    // MARK: -
    //--------------------------------------------------------------

    
    func shareText(_ textToShare: String){
        //        let textToShare = "Swift is awesome!  Check out this website about it!"
        //        
        //        if let myWebsite = NSURL(string: "http://www.codingexplorer.com/")
        //        {
        //            let objectsToShare = [textToShare, myWebsite]
        //            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        //            
        //            self.presentViewController(activityVC, animated: true, completion: nil)
        //        }
        
        let objectsToShare = [textToShare]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        
        self.present(activityVC, animated: true, completion: nil)
    
    }
    
    //--------------------------------------------------------------
    // MARK: - EditCLKPlaceAddressViewControllerDelegate
    // MARK: -
    //--------------------------------------------------------------
    func controller(_ controller: EditCLKPlaceAddressViewController, didEditCLKPlace: CLKPlace){
        // TODO: - TODO didEditCLKPlace
logger.info("TODO didEditCLKPlace")
//        if let placeType = didEditCLKPlace.placeType {
//            switch placeType{
//            case .Start:
//                self.clkPlaceStart = didEditCLKPlace
//                
//            case .End:
//                self.clkPlaceEnd = didEditCLKPlace
//            }
//        }else{
//            logger.error("didEditCLKPlace.placeType is nil")
//        }
    }
   
    func openOptions(_ placeType: PlaceType){
        
        //-----------------------------------------------------------------------------------
        let alertController = UIAlertController(title:placeType.rawValue , message:nil , preferredStyle: UIAlertController.Style.actionSheet)
        //-----------------------------------------------------------------------------------
        //must have at least one action or you get an Alert you can close

        //-----------------------------------------------------------------------------------
        //Edit
        //-----------------------------------------------------------------------------------

        let alertActionEdit = UIAlertAction(title: "Edit", style: .default) { (alertAction) -> Void in
            print("alertActionEdit tapped")
            
            switch placeType{
            case .Start:
                if let _ = self.labelAddressStart.text {
                    self.clkPlaceToEdit = self.clkPlaceStart
                    self.performSegue(withIdentifier: self.segueTripPlannerViewControllerTOEditCLKPlaceAddressViewController, sender: nil)
                    
                }else{
                    logger.error("self.labelAddressStart.text is nil")
                }
                
            case .End:
                if let _ = self.labelAddressEnd.text {
                    self.clkPlaceToEdit = self.clkPlaceEnd
                    self.performSegue(withIdentifier: self.segueTripPlannerViewControllerTOEditCLKPlaceAddressViewController, sender: nil)
                }else{
                    logger.error("self.labelAddressEnd.text is nil")
                }
            }
        }
        alertController.addAction(alertActionEdit)
        
        
        //-----------------------------------------------------------------------------------
        //SHARE
        //-----------------------------------------------------------------------------------
        let alertActionShare = UIAlertAction(title: "Share", style: .default) { (alertAction) -> Void in
            print("alertActionShare tapped")
            
            switch placeType{
            case .Start:
                if let text = self.labelAddressStart.text {
                    self.shareText(text)
                }else{
                    logger.error("self.labelAddressStart.text is nil")
                }
                
              
            case .End:
                if let text = self.labelAddressEnd.text {
                    self.shareText(text)
                }else{
                    logger.error("self.labelAddressEnd.text is nil")
                }
            }
        }
        alertController.addAction(alertActionShare)
        
        //-----------------------------------------------------------------------------------
        //SWAP
        //-----------------------------------------------------------------------------------
        let alertActionSwap = UIAlertAction(title: "Swap", style: .default) { (alertAction) -> Void in
            self.swapStartAndEnd()
            
        }
        alertController.addAction(alertActionSwap)
        
       //-----------------------------------------------------------------------------------
        //CLEAR
        //-----------------------------------------------------------------------------------
        let alertActionClear = UIAlertAction(title: "Clear", style: UIAlertController.Style.destructive) { (alertAction) -> Void in
            //print("alertActionClear tapped")
            
            switch placeType{
            case .Start:
                // TODO: - CLEANUP after test
                //self.clkPlaceStart = nil
                self.clkPlacesPickerResultStart = nil
            case .End:
                // TODO: - CLEANUP after test
                //self.clkPlaceEnd = nil
                self.clkPlacesPickerResultEnd = nil
            }
        }
        alertController.addAction(alertActionClear)
        
        //-----------------------------------------------------------------------------------
        //CANCEL
        //-----------------------------------------------------------------------------------
        let alertActionCancel = UIAlertAction(title: "Cancel", style: .cancel) { (alertAction) -> Void in
            print("alertActionCancel tapped")
        }
        alertController.addAction(alertActionCancel)
    
        //-----------------------------------------------------------------------------------
        
        self.present(alertController, animated: true, completion: nil)
        //------------------------------------------------
        //tint color too bright and alert is white
        //------------------------------------------------
        //if app tint color is bright hard to see
        //from stackoverlfow - to fix issue with tint too pale - set it AFTER you present the alert
        //alertController.view.tintColor = UIColor.blackColor()
        //gmsCircle.strokeColor = UIColor.appBaseColor()
        alertController.view.tintColor = AppearanceManager.appColorNavbarAndTabBar
        //-----------------------------------------------------------------------------------
        
    }
    
    
    
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - SEGUE
    //--------------------------------------------------------------
    
    let segueTripPlannerViewControllerTOEditCLKPlaceAddressViewController = "segueTripPlannerViewControllerTOEditCLKPlaceAddressViewController"

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == ksegueTOLocationPickerViewController){

            if(segue.destination.isMember(of: LocationPickerViewController.self)){
              
                let locationPickerViewController = (segue.destination as! LocationPickerViewController)
                //if user is editing a place
                
//                switch currentPlaceType{
//                case .Start:
//                    locationPickerViewController.clkPlaceSelected = self.clkPlaceStart
//                case .End:
//                    locationPickerViewController.clkPlaceSelected = self.clkPlaceEnd
//                }
                
                locationPickerViewController.delegate = self

            }else{
                logger.error("destinationViewController not COLCPlacePickerViewController")
            }
        }
        else if(segue.identifier == ksegueTOOpenWithViewController){
            
            if(segue.destination.isMember(of: OpenWithViewController.self)){
                
                let openWithViewController = (segue.destination as! OpenWithViewController)
                // TODO: - fix
logger.error("OPEN WITH BROKEN 878978")
//                openWithViewController.clkPlaceStart = self.clkPlaceStart
//                openWithViewController.clkPlaceEnd = self.clkPlaceEnd
//                
//                //openWithViewController.delegate = self
                
            }else{
                logger.error("destinationViewController not OpenWithViewController")
            }
        }
        else if(segue.identifier == segueTripPlannerViewControllerTOEditCLKPlaceAddressViewController){
            
            if(segue.destination.isMember(of: EditCLKPlaceAddressViewController.self)){
                
                let editCLKPlaceAddressViewController = (segue.destination as! EditCLKPlaceAddressViewController)
                editCLKPlaceAddressViewController.delegate = self
                
                editCLKPlaceAddressViewController.clkPlaceToEdit = self.clkPlaceToEdit
                
            }else{
                logger.error("destinationViewController not OpenWithViewController")
            }
        }
        else{
            logger.error("UNHANDLED SEGUE:\(segue.identifier)")
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - LocationPickerViewControllerDelegate
    // MARK: -
    //--------------------------------------------------------------
    
    func locationPickerViewController(_ locationPickerViewController: LocationPickerViewController, clkPlacesPickerResult: CLKPlacesPickerResult){
        switch self.currentPlaceType{
        case .Start:
            self.clkPlacesPickerResultStart = clkPlacesPickerResult
        case .End:
            self.clkPlacesPickerResultEnd = clkPlacesPickerResult
        }
    }

    
    func locationPickerViewControllerCancelled(_ locationPickerViewController: LocationPickerViewController){
        logger.error("Cancelled - do nothing")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
