//
//  TFLAdditionalPropertyCategory_TaxiRank.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 13/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

//TFLAdditionalProperty:[category:Address - key:Borough - value:Tower Hamlets]
//TFLAdditionalProperty:[category:Description - key:NumberOfSpaces - value:3]
//TFLAdditionalProperty:[category:Description - key:OperationDays - value:Mon - Sun]
//TFLAdditionalProperty:[category:Description - key:OperationTimes - value:24 hours]
//TFLAdditionalProperty:[category:Description - key:RankType - value:Working]


enum TFLApiPlaceTaxiRank_additionalProperty_category: String{
    case unknown = "Unknown"
    case address = "Address"
    case description = "Description"
    
    func displayString() -> String{
        switch self{
        case .unknown:  return "Unknown"
            
        case .address:  return "Address"
        case .description:  return "Description"
        }
    }
    
    //"Address" >> TFLAdditionalPropertyCategory.Address
    static func categoryForCategoryString(categoryString: String?) -> TFLApiPlaceTaxiRank_additionalProperty_category{
        if let categoryString = categoryString {
            switch categoryString{
                
            case "Address": return .address
            case "Description": return .description
        
            default:
                return .unknown
            }
        }else{
            logger.error("categoryString is nil")
            return .unknown
        }
    }
}
