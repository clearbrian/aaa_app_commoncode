//
//  TFLAdditionalPropertyKey.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 13/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

//https://docs.google.com/spreadsheets/d/1ZD7Oty83tidOD-spUpJsMFRFphzVFIY9hqpyef6BrnY/edit#gid=0

enum TFLApiPlaceTaxiRank_additionalProperty_key: String{
    
    case unknown = "Unknown"
    case borough = "Borough"
    case numberOfSpaces = "NumberOfSpaces"
    case operationDays = "OperationDays"
    case operationTimes = "OperationTimes"
    case rankType = "RankType"
    case stationAtcoCode = "StationAtcoCode" //stationAtcoCode,910GSRUISLP - TaxiRank_3569,PROP,Address,borough,Hillingdon
    
    func displayString() -> String{
        switch self{
        case .unknown: return "Unknown"
        case .borough: return "borough"
        case .numberOfSpaces:  return "Number Of Spaces"
        case .operationDays: return "Operation Days"
        case .operationTimes:  return "Operation Times"
        case .rankType:  return "Rank Type"
        case .stationAtcoCode:  return "Station Atco Code"
            
        }
    }
    
    //"rankType" >> TFLAdditionalPropertyKey.rankType
    static func keyForKeyString(keyString: String?) -> TFLApiPlaceTaxiRank_additionalProperty_key{
        
        if let keyString = keyString {
            switch keyString{
                
            case "Borough": return .borough
            case "NumberOfSpaces":  return .numberOfSpaces
            case "OperationDays":  return .operationDays
            case "OperationTimes":  return .operationTimes
            case "RankType":  return .operationTimes
            case "StationAtcoCode":   return .stationAtcoCode
                
            default:
                return .unknown
            }
        }else{
            logger.error("keyString is nil")
            return .unknown
        }
    }
}
