//
//  RankType.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 13/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

enum TFLApiPlaceTaxiRank_additionalProperty_values_RankType: String{
    
    case unknown = "UNKNOWN RANK TYPE"

    case working = "Working"
    case rest_rank = "Rest Rank"
    case refreshment_rank = "Refreshment rank"
    
    static func rankTypeForValue(_ value: String?) -> TFLApiPlaceTaxiRank_additionalProperty_values_RankType?{
        if let value = value {
            
            switch value{
                
            case "Working": return .working
            case "Rest Rank":  return .rest_rank
            case "Refreshment rank":  return .refreshment_rank
                
            default:
                return .unknown
            }
        }else{
            logger.error("json value is nil - not found")
            return nil
        }
    }
    
}
