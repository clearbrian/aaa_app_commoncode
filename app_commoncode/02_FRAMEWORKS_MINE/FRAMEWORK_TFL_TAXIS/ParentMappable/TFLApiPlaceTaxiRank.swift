//
//  TFLApiPlaceTaxiRankVersion.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 11/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation


class TFLApiPlaceTaxiRank: TFLApiPlace{
    
    //--------------------------------------------------------------
    // MARK: - TO ADD MISSING ONES NOT IN JSON
    // MARK: -
    //--------------------------------------------------------------
    
    init(id : String,
         commonName : String,
//         lat : Float,
//         lon : Float
        lat : Double,
        lon : Double
        )
    {
        //----------------------------------------------------------------------------------------
        super.init(id: id,
                   commonName: commonName,
                   lat: lat,
                   lon: lon,
                   placeType: "TaxiRank",
                   url: nil)
        //----------------------------------------------------------------------------------------
        //self.id = id
        //self.type = "Tfl.Api.Presentation.Entities.Place, Tfl.Api.Presentation.Entities"
        //self.additionalProperties =  [TFLApiPlaceAdditionalProperty]()
        //self.children = [AnyObject]()
        //self.childrenUrls = [AnyObject]()
        //
        //self.commonName = commonName
        //self.lat = lat
        //self.lon = lon
        //self.placeType = "TaxiRank"
        //self.url = nil
        //----------------------------------------------------------------------------------------
        
        //req else not put into dict properly
        self.tflApiPlaceTaxiRankVersionInfo = TFLApiPlaceTaxiRankVersionInfo.init(rankIdJSON: id,
                                                                                  rankIdUnversioned: id,
                                                                                  version: 0,
                                                                                  hasVersions: false)
        //----------------------------------------------------------------------------------------
    }
    
    required init?(map: Map){
        super.init(map: map)
        
    }
    
    //--------------------------------------------------------------
    // MARK: - MISSING RANKS
    // MARK: -
    //--------------------------------------------------------------
    class func  createWorkingRank(id : String,
                            commonName : String,
//                            lat : Float,
//                            lon : Float,
                            lat : Double,
                            lon : Double,
                            Borough : String) -> TFLApiPlaceTaxiRank
        
    {
        //----------------------------------------------------------------------------------------
        let tflApiPlaceTaxiRank = TFLApiPlaceTaxiRank(id: id,
                                                      commonName: commonName,
                                                      lat: lat,
                                                      lon: lon)
        //----------------------------------------------------------------------------------------
        tflApiPlaceTaxiRank.appendAdditionalProperty(TFLApiPlaceAdditionalProperty(category: "Address", key: "Borough", sourceSystemKey: "3207", value: Borough))
        
        tflApiPlaceTaxiRank.appendAdditionalProperty(TFLApiPlaceAdditionalProperty(category: "Description", key: "NumberOfSpaces", sourceSystemKey: "3207", value: ""))
        tflApiPlaceTaxiRank.appendAdditionalProperty(TFLApiPlaceAdditionalProperty(category: "Description", key: "OperationDays", sourceSystemKey: "3207", value: ""))
        tflApiPlaceTaxiRank.appendAdditionalProperty(TFLApiPlaceAdditionalProperty(category: "Description", key: "OperationTimes", sourceSystemKey: "3207", value: ""))
    
        tflApiPlaceTaxiRank.appendAdditionalProperty(TFLApiPlaceAdditionalProperty(category: "Description", key: "RankType", sourceSystemKey: "3207", value: "Working"))
        //----------------------------------------------------------------------------------------
        return tflApiPlaceTaxiRank
        
    }
    
    
    func appendAdditionalProperty(_ tflApiPlaceAdditionalProperty : TFLApiPlaceAdditionalProperty){
        ///let tflApiPlaceAdditionalPropertyBorough = TFLApiPlaceAdditionalProperty(category: "Address", key: "Borough", sourceSystemKey: "3207", value: "Chelsea")
        
        if let _ =  self.additionalProperties {
            self.additionalProperties?.append(tflApiPlaceAdditionalProperty)
        }else{
            logger.error("self.additionalProperties is nil")
        }
    }
    
    
    
    //--------------------------------------------------------------
    // MARK: - VERSIONS
    // MARK: -
    //--------------------------------------------------------------
    //unversioned
    //[id:TaxiRank_5633] ...
    //versioned
    //[id:TaxiRank_5642-1] Bethnal Green Road (Tesco), Bethnal Green [51.5269, -0.061084] [RankType:Working]
    //[id:TaxiRank_5642-2] Bethnal Green Road (Tesco), Bethnal Green [51.5269, -0.061084] [RankType:Working]
    //--------------------------------------------------------------
    var tflApiPlaceTaxiRankVersionInfo : TFLApiPlaceTaxiRankVersionInfo?
    //--------------------------------------------------------------
    
    var isVersioned : Bool{
        var isVersioned_ = false
        
        if let idJSON = self.id {
            if idJSON.contains("-"){
                isVersioned_ = true
            }else{
                
            }
        }else{
            
        }
        return isVersioned_
    }
    
    //--------------------------------------------------------------
    // MARK: - id
    // MARK: -
    //--------------------------------------------------------------

    override var id : String?{
        didSet {
            
            if let idJSON = self.id {
                
                if idJSON.contains("-"){
                    //---------------------------------------------------------
                    //ID HAS VERSION
                    //---------------------------------------------------------
                    //[id:TaxiRank_5642-1]
                    //[id:TaxiRank_5642-2]
                    
                    let idSplitArray: [String] = idJSON.split("-")
                    
                    //---------------------------------------------------------
                    //"TaxiRank_5642"
                    //"1"
                    //---------------------------------------------------------
                    if idSplitArray.count == 2{
                        //---------------------------------------------------------
                        //ID UNVERSIONED  "TaxiRank_5642-1" >> "TaxiRank_5642"
                        //---------------------------------------------------------
                        let idUnversioned_ = idSplitArray[0]
                        
                        //---------------------------------------------------------
                        //VERSION NUMBER "TaxiRank_5642-1" >> 1
                        //---------------------------------------------------------
                        let versionString_ = idSplitArray[1]
                        
                        let formatter = NumberFormatter()
                        formatter.numberStyle = NumberFormatter.Style.none
                        
                        if let num_ = formatter.number(from: versionString_){
                            
                            self.tflApiPlaceTaxiRankVersionInfo = TFLApiPlaceTaxiRankVersionInfo(  rankIdJSON : idJSON,
                                                                                                   rankIdUnversioned : idUnversioned_,
                                                                                                   version : num_.intValue,
                                                                                                   hasVersions: true)
                            
                            //print(self.tflApiPlaceTaxiRankVersionInfo)
                        }else{
                            
                        }
                        
                    }else{
                        logger.error("idSplitArray.count == 2 faild: \(idSplitArray.count)")
                    }
                }else{
                    
                    //[id:TaxiRank_5642]
                    
                    self.tflApiPlaceTaxiRankVersionInfo = TFLApiPlaceTaxiRankVersionInfo(rankIdJSON : idJSON,
                                                                                         rankIdUnversioned : idJSON,
                                                                                         version : 0,
                                                                                         hasVersions: false)
                    // print(self.tflApiPlaceTaxiRankVersionInfo)
                }
            }else{
                logger.error("self.id is nil >> tflApiPlaceTaxiRankVersionInfo_ is nil")
            }
        }
    }
    
    
    
    //--------------------------------------------------------------
    // MARK: - ADDITIONAL PROPERTIES
    //--------------------------------------------------------------
    
    //    case unknown = "Unknown"
    //    case Borough = "Borough"
    //    case NumberOfSpaces = "NumberOfSpaces"
    //    case OperationDays = "OperationDays"
    //    case OperationTimes = "OperationTimes"
    //    case RankType = "RankType"
    //    case StationAtcoCode = "StationAtcoCode"

    //--------------------------------------------------------------
    // MARK: - Additional Property: "NumberOfSpaces"
    //--------------------------------------------------------------
    var numberOfSpaces: Int?{
        if let numberOfSpacesInt = self.findIntProperty_TaxiRank(.numberOfSpaces) {
            return numberOfSpacesInt
        }else{
            logger.error("self.findIntProperty_TaxiRank(.numberOfSpaces) is nil")
            return nil
        }
    }
   
    
    //--------------------------------------------------------------
    // MARK: - Additional Property: "OperationDays"
    //--------------------------------------------------------------
    var operationDays: String?{
        let operationDays = self.findStringProperty_TaxiRank(.operationDays)
        return operationDays
    }

    
    //--------------------------------------------------------------
    // MARK: - Additional Property: "OperationTimes"
    //--------------------------------------------------------------
     //value": "10:00 - 16:00 \n19:00 - 10:00",
    var operationTimes: String?{

        if let operationTimes = self.findStringProperty_TaxiRank(.operationTimes) {
            //----------------------------------------------------------------------------------------
            //EXTRA PROCESSING
            //----------------------------------------------------------------------------------------
            let operationTimesNotCR = operationTimes.replace("\n", with: " ")
            return operationTimesNotCR
            
        }else{
            return nil
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - SPECIAL operationDayaAndTimes
    //--------------------------------------------------------------
    //if both the same only show one
    var operationDayaAndTimesSafe: String{
        var operationDayaAndTimesSafe: String = ""
        
        if let operationDaysSafe_ = self.operationDays {
            if let operationTimesSafe_ = self.operationTimes {
                
                if operationDaysSafe_ == operationTimesSafe_{
                    
                    //special message
                    operationDayaAndTimesSafe = "\(operationDaysSafe_)"
                    
                }else{
                    operationDayaAndTimesSafe = "\(operationDaysSafe_), \(operationTimesSafe_)"
                    
                }
            }else{
                logger.error("self.operationTimes is nil")
            }
        }else{
            if let operationTimesSafe_ = self.operationTimes {
                operationDayaAndTimesSafe = "\(operationTimesSafe_)"
                
            }else{
                logger.error("self.operationTimes/operationDays are nil")
            }
        }
        return operationDayaAndTimesSafe
    }
    
    //--------------------------------------------------------------
    // MARK: - Additional Property: "RankType"
    // MARK: -
    //--------------------------------------------------------------
    var rankTypeOrStationString: String{
        var rankTypeOrStationString: String = ""
        
        if let rankTypeString_ = self.rankTypeString{
            
            if self.isStationRank{
                rankTypeOrStationString = "Type: \(rankTypeString_) [Station]"
            }else{
                rankTypeOrStationString = "Type: \(rankTypeString_)"
            }
            
        }else{
            logger.error("self.rankTypeString is nil")
            
            if self.isStationRank{
                rankTypeOrStationString = "Type: [Station]"
            }else{
                rankTypeOrStationString = ""
            }
        }
        
        return rankTypeOrStationString
    }

    var rankTypeString: String?{
        let rankTypeValueString = self.findStringProperty_TaxiRank(.rankType)
        return rankTypeValueString
    }
    
    //--------------------------------------------------------------
    //RankType > "RankType : Working" > .working
    //--------------------------------------------------------------
    
    var rankType: TFLApiPlaceTaxiRank_additionalProperty_values_RankType?{
        if let valueString = self.findStringProperty_TaxiRank(.rankType) {
            
            let rankType = TFLApiPlaceTaxiRank_additionalProperty_values_RankType.rankTypeForValue(valueString)
            return rankType
        }else{
            //logger.error("self.findIntProperty_TaxiRank(.RankTypePropKey) is nil - not found so return nil")
            return nil
        }
    }
    
    //--------------------------------------------------------------
    //if false dont show icon
    var isWorkingRank: Bool{
        //910GSRUISLP
        if let rankType = self.rankType {
            if rankType == TFLApiPlaceTaxiRank_additionalProperty_values_RankType.working{
                return true
            }else{
                return false
                //rest_rank = "Rest Rank"
                //case refreshment_rank = "Refreshment rank"
            }
            
        }else{
            //logger.error("self.stationAtcoCodeString is nil - not a station rank")
            return false
        }
        
    }
    
    //--------------------------------------------------------------
    // MARK: - Additional Property: StationAtcoCode,940GZZLUTBY
    // MARK: -
    //--------------------------------------------------------------
    var stationAtcoCodeString: String?{
        if let stationAtcoCodeValueString = self.findStringProperty_TaxiRank(.stationAtcoCode) {
            return stationAtcoCodeValueString
        }else{
            //logger.error("self.findIntProperty_TaxiRank(.StationAtcoCodePropKey) is nil")
            return nil
        }
    }
    
    //-----------------------------
    var isStationRank: Bool{
        //910GSRUISLP
        if let _ = self.stationAtcoCodeString {
            return true
        }else{
            //logger.error("self.stationAtcoCodeString is nil - not a station rank")
            return false
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - Borough
    // MARK: -
    //--------------------------------------------------------------
    var boroughString: String?{
        if let boroughPropValueString = self.findStringProperty_TaxiRank(.borough) {
            return boroughPropValueString
        }else{
            //logger.error("self.findIntProperty_TaxiRank(.BoroughPropKey) is nil")
            return nil
        }
    }
    
    
    
    //--------------------------------------------------------------
    // MARK: - Search Additional Properties
    // MARK: -
    //--------------------------------------------------------------
    //.RankType
    func findStringProperty_TaxiRank(_ tflAdditionalPropertyKey_TaxiRank: TFLApiPlaceTaxiRank_additionalProperty_key) -> String?{
        
        let stringValue: String? = findStringPropertyForKeyRawValue(tflAdditionalPropertyKey_TaxiRank.rawValue)
        
        return stringValue
    }
    
    //.NumberOfSpaces,"123" >> Int: 123
    func findIntProperty_TaxiRank(_ tflAdditionalPropertyKey_TaxiRank: TFLApiPlaceTaxiRank_additionalProperty_key) -> Int?{
        let intValue: Int? = findIntPropertyForKeyRawValue(tflAdditionalPropertyKey_TaxiRank.rawValue)
        
        return intValue
    }
    
    
    
    
    //--------------------------------------------------------------
    // MARK: - Description
    // MARK: -
    //--------------------------------------------------------------
    //descriptionLineDetailAll is TFLApiPlace
    
    //--------------------------------------------------------------
    //alert message and map pin subtitle
    
    override var alertTitle: String{
        return self.descriptionLineMain
    }
    //Tap on list OR pin on map
    override var alertMessage: String{
        return "Places: \(Safe.safeIntAsString(self.numberOfSpaces))\r\(self.rankTypeOrStationString)\rTimes: \(self.operationDayaAndTimesSafe)"
    }
    //--------------------------------------------------------------
    // MARK: - Detail
    // MARK: -
    //--------------------------------------------------------------
    override var descriptionLineDetail0 : String {
        var description_ = ""
        
        description_ = description_ +  "Places: \(Safe.safeIntAsString(self.numberOfSpaces))"
        
        return description_
    }
    
    override var descriptionLineDetail1 : String {
        var description_ = ""
        description_ = description_ +  "\(self.operationDayaAndTimesSafe)"
        return description_
    }
    
    //--------------------------------------------------------------
    // MARK: - SubDetail
    // MARK: -
    //--------------------------------------------------------------

    override var descriptionLineSubDetail0 : String {
        var description_ = ""
        
        description_ = description_ +  "\(self.distanceToCurrentLocationFormatted)"
        
        return description_
    }
    
    override var descriptionLineSubDetail1 : String {
        var description_ = ""
    
        //---------------------------------------------------------
        //RANK TYPE 'Working' etc
        //---------------------------------------------------------
        //only show if NOT working
        //description_ = description_ +  "Type: \(self.rankTypeSafe),"
        // TODO: - DEBUG
//        if(self.isWorkingRank){
//            //description_ = description_ +  "[Station Rank]"
//        }else{
            description_ = description_ +  "\(Safe.safeString(self.rankTypeString))"
//        }
        
        //---------------------------------------------------------
        if(self.isStationRank){
            if description_ == ""{
                description_ = description_ +  "Station Rank "
            }else{
                description_ = description_ +  " / Station Rank "
            }
            
        }else{
            //description_ = description_ +  ""
            
        }
        
        //---------------------------------------------------------
        //BOROUGH - add last field is right aligned
        //---------------------------------------------------------
        if description_ == ""{
            //
        }else{
            //APPEND GAP
            description_ = description_ +  " "
        }
        description_ = description_ +  "[\(Safe.safeString(self.boroughString))]"
        
        
        
        return description_
    }
    
    override var descriptionOneLine : String {
        return "[id:\(self.idSafe)] \(Safe.safeString(self.commonName)) [\(self.latStringSafe), \(self.lonStringSafe)] [RankType:\(Safe.safeString(self.rankTypeString))]"
    }
    
    //RANK,JamCam_5973,Park Plaza - County Hall Hotel, hotel forecourt,JamCam,-0.116399,51.5014,2,Mon - Sun,24 hours,Working,,
    override var descriptionCSV : String {
        
        var description_ = ""
        
        description_ = description_ +  "TaxiRank,"
        description_ = description_ +  "\(self.idSafe),"
        description_ = description_ +  "\(Safe.safeString(self.commonName)),"
        description_ = description_ +  "\(Safe.safeString(self.placeType)),"
        description_ = description_ +  "\(self.lonStringSafe),"
        description_ = description_ +  "\(self.latStringSafe),"
        description_ = description_ +  "\(Safe.safeIntAsString(self.numberOfSpaces)),"
        description_ = description_ +  "\(Safe.safeString(self.operationDays)),"
        description_ = description_ +  "\(Safe.safeString(self.operationTimes)),"
        description_ = description_ +  "\(Safe.safeString(self.rankTypeString)),"
        description_ = description_ +  "\(self.isStationRank),"
        description_ = description_ +  "\(self.isWorkingRank),"
        
        return description_
    }
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - MapPinnable
    //--------------------------------------------------------------
    //used by GoogleMappable gmsMarker > markerImageName> markerImage > UI
    //DO not override MapPinnable > mapImage - change the image name in assets here
    //DO not override GoogleMappable - you can overdide something in an extention
    //overriing markerImageName and changing the final image name based on rules in the subclass e.g TaxiRank.rankType
    //-------------------------------------------------------------------
 
    //let taxi_map_pin_flat_pdf = "taxi_map_pin_flat_pdf"
    
    let taxi_map_pin_flat_rest = "taxi_map_pin_flat_rest"
    let taxi_map_pin_flat_station = "taxi_map_pin_flat_station"
    let taxi_map_pin_flat_working = "taxi_map_pin_flat_working"
    let taxi_map_pin_flat_refresh = "taxi_map_pin_flat_refresh"

    
    override var markerImageName: String?{
        var markerImageNameRet: String? = nil
        
        //----------------------------------------------------------------------------------------
        //PIN NAME - varies by type
        //----------------------------------------------------------------------------------------
        // TODO: - move the filename into the RankType
        if let rankType = self.rankType {
            var imageName = taxi_map_pin_flat_working
            
            switch rankType{
            case .unknown:
                imageName = taxi_map_pin_flat_working
                
            case .working:
                if self.isStationRank{
                    imageName = taxi_map_pin_flat_station
                }else{
                    imageName = taxi_map_pin_flat_working
                }
            
            case .rest_rank:
                imageName = taxi_map_pin_flat_rest
                
            case .refreshment_rank:
                imageName = taxi_map_pin_flat_refresh
            }
            //-------------------------------------------------------------------
            markerImageNameRet = imageName
            //-------------------------------------------------------------------
            
        }else{
            logger.error("self.rankType is nil")
        }
    
        return markerImageNameRet
    }

    //--------------------------------------------------------------
    // MARK: - COLCQueryable
    // MARK: -
    //--------------------------------------------------------------
    //cant override static - 
    //http://stackoverflow.com/questions/29189700/overriding-static-vars-in-subclasses-swift-1-2
    

    override class var colcQueryCollectionForType : COLCQueryCollection{
        return TFLApiPlaceCOLCQueryCollectionTaxiRank()
    }
    
    //--------------------------------------------------------------
    // MARK: - MAP property name in json to value types
    // MARK: -
    //--------------------------------------------------------------

    fileprivate static let propertyName_borough = "borough"
    fileprivate static let propertyName_rankTypeString = "rankTypeString"
    fileprivate static let propertyName_isStationRank = "isStationRank"
    
    
    
    override func value(forPropertyName propertyName: String) -> Any?{
       
        if let valueAny = super.value(forPropertyName: propertyName) {
            return valueAny
            
        }else{
            //not found in parent - may be specific to property
            switch propertyName{
                
            case TFLApiPlaceTaxiRank.propertyName_borough:
                return self.boroughString
                
            case TFLApiPlaceTaxiRank.propertyName_rankTypeString:
                return self.rankTypeString
            case TFLApiPlaceTaxiRank.propertyName_isStationRank:
                return self.isStationRank
                
            default:
                logger.error("[Taxi ] UNHANDLED PROPERTY: value:forPropertyName:'\(propertyName) - TAXIRANK'")
                return nil
            }
        }
    }

    //--------------------------------------------------------------
    // MARK: - FACTORY - common ones in parent
    // MARK: -
    //--------------------------------------------------------------

    //---------------------------------------------------------
    //Used in builders
    class func colcQuery_RefreshmentRanks_sortByAtoZ() -> COLCQuery{
        return colcQuery_RefreshmentRanks(sortByNearest:false)
    }
    class func colcQuery_RefreshmentRanks_sortByNearest() -> COLCQuery{
        return colcQuery_RefreshmentRanks(sortByNearest:true)
    }
    //---------------------------------------------------------
    class func colcQuery_RestRanks_sortByAtoZ() -> COLCQuery{
        return colcQuery_RestRanks(sortByNearest:false)
    }
    class func colcQuery_RestRanks_sortByNearest() -> COLCQuery{
        return colcQuery_RestRanks(sortByNearest:true)
    }
    //---------------------------------------------------------
    class func colcQuery_WorkingRanks_sortByAtoZ() -> COLCQuery{
        return colcQuery_WorkingRanks(sortByNearest:false)
    }
    class func colcQuery_WorkingRanks_sortByNearest() -> COLCQuery{
        return colcQuery_WorkingRanks(sortByNearest:true)
    }
    //---------------------------------------------------------
    class func colcQuery_StationRanks_sortByAtoZ() -> COLCQuery{
        return colcQuery_StationRanks_sortByNearest(sortByNearest:false)
    }
    class func colcQuery_StationRanks_sortByNearest() -> COLCQuery{
        return colcQuery_StationRanks_sortByNearest(sortByNearest:true)
    }

    
    class func colcQuery_RefreshmentRanks(sortByNearest: Bool) -> COLCQuery{
        
        //----------------------------------------------------------------------------------------
        //QUERY
        //----------------------------------------------------------------------------------------
        var title = "Refreshment Ranks - A..Z"
        if sortByNearest{
            title = "Nearest Refreshment Ranks"
        }else{
            //a..z
        }
        //---------------------------------------------------------------
        
        let colcQuery = TFLApiPlace.COLCQuery_defaultSearchQuery(title:title)
        
    
        //----------------------------------------------------------------------------------------
        //FILTER
        //----------------------------------------------------------------------------------------
        let colcFilter_rankType_refreshment_rank = COLCFilterString(propertyName: TFLApiPlaceTaxiRank.propertyName_rankTypeString,
                                                                    searchString: TFLApiPlaceTaxiRank_additionalProperty_values_RankType.refreshment_rank.rawValue,
                                                                    isPartialSearch: false,
                                                                    isCaseSensitive : true)
        
        //---------------------------------------------------------
        colcQuery.appendCOLCFilter(colcFilter_rankType_refreshment_rank)
   
        //----------------------------------------------------------------------------------------
        //SORT
        //----------------------------------------------------------------------------------------
        if sortByNearest{
            colcQuery.colcSort = COLCSortByCLLocation(title: title)
        }else{
            //a..z
        }
        
        return colcQuery
    }
    

    class func colcQuery_RestRanks(sortByNearest: Bool) -> COLCQuery{
        
        
        //---------------------------------------------------------
        //QUERY
        //---------------------------------------------------------
        var title = "Rest Ranks - A..Z"
        if sortByNearest{
            title = "Nearest Rest Ranks"
        }else{
            //a..z
        }
        
        let colcQuery = TFLApiPlace.COLCQuery_defaultSearchQuery(title: title)
    
        //----------------------------------------------------------------------------------------
        //FILTER
        //----------------------------------------------------------------------------------------
        let colcFilter_rankType_rest_rank       = COLCFilterString(propertyName: TFLApiPlaceTaxiRank.propertyName_rankTypeString,
                                                                   searchString: TFLApiPlaceTaxiRank_additionalProperty_values_RankType.rest_rank.rawValue,
                                                                   isPartialSearch: false,
                                                                   isCaseSensitive : true)
        
        colcQuery.appendCOLCFilter(colcFilter_rankType_rest_rank)
        
        
        
        
        //-----------------------------------------------------------------
        
        //----------------------------------------------------------------------------------------
        //SORT
        //----------------------------------------------------------------------------------------
        if sortByNearest{
            colcQuery.colcSort = COLCSortByCLLocation(title: title)
        }else{
            //a..z
        }
        
        return colcQuery
    }
    
    class func colcQuery_WorkingRanks(sortByNearest: Bool) -> COLCQuery{
        
        //---------------------------------------------------------
        var title = "Working Ranks - A..Z"
        if sortByNearest{
            title = "Nearest Working Ranks"
        }else{
            //a..z
        }
        
        let colcQuery = TFLApiPlace.COLCQuery_defaultSearchQuery(title:title)
        
        let colcFilter_rankType_working  = COLCFilterString(propertyName: TFLApiPlaceTaxiRank.propertyName_rankTypeString,
                                                              searchString: TFLApiPlaceTaxiRank_additionalProperty_values_RankType.working.rawValue,
                                                              isPartialSearch: false,
                                                              isCaseSensitive : true)
        
        
        colcQuery.appendCOLCFilter(colcFilter_rankType_working)
        
        //----------------------------------------------------------------------------------------
        //SORT
        //----------------------------------------------------------------------------------------
        if sortByNearest{
            colcQuery.colcSort = COLCSortByCLLocation(title: title)
        }else{
            //a..z
        }

        return colcQuery
    }
    
    //--------------------------------------------------------------
    // MARK: - Factory queries - others in parent
    // MARK: -
    //--------------------------------------------------------------
    class func colcQuery_StationRanks_sortByNearest(sortByNearest: Bool) -> COLCQuery{
        
        //----------------------------------------------------------------------------------------
        //e.g. COLCFilterBool - SEARCH for isAvailable = true
        //----------------------------------------------------------------------------------------
        
        //----------------------------------------------------------------------------------------
        //QUERY
        //----------------------------------------------------------------------------------------
        var title = "Station Ranks - A..Z"
        if sortByNearest{
            title = "Nearest Station Ranks"
        }else{
            //a..z
        }
        //---------------------------------------------------------------
        let colcQuery = TFLApiPlace.COLCQuery_defaultSearchQuery(title:title)
        
        
        //----------------
        //FILTER
        //----------------
        let colcFilterBool = COLCFilterBool(propertyName: TFLApiPlaceTaxiRank.propertyName_isStationRank, searchBool: true)
        //---------------------------------------------------------
        colcQuery.appendCOLCFilter(colcFilterBool)
        
        //----------------------------------------------------------------------------------------
        //SORT
        //----------------------------------------------------------------------------------------
        if sortByNearest{
            colcQuery.colcSort = COLCSortByCLLocation(title: title)
        }else{
            //a..z
        }
        
        return colcQuery
        
        
        
    }
    
    //--------------------------------------------------------------
    // MARK: - TFLApiPlaceTaxiRank processTankRankVersions
    // MARK: -
    //--------------------------------------------------------------

    class func processTankRankVersions(tflApiPlaceTaxiRankArray : [TFLApiPlaceTaxiRank]) -> [TFLApiPlaceTaxiRank]{
        var tflApiPlaceTaxiRankDictionary = [String : TFLApiPlaceTaxiRank]()
        
        //VERSIONED:TaxiRank_4849-1 - ver:(Optional(1) - rankIdUnversioned:(Optional("TaxiRank_4849"))
        //VERSIONED:TaxiRank_4849-2 - ver:(Optional(2) - rankIdUnversioned:(Optional("TaxiRank_4849"))
        //VERSIONED:TaxiRank_4849-3 - ver:(Optional(3) - rankIdUnversioned:(Optional("TaxiRank_4849"))
        var countVersioned = 0
        var countUnVersioned = 0
        
        for tflApiPlaceTaxiRankNEW: TFLApiPlaceTaxiRank in tflApiPlaceTaxiRankArray{
            
            if let tflApiPlaceTaxiRankInArrayVersionInfoNEW = tflApiPlaceTaxiRankNEW.tflApiPlaceTaxiRankVersionInfo {
                
                if tflApiPlaceTaxiRankNEW.isVersioned{
                    
                    
                    //print("VERSIONED:\(tflApiPlaceTaxiRankNEW.idSafe) - ver:(\(tflApiPlaceTaxiRankNEW.tflApiPlaceTaxiRankInfo?.version) - rankIdUnversioned:(\(tflApiPlaceTaxiRankNEW.tflApiPlaceTaxiRankInfo?.rankIdUnversioned))")
                    
                    //----------------------------------------------------------------------------------------
                    //EXISTING
                    //----------------------------------------------------------------------------------------
                    if let tflApiPlaceTaxiRankEXISTING = tflApiPlaceTaxiRankDictionary[tflApiPlaceTaxiRankInArrayVersionInfoNEW.rankIdUnversioned]{
                        //a version with this rankid is already in this spot -
                        //check the version int - if lower then replace with new on
                        if let tflApiPlaceTaxiRankInDictVersionInfoEXISTING = tflApiPlaceTaxiRankEXISTING.tflApiPlaceTaxiRankVersionInfo {
                            
                            //logger.debug("tflApiPlaceTaxiRankEXISTING :\(tflApiPlaceTaxiRankInDictVersionInfoEXISTING.version)")
                            //logger.debug("tflApiPlaceTaxiRankNEW      :\(tflApiPlaceTaxiRankInArrayVersionInfoNEW.version)")
                            
                            if tflApiPlaceTaxiRankInDictVersionInfoEXISTING.version < tflApiPlaceTaxiRankInArrayVersionInfoNEW.version{
                                //logger.debug("REPLACING VERSION WITH HIGHER")
                                tflApiPlaceTaxiRankDictionary[tflApiPlaceTaxiRankInArrayVersionInfoNEW.rankIdUnversioned] = tflApiPlaceTaxiRankNEW
                            }else{
                                //logger.error("DONT REPLACE VERSION - SAME OR LOWER Version")
                            }
                            
                        }else{
                            logger.error("tflApiPlaceTaxiRankInDict.tflApiPlaceTaxiRankInfo is nil")
                        }
                        
                        
                    }else{
                        //all versions will be inserted at least once - after that entry will be replace so total version count wont inc
                        
                        countVersioned += 1
                        //versioned - but this is first time this unversioned key found - safe to insert
                        tflApiPlaceTaxiRankDictionary[tflApiPlaceTaxiRankInArrayVersionInfoNEW.rankIdUnversioned] = tflApiPlaceTaxiRankNEW
                    }
                    //----------------------------------------------------------------------------------------
                    
                }else{
                    //not versioned - just add to dictionary
                    
                    if let _ = tflApiPlaceTaxiRankDictionary[tflApiPlaceTaxiRankInArrayVersionInfoNEW.rankIdUnversioned]{
                        logger.error("CANT ADD OBJ AT KEY ALREADY:\(tflApiPlaceTaxiRankInArrayVersionInfoNEW.rankIdUnversioned)")
                    }else{
                        countUnVersioned += 1
                        tflApiPlaceTaxiRankDictionary[tflApiPlaceTaxiRankInArrayVersionInfoNEW.rankIdUnversioned] = tflApiPlaceTaxiRankNEW
                    }
                }
                
            }else{
                logger.error("tflApiPlaceTaxiRank.tflApiPlaceTaxiRankInfo is nil - cant process")
            }
            
        }//for
        
        //print("WS             : tflApiPlaceTaxiRankArray.count               :\(tflApiPlaceTaxiRankArray.count)")
        //print("a. Versions       : countVersions (even if 3 v only 1 is left in dict)  :\(countVersioned)")
        //print("b. Versions found : countUnVersioned                                    :\(countUnVersioned)")
        //print("tflApiPlaceTaxiRankDictionary.keys.count:\(tflApiPlaceTaxiRankDictionary.keys.count)")
        
        if (countVersioned + countUnVersioned) == tflApiPlaceTaxiRankDictionary.keys.count{
            //OK - each version only inserted once
            //logger.debug("SUCCESS (countVersioned:\(countVersioned) + countUnVersioned:\(countUnVersioned)) == tflApiPlaceTaxiRankDictionary.keys.count:\(tflApiPlaceTaxiRankDictionary.keys.count) - each version inserted once and highest version used")
        }else{
            logger.error("ERROR; (countVersioned:\(countVersioned) + countUnVersioned:\(countUnVersioned)) == tflApiPlaceTaxiRankDictionary.keys.count:\(tflApiPlaceTaxiRankDictionary.keys.count) FAILED")
        }
        
        //--------------------------------------------------------------
        // DICT - array sorted by key(TaxiRankId)
        //--------------------------------------------------------------
        let keysArrayUnsorted = Array(tflApiPlaceTaxiRankDictionary.keys)
        
        let keysArraySorted = keysArrayUnsorted.sorted { (string0, string1) -> Bool in
            if string0.compare(string1, options: .numeric) == .orderedAscending{
                return true
                
            }else{
                return false
            }
        }
        
        var tflApiPlaceTaxiRankArrayProcessed = [TFLApiPlaceTaxiRank]()
        for key_rankId in keysArraySorted{
            //print("key_rankId: \(key_rankId)")
            
            if let tflApiPlaceTaxiRank: TFLApiPlaceTaxiRank = tflApiPlaceTaxiRankDictionary[key_rankId] {
                
                
                //----------------------------------------------------------------------------------------
                //FIX BROKEN ranks - two in north sea
                //----------------------------------------------------------------------------------------
                
                //https://blog.tfl.gov.uk/2016/12/13/unified-api-taxi-ranks-added-to-places/comment-page-1/#comment-27049
                
                /*
                 ============================
                 "id": "TaxiRank_4045",
                 "url": "https://api-argon.tfl.gov.uk/Place/TaxiRank_4045",
                 "commonName": "Grosvenor Street",
                 "placeType": "TaxiRank",
                 "lat": 54.637877,
                 "lon": -0.005901
                 
                 54.637877, -0.005901
                 INCORRECT LOCATION
                 https://www.google.co.uk/maps/place/54%C2%B038'16.4%22N+0%C2%B000'21.2%22W/@54.6378801,-0.008095,17z/data=!3m1!4b1!4m5!3m4!1s0x0:0x0!8m2!3d54.637877!4d-0.005901
                 
                 Correct – 51.5120928,-0.1457605
                 https://www.google.co.uk/maps/place/51%C2%B030'43.5%22N+0%C2%B008'42.8%22W/@51.5120928,-0.1457605,19z/data=!3m1!4b1!4m5!3m4!1s0x0:0x0!8m2!3d51.512092!4d-0.145212
                 
                 ============================
                 
                 "id": "TaxiRank_4693",
                 "url": "https://api-argon.tfl.gov.uk/Place/TaxiRank_4693",
                 "commonName": "Station Road (Harold Wood Station), Harold Wood",
                 
                 "lat": 54.863954,
                 "lon": 0.410287
                 
                 54.863954, 0.410287
                 https://www.google.co.uk/maps/place/54%C2%B051'50.2%22N+0%C2%B024'37.0%22E/@54.8639571,0.408093,17z/data=!3m1!4b1!4m5!3m4!1s0x0:0x0!8m2!3d54.863954!4d0.410287
                 CORRECT LOCATION
                 51.5934154,0.2335873
                 https://www.google.co.uk/maps/place/Harold+Wood/@51.5934154,0.2335873,21z/data=!4m5!3m4!1s0x47d8bb78d96aa989:0x49d1c6ed3bcfeeb3!8m2!3d51.59277!4d0.2331
                 
                 ============================
                 
                 */
                // TODO: - CHECK THESE SAID THEY WERE FIXED - in north sea - check for versions
                if let rankId = tflApiPlaceTaxiRank.id  {
                    if rankId == "TaxiRank_4045"{
                        //noisy logger.error("HACK - two taxi ranks hard coded - TaxiRank_4045")
                        tflApiPlaceTaxiRank.lat = 51.5120928
                        tflApiPlaceTaxiRank.lon = -0.1457605
                        
                    }else if rankId == "TaxiRank_4693"{
                        //noisy logger.error("HACK - two taxi ranks hard coded - TaxiRank_4693")
                        tflApiPlaceTaxiRank.lat = 51.5934154
                        tflApiPlaceTaxiRank.lon = 0.2335873
                        
                    }else if rankId == "TaxiRank_5948"{
                        //noisy logger.error("HACK - London Bridge Bus Station (London Bridge Station) streetview underground - TaxiRank_5948")
                        tflApiPlaceTaxiRank.lat = 51.5049307
                        tflApiPlaceTaxiRank.lon = -0.0866951
                        
                    }
                    else{
                        //other rank - ignore
                    }
                }else{
                    //other rank - ignore
                }
                
                
                //----------------------------------------------------------------------------------------
                //APPEND
                //----------------------------------------------------------------------------------------
                tflApiPlaceTaxiRankArrayProcessed.append(tflApiPlaceTaxiRank)
                
                //----------------------------------------------------------------------------------------
            }else{
                logger.error("tflApiPlaceTaxiRankDictionary[key_rankId:'\(key_rankId)']  is nil")
            }
        }
        
        //----------------------------------------------------------------------------------------
        //ADD MISSING rank reported by drivers
        //----------------------------------------------------------------------------------------
        //https://www.google.co.uk/maps/@51.4741891,-0.1831795,3a,75y,277.97h,85.77t/data=!3m6!1e1!3m4!1sPukqQJmqcfCgHoBWAjquLg!2e0!7i13312!8i6656!6m1!1e1
        
        let tflApiPlaceTaxiRankNovotel = TFLApiPlaceTaxiRank.createWorkingRank(id:"TaxiRank_9999",
                                                                               commonName: "DoubleTree Imperial Wharf - Chelsea (Driver Reported)",
                                                                               lat: 51.4741891,
                                                                               lon: -0.183179,
                                                                               Borough: "Chelsea")
        
        tflApiPlaceTaxiRankArrayProcessed.append(tflApiPlaceTaxiRankNovotel)
        
        
        
        
        
        //----------------------------------------------------------------------------------------
        //15 feb - 580 in json + 1 added = 581
        //NOISYprint("FINAL: tflApiPlaceTaxiRankArrayProcessed :\(tflApiPlaceTaxiRankArrayProcessed.count)")
        return tflApiPlaceTaxiRankArrayProcessed
        
    }
    
    
    
    
    
    
}
