//
//  TFLApiPlaceCOLCQueryCollectionTaxiRank.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 27/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

class TFLApiPlaceCOLCQueryCollectionTaxiRank : TFLApiPlaceCOLCQueryCollection{
    
    override init() {
        super.init()
    }

    override func build_colcQueryDictionary() -> [String: COLCQuery]{
        
        //nearest/Name added in super
        var colcQueryDictionary_ : [String: COLCQuery] = super.build_colcQueryDictionary()
        
        //----------------------------------------------------------------------------------------
        //REFRESH
        //----------------------------------------------------------------------------------------
        //colcQueryDictionary_[TFLApiPlaceTaxiRank.colcQuery_RefreshmentRanks_sortByAtoZ().title] = TFLApiPlaceTaxiRank.colcQuery_RefreshmentRanks_sortByAtoZ()
        //self.colcQueryDictionaryKeys.append(TFLApiPlaceTaxiRank.colcQuery_RefreshmentRanks_sortByAtoZ().title)
        
        colcQueryDictionary_[TFLApiPlaceTaxiRank.colcQuery_RefreshmentRanks_sortByNearest().title] = TFLApiPlaceTaxiRank.colcQuery_RefreshmentRanks_sortByNearest()
        self.colcQueryDictionaryKeys.append(TFLApiPlaceTaxiRank.colcQuery_RefreshmentRanks_sortByNearest().title)
        
        //----------------------------------------------------------------------------------------
        //REST
        //----------------------------------------------------------------------------------------
        //colcQueryDictionary_[TFLApiPlaceTaxiRank.colcQuery_RestRanks_sortByAtoZ().title] = TFLApiPlaceTaxiRank.colcQuery_RestRanks_sortByAtoZ()
        //self.colcQueryDictionaryKeys.append(TFLApiPlaceTaxiRank.colcQuery_RestRanks_sortByAtoZ().title)
        
        colcQueryDictionary_[TFLApiPlaceTaxiRank.colcQuery_RestRanks_sortByNearest().title] = TFLApiPlaceTaxiRank.colcQuery_RestRanks_sortByNearest()
        self.colcQueryDictionaryKeys.append(TFLApiPlaceTaxiRank.colcQuery_RestRanks_sortByNearest().title)

        //----------------------------------------------------------------------------------------
        //Working
        //----------------------------------------------------------------------------------------
        //colcQueryDictionary_[TFLApiPlaceTaxiRank.colcQuery_WorkingRanks_sortByAtoZ().title] = TFLApiPlaceTaxiRank.colcQuery_WorkingRanks_sortByAtoZ()
        //self.colcQueryDictionaryKeys.append(TFLApiPlaceTaxiRank.colcQuery_WorkingRanks_sortByAtoZ().title)
        
        colcQueryDictionary_[TFLApiPlaceTaxiRank.colcQuery_WorkingRanks_sortByNearest().title] = TFLApiPlaceTaxiRank.colcQuery_WorkingRanks_sortByNearest()
        self.colcQueryDictionaryKeys.append(TFLApiPlaceTaxiRank.colcQuery_WorkingRanks_sortByNearest().title)
        
        //----------------------------------------------------------------------------------------
        //isStationRank = true
        //----------------------------------------------------------------------------------------
        colcQueryDictionary_[TFLApiPlaceTaxiRank.colcQuery_StationRanks_sortByNearest().title] = TFLApiPlaceTaxiRank.colcQuery_StationRanks_sortByNearest()
        colcQueryDictionaryKeys.append(TFLApiPlaceTaxiRank.colcQuery_StationRanks_sortByNearest().title)
        //----------------------------------------------------------------------------------------
        
    
        return colcQueryDictionary_
    }
    
}
