//
//  COLCMKAnnotation.swift
//  joyride
//
//  Created by Brian Clear on 11/11/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import MapKit

//CLPlacemark is hard to init but map only requires a class that IMPLEMENTS MKAnnotation
//so create our own
//drawn in:
//ParentPickerViewController
//public func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?{

class COLCMKAnnotation: NSObject, MKAnnotation {
    
    var title: String?
    var coordinate: CLLocationCoordinate2D
    var placemark: MKPlacemark
    
    var clLocation: CLLocation?{
        return CLLocation.init(latitude: coordinate.latitude, longitude: coordinate.longitude)
    }
    
    //Apple Address > MKPlacemark
    init(title: String, coordinate: CLLocationCoordinate2D, placemark: MKPlacemark) {
        self.title = title
        self.coordinate = coordinate
        self.placemark = placemark
    }
    
    //Google Address - no addressDictionary
    init(title: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.coordinate = coordinate
        self.placemark = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
    }
}
