//
//  GoogleMapUtils.swift
//  ClarksonsArc
//
//  Created by Brian Clear on 27/10/2015.
//  Copyright © 2015 Clarksons. All rights reserved.
//

import Foundation
import GoogleMaps

/*
 
Generate a map pin as UIImage.
We draw the text is a specific rectangle.
The size of the rect is calculated using sizeWithAttributes
to find the largest frame needed to display this text string at this font

ISSUES: Crash in Mapbox is all UIImages used are different sizes.
So I cheat and keep the UIImage at a fixed size.
I display names of ports so found the largest one possible
"Ginsheim-Gustavsburg"
found "Ginsheim-Gustavsburg".sizeWithAttributes
//    CGSize
//    - width : 135.826171875
//    - height : 14.3203125
So my UIImage had to be at least 135x14 so I chose
let sizeLabel:CGSize = CGSizeMake(200, 20)

To prevent lots of gaps I cheat and have an outer and inner rectangle fill then drawText over inner fill to mimic a UILabel
UIImage - CGSizeMake(200, 20)
CGFILL Background color : clear - rect (200,20)

INNER LABEL - Note not UILabel
CGFill -
drawText inner text
will vary depending on length of port name
"Ginsheim-Gustavsburg".sizeWithAttributes  =  CGSize(width : 135.826171875, height : 14.3203125)
"Hull".sizeWithAttributes  =  CGSize(width : 23.267578125, height : 14.3203125)

*/

let stormForecastCircleColor = UIColor.red.withAlphaComponent(0.1)

//used to color the shape
let k_PiracyZoneShape = "PiracyZoneShape"
let k_SecaZoneShape = "SecaZoneShape"
let k_StormShape = "StormShape"
class GoogleMapUtils{
    
    
    class func fillColorForPolygonAnnotation( _ annotation: GMSPolygon) -> UIColor{
        
        //alpha is applied by alphaForShapeAnnotation: above
        
        var fillColorForPolygonAnnotation = UIColor.white
        
        if let title = annotation.title{
            if title == k_PiracyZoneShape{
                fillColorForPolygonAnnotation = UIColor.red.withAlphaComponent(0.3)
                
            }
            else if annotation.title == k_SecaZoneShape{
                fillColorForPolygonAnnotation = UIColor.white.withAlphaComponent(0.3)
                
            }
            else if annotation.title == k_StormShape{
                fillColorForPolygonAnnotation = stormForecastCircleColor

            }
            else{
                print("ERROR: annotation.title is UNKNOWN:'\(String(describing: annotation.title))'")
            }
        }else{
            print("ERROR: annotation.title is nil - cant color shape - using default white")
        }
        return fillColorForPolygonAnnotation
    }
    
    class func strokeColorForGMSOverlay(_ annotation: GMSOverlay) -> UIColor {
        
        var colorReturned = UIColor.red
        
        if (annotation is GMSPolyline) {
            if annotation.title == "0"{
                colorReturned = UIColor.red
            }
            else if annotation.title == "1"{
                colorReturned = UIColor.blue
            }
            else if annotation.title == "2"{
                colorReturned = UIColor.magenta
            }
            else if annotation.title == "3"{
                colorReturned = UIColor.purple
            }
            else if annotation.title == "4"{
                colorReturned = UIColor.brown
            }else /*repeats*/
                if annotation.title == "5"{
                    colorReturned = UIColor.red
                }
                else if annotation.title == "6"{
                    colorReturned = UIColor.blue
                }
                else if annotation.title == "7"{
                    colorReturned = UIColor.magenta
                }
                else if annotation.title == "8"{
                    colorReturned = UIColor.purple
                }
                else if annotation.title == "9"{
                    colorReturned = UIColor.brown
                }
                else
                {
                    colorReturned = UIColor.blue
            }
        }
        else if (annotation is GMSPolygon) {
            
            if let title = annotation.title{
                
                if title == k_PiracyZoneShape{
                    colorReturned = UIColor.red
                    
                }else if annotation.title == k_SecaZoneShape{
                    colorReturned = UIColor.appColorGREEN()
                    
                }
                else if annotation.title == k_StormShape{
                    colorReturned = stormForecastCircleColor
                    
                }
                else{
                    print("ERROR: strokeColorForGMSPolyline annotation.title is nil - cant color shape - using default 1111")
                    colorReturned = UIColor.black
                }
            }else{
                print("ERROR: annotation.title is nil - cant color shape - using default 222")
            }
            
        }
        else
        {
            colorReturned = UIColor.black
        }
        return colorReturned
    }
    
    class func imageForMarker( _ marker: GMSMarker, clkPlace: CLKPlace) -> UIImage? {
        var annotationImage : UIImage? = nil
        
        var title = ""
        //OK but not used later commented out to hide warning - set but not used
        // var subTitle = ""
        
        //--------------------------------------------------
        //TITLE
        //--------------------------------------------------
        if let title_ = marker.title {
            title = title_
        }else{
            print("ERROR: marker.title is nil")
        }
        
        //--------------------------------------------------
        //SUBTITLE
        //--------------------------------------------------
        //        if let snippet_ = marker.title {
        //            subTitle = snippet_
        //        }else{
        //            logger.error("marker.title is nil")
        //            subTitle = ""
        //        }
        //---------------------------------------------------------------------

        if let placeType = clkPlace.placeType {
            switch placeType {
            case .Start:
                annotationImage = MapImageUtils.textToImage(title, iconColor: UIColor.appColorFlat_Emerald())
            case .End:
                annotationImage = MapImageUtils.textToImage(title, iconColor: UIColor.appColorFlat_Alizarin_Red())
            }
        }else{
    
            MyXCodeEmojiLogger.defaultInstance.error("clkPlace.placeType is nil")
            annotationImage = MapImageUtils.textToImage(title, iconColor: UIColor.appColorFlat_Emerald())
        }
        
        return annotationImage
    }
}


