//
//  GoogleMapsDistance.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 17/01/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
import CoreLocation
import GoogleMaps

class GoogleMapsDistance{
    
    //--------------------------------------------------------------
    // MARK: - Distance between CLLocations
    // MARK: -
    //--------------------------------------------------------------
    static var NotFound : CLLocationDistance = -1.0
    
    //Uses GMSGeometryDistance so not in CoreLocation utils
    class func distanceInMeters(from locationFrom: CLLocation, to locationTo: CLLocation) -> CLLocationDistance{
        
        var  distanceReturned : CLLocationDistance = GoogleMapsDistance.NotFound
        
        if locationFrom.isValid{
            if locationTo.isValid{
                let distanceMetersDouble : CLLocationDistance =  GMSGeometryDistance(locationFrom.coordinate, locationTo.coordinate)
                //print("distanceMetersDouble:\(distanceMetersDouble)")
                distanceReturned = distanceMetersDouble
                
            }else{
                logger.error("locationTo.isValid is FALSE:\(locationTo)")
            }
        }else{
            logger.error("locationFrom.isValid is FALSE:\(locationFrom)")
        }
        
        return distanceReturned
    }
    class func distanceInMetersFromCurrentLocation(to locationTo: CLLocation) -> CLLocationDistance{
        
        var  distanceReturned : CLLocationDistance = GoogleMapsDistance.NotFound
        
        
        if let currentLocation = appDelegate.currentLocationManager.currentLocation {
            if currentLocation.isValid{
                if locationTo.isValid{
                    let distanceMetersDouble : CLLocationDistance =  GMSGeometryDistance(currentLocation.coordinate, locationTo.coordinate)
                    //print("distanceMetersDouble:\(distanceMetersDouble)")
                    distanceReturned = distanceMetersDouble
                    
                }else{
                    logger.error("locationTo.isValid is FALSE:\(locationTo)")
                }
            }else{
                logger.error("currentLocation.isValid is FALSE:\(currentLocation)")
            }
        }else{
            logger.error("appDelegate.currentLocationManager.currentLocation is nil")
        }
        return distanceReturned
    }
}
