//
//  GMSMapView+Utils.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 17/01/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
import GoogleMaps

extension GMSMapView{
    
    func animateToBounds(bounds: GMSCoordinateBounds){

        if let gmsCameraPosition = self.camera(for: bounds, insets: UIEdgeInsets(top: 200, left: 50, bottom: 50, right: 50))
        {
            self.animate(to: gmsCameraPosition)

        }else{
            logger.error("gmsCameraPosition is nil for self.camera(for: bounds")
        }


    }
    
    //self.gmsMapView.animateToLocationWithPostcodeZoom(location: currentLocation)
    func animateToLocationWithPostcodeZoom(location: CLLocation){
        
        let cameraPosition = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                                      longitude: location.coordinate.longitude,
                                                      zoom:GoogleMapsZoomLevel.zoomlevel16_POSTCODE.floatValue())

        self.animate(to: cameraPosition)
        
    }
    //self.gmsMapView.animateToLocation(location: currentLocation, atZoom: GoogleMapsZoomLevel.zoomlevel17_STREET)
    func animateToLocation(location: CLLocation, atZoom googleMapsZoomLevel: GoogleMapsZoomLevel){
        
        let cameraPosition = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                                      longitude: location.coordinate.longitude,
                                                      zoom:googleMapsZoomLevel.floatValue())
        
        self.animate(to: cameraPosition)
        
    }

}
