//
//  GMSPanoramaCamera+Description.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 17/01/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
import GoogleMaps

//already conforms to CustomStringConvertible
//extension GMSPanoramaCamera: CustomStringConvertible{
extension GMSPanoramaCamera{

    open override var description: String {
        
        let description_ = "\r**** \(type(of: self)) START *****\r"
 
        //description_ = description_ + "fov:\(self.fov)\r"
        //description_ = description_ + "zoom:\(self.zoom)\r"

        //description_ = description_ + "orientation.pitch:\(self.orientation.pitch)\r"
        //description_ = description_ + "orientation.heading:\(self.orientation.heading)\r"
        //description_ = description_ +  "\r**** \(type(of: self)) END *****\r"
        
        return description_
    }
}

extension GMSPanoramaView{
    
    open override var description: String {
        
        let description_ = "\r**** \(type(of: self)) *****\r"
        //description_ = description_ + "camera:\(self.camera)\r"
        return description_
    }
}
