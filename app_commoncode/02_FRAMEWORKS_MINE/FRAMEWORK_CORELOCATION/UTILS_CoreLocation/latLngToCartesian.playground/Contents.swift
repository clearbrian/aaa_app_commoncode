//: Playground - noun: a place where people can play

import UIKit
import CoreLocation

//https://stackoverflow.com/questions/17456783/javascript-figure-out-point-y-by-angle-and-distance
func findNewPoint( x: Double,
                   y: Double,
                   angle: Double,
                   distance: Double) -> (Double, Double)
{
    //var result : (Double, Double)
    
    let resultx: Double = round(cos(angle * .pi / 180) * distance + x)
    let resulty: Double = round(sin(angle * .pi / 180) * distance + y)
    
    return (resultx, resulty)
}

var newPoint = findNewPoint(x:10,
                            y:20,
                            angle:10,
                            distance:200)

print("newPoint:\(newPoint)");



/*
 "steps": [
 {
 "distance": {
 "text": "49 m",
 "value": 49
 },
 "duration": {
 "text": "1 min",
 "value": 42
 },
 "end_location": {
 "lat": 51.5093262,
 "lng": -0.07364699999999999
 },
 "html_instructions": "Head <b>southeast</b> on <b>Tower Hill</b>/<b>A100</b> toward <b>Mansell St</b>/<b>A1210</b>",
 "polyline": {
 "points": "unkyHnmMBCPQZYXU"
 },
 "start_location": {
 "lat": 51.5097067,
 "lng": -0.07400139999999999
 },
 "travel_mode": "WALKING"
 },

 
 
 */
//
func degreesToRadians(degrees: Double) -> Double { return degrees * .pi / 180.0 }
func radiansToDegrees(radians: Double) -> Double { return radians * 180.0 / .pi }

func getBearing(fromPoint: CLLocationCoordinate2D,
                  toPoint: CLLocationCoordinate2D) -> Double {
    
    
    
    let lat1 = degreesToRadians(degrees: fromPoint.latitude)
    let lon1 = degreesToRadians(degrees: fromPoint.longitude)
    
    let lat2 = degreesToRadians(degrees: toPoint.latitude);
    let lon2 = degreesToRadians(degrees: toPoint.longitude);
    
    let dLon = lon2 - lon1;
    
    let y = sin(dLon) * cos(lat2);
    let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon);
    let radiansBearing = atan2(y, x);
    
    return radiansToDegrees(radians: radiansBearing)
}

var fromPoint = CLLocationCoordinate2D(latitude: 51.5097067, longitude: -0.07400139999999999)
var toPoint = CLLocationCoordinate2D(latitude: 51.5093262, longitude: -0.07364699999999999)

var bearing = getBearing(fromPoint: fromPoint, toPoint: toPoint)
print("bearing:\(bearing)");
//bearing:149.899299805773

//Distance
let fromLocation = CLLocation.init(latitude: fromPoint.latitude, longitude: fromPoint.longitude)
let toLocation = CLLocation.init(latitude: toPoint.latitude, longitude: toPoint.longitude)

let distance : CLLocationDistance = fromLocation.distance(from: toLocation)

print("distance:\(distance)");
//distance:48.9645385798274

//MATCHES http://www.movable-type.co.uk/scripts/latlong.html



