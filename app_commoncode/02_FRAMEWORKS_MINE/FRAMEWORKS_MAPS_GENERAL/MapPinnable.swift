//
//  Mappable.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 19/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

import UIKit

/*
 Any pin on any map - needs these thing
 
 * lat,lng
 * pin image
 * title
 * subtitle (optional)
 
 
 Google Maps
 Mapbox
 Apple Map
 
 */
protocol MapPinnable: CoreLocatable{
    
    
    func markerTitle() -> String
    func markerSnippet() -> String
    
    var markerImageName: String? {get}
    var markerImage: UIImage? {get}

}

/* EXAMPLE
 var markerImageName: String?{
 var markerImageNameRet: String? = nil
 
 return markerImageNameRet
 }
 
 var markerImage: UIImage?{
 var markerImageReturned: UIImage? = nil
 
 if let markerImageName = self.markerImageName {
 if let imageFound = UIImage.init(named: markerImageName) {
 markerImageReturned = imageFound
 }
 }else{
 logger.error("markerImageName not found in bundle:'\(markerImageName)'")
 }
 
 return markerImageReturned
 }
 
 
 //----------------------------------------------------------------------------------------
 //ANIMATED - never got it to work
 //----------------------------------------------------------------------------------------
 //  if let loading_1 = UIImage(named: "loading-1"){
 //      if let loading_2 = UIImage(named: "loading-2"){
 //          if let loading_3 = UIImage(named: "loading-3"){
 //              let imagesArray: [UIImage] = [loading_1, loading_2, loading_3]
 //
 //              let animatedImage = UIImage.animatedImage(with: imagesArray, duration: 3.0)
 //
 //              marker.icon = animatedImage
 //          }else{
 //              logger.error("loading_3 is nil")
 //          }
 //      }else{
 //          logger.error("loading_2 is nil")
 //      }
 //  }else{
 //      logger.error("loading_1 is nil")
 //  }
 */
