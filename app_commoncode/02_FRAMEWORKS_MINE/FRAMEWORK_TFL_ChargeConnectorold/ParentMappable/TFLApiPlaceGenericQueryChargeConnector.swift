//
//  TFLApiPlaceCOLCQueryCollectionChargeStation.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 27/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

class TFLApiPlaceCOLCQueryCollectionChargeStation : TFLApiPlaceCOLCQueryCollection{
    
    override init() {
        super.init()
        
    }
    override func build_colcQueryDictionary() -> [String: COLCQuery]{
       
        //Nearest/Name in parent
        var colcQueryDictionary_ : [String: COLCQuery] = super.build_colcQueryDictionary()
        
        
        
        //----------------------------------------------------------------------------------------
        //isAvailable = true
        //----------------------------------------------------------------------------------------
        colcQueryDictionary_[TFLApiPlaceChargeStation.colcQuery_isAvailable().title] = TFLApiPlaceChargeStation.colcQuery_isAvailable()
        colcQueryDictionaryKeys.append(TFLApiPlaceChargeStation.colcQuery_isAvailable().title)
        //----------------------------------------------------------------------------------------
        return colcQueryDictionary_
        
    }
    
}
