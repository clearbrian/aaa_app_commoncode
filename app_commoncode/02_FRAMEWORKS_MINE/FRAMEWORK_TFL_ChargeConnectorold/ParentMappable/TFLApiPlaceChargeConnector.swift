//
//  TFLApiPlaceChargeStationVersion.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 11/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation


class TFLApiPlaceChargeStation: TFLApiPlace{
    
    //--------------------------------------------------------------
    // MARK: - init - TO ADD MISSING ONES NOT IN JSON
    // MARK: -
    //--------------------------------------------------------------
    
    init(id : String,
         commonName : String,
         lat : Float,
         lon : Float
        )
    {
        //----------------------------------------------------------------------------------------
        super.init(id: id,
                   commonName: commonName,
                   lat: lat,
                   lon: lon,
                   placeType: "ChargeStation",
                   url: nil)
        
        // TODO: - CLEANUP after test
        //----------------------------------------------------------------------------------------
        //self.id = id
        //self.type = "Tfl.Api.Presentation.Entities.Place, Tfl.Api.Presentation.Entities"
        //self.additionalProperties =  [TFLApiPlaceAdditionalProperty]()
        //self.children = [AnyObject]()
        //self.childrenUrls = [AnyObject]()
        //
        //self.commonName = commonName
        //self.lat = lat
        //self.lon = lon
        //self.placeType = "ChargeStation"
        //self.url = nil
        //----------------------------------------------------------------------------------------
        
        //req else not put into dict properly
        //        self.tflApiPlaceChargeStationVersionInfo = TFLApiPlaceChargeStationVersionInfo.init(rankIdJSON: id,
        //                                                                                  rankIdUnversioned: id,
        //                                                                                  version: 0,
        //                                                                                  hasVersions: false)
        //----------------------------------------------------------------------------------------
    }
    
    required init?(map: Map){
        super.init(map: map)
        
    }
    
    //--------------------------------------------------------------
    // MARK: - MISSING RANKS
    // MARK: -
    //--------------------------------------------------------------
    class func  createChargeStation(id : String,
                                  commonName : String,
                                  lat : Float,
                                  lon : Float,
                                  Borough : String) -> TFLApiPlaceChargeStation
        
    {
        //----------------------------------------------------------------------------------------
        let tflApiPlaceChargeStation = TFLApiPlaceChargeStation(id: id,
                                                      commonName: commonName,
                                                      lat: lat,
                                                      lon: lon)
        
        tflApiPlaceChargeStation.appendAdditionalProperty(TFLApiPlaceAdditionalProperty(category: "Address", key: "Borough", sourceSystemKey: "3207", value: Borough))
        
        tflApiPlaceChargeStation.appendAdditionalProperty(TFLApiPlaceAdditionalProperty(category: "Description", key: "NumberOfSpaces", sourceSystemKey: "3207", value: ""))
        tflApiPlaceChargeStation.appendAdditionalProperty(TFLApiPlaceAdditionalProperty(category: "Description", key: "OperationDays", sourceSystemKey: "3207", value: ""))
        tflApiPlaceChargeStation.appendAdditionalProperty(TFLApiPlaceAdditionalProperty(category: "Description", key: "OperationTimes", sourceSystemKey: "3207", value: ""))
        
        tflApiPlaceChargeStation.appendAdditionalProperty(TFLApiPlaceAdditionalProperty(category: "Description", key: "RankType", sourceSystemKey: "3207", value: "Working"))
        //----------------------------------------------------------------------------------------
        return tflApiPlaceChargeStation
        
    }
    
    
    func appendAdditionalProperty(_ tflApiPlaceAdditionalProperty : TFLApiPlaceAdditionalProperty){
        ///let tflApiPlaceAdditionalPropertyBorough = TFLApiPlaceAdditionalProperty(category: "Address", key: "Borough", sourceSystemKey: "3207", value: "Chelsea")
        
        if let _ =  self.additionalProperties {
            self.additionalProperties?.append(tflApiPlaceAdditionalProperty)
        }else{
            appDelegate.log.error("self.additionalProperties is nil")
        }
    }
    
    
    
    //--------------------------------------------------------------
    // MARK: - VERSIONS
    // MARK: -
    //--------------------------------------------------------------
    //unversioned
    //[id:ChargeStation_5633] ...
    //versioned
    //[id:ChargeStation_5642-1] Bethnal Green Road (Tesco), Bethnal Green [51.5269, -0.061084] [RankType:Working]
    //[id:ChargeStation_5642-2] Bethnal Green Road (Tesco), Bethnal Green [51.5269, -0.061084] [RankType:Working]
    //--------------------------------------------------------------
    //var tflApiPlaceChargeStationVersionInfo : TFLApiPlaceChargeStationVersionInfo?
    //--------------------------------------------------------------
   // TODO: - remove from all except TaxiRank
    var isVersioned : Bool{
//        var isVersioned_ = false
        
//        if let idJSON = self.id {
//            if idJSON.contains("-"){
//                isVersioned_ = true
//            }else{
//                
//            }
//        }else{
//            
//        }
        return false
    }
    
    //--------------------------------------------------------------
    // MARK: - id
    // MARK: -
    //--------------------------------------------------------------
    
    override var id : String?{
        didSet {
            //to remove warnig
            
        }
    }


    
    
    //--------------------------------------------------------------
    // MARK: - ADDITIONAL PROPERTIES
    //--------------------------------------------------------------
    var availableString: String?{
        if let stringValue_ = self.findStringProperty_ChargeStation(.available_PropKey) {
            return stringValue_
        }else{
            appDelegate.log.error("self.findStringProperty_ChargeStation(.available_PropKey) is nil")
            return nil
        }
    }
    
    var isAvailable : Bool{
        var isAvailable_ = false

        if let availableString = self.availableString {
            
            if availableString == "true"{
                isAvailable_ = true
            }else{

            }
        }else{
            //nil - so false
        }
        return isAvailable_
    }
    
    var imageUrlString: String?{
        if let stringValue_ = self.findStringProperty_ChargeStation(.imageUrl_PropKey) {
            return stringValue_
        }else{
            appDelegate.log.error("self.findStringProperty_ChargeStation(.imageUrl_PropKey) is nil")
            return nil
        }
    }
    var videoUrlString: String?{
        if let stringValue_ = self.findStringProperty_ChargeStation(.videoUrl_PropKey) {
            return stringValue_
        }else{
            appDelegate.log.error("self.findStringProperty_ChargeStation(.imageUrl_PropKey) is nil")
            return nil
        }
    }
    
    var viewString: String?{
        if let stringValue_ = self.findStringProperty_ChargeStation(.view_PropKey) {
            //A10 Sth of St Peters Way N\\/B",
            return stringValue_.replace("\\/", with: "\\")
            
        }else{
            appDelegate.log.error("self.findStringProperty(.view_PropKey) is nil")
            return nil
        }
    }
    

    //--------------------------------------------------------------
    // MARK: - Search Additional Properties
    // MARK: -
    //--------------------------------------------------------------
    func findStringProperty_ChargeStation(_ tflAdditionalPropertyKey_ChargeStation: TFLAdditionalPropertyKey_ChargeStation) -> String?{
        
        let stringValue: String? = findStringPropertyForKeyRawValue(tflAdditionalPropertyKey_ChargeStation.rawValue)
        
        return stringValue
    }
    
    //.NumberOfSpaces,"123" >> Int: 123
    func findIntProperty_ChargeStation(_ tflAdditionalPropertyKey_ChargeStation: TFLAdditionalPropertyKey_ChargeStation) -> Int?{
        let intValue: Int? = findIntPropertyForKeyRawValue(tflAdditionalPropertyKey_ChargeStation.rawValue)
        
        return intValue
    }
    
    
    //--------------------------------------------------------------
    // MARK: - Description
    // MARK: -
    //--------------------------------------------------------------
    override var alertTitle: String{
        return self.descriptionLineMain
    }
    

    //alert message
    override var alertMessage: String{
        var description_ = ""
        
        //description_ = description_ +  "Available: \(Safe.safeString(self.availableString)), "
        //description_ = description_ +  "Viewing: \(Safe.safeString(self.viewString)), "
    
        return description_
    }
    
    //--------------------------------------------------------------
    // MARK: - Detail
    // MARK: -
    //--------------------------------------------------------------
    override var descriptionLineDetail0 : String {
        return "Viewing: \(Safe.safeString(self.viewString))"
    }
    
    override var descriptionLineDetail1 : String {
        return ""
    }
    
    //--------------------------------------------------------------
    // MARK: - SubDetail
    // MARK: -
    //--------------------------------------------------------------
    
    override var descriptionLineSubDetail0 : String {
        var description_ = ""
        
        //description_ = description_ +  "\(self.distanceToCurrentLocationFormatted)"
        
        return description_
    }
    
    override var descriptionLineSubDetail1 : String {
        return "Available: \(Safe.safeString(self.availableString))"
    }
    
    override var descriptionOneLine : String {
        return "[id:\(self.idSafe)] \(Safe.safeString(self.commonName)) [\(self.latStringSafe), \(self.lonStringSafe)] [Viewing:\(Safe.safeString(self.viewString))]"
    }
    
    //RANK,ChargeStation_5973,Park Plaza - County Hall Hotel, hotel forecourt,ChargeStation,-0.116399,51.5014,2,Mon - Sun,24 hours,Working,,
    override var descriptionCSV : String {
        
        var description_ = ""
        
        //description_ = description_ +  "ChargeStation,"
        //description_ = description_ +  "\(self.idSafe),"
        //description_ = description_ +  "\(Safe.safeString(self.commonName)),"
        //description_ = description_ +  "\(self.lonStringSafe),"
        //description_ = description_ +  "\(self.latStringSafe),"
        //description_ = description_ +  "\(Safe.safeString(self.availableString)),"
        //description_ = description_ +  "\(Safe.safeString(self.availableString)),"
        return description_
    }
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - MapPinnable
    //--------------------------------------------------------------
    //used by GoogleMappable gmsMarker > markerImageName> markerImage > UI
    //DO not override MapPinnable > mapImage - change the image name in assets here
    //DO not override GoogleMappable - you can overdide something in an extention
    //overriing markerImageName and changing the final image name based on rules in the subclass e.g ChargeStation.rankType
    //-------------------------------------------------------------------
    
    override var markerImageName: String?{
        var markerImageNameRet: String? = nil
        
        //----------------------------------------------------------------------------------------
        //PIN NAME - varies by type
        //----------------------------------------------------------------------------------------
        // TODO: - move the filename into the RankType
        //        if let rankType = self.rankType {
        //            var imageName = "taxipin_working"
        //
        //            switch rankType{
        //            case .unknown:
        //                imageName = "taxipin_unknown"
        //
        //
        //            case .working:
        //                imageName = "taxipin_working"
        //
        //            case .rest_rank:
        //                imageName = "taxipin_rest_rank"
        //
        //
        //            case .refreshment_rank:
        //                imageName = "taxipin_refreshment_rank"
        //            }
        //            markerImageNameRet = imageName
        //        }else{
        //            appDelegate.log.error("self.rankType is nil")
        //        }
        //-------------------------------------------------------------------
        //SCALABLE SVG of taxi
        //-------------------------------------------------------------------
        // TODO: - pin for others
        markerImageNameRet = "map_pin_flat_alpha"
        return markerImageNameRet
    }
    
    
    //--------------------------------------------------------------
    // MARK: - COLCQueryCollection
    // MARK: -
    //--------------------------------------------------------------
    //cant override static -
    //http://stackoverflow.com/questions/29189700/overriding-static-vars-in-subclasses-swift-1-2
    
    override class var colcQueryCollectionForType : COLCQueryCollection{
        return TFLApiPlaceCOLCQueryCollectionChargeStation()
    }
    
    //--------------------------------------------------------------
    // MARK: - map json Property string to type to query on
    // MARK: -
    //--------------------------------------------------------------

    fileprivate static let propertyName_isAvailable = "isAvailable"

    override func value(forPropertyName propertyName: String) -> Any?{
        
        if let valueAny = super.value(forPropertyName: propertyName) {
            return valueAny
        }else{
            
            //not found in parent - may be specific to property
            switch propertyName{
                
            case TFLApiPlaceChargeStation.propertyName_isAvailable:
                return self.isAvailable
                
            default:
                appDelegate.log.error("[TFLApiPlaceChargeStation] UNHANDLED PROPERTY: value:forPropertyName:'\(propertyName) - JAM CAM'")
                return nil
            }
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - Factory queries - others in parent
    // MARK: -
    //--------------------------------------------------------------
    class func colcQuery_isAvailable() -> COLCQuery{
        
        //----------------------------------------------------------------------------------------
        //e.g. COLCFilterBool - SEARCH for isAvailable = true
        //----------------------------------------------------------------------------------------
        
        
//        
//        let colcQuery = COLCQuery.colcQuery_COLCFilterBool(title        : "Available",
//                                                           propertyName : TFLApiPlaceChargeStation.propertyName_isAvailable,
//                                                           searchBool   : true,
//                                                      searchPropertyName: TFLApiPlace.propertyName_commonName)
        //----------------------------------------------------------------------------------------
        let colcQuery = TFLApiPlace.COLCQuery_defaultSearchQuery(title:"Available")
        
        let colcFilterBool = COLCFilterBool(propertyName: TFLApiPlaceChargeStation.propertyName_isAvailable, searchBool: true)
        
        colcQuery.appendCOLCFilter(colcFilterBool)
        //----------------------------------------------------------------------------------------
        return colcQuery
    }
    
}
