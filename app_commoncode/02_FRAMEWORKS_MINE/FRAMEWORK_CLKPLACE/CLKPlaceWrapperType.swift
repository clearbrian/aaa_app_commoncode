//
//  CLKPlaceWrapperType.swift
//  joyride
//
//  Created by Brian Clear on 14/10/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation

//places can be found in two ways
//PlacesPicker  returns GMSPlace
//API
//    API/nearbysearch returns CLKGooglePlaceResult
//    API/place_detail returns JSON >> clkPlaceSearchResponse : CLKGooglePlaceSearchResponse  which contains //var results: [CLKGooglePlaceResult]?


enum CLKPlaceWrapperType: Int{
    case clkPlace_Default = 0                                   /* a blank object created from scratch  CLKPlace() not as result of webservice call */
    case geoCodeLocation = 1                                    /* move map lat,lng >> address only */
    case nearbySearch_CLKGooglePlaceResult = 2
    case nearbySearch_CLKGooglePlaceResult_GeoCodeLocation = 3  /* used name from Nearby search but address for GeoCodeLocation*/
    case geocodedContact = 4                                    /* CONTACTS */
    case geocodedCalendarEvent = 5                              /* Calendar/MKMapItem for EKEvent.location geocoded */
    case colcFacebookEvent = 6                                  /* FB EVENT */
    
    
    func nameReadable() -> String{
        switch self{
        case .clkPlace_Default:
            return "Place"
            
        case .geoCodeLocation:
            return "Geocoded Location"
            
        case .nearbySearch_CLKGooglePlaceResult:
            return "Google Business"
            
        case .nearbySearch_CLKGooglePlaceResult_GeoCodeLocation:
            return "Google Business / Street Address"
            
        case .geocodedContact:
            return "Contact"
            
        case .geocodedCalendarEvent:
            return "Calendar"
            
        case .colcFacebookEvent:
            return "Facebook Event"
            
        }
    }
    
    //store rawvalue in db
    static func clkPlaceWrapperTypeForRaw(_ rawValue:Int) -> CLKPlaceWrapperType{
        switch rawValue{
        case CLKPlaceWrapperType.clkPlace_Default.rawValue:    // means we only have to set o...1...2.. when we define the enum
            return .clkPlace_Default
            
        case CLKPlaceWrapperType.geoCodeLocation.rawValue:
            return .geoCodeLocation
            
        case CLKPlaceWrapperType.nearbySearch_CLKGooglePlaceResult.rawValue:
            return .nearbySearch_CLKGooglePlaceResult
            
        case CLKPlaceWrapperType.nearbySearch_CLKGooglePlaceResult_GeoCodeLocation.rawValue:
            return .nearbySearch_CLKGooglePlaceResult_GeoCodeLocation
            
        case CLKPlaceWrapperType.geocodedContact.rawValue:
            return .geocodedContact
            
        case CLKPlaceWrapperType.geocodedCalendarEvent.rawValue:
            return .geocodedCalendarEvent
            
        case CLKPlaceWrapperType.colcFacebookEvent.rawValue:
            return .colcFacebookEvent
            
        default:
            MyXCodeEmojiLogger.defaultInstance.error("clkPlaceWrapperTypeForRaw UNKNOWN rawValue:\(rawValue)")
            return .clkPlace_Default
        }
    }
}
