//
//  SchipolFlightsAllRequest.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 31/03/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

//https://api.schiphol.nl/public-flights/flights

class SchipolFlightsRequest : SchipolApiParentWSRequest{
    
 /*
    ## FlightsAllTodayRequest
    curl "https://api.schiphol.nl/public-flights/flights?app_id=69674e7b&app_key=842864977d6c203283bcca199ffc9944&scheduledate=2017-03-31&scheduletime=13:00&includedelays=false&page=0&sort=%2Bscheduletime" \
    -H "Cookie: 1dfcccc8ae287a8b7f6b99a3be27b6fb=e0786da622773154de01fd429db7c234" \
    -H "Accept: application/json" \
    -H "Resourceversion: v3"
  */
    
    
//    https://api.schiphol.nl/public-flights/flights?app_id=69674e7b
//    &app_key=842864977d6c203283bcca199ffc9944
//    &scheduledate=2017-03-31
//    &scheduletime=13:00
//    &includedelays=false
//    &page=0
//    &sort=%2Bscheduletime"
    
    //HEADER ResourceVersion: "v3"
    
    
    var scheduledate:String?
    var scheduletime:String?
    var flightname:String?
    var flightdirection:String?
    var airline:String?
    var nvlscode:String?
    var route:String?
    var includedelays:String?
    var page:String?
    var sort:String?
    var fromdate:String?
    var todate:String?
    
    
    //should be set by subclass
    override init(){
        super.init()
        
    }
    //https://api.schiphol.nl/public-flights/flights?app_id=69674e7b&app_key=842864977d6c203283bcca199ffc9944&scheduledate=2017-03-31&scheduletime=13:00&includedelays=false&page=0&sort=%2Bscheduletime"

    override var webServiceEndpoint :String? {
        get {
            
            let webServiceEndpoint_ : String? = nil
            
            if let webServiceEndpoint = super.webServiceEndpoint {
                
                
//                webServiceEndpoint_ = "\(webServiceEndpoint)/flights/\(self.tflApiPlaceType.apiStringForType)"
                
                
            }else{
                logger.error("super.webServiceEndpoint is nil")
            }
            
            return webServiceEndpoint_
        }
    }
    
//    convenience init(){
//        self.init()
//        
//    }
    
    override var urlString :String? {
        get {
            var urlString_ :String?
            
            if let query = query{
                if let webServiceEndpoint = self.webServiceEndpoint {
                    urlString_ = "\(webServiceEndpoint)?\(query)"
                    
                    // TODO: - cleanup
                    let _ = parametersAppendAndCheckRequiredFieldsAreSet()
                    
                }else{
                    logger.error("self.webServiceEndpoint is nil")
                }
                
            }else{
                logger.error("query is nil - should be set by subclass")
            }
            
            return urlString_
        }
    }
    
    override func parametersAppendAndCheckRequiredFieldsAreSet() -> Bool{

        var requiredFieldsAreSet = false
        
        let requiredFieldsAreSetInSuper = super.parametersAppendAndCheckRequiredFieldsAreSet()
        if requiredFieldsAreSetInSuper{
            
            //------------------------------------------------
            //REQUIRED
            //------------------------------------------------
            //if let origin = origin{
            //    if let destination = destination{
            //        //------------------------------------------------
            //        // TODO: - DOESNT HANDLED blanks
            //        parameters["origin"] = origin as AnyObject?
            //        parameters["destination"] = destination as AnyObject?
            //        if origin == destination{
            //            logger.error("origin and destination are same")
            //        }else{
            //            
            //        }
            //        requiredFieldsAreSet = true
            //        //------------------------------------------------
            //    }else{
            //        logger.error("radius is nil")
            //    }
            //}else{
            //    logger.error("origin is nil")
            //}
            //------------------------------------------------
            requiredFieldsAreSet = true
            //------------------------------------------------
            if requiredFieldsAreSet{
                //---------------------------------------------------------------------
                //OPTIONAL PARAMETERS
                //---------------------------------------------------------------------
            
                self.addToParametersIfNotNil("scheduledate", string: self.scheduledate)
                self.addToParametersIfNotNil("scheduletime", string: self.scheduletime)
                self.addToParametersIfNotNil("flightname", string: self.flightname)
                self.addToParametersIfNotNil("flightdirection", string:self.flightdirection)
                self.addToParametersIfNotNil("airline", string:self.airline)
                self.addToParametersIfNotNil("nvlscode", string:self.nvlscode)
                self.addToParametersIfNotNil("route", string:self.route)
                self.addToParametersIfNotNil("includedelays", string:self.includedelays)
                self.addToParametersIfNotNil("page", string:self.page)
                self.addToParametersIfNotNil("sort", string:self.sort)
                self.addToParametersIfNotNil("fromdate", string:self.fromdate)
                self.addToParametersIfNotNil("todate", string:self.todate)

               
                //---------------------------------------------------------------------
            }else{
                logger.error("requiredFieldsAreSet is false - skip OPTIONAL FIELDS")
            }
            //------------------------------------------------
        }else{
            //required param missing in super class
            requiredFieldsAreSet = false
        }
        
        return requiredFieldsAreSet
    }
    
    

}


//----------------------------------------------------------------------------------------
//DOCS
//----------------------------------------------------------------------------------------
/*
 //DEFAULTS TO TODAY
 curl -X GET --header "Accept: application/json" --header "ResourceVersion: v3" "https://api.schiphol.nl/public-flights/flights?app_id=69674e7b&app_key=842864977d6c203283bcca199ffc9944&includedelays=false&page=0&sort=%2Bscheduletime"
 
 
Parameter	Value	Description	Parameter Type	Data Type
app_id
 69674e7b
Your API app id	query	string
 app_key
    842864977d6c203283bcca199ffc9944
Your API app key	query	string
    ResourceVersion
    v3
    Resource version of the API	header	string
 
 
scheduledate
    2017-03-31
    Scheduled date to get flights for.
    Format: yyyy-MM-dd.
    Defaults to today if not provided
    query	string
scheduletime
    13:00
    Scheduled time to get flights from. Format: HH:mm	query	string
flightname
    Flight number as printed on the ticket	query	string
flightdirection
    Direction of the flight	query	string
airline
 Prefix in flight number as printed on the ticket (size: 2 or 3 characters), For exmaple: KL	query	string
nvlscode
 NVLS code of a airliner	query	integer
route
 IATA or ICAO code of airport in route; multiple values, comma separated	query	string
includedelays
 Include delays when searching for a flight	query	boolean
 
 */
