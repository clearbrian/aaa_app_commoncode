//
//  HailoKit.swift
//  joyride
//
//  Created by Brian Clear on 16/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation

//class HailoKit : ParentSwiftObject{

//---------------------------------------------------------------------
//    
//    /*
//    https://developer.hailoapp.com/applications
//
//    support@cityoflondonconsulting.com
//    teV9mef0uT9hoJ0wauT3eM6vE

//  https://developer.hailoapp.com/docs

//  clearbrian@googlemail.com - DONT USE
//  norcomm2!


//---------------------------------------------------------------------
//    
//    KEY - MUST URLENCODE IT
//    cDB21dEEc5S5dUZfokUgXeL5CjSGA+o9bkiM5bC3FSndVihz5S4OkRb9RO0hI5YQ4B2FxGrTipmEsfFW/zYb4CXk2dGoK5TDbaAKTI6b+gIOnoywEVUeJm8/2R78iarX3iqw5e3ruSAVDoMErmw/vKbVrFodQMtjtB+jrAkLkTpO1rZLmfjympUVOQSn/znAR//rUum71aKBOS01QOFXBQ==
//
//    //OK - driver/eta
//    https://api.hailoapp.com/drivers/eta?latitude=51.510761&longitude=0.1174437&api_token=cDB21dEEc5S5dUZfokUgXeL5CjSGA%2Bo9bkiM5bC3FSndVihz5S4OkRb9RO0hI5YQ4B2FxGrTipmEsfFW%2FzYb4CXk2dGoK5TDbaAKTI6b%2BgIOnoywEVUeJm8%2F2R78iarX3iqw5e3ruSAVDoMErmw%2FvKbVrFodQMtjtB%2BjrAkLkTpO1rZLmfjympUVOQSn%2FznAR%2F%2FrUum71aKBOS01QOFXBQ%3D%3D
//
//    //--------------------------------------------------------------------
//    {
//    "etas": [
//        {
//            "count": 1,
//            "eta": 18,
//            "service_type": "regular"
//        }
//        ]
//    }
//    */
//    
//    //can be passed in Authorization header but never got it to work
//    let kHAILO_CLIENT_ID:String = "cDB21dEEc5S5dUZfokUgXeL5CjSGA+o9bkiM5bC3FSndVihz5S4OkRb9RO0hI5YQ4B2FxGrTipmEsfFW/zYb4CXk2dGoK5TDbaAKTI6b+gIOnoywEVUeJm8/2R78iarX3iqw5e3ruSAVDoMErmw/vKbVrFodQMtjtB+jrAkLkTpO1rZLmfjympUVOQSn/znAR//rUum71aKBOS01QOFXBQ=="
//    
//    //USE IN API
//    //api_token= kUBER_CLIENT_ID_ENCODED
//    let kHAILO_CLIENT_ID_ENCODED:String = "cDB21dEEc5S5dUZfokUgXeL5CjSGA%2Bo9bkiM5bC3FSndVihz5S4OkRb9RO0hI5YQ4B2FxGrTipmEsfFW%2FzYb4CXk2dGoK5TDbaAKTI6b%2BgIOnoywEVUeJm8%2F2R78iarX3iqw5e3ruSAVDoMErmw%2FvKbVrFodQMtjtB%2BjrAkLkTpO1rZLmfjympUVOQSn%2FznAR%2F%2FrUum71aKBOS01QOFXBQ%3D%3D"
//    
//    
//    //https://itunes.apple.com/gb/app/hailo-taxi-app.-book-personal/id468420446?mt=8
//    static let openAppConfig = OpenAppConfigDeepLinking(openWithType : OpenWithType.OpenWithType_Hailo,
//                                                        appSchemeString     : "hailoapp://",
//                                                        appDeepLinkURLString: "",
//                                                        appWebsiteURLString : "https://www.hailoapp.com/",
//                                                        appStoreIdUInt      : 468420446)
//    
//    
//    
//    //--------------------------------------------------------------
//    // MARK: -
//    // MARK: - Deep Linking
//    //--------------------------------------------------------------
//    //hailoapp://confirm?pickupCoordinate=51.511807,-0.117389&pickupAddress=Somerset%20House,%20Strand&destinationCoordinate=51.514996,-0.098970&destinationAddress=London%20Stock%20Exchange&pickupLocality=London&destinationLocality=London&referrer=your_hailo_application_token"];
//    //--------------------------------------------------------------
//    //    hailoapp://confirm?pickupCoordinate=51.511807,-0.117389
//    //    &pickupAddress=Somerset%20House,%20Strand
//    //    &destinationCoordinate=51.514996,-0.098970
//    //    &destinationAddress=London%20Stock%20Exchange
//    //    &pickupLocality=London
//    //    &destinationLocality=London
//    //    &referrer=your_hailo_application_token
//    //--------------------------------------------------------------
//
//    func openTripInHailo(hailoPlacePickup hailoPlacePickup:HailoPlace, hailoPlaceDestination:HailoPlace)
//    {
//        let urlString_ = "hailoapp://"
//        logger.debug("OPENING:\r\(urlString_)")
//        
//        if let urlHailoApp = NSURL(string:urlString_){
//            if (UIApplication.sharedApplication().canOpenURL(urlHailoApp))
//            {
//               
//                var urlString = ""
//                
//                urlString = urlString + "hailoapp://confirm?"
//                
//                urlString = urlString + hailoPlacePickup.generateParamsForHailoPlace("pickup")
//                urlString = urlString + hailoPlaceDestination.generateParamsForHailoPlace("destination")
//                
//                urlString = urlString + "&referrer=\(kHAILO_CLIENT_ID_ENCODED)"
//                
//                //---------------------------------------------------------------------
//                //DEBUG
//                //---------------------------------------------------------------------
//                logger.debug("urlString:\(urlString)")
//                //DEBUG
//                let lines = urlString.componentsSeparatedByString("&")
//                for line in lines{
//                    logger.debug("\(line)")
//                }
//                //---------------------------------------------------------------------
//                if let url = NSURL(string:urlString){
//                    UIApplication.sharedApplication().openURL(url)
//                }else{
//                    logger.error("url is nil - not valid:")
//                }
//            }
//            else
//            {
//                if let urlHailoWebsite = NSURL(string:"https://hailo.com") {
//                    UIApplication.sharedApplication().openURL(urlHailoWebsite)
//                }else{
//                    logger.error("urlHailoApp is nil - 'https://hailo.com' not valid")
//                }
//            }
//        }else{
//            logger.error("urlHailoApp is nil - 'hailo://' not valid")
//        }
//        
//    }
//
//    func openTripInHailo(clkPlaceStart clkPlaceStart: CLKPlace?,
//                                       clkPlaceEnd: CLKPlace?,
//                                       presentingViewController: UIViewController)
//    {
//        if let clkPlaceStart = clkPlaceStart{
//            if let clkPlaceEnd = clkPlaceEnd{
//                //-----------------------------------------------------------------------------------
//                if let hailoPlacePickup : HailoPlace = HailoPlace.convert_CLKPlace_To_HailoPlace(clkPlaceStart, hailoPlaceType : .Pickup){
//                    if let hailoPlaceDestination: HailoPlace = HailoPlace.convert_CLKPlace_To_HailoPlace(clkPlaceEnd, hailoPlaceType : .Destination){
//                        //---------------------------------------------------------------
//                        
//                        self.openTripInHailo(hailoPlacePickup: hailoPlacePickup,
//                                             hailoPlaceDestination: hailoPlaceDestination)
//                        //---------------------------------------------------------------
//                    }else{
//                        logger.error("hailoPlaceDestination is nil")
//                    }
//                }else{
//                    logger.error("hailoPlacePickup is nil ")
//                }
//                //-----------------------------------------------------------------------------------
//                
//            }else{
//                logger.error("self.clkPlaceEnd is nil")
//            }
//        }else{
//            logger.error("clkPlaceStart is nil")
//        }
//    }
//}
