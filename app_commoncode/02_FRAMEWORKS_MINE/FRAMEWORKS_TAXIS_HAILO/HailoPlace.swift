//
//  HailoPlace.swift
//  joyride
//
//  Created by Brian Clear on 16/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation

//-----------------------------------------------------------------------------------
//hailoapp://confirm?pickupCoordinate=51.511807,-0.117389&pickupAddress=Somerset%20House,%20Strand&destinationCoordinate=51.514996,-0.098970&destinationAddress=London%20Stock%20Exchange&pickupLocality=London&destinationLocality=London&referrer=your_hailo_application_token"];
//-----------------------------------------------------------------------------------
//    hailoapp://confirm?
//    pickupCoordinate=51.511807,-0.117389

//    &pickupAddress=Somerset%20House,%20Strand
//    &destinationCoordinate=51.514996,-0.098970
//    &destinationAddress=London%20Stock%20Exchange

//LOCALITY
//    &pickupLocality=London
//    &destinationLocality=London

//    &referrer=your_hailo_application_token

//https://developer.hailoapp.com/docs#deeplink
//-----------------------------------------------------------------------------------

enum HailoPlaceType : String {
    case Pickup = "pickup" //used in deeplink url
    case Destination = "destination" //used in deeplink url
}

class HailoPlace : ParentSwiftObject{
    
    var lat : Double?
    var lng : Double?
    
    var coordinateString : String?{
        set
        {
            
        }
        get {
            var coordinateString_ : String?
            if let lat = lat{
                if let lng = lng{
                    coordinateString_ = "\(lat),\(lng)"
                }else{
                    logger.error("lng is nil")
                }
            }else{
                logger.error("lat is nil")
            }
        
            return coordinateString_
        }
    }
    //name + formatted_address for CLKPlaceResponse
    var address : String?

    //City
    var locality : String?
    
    var hailoPlaceType : HailoPlaceType?
    
    override init(){
        
    }
    init(hailoPlaceType : HailoPlaceType){
        self.hailoPlaceType = hailoPlaceType
    }
    
    
//    static func convert_CLKPlace_To_HailoPlace(clkPlace: CLKPlace, hailoPlaceType: HailoPlaceType) -> HailoPlace?{
//        
//        var hailoPlaceTypeReturned: HailoPlace?
//        if let clLocation = clkPlace.clLocation{
//            
//            let lat = clLocation.coordinate.latitude
//            let lng = clLocation.coordinate.longitude
//            
//            
//            if let name = clkPlace.name{
//                if let formatted_address = clkPlace.formatted_address{
//                    // TODO: - LONDON IS HARDCODED
//                    hailoPlaceTypeReturned = HailoPlace.createHailoPlace(
//                        lat: lat,
//                        lng: lng,
//                        locality: "London",
//                        formatted_address: formatted_address,
//                        name : name,
//                        hailoPlaceType: hailoPlaceType
//                    )
//                    
//                }else{
//                    logger.error("clkPlace.formatted_address is nil")
//                }
//            }else{
//                logger.error("clkPlace.name is nil")
//            }
//        }else{
//            logger.error("clkPlace.clLocation is nil")
//        }
//        return hailoPlaceTypeReturned
//        
//    }
    
    static func convert_CLKPlace_To_HailoPlace(_ clkPlace: CLKPlace, hailoPlaceType: HailoPlaceType) -> HailoPlace?{
         
        var hailoPlaceTypeReturned: HailoPlace?
        if let clLocation = clkPlace.clLocation{
            
            let lat = clLocation.coordinate.latitude
            let lng = clLocation.coordinate.longitude
            
            //------------------------------------------------------------------------------------------------
            //NAME
            //------------------------------------------------------------------------------------------------
            //if geocode then name not set
            //.GeoCodeLocation name is blank so put on in then overwrite if it is set
            
            //------------------------------------------
           
            // TODO: - test removed fallback: method
            //let nameToShow = clkPlace.name(fallbackNameString: hailoPlaceType.rawValue.uppercased())
            
            var nameToShow = ""
            if let name = clkPlace.name {
                nameToShow = name
            }else{
                logger.error("clkPlace.name is nil: using '\(hailoPlaceType.rawValue.uppercased())'")
                nameToShow = hailoPlaceType.rawValue.uppercased()
            }
            
            //------------------------------------------------------------------------------------------------
            //LOCALITY
            //------------------------------------------------------------------------------------------------
            // TODO: - test this in unknown locality
            if let locality = OpenInAppPlace.localityForCLKPlace(clkPlace){
                if let formatted_address = clkPlace.formatted_address{
                    
                    hailoPlaceTypeReturned = createHailoPlace(
                        lat: lat,
                        lng: lng,
                        locality: locality, //e.g. London
                        formatted_address: formatted_address,
                        name : nameToShow,
                        hailoPlaceType: hailoPlaceType
                    )
                    
                }else{
                    print("ERROR:clkPlace.formatted_address is nil")
                }
            }else{
                print("ERROR:locality is nil cant create Hailo Place - OpenInAppPlace.localityForCLKPlace failed ")
            }
            
            //-----------------------------------------------------------------------------------
            
        }else{
            print("ERROR:clkPlace.clLocation is nil")
        }
        return hailoPlaceTypeReturned
        
    }
    
    
    //May be created from user picking place (clkPlaceSearchResponse) or from lat/lng with coordinate and geocoded address
    static func createHailoPlace(lat:Double, lng:Double, locality: String, formatted_address: String, name : String, hailoPlaceType: HailoPlaceType) -> HailoPlace
    {
        
        //------------------------------------------------------------------------------------------------
        //hailoapp://confirm?
        //---------------------------------------------------------------------
        //pickupCoordinate=51.511807,-0.117389
        //&pickupAddress=Somerset%20House,%20Strand
        //&pickupLocality=London
        //---------------------------------------------------------------------
        //&destinationCoordinate=51.514996,-0.098970
        //&destinationAddress=London%20Stock%20Exchange
        //&destinationLocality=London
        //---------------------------------------------------------------------
        //&referrer=your_hailo_application_token
        //---------------------------------------------------------------------
        //------------------------------------------------------------------------------------------------
        
        
        let hailoPlace = HailoPlace()
        //---------------------------------------------------------------------
        hailoPlace.lat = lat
        hailoPlace.lng = lng
        //---------------------------------------------------------------------
        // TODO: - LONDON IS HARDCODED
        //find where hailo is then find nearest point from users location
        //"administrative_area_level_2"
        //"political"
        hailoPlace.locality = locality
        //---------------------------------------------------------------------
        //DONT ADD NAME IF ADDRESS STARTS WITH NAME AS WELL
        //- "name" : "Claredale House", can be in address Optional("Claredale House, Claredale House, Claredale Street, London E2 6PE, United Kingdom")
        //you get "Claredale House", Claredale House, Claredale House, Claredale Street, London E2 6PE, United Kingdom"
        
        if formatted_address.hasPrefix(name){
            hailoPlace.address = "\(formatted_address)"
        }else{
            hailoPlace.address = "\(name), \(formatted_address)"
        }
        //---------------------------------------------------------------------
        hailoPlace.hailoPlaceType = hailoPlaceType
        //---------------------------------------------------------------------
        return hailoPlace
    }
    
    
    //    hailoapp://confirm?
    //
    //    pickupCoordinate=51.511807,-0.117389
    //    &pickupAddress=Somerset%20House,%20Strand
    //    &pickupLocality=London
    //
    //    &destinationCoordinate=51.514996,-0.098970
    //    &destinationAddress=London%20Stock%20Exchange
    //    &destinationLocality=London
    //
    //    &referrer=your_hailo_application_token
    
    //type : "dropoff"/"pickup"
    func generateParamsForHailoPlace(_ type: String) -> String{
        
        var urlString = ""
        //---------------------------------------------------------------------
        //pickupCoordinate=51.511807,-0.117389
        //destinationCoordinate=51.514996,-0.098970
        //---------------------------------------------------------------------
        if let coordinateString = self.coordinateString{
            
            urlString = urlString + "&\(type)Coordinate=\(coordinateString)"
        }else{
            logger.error("hailoPlace.coordinateString is nil")
        }
        
        //---------------------------------------------------------------------
        if let address = self.address{
            //            if let addressEncoded = address.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding){
            //
            //                urlString = urlString + "&\(type)Address=\(addressEncoded)"
            //
            //            }else{
            //                logger.error("address.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding is nil")
            //            }
            
            let addressEncoded = address.addingPercentEncoding( withAllowedCharacters: CharacterSet.urlQueryAllowed)
            
            if let addressEncoded = addressEncoded{
                urlString = urlString + "&\(type)Address=\(addressEncoded)"
                
            }else{
                logger.error("address.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding is nil")
            }
        }else{
            logger.error("hailoPlace.formatted_address is nil")
        }
        //---------------------------------------------------------------------
        if let locality = self.locality{
            
            urlString = urlString + "&\(type)Locality=\(locality)"
        }else{
            logger.error("hailoPlace.locality is nil")
        }
        //---------------------------------------------------------------------
        return urlString
    }
}
