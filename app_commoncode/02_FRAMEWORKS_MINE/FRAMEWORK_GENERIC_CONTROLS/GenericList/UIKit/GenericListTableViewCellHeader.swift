//
//  GenericListTableViewCellHeader.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 21/03/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
import UIKit

class GenericListTableViewCellHeader : UITableViewCell{
    
    @IBOutlet weak var labelTitle: UILabel!
    
    var customFontApplied = false
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //print("PlacesTableViewCell init coder")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        
        super.init(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: reuseIdentifier)
        
    }
    
    func setup(){
        
    }
    
    override func draw(_ rect: CGRect) {
        
        //print("drawRect")
        //didnt work here only for cells off screen
        //applyCustomFont()
    }
    
    func applyCustomFont(){
        if customFontApplied{
            //cell created already - BODY is replace with customfont - if we reuse it and call this again it will try and convert font again
        }else{
            self.labelTitle?.applyCustomFontForCurrentTextStyle()
            
            customFontApplied = true
        }
    }
}
