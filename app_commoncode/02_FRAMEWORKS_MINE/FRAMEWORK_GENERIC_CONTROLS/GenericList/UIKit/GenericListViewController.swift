//
//  COLCParentListViewController.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 23/01/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation



//--------------------------------------------------------------
// MARK: - COLCParentListViewController
// MARK: -
//--------------------------------------------------------------
// A tableVC but table is not self.view

class GenericListViewController: ParentViewController,
                                 UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate,
                                 COLCQueryCollectionDelegate,
                                 CurrentLocationManagerDelegate
{
    
    @IBOutlet weak var colcDGActivityIndicatorView: COLCDGActivityIndicatorView!
    @IBOutlet weak var viewWrapper_colcDGActivityIndicatorView: UIView!
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableView_BottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var buttonQuerySort: UIButton!

    @IBOutlet weak var viewToolbar: UIView!
    
    @IBOutlet weak var labelPlaceCount: UILabel!
    
    override func applicationDidBecomeActive_refreshTableView(){
        logger.debug("applicationDidBecomeActive_refreshTableView - self.callWebService(forceNewWSCall: true)")
        
        self.callWebService(forceNewWSCall: true)
    }
    
    
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - CurrentLocationManagerDelegate
    //--------------------------------------------------------------
    func currentLocationUpdated(currentLocationManager: CurrentLocationManager, currentLocation: CLLocation, distanceMetersDouble : CLLocationDistance){
        //print("CurrentLocationManagerDelegate  currentLocationUpdated NEW LOCATION")
        var okToQuery = false
        if distanceMetersDouble == -1{
            //first location
            okToQuery = true
            
        }else if distanceMetersDouble > 10.0{
            okToQuery = true
            
        }else{
            //ok happens in simulator when you change location
            //print("CurrentLocationManagerDelegate  currentLocationUpdated NEW LOCATION distanceMetersDouble < 10.0 - SKIP")
        }
        
        if(okToQuery){
            if let currentCOLCQuery = self.genericListViewDataSource.currentCOLCQuery(){
                if currentCOLCQuery.isLocationSort{
                    //print("CurrentLocationManagerDelegate  currentLocationUpdated NEW LOCATION - currentCOLCQuery.isLocationSort REQUERY")

                    self.requeryinBackgroundWithCurrentCOLCQueryAndCurrentSearchString()
                }else{
                    logger.debug("self.genericListViewDataSource.currentCOLCQuery() NOT currentCOLCQuery.isLocationSort - skip")
                }
                
            }else{
                logger.error("self.genericListViewDataSource.currentCOLCQuery()")
            }
        }else{
            //noisy logger.debug("okToQuery is false - not moved enough - dont requery")
        }
        
    }
    
    func currentLocationFailed(currentLocationManager: CurrentLocationManager, error: Error?)
    {
        print("CurrentLocationManagerDelegate  currentLocationFailed TODO")
    }

    func currentHeadingUpdated(currentLocationManager: CurrentLocationManager, newHeading: CLHeading){
        print("currentHeadingUpdated: newHeading:\r\(newHeading)")
    }
    
    //--------------------------------------------------------------
    // MARK: - SEARCH
    // MARK: -
    //--------------------------------------------------------------
    //let searchController = UISearchController(searchResultsController: nil)
    @IBOutlet weak var searchBar: UISearchBar!

    @IBOutlet weak var buttonSearchCancel: UIButton!
    
    @IBAction func buttonSearchCancel_Action(_ sender: Any) {
        self.searchBar?.text = ""
        self.searchBar?.resignFirstResponder()
    }
    
    
    
    //----------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------
    //OK
    //but hard to debug CMD+CLICK opened protocl class not parent class implementing the protocol
    //var genericListViewDataSource: GenericListViewDataSourceProtocol = EmptyListDataSource(){
    //----------------------------------------------------------------------------------------
    //genericListViewDataSource
    //----------------------------------------------------------------------------------------
    var genericListViewDataSource: ParentGenericListViewDataSource = EmptyListDataSource(){
        didSet {
            //dont requery here -
            //done in ParentWSControllerDelegate.response_success( in TFl subclass
            //override func response_success(parentWSController: ParentWSController, parsedObject: Any)
            //AFTER genericListViewDataSource is set
        }
    }
    
    //---------------------------------------------------------
    //didSelectRow
    //---------------------------------------------------------
    //same type as Dictionary<String, Array<Any>>
    var selectedRowItemAny : Any?
    

    func button_setTitle(button: UIButton, title: String?){
        button.setTitle(title, for: .normal)
        button.setTitle(title, for: .selected)
        button.setTitle(title, for: .highlighted)
    }
    func button_setTitleColor(button: UIButton, color: UIColor){
        button.setTitleColor(color, for: .normal)
        button.setTitleColor(color, for: .selected)
        button.setTitleColor(color, for: .highlighted)
    }

    
    //--------------------------------------------------------------
    // MARK: - METHODS
    // MARK: -
    //--------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = ""
        
        
        subscribeToKeyboardNotifications()
        
        
        self.tableView.sectionIndexColor = AppearanceManager.appColorLight0
        self.tableView.sectionIndexBackgroundColor = AppearanceManager.appColorNavbarAndTabBar
        
        
        //----------------------------------------------------------------------------------------
        //SEARCH / CANCEL BUTTON
        //----------------------------------------------------------------------------------------
        buttonSearchCancelDisable()
        //----------------------------------------------------------------------------------------

        //----------------------------------------------------------------------------------------
        //SHOW BUTTON
        //----------------------------------------------------------------------------------------
        self.buttonQuerySort.layer.cornerRadius = 4.0
        
        self.buttonQuerySort.backgroundColor = AppearanceManager.appColorLight1
        
        self.button_setTitleColor(button: self.buttonQuerySort, color: AppearanceManager.appColorDark0)
        self.button_setTitle(button: self.buttonQuerySort, title: "Show")
        //----------------------------------------------------------------------------------------

        
        self.buttonQuerySort.applyCustomFontForCurrentTextStyle()
        
        //----------------------------------------------------------------------------------------
        //SEARCH BAR
        //----------------------------------------------------------------------------------------
        //NAV BAR - sub class to turn off
        configureNavBar()
        
        //SEARCH BAR - sub class to turn off
        //configureSearchbar()
        
        //self.searchBar.tintColor = AppearanceManager.appColorTintYellow
        
        //dont make these the same the cancel button disappears
        //self.searchBar.barTintColor = AppearanceManager.appColorNavbarAndTabBar
        //self.searchBar.backgroundColor = AppearanceManager.appColorNavbarAndTabBar
        
        self.searchBar.layer.borderColor = AppearanceManager.appColorTintYellow.cgColor
        //self.searchBar.returnKeyType = .done
        
        //------------------------------------------------------------
        //DYNAMIC TEXT and TABLE ROW HEIGHT
        //------------------------------------------------------------
        config_tableView_dynamicHeight(estimatedRowHeight:80.0)
        
        //------------------------------------------------------------
        //TableView : RefreshControl
        //------------------------------------------------------------
        config_tableView_RefreshControl()
        //------------------------------------------------------------
        

        //------------------------------------------------------------
        //CALL WS -CALL IN SUB CLASS - Else its called twice
        //callWebService()
        //------------------------------------------------------------
    }
    
    func config_tableView_RefreshControl() {
        self.tableView.addSubview(self.refreshControl)
    }
    
    //override
    func config_tableView_dynamicHeight(estimatedRowHeight: Double){
        
        //self.tableView.estimatedRowHeight = 80.0
        self.tableView.estimatedRowHeight = CGFloat.init(estimatedRowHeight)
        
        //also done in heightForRowAtIndexPath
        self.tableView.rowHeight = UITableView.automaticDimension;
        
        //self.tableView.sectionHeaderHeight = UITableView.automaticDimension;
        //self.tableView.estimatedSectionHeaderHeight = 80.0f;
    }

        
    //--------------------------------------------------------------
    // MARK: - COLCQueryCollectionDelegate
    // MARK: -
    //--------------------------------------------------------------
    //user picked new query from alert - search should be "" - pass this into filter
    func colcQuery_HasChanged(colcQuery: COLCQuery, inCOLCQueryCollection: COLCQueryCollection){
        
        //self.requeryinBackgroundWithCurrentCOLCQuery()
        self.requeryinBackgroundWithCurrentCOLCQueryAndCurrentSearchString()
    }
    
    func requeryinBackgroundWithCurrentCOLCQueryAndCurrentSearchString(){
        if let searchBar = self.searchBar {
            if let searchBarText = searchBar.text {
                self.requeryinBackgroundWithCurrentCOLCQueryAnd(searchString: searchBarText)
            }else{
                logger.error("searchBar.text is nil")
            }
        }else{
            logger.error("self.searchBar is nil")
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - requeryInBackground
    // MARK: -
    //--------------------------------------------------------------

    func requeryInBackground(colcQuery: COLCQuery){
     
        //-------------------------------------------------------------------
        //REQUERY the results - will be run in background else activity indicator wont work
        //-------------------------------------------------------------------
        self.showLoadingOnMain()
        
        DispatchQueue.global(qos: .background).async {
            
            self.genericListViewDataSource.requery(colcQuery:colcQuery)
            
            DispatchQueue.main.async{
                self.hideLoadingOnMain()
                self.reloadTableOnMain()
            }
        }
    }
    
    
    //--------------------------------------------------------------
    // MARK: - reloadTableOnMain
    // MARK: -
    //--------------------------------------------------------------
    func reloadTableOnMain(){
        if Thread.isMainThread{
            hideLoading()
        }else{
            DispatchQueue.main.async{
                self.tableView.reloadData()
            }
        }
    }

    
    //--------------------------------------------------------------
    // MARK: - Navigation Bar
    // MARK: -
    //--------------------------------------------------------------
    //SUBLCASS
    func configureNavBar() {
        ////----------------------------------------------------------------------------------------
        ////Menu
        ////----------------------------------------------------------------------------------------
        //navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Menu",
        //                                                   style: .plain,
        //                                                   target: self,
        //                                                   action: #selector(TFLTaxisRankListViewController.leftBarButtonItem_Action))
        ////----------------------------------------------------------------------------------------
        ////Map
        ////----------------------------------------------------------------------------------------
        //navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Map",
        //                                                    style: .plain,
        //                                                    target: self,
        //                                                    action: #selector(TFLTaxisRankListViewController.rightBarButtonItem_Action))
        ////----------------------------------------------------------------------------------------
    }
    
    func leftBarButtonItem_Action() {
        logger.error("SUBCLASS : leftBarButtonItem_Action")
        //self.performSegue(withIdentifier: segueTFLTaxisRankListViewControllerTOSettingsTableViewController, sender: nil)
        
    }
    
    func rightBarButtonItem_Action() {
        logger.error("SUBCLASS : rightBarButtonItem_Action")
        //self.performSegue(withIdentifier: segueTFLTaxisRankListViewControllerTOTFLTaxisRankMapViewController, sender: nil)
        
    }
    
    
    @IBAction func buttonQuerySort_Action(_ sender: Any) {
        buttonQuerySort_Action()
        
    }
    
    func buttonQuerySort_Action() {
        logger.error("SUBCLASS : buttonQuerySort_Action")
        //self.performSegue(withIdentifier: segueTFLTaxisRankListViewControllerTOGenericListOptionsViewController, sender: nil)
        
    }
    
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - KEYBOARD
    //--------------------------------------------------------------
    fileprivate func subscribeToKeyboardNotifications() {
        
//        NotificationCenter.default.addObserver(self, selector: #selector(COLCPlacePickerViewController.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(COLCPlacePickerViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        //------------------------------------------------------------------------------------------------
        //must prefix func  with @objc
        //using keyboardWillShow
        //not COLCPlacePickerViewController.keyboardWillShow(
        //------------------------------------------------------------------------------------------------
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)),
                                               name: UIResponder.keyboardWillShowNotification, object: nil)
        //-------------------------------------------------------------------
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)),
                                               name: UIResponder.keyboardWillHideNotification, object: nil)
        
        
    }
    
    fileprivate func unsubscribeFromKeyboardNotifications() {
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        
        if let notification_userInfo = (notification as NSNotification).userInfo {
            
            //---------------------------------------------------------------------
            //KEYBOARD HEIGHT
            //---------------------------------------------------------------------
            //OK let heightKeyboard = (notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            //OK let heightKeyboard1 = (notification_userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            
            if let value = notification_userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
                let heightKeyboard = value.cgRectValue.height
                
                //---------------------------------------------------------------------
                //DURATION
                //---------------------------------------------------------------------
                
                if let durationNSValue = notification_userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber {
                    let durationDouble = durationNSValue.doubleValue
                    
                    //logger.debug("keyboardWillShow: durationDouble:\(durationDouble)")
                    //self.viewAddNewSearchResults.alpha = 0.0
                    //self.viewAddNewSearchResults.isHidden = false
                    
                    UIView.animate(withDuration: durationDouble, delay:0.0, options: UIView.AnimationOptions(),
                                   animations: {
                                        //self.view.frame = CGRectMake(0, 0, Geo.width(), Geo.height() - height)
                                        //self.tableView_BottomConstraint.constant = heightKeyboard - (50 + 44);
                                        //self.tableView_BottomConstraint.constant = heightKeyboard - 50;
                                        self.tableView_BottomConstraint.constant = heightKeyboard - 83 //tabbar heignt

                        //self.viewAddNewSearchResults.alpha = 1.0
                                    },
                                   completion: {finished in
                                        //self.textFieldAddSearch.text = ""
                                    }
                    )
                    
                }else{
                    logger.error("notification_userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber is nil")
                }
            }else{
                logger.error("notification_userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue is nil")
            }
            
        }else{
            logger.error("(notification as NSNotification).userInfo is nil")
        }
    }
    
    
    @objc func keyboardWillHide(_ notification: Notification) {
        //---------------------------------------------------------------------
        if let notification_userInfo = (notification as NSNotification).userInfo {
            
            //---------------------------------------------------------------------
            //KEYBOARD HEIGHT
            //---------------------------------------------------------------------
            //OK let heightKeyboard = (notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            //OK let heightKeyboard1 = (notification_userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            
            if let value = notification_userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
                //height not require we just set constraint back to 0
                let _ = value.cgRectValue.height
                
                //---------------------------------------------------------------------
                //DURATION
                //---------------------------------------------------------------------
                
                if let durationNSValue = notification_userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber {
                    let durationDouble = durationNSValue.doubleValue
                    logger.debug("keyboardWillHide: duration:\(durationDouble)")
                    
                    UIView.animate(withDuration: durationDouble, delay:0.0, options: UIView.AnimationOptions(),
                                   animations: {
                                    self.tableView_BottomConstraint.constant = 0;
                                    //self.viewAddNewSearchResults.alpha = 0.0
                    },
                                   completion: {finished in
                                    //self.textFieldAddSearch.text = ""
                    }
                    )
                    
                }else{
                    logger.error("notification_userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber is nil")
                }
            }else{
                logger.error("notification_userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue is nil")
            }
            
        }else{
            logger.error("(notification as NSNotification).userInfo is nil")
        }
    }
    
    
    //--------------------------------------------------------------
    // MARK: - SEARCH
    //--------------------------------------------------------------
//    func showHideSearchBarInTableHeader(showSearchBar: Bool, animated: Bool){
//        let duration: TimeInterval = 0.3
//        
//        
//        logger.debug("showSearchBar HARDCODED true")
//       
//        //let showSearchBar_ = showSearchBar
//        let showSearchBar_ = true
//        if showSearchBar_{
//            self.searchBar.delegate = self
//            //self.searchBar.isHidden = false
//            if animated{
//                UIView.animate(withDuration: duration, delay:0.0, options: UIViewAnimationOptions(),
//                               animations: {
//                                self.nsLayoutConstraint_SearchBar_Height.constant = 44.0;
//                                self.view.layoutIfNeeded()
//                                
//                },
//                               completion: {finished in
//                                //done
//                }
//                )
//            }else{
//                self.nsLayoutConstraint_SearchBar_Height.constant = 44.0;
//            }
//            
//            
//        }else{
//            //HIDE
//            //self.searchBar.isHidden = true
//            
//            if animated{
//                UIView.animate(withDuration: duration, delay:0.0, options: UIViewAnimationOptions(),
//                               animations: {
//                                self.nsLayoutConstraint_SearchBar_Height.constant = 0.0;
//                                self.view.layoutIfNeeded()
//                },
//                               completion: {finished in
//                                //done
//                }
//                )
//            }else{
//                self.nsLayoutConstraint_SearchBar_Height.constant = 0.0;
//            }
//        }
//       
//    }
    
    
    
    
    //--------------------------------------------------------------
    // MARK: - SEARCH - UISearchBarDelegate
    // MARK: -
    //--------------------------------------------------------------

    public func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool{
        // return NO to not become first responder
        //logger.debug("searchBarShouldBeginEditing -> true")
        
        return true
    }
    
    public func searchBarTextDidBeginEditing(_ searchBar: UISearchBar){
        // called when text starts editing
        buttonSearchCancelEnable()
        
    }
    

    
    public func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool{
        // return NO to not resign first responder
        //logger.debug("searchBarShouldEndEditing -> true")
        return true
    }
    
    func buttonSearchCancelEnable(){
        //----------------------------------------------------------------------------------------
        self.button_setTitle(button: self.buttonSearchCancel, title: "Cancel")
        self.button_setTitleColor(button: self.buttonSearchCancel, color: AppearanceManager.appColorLight1)
        self.buttonSearchCancel.isEnabled = true
        //----------------------------------------------------------------------------------------
    }
    func buttonSearchCancelDisable(){
        //----------------------------------------------------------------------------------------
        self.button_setTitle(button: self.buttonSearchCancel, title: "Search")
        self.button_setTitleColor(button: self.buttonSearchCancel, color: .lightGray)
        self.buttonSearchCancel.isEnabled = false
        //----------------------------------------------------------------------------------------
    }
    
    
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        // called when text changes (including clear)
        logger.debug("searchBarTextDidEndEditing searchText:'\(searchText)' -- FIRE TIMER?")
        //no use updateSearchResults
        doTimedSearchFromSearchBar()
    }
    
    public func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool{
        // called before text changes
        //logger.debug("shouldChangeTextIn -> true")
        return true
    }
    
    
    //--------------------------------------------------------------
    // MARK: - SEARCH - text changed do search
    // MARK: -
    //--------------------------------------------------------------
    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        // called when keyboard search button pressed
        logger.debug("searchBarSearchButtonClicked")
        doTimedSearchFromSearchBar()
    }
    
    
    //search button on keyboard - searchBarSearchButtonClicked >> searchBarTextDidEndEditing
    public func searchBarTextDidEndEditing(_ searchBar: UISearchBar){
        // called when text ends editing
        logger.debug("searchBarTextDidEndEditing searchBar.text:\(String(describing: searchBar.text))")
        doTimedSearchFromSearchBar()
        
        //----------------------------------------------------------------------------------------
        buttonSearchCancelDisable()
        //----------------------------------------------------------------------------------------

        
    }

    
    
    //--------------------------------------------------------------
    // MARK: - SEARCH - buttons
    // MARK: -
    //--------------------------------------------------------------

    public func searchBarBookmarkButtonClicked(_ searchBar: UISearchBar){
        // called when bookmark button pressed
        logger.debug("searchBarBookmarkButtonClicked")
    }
    
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar){
        // called when cancel button pressed
        //logger.debug("searchBarCancelButtonClicked")
        searchBar.text = ""
        searchBar.resignFirstResponder()
        
    }
    
    public func searchBarResultsListButtonClicked(_ searchBar: UISearchBar){
        // called when search results button pressed
        logger.debug("searchBarResultsListButtonClicked")
        
    }   
    
    public func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int){
        logger.debug("selectedScopeButtonIndexDidChange: selectedScope:\(selectedScope)")
        
    }
    
    
        
    
    //--------------------------------------------------------------
    // MARK: - SEARCH - timer
    // MARK: -
    //--------------------------------------------------------------
    var timerTypeAndSearch: Timer?
    
    
    func doTimedSearchFromSearchBar(){
        if let text = self.searchBar.text {
            doTimedSearch(forSearchString: text)
        }else{
            logger.error("self.searchController.searchBar.text is nil")
        }
    }
    
    func doTimedSearch(forSearchString searchString : String){
        
        //do all searches using timer else they can get out of sync
        
        if let timerTypeAndSearch = self.timerTypeAndSearch{
            //every time you type a letter - reset the time - by deleteing the old one and start new one
            timerTypeAndSearch.invalidate()
        }else{
            //first time - only need to create timer
        }
        
        createTimer(searchString)
    }
    
    let kuserInfo_SearchText = "searchText"
    
    func createTimer(_ searchText: String){
        
            //    if searchText == ""{
            //        self.searchBar.resignFirstResponder()
            //        
            //    }else{
            //        
            //    }
        
            //----------------------------------------------------------------------------------------
            self.timerTypeAndSearch = Timer.scheduledTimer(timeInterval: 0.5,
                                                           target: self,
                                                           selector: #selector(GenericListViewController.genericListVC_timerTypeAndSearch_fired(_:)),
                                                           userInfo: [kuserInfo_SearchText : searchText],
                                                           repeats: false)
            //----------------------------------------------------------------------------------------

 
    }
    
    //make selector unique to this class - #selector class may not be current class
    //user has stoped typing in search box for long enough time to say they wish to search
    @objc func genericListVC_timerTypeAndSearch_fired(_ timer:Timer) {
        
        if let userInfo = timer.userInfo as? Dictionary<String, AnyObject>{
            if let textFieldTextString:String = userInfo[kuserInfo_SearchText] as? String{
                
                requeryinBackgroundWithCurrentCOLCQueryAnd(searchString:textFieldTextString)
                
            }else{
                logger.error("userInfo[kuserInfo_SearchText] is nil")
            }
        }else{
            logger.error("userInfo is nil")
        }
    }
    
    
    func requeryinBackgroundWithCurrentCOLCQueryAnd(searchString:String){
 
        logger.debug("requeryinBackgroundWithCurrentCOLCQueryAnd: searchString:'\(searchString)'")
    
        if let currentCOLCQuery = self.genericListViewDataSource.currentCOLCQuery(){
            
            //If fiter not working check the .and / .or in reQuery - they were out of sync
            if let colcFilterString = currentCOLCQuery.colcFilterString {
                colcFilterString.searchString = searchString
            }else{
                logger.error("currentCOLCQuery.colcFilterString is nil")
            }
            
            //-------------------------------------------------------
            //REQUERY
            //-------------------------------------------------------
            self.requeryInBackground(colcQuery: currentCOLCQuery)
            //-------------------------------------------------------
        }else{
            logger.error("self.genericListViewDataSource.currentCOLCQuery()")
        }
    }
    
    
    //--------------------------------------------------------------
    // MARK: - lifecycle
    // MARK: -
    //--------------------------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //--------------------------------------------------------------
    // MARK: - PULL TO REFRESH
    // MARK: -
    //--------------------------------------------------------------
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(GenericListViewController.handleRefresh(_:)), for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.appColorGREEN()
        return refreshControl
    }()
    var currentQueryIndexBeforePullToRefresh = -1
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        currentQueryIndexBeforePullToRefresh = self.genericListViewDataSource.colcQueryCollectionForType.currentQueryIndex
    
        
        //app will try to use cached response but if you pull to refresh it will ALWAYS call ws
        self.callWebService(forceNewWSCall: true)
        
        //self.tableView.reloadData()
        refreshControl.endRefreshing()
    }
    
    
    func callWebService(forceNewWSCall: Bool)
    {
        logger.error("SHOULD SUBCLASS - callWebService - dont call super")
        
        //will be overwritten - default for generic List is just refresh table
        //----------------------------------------------------------------------------------------
        self.showLoadingOnMain()
        //---------------------------------------------------------------------
        //        let tflPlaceTaxiRankRequest = TFLPlaceTaxiRankRequest()
        //        self.tflTaxisWSController.get_TFLTaxis(tflPlaceTaxiRankRequest,
        //                                               success:{
        //                                                (tflPlaceTaxiRankResponse : TFLPlaceTaxiRankResponse)->Void in
        //
        //
        //                                                //---------------------------------------------------------------------
        //                                                logger.debug("tflTaxiRankArray returned:\(tflPlaceTaxiRankResponse.tflTaxiRankArrayUnsorted.count)")
        //                                                //-------------------------------------------------------------------
        //                                                self.tflPlaceTaxiRankResponse = tflPlaceTaxiRankResponse
        //
        //                                                //self.tflPlaceTaxiRankResponse.dumpTaxiRankOneLine()
        //                                                //print("\r\r\r\r")
        //                                                //self.tflPlaceTaxiRankResponse.dumpTaxiRankOneLine_VersionsOnly()
        //                                                //-------------------------------------------------------------------
        self.hideLoadingOnMain()
        self.reloadTableOnMain()
        //                                                //-------------------------------------------------------------------
        //        },
        //                                               failure:{
        //                                                (error) -> Void in
        //                                                print(error)
        //        }
        //        )
        //---------------------------------------------------------------------
    }
    
    //--------------------------------------------------------------
    // MARK: - SHOW/HIDE
    // MARK: -
    //--------------------------------------------------------------

    func showLoadingOnMain(){
        if Thread.isMainThread{
            showLoading()
        }else{
            DispatchQueue.main.async{
                self.showLoading()
            }
        }
    }
    
    private func showLoading(){

        self.labelPlaceCount?.text = ""
        
        if let _ = self.colcDGActivityIndicatorView {
            self.colcDGActivityIndicatorView?.showLoadingOnMain()
            self.viewWrapper_colcDGActivityIndicatorView.isHidden = false
            
            //UIApplication.shared.isNetworkActivityIndicatorVisible = true
            self.buttonQuerySort.isEnabled = false
        }else{
            logger.error("self.colcDGActivityIndicatorView is nil")
        }
    }
    
    func hideLoadingOnMain(){
        if Thread.isMainThread{
            hideLoading()
        }else{
            DispatchQueue.main.async{
                self.hideLoading()
            }
        }
    }
    
    private func hideLoading(){
        if let _ = self.colcDGActivityIndicatorView {
            self.colcDGActivityIndicatorView?.hideLoadingOnMain()
            self.viewWrapper_colcDGActivityIndicatorView.isHidden = true
            //UIApplication.shared.isNetworkActivityIndicatorVisible = false
            self.buttonQuerySort.isEnabled = true
        }else{
            logger.error("self.colcDGActivityIndicatorView is nil")
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - TABLE
    // MARK: -
    //--------------------------------------------------------------
    internal let sectionHeaderHeight: CGFloat = 35.0
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        //return UITableView.automaticDimension
        return sectionHeaderHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        var cell = UITableViewCell()
        
        let cellidentifier = "GenericListTableViewCellHeader"
        
        if let genericListTableViewCellHeader : GenericListTableViewCellHeader = tableView.dequeueReusableCell(withIdentifier: cellidentifier) as? GenericListTableViewCellHeader {
            
            //------------------------------------------------------------------
            genericListTableViewCellHeader.applyCustomFont()
            
            genericListTableViewCellHeader.backgroundColor = AppearanceManager.appColorLight0
            genericListTableViewCellHeader.contentView.backgroundColor = AppearanceManager.appColorLight0
            genericListTableViewCellHeader.labelTitle?.textColor = AppearanceManager.appColorDark0
            
            //------------------------------------------------------------------
            //let openWithAppGroup = arrayTableDataSections_OpenWithAppGroup[section]
            if let titleForHeaderInSection = self.tableView(tableView, titleForHeaderInSection: section) {
                genericListTableViewCellHeader.labelTitle.text = titleForHeaderInSection
            }else{
                genericListTableViewCellHeader.labelTitle.text = ""
            }
            
            //------------------------------------------------------------------
            cell = genericListTableViewCellHeader
            //------------------------------------------------------------------
            
        }else{
            logger.error("dequeueReusableCell is nil - cellIdentifier not found : '\(cellidentifier)'")
        }
    
        return cell
        
        
        
        return cell
    }
    //------------------------------------------------
    // MARK: UITableViewDataSource - SECTIONS
    //------------------------------------------------
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?{
        //---------------------------------------------------------
        //nil for ungrouped - but all views grouped now
        //---------------------------------------------------------

        //---------------------------------------------------------
        var titleForHeaderInSectionReturned : String? = nil
        
        if let titleForHeaderInSection = self.genericListViewDataSource.titleForHeaderInSection(section){
            
            if let searchBar = self.searchBar {
                if let searchBarText = searchBar.text {
                    if searchBarText == ""{
                        titleForHeaderInSectionReturned = "\(titleForHeaderInSection) (Total: \(self.genericListViewDataSource.totalsCount))"
                    }else{
                        titleForHeaderInSectionReturned = "\(titleForHeaderInSection) (Filtered: \(self.genericListViewDataSource.totalsCount))"
                    }
                }else{
                    logger.error("searchBar.text is nil")
                }
            }else{
                logger.error("self.searchBar is nil")
            }
        }else{
             logger.error("self.genericListViewDataSource.titleForHeaderInSection(section) is nil")
        }
        
        return titleForHeaderInSectionReturned
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        let numberOfSections = self.genericListViewDataSource.numberOfSections()
        return numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let numberOfRowsInSection = self.genericListViewDataSource.numberOfRowsInSection(section)

        return numberOfRowsInSection
    }
    
    //--------------------------------------------------------------
    // MARK: - TABLEVIEW: CELL FOR ROW
    //--------------------------------------------------------------
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let tflTaxisRankListTableViewCell : UITableViewCell = UITableViewCell()
        
        //Array<Any> >> Array<String>
        if let rowDataArray_ : Array<String> = self.genericListViewDataSource.rowDataArrayForSection(indexPath.section) as? Array<String>{
            let rowTitle = rowDataArray_[indexPath.row]
            tflTaxisRankListTableViewCell.textLabel?.text = rowTitle
        }else{

        }
        return tflTaxisRankListTableViewCell
    }
    
    //--------------------------------------------------------------
    // MARK: - TABLEVIEW: didSelectRowAt
    //--------------------------------------------------------------
    //SUNCLASS - handle your own type
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)

        //DONE IN SUB CLASS

        
//        //----------------------------------------------------------------------------------------
//        //Array<Any> >> Array<String>
//        //----------------------------------------------------------------------------------------
//        if var rowDataArray_ : Array<String> = self.genericListViewDataSource.rowDataArrayForSection(indexPath.section) as? Array<String>{
//            self.selectedRowItemAny = rowDataArray_[indexPath.row]
//            
//            //self.performSegue(withIdentifier: self.segueTFLTaxisRankListViewControllerTOTFLTaxisRankSingleTableViewController, sender: nil)
//            
//        }else{
//            logger.debug("TODO didSelectRowAt SUBCLASS rowDataArrayForSection call segue")
//        }
//        
//        logger.debug("TODO SUBCLASS didSelectRowAt:\(self.selectedRowItemAny)")
        
    }
    
    //------------------------------------------------
    // MARK: TABLE INDEX
    //------------------------------------------------
    //var arrayFirstLetters = [String]()
    
    //The first letter displayed down the side in the index - usually A..Z
    public func sectionIndexTitles(for tableView: UITableView) -> [String]? {

        //non nil if self.genericListViewDataSource.genericListSort is .sortByName
        return self.genericListViewDataSource.sectionIndexTitles()
    }
    
    
    public func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        var indexFound_ = 0
        //print("sectionForSectionIndexTitle:index:\(index)")
        
        if (index == 0) {
            // If the user taps the search icon, scroll the table view to the top
            indexFound_ = -1
            //note when you requery it scrolls to top - couldnt get
            self.tableView.setContentOffset(CGPoint.init(x: 0.0, y: 0.0), animated: true)
 
        } else {
            indexFound_ = self.genericListViewDataSource.sectionForSectionIndexTitle(title: title, at: index)
        }
        
        
        return indexFound_
        
    }
    
    
    
    //--------------------------------------------------------------
    // MARK: - SEGUE
    // MARK: -
    //--------------------------------------------------------------
    let segueGenericListViewController_TO_TFLTaxisRankMapViewController = "segueTFLTaxisRankListViewControllerTOTFLTaxisRankMapViewController"
    
    //--------------------------------------------------------------
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        //if(segue.identifier == segueTFLTaxisRankListViewControllerTOTFLTaxisRankMapViewController){
        //    
        //    if(segue.destination.isMember(of: TFLTaxisRankMapViewController.self)){
        //        
        //        let tflTaxisRankMapViewController = (segue.destination as! TFLTaxisRankMapViewController)
        //        //tflTaxisRankMapViewController.delegate = self
        //        tflTaxisRankMapViewController.tflPlaceTaxiRankResponse = self.tflPlaceTaxiRankResponse
        //        
        //    }else{
        //        print("ERROR:not TFLTaxisRankMapViewController")
        //    }
        //}
        //else{
        //    logger.error("UNHANDLED SEGUE:\(segue.identifier)")
        //}
        logger.error("UNHANDLED SEGUE:\(String(describing: segue.identifier))")
    }
    
}
