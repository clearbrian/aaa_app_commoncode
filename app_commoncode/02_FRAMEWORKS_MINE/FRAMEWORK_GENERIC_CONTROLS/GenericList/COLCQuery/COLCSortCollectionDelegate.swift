//
//  COLCSortCollectionDelegate.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 15/03/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

//--------------------------------------------------------------
// MARK: - COLCSortCollectionDelegate
// MARK: -
//--------------------------------------------------------------
protocol COLCSortCollectionDelegate {
    func colcSortCollectionHasChanged(colcSortCollection: COLCSortCollection)
    func colcSortHasChanged(colcSort: COLCSort)
}
