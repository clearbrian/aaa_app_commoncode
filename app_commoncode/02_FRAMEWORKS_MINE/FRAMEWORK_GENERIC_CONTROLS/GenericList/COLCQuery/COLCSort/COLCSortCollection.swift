//
//  COLCSortCollection.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 15/03/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation



class COLCSortCollection{
    
    var delegate: COLCSortCollectionDelegate?
    
    //--------------------------------------------------------------
    // MARK: - COLCSortCollection
    // MARK: -
    //--------------------------------------------------------------
    var colcSortDictionary = [String: COLCSort]()
    
    
    //self.colcSortDictionary.keys insertion order not guaranteed
    var colcSortKeys = [String]()
    
    
    //Subclassed
    //called in init
    func build_colcSortDictionary() -> [String: COLCSort]{
        
        let colcSortDictionary_ = [String: COLCSort]()
        
        
//        //----------------------------------------------------------------------------------------
//        //NEAREST
//        //----------------------------------------------------------------------------------------
//        //we want Nearest first
//        colcSortDictionary_[COLCSort.COLCSort_sortByNearest().title] = COLCSort.COLCSort_sortByNearest()
//        self.colcSortKeys.append(COLCSort.COLCSort_sortByNearest().title)
//        
//        //----------------------------------------------------------------------------------------
//        //NAME
//        //----------------------------------------------------------------------------------------
//        colcSortDictionary_[COLCSort.COLCSort_sortByName().title] = COLCSort.COLCSort_sortByName()
//        self.colcSortKeys.append(COLCSort.COLCSort_sortByName().title)
//        //----------------------------------------------------------------------------------------
        
        return colcSortDictionary_
        
    }
    
    
    //--------------------------------------------------------------
    // MARK: - INIT
    // MARK: -
    //--------------------------------------------------------------
    //DESIGNATED INITIALIZER
    init() {
        //----------------------------------------------------------------------------------------
        self.colcSortDictionary = self.build_colcSortDictionary()
        //----------------------------------------------------------------------------------------
    }
    
    var currentSortIndex = 0{
        didSet {
            logger.debug("currentSortIndex didSet \(currentSortIndex)")
        }
    }
    
    var currentCOLCSort: COLCSort?{
        var currentCOLCSort_: COLCSort? = nil
        
        //order not guaranteed
        //let keysArray = Array(self.colcSortDictionary.keys)
        
        //if key ISNT same as title this wont work
        let keysArray = self.colcSortKeys
        
        if keysArray.count > 0{
            //----------------------------------------------------------------------------------------
            if self.currentSortIndex >= 0{
                if self.currentSortIndex >= keysArray.count{
                    logger.error("currentSortIndex[\(self.currentSortIndex)] >= keysArray.count[\(keysArray.count)]")
                }else{
                    
                    let key = keysArray[self.currentSortIndex]
                    
                    if let colcSort : COLCSort = colcSortDictionary[key]{
                        currentCOLCSort_ = colcSort
                    }else{
                        logger.error("colcSortDictionary[\(key)] is nil")
                    }
                }
            }else{
                logger.error("currentSortIndex is < 0")
            }
            
            //----------------------------------------------------------------------------------------
        }else{
            logger.error("colcSortDictionary.keys.count is 0")
        }
        logger.debug("currentCOLCSort_ \(String(describing: currentCOLCSort_?.title))")
        return currentCOLCSort_
        
    }
    
    //--------------------------------------------------------------
    // MARK: - currentSortIndexChanged
    // MARK: -
    //--------------------------------------------------------------
    func currentSortIndexChanged(selectedIndex: Int) {
        
        
        //let keysArray = Array(self.colcSortDictionary.keys)
        //if key ISNT same as title this wont work
        
        let keysArray = self.colcSortKeys
        
        if selectedIndex >= keysArray.count{
            logger.error("selectedIndex[\(selectedIndex)] >= keysArray.count[\(keysArray.count)]")
            
        }else{
            
            self.currentSortIndex = selectedIndex
            
            let key = keysArray[selectedIndex]
            
            if let colcSort : COLCSort = colcSortDictionary[key]{
                
                // TODO: - rename to currentSortIndexHasChanged
                delegate_colcSortHasChanged(colcSort: colcSort)
            }else{
                logger.error("colcSortDictionary[\(key)] is nil")
            }
        }
    }
    
    
    //--------------------------------------------------------------
    // MARK: - COLCSortCollectionDelegate
    // MARK: -
    //--------------------------------------------------------------
    // TODO: - remove
    func delegate_queryHasChanged(){
        if let delegate = self.delegate{
            
            //------------------------------------------------
            delegate.colcSortCollectionHasChanged(colcSortCollection: self)
            //------------------------------------------------
            
        }else{
            logger.error("delegate is nil")
        }
    }
    // TODO: - change to currentSortIndexHasChanged
    func delegate_colcSortHasChanged(colcSort: COLCSort){
        if let delegate = self.delegate{
            
            //------------------------------------------------
            delegate.colcSortHasChanged(colcSort:colcSort)
            //------------------------------------------------
            
        }else{
            logger.error("delegate is nil")
        }
    }
    
    
}
