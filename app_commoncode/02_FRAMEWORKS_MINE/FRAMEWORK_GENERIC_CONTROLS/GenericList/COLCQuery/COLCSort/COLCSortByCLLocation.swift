//
//  COLCSortByCLLocation.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 17/03/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

//--------------------------------------------------------------
// MARK: - COLCSortByCLLocation
// MARK: -
//--------------------------------------------------------------

// TODO: - subclass COLCSortByNearest - near to device
//COLCSortByCLLocation should be to ANy location

// TODO: - if location changes in app then Nearest list should be refreashed
class COLCSortByCLLocation : COLCSort{
    
    convenience init(title: String)
    {
        self.init(title:title, propertyName: "", isSorted: true, isAscending: true, isCaseSensitive: false)
    }
    
    
    //protocol COLCQueryable : COLCSortable, CoreLocatable
    override func sortResults(arrayToSort: [COLCQueryable]) -> [COLCQueryable]{
        
        //return unsorted if error
        var arraySorted = arrayToSort
        
        if self.isSorted{
            //[CoreLocatable] >> [COLCSortable]
            arraySorted = COLCSortByCLLocation.sortByNearest(arrayToSort: arrayToSort)
            
        }else{
            logger.error("COLCSortByCLLocation isSorted is false - should only be false for COLCSortNone")
            //use unsorted - set by default above
            //arraySorted = arrayToSort
        }
        return arraySorted
    }
    
    class func sortByNearest(arrayToSort: [COLCQueryable]) -> [COLCQueryable]{
        
        //-------------------------------------------------------------
        //if error below just return unsorted
        var sortByNearest_ = arrayToSort
        //-------------------------------------------------------------
        
        //----------------------------------------------------------------------------------------
        //SORT BY CURRENT LOCATION
        //----------------------------------------------------------------------------------------
        // TODO: - what if theres no location cant use traf sq for non london app
        if let currentLocation = appDelegate.currentLocationManager.currentLocation{
            //----------------------------------------------------------------------------------------
            //SORTED BY NEAREST
            //-------------------------------------------------------------
            let sortedByDistArray = arrayToSort.sorted {
                
                $0.distanceTo(otherLocation:currentLocation) <
                    $1.distanceTo(otherLocation:currentLocation)
            }
            //-------------------------------------------------------------
            sortByNearest_ = sortedByDistArray
            //----------------------------------------------------------------------------------------
        }else{
            logger.error("appDelegate.currentLocationManager.currentLocation - cant sort by distance - returning unsorted")
        }
        return sortByNearest_
    }
}
