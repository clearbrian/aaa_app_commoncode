//
//  COLCGroupBy.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 16/03/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

class COLCGroupBy{
    
    static let NoneKey = "Ungrouped" //should not matter the section
    
    //----------------------------------------------------------------------------------------
    //shown in SORT picker
    var title: String = COLCGroupBy.NoneKey
    
    var showSectionHeaders = true
    
    //----------------------------------------------------------------------------------------
    //init
    //----------------------------------------------------------------------------------------
    init(title : String)
    {
        self.title = title

    }
 
    //----------------------------------------------------------------------------------------
    // MARK: - groupResults
    // MARK: -
    //----------------------------------------------------------------------------------------
    //returns - dictionary - each key is a Section in the results table
    //----------------------------------------------------------------------------------------
    func groupResults(arrayToGroup     : [COLCQueryable],
                      andSort colcSort : COLCSort) -> Dictionary<String, Array<COLCQueryable>>
    
    {
        //---------------------------------------------------------
        //SORT
        //---------------------------------------------------------
        let arrayForGroupSorted : [COLCQueryable] = colcSort.sortResults(arrayToSort: arrayToGroup)
        
        //---------------------------------------------------------
        //GROUP - only one by default
        //---------------------------------------------------------
        var dictionaryTableData_Grouped = Dictionary<String, Array<COLCQueryable>>()
        
        //---------------------------------------------------------
        //KEY - shown in section header : key > titleForHeaderInSection
        //---------------------------------------------------------
        if colcSort.title == ""{
            dictionaryTableData_Grouped[COLCGroupBy.NoneKey] = arrayForGroupSorted
        }else{
            dictionaryTableData_Grouped[colcSort.title] = arrayForGroupSorted
        }

        return dictionaryTableData_Grouped
    }
}


class COLCGroupByNone : COLCGroupBy{
    
    init() {
        //shown in sort Options
        super.init(title: COLCGroupBy.NoneKey)
        showSectionHeaders = false
    }
    
}


class COLCGroupByAZ : COLCGroupBy{
    
    //--------------------------------------------------------------
    // MARK: - sortResults
    // MARK: -
    //--------------------------------------------------------------
    ////protocol COLCQueryable : COLCGroupByable, CoreLocatable
    override func groupResults(arrayToGroup     : [COLCQueryable],
                               andSort colcSort : COLCSort) -> Dictionary<String, Array<COLCQueryable>>
        
    {
        //---------------------------------------------------------
        //the COLCSort knows what field to sort on 
        //---------------------------------------------------------
        var dictionaryTableData_Grouped = Dictionary<String, Array<COLCQueryable>>()
        
        if let arrayFirstLettersSorted : [String] = colcSort.arrayFirstLetters(sortedArray: arrayToGroup){
            for firstLetter in arrayFirstLettersSorted{
                let arraySortedAndFilteredByFirstLetter : [COLCQueryable] = colcSort.sortResults(arrayToSort: arrayToGroup, filterByFirstLetter:firstLetter)
                
                dictionaryTableData_Grouped[firstLetter] = arraySortedAndFilteredByFirstLetter
            }
        }else{
            logger.error("colcSort.arrayFirstLetters is nil")
        }
        
        return dictionaryTableData_Grouped
    }
}
