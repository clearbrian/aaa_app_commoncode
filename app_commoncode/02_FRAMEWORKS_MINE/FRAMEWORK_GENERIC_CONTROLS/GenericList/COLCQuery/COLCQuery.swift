//
//  COLCQuery.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 28/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

//------------------------------------------------------------


//------------------------------------------------------------
/*
 SELECT column-names
 FROM table-name
 
 WHERE condition
    
    //wildcard
    columnName LIKE "%subString%"
    //Exact .rest.rawValue
    columnName = "Rest rank"
 
 
 GROUP BY column-names
 ORDER BY column-names
 
 
 //----------------------------------------------------------------------------------------
 //SORT
 //----------------------------------------------------------------------------------------
 Enum
 unsorted //as downloaded
 asc
 desc
 
 //----------------------------------------------------------------------------------------
 //GROUPED
 //----------------------------------------------------------------------------------------
 Name - A..Z
    protocol as may be grouped on anything
    - index would be sorted in same direction as
    - String a..z z...a
 
 
 see
https://docs.google.com/spreadsheets/d/1XP_6CJSNz6xotaXm6uvMHUlC7K0u0r2xBPx1NSV36mg/edit#gid=0
 
 
 */

//--------------------------------------------------------------
// MARK: - COLCQuery
// MARK: -
//--------------------------------------------------------------
class COLCQuery{
    

    //show in segmentedControls
    var title: String = ""
    
    var showSearchBar = false
    
    //--------------------------------------------------------------
    // GROUP BY
    //--------------------------------------------------------------
    var colcGroupBy : COLCGroupBy = COLCGroupByNone()
    
    //requry if new location received
    var isLocationSort : Bool{
        if self.colcSort is COLCSortByCLLocation{
            return true
        }else{
            return false
        }
    }
    
    //--------------------------------------------------------------
    // FILTERS
    //--------------------------------------------------------------
    ///Users/gbxc/Documents/00_ME/00_BITBUCKET_COLC/app_apptemplatesandnotes/00_CODE/AppWithCodeTemplates_iOS_Swift3/PLAYGROUNDS/Sort_Object_MultipleProperties.playground
    //var arrayOfCOLCSort = [COLCSort]()
    var colcSort : COLCSort = COLCSortNone(title: "None")
    
    
    //--------------------------------------------------------------
    // MARK: - init
    // MARK: -
    //--------------------------------------------------------------
   
    //colcQuery_sortByCommonName
    init(title: String, colcSort: COLCSort, searchPropertyName: String){
        self.title = title
        self.colcSort = colcSort
        
        
        //---------------------------------------------------------
        //SEARCH
        //---------------------------------------------------------
        let colcFilterString_ = COLCFilterString(propertyName    : searchPropertyName,
                                                 searchString    : "",
                                                 isPartialSearch : true,
                                                 isCaseSensitive : false)
        self.colcFilterString = colcFilterString_
        self.colcFilterGroupAND.appendCOLCFilter(colcFilterString_)
        //---------------------------------------------------------
    }
    
    //--------------------------------------------------------------
    // FILTERS
    //--------------------------------------------------------------
    //fileprivate var arrayOfCOLCFilterOrGroupParent = [COLCFilterOrGroupParent]()
    let colcFilterGroupAND = COLCFilterGroup(colcFilterGroupType:.and)
    var colcFilterString: COLCFilterString?
    
    func appendCOLCFilter(_ colcFilter: COLCFilter){
        
        self.colcFilterGroupAND.appendCOLCFilter(colcFilter)
        
    }
    
//    func appendCOLCFilterSearch(_ colcFilterString: COLCFilterString){
//        
//        self.colcFilterString = colcFilterString
//        
//        self.colcFilterGroupAND.appendCOLCFilter(colcFilterString)
//        
//    }
    
    //--------------------------------------------------------------
    // TODO: - Factory >> COLCQuery(search string)

    var searchString: String = ""{
        didSet {
            print("COLCQuery searchString didSet: new value is '\(searchString)'")
            if let colcFilterString = self.colcFilterString {
                colcFilterString.searchString = searchString
            }else{
                logger.error("self.colcFilterString is nil")
            }
        }
    }
    
    func clearFilters(){
        // TODO: - clearFilters
        logger.error("clearFilters() does nothing")
       // self.arrayOfCOLCFilterOrGroupParent = [COLCFilterOrGroupParent]()
    }
    
    
   

    //--------------------------------------------------------------
    // MARK: - QUERY RESULT and COUNT (filtered array) - stored with query
    // MARK: -
    //--------------------------------------------------------------
    
    //used to show total on TableVC
    var resultsFilteredCount : Int{
        return arrayCOLCQueryable_Filtered.count
    }
    
    //RESULTS - FILTERED but GROUPED
    var arrayCOLCQueryable_Filtered = [COLCQueryable]()
    
    //RESULTS - GROUPED
    var dictionaryTableData_Result: Dictionary<String, Array<COLCQueryable>> = Dictionary<String, Array<COLCQueryable>>()
    //--------------------------------------------------------------
    // MARK: - APPLY QUERY
    // MARK: -
    //--------------------------------------------------------------
    func requery(arrayCOLCQueryable: [COLCQueryable]){

       
        
        //----------------------------------------------------------------------------------------
        //colcQueryCollection.searchString - search by name
        //----------------------------------------------------------------------------------------
        // TODO: - todo search name - but allfields?
        //            if self.colcQueryCollection.searchString == ""{
        //                //----------------------------------------------------------------------------------------
        //                //NO FILTER
        //                //----------------------------------------------------------------------------------------
        //                logger.debug("[requery()] self.colcQueryCollection.searchString == '' - SKIP FILTERING")
        //                logger.debug("self.tflApiPlaceArrayProcessed        :\(self.tflApiPlaceArrayProcessed.count)")
        //                self.tflApiPlaceArrayUnsorted = self.tflApiPlaceArrayProcessed
        //
        //            }else{
        //
        //            }
        
        //----------------------------------------------------------------------------------------
        //FILTER
        //----------------------------------------------------------------------------------------
        self.arrayCOLCQueryable_Filtered = arrayCOLCQueryable
        
//        if self.arrayOfCOLCFilterOrGroupParent.count > 0{
//            
//            arrayCOLCQueryable_Filtered = arrayCOLCQueryable.filter( { (colcQueryable: COLCQueryable) -> Bool in
//                
//                //TFLApiPlace
//                if self.filterMatches(colcQueryable: colcQueryable){
//                    return true
//                }else{
//                    return false
//                }
//            })
//            
//            //----------------------------------------------------------------------------------------
//            logger.debug("[requery] AFTER FILTER")
//            logger.debug("[requery] arrayCOLCQueryable          :\(arrayCOLCQueryable.count)")
//            logger.debug("[requery] arrayCOLCQueryable_Filtered :\(arrayCOLCQueryable_Filtered.count)")
//             //----------------------------------------------------------------------------------------
//            
//        }else{
//            //done above by default
//            //arrayCOLCQueryable_Filtered = arrayCOLCQueryable
//            
//            //----------------------------------------------------------------------------------------
//            logger.debug("[requery] NO FILTER - SKIP")
//            logger.debug("[requery] arrayCOLCQueryable          :\(arrayCOLCQueryable.count)")
//            logger.debug("[requery] arrayCOLCQueryable_Filtered :\(arrayCOLCQueryable_Filtered.count)")
//            //----------------------------------------------------------------------------------------
//        }
        
        
        arrayCOLCQueryable_Filtered = arrayCOLCQueryable.filter( { (colcQueryable: COLCQueryable) -> Bool in
            
            //TFLApiPlace
            if self.filterMatches(colcQueryable: colcQueryable){
                return true
            }else{
                return false
            }
        })
        
        //----------------------------------------------------------------------------------------
        print("[requery] AFTER FILTER")
        print("[requery] arrayCOLCQueryable          :\(arrayCOLCQueryable.count)")
        print("[requery] arrayCOLCQueryable_Filtered :\(arrayCOLCQueryable_Filtered.count)")
        //----------------------------------------------------------------------------------------
    
        //----------------------------------------------------------------------------------------
        //SORT - TODO - done is groupedby - but only handled 1 sort
        //---------------------------------------------------------
        //- cant apply to one object - need to pass strings from two object into a sort
        //and then call it in step
        //---------------------------------------------------------
        // /Users/gbxc/Documents/00_ME/00_BITBUCKET_COLC/app_apptemplatesandnotes/00_CODE/AppWithCodeTemplates_iOS_Swift3/PLAYGROUNDS/Sort_Object_MultipleProperties.playground
        //---------------------------------------------------------
        //        for colcSort : COLCSort in arrayOfCOLCSort{
        //            
        //            arraySorted_ = colcSort.sortResults(arrayToSort: arrayUnsorted)
        //            
        //        }
        //---------------------------------------------------------
        //arraySorted_ = colcSort.sortResults(arrayToSort: arrayUnsorted)
        //----------------------------------------------------------------------------------------
        
        //---------------------------------------------------------
        //GROUPED / SORTED
        //---------------------------------------------------------
        //Dictionary<String, Array<COLCQueryable>>
        //store the result with the query - so its cached
        self.dictionaryTableData_Result
            = self.colcGroupBy.groupResults(arrayToGroup : arrayCOLCQueryable_Filtered,
                                                 andSort : self.colcSort)
        

        //----------------------------------------------------------------------------------------
    }
    
    
  
    //--------------------------------------------------------------
    // MARK: - FILTER
    // MARK: -
    //--------------------------------------------------------------

    func filterMatches(colcQueryable: COLCQueryable) -> Bool{
        var passesAllFilters = true
        
//        for colcFilterParent: COLCFilterOrGroupParent in arrayOfCOLCFilterOrGroupParent{
//            
//            if let colcFilterGroup = colcFilterParent as? COLCFilterGroup {
//                
//                if colcFilterGroup.filterMatch(colcQueryable: colcQueryable){
//                    passesAllFilters = true
//                    
//                }else{
//                    passesAllFilters = false
//                }
//            }
//            else if let _ = colcFilterParent as? COLCFilter
//            {
//                //    if colcFilter.filterMatch(colcQueryable: colcQueryable){
//                //        //dont set to true - default is true - if all pass then leave at true
//                //        
//                //    }else{
//                //        //if ONE fails then ALL fail
//                //        logger.error("filterMatch failed:\(colcFilter)")
//                //        passesAllFilters = false
//                //        break
//                //    }
//                logger.error("colcFilterParent is not COLCFilterGroup put COLCFilter in a group and mark as .and or .or")
//            }
//            else{
//                logger.error("colcFilterParent is nil or not COLCFilterGroup or COLCFilter")
//            }
//        }
        
        
        //self.colcFilterGroupAND.appendCOLCFilter(colcFilter)
        
        if self.colcFilterGroupAND.filterMatch(colcQueryable: colcQueryable){
            passesAllFilters = true
            
        }else{
            passesAllFilters = false
        }
        
        
        return passesAllFilters
    }
}
