//
//  COLCFilterGroup.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 15/03/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

class COLCFilterGroup: COLCFilterOrGroupParent{
    
    //--------------------------------------------------------------
    // MARK: - and / or
    // MARK: -
    //--------------------------------------------------------------
    var colcFilterGroupType : COLCFilterGroupType
    
    init(colcFilterGroupType : COLCFilterGroupType) {
        self.colcFilterGroupType = colcFilterGroupType
    }
    
    //--------------------------------------------------------------
    // MARK: - FACTORY - if only one filter e.g commonName string search
    // MARK: -
    //--------------------------------------------------------------
    class func withSingleFilter(colcFilter: COLCFilter) -> COLCFilterGroup{
        
        let colcFilterGroup = COLCFilterGroup(colcFilterGroupType:.or)
        colcFilterGroup.appendCOLCFilter(colcFilter)
        return colcFilterGroup
        
    }
    
    //--------------------------------------------------------------
    // MARK: - Array of Filters in a group
    // MARK: -
    //--------------------------------------------------------------

    var arrayCOLCFilter = [COLCFilter]()
    
    func appendCOLCFilter(_ colcFilter: COLCFilter){
        self.arrayCOLCFilter.append(colcFilter)
    }
    
    //--------------------------------------------------------------
    // MARK: - filterMatch
    // MARK: -
    //--------------------------------------------------------------
    func filterMatch(colcQueryable: COLCQueryable) -> Bool{
        
        var filterMatch = false
        
        //--------------------------------------------------------------
        // MARK: - ITERATE OVER ALL FILTERS in this GROUP - and/or with others
        // MARK: -
        //--------------------------------------------------------------
        //logger.debug("[AND] ***** LOOP [arrayCOLCFilter:\(arrayCOLCFilter.count)] ************")
        var count = 0
        for colcFilter: COLCFilter in arrayCOLCFilter{
            
            if colcFilter.filterMatch(colcQueryable: colcQueryable){
                //logger.debug("[AND:\(count)] [\(colcFilter)] filterMatch\(count) = true")
                //save the value
                filterMatch = true
                
                //dont set to true - default is true - if all pass then leave at true
                
                switch colcFilterGroupType{
                case .and:
                    //---------------------------------------------------------
                    //AND
                    //---------------------------------------------------------
                    //need to check the rest - if any fails then all fails - this is tested in //ONE FAILED
                    //logger.debug("[AND:\(count)] one passed - AND -  check the rest - if any")
                    let _ = 0
                    
                case .or:
                    //---------------------------------------------------------
                    //OR - also handle GROUPS with one FILTER
                    //---------------------------------------------------------
                    //if at least one passed then they all passed - e.g test [.rest, .refreshment]  rankType is "Rest" - pass on first check
                    //NOISY - //logger.debug("one passed - OR - skip the rest")
                    break
                }
                
            
            }else{
                //ONE FAILED
                //NOISY - logger.error("[COLCFilterGroup] filterMatch failed:\(colcFilter)")
                
                //DONT USE switch the break only jumps out of the switch not the array of filters
                
                //                switch colcFilterGroupType{
                //                case .and:
                //                    //---------------------------------------------------------
                //                    //AND
                //                    //---------------------------------------------------------
                //                    //all filters in this group must pass
                //                    //if ONE fails then ALL fail
                //                    //logger.debug("[AND:\(count)] [\(colcFilter)] filterMatch\(count) = false")
                //                    filterMatch = false
                //                    break
                //                    
                //                case .or:
                //                    //---------------------------------------------------------
                //                    //OR - also handle GROUPS with one FILTER
                //                    //---------------------------------------------------------
                //                    //if ONE fails then check the rest
                //                    //NOISY //logger.debug("filterMatch failed:\(colcFilter) - OR - check the rest")
                //                    //filterMatch = false
                //                    //print(".or - ok - continue")
                //                    //switch demands some code
                //                    //logger.debug("[AND:\(count)] .or")
                //                    var crap = 0
                //                    crap += 1
                //                    //---------------------------------------------------------
                //                }
                
                
                if colcFilterGroupType == .and{
                    //---------------------------------------------------------
                    //AND
                    //---------------------------------------------------------
                    //all filters in this group must pass
                    //if ONE fails then ALL fail
                    //logger.debug("[AND:\(count)] [\(colcFilter)] filterMatch\(count) = false")
                    filterMatch = false
                    break
                }else if colcFilterGroupType == .or{
                    //---------------------------------------------------------
                    //OR
                    //---------------------------------------------------------
                     //logger.debug("[or:\(count)] .or")
                    
                }else{
                    logger.error("UNHANDLED - NOT .and / .or")
                }
                
            }
            count += 1
        }
        //logger.debug("[AND:\(count)] return filterMatch:\(filterMatch)")
        return filterMatch
    }
}
