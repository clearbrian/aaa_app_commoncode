//
//  COLCFilterBool.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 15/03/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

//--------------------------------------------------------------
// MARK: - COLCFilterInt
// MARK: -
//--------------------------------------------------------------
class COLCFilterBool: COLCFilter{
    
    //var propertyName : String >> in parent
    var searchBool: Bool
    
    init(   propertyName : String,
            searchBool : Bool)
    {
        self.searchBool = searchBool
        
        super.init(propertyName: propertyName)
        
    }
    
    //--------------------------------------------------------------
    // MARK: - filterMatch
    // MARK: -
    //--------------------------------------------------------------
    override func filterMatch(colcQueryable: COLCQueryable) -> Bool{
        
        var filterMatch = false
        
        if let propertyBool = colcQueryable.value(forPropertyName: self.propertyName) as? Bool {
            
            //---------------------------------------------------------
            //MATCH? Bool >> Bool
            //---------------------------------------------------------
            if propertyBool == self.searchBool{
                //NOISY logger.debug("[filterMatch? Bool] propertyBool:'\(propertyBool)' contains self.searchBool:'\(self.searchBool)' - MATCH")
                filterMatch = true
            }else{
                //NOISY logger.debug("[filterMatch? Bool] propertyBool:'\(propertyBool)' contains self.searchBool:'\(self.searchBool)' - NO MATCH")
                filterMatch = false
            }
        }else{
            logger.error("[filterMatch?] colcQueryable.value(forPropertyName: colcFilter.propertyName:'\(propertyName)' is nil OR Not Bool")
        }
        return filterMatch
    }
}
