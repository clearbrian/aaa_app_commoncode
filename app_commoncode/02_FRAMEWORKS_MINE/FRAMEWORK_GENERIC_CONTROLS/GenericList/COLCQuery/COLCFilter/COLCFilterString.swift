//
//  COLCFilterString.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 15/03/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

//--------------------------------------------------------------
// MARK: - COLCFilterString  - PARTIAL or EXACT search
// MARK: -
//--------------------------------------------------------------

class COLCFilterString: COLCFilter{
    
    var searchString: String
    var isPartialSearch: Bool
    var isCaseSensitive: Bool
    
    init(   propertyName : String,
            searchString : String,
            isPartialSearch : Bool,
            isCaseSensitive: Bool)
    {
        self.searchString = searchString
        self.isPartialSearch = isPartialSearch
        self.isCaseSensitive = isCaseSensitive
        
        super.init(propertyName: propertyName)
        
    }
    
    override func filterMatch(colcQueryable: COLCQueryable) -> Bool{
        var filterMatch = false
        
        if self.searchString == ""{
            //----------------------------------------------------------------------------------------
            //NO FILTER
            //----------------------------------------------------------------------------------------
            filterMatch = true
            //-------------------------------------------------------------------
            

        }else{
            //----------------------------------------------------------------------------------------
            //FILTER
            //----------------------------------------------------------------------------------------
            if let propertyString = colcQueryable.value(forPropertyName: self.propertyName) as? String {
                
                if self.isPartialSearch{
                    //---------------------------------------------------------
                    //STRING : PARTIAL SEARCH : contains
                    //---------------------------------------------------------
                    if self.isCaseSensitive{
                        if propertyString.contains(self.searchString){
                            //NOISY - logger.debug("[filterMatch? PARTIAL] propertyString:'\(propertyString)' contains self.searchString:'\(self.searchString)' - MATCH")
                            filterMatch = true
                        }else{
                            //NOISY - logger.debug("[filterMatch? PARTIAL] propertyString:'\(propertyString)' contains self.searchString:'\(self.searchString)' - NO MATCH")
                            filterMatch = false
                        }
                    }else{
                        if propertyString.uppercased().contains(self.searchString.uppercased()){
                            //NOISY - logger.debug("[filterMatch? PARTIAL] propertyString:'\(propertyString)' contains self.searchString:'\(self.searchString)' - MATCH")
                            filterMatch = true
                        }else{
                            //NOISY - logger.debug("[filterMatch? PARTIAL] propertyString:'\(propertyString)' contains self.searchString:'\(self.searchString)' - NO MATCH")
                            filterMatch = false
                        }
                    }
                    
                    
                }else{
                    //---------------------------------------------------------
                    //STRING : EXACT SEARCH - ==
                    //---------------------------------------------------------
                    
                    if self.isCaseSensitive{
                        if propertyString == self.searchString{
                            //NOISY - logger.debug("[filterMatch? EXACT] propertyString:'\(propertyString)' == self.searchString:'\(self.searchString)' - MATCH")
                            filterMatch = true
                        }else{
                            //NOISY - logger.debug("[filterMatch? EXACT] propertyString:'\(propertyString)' == self.searchString:'\(self.searchString)' - NO MATCH")
                            filterMatch = false
                        }
                    }else{

                        if propertyString.uppercased() == self.searchString.uppercased(){
                            //NOISY - logger.debug("[filterMatch? EXACT] propertyString:'\(propertyString)' == self.searchString:'\(self.searchString)' - MATCH")
                            filterMatch = true
                        }else{
                            //NOISY - logger.debug("[filterMatch? EXACT] propertyString:'\(propertyString)' == self.searchString:'\(self.searchString)' - NO MATCH")
                            filterMatch = false
                        }
                    }
                    
                    
                    
                }
            }else{
                logger.error("[filterMatch?] colcQueryable.value(forPropertyName: colcFilter.propertyName:'\(propertyName)' is nil OR Not String")
            }
            //-------------------------------------------------------------------
            
        }
        
        
        
        return filterMatch
    }
}
