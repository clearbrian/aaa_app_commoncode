//
//  COLCFilterDouble.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 15/03/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation


//Swift book - Double varies by platform 32bit/64 - Double prefered over Float
class COLCFilterDouble: COLCFilterNumber{
    
    
    var searchDouble: Double
    
    init(propertyName : String,
         searchDouble : Double,
         colcFilterNumberComparison : COLCFilterNumberComparison)
    {
        self.searchDouble = searchDouble
        
        super.init(propertyName: propertyName, colcFilterNumberComparison: colcFilterNumberComparison)
        
    }
    
    //--------------------------------------------------------------
    // MARK: - MATCH rules for Double
    // MARK: -
    //--------------------------------------------------------------
    
    override func filterMatch(colcQueryable: COLCQueryable) -> Bool{
        var filterMatch = false
        
        if let propertyDouble = colcQueryable.value(forPropertyName: self.propertyName) as? Double {
            
            //----------------------------------------------------------------------------------------
            //COLCFilterNumberComparison
            //----------------------------------------------------------------------------------------
            switch self.colcFilterNumberComparison{
            case .equals:
                if propertyDouble == self.searchDouble{
                    logger.debug("[filterMatch? equals] propertyDouble:'\(propertyDouble)' == searchDouble:'\(self.searchDouble)' - MATCH")
                    filterMatch = true
                }else{
                    logger.debug("[filterMatch? equals] propertyDouble:'\(propertyDouble)' == searchDouble:'\(self.searchDouble)' - NO MATCH")
                    filterMatch = false
                }
                
            case .greaterThan:
                if propertyDouble > self.searchDouble{
                    logger.debug("[filterMatch? greaterThan] propertyDouble:'\(propertyDouble) > searchDouble:'\(self.searchDouble)' - MATCH")
                    filterMatch = true
                }else{
                    logger.debug("[filterMatch? greaterThan] propertyDouble:'\(propertyDouble)' > searchDouble:'\(self.searchDouble)' - NO MATCH")
                    filterMatch = false
                }
                
            case .greaterThanOrEquals:
                if propertyDouble >= self.searchDouble{
                    logger.debug("[filterMatch? greaterThanOrEquals] propertyDouble:'\(propertyDouble)' >= searchDouble:'\(self.searchDouble)' - MATCH")
                    filterMatch = true
                }else{
                    logger.debug("[filterMatch? greaterThanOrEquals] propertyDouble:'\(propertyDouble)' >= searchDouble:'\(self.searchDouble)' - NO MATCH")
                    filterMatch = false
                }
                
            case .lessThan:
                if propertyDouble < self.searchDouble{
                    logger.debug("[filterMatch? lessThan] propertyDouble:'\(propertyDouble)' < searchDouble:'\(self.searchDouble)' - MATCH")
                    filterMatch = true
                }else{
                    logger.debug("[filterMatch? lessThan] propertyDouble:'\(propertyDouble)' < searchDouble:'\(self.searchDouble)' - NO MATCH")
                    filterMatch = false
                }
                
            case .lessThanOrEquals:
                if propertyDouble <= self.searchDouble{
                    logger.debug("[filterMatch? lessThanOrEquals] propertyDouble:'\(propertyDouble)' <= searchDouble:'\(self.searchDouble)' - MATCH")
                    filterMatch = true
                }else{
                    logger.debug("[filterMatch? lessThanOrEquals] propertyDouble:'\(propertyDouble)' <= searchDouble:'\(self.searchDouble)' - NO MATCH")
                    filterMatch = false
                }
            }
            //----------------------------------------------------------------------------------------
        }else{
            logger.error("[filterMatch? - Double] colcQueryable.value(forPropertyName: colcFilter.propertyName:'\(propertyName)' is nil OR Not Double")
        }
        return filterMatch
    }
}
