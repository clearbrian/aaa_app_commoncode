//
//  COLCFilterNumber.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 15/03/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

//--------------------------------------------------------------
// MARK: - COLCFilterNumbers
// MARK: -
//--------------------------------------------------------------
class COLCFilterNumber: COLCFilter{
    
    var colcFilterNumberComparison : COLCFilterNumberComparison
    
    init(propertyName: String,
         colcFilterNumberComparison: COLCFilterNumberComparison)
    {
        self.colcFilterNumberComparison = colcFilterNumberComparison
        
        super.init(propertyName: propertyName)
        
    }
}
