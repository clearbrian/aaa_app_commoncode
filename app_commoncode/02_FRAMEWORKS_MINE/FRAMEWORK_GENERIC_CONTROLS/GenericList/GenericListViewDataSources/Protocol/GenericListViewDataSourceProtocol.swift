//
//  GenericListViewDataSourceProtocol.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 17/01/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
//import UIKit

protocol GenericListViewDataSourceProtocol{
    
    
    func requery(colcQuery: COLCQuery)
    func requeryWithCurrentCOLCQuery()
    
    
    var dictionaryTableData : Dictionary<String, Array<Any>> {get}
    
    //----------------------------------------------------------------------------------------
    //SECTION KEYS
    //----------------------------------------------------------------------------------------
    var keysArrayUnsorted : Array<String> {get}
    var keysArraySorted   : Array<String> {get}
 
    //----------------------------------------------------------------------------------------
    //SECTIONS/ROWS
    //----------------------------------------------------------------------------------------

    func numberOfSections() -> Int
    func numberOfRowsInSection(_ section: Int) -> Int
    
    func rowDataArrayForSection (_ section: Int) -> Array<Any>
    
    //----------------------------------------------------------------------------------------
    //SECTION TITLES - nil to hide
    //----------------------------------------------------------------------------------------
    func titleForHeaderInSection(_ section: Int) -> String?
  
    //----------------------------------------------------------------------------------------
    //SIDE INDEX - RHS of table - jump to section
    //----------------------------------------------------------------------------------------

    func sectionIndexTitles() -> [String]?
    func sectionForSectionIndexTitle(title: String, at index: Int) -> Int
    func retrieveSortedKeyForSection(_ section: Int) ->String
    
    
    var totalsCount : Int {get}
    
}
