//
//  COLCQueryCollection.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 27/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation


/*
 ========================================================================================
 search by
 commonname
 borough
 
 filter by
	RankType
	borough
	
 Sort by
	nearest
	type
	number of spaces
	
 Group by
	commonname: a..z
	type
 
 rest/refreshment/working
	borough
 
 ========================================================================================
 TPlace	idString        String          TaxiRank_3207	a..z/z..a
        commonName      String          a..z/z..a	contains
        clLocation      CLLocation		nearest
        placeType       String          TaxiRank/JamCam         no	yes
	.	.	.	.	.	.
 Txi	Borough         String          "Tower Hamlets"         yes but group by better		yes + sort by a..z
        NumberOfSpaces	Int	5           0..n/n..0               no	no
        OperationDays	String          Mon - Sun               no	maybe show list of all strings grouped
        OperationTimes                  24 hours                no
        RankType        String >> Type	"Working">> .working			yes
 
 jCm	available       String >> Bool	"true" > true	yes but group by better	yes (true OR false)	true/false
        imageUrl            url                         no	no	no
        videoUrl            url                         no	no	no
        view                    towards place           no	no	no
 ========================================================================================
 */



class COLCQueryCollection{
    
    var delegate: COLCQueryCollectionDelegate?
    
 
    var arrayOfCOLCSort = [COLCSort]()
    
    
    /*
     // SELECT column-names //ignore whole object
     // FROM table-name     // common base type // TFLApiPlace or protocol
     
     // WHERE condition
     column = value
     
     
     // ORDER BY column-names
     // GROUP BY column
     // WHERE rankType = .rest
     
     */
    
    /*
     // SELECT column-names
     // FROM table-name
     
     // WHERE condition
     column = value
     
     
     // ORDER BY column-names
     // GROUP BY column
     // WHERE rankType = .rest
     */
    
    //A..Z / NEAREST
    //    var arrayORDERBY = [COLCQueryCollection.propertyName_clLocation]
    //    var arrayGROUPBY = [COLCQueryCollection.propertyName_commonName]
    

    
    //----------------------------------------------------------------------------------------
    //    var searchString: String{
    //        didSet {
    //            print("COLCQueryCollection searchString didSet: new value is '\(searchString)'")
    //            delegate_queryHasChanged()
    //            
    //        }
    //    }
    //    
    //    var genericListSort : GenericListSort{
    //        didSet {
    //            print("COLCQueryCollection searchString didSet: new value is '\(searchString)'")
    //            delegate_queryHasChanged()
    //            
    //        }
    //    }
    //----------------------------------------------------------------------------------------

    
    //--------------------------------------------------------------
    // MARK: - COLCQueryCollection
    // MARK: -
    //--------------------------------------------------------------
    var colcQueryDictionary = [String: COLCQuery]()
    
    
    //self.colcQueryDictionary.keys insertion order not guaranteed
    var colcQueryDictionaryKeys = [String]()

    
    //Subclassed
    //called in init
    func build_colcQueryDictionary() -> [String: COLCQuery]{
        
//        var colcQueryDictionary_ = [String: COLCQuery]()
//moved down
//        //----------------------------------------------------------------------------------------
//        //NEAREST
//        //----------------------------------------------------------------------------------------
//        //we want Nearest first
//        let titleNearest = COLCPlace.colcQuery_sortByNearest().title
//        colcQueryDictionary_[titleNearest] = COLCPlace.colcQuery_sortByNearest()
//        self.colcQueryDictionaryKeys.append(titleNearest)
        
//        //----------------------------------------------------------------------------------------
//        //NAME
//        //---------------------------------------------------------------------------------------- 
//        let titleName = COLCPlace.colcQuery_sortByName().title
//        
//        colcQueryDictionary_[titleName] = COLCPlace.colcQuery_sortByName()
//        
//        self.colcQueryDictionaryKeys.append(titleName)
//        //----------------------------------------------------------------------------------------
        
//        return colcQueryDictionary_
        
        return  [String: COLCQuery]()
        
    }
    

    //--------------------------------------------------------------
    // MARK: - INIT
    // MARK: -
    //--------------------------------------------------------------
    //DESIGNATED INITIALIZER
    //--------------------------------------------------------------
    init() {
        //----------------------------------------------------------------------------------------
        self.colcQueryDictionary = self.build_colcQueryDictionary()
        //----------------------------------------------------------------------------------------
    }
    
    var currentQueryIndex = 0{
        didSet {
             logger.debug("currentQueryIndex didSet \(currentQueryIndex)")
        }
    }
    
    var currentCOLCQuery: COLCQuery?{
        var currentCOLCQuery_: COLCQuery? = nil

        //order not guaranteed
        //let keysArray = Array(self.colcQueryDictionary.keys)
        
        //if key ISNT same as title this wont work
        let keysArray = self.colcQueryDictionaryKeys
        
        if keysArray.count > 0{
            //----------------------------------------------------------------------------------------
            if self.currentQueryIndex >= 0{
                if self.currentQueryIndex >= keysArray.count{
                    logger.error("currentQueryIndex[\(self.currentQueryIndex)] >= keysArray.count[\(keysArray.count)]")
                }else{
                
                    let key = keysArray[self.currentQueryIndex]
                    
                    if let colcQuery : COLCQuery = colcQueryDictionary[key]{
                       currentCOLCQuery_ = colcQuery
                    }else{
                        logger.error("colcQueryDictionary[\(key)] is nil")
                    }
                }
            }else{
                logger.error("currentQueryIndex is < 0")
            }
            //----------------------------------------------------------------------------------------
        }else{
            logger.error("colcQueryDictionary.keys.count is 0")
        }
        //NOISY logger.debug("currentCOLCQuery_ \(String(describing: currentCOLCQuery_?.title))")
        return currentCOLCQuery_
    }
    
    //--------------------------------------------------------------
    // MARK: - currentQueryIndexChanged
    // MARK: -
    //--------------------------------------------------------------
    func currentQueryIndexChanged(selectedIndex: Int) {


        //let keysArray = Array(self.colcQueryDictionary.keys)
        //if key ISNT same as title this wont work
        
        let keysArray = self.colcQueryDictionaryKeys
        
        if selectedIndex >= keysArray.count{
            logger.error("selectedIndex[\(selectedIndex)] >= keysArray.count[\(keysArray.count)]")
            
        }else{
            
            self.currentQueryIndex = selectedIndex
            
            let key = keysArray[selectedIndex]
                
            if let colcQuery : COLCQuery = colcQueryDictionary[key]{
                
                // TODO: - rename to currentQueryIndexHasChanged
                delegate_colcQuery_HasChanged(colcQuery: colcQuery)
            }else{
                logger.error("colcQueryDictionary[\(key)] is nil")
            }
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - COLCQueryCollectionDelegate
    // MARK: -
    //--------------------------------------------------------------
    // TODO: - change to currentQueryIndexHasChanged
    func delegate_colcQuery_HasChanged(colcQuery: COLCQuery){
        if let delegate = self.delegate{
            
            //------------------------------------------------
            delegate.colcQuery_HasChanged(colcQuery:colcQuery, inCOLCQueryCollection: self)
            //------------------------------------------------
            
        }else{
            logger.error("delegate is nil")
        }
    }
    
    
}
