//
//  OlaCabsKit.swift
//  joyride
//
//  Created by Brian Clear on 16/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation

/*
 https://developer.OlaCabs.com/docs/deeplinking
 
 https://docs.google.com/document/d/1Gn3Fo5-jWkGZumJLlEgK1nAizxCCMm5T0X4qTF7yeLE/edit
 //make sure you add this to Info.plist LSApplicationQueriesSchemes
 //    <key>CFBundleURLSchemes</key>
 //    <array>
 //    <string>fb1624443627822246</string>
 //    </array>
 */

class OlaCabsKit : ParentSwiftObject{
    //-----------------------------------------------------------------------------------
    //https://itunes.apple.com/gb/app/ola-cabs-book-taxi-one-touch/id539179365?mt=8
    //-----------------------------------------------------------------------------------
    static let openAppConfig = OpenAppConfigClipboard(openWithType : OpenWithType.openWithType_OlaCabs,
                                      appSchemeString     : "olacabs://",
                                      appWebsiteURLString : "http://www.olacabs.com/",
                                      appStoreIdUInt      : 539179365)
    //-----------------------------------------------------------------------------------
}
