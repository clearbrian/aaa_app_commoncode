//
//  ForecastIOViewController.swift
//  joyride
//
//  Created by Brian Clear on 13/04/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit

class ForecastIOViewController: UIViewController  {
     let forecastIOController = ForecastIOController()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let forecastIOForecastRequest = ForecastIOForecastRequest()
        forecastIOForecastRequest.lat = 51.5080776
        forecastIOForecastRequest.lng = -0.0717707
        
        //Time Machine Forecast in the future - ,time added to query otherwise defaults to Now()
        //forecastIOForecastRequest.forecastIOForecastType = .ForecastType_TimeMachine
        //todo forecastIOForecastRequest.timeMachineCallTime
        //[;[bg255,255,255;[fg140,105,251;forecastIOForecastResponse.currently.timeNSdate:Optional(2047-04-16 12:00:00 +0000) [; 
        
        sharedApplication.showNetworkActivity()
        
        self.getForecast(forecastIOForecastRequest,
            success:{(forecastIOForecastResponse: ForecastIOForecastResponse?) -> Void in
                
                //---------------------------------------------------------------------
                if let forecastIOForecastResponse = forecastIOForecastResponse {
                    print("forecastIOForecastResponse \(forecastIOForecastResponse)")
                }else{
                    logger.error("forecastIOForecastResponse is nil")
                }
                //---------------------------------------------------------------------
                //UIApplication.shared.isNetworkActivityIndicatorVisible = false
                sharedApplication.hideNetworkActivity()
                //---------------------------------------------------------------------
            },
            failure:{(error) -> Void in
                logger.error("error:\(error)")
                if let nserror = error {
                    print("nserror:\(nserror)")
                    //self.handleError(nserror)
                    
                }else{
                    logger.error("error is nil")
                }
                
              
                //UIApplication.shared.isNetworkActivityIndicatorVisible = false
                sharedApplication.hideNetworkActivity()

            }
        )
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getForecast(_ forecastIOForecastRequest: ForecastIOForecastRequest,
                              success: (_ forecastIOForecastResponse: ForecastIOForecastResponse?) -> Void,
                              failure: @escaping (_ error: Error?) -> Void
        )
    {
        //---------------------------------------------------------------------
        self.forecastIOController.getForecast(forecastIOForecastRequest,
             success:{
                (forecastIOForecastResponse: ForecastIOForecastResponse?)->Void in
                //---------------------------------------------------------------------
                if let forecastIOForecastResponse = forecastIOForecastResponse{
                    logger.info("forecastIOForecastResponse returned:\(forecastIOForecastResponse)")
                    
                    if let currently = forecastIOForecastResponse.currently {
                        //2016-04-15 17:01:44 +0000
                        logger.info("UTC:forecastIOForecastResponse.currently.timeNSdate:\(currently.timeNSdate)")
                        
                        if let timeNSdate = currently.timeNSdate {
                            //time is in UTC
                            let dateFormatterOUT = DateFormatter()
                            dateFormatterOUT.timeZone = TimeZone.current
                            
                            //(2016-04-15 17:07:24 +0000)
                             dateFormatterOUT.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                            
                            //Europe/London (BST) offset 3600 (Daylight)
                            //2016-04-15 17:07:24 +0000
                            let dateInUserTimeZone = dateFormatterOUT.string(from: timeNSdate)
                            logger.info("UTC:forecastIOForecastResponse.currently.timeNSdate LOCALE:\(dateInUserTimeZone)")
                            //LOCALE:2016-04-15T18:09:50.000+0100
                            print("")
                            
                    
                        }else{
                            logger.error("currently.timeNSdate  is nil")
                        }
                        
                        
                    }else{
                        logger.error("forecastIOForecastResponse.currently is nil")
                    }
                    
                    
    ////                                                        check the other forecast client for utils on converting F/C and any weather icons
    ////                                                        https://github.com/search?l=Swift&q=api.forecast.io&type=Repositories&utf8=%E2%9C%93
    ////                                                        do time machine request
    ////                                                        
    ////                                                        https://developer.forecast.io/docs/v2#time_call
    ////                                                        
    ////                                                        how do you get a map image?
    //                                                        test the forecast
    //                                                        time is in milliseconds - check other gothub for how to convert
    //                                                        
    //                                                        success(forecastIOForecastResponse: forecastIOForecastResponse)
                    
                }else{
                    logger.error("forecastIOForecastResponse is nil")
                }
            
            },
         failure:{
            (error) -> Void in
            return failure( error)
            }
        )
        //---------------------------------------------------------------------
    }
}



