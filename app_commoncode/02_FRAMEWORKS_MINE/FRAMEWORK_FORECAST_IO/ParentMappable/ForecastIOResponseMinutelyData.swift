//
//  ForecastIOResponseMinutelyData.swift
//  joyride
//
//  Created by Brian Clear on 13/04/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation

class ForecastIOResponseMinutelyData: ParentMappable {
    
    
    var time: NSNumber? //1460563020,
    var precipIntensity: NSNumber? // 0.0058,
    var precipIntensityError: NSNumber? // 0.0023,
    var precipProbability: NSNumber? // 0.18,
    var precipType: NSNumber? // "rain"
    
    //---------------------------------------------------------------------
    // root calls mapping()
    //    required init?(_ map: Map){
    //        mapping(map: map)
    //    }
    //---------------------------------------------------------------------
    
    //https://github.com/Hearst-DD/ObjectMapper
    override func mapping(map: Map){
        
        time <- map["time"]
        precipIntensity <- map["precipIntensity"]
        precipIntensityError <- map["precipIntensityError"]
        precipProbability <- map["precipProbability"]
        precipType <- map["precipType"]
        
    }
    
}
//data: [
//{
//time: 1460563020,
//precipIntensity: 0.0058,
//precipIntensityError: 0.0023,
//precipProbability: 0.18,
//precipType: "rain"
//},
