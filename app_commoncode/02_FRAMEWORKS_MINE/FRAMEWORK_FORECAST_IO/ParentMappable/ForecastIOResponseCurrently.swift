//
//  ForecastIOResponseCurrently.swift
//  joyride
//
//  Created by Brian Clear on 13/04/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation

class ForecastIOResponseCurrently: ParentMappable {

    
    var time: NSNumber? // 1460563028,
    
    
    var timeNSdate: Date? {
        var timeNSdate_: Date?
        
        if let time = self.time {
            timeNSdate_ = Date(timeIntervalSince1970:time.doubleValue)
          
            
        }else{
            logger.error("self.time is nil")
        }
        return timeNSdate_
    }

    
    
    var summary: NSString?  //"Partly Cloudy",
    var icon: NSString? //"partly-cloudy-day",
    var nearestStormDistance: NSNumber? //0,
    var nearestStormBearing: NSNumber? //0,
    
    var precipIntensity: NSNumber? //0.0058,
    var precipIntensityError: NSNumber? //0.0023,
    var precipProbability: NSNumber? //0.18,
    var precipType: NSString? //"rain",
    var temperature: NSNumber? //61.28,
    var apparentTemperature: NSNumber? //61.28,
    var dewPoint: NSNumber? //37.29,
    var humidity: NSNumber? //0.41,
    var windSpeed: NSNumber? //7.02,
    var windBearing: NSNumber? //243,
    var visibility: NSNumber? //10,
    var cloudCover: NSNumber? //0.39,
    var pressure: NSNumber? //1008.24,
    var ozone: NSNumber? //386.73
    
    //---------------------------------------------------------------------
    // root calls mapping()
    //    required init?(_ map: Map){
    //        mapping(map: map)
    //    }
    //---------------------------------------------------------------------
    
    //https://github.com/Hearst-DD/ObjectMapper
    override func mapping(map: Map){
        
        time <- map["time"]
        summary <- map["summary"]
        icon <- map["icon"]
        nearestStormDistance <- map["nearestStormDistance"]
        nearestStormBearing <- map["nearestStormBearing"]
        
        precipIntensity <- map["precipIntensity"]
        precipIntensityError <- map["precipIntensityError"]
        precipProbability <- map["precipProbability"]
        precipType <- map["precipType"]
        temperature <- map["temperature"]
        apparentTemperature <- map["apparentTemperature"]
        dewPoint <- map["dewPoint"]
        humidity <- map["humidity"]
        windSpeed <- map["windSpeed"]
        windBearing <- map["windBearing"]
        visibility <- map["visibility"]
        cloudCover <- map["cloudCover"]
        pressure <- map["pressure"]
        ozone <- map["ozone"]
        
    }
    
}

//time: 1460563028,
//summary: "Partly Cloudy",
//icon: "partly-cloudy-day",
//nearestStormDistance: 0,
//precipIntensity: 0.0058,
//precipIntensityError: 0.0023,
//precipProbability: 0.18,
//precipType: "rain",
//temperature: 61.28,
//apparentTemperature: 61.28,
//dewPoint: 37.29,
//humidity: 0.41,
//windSpeed: 7.02,
//windBearing: 243,
//visibility: 10,
//cloudCover: 0.39,
//pressure: 1008.24,
//ozone: 386.73

