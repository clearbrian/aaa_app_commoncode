//
//  ForecastIOForecastRequest.swift
//  joyride
//
//  Created by Brian Clear on 13/04/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation


/*
 https://developer.forecast.io/docs/v2#forecast_call
 
 units=[setting]: 
 
 Return the API response in units other than the default Imperial units. In particular, the following settings are possible:
 
     us: The default, as outlined above.
 
     si: Returns results in SI units. 
 
         In particular, properties now have the following units:
         summary: Any summaries containing temperature or snow accumulation units will have their values in degrees Celsius or in centimeters (respectively).
         nearestStormDistance: Kilometers.
         precipIntensity: Millimeters per hour.
         precipIntensityMax: Millimeters per hour.
         precipAccumulation: Centimeters.
         temperature: Degrees Celsius.
         temperatureMin: Degrees Celsius.
         temperatureMax: Degrees Celsius.
         apparentTemperature: Degrees Celsius.
         dewPoint: Degrees Celsius.
         windSpeed: Meters per second.
         pressure: Hectopascals (which are, conveniently, equivalent to the default millibars).
         visibility: Kilometers.
 
     ca: Identical to si, except that windSpeed is in kilometers per hour.
     uk2: Identical to si, except that windSpeed is in miles per hour, and nearestStormDistance and visibility are in miles, as in the US. (This option is provided because adoption of SI in the UK has been inconsistent.)
     auto: Selects the relevant units automatically, based on geographic location.
 
 */
enum ForecastIOForecastRequestUnits : String {
    case US = "us"
    case SI = "si"
    case CA = "ca"
    case UK2 = "uk2"
    case Auto = "auto" /* Selects the relevant units automatically, based on geographic location.*/
}

enum ForecastIOForecastType {
    case forecastType_ForecastCall /* https://api.forecast.io/forecast/APIKEY/LATITUDE,LONGITUDE      data in currently will be for NOW */
    case forecastType_TimeMachine  /* https://api.forecast.io/forecast/APIKEY/LATITUDE,LONGITUDE,TIME data in currently will be for TIME */
}


//Docs at bottom
//https://developer.forecast.io/docs/v2#forecast_call
class ForecastIOForecastRequest: ForecastIOParentRequest {
    
    var forecastIOForecastType : ForecastIOForecastType = .forecastType_ForecastCall{
        didSet {
            print("didSet: Old value: '\(oldValue)', New value: '\(lat)'")
            //-----------------------------------------------------------------------------------
            
            //will append the TIME to the query
            /* https://api.forecast.io/forecast/APIKEY/LATITUDE,LONGITUDE,TIME data in currently will be for TIME */
            updateQuery()
            //-----------------------------------------------------------------------------------
        }
    }
    //-----------------------------------------------------------------------------------
     //default to now - but only added to call if .ForecastType_TimeMachine
    var timeMachineCallTime = Date(){
        didSet {
            print("didSet: Old value: '\(oldValue)', New value: '\(lat)'")
            //-----------------------------------------------------------------------------------
            
            //will append the TIME to the query
            /* https://api.forecast.io/forecast/APIKEY/LATITUDE,LONGITUDE,TIME data in currently will be for TIME */
            updateQuery()
            //-----------------------------------------------------------------------------------
        }
    }
    
    //===============================
    //Optional parameters
    //===============================
    var units:ForecastIOForecastRequestUnits? = .Auto /* Selects the relevant units automatically, based on geographic location. */
    
    //===============================
    //Required parameters
    //===============================
    
    func updateQuery(){
        //-----------------------------------------------------------------------------------
        if let lat = self.lat {
            if let lng = self.lng {
                //-----------------------------------------------------------------------------------
                /* query must start with /  */
                
                switch self.forecastIOForecastType{
                case .forecastType_ForecastCall:
                    
                    query = "/\(lat),\(lng)"
                    
                case .forecastType_TimeMachine:
                    let time = "2016-04-15T12:00:00-0000"
                    
                    query = "/\(lat),\(lng),\(time)"
                    
                }
                //-----------------------------------------------------------------------------------

            }else{
                logger.error("self.lng is nil - query = nil")
                query = nil
            }
        }else{
            logger.error("self.lat is nil - query = nil")
            query = nil
        }
        //-----------------------------------------------------------------------------------
    }
    var lat: Double? {
        
        didSet {
            print("didSet: Old value: '\(oldValue)', New value: '\(lat)'")
            //-----------------------------------------------------------------------------------
            updateQuery()
            //-----------------------------------------------------------------------------------
        }
    }
    var lng: Double? {
        
        didSet {
            print("didSet: Old value: '\(oldValue)', New value: '\(lng)'")
            //-----------------------------------------------------------------------------------
            updateQuery()
            //-----------------------------------------------------------------------------------
        }
    }

    override init(){
        super.init()
        
        //use the convenience init below
//        query = "/\(lat),\(lng)"        /* must start with /  */
        
    }
    
    
    convenience init(
        lat: Double,
        lng: Double)
    {
        self.init()
        
        self.lat = lat
        self.lng = lng
        //done in willset
//        query = "/\(self.lat),\(self.lng)"        /* must start with /  */
        
    }
    
    override func parametersAppendAndCheckRequiredFieldsAreSet() -> Bool{
        var requiredFieldsAreSet = false
        
        let requiredFieldsAreSetInSuper = super.parametersAppendAndCheckRequiredFieldsAreSet()
        if requiredFieldsAreSetInSuper{
            
            //------------------------------------------------
            //address will be geocoded first
            //https:// maps.googleapis.com/maps/api/directions/json?lat=Toronto&lng=Montreal&key=YOUR_API_KEY
            //https:// maps.googleapis.com/maps/api/directions/json?lat=place_id:ChIJ685WIFYViEgRHlHvBbiD5nE&lng=place_id:ChIJA01I-8YVhkgRGJb0fW4UX7Y&key=YOUR_API_KEY
            //------------------------------------------------
            //------------------------------------------------
            
            //Added as part of the URL not afte the ?
            //            if let lat = lat{
//                if let lng = lng{
//                    //------------------------------------------------
//                    // TODO: - DOESNT HANDLED blanks
//                    parameters["lat"] = lat
//                    parameters["lng"] = lng
//                    
//                    requiredFieldsAreSet = true
//                    //------------------------------------------------
//                }else{
//                    logger.error("radius is nil")
//                }
//            }else{
//                logger.error("lat is nil")
//            }
            
            
            
            
            
//            if requiredFieldsAreSet{
            
                //---------------------------------------------------------------------
                //OPTIONAL PARAMETERS
                //---------------------------------------------------------------------
                if let units = units{
                    parameters["units"] = units.rawValue as AnyObject?
                }else{
                    //logger.info("mode is nil - OK - optional")
                }
                //---------------------------------------------------------------------
//            }else{
//                logger.error("requiredFieldsAreSet is false - skip OPTIONAL FIELDS")
//            }
            //------------------------------------------------
        }else{
            //required param missing in super class
            requiredFieldsAreSet = false
        }
        
        return requiredFieldsAreSet
    }
}
