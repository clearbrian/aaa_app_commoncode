//
//  ForecastIOWSController.swift
//  joyride
//
//  Created by Brian Clear on 13/04/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation


//
//  ForecastIOWSController.swift
//  joyride
//
//  Created by Brian Clear on 22/01/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation

//https://docs.google.com/document/d/1Vuv4kX_lmjoY6C13REX3APHMH4bzktfV7_o-19o1LHA/edit
class ForecastIOWSController: ParentWSController{
    
    
    // https://api.forecast.io/forecast/fe29b3a4003dc1b0c33828fc8732244c/51.5080776,-0.0717707
    
    func get_forecast(_ clkForecastIORequest: ForecastIOForecastRequest,
                               success: @escaping (_ forecastIOForecastResponse: ForecastIOForecastResponse?) -> Void,
                               failure: @escaping (_ error: Error?) -> Void
        )
    {

        //---------------------------------------------------------------------
        //CALL THE WS
        //---------------------------------------------------------------------
        //if let urlString =  clkForecastIORequest.urlString{
        if let _ =  clkForecastIORequest.urlString{
            
            //NOISY logger.debug("urlString:\r\(urlString)")
            //---------------------------------------------------------------------
            self.callWS(
                clkForecastIORequest,
                success:{ (clkForecastIORequest, parsedObject: Any?)->Void in
                    //------------------------------------------------------------------------------------------------
                    //---------------------------------------------------------------------
                    //response handler takes CLKForecastIOResponse? so use
                    //if let clkForecastIOResponse : CLKForecastIOResponse? =
                    //instead of
                    //if let clkForecastIOResponse =
                    //as this is a CLKForecastIOResponse not CLKForecastIOResponse? so response(clkForecastIOResponse) wont take it
                    if let forecastIOForecastResponse : ForecastIOForecastResponse = Mapper<ForecastIOForecastResponse>().map(JSONObject:parsedObject){
                        //---------------------------------------------------------------------
                        logger.info("forecastIOForecastResponse returned")
//                        //---------------------------------------------------------------------
//                        //check response status
//                        if let status = forecastIOForecastResponse.status{
//                            switch status{
//                            case GooglePlacesResponseStatus.OK.rawValue:
//                                logger.info("status: OK")
//                                //---------------------------------------------------------------------
//                                //RESPONSE OK
//                                //---------------------------------------------------------------------
//                                success(clkForecastIOResponse: clkForecastIOResponse)
//                                //---------------------------------------------------------------------
//                            // TODO: - HANDLE EACH OF THESE PROPERLY
//                            case GooglePlacesResponseStatus.ZERO_RESULTS.rawValue:
//                                logger.info("status: ZERO_RESULTS")
//                                //return failure(error: NSError.appError(.GooglePlacesResponseStatus_ZERO_RESULTS_PLACES))
//                                return failure(error: NSError.appError(.GooglePlacesResponseStatus_ZERO_RESULTS_DISTANCE))
//                                
//                            case GooglePlacesResponseStatus.OVER_QUERY_LIMIT.rawValue:
//                                logger.info("status: OVER_QUERY_LIMIT")
//                                return failure(error: NSError.appError(.GooglePlacesResponseStatus_OVER_QUERY_LIMIT))
//                                
//                            case GooglePlacesResponseStatus.REQUEST_DENIED.rawValue:
//                                logger.info("status: REQUEST_DENIED")
//                                return failure(error: NSError.appError(.GooglePlacesResponseStatus_REQUEST_DENIED))
//                                
//                            case GooglePlacesResponseStatus.INVALID_REQUEST.rawValue:
//                                
//                                logger.info("status: INVALID_REQUEST")
//                                return failure(error: NSError.appError(.GooglePlacesResponseStatus_INVALID_REQUEST))
//                                
//                            default:
//                                //HANDLE ALL ERRORS - if you need to handle specific ones such as NO RESULT can comment out here but probably should handle in delegate/caller
//                                logger.error("UNKNOWN status: \(status)")
//                                return failure(error: NSError.googlePlacesAPIStatusError(status))
//                            }
//                            
//                        }else{
//                            logger.error("clkForecastIOResponse.status is nil")
//                            return failure(error: NSError.unexpectedResponseObject("clkForecastIOResponse.status is nil"))
//                        }
                        
                        
                        //---------------------------------------------------------------------
                        //RESPONSE OK
                        //---------------------------------------------------------------------
                        success(forecastIOForecastResponse)
                        //---------------------------------------------------------------------
                        //------------------------------------------------------------------------------------------------
                        
                    }else{
                        logger.error("clkForecastIOResponse is nil or not CLKForecastIOResponse")
                    }
                    //------------------------------------------------------------------------------------------------
                    
                },
                failure:{ (clkForecastIORequest,error) -> Void in
                    //NO WS ERROR but may be status errors see success:
                    logger.error("error:\(error)")
                    failure(error)
                }
            )
            //---------------------------------------------------------------------
        }else{
            logger.error("urlString is nil - 33333")
        }
    }
}
