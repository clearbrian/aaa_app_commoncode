//
//	GeoJSONPointCartography.swift
//
//	Create by Brian Clear on 26/1/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
//import ObjectMapper


class GeoJSONPointCartography : ParentMappable{

	var labelX : Int?
	var labelY : Int?


	class func newInstance(map: Map) -> Mappable?{
		return GeoJSONPointCartography()
	}
	/*required init?(map: Map){}*/
	/*private override init(){}*/

	override func mapping(map: Map)
	{
		labelX <- map["labelX"]
		labelY <- map["labelY"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
   /* @objc required init(coder aDecoder: NSCoder)
	{
         labelX = aDecoder.decodeObject(forKey: "labelX") as? Int
         labelY = aDecoder.decodeObject(forKey: "labelY") as? Int

	}*/

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    /*@objc func encode(with aCoder: NSCoder)
	{
		if labelX != nil{
			aCoder.encode(labelX, forKey: "labelX")
		}
		if labelY != nil{
			aCoder.encode(labelY, forKey: "labelY")
		}

	}*/
	var description: String
	{
		let description_ = "**** \(type(of: self)) *****\r"
		//description_ = description_ + "labelX: \(String(describing: labelX))\r"
		//description_ = description_ + "labelY: \(String(describing: labelY))\r"
		return description_
	}

}
