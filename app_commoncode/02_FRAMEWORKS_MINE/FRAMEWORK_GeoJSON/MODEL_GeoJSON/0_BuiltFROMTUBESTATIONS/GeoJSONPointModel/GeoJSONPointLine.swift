//
//	GeoJSONPointLine.swift
//
//	Create by Brian Clear on 26/1/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
//import ObjectMapper


class GeoJSONPointLine : ParentMappable{

	var name : String?
	var opened : Int?


	class func newInstance(map: Map) -> Mappable?{
		return GeoJSONPointLine()
	}
	/*required init?(map: Map){}*/
	/*private override init(){}*/

	override func mapping(map: Map)
	{
		name <- map["name"]
		opened <- map["opened"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
   /* @objc required init(coder aDecoder: NSCoder)
	{
         name = aDecoder.decodeObject(forKey: "name") as? String
         opened = aDecoder.decodeObject(forKey: "opened") as? Int

	}*/

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    /*@objc func encode(with aCoder: NSCoder)
	{
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if opened != nil{
			aCoder.encode(opened, forKey: "opened")
		}

	}*/
	var description: String
	{
		let description_ = "**** \(type(of: self)) *****\r"
		//description_ = description_ + "name: \(String(describing: name))\r"
		//description_ = description_ + "opened: \(String(describing: opened))\r"
		return description_
	}

}
