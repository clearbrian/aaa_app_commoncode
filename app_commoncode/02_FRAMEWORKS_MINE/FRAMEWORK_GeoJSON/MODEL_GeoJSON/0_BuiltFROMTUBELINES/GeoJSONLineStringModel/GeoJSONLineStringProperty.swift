//
//	GeoJSONLineStringProperty.swift
//
//	Create by Brian Clear on 26/1/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
//import UIKit
//import ObjectMapper




class GeoJSONLineStringProperty : ParentMappable{

	var id : String?
	var lines : [GeoJSONLineStringLine]?

    


	class func newInstance(map: Map) -> Mappable?{
		return GeoJSONLineStringProperty()
	}
	/*required init?(map: Map){}*/
	/*private override init(){}*/

	override func mapping(map: Map)
	{
		id <- map["id"]
		lines <- map["lines"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
   /* @objc required init(coder aDecoder: NSCoder)
	{
         id = aDecoder.decodeObject(forKey: "id") as? String
         lines = aDecoder.decodeObject(forKey: "lines") as? [GeoJSONLineStringLine]

	}*/

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    /*@objc func encode(with aCoder: NSCoder)
	{
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if lines != nil{
			aCoder.encode(lines, forKey: "lines")
		}

	}*/
	var description: String
	{
		let description_ = "**** \(type(of: self)) *****\r"
		//description_ = description_ + "id: \(String(describing: id))\r"
		//description_ = description_ + "lines: \(String(describing: lines))\r"
		return description_
	}

}
