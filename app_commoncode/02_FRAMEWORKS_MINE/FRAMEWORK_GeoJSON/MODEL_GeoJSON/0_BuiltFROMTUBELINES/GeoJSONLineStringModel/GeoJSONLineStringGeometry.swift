//
//	GeoJSONLineStringGeometry.swift
//
//	Create by Brian Clear on 26/1/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
//import ObjectMapper
//import GoogleMaps
import CoreLocation




class GeoJSONLineStringGeometry : ParentMappable{

    //var coordinates : [[Float]]? //nil
    //var coordinates : Array<Any>? //OK
    //var coordinates : Array<Array<Double>>? //OK
    var coordinates   : [[Double]]?

	var type : String?


	class func newInstance(map: Map) -> Mappable?{
		return GeoJSONLineStringGeometry()
	}
	/*required init?(map: Map){}*/
	/*private override init(){}*/

	override func mapping(map: Map)
	{
		coordinates <- map["coordinates"]
		type <- map["type"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
   /* @objc required init(coder aDecoder: NSCoder)
	{
         coordinates = aDecoder.decodeObject(forKey: "coordinates") as? [[Float]]
         type = aDecoder.decodeObject(forKey: "type") as? String

	}*/

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    /*@objc func encode(with aCoder: NSCoder)
	{
		if coordinates != nil{
			aCoder.encode(coordinates, forKey: "coordinates")
		}
		if type != nil{
			aCoder.encode(type, forKey: "type")
		}

	}*/
	var description: String
	{
		let description_ = "**** \(Swift.type(of: self)) *****\r"
		//description_ = description_ + "coordinates: \(String(describing: coordinates))\r"
		//description_ = description_ + "type: \(String(describing: type))\r"
		return description_
	}

}
