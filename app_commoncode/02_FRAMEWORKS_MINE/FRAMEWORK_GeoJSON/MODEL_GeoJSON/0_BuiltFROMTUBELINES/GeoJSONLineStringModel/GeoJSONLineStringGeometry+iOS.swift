//
//	GeoJSONLineStringGeometry.swift
//
//	Create by Brian Clear on 26/1/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
//import ObjectMapper
import GoogleMaps
import CoreLocation


extension GeoJSONLineStringGeometry{
    

    func gmsPolyLineForGeometry(strokeWidth: CGFloat, strokeColor: UIColor) -> GMSPolyline?{
        
        var gmsPolyline_ : GMSPolyline?
    
        
        let path = GMSMutablePath()

        let arrayCLLocationCoordinate2D_ = self.arrayCLLocationCoordinate2D
        
        if arrayCLLocationCoordinate2D_.count > 0{
            for clLocationCoordinate2D in arrayCLLocationCoordinate2D_{
                 path.add(clLocationCoordinate2D)
            }
            
            gmsPolyline_ = GMSPolyline(path: path)
            gmsPolyline_?.strokeWidth = strokeWidth
            gmsPolyline_?.strokeColor = strokeColor  //.randomColor()
            //gmsPolyline_?.map = self.gmsMapView
            
        }else{
            logger.error("arrayCLLocationCoordinate2D_.count is 0")
        }
        //------------------------------------------------------------------------------------------------
        return gmsPolyline_
    }
}
