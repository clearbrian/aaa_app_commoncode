//
//  GeoJSONLineStringLine+LineColorMacOS.swift
//  mac_app_applemaps_tfl
//
//  Created by Brian Clear on 11/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
import UIKit
extension GeoJSONLineStringLine{
    var colourSafe : UIColor{
        var colourSafe_  : UIColor = .black
        if let colour_ = self.colour {
            
            colourSafe_ = UIColor.hexStringToUIColor(colour_)
            
        }else{
            logger.error("self.colour is nil - cant get line color")
        }
        return colourSafe_
    }
}
