//
//  GeoJSONLineStringProperty+iOS.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 11/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
import UIKit
extension GeoJSONLineStringProperty{
    
    var colorActiveLine: UIColor {
        var colorActiveLine_: UIColor = .cyan //if something wrong cant use red thats the central line
        
        if self.arrayActiveLines.count == 1{
            colorActiveLine_ = self.arrayActiveLines[0].colourSafe
            
        }else if self.arrayActiveLines.count > 1{
            
            //---------------------------------------------------------
            //for line: GeoJSONLineStringLine in self.arrayActiveLines{
            //
            //  colorActiveLine = line.colourSafe
            //}
            //---------------------------------------------------------
            colorActiveLine_ = self.arrayActiveLines[0].colourSafe
            
        }
        else{
            logger.error("self.arrayActiveLines.count : 0 - cant get line color")
        }
        return colorActiveLine_
    }
}
