//
//  TFLGeoJSONManager+iOS.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 11/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
import UIKit

class TFLGeoJSONManagerParentOS: GeoJSONManager{
    var dictionaryStationImages = [String : UIImage]()
}
