//
//  GeoJSONManager.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 26/01/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

//import UIKit


//--------------------------------------------------------------
// MARK: - TFLGeoJSONManagerDelegate
// MARK: -
//--------------------------------------------------------------

protocol TFLGeoJSONManagerDelegate {
    func tflGeoJSONManagerLinesAndStationsLoaded(tflGeoJSONManager: TFLGeoJSONManager)
    
}


class TFLGeoJSONManager: TFLGeoJSONManagerParentOS{
    var delegate: TFLGeoJSONManagerDelegate?
    var tflStations_geoJSONPointFeatureCollection: GeoJSONPointFeatureCollection?
    var tflStations_geoJSONLineStringFeatureCollection: GeoJSONLineStringFeatureCollection?
    
    var stationsLoaded = false
    var linesLoaded = false
    
    var arrayStations = [TFLStation]()
    
    override init() {
        super.init()
        
        //self.loadJSON()
        
        self.loadAllStations()
    }

    // TODO: - HARDCODED addTFLStation
    func loadAllStations(){
        addTFLStation("Abbey Road","940GZZDLABR",51.5324,0.0035303)
        addTFLStation("Abbey Wood","910GABWD",51.4911,0.120405)
        addTFLStation("Acton Central","910GACTNCTL",51.5087,-0.262833)
        addTFLStation("Acton Main Line","910GACTONML",51.5171,-0.267155)
        addTFLStation("Acton Town","940GZZLUACT",51.5026,-0.279917)
        addTFLStation("Addington Village","940GZZCRADV",51.3562,-0.0324952)
        addTFLStation("Addiscombe","940GZZCRADD",51.3799,-0.073267)
        addTFLStation("Aldgate East","940GZZLUADE",51.5151,-0.0718766)
        addTFLStation("Aldgate","940GZZLUALD",51.5141,-0.0757156)
        addTFLStation("All Saints","940GZZDLALL",51.5107,-0.0130713)
        addTFLStation("Alperton","940GZZLUALP",51.5406,-0.299455)
        addTFLStation("Amersham","940GZZLUAMS",51.6742,-0.608469)
        addTFLStation("Ampere Way","940GZZCRAMP",51.3824,-0.123687)
        addTFLStation("Anerley","910GANERLEY",51.4123,-0.065798)
        addTFLStation("Angel","940GZZLUAGL",51.5318,-0.10613)
        addTFLStation("Archway","940GZZLUACY",51.5654,-0.134745)
        addTFLStation("Arena","940GZZCRARA",51.3916,-0.0582529)
        addTFLStation("Arnos Grove","940GZZLUASG",51.6165,-0.133269)
        addTFLStation("Arsenal","940GZZLUASL",51.5578,-0.108099)
        addTFLStation("Avenue Road","940GZZCRAVE",51.407,-0.0489996)
        addTFLStation("Baker Street","940GZZLUBST",51.5225,-0.155512)
        addTFLStation("Balham","940GZZLUBLM",51.4432,-0.152988)
        addTFLStation("Bank","940GZZLUBNK",51.5135,-0.0889896)
        addTFLStation("Barbican","940GZZLUBBN",51.5202,-0.0977924)
        addTFLStation("Barking","940GZZLUBKG",51.5395,0.0812006)
        addTFLStation("Barkingside","940GZZLUBKE",51.5857,0.08858)
        addTFLStation("Barons Court","940GZZLUBSC",51.4903,-0.213749)
        addTFLStation("Battersea Park","910GBATRSPK",51.478,-0.147444)
        addTFLStation("Battersea Power Station","940GZZLU991",51.4794,-0.142994)
        addTFLStation("Bayswater","940GZZLUBWT",51.5123,-0.187837)
        addTFLStation("Beckenham Junction","940GZZCRBEK",51.4107,-0.0261587)
        addTFLStation("Beckenham Road","940GZZCRBRD",51.4096,-0.0431569)
        addTFLStation("Beckton Park","940GZZDLBPK",51.5087,0.0549757)
        addTFLStation("Beckton","940GZZDLBEC",51.5143,0.0610527)
        addTFLStation("Becontree","940GZZLUBEC",51.5403,0.127269)
        addTFLStation("Beddington Lane","940GZZCRBED",51.3895,-0.142846)
        addTFLStation("Belgrave Walk","940GZZCRBGV",51.401,-0.178357)
        addTFLStation("Belsize Park","940GZZLUBZP",51.5503,-0.164704)
        addTFLStation("Bermondsey","940GZZLUBMY",51.4977,-0.0637382)
        addTFLStation("Bethnal Green (NR)","910GBTHNLGR",51.5239,-0.059429)
        addTFLStation("Bethnal Green","940GZZLUBLG",51.5275,-0.0554885)
        addTFLStation("Birkbeck","940GZZCRBIR",51.4037,-0.0558016)
        addTFLStation("Blackfriars","940GZZLUBKF",51.5118,-0.10314)
        addTFLStation("Blackhorse Lane","940GZZCRBLA",51.3848,-0.0704624)
        addTFLStation("Blackhorse Road","940GZZLUBLR",51.5866,-0.0413391)
        addTFLStation("Blackwall","940GZZDLBLA",51.5079,-0.0072262)
        addTFLStation("Bond Street","940GZZLUBND",51.5143,-0.148986)
        addTFLStation("Borough","940GZZLUBOR",51.5011,-0.09341)
        addTFLStation("Boston Manor","940GZZLUBOS",51.4957,-0.324733)
        addTFLStation("Bounds Green","940GZZLUBDS",51.6071,-0.124135)
        addTFLStation("Bow Church","940GZZDLBOW",51.5277,-0.0209108)
        addTFLStation("Bow Road","940GZZLUBWR",51.5269,-0.0248416)
        addTFLStation("Brent Cross","940GZZLUBTX",51.5767,-0.213553)
        addTFLStation("Brentwood","910GBRTWOOD",51.6135,0.299834)
        addTFLStation("Brixton","940GZZLUBXN",51.4626,-0.114872)
        addTFLStation("Brockley","910GBROCKLY",51.4646,-0.0374688)
        addTFLStation("Bromley-by-Bow","940GZZLUBBB",51.5247,-0.0115851)
        addTFLStation("Brondesbury Park","910GBRBYPK",51.5407,-0.210148)
        addTFLStation("Brondesbury","910GBRBY",51.5451,-0.202323)
        addTFLStation("Bruce Grove","910GBRUCGRV",51.5937,-0.0700811)
        addTFLStation("Buckhurst Hill","940GZZLUBKH",51.6259,0.0467968)
        addTFLStation("Burnham","910GBNHAM",51.5234,-0.646353)
        addTFLStation("Burnt Oak","940GZZLUBTK",51.6028,-0.263952)
        addTFLStation("Bush Hill Park","910GBHILLPK",51.6412,-0.0690344)
        addTFLStation("Bushey","910GBUSHEY",51.6457,-0.385197)
        addTFLStation("Caledonian Road & Barnsbury","910GCLDNNRB",51.5436,-0.114988)
        addTFLStation("Caledonian Road","940GZZLUCAR",51.5484,-0.118367)
        addTFLStation("Cambridge Heath","910GCAMHTH",51.5324,-0.0575927)
        addTFLStation("Camden Road","910GCMDNRD",51.542,-0.139464)
        addTFLStation("Camden Town","940GZZLUCTN",51.5389,-0.142557)
        addTFLStation("Canada Water","940GZZLUCWR",51.498,-0.0502081)
        addTFLStation("Canary Wharf (Crossrail)","910G950",51.5063,-0.0174659)
        addTFLStation("Canary Wharf (DLR)","940GZZDLCAN",51.5052,-0.0208973)
        addTFLStation("Canary Wharf","940GZZLUCYF",51.5035,-0.018612)
        addTFLStation("Canning Town","940GZZLUCGT",51.514,0.0081707)
        addTFLStation("Cannon Street","940GZZLUCST",51.5114,-0.0903232)
        addTFLStation("Canonbury","910GCNNB",51.5487,-0.0926833)
        addTFLStation("Canons Park","940GZZLUCPK",51.6078,-0.294744)
        addTFLStation("Carpenders Park","910GCRPNDPK",51.6284,-0.385674)
        addTFLStation("Cassiobridge","940GZZLU960",51.6506,-0.424368)
        addTFLStation("Centrale","940GZZCRCTR",51.3759,-0.103774)
        addTFLStation("Chadwell Heath","910GCHDWLHT",51.5679,0.12906)
        addTFLStation("Chalfont & Latimer","940GZZLUCAL",51.668,-0.560821)
        addTFLStation("Chalk Farm","940GZZLUCFM",51.5442,-0.15347)
        addTFLStation("Chancery Lane","940GZZLUCHL",51.5181,-0.112121)
        addTFLStation("Charing Cross","940GZZLUCHX",51.5073,-0.127362)
        addTFLStation("Chesham","940GZZLUCSM",51.7054,-0.61093)
        addTFLStation("Cheshunt","910GCHESHNT",51.7029,-0.0239747)
        addTFLStation("Chigwell","940GZZLUCWL",51.6178,0.0747482)
        addTFLStation("Chingford","910GCHINGFD",51.6332,0.0100785)
        addTFLStation("Chiswick Park","940GZZLUCWP",51.4946,-0.268122)
        addTFLStation("Chorleywood","940GZZLUCYD",51.6542,-0.518356)
        addTFLStation("Church Street","940GZZCRCHR",51.3736,-0.104029)
        addTFLStation("Clapham Common","940GZZLUCPC",51.462,-0.137589)
        addTFLStation("Clapham High Street","910GCLPHHS",51.4655,-0.132392)
        addTFLStation("Clapham Junction","910GCLPHMJ1",51.4648,-0.171967)
        addTFLStation("Clapham North","940GZZLUCPN",51.4651,-0.129898)
        addTFLStation("Clapham South","940GZZLUCPS",51.4528,-0.147584)
        addTFLStation("Clapton","910GCLAPTON",51.5617,-0.0568321)
        addTFLStation("Cockfosters","940GZZLUCKS",51.6516,-0.14935)
        addTFLStation("Colindale","940GZZLUCND",51.5953,-0.249783)
        addTFLStation("Colliers Wood","940GZZLUCSD",51.4181,-0.17809)
        addTFLStation("Coombe Lane","9400ZZCRCOO",51.3598,-0.0600865)
        addTFLStation("Covent Garden","940GZZLUCGN",51.5131,-0.124196)
        addTFLStation("Crossharbour","940GZZDLCLA",51.4958,-0.0144681)
        addTFLStation("Crouch Hill","910GCROUCHH",51.5713,-0.117195)
        addTFLStation("Croxley","940GZZLUCXY",51.6471,-0.441667)
        addTFLStation("Crystal Palace","910GCRYSTLP",51.4179,-0.0722046)
        addTFLStation("Custom House","940GZZDLCUS",51.5097,0.0263723)
        addTFLStation("Cutty Sark","940GZZDLCUT",51.4817,-0.0107208)
        addTFLStation("Cyprus","940GZZDLCYP",51.5084,0.0639712)
        addTFLStation("Dagenham East","940GZZLUDGE",51.5442,0.165642)
        addTFLStation("Dagenham Heathway","940GZZLUDGY",51.5417,0.1477)
        addTFLStation("Dalston Junction","910GDALS",51.5461,-0.0749078)
        addTFLStation("Dalston Kingsland","910GDALSKLD",51.5482,-0.0758235)
        addTFLStation("Debden","940GZZLUDBN",51.6453,0.0835733)
        addTFLStation("Denmark Hill","910GDENMRKH",51.4682,-0.0890057)
        addTFLStation("Deptford Bridge","940GZZDLDEP",51.4741,-0.022394)
        addTFLStation("Devons Road","940GZZDLDEV",51.5225,-0.0175011)
        addTFLStation("Dollis Hill","940GZZLUDOH",51.5521,-0.239229)
        addTFLStation("Dundonald Road","940GZZCRDDR",51.4175,-0.207618)
        addTFLStation("Ealing Broadway","940GZZLUEBY",51.5148,-0.301543)
        addTFLStation("Ealing Common","940GZZLUECM",51.5099,-0.288181)
        addTFLStation("Earl's Court","940GZZLUECT",51.4915,-0.194168)
        addTFLStation("East Acton","940GZZLUEAN",51.5169,-0.247572)
        addTFLStation("East Croydon","940GZZCRECR",51.3748,-0.0927132)
        addTFLStation("East Finchley","940GZZLUEFY",51.5876,-0.165219)
        addTFLStation("East Ham","940GZZLUEHM",51.5392,0.0520114)
        addTFLStation("East India","940GZZDLEIN",51.5094,-0.002259)
        addTFLStation("East Putney","940GZZLUEPY",51.4591,-0.211062)
        addTFLStation("Eastcote","940GZZLUEAE",51.5766,-0.397075)
        addTFLStation("Edgware Road (Bakerloo)","940GZZLUERB",51.5205,-0.170339)
        addTFLStation("Edgware Road","940GZZLUERC",51.5197,-0.168088)
        addTFLStation("Edgware","940GZZLUEGW",51.614,-0.275504)
        addTFLStation("Edmonton Green","910GEDMNGRN",51.625,-0.0612434)
        addTFLStation("Elephant & Castle","940GZZLUEAC",51.4954,-0.100545)
        addTFLStation("Elm Park","940GZZLUEPK",51.5496,0.197464)
        addTFLStation("Elmers End","940GZZCRELM",51.3983,-0.0496933)
        addTFLStation("Elverson Road","940GZZDLELV",51.4691,-0.0167864)
        addTFLStation("Embankment","940GZZLUEMB",51.507,-0.122577)
        addTFLStation("Emerson Park","910GEMRSPKH",51.5686,0.220267)
        addTFLStation("Emirates Greenwich Peninsula","940GZZALGWP",51.4997,0.00821139)
        addTFLStation("Emirates Royal Docks","940GZZALRDK",51.5078,0.0180856)
        addTFLStation("Enfield Town","910GENFLDTN",51.652,-0.0793861)
        addTFLStation("Epping","940GZZLUEPG",51.6942,0.114528)
        addTFLStation("Euston Square","940GZZLUESQ",51.5257,-0.135534)
        addTFLStation("Euston","940GZZLUEUS",51.5284,-0.134286)
        addTFLStation("Fairlop","940GZZLUFLP",51.596,0.0911132)
        addTFLStation("Farringdon","940GZZLUFCN",51.5202,-0.104666)
        addTFLStation("Fieldway","940GZZCRFLD",51.351,-0.0242146)
        addTFLStation("Finchley Central","940GZZLUFYC",51.6007,-0.191944)
        addTFLStation("Finchley Road & Frognal","910GFNCHLYR",51.55,-0.183954)
        addTFLStation("Finchley Road","940GZZLUFYR",51.5474,-0.180884)
        addTFLStation("Finsbury Park","940GZZLUFPK",51.5646,-0.106338)
        addTFLStation("Forest Gate","910GFRSTGT",51.5495,0.0241217)
        addTFLStation("Forest Hill","910GFORESTH",51.4398,-0.0528341)
        addTFLStation("Fulham Broadway","940GZZLUFBY",51.4802,-0.195281)
        addTFLStation("Gallions Reach","940GZZDLGAL",51.509,0.0717712)
        addTFLStation("Gants Hill","940GZZLUGTH",51.5766,0.0663506)
        addTFLStation("George Street","940GZZCRCEN",51.3739,-0.0987101)
        addTFLStation("Gidea Park","910GGIDEAPK",51.5816,0.205288)
        addTFLStation("Gloucester Road","940GZZLUGTR",51.4944,-0.183472)
        addTFLStation("Golders Green","940GZZLUGGN",51.5724,-0.194086)
        addTFLStation("Goldhawk Road","940GZZLUGHK",51.5019,-0.226674)
        addTFLStation("Goodge Street","940GZZLUGDG",51.5205,-0.134358)
        addTFLStation("Goodmayes","910GGODMAYS",51.5655,0.110997)
        addTFLStation("Gospel Oak","910GGOSPLOK",51.5557,-0.152153)
        addTFLStation("Grange Hill","940GZZLUGGH",51.6131,0.0925161)
        addTFLStation("Gravel Hill","940GZZCRGRA",51.3545,-0.0428615)
        addTFLStation("Great Portland Street","940GZZLUGPS",51.5238,-0.143921)
        addTFLStation("Green Park","940GZZLUGPK",51.5066,-0.143119)
        addTFLStation("Greenford","940GZZLUGFD",51.5422,-0.344816)
        addTFLStation("Greenwich","940GZZDLGRE",51.478,-0.0154873)
        addTFLStation("Gunnersbury","940GZZLUGBY",51.4916,-0.27538)
        addTFLStation("Hackney Central","910GHACKNYC",51.5471,-0.0573172)
        addTFLStation("Hackney Downs","910GHAKNYNM",51.5491,-0.0611511)
        addTFLStation("Hackney Wick","910GHACKNYW",51.5434,-0.0248344)
        addTFLStation("Haggerston","910GHAGGERS",51.5389,-0.0755378)
        addTFLStation("Hainault","940GZZLUHLT",51.6037,0.0934718)
        addTFLStation("Hammersmith (H&C)","940GZZLUHSC",51.4941,-0.225083)
        addTFLStation("Hammersmith","940GZZLUHSD",51.4921,-0.223331)
        addTFLStation("Hampstead Heath","910GHMPSTDH",51.5552,-0.165335)
        addTFLStation("Hampstead","940GZZLUHTD",51.5564,-0.178196)
        addTFLStation("Hanger Lane","940GZZLUHGR",51.5303,-0.293287)
        addTFLStation("Hanwell","910GHANWELL",51.5118,-0.338596)
        addTFLStation("Harlesden","940GZZLUHSN",51.5364,-0.257954)
        addTFLStation("Harold Wood","910GHRLDWOD",51.5927,0.233006)
        addTFLStation("Harringay Green Lanes","910GHRGYGL",51.5772,-0.097821)
        addTFLStation("Harrington Road","940GZZCRHAR",51.3997,-0.0604811)
        addTFLStation("Harrow & Wealdstone","940GZZLUHAW",51.5923,-0.335399)
        addTFLStation("Harrow-on-the-Hill","940GZZLUHOH",51.5792,-0.336305)
        addTFLStation("Hatch End","910GHTCHEND",51.6095,-0.368613)
        addTFLStation("Hatton Cross","940GZZLUHNX",51.4667,-0.423117)
        addTFLStation("Hayes & Harlington","910GHAYESAH",51.503,-0.419624)
        addTFLStation("Headstone Lane","910GHEDSTNL",51.6024,-0.356632)
        addTFLStation("Heathrow Terminal 4","940GZZLUHR4",51.4587,-0.445435)
        addTFLStation("Heathrow Terminal 5","940GZZLUHR5",51.4725,-0.490104)
        addTFLStation("Heathrow Terminals 2 & 3","940GZZLUHRC",51.4725,-0.455001)
        addTFLStation("Hendon Central","940GZZLUHCL",51.5831,-0.226099)
        addTFLStation("Heron Quays","940GZZDLHEQ",51.5033,-0.0214372)
        addTFLStation("High Barnet","940GZZLUHBT",51.6509,-0.194816)
        addTFLStation("High Street Kensington","940GZZLUHSK",51.5009,-0.19266)
        addTFLStation("Highams Park","910GHGHMSPK",51.6089,-0.000198)
        addTFLStation("Highbury & Islington","940GZZLUHAI",51.5463,-0.103525)
        addTFLStation("Highgate","940GZZLUHGT",51.5776,-0.145782)
        addTFLStation("Hillingdon","940GZZLUHGD",51.5537,-0.450198)
        addTFLStation("Holborn","940GZZLUHBN",51.5176,-0.120177)
        addTFLStation("Holland Park","940GZZLUHPK",51.5073,-0.205642)
        addTFLStation("Holloway Road","940GZZLUHWY",51.5527,-0.113009)
        addTFLStation("Homerton","910GHOMRTON",51.5471,-0.0425037)
        addTFLStation("Honor Oak Park","910GHONROPK",51.45,-0.0453613)
        addTFLStation("Hornchurch","940GZZLUHCH",51.554,0.218542)
        addTFLStation("Hounslow Central","940GZZLUHWC",51.4713,-0.366413)
        addTFLStation("Hounslow East","940GZZLUHWE",51.4732,-0.356313)
        addTFLStation("Hounslow West","940GZZLUHWT",51.4735,-0.386447)
        addTFLStation("Hoxton","910GHOXTON",51.5315,-0.0756251)
        addTFLStation("Hyde Park Corner","940GZZLUHPC",51.503,-0.152557)
        addTFLStation("Ickenham","940GZZLUICK",51.562,-0.442056)
        addTFLStation("Ilford","910GILFORD",51.559,0.0699171)
        addTFLStation("Imperial Wharf","910GCSEAH",51.4753,-0.183135)
        addTFLStation("Island Gardens","940GZZDLISL",51.4879,-0.0102938)
        addTFLStation("Iver","910GIVER",51.5086,-0.506544)
        addTFLStation("Kennington","940GZZLUKNG",51.4888,-0.105553)
        addTFLStation("Kensal Green","940GZZLUKSL",51.5306,-0.224596)
        addTFLStation("Kensal Rise","910GKENR",51.5344,-0.220226)
        addTFLStation("Kensington (Olympia)","940GZZLUKOY",51.4975,-0.209593)
        addTFLStation("Kentish Town West","910GKNTSHTW",51.5471,-0.14675)
        addTFLStation("Kentish Town","940GZZLUKSH",51.5503,-0.14074)
        addTFLStation("Kenton","940GZZLUKEN",51.5815,-0.31662)
        addTFLStation("Kew Gardens","940GZZLUKWG",51.4771,-0.285013)
        addTFLStation("Kilburn High Road","910GKLBRNHR",51.5377,-0.191495)
        addTFLStation("Kilburn Park","940GZZLUKPK",51.5352,-0.194134)
        addTFLStation("Kilburn","940GZZLUKBN",51.547,-0.205286)
        addTFLStation("King George V","940GZZDLKGV",51.502,0.0621579)
        addTFLStation("King Henry's Drive","9400ZZCRKGH",51.3455,-0.0203926)
        addTFLStation("King's Cross St. Pancras","940GZZLUKSX",51.5306,-0.122944)
        addTFLStation("Kingsbury","940GZZLUKBY",51.5847,-0.278623)
        addTFLStation("Knightsbridge","940GZZLUKNB",51.5016,-0.160592)
        addTFLStation("Ladbroke Grove","940GZZLULAD",51.5173,-0.210942)
        addTFLStation("Lambeth North","940GZZLULBN",51.4989,-0.112287)
        addTFLStation("Lancaster Gate","940GZZLULGT",51.5119,-0.175145)
        addTFLStation("Langdon Park","940GZZDLLDP",51.5151,-0.0141129)
        addTFLStation("Langley","910GLANGLEY",51.5079,-0.54121)
        addTFLStation("Latimer Road","940GZZLULRD",51.5134,-0.21791)
        addTFLStation("Lebanon Road","940GZZCRLEB",51.3751,-0.0845004)
        addTFLStation("Leicester Square","940GZZLULSQ",51.5112,-0.12849)
        addTFLStation("Lewisham","940GZZDLLEW",51.4647,-0.0129099)
        addTFLStation("Leyton Midland Road","910GLEYTNMR",51.5695,-0.0072681)
        addTFLStation("Leyton","940GZZLULYN",51.5566,-0.0054072)
        addTFLStation("Leytonstone High Road","910GLYTNSHR",51.5639,0.0077122)
        addTFLStation("Leytonstone","940GZZLULYS",51.5686,0.0084026)
        addTFLStation("Limehouse","940GZZDLLIM",51.5124,-0.0396872)
        addTFLStation("Liverpool Street","940GZZLULVT",51.5173,-0.083213)
        addTFLStation("Lloyd Park","940GZZCRLOY",51.3642,-0.0806955)
        addTFLStation("London Bridge","940GZZLULNB",51.505,-0.089635)
        addTFLStation("London City Airport","940GZZDLLCA",51.5037,0.0480118)
        addTFLStation("London Fields","910GLONFLDS",51.5412,-0.0578549)
        addTFLStation("Loughton","940GZZLULGN",51.6414,0.0552549)
        addTFLStation("Maida Vale","940GZZLUMVL",51.5298,-0.185775)
        addTFLStation("Maidenhead","910GMDNHEAD",51.5183,-0.722758)
        addTFLStation("Manor House","940GZZLUMRH",51.5705,-0.0961857)
        addTFLStation("Manor Park","910GMANRPK",51.5524,0.0457816)
        addTFLStation("Mansion House","940GZZLUMSH",51.5119,-0.0937541)
        addTFLStation("Marble Arch","940GZZLUMBA",51.5135,-0.157836)
        addTFLStation("Maryland","910GMRYLAND",51.5462,0.0062276)
        addTFLStation("Marylebone","940GZZLUMYB",51.5223,-0.163222)
        addTFLStation("Merton Park","940GZZCRMTP",51.4138,-0.201386)
        addTFLStation("Mile End","940GZZLUMED",51.5254,-0.0334449)
        addTFLStation("Mill Hill East","940GZZLUMHL",51.6082,-0.210151)
        addTFLStation("Mitcham Junction","940GZZCRMJT",51.3926,-0.157297)
        addTFLStation("Mitcham","940GZZCRMCH",51.3976,-0.171138)
        addTFLStation("Monument","940GZZLUMMT",51.5107,-0.0864765)
        addTFLStation("Moor Park","940GZZLUMPK",51.6298,-0.432234)
        addTFLStation("Moorgate","940GZZLUMGT",51.5181,-0.0882901)
        addTFLStation("Morden Road","940GZZCRMDN",51.4089,-0.192912)
        addTFLStation("Morden","940GZZLUMDN",51.4023,-0.194734)
        addTFLStation("Mornington Crescent","940GZZLUMTC",51.5344,-0.138539)
        addTFLStation("Mudchute","940GZZDLMUD",51.4917,-0.0149536)
        addTFLStation("Neasden","940GZZLUNDN",51.5542,-0.249963)
        addTFLStation("New Addington","940GZZCRNWA",51.3425,-0.0173388)
        addTFLStation("New Cross Gate","910GNEWXGEL",51.4754,-0.040267)
        addTFLStation("New Cross","910GNWCRELL",51.4766,-0.0323412)
        addTFLStation("Newbury Park","940GZZLUNBP",51.5756,0.0899473)
        addTFLStation("Nine Elms","940GZZLU990",51.4802,-0.128617)
        addTFLStation("North Acton","940GZZLUNAN",51.5236,-0.259714)
        addTFLStation("North Ealing","940GZZLUNEN",51.5177,-0.288536)
        addTFLStation("North Greenwich","940GZZLUNGW",51.5001,0.0038848)
        addTFLStation("North Harrow","940GZZLUNHA",51.585,-0.362797)
        addTFLStation("North Wembley","940GZZLUNWY",51.5624,-0.303875)
        addTFLStation("Northfields","940GZZLUNFD",51.4996,-0.313162)
        addTFLStation("Northolt","940GZZLUNHT",51.5484,-0.369071)
        addTFLStation("Northwick Park","940GZZLUNKP",51.5785,-0.317777)
        addTFLStation("Northwood Hills","940GZZLUNWH",51.6004,-0.408963)
        addTFLStation("Northwood","940GZZLUNOW",51.6108,-0.423729)
        addTFLStation("Norwood Junction","910GNORWDJ",51.3972,-0.0746248)
        addTFLStation("Notting Hill Gate","940GZZLUNHG",51.5092,-0.196095)
        addTFLStation("Oakwood","940GZZLUOAK",51.6474,-0.131485)
        addTFLStation("Old Street","940GZZLUODS",51.5256,-0.0875418)
        addTFLStation("Osterley","940GZZLUOSY",51.4811,-0.352297)
        addTFLStation("Oval","940GZZLUOVL",51.4818,-0.112369)
        addTFLStation("Oxford Circus","940GZZLUOXC",51.5153,-0.14208)
        addTFLStation("Paddington (Praed St)","940GZZLUPAC",51.516,-0.174491)
        addTFLStation("Paddington","940GZZLUPAH",51.5173,-0.179204)
        addTFLStation("Park Royal","940GZZLUPKR",51.5265,-0.283955)
        addTFLStation("Parsons Green","940GZZLUPSG",51.4753,-0.201087)
        addTFLStation("Peckham Rye","910GPCKHMRY",51.4694,-0.0703864)
        addTFLStation("Penge West","910GPENEW",51.4175,-0.0606364)
        addTFLStation("Perivale","940GZZLUPVL",51.5367,-0.32358)
        addTFLStation("Phipps Bridge","940GZZCRPHI",51.4033,-0.182106)
        addTFLStation("Piccadilly Circus","940GZZLUPCC",51.51,-0.133442)
        addTFLStation("Pimlico","940GZZLUPCO",51.4889,-0.132954)
        addTFLStation("Pinner","940GZZLUPNR",51.5927,-0.38081)
        addTFLStation("Plaistow","940GZZLUPLW",51.5312,0.0165714)
        addTFLStation("Pontoon Dock","940GZZDLPDK",51.5022,0.0327038)
        addTFLStation("Poplar","940GZZDLPOP",51.5078,-0.017478)
        addTFLStation("Preston Road","940GZZLUPRD",51.5721,-0.295144)
        addTFLStation("Prince Regent","940GZZDLPRE",51.5095,0.0336226)
        addTFLStation("Pudding Mill Lane","940GZZDLPUD",51.5338,-0.0135153)
        addTFLStation("Putney Bridge","940GZZLUPYB",51.4683,-0.208817)
        addTFLStation("Queen's Park","940GZZLUQPS",51.534,-0.205506)
        addTFLStation("Queens Road Peckham","910GPCKHMQD",51.4741,-0.0569919)
        addTFLStation("Queensbury","940GZZLUQBY",51.5942,-0.286186)
        addTFLStation("Queensway","940GZZLUQWY",51.5105,-0.18723)
        addTFLStation("Ravenscourt Park","940GZZLURVP",51.4941,-0.235797)
        addTFLStation("Rayners Lane","940GZZLURYL",51.5752,-0.371281)
        addTFLStation("Reading","910GRDNGSTN",51.4592,-0.972914)
        addTFLStation("Rectory Road","910GRCTRYRD",51.5586,-0.0684446)
        addTFLStation("Redbridge","940GZZLURBG",51.5764,0.045411)
        addTFLStation("Reeves Corner","940GZZCRRVC",51.3749,-0.106321)
        addTFLStation("Regent's Park","940GZZLURGP",51.5234,-0.1465)
        addTFLStation("Richmond","940GZZLURMD",51.4633,-0.301184)
        addTFLStation("Rickmansworth","940GZZLURKW",51.6402,-0.473583)
        addTFLStation("Roding Valley","940GZZLURVY",51.6171,0.0438798)
        addTFLStation("Romford","910GROMFORD",51.5749,0.183417)
        addTFLStation("Rotherhithe","910GRTHERHI",51.501,-0.0521216)
        addTFLStation("Royal Albert","940GZZDLRAL",51.5083,0.0456575)
        addTFLStation("Royal Oak","940GZZLURYO",51.5191,-0.187989)
        addTFLStation("Royal Victoria","940GZZDLRVC",51.5092,0.0179298)
        addTFLStation("Ruislip Gardens","940GZZLURSG",51.5607,-0.410595)
        addTFLStation("Ruislip Manor","940GZZLURSM",51.5733,-0.412388)
        addTFLStation("Ruislip","940GZZLURSP",51.5714,-0.421628)
        addTFLStation("Russell Square","940GZZLURSQ",51.5231,-0.124203)
        addTFLStation("Sandilands","940GZZCRSAN",51.3749,-0.077727)
        addTFLStation("Seven Kings","910GSVNKNGS",51.5639,0.0971011)
        addTFLStation("Seven Sisters","940GZZLUSVS",51.5824,-0.0751139)
        addTFLStation("Shadwell","940GZZDLSHA",51.5117,-0.0568915)
        addTFLStation("Shenfield","910GSHENFLD",51.6311,0.330607)
        addTFLStation("Shepherd's Bush Market","940GZZLUSBM",51.5058,-0.226383)
        addTFLStation("Shepherd's Bush","940GZZLUSBC",51.5046,-0.217128)
        addTFLStation("Shoreditch High Street","910GSHRDHST",51.5233,-0.0760711)
        addTFLStation("Shoreditch","940GZZLUSDH",51.5226,-0.07106)
        addTFLStation("Silver Street","910GSIVRST",51.6149,-0.067204)
        addTFLStation("Sloane Square","940GZZLUSSQ",51.4923,-0.156526)
        addTFLStation("Slough","910GSLOUGH",51.5124,-0.592388)
        addTFLStation("Snaresbrook","940GZZLUSNB",51.5807,0.0215136)
        addTFLStation("South Acton","910GSACTON",51.4996,-0.270097)
        addTFLStation("South Ealing","940GZZLUSEA",51.501,-0.307101)
        addTFLStation("South Hampstead","910GSHMPSTD",51.5413,-0.178824)
        addTFLStation("South Harrow","940GZZLUSHH",51.5647,-0.35221)
        addTFLStation("South Kensington","940GZZLUSKS",51.494,-0.1729)
        addTFLStation("South Kenton","940GZZLUSKT",51.5703,-0.3084)
        addTFLStation("South Quay","940GZZDLSOQ",51.5,-0.0161934)
        addTFLStation("South Ruislip","940GZZLUSRP",51.557,-0.399288)
        addTFLStation("South Tottenham","910GSTOTNHM",51.5805,-0.0716808)
        addTFLStation("South Wimbledon","940GZZLUSWN",51.4153,-0.192138)
        addTFLStation("South Woodford","940GZZLUSWF",51.5919,0.0273908)
        addTFLStation("Southall","910GSTHALL",51.5062,-0.377434)
        addTFLStation("Southbury","910GSBURY",51.6487,-0.0525309)
        addTFLStation("Southfields","940GZZLUSFS",51.4455,-0.206706)
        addTFLStation("Southgate","940GZZLUSGT",51.6323,-0.127811)
        addTFLStation("Southwark","940GZZLUSWK",51.5041,-0.105149)
        addTFLStation("St James Street","910GSTJMSST",51.581,-0.0328098)
        addTFLStation("St. James's Park","940GZZLUSJP",51.4996,-0.133536)
        addTFLStation("St. John's Wood","940GZZLUSJW",51.5348,-0.174052)
        addTFLStation("St. Paul's","940GZZLUSPU",51.5149,-0.097482)
        addTFLStation("Stamford Brook","940GZZLUSFB",51.4949,-0.245835)
        addTFLStation("Stamford Hill","910GSTMFDHL",51.5744,-0.0766814)
        addTFLStation("Stanmore","940GZZLUSTM",51.6194,-0.303016)
        addTFLStation("Star Lane","940GZZDLSTL",51.5206,0.0041749)
        addTFLStation("Stepney Green","940GZZLUSGN",51.5217,-0.0467282)
        addTFLStation("Stockwell","940GZZLUSKW",51.4722,-0.122502)
        addTFLStation("Stoke Newington","910GSTKNWNG",51.5651,-0.0727145)
        addTFLStation("Stonebridge Park","940GZZLUSGP",51.5439,-0.2757)
        addTFLStation("Stratford High Street","940GZZDLSHS",51.5377,-0.0003379)
        addTFLStation("Stratford International","940GZZDLSIT",51.5458,-0.0088611)
        addTFLStation("Stratford","940GZZLUSTD",51.5415,-0.0038845)
        addTFLStation("Sudbury Hill","940GZZLUSUH",51.557,-0.336682)
        addTFLStation("Sudbury Town","940GZZLUSUT",51.5508,-0.315803)
        addTFLStation("Surrey Quays","910GSURREYQ",51.4933,-0.0476225)
        addTFLStation("Swiss Cottage","940GZZLUSWC",51.5433,-0.174663)
        addTFLStation("Sydenham","910GSYDENHM",51.4273,-0.0543592)
        addTFLStation("Taplow","910GTAPLOW",51.5235,-0.681683)
        addTFLStation("Temple","940GZZLUTMP",51.5111,-0.11375)
        addTFLStation("Theobalds Grove","910GTHBLDSG",51.692,-0.0356518)
        addTFLStation("Therapia Lane","940GZZCRTPA",51.3857,-0.129092)
        addTFLStation("Theydon Bois","940GZZLUTHB",51.6717,0.10321)
        addTFLStation("Tooting Bec","940GZZLUTBC",51.4356,-0.159651)
        addTFLStation("Tooting Broadway","940GZZLUTBY",51.4277,-0.168185)
        addTFLStation("Tottenham Court Road","940GZZLUTCR",51.5165,-0.130524)
        addTFLStation("Tottenham Hale","940GZZLUTMH",51.5884,-0.0600881)
        addTFLStation("Totteridge & Whetstone","940GZZLUTAW",51.6304,-0.179153)
        addTFLStation("Tower Gateway","940GZZDLTWG",51.5106,-0.0744137)
        addTFLStation("Tower Hill","940GZZLUTWH",51.5098,-0.0777502)
        addTFLStation("Tufnell Park","940GZZLUTFP",51.5568,-0.138433)
        addTFLStation("Turkey Street","910GTURKYST",51.6727,-0.0472263)
        addTFLStation("Turnham Green","940GZZLUTNG",51.4951,-0.25453)
        addTFLStation("Turnpike Lane","940GZZLUTPN",51.5903,-0.102847)
        addTFLStation("Twyford","910GTWYFORD",51.4751,-0.863739)
        addTFLStation("Upminster Bridge","940GZZLUUPB",51.5583,0.234694)
        addTFLStation("Upminster","940GZZLUUPM",51.559,0.253024)
        addTFLStation("Upney","940GZZLUUPY",51.5383,0.0997277)
        addTFLStation("Upper Holloway","910GUPRHLWY",51.5636,-0.129485)
        addTFLStation("Upton Park","940GZZLUUPK",51.5352,0.0344141)
        addTFLStation("Uxbridge","940GZZLUUXB",51.5464,-0.478411)
        addTFLStation("Vauxhall","940GZZLUVXL",51.485,-0.12308)
        addTFLStation("Victoria","940GZZLUVIC",51.4964,-0.143229)
        addTFLStation("Waddon Marsh","940GZZCRWAD",51.377,-0.117827)
        addTFLStation("Walthamstow Central","940GZZLUWWL",51.5829,-0.0200101)
        addTFLStation("Walthamstow Queens Road","910GWLTHQRD",51.5815,-0.0236961)
        addTFLStation("Wandle Park","940GZZCRWAN",51.3735,-0.113362)
        addTFLStation("Wandsworth Road","910GWNDSWRD",51.4697,-0.138179)
        addTFLStation("Wanstead Park","910GWNSTDPK",51.5517,0.0263338)
        addTFLStation("Wanstead","940GZZLUWSD",51.5757,0.028621)
        addTFLStation("Wapping","910GWAPPING",51.5043,-0.0558997)
        addTFLStation("Warren Street","940GZZLUWRR",51.5247,-0.1381)
        addTFLStation("Warwick Avenue","940GZZLUWKA",51.5233,-0.18401)
        addTFLStation("Waterloo","940GZZLUWLO",51.5031,-0.114357)
        addTFLStation("Watford High Street","910GWATFDHS",51.6523,-0.392159)
        addTFLStation("Watford Junction","910GWATFDJ",51.6635,-0.395966)
        addTFLStation("Watford Vicarage Road","940GZZLU961",51.6468,-0.408661)
        addTFLStation("Watford","940GZZLUWAF",51.6574,-0.417373)
        addTFLStation("Wellesley Road","940GZZCRWEL",51.3756,-0.0976166)
        addTFLStation("Wembley Central","940GZZLUWYC",51.5514,-0.295883)
        addTFLStation("Wembley Park","940GZZLUWYP",51.5638,-0.28004)
        addTFLStation("West Acton","940GZZLUWTA",51.5182,-0.280605)
        addTFLStation("West Brompton","940GZZLUWBN",51.487,-0.195488)
        addTFLStation("West Croydon","910GWCROYDN",51.3784,-0.102299)
        addTFLStation("West Drayton","910GWDRYTON",51.5098,-0.471897)
        addTFLStation("West Ealing","910GWEALING",51.5135,-0.320473)
        addTFLStation("West Finchley","940GZZLUWFN",51.6095,-0.188301)
        addTFLStation("West Ham","940GZZLUWHM",51.5283,0.00439908)
        addTFLStation("West Hampstead","940GZZLUWHP",51.5469,-0.1908)
        addTFLStation("West Harrow","940GZZLUWHW",51.5794,-0.353765)
        addTFLStation("West India Quay","940GZZDLWIQ",51.5069,-0.0204041)
        addTFLStation("West Kensington","940GZZLUWKN",51.4907,-0.20598)
        addTFLStation("West Ruislip","940GZZLUWRP",51.5696,-0.437662)
        addTFLStation("West Silvertown","940GZZDLWSV",51.5029,0.0221253)
        addTFLStation("Westbourne Park","940GZZLUWSP",51.521,-0.201509)
        addTFLStation("Westferry","940GZZDLWFE",51.5095,-0.0267357)
        addTFLStation("Westminster","940GZZLUWSM",51.5009,-0.125956)
        addTFLStation("White City","940GZZLUWCY",51.5121,-0.224386)
        addTFLStation("White Hart Lane","910GWHHRTLA",51.6045,-0.0709492)
        addTFLStation("Whitechapel","940GZZLUWPL",51.5198,-0.0601544)
        addTFLStation("Willesden Green","940GZZLUWIG",51.5493,-0.221855)
        addTFLStation("Willesden Junction","940GZZLUWJN",51.5322,-0.24318)
        addTFLStation("Wimbledon Park","940GZZLUWIP",51.4345,-0.199632)
        addTFLStation("Wimbledon","940GZZLUWIM",51.4216,-0.206164)
        addTFLStation("Wood Green","940GZZLUWOG",51.5974,-0.109755)
        addTFLStation("Wood Lane","940GZZLUWLA",51.5101,-0.223712)
        addTFLStation("Wood Street","910GWDST",51.5866,-0.0023458)
        addTFLStation("Woodford","940GZZLUWOF",51.6067,0.0338595)
        addTFLStation("Woodgrange Park","910GWDGRNPK",51.5492,0.044549)
        addTFLStation("Woodside Park","940GZZLUWOP",51.6178,-0.18562)
        addTFLStation("Woodside","940GZZCRWOD",51.3869,-0.0656603)
        addTFLStation("Woolwich Arsenal","940GZZDLWLA",51.4903,0.0693846)
        addTFLStation("Woolwich","910G971",51.4918,0.07081)

        
        
        
    }
    
    func addTFLStation(_ name: String,_ idString: String, _ lat: Double,_ lng: Double){
        
        let tFLStation = TFLStation.init(name: name, idString: idString, lat: lat, lng: lng)
        arrayStations.append(tFLStation)

        
        let imageStation = MapImageUtils.textToImage(name,
                                                     iconColor: AppearanceManager.appColorTintRed,
                                                     fillColor: AppearanceManager.appColorLight0)
        
        self.dictionaryStationImages[idString] = imageStation
        
    }
    
    
    //--------------------------------------------------------------
    // MARK: - LOAD JSON
    // MARK: -
    //--------------------------------------------------------------
    //if user hit map before ranklist then we need to start the load
    var isLoading = false
    var isLoaded = false
    
    
    func loadTFLStationsAndLines(loadStationsFromJSON: Bool){
        self.isLoading = true
        
        if self.isLoaded{
            logger.error("error: USING CACHED STATIONS IN MEMORY -  just call delegate")
            
            callDelegate_tflGeoJSONManagerLinesAndStationsLoaded()
            
        }else{
            //----------------------------------------------------------------------------------------
            //----------------------------------------------------------------------------------------
            
            if loadStationsFromJSON{
            
                self.loadGeoJSONPoint("tfl_stations",
                                      success:{ (geoJSONPointFeatureCollection: GeoJSONPointFeatureCollection)->Void in
                                        //----------------------------------------------------------------------------------------
                                        logger.debug("geoJSONPointFeatureCollection.features?.count returned:\(String(describing: geoJSONPointFeatureCollection.features?.count))")
                                        //----------------------------------------------------------------------------------------
                                        self.tflStations_geoJSONPointFeatureCollection = geoJSONPointFeatureCollection
                                        
                                        
                                        DispatchQueue.main.async {
                                            () -> Void in
                                            
                                            self.stationsLoaded = true
                                            self.testIfAllWebservicesReturned()
                                        }
                                        
                },
                                      failure:{ (error) -> Void in
                                        logger.error("error:\(String(describing: error))")
                                        //failure(error)
                }
                )
            }else{
                logger.debug("loadStationsFromJSON is OFF")
                
                self.stationsLoaded = true
            }
        
            
            //----------------------------------------------------------------------------------------
            //PARSE TUBE LINES FROM JSON
            //----------------------------------------------------------------------------------------
            //print(">>>0. LOAD LINES JSON START:\(Date())")
            self.loadGeoJSONLineString("tfl_lines_pretty",
                                       success:{ (geoJSONLineStringFeatureCollection: GeoJSONLineStringFeatureCollection)->Void in
                                        
                                        //print(">>>1. LOAD LINES JSON END0:\(Date())")

                                        //----------------------------------------------------------------------------------------
                                        //NOISY logger.debug("geoJSONLineStringFeatureCollection.features?.count returned:\(String(describing: geoJSONLineStringFeatureCollection.features?.count))")
                                        //----------------------------------------------------------------------------------------
                                        self.tflStations_geoJSONLineStringFeatureCollection = geoJSONLineStringFeatureCollection
                                        
                                        if let _ = self.tflStations_geoJSONLineStringFeatureCollection {
                                            
                                            //print(">>>2. LOAD LINES JSON BUILD NETWORK START:\(Date())")
                                            self.tflStations_geoJSONLineStringFeatureCollection?.buildTFLNetwork()
                                        }else{
                                            logger.error("self.tflStations_geoJSONLineStringFeatureCollection is nil")
                                        }
                                        
                                        
                                        //----------------------------------------------------------------------------------------
                                        //Call main thread - update ui
                                        //----------------------------------------------------------------------------------------
                                        //print(">>>3. LOAD LINES JSON BUILD NETWORK END:\(Date())")
                                        DispatchQueue.main.async {
                                            () -> Void in
                                            
                                            self.linesLoaded = true
                                            self.testIfAllWebservicesReturned()
                                        }
                                        
                                        
                                        
                                        //----------------------------------------------------------------------------------------
                },
                                       failure:{ (error) -> Void in
                                        //NO WS ERROR but may be status errors see success:
                                        logger.error("error:\(String(describing: error))")
                                        //failure(error)
                }
            )
            //----------------------------------------------------------------------------------------
        }

    }
    
    func testIfAllWebservicesReturned(){
        
        if self.stationsLoaded && self.linesLoaded{
            
            //preloaded in background after Rank Table view filled then this is called when map vc is loaded
            //dotn set inside if..delegate the delegate may be nil
            self.isLoaded = true
            
            callDelegate_tflGeoJSONManagerLinesAndStationsLoaded()
            
        }else{
            logger.error("testIfAllWebservicesReturned self.stationsLoaded:\(self.stationsLoaded) && self.linesLoaded:\(self.linesLoaded)- waiting")
        }
    }
    
    func callDelegate_tflGeoJSONManagerLinesAndStationsLoaded(){
        //----------------------------------------------------------------------------------------
        //both loaded inform delegate
        //----------------------------------------------------------------------------------------
        if let delegate = self.delegate{
            
            //------------------------------------------------
            //errorIN may be nil
            
            delegate.tflGeoJSONManagerLinesAndStationsLoaded(tflGeoJSONManager: self)
            //------------------------------------------------
            
        }else{
            logger.error("delegate is nil")
        }
    }
}


//--------------------------------------------------------------
// MARK: - GeoJSONManager
// MARK: -
//--------------------------------------------------------------

//Trying to mimic the load from WS
class LoadJsonRequest : ParentSwiftObject{
    
    //should be set by subclass
    var filename: String?
    var filenameExt: String = "json"
    
    
    override init(){
        super.init()
    }
    
    convenience init(filename:String){
        
        self.init()
        self.filename = filename
    }
}

class GeoJSONManager{
    
    init() {
    }
    
    

    //"tfl_stations".json
    func loadGeoJSONPoint(_ filename: String,
                          success: @escaping (_ geoJSONPointFeatureCollection: GeoJSONPointFeatureCollection) -> Void,
                          failure: @escaping (_ error: Error?) -> Void
        )
    {
        
        //---------------------------------------------------------------------
        let loadJsonRequest = LoadJsonRequest.init(filename: filename)
        //---------------------------------------------------------------------

        if let _ = loadJsonRequest.filename{
            //---------------------------------------------------------------------
            //NOISY logger.debug("urlString:\r\(urlString)")
            //---------------------------------------------------------------------
            self.loadJSON(loadJsonRequest,
                            success:{ (parsedObject: Any)->Void in
                                
                                if let geoJSONPointFeatureCollection : GeoJSONPointFeatureCollection = Mapper<GeoJSONPointFeatureCollection>().map(JSONObject:parsedObject)
                                {
                                    
                                    //--------------------------------------------------------------------
                                    logger.debug("geoJSONPointFeatureCollection.features?.count returned:\(String(describing: geoJSONPointFeatureCollection.features?.count))")
                                    //---------------------------------------------------------------------
                                    //RESPONSE OK - on backgrounf th
                                    //---------------------------------------------------------------------
                                    success(geoJSONPointFeatureCollection)
                                    
                        
                                    
                                }else{
                                    logger.error("parsedObject is nil or not [TFLTaxiRank]")
                                }
                        },
                        failure:{
                            (error) -> Void in
                            //NO WS ERROR but may be status errors see success:
                            logger.error("error:\(error)")
                            failure(error)
                        }
            )
            //---------------------------------------------------------------------
        }else{
            logger.error("loadJsonRequest.filename is nil - 16787876")
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - GeoJSON - LineString Collection
    // MARK: -
    //--------------------------------------------------------------

    //"tfl_stations".json
    func loadGeoJSONLineString(_ filename: String,
                               success: @escaping (_ geoJSONLineStringFeatureCollection: GeoJSONLineStringFeatureCollection) -> Void,
                               failure: @escaping (_ error: Error?) -> Void
        )
    {
        
        
        //---------------------------------------------------------------------
        let loadJsonRequest = LoadJsonRequest.init(filename: filename)
        //---------------------------------------------------------------------
        
        if let _ = loadJsonRequest.filename{
                //---------------------------------------------------------------------
                //NOISY logger.debug("urlString:\r\(urlString)")
                //---------------------------------------------------------------------
                self.loadJSON(loadJsonRequest,
                              success:{ (parsedObject: Any)->Void in
                                
                                if let geoJSONLineStringFeatureCollection : GeoJSONLineStringFeatureCollection = Mapper<GeoJSONLineStringFeatureCollection>().map(JSONObject:parsedObject)
                                {
                                    
                                    //--------------------------------------------------------------------
                                    logger.debug("geoJSONLineStringFeatureCollection.features?.count returned:\(String(describing: geoJSONLineStringFeatureCollection.features?.count))")
                                    //---------------------------------------------------------------------
                                    //RESPONSE OK - NOTE STILL ON BACKGROUND THREAD
                                    //---------------------------------------------------------------------
                                    
                                    success(geoJSONLineStringFeatureCollection)
                                
                                    
                                }else{
                                    logger.error("parsedObject is nil or not [TFLTaxiRank]")
                                }
                },
                              failure:{
                                (error) -> Void in
                                //NO WS ERROR but may be status errors see success:
                                logger.error("error:\(error)")
                                failure(error)
                }
                )
                //---------------------------------------------------------------------
           
        }else{
            logger.error("loadJsonRequest.filename is nil - 16787876")
        }
    }
    
    //see callWS
    func loadJSON(_ loadJsonRequest: LoadJsonRequest,
                success: @escaping (_ parsedObject: Any) -> Void,
                failure: @escaping (_ error: Error) -> Void
        )
    {
        //"tfl_stations"."json"
        if let filename = loadJsonRequest.filename{

            
            DispatchQueue.global(qos: .background).async {
                //----------------------------------------------------------------------------------------
                //BACKGROUND THREAD
                //----------------------------------------------------------------------------------------
                if let filepath = Bundle.main.path(forResource: filename, ofType: loadJsonRequest.filenameExt) {
                    do {
                        
                        //----------------------------------------------------------------------------------------
                        let contents = try String(contentsOfFile: filepath)
                        //if nil make sure youre not loading this from appD global logger.debug may not exist yet
                        //logger.debug("json loaded:\(contents.length)")
                        
                        //----------------------------------------------------------------------------------------
                        if let dataJSON = contents.data(using: String.Encoding.utf8){
                            //----------------------------------------------------------------------------------------
                            //----------------------------------------------------------------------------------------
                            do {
                                //SWIFT3
                                let parsedObject: Any = try JSONSerialization.jsonObject(with: dataJSON, options: JSONSerialization.ReadingOptions.allowFragments)
                                
                                success(parsedObject)
                                
                                
                            } catch let error {
                                logger.error("JSONSerialization:\(error)")
                                failure(error)
                            }
                            //----------------------------------------------------------------------------------------
                        }else{
                            
                        }
                        //----------------------------------------------------------------------------------------
                        
                    } catch let error {
                        logger.error("contentsOfFile:\(error)")
                        failure(error)
                    }
                } else {
                    logger.error("file path not found")
                }
                
                //----------------------------------------------------------------------------------------
                //
                //----------------------------------------------------------------------------------------
            }
            
            
        }else{
            logger.error("loadJsonRequest.filename is nil - 16787876")
        }
    }

}
