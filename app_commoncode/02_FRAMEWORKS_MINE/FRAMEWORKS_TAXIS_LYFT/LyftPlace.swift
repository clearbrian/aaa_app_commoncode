//
//  LyftPlace.swift
//  joyride
//
//  Created by Brian Clear on 16/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation

//lyftapp://confirm?pickupCoordinate=51.511807,-0.117389&pickupAddress=Somerset%20House,%20Strand&destinationCoordinate=51.514996,-0.098970&destinationAddress=London%20Stock%20Exchange&pickupLocality=London&destinationLocality=London&referrer=your_lyft_application_token"];

//    lyftapp://confirm?
//pickupCoordinate=51.511807,-0.117389

//    &pickupAddress=Somerset%20House,%20Strand
//    &destinationCoordinate=51.514996,-0.098970
//    &destinationAddress=London%20Stock%20Exchange
//LOCALITY
//    &pickupLocality=London
//    &destinationLocality=London

//    &referrer=your_lyft_application_token

/*
lyft://ridetype?id=lyft&pickup[latitude]=37.764728&pickup[longitude]=-122.422999&destination[latitude]=37.7763592&destination[longitude]=-122.4242038

lyft://ridetype?id=lyft
&pickup[latitude]=37.764728&pickup[longitude]=-122.422999
&destination[latitude]=37.7763592&destination[longitude]=-122.4242038
 
 
*/

enum LyftPlaceType : String {
    case Pickup = "pickup" //used in deeplink url
    case Destination = "destination" //used in deeplink url
}

class LyftPlace : ParentSwiftObject{
    
    var lat : Double?
    var lng : Double?
    
    var coordinateString : String?{
        set
        {
            
        }
        get {
            var coordinateString_ : String?
            if let lat = lat{
                if let lng = lng{
                    if let lyftPlaceType = self.lyftPlaceType {
                        //------------------------------------------------
                        //&pickup[latitude]=37.764728&pickup[longitude]=-122.422999
                        coordinateString_ = "\(lyftPlaceType.rawValue)[latitude]=\(lat)&\(lyftPlaceType.rawValue)[longitude]=\(lng)"
                        //------------------------------------------------
                    }else{
                        logger.error("self.lyftPlaceType is nil")
                    }
              
                }else{
                    logger.error("lng is nil")
                }
            }else{
                logger.error("lat is nil")
            }
            
            return coordinateString_
        }
    }
    
    //NOT USED by Lyft deeplinking APi only takes lat/lng
    //name + formatted_address for CLKPlaceResponse
    var address : String?
    
    //City
    var locality : String?
    
    var lyftPlaceType : LyftPlaceType?
    
    
    var paramsForLyftPlace:String{
        
        var urlString = ""
        //---------------------------------------------------------------------
        //pickupCoordinate=51.511807,-0.117389
        //destinationCoordinate=51.514996,-0.098970
        //---------------------------------------------------------------------
        if let coordinateString = self.coordinateString{
            
            urlString = urlString + "&\(coordinateString)"
            
        }else{
            logger.error("lyftPlace.coordinateString is nil")
        }
        
        return urlString
    }
    
    
    
    
    override init(){
        
    }
    
    
    init(lyftPlaceType : LyftPlaceType){
        self.lyftPlaceType = lyftPlaceType
    }
    
    
    /*
     lyft://ridetype?id=lyft
     &pickup[latitude]=37.764728
     &pickup[longitude]=-122.422999
     &destination[latitude]=37.7763592
     &destination[longitude]=-122.4242038
     */
    static func createLyftPlace(lat:Double, lng:Double, lyftPlaceType: LyftPlaceType) -> LyftPlace
    {
        let lyftPlace = LyftPlace()
        //---------------------------------------------------------------------
        lyftPlace.lat = lat
        lyftPlace.lng = lng
        //---------------------------------------------------------------------
        lyftPlace.lyftPlaceType = lyftPlaceType
        //---------------------------------------------------------------------
        return lyftPlace
    }
    
    static func convert_CLKPlace_To_LyftPlace(_ clkPlace: CLKPlace, lyftPlaceType: LyftPlaceType) -> LyftPlace?{
        
        var lyftPlaceTypeReturned: LyftPlace?
        if let clLocation = clkPlace.clLocation{
            
            let lat = clLocation.coordinate.latitude
            let lng = clLocation.coordinate.longitude
            
            //-----------------------------------------------------------------------------------
            lyftPlaceTypeReturned = LyftPlace.createLyftPlace(
                lat: lat,
                lng: lng,
                lyftPlaceType: lyftPlaceType
            )
            //-----------------------------------------------------------------------------------
        }else{
            print("ERROR: clkPlace.clLocation is nil 46364")
        }
        return lyftPlaceTypeReturned
    }
    
    
}
