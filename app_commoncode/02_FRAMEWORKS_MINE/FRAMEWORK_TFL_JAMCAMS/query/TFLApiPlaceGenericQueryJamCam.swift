//
//  TFLApiPlaceCOLCQueryCollectionJamCam.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 27/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

class TFLApiPlaceCOLCQueryCollectionJamCam : TFLApiPlaceCOLCQueryCollection{
    
    override init() {
        super.init()
        
    }
    override func build_colcQueryDictionary() -> [String: COLCQuery]{
       
        //Nearest/Name in parent
        var colcQueryDictionary_ : [String: COLCQuery] = super.build_colcQueryDictionary()
        
        
        
        //----------------------------------------------------------------------------------------
        //isAvailable = true
        //----------------------------------------------------------------------------------------
        colcQueryDictionary_[TFLApiPlaceJamCam.colcQuery_isAvailable().title] = TFLApiPlaceJamCam.colcQuery_isAvailable()
        colcQueryDictionaryKeys.append(TFLApiPlaceJamCam.colcQuery_isAvailable().title)
        //----------------------------------------------------------------------------------------
        return colcQueryDictionary_
        
    }
    
}
