//
//  TFLApiPlaceJamCamAdditionalPropertyKey.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 08/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation


//"key": "available",
//"key": "imageUrl",
//"key": "videoUrl",
//"key": "view",

//available
//imageUrl
//videoUrl
//view

enum TFLApiPlaceJamCam_additionalProperty_key: String{
    
    case unknown = "UNKNOWN KEY"
    case available = "available"
    case imageUrl = "imageUrl"
    case videoUrl = "videoUrl"
    case view = "view"
    
    func displayString() -> String{
        switch self{
        case .unknown: return "Unknown"
        case .available: return "available"
        case .imageUrl: return "imageUrl"
        case .videoUrl: return "videoUrl"
        case .view: return "view"
        }
    }
    
    //"RankType" >> TFLApiPlaceJamCamAdditionalPropertyKey.RankType
    static func keyForKeyString(keyString: String?) -> TFLApiPlaceJamCam_additionalProperty_key{
        
        if let keyString = keyString {
            switch keyString{
                
            case "available":  return .available
            case "imageUrl":  return .imageUrl
            case "videoUrl":  return .videoUrl
            case "view":  return .view
                
            default:
                return .unknown
            }
        }else{
            logger.error("keyString is nil")
            return .unknown
        }
    }
}
