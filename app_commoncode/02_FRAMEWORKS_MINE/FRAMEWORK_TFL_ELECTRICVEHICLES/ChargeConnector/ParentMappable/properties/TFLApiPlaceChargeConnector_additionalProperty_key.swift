//
//  TFLApiPlaceChargeConnectorAdditionalPropertyKey.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 08/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

//    "key": "Name",
//    "key": "Reference",
//    "key": "Location",
//    "key": "Region",
//    "key": "Operator",
//    "key": "Restrictions",
//    "key": "PricingUrl",
//    "key": "PaymentMethods",
//    "key": "AuthenticationMethods",
//    "key": "ElectricalCharacteristics",
//    "key": "IsReservable",
//    "key": "ConnectorCount",
//    "key": "LastUpdated",
//    "key": "Url",

//    authenticationMethods
//    connectorCount
//    electricalCharacteristics
//    isReservable
//    lastUpdated
//    location
//    name

enum TFLApiPlaceChargeConnector_additionalProperty_key: String{
    
    case unknown = "UNKNOWN KEY"
    
    case authenticationMethods = "AuthenticationMethods"
    case connectorCount = "ConnectorCount"
    case electricalCharacteristics = "ElectricalCharacteristics"
    case isReservable = "IsReservable"
    case lastUpdated = "LastUpdated"
    case location = "Location"
    case name = "Name"
    //operator is reserved word
    //case operator = "Operator"
    case operatorChargeConnector = "Operator"
    case paymentMethods = "PaymentMethods"
    case pricingUrl = "PricingUrl"
    case reference = "Reference"
    case region = "Region"
    case restrictions = "Restrictions"
    case url = "Url"
    
    func displayString() -> String{
        switch self{
        case .unknown: return "Unknown"
        //---------------------------------------------------------
        case .authenticationMethods: return "AuthenticationMethods"
        case .connectorCount: return "ConnectorCount"
        case .electricalCharacteristics: return "ElectricalCharacteristics"
        case .isReservable: return "IsReservable"
        case .lastUpdated: return "LastUpdated"
        case .location: return "Location"
        case .name: return "Name"
        case .operatorChargeConnector: return "Operator"
        case .paymentMethods: return "PaymentMethods"
        case .pricingUrl: return "PricingUrl"
        case .reference: return "Reference"
        case .region: return "Region"
        case .restrictions: return "Restrictions"
        case .url: return "Url"
        //---------------------------------------------------------
        }
    }
    
    //"RankType" >> TFLApiPlaceChargeConnectorAdditionalPropertyKey.RankType
    static func keyForKeyString(keyString: String?) -> TFLApiPlaceChargeConnector_additionalProperty_key{
        
        if let keyString = keyString {
            switch keyString{
            //---------------------------------------------------------
            case "AuthenticationMethods": return .authenticationMethods
            case "ConnectorCount": return .connectorCount
            case "ElectricalCharacteristics": return.electricalCharacteristics
            case "IsReservable": return .isReservable
            case "LastUpdated": return .lastUpdated
            case "Location": return .location
            case "Name": return .name
            case "Operator": return .operatorChargeConnector
            case "PaymentMethods": return .paymentMethods
            case "PricingUrl": return .pricingUrl
            case "Reference": return .reference
            case "Region": return .region
            case "Restrictions": return .restrictions
            case "Url": return .url
            //---------------------------------------------------------
            default:
                return .unknown
            }
        }else{
            logger.error("keyString is nil")
            return .unknown
        }
    }
}
