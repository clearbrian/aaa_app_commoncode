//
//  TFLApiPlaceChargeConnectorVersion.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 11/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

//additionalProperties:
//    "category": "Description",

//    "key": "Name",
//    "key": "Reference",
//    "key": "Location",
//    "key": "Region",
//    "key": "Operator",
//    "key": "Restrictions",
//    "key": "PricingUrl",
//    "key": "PaymentMethods",
//    "key": "AuthenticationMethods",
//    "key": "ElectricalCharacteristics",
//    "key": "IsReservable",
//    "key": "ConnectorCount",
//    "key": "LastUpdated",
//    "key": "Url",

//    "sourceSystemKey": "EsbChargePoint",
//    "sourceSystemKey": "ChargeMasterChargePoint",
//    "sourceSystemKey": "BritishGasChargePoint",

class TFLApiPlaceChargeConnector: TFLApiPlace{
    
    //--------------------------------------------------------------
    // MARK: - init - TO ADD MISSING ONES NOT IN JSON
    // MARK: -
    //--------------------------------------------------------------
    
    init(id : String,
         commonName : String,
//         lat : Float,
//         lon : Float
        lat : Double,
        lon : Double
        )
    {
        //----------------------------------------------------------------------------------------
        super.init(id: id,
                   commonName: commonName,
                   lat: lat,
                   lon: lon,
                   placeType: "ChargeConnector",
                   url: nil)
        
        // TODO: - CLEANUP after test
        //----------------------------------------------------------------------------------------
        //self.id = id
        //self.type = "Tfl.Api.Presentation.Entities.Place, Tfl.Api.Presentation.Entities"
        //self.additionalProperties =  [TFLApiPlaceAdditionalProperty]()
        //self.children = [AnyObject]()
        //self.childrenUrls = [AnyObject]()
        //
        //self.commonName = commonName
        //self.lat = lat
        //self.lon = lon
        //self.placeType = "ChargeConnector"
        //self.url = nil
        //----------------------------------------------------------------------------------------
        
        //req else not put into dict properly
        //        self.tflApiPlaceChargeConnectorVersionInfo = TFLApiPlaceChargeConnectorVersionInfo.init(rankIdJSON: id,
        //                                                                                  rankIdUnversioned: id,
        //                                                                                  version: 0,
        //                                                                                  hasVersions: false)
        //----------------------------------------------------------------------------------------
    }
    
    required init?(map: Map){
        super.init(map: map)
        
    }
    
    //--------------------------------------------------------------
    // MARK: - MISSING RANKS
    // MARK: -
    //--------------------------------------------------------------
    class func  createChargeConnector(id : String,
                                  commonName : String,
//                                  lat : Float,
//                                  lon : Float,
                                  lat : Double,
                                  lon : Double,
                                  Borough : String) -> TFLApiPlaceChargeConnector
        
    {
        //----------------------------------------------------------------------------------------
        let tflApiPlaceChargeConnector = TFLApiPlaceChargeConnector(id: id,
                                                      commonName: commonName,
                                                      lat: lat,
                                                      lon: lon)
        
        tflApiPlaceChargeConnector.appendAdditionalProperty(TFLApiPlaceAdditionalProperty(category: "Address", key: "Borough", sourceSystemKey: "3207", value: Borough))
        
        tflApiPlaceChargeConnector.appendAdditionalProperty(TFLApiPlaceAdditionalProperty(category: "Description", key: "NumberOfSpaces", sourceSystemKey: "3207", value: ""))
        tflApiPlaceChargeConnector.appendAdditionalProperty(TFLApiPlaceAdditionalProperty(category: "Description", key: "OperationDays", sourceSystemKey: "3207", value: ""))
        tflApiPlaceChargeConnector.appendAdditionalProperty(TFLApiPlaceAdditionalProperty(category: "Description", key: "OperationTimes", sourceSystemKey: "3207", value: ""))
        
        tflApiPlaceChargeConnector.appendAdditionalProperty(TFLApiPlaceAdditionalProperty(category: "Description", key: "RankType", sourceSystemKey: "3207", value: "Working"))
        //----------------------------------------------------------------------------------------
        return tflApiPlaceChargeConnector
        
    }
    
    
    func appendAdditionalProperty(_ tflApiPlaceAdditionalProperty : TFLApiPlaceAdditionalProperty){
        ///let tflApiPlaceAdditionalPropertyBorough = TFLApiPlaceAdditionalProperty(category: "Address", key: "Borough", sourceSystemKey: "3207", value: "Chelsea")
        
        if let _ =  self.additionalProperties {
            self.additionalProperties?.append(tflApiPlaceAdditionalProperty)
        }else{
            logger.error("self.additionalProperties is nil")
        }
    }
    
    
    
    //--------------------------------------------------------------
    // MARK: - VERSIONS
    // MARK: -
    //--------------------------------------------------------------
    //unversioned
    //[id:ChargeConnector_5633] ...
    //versioned
    //[id:ChargeConnector_5642-1] Bethnal Green Road (Tesco), Bethnal Green [51.5269, -0.061084] [RankType:Working]
    //[id:ChargeConnector_5642-2] Bethnal Green Road (Tesco), Bethnal Green [51.5269, -0.061084] [RankType:Working]
    //--------------------------------------------------------------
    //var tflApiPlaceChargeConnectorVersionInfo : TFLApiPlaceChargeConnectorVersionInfo?
    //--------------------------------------------------------------
   // TODO: - remove from all except TaxiRank
    var isVersioned : Bool{
//        var isVersioned_ = false
        
//        if let idJSON = self.id {
//            if idJSON.contains("-"){
//                isVersioned_ = true
//            }else{
//                
//            }
//        }else{
//            
//        }
        return false
    }
    
    //--------------------------------------------------------------
    // MARK: - id
    // MARK: -
    //--------------------------------------------------------------
    
    override var id : String?{
        didSet {
            //to remove warnig
            
        }
    }

    //--------------------------------------------------------------
    // MARK: - ADDITIONAL PROPERTIES
    //--------------------------------------------------------------
    
    var authenticationMethodsString: String?{
        if let stringValue_ = self.findStringProperty_ChargeConnector(.authenticationMethods) {
            return stringValue_
        }else{
            logger.error("self.findStringProperty_ChargeConnector(.authenticationMethods) is nil")
            return nil
        }
    }
    var connectorCountString: String?{
        if let stringValue_ = self.findStringProperty_ChargeConnector(.connectorCount) {
            return stringValue_
        }else{
            logger.error("self.findStringProperty_ChargeConnector(.connectorCount) is nil")
            return nil
        }
    }
    var electricalCharacteristicsString: String?{
        if let stringValue_ = self.findStringProperty_ChargeConnector(.electricalCharacteristics) {
            return stringValue_
        }else{
            logger.error("self.findStringProperty_ChargeConnector(.electricalCharacteristics) is nil")
            return nil
        }
    }
    var isReservableString: String?{
        if let stringValue_ = self.findStringProperty_ChargeConnector(.isReservable) {
            return stringValue_
        }else{
            logger.error("self.findStringProperty_ChargeConnector(.isReservable) is nil")
            return nil
        }
    }
    var lastUpdatedString: String?{
        if let stringValue_ = self.findStringProperty_ChargeConnector(.lastUpdated) {
            return stringValue_
        }else{
            logger.error("self.findStringProperty_ChargeConnector(.lastUpdated) is nil")
            return nil
        }
    }
    var locationString: String?{
        if let stringValue_ = self.findStringProperty_ChargeConnector(.location) {
            return stringValue_
        }else{
            logger.error("self.findStringProperty_ChargeConnector(.location) is nil")
            return nil
        }
    }
    var nameString: String?{
        if let stringValue_ = self.findStringProperty_ChargeConnector(.name) {
            return stringValue_
        }else{
            logger.error("self.findStringProperty_ChargeConnector(.name) is nil")
            return nil
        }
    }
    var operatorChargeConnectorString: String?{
        if let stringValue_ = self.findStringProperty_ChargeConnector(.operatorChargeConnector) {
            return stringValue_
        }else{
            logger.error("self.findStringProperty_ChargeConnector(.operatorChargeConnector) is nil")
            return nil
        }
    }
    var paymentMethodsString: String?{
        if let stringValue_ = self.findStringProperty_ChargeConnector(.paymentMethods) {
            return stringValue_
        }else{
            logger.error("self.findStringProperty_ChargeConnector(.paymentMethods) is nil")
            return nil
        }
    }
    var referenceString: String?{
        if let stringValue_ = self.findStringProperty_ChargeConnector(.reference) {
            return stringValue_
        }else{
            logger.error("self.findStringProperty_ChargeConnector(.reference) is nil")
            return nil
        }
    }
    var regionString: String?{
        if let stringValue_ = self.findStringProperty_ChargeConnector(.region) {
            return stringValue_
        }else{
            logger.error("self.findStringProperty_ChargeConnector(.region) is nil")
            return nil
        }
    }
    var restrictionsString: String?{
        if let stringValue_ = self.findStringProperty_ChargeConnector(.restrictions) {
            return stringValue_
        }else{
            logger.error("self.findStringProperty_ChargeConnector(.restrictions) is nil")
            return nil
        }
    }
    var urlString: String?{
        if let stringValue_ = self.findStringProperty_ChargeConnector(.url) {
            return stringValue_
        }else{
            logger.error("self.findStringProperty_ChargeConnector(.url) is nil")
            return nil
        }
    }

    //--------------------------------------------------------------
    // MARK: - Search Additional Properties
    // MARK: -
    //--------------------------------------------------------------
    func findStringProperty_ChargeConnector(_ tflApiPlaceChargeConnector_additionalProperty_key: TFLApiPlaceChargeConnector_additionalProperty_key) -> String?{
        
        let stringValue: String? = findStringPropertyForKeyRawValue(tflApiPlaceChargeConnector_additionalProperty_key.rawValue)
        
        return stringValue
    }
    
    //.NumberOfSpaces,"123" >> Int: 123
    func findIntProperty_ChargeConnector(_ tflApiPlaceChargeConnector_additionalProperty_key: TFLApiPlaceChargeConnector_additionalProperty_key) -> Int?{
        let intValue: Int? = findIntPropertyForKeyRawValue(tflApiPlaceChargeConnector_additionalProperty_key.rawValue)
        
        return intValue
    }
    
    
    //--------------------------------------------------------------
    // MARK: - Description
    // MARK: -
    //--------------------------------------------------------------
    override var alertTitle: String{
        return self.descriptionLineMain
    }
    

    //alert message
    override var alertMessage: String{
        var description_ = ""
        
        description_ = description_ +  "authenticationMethodsString:\(String(describing: self.authenticationMethodsString)),"
        description_ = description_ +  "connectorCountString:\(String(describing: self.connectorCountString)),"
        description_ = description_ +  "electricalCharacteristicsString:\(String(describing: self.electricalCharacteristicsString)),"
        description_ = description_ +  "isReservableString:\(String(describing: self.isReservableString)),"
        description_ = description_ +  "lastUpdatedString:\(String(describing: self.lastUpdatedString)),"
        description_ = description_ +  "locationString:\(String(describing: self.locationString)),"
        description_ = description_ +  "nameString:\(String(describing: self.nameString)),"
        description_ = description_ +  "operatorChargeConnectorString:\(String(describing: self.operatorChargeConnectorString)),"
        description_ = description_ +  "paymentMethodsString:\(String(describing: self.paymentMethodsString)),"
        description_ = description_ +  "referenceString:\(String(describing: self.referenceString)),"
        description_ = description_ +  "regionString:\(String(describing: self.regionString)),"
        description_ = description_ +  "restrictionsString:\(String(describing: self.restrictionsString)),"
        description_ = description_ +  "urlString:\(String(describing: self.urlString)),"
    
        return description_
    }
    
    //--------------------------------------------------------------
    // MARK: - Detail
    // MARK: -
    //--------------------------------------------------------------
    override var descriptionLineDetail0 : String {
        return "Viewing: \(Safe.safeString(self.operatorChargeConnectorString))"
    }
    
    override var descriptionLineDetail1 : String {
        return "Connector Count: \(Safe.safeString(self.connectorCountString))"

    }
    
    //--------------------------------------------------------------
    // MARK: - SubDetail
    // MARK: -
    //--------------------------------------------------------------
    override var descriptionLineSubDetail0 : String {
        var description_ = ""
        description_ = description_ +  "\(self.distanceToCurrentLocationFormatted)"
        return description_
    }
    
    override var descriptionLineSubDetail1 : String {
        return "Available: \(Safe.safeString(self.connectorCountString))"
    }
    
    override var descriptionOneLine : String {
        return "[id:\(self.idSafe)] \(Safe.safeString(self.commonName)) [\(self.latStringSafe), \(self.lonStringSafe)] [Viewing:\(Safe.safeString(self.connectorCountString))]"
    }
    
    //RANK,ChargeConnector_5973,Park Plaza - County Hall Hotel, hotel forecourt,ChargeConnector,-0.116399,51.5014,2,Mon - Sun,24 hours,Working,,
    override var descriptionCSV : String {
        
        var description_ = ""
        
        description_ = description_ +  "ChargeConnector"
        
        description_ = description_ +  "authenticationMethodsString:\(String(describing: self.authenticationMethodsString)),"
        description_ = description_ +  "connectorCountString:\(String(describing: self.connectorCountString)),"
        description_ = description_ +  "electricalCharacteristicsString:\(String(describing: self.electricalCharacteristicsString)),"
        description_ = description_ +  "isReservableString:\(String(describing: self.isReservableString)),"
        description_ = description_ +  "lastUpdatedString:\(String(describing: self.lastUpdatedString)),"
        description_ = description_ +  "locationString:\(String(describing: self.locationString)),"
        description_ = description_ +  "nameString:\(String(describing: self.nameString)),"
        description_ = description_ +  "operatorChargeConnectorString:\(String(describing: self.operatorChargeConnectorString)),"
        description_ = description_ +  "paymentMethodsString:\(String(describing: self.paymentMethodsString)),"
        description_ = description_ +  "referenceString:\(String(describing: self.referenceString)),"
        description_ = description_ +  "regionString:\(String(describing: self.regionString)),"
        description_ = description_ +  "restrictionsString:\(String(describing: self.restrictionsString)),"
        description_ = description_ +  "urlString:\(String(describing: self.urlString)),"

        return description_
    }
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - MapPinnable
    //--------------------------------------------------------------
    //used by GoogleMappable gmsMarker > markerImageName> markerImage > UI
    //DO not override MapPinnable > mapImage - change the image name in assets here
    //DO not override GoogleMappable - you can overdide something in an extention
    //overriing markerImageName and changing the final image name based on rules in the subclass e.g ChargeConnector.rankType
    //-------------------------------------------------------------------
    
    override var markerImageName: String?{
        var markerImageNameRet: String? = nil
        
        //----------------------------------------------------------------------------------------
        //PIN NAME - varies by type
        //----------------------------------------------------------------------------------------
        // TODO: - move the filename into the RankType
        //        if let rankType = self.rankType {
        //            var imageName = "taxipin_working"
        //
        //            switch rankType{
        //            case .unknown:
        //                imageName = "taxipin_unknown"
        //
        //
        //            case .working:
        //                imageName = "taxipin_working"
        //
        //            case .rest_rank:
        //                imageName = "taxipin_rest_rank"
        //
        //
        //            case .refreshment_rank:
        //                imageName = "taxipin_refreshment_rank"
        //            }
        //            markerImageNameRet = imageName
        //        }else{
        //            logger.error("self.rankType is nil")
        //        }
        //-------------------------------------------------------------------
        //SCALABLE SVG of taxi
        //-------------------------------------------------------------------
        // TODO: - pin for others
        markerImageNameRet = "map_pin_flat_alpha"
        return markerImageNameRet
    }
    
    
    //--------------------------------------------------------------
    // MARK: - COLCQueryCollection
    // MARK: -
    //--------------------------------------------------------------
    //cant override static -
    //http://stackoverflow.com/questions/29189700/overriding-static-vars-in-subclasses-swift-1-2
    
    override class var colcQueryCollectionForType : COLCQueryCollection{
        return TFLApiPlaceCOLCQueryCollectionChargeConnector()
    }
    
    //--------------------------------------------------------------
    // MARK: - map json Property string to type to query on
    // MARK: -
    //--------------------------------------------------------------

    fileprivate static let propertyName_connectorCount = "ConnectorCount"

    override func value(forPropertyName propertyName: String) -> Any?{
        
        if let valueAny = super.value(forPropertyName: propertyName) {
            return valueAny
        }else{
            
            //not found in parent - may be specific to property
            switch propertyName{
                
            case TFLApiPlaceChargeConnector.propertyName_connectorCount:
                return self.connectorCountString
                
            default:
                logger.error("[TFLApiPlaceChargeConnector] UNHANDLED PROPERTY: value:forPropertyName:'\(propertyName) - ChargeConnector")
                return nil
            }
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - Factory queries - others in parent
    // MARK: -
    //--------------------------------------------------------------
    class func colcQuery_isAvailable() -> COLCQuery{
        
        //----------------------------------------------------------------------------------------
        //e.g. COLCFilterBool - SEARCH for isAvailable = true
        //----------------------------------------------------------------------------------------
        
        
//        
//        let colcQuery = COLCQuery.colcQuery_COLCFilterBool(title        : "Available",
//                                                           propertyName : TFLApiPlaceChargeConnector.propertyName_isAvailable,
//                                                           searchBool   : true,
//                                                      searchPropertyName: TFLApiPlace.propertyName_commonName)
        //----------------------------------------------------------------------------------------
        let colcQuery = TFLApiPlace.COLCQuery_defaultSearchQuery(title:"Connector Count")
        
        let colcFilterBool = COLCFilterBool(propertyName: TFLApiPlaceChargeConnector.propertyName_connectorCount, searchBool: true)
        
        colcQuery.appendCOLCFilter(colcFilterBool)
        //----------------------------------------------------------------------------------------
        return colcQuery
    }
    
}
