//
//  TFLApiPlaceChargeStationVersion.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 11/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation


class TFLApiPlaceChargeStation: TFLApiPlace{
    
    //--------------------------------------------------------------
    // MARK: - init - TO ADD MISSING ONES NOT IN JSON
    // MARK: -
    //--------------------------------------------------------------
    
    init(id : String,
         commonName : String,
//         lat : Float,
//         lon : Float
        lat : Double,
        lon : Double
        )
    {
        //----------------------------------------------------------------------------------------
        super.init(id: id,
                   commonName: commonName,
                   lat: lat,
                   lon: lon,
                   placeType: "ChargeStation",
                   url: nil)
        
        // TODO: - CLEANUP after test
        //----------------------------------------------------------------------------------------
        //self.id = id
        //self.type = "Tfl.Api.Presentation.Entities.Place, Tfl.Api.Presentation.Entities"
        //self.additionalProperties =  [TFLApiPlaceAdditionalProperty]()
        //self.children = [AnyObject]()
        //self.childrenUrls = [AnyObject]()
        //
        //self.commonName = commonName
        //self.lat = lat
        //self.lon = lon
        //self.placeType = "ChargeStation"
        //self.url = nil
        //----------------------------------------------------------------------------------------
        
        //req else not put into dict properly
        //        self.tflApiPlaceChargeStationVersionInfo = TFLApiPlaceChargeStationVersionInfo.init(rankIdJSON: id,
        //                                                                                  rankIdUnversioned: id,
        //                                                                                  version: 0,
        //                                                                                  hasVersions: false)
        //----------------------------------------------------------------------------------------
    }
    
    required init?(map: Map){
        super.init(map: map)
        
    }
    
    //--------------------------------------------------------------
    // MARK: - MISSING RANKS
    // MARK: -
    //--------------------------------------------------------------
    class func  createChargeStation(id : String,
                                  commonName : String,
//                                  lat : Float,
//                                  lon : Float,
                                  lat : Double,
                                  lon : Double,
                                  Borough : String) -> TFLApiPlaceChargeStation
        
    {
        //----------------------------------------------------------------------------------------
        let tflApiPlaceChargeStation = TFLApiPlaceChargeStation(id: id,
                                                      commonName: commonName,
                                                      lat: lat,
                                                      lon: lon)
        
        tflApiPlaceChargeStation.appendAdditionalProperty(TFLApiPlaceAdditionalProperty(category: "Address", key: "Borough", sourceSystemKey: "3207", value: Borough))
        
        tflApiPlaceChargeStation.appendAdditionalProperty(TFLApiPlaceAdditionalProperty(category: "Description", key: "NumberOfSpaces", sourceSystemKey: "3207", value: ""))
        tflApiPlaceChargeStation.appendAdditionalProperty(TFLApiPlaceAdditionalProperty(category: "Description", key: "OperationDays", sourceSystemKey: "3207", value: ""))
        tflApiPlaceChargeStation.appendAdditionalProperty(TFLApiPlaceAdditionalProperty(category: "Description", key: "OperationTimes", sourceSystemKey: "3207", value: ""))
        
        tflApiPlaceChargeStation.appendAdditionalProperty(TFLApiPlaceAdditionalProperty(category: "Description", key: "RankType", sourceSystemKey: "3207", value: "Working"))
        //----------------------------------------------------------------------------------------
        return tflApiPlaceChargeStation
        
    }
    
    
    func appendAdditionalProperty(_ tflApiPlaceAdditionalProperty : TFLApiPlaceAdditionalProperty){
        ///let tflApiPlaceAdditionalPropertyBorough = TFLApiPlaceAdditionalProperty(category: "Address", key: "Borough", sourceSystemKey: "3207", value: "Chelsea")
        
        if let _ =  self.additionalProperties {
            self.additionalProperties?.append(tflApiPlaceAdditionalProperty)
        }else{
            logger.error("self.additionalProperties is nil")
        }
    }
    
    
    
    //--------------------------------------------------------------
    // MARK: - VERSIONS
    // MARK: -
    //--------------------------------------------------------------
    //unversioned
    //[id:ChargeStation_5633] ...
    //versioned
    //[id:ChargeStation_5642-1] Bethnal Green Road (Tesco), Bethnal Green [51.5269, -0.061084] [RankType:Working]
    //[id:ChargeStation_5642-2] Bethnal Green Road (Tesco), Bethnal Green [51.5269, -0.061084] [RankType:Working]
    //--------------------------------------------------------------
    //var tflApiPlaceChargeStationVersionInfo : TFLApiPlaceChargeStationVersionInfo?
    //--------------------------------------------------------------
   // TODO: - remove from all except TaxiRank
    var isVersioned : Bool{
//        var isVersioned_ = false
        
//        if let idJSON = self.id {
//            if idJSON.contains("-"){
//                isVersioned_ = true
//            }else{
//                
//            }
//        }else{
//            
//        }
        return false
    }
    
    //--------------------------------------------------------------
    // MARK: - id
    // MARK: -
    //--------------------------------------------------------------
    
    override var id : String?{
        didSet {
            //to remove warnig
            
        }
    }


    
    
    //--------------------------------------------------------------
    // MARK: - ADDITIONAL PROPERTIES
    //--------------------------------------------------------------
//    var availableString: String?{
//        if let stringValue_ = self.findStringProperty_ChargeStation(.available) {
//            return stringValue_
//        }else{
//            logger.error("self.findStringProperty_ChargeStation(.available) is nil")
//            return nil
//        }
//    }
//
//    var isAvailable : Bool{
//        var isAvailable_ = false
//
//        if let availableString = self.availableString {
//
//            if availableString == "true"{
//                isAvailable_ = true
//            }else{
//
//            }
//        }else{
//            //nil - so false
//        }
//        return isAvailable_
//    }
//
//    var imageUrlString: String?{
//        if let stringValue_ = self.findStringProperty_ChargeStation(.imageUrl) {
//            return stringValue_
//        }else{
//            logger.error("self.findStringProperty_ChargeStation(.imageUrl) is nil")
//            return nil
//        }
//    }
//    var videoUrlString: String?{
//        if let stringValue_ = self.findStringProperty_ChargeStation(.videoUrl) {
//            return stringValue_
//        }else{
//            logger.error("self.findStringProperty_ChargeStation(.imageUrl) is nil")
//            return nil
//        }
//    }
//
//    var viewString: String?{
//        if let stringValue_ = self.findStringProperty_ChargeStation(.view) {
//            //A10 Sth of St Peters Way N\\/B",
//            return stringValue_.replace("\\/", with: "\\")
//
//        }else{
//            logger.error("self.findStringProperty(.view) is nil")
//            return nil
//        }
//    }
    
    //--------------------------------------------------------------
    
    var nameString: String?{
        if let stringValue_ = self.findStringProperty_ChargeStation(.name) {
            return stringValue_
        }else{
            logger.error("self.findStringProperty_ChargeStation(.name) is nil")
            return nil
        }
    }
    var referenceString: String?{
        if let stringValue_ = self.findStringProperty_ChargeStation(.reference) {
            return stringValue_
        }else{
            logger.error("self.findStringProperty_ChargeStation(.reference) is nil")
            return nil
        }
    }

    var regionString: String?{
        if let stringValue_ = self.findStringProperty_ChargeStation(.region) {
            return stringValue_
        }else{
            logger.error("self.findStringProperty_ChargeStation(.region) is nil")
            return nil
        }
    }
    var locationString: String?{
        if let stringValue_ = self.findStringProperty_ChargeStation(.location) {
            return stringValue_
        }else{
            logger.error("self.findStringProperty_ChargeStation(.location) is nil")
            return nil
        }
    }
    var operatorChargeStationString: String?{
        if let stringValue_ = self.findStringProperty_ChargeStation(.operatorChargeStation) {
            return stringValue_
        }else{
            logger.error("self.findStringProperty_ChargeStation(.operatorChargeStation) is nil")
            return nil
        }
    }
    var restrictionsString: String?{
        if let stringValue_ = self.findStringProperty_ChargeStation(.restrictions) {
            return stringValue_
        }else{
            logger.error("self.findStringProperty_ChargeStation(.restrictions) is nil")
            return nil
        }
    }
    var pricingUrlString: String?{
        if let stringValue_ = self.findStringProperty_ChargeStation(.pricingUrl) {
            return stringValue_
        }else{
            logger.error("self.findStringProperty_ChargeStation(.pricingUrl) is nil")
            return nil
        }
    }
    var paymentMethodsString: String?{
        if let stringValue_ = self.findStringProperty_ChargeStation(.paymentMethods) {
            return stringValue_
        }else{
            logger.error("self.findStringProperty_ChargeStation(.paymentMethods) is nil")
            return nil
        }
    }
    var authenticationMethodsString: String?{
        if let stringValue_ = self.findStringProperty_ChargeStation(.authenticationMethods) {
            return stringValue_
        }else{
            logger.error("self.findStringProperty_ChargeStation(.authenticationMethods) is nil")
            return nil
        }
    }
    var electricalCharacteristicsString: String?{
        if let stringValue_ = self.findStringProperty_ChargeStation(.electricalCharacteristics) {
            return stringValue_
        }else{
            logger.error("self.findStringProperty_ChargeStation(.electricalCharacteristics) is nil")
            return nil
        }
    }
    var isReservableString: String?{
        if let stringValue_ = self.findStringProperty_ChargeStation(.isReservable) {
            return stringValue_
        }else{
            logger.error("self.findStringProperty_ChargeStation(.isReservable) is nil")
            return nil
        }
    }
    var connectorCountString: String?{
        if let stringValue_ = self.findStringProperty_ChargeStation(.connectorCount) {
            return stringValue_
        }else{
            logger.error("self.findStringProperty_ChargeStation(.connectorCount) is nil")
            return nil
        }
    }
    var lastUpdatedString: String?{
        if let stringValue_ = self.findStringProperty_ChargeStation(.lastUpdated) {
            return stringValue_
        }else{
            logger.error("self.findStringProperty_ChargeStation(.lastUpdated) is nil")
            return nil
        }
    }
    var urlString: String?{
        if let stringValue_ = self.findStringProperty_ChargeStation(.url) {
            return stringValue_
        }else{
            logger.error("self.findStringProperty_ChargeStation(.url) is nil")
            return nil
        }
    }
    
    
    
    
    
    
    //--------------------------------------------------------------
    // MARK: - Search Additional Properties
    // MARK: -
    //--------------------------------------------------------------
    func findStringProperty_ChargeStation(_ tflApiPlaceChargeStation_additionalProperty_key: TFLApiPlaceChargeStation_additionalProperty_key) -> String?{
        
        let stringValue: String? = findStringPropertyForKeyRawValue(tflApiPlaceChargeStation_additionalProperty_key.rawValue)
        
        return stringValue
    }
    
    //.NumberOfSpaces,"123" >> Int: 123
    func findIntProperty_ChargeStation(_ tflApiPlaceChargeStation_additionalProperty_key: TFLApiPlaceChargeStation_additionalProperty_key) -> Int?{
        let intValue: Int? = findIntPropertyForKeyRawValue(tflApiPlaceChargeStation_additionalProperty_key.rawValue)
        
        return intValue
    }
    
    
    //--------------------------------------------------------------
    // MARK: - Description
    // MARK: -
    //--------------------------------------------------------------
    override var alertTitle: String{
        return self.descriptionLineMain
    }
    

    //alert message
    override var alertMessage: String{
        var description_ = ""
        description_ = description_ +  "id: \(Safe.safeString(self.idSafe))\n"
        description_ = description_ +  "commonName: \(Safe.safeString(self.commonName))\n"
        description_ = description_ +  "lon: \(Safe.safeString(self.lonStringSafe))\n"
        description_ = description_ +  "lat: \(Safe.safeString(self.latStringSafe))\n"

        
        //description_ = description_ +  "name: \(Safe.safeString(self.nameString))\n"
        //description_ = description_ +  "reference: \(Safe.safeString(self.referenceString))\n"
        //description_ = description_ +  "location: \(Safe.safeString(self.locationString))\n"
        description_ = description_ +  "region: \(Safe.safeString(self.regionString))\n"
        description_ = description_ +  "Operator: \(Safe.safeString(self.operatorChargeStationString))\n"
        description_ = description_ +  "Restrictions: \(Safe.safeString(self.restrictionsString))\n"
        description_ = description_ +  "Pricing: \(Safe.safeString(self.pricingUrlString))\n"
        description_ = description_ +  "Payments: \(Safe.safeString(self.paymentMethodsString))\n"
        description_ = description_ +  "Authentication Methods: \(Safe.safeString(self.authenticationMethodsString))\n"
        description_ = description_ +  "Electrical Characteristics: \(Safe.safeString(self.electricalCharacteristicsString))\n"
        description_ = description_ +  "Reservable: \(Safe.safeString(self.isReservableString))\n"
        description_ = description_ +  "Connectors: \(Safe.safeString(self.connectorCountString))\n"
        //description_ = description_ +  "lastUpdated: \(Safe.safeString(self.lastUpdatedString))\n"
        //description_ = description_ +  "url: \(Safe.safeString(self.urlString))\n"

    
        return description_
    }
    
    //--------------------------------------------------------------
    // MARK: - descriptionLineMain
    // MARK: -
    //--------------------------------------------------------------
    
    //common name includes id
    override var descriptionLineMain : String {
        return "\(Safe.safeString(self.locationString))"
    }
    //--------------------------------------------------------------
    // MARK: - Detail
    // MARK: -
    //--------------------------------------------------------------
    //"ChargeStation"
    override var descriptionLineDetail0 : String {
        return "\(Safe.safeString(self.placeType))"
    }
    
    override var descriptionLineDetail1 : String {
        return "\(Safe.safeString(self.restrictionsString))"
    }
    
    //--------------------------------------------------------------
    // MARK: - SubDetail
    // MARK: -
    //--------------------------------------------------------------
    
    override var descriptionLineSubDetail0 : String {
        var description_ = ""
        description_ = description_ +  "\(self.distanceToCurrentLocationFormatted)"
        return description_
    }
    
    override var descriptionLineSubDetail1 : String {
        return "Connector Count:\(Safe.safeString(self.connectorCountString))"
    }
    
    override var descriptionOneLine : String {
        return "[id:\(self.idSafe)] \(Safe.safeString(self.commonName)) [\(self.latStringSafe), \(self.lonStringSafe)] [isReservableString:\(Safe.safeString(self.isReservableString))]"
    }
    
    //RANK,ChargeStation_5973,Park Plaza - County Hall Hotel, hotel forecourt,ChargeStation,-0.116399,51.5014,2,Mon - Sun,24 hours,Working,,
    override var descriptionCSV : String {
        
        var description_ = ""
        
        description_ = description_ +  "ChargeStation, TOSO"
        //description_ = description_ +  "\(self.idSafe),"
        //description_ = description_ +  "\(Safe.safeString(self.commonName)),"
        //description_ = description_ +  "\(self.lonStringSafe),"
        //description_ = description_ +  "\(self.latStringSafe),"
        //description_ = description_ +  "\(Safe.safeString(self.availableString)),"
        //description_ = description_ +  "\(Safe.safeString(self.availableString)),"
        return description_
    }
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - MapPinnable
    //--------------------------------------------------------------
    //used by GoogleMappable gmsMarker > markerImageName> markerImage > UI
    //DO not override MapPinnable > mapImage - change the image name in assets here
    //DO not override GoogleMappable - you can overdide something in an extention
    //overriing markerImageName and changing the final image name based on rules in the subclass e.g ChargeStation.rankType
    //-------------------------------------------------------------------
    
    override var markerImageName: String?{
        var markerImageNameRet: String? = nil
        
        //----------------------------------------------------------------------------------------
        //PIN NAME - varies by type
        //----------------------------------------------------------------------------------------
        // TODO: - move the filename into the RankType
        //        if let rankType = self.rankType {
        //            var imageName = "taxipin_working"
        //
        //            switch rankType{
        //            case .unknown:
        //                imageName = "taxipin_unknown"
        //
        //
        //            case .working:
        //                imageName = "taxipin_working"
        //
        //            case .rest_rank:
        //                imageName = "taxipin_rest_rank"
        //
        //
        //            case .refreshment_rank:
        //                imageName = "taxipin_refreshment_rank"
        //            }
        //            markerImageNameRet = imageName
        //        }else{
        //            logger.error("self.rankType is nil")
        //        }
        //-------------------------------------------------------------------
        //SCALABLE SVG of taxi
        //-------------------------------------------------------------------
        // TODO: - pin for others
        markerImageNameRet = "map_pin_flat_alpha"
        return markerImageNameRet
    }
    
    
    //--------------------------------------------------------------
    // MARK: - COLCQueryCollection
    // MARK: -
    //--------------------------------------------------------------
    //cant override static -
    //http://stackoverflow.com/questions/29189700/overriding-static-vars-in-subclasses-swift-1-2
    
    override class var colcQueryCollectionForType : COLCQueryCollection{
        return TFLApiPlaceCOLCQueryCollectionChargeStation()
    }
    
    //--------------------------------------------------------------
    // MARK: - map json Property string to type to query on
    // MARK: -
    //--------------------------------------------------------------

    fileprivate static let propertyName_isReservable = "isReservable"

    override func value(forPropertyName propertyName: String) -> Any?{
        
        if let valueAny = super.value(forPropertyName: propertyName) {
            return valueAny
        }else{
            
            //not found in parent - may be specific to property
            switch propertyName{
                
            case TFLApiPlaceChargeStation.propertyName_isReservable:
                return self.isReservableString
                
            default:
                logger.error("[TFLApiPlaceChargeStation] UNHANDLED PROPERTY: value:forPropertyName:'\(propertyName) - Charge Station'")
                return nil
            }
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - Factory queries - others in parent
    // MARK: -
    //--------------------------------------------------------------
    class func colcQuery_isAvailable() -> COLCQuery{
        
        //----------------------------------------------------------------------------------------
        //e.g. COLCFilterBool - SEARCH for isAvailable = true
        //----------------------------------------------------------------------------------------
        
        
//        
//        let colcQuery = COLCQuery.colcQuery_COLCFilterBool(title        : "Available",
//                                                           propertyName : TFLApiPlaceChargeStation.propertyName_isAvailable,
//                                                           searchBool   : true,
//                                                      searchPropertyName: TFLApiPlace.propertyName_commonName)
        //----------------------------------------------------------------------------------------
        let colcQuery = TFLApiPlace.COLCQuery_defaultSearchQuery(title:"Reservable")
        
        let colcFilterBool = COLCFilterBool(propertyName: TFLApiPlaceChargeStation.propertyName_isReservable, searchBool: true)
        
        colcQuery.appendCOLCFilter(colcFilterBool)
        //----------------------------------------------------------------------------------------
        return colcQuery
    }
    
}
