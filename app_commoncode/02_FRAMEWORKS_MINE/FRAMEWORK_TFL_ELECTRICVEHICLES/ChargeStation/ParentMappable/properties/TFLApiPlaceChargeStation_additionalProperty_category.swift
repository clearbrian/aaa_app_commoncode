//
//  TFLApiPlaceChargeStationAdditionalPropertyCategory.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 08/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

//"category": "Description",

enum TFLApiPlaceChargeStation_additionalProperty_category: String{
    case unknown = "unknown"
    case description = "Description"
    
    func displayString() -> String{
        switch self{
        case .unknown: return "Unknown"
        //---------------------------------------------------------
        case .description:  return "Description"
        //---------------------------------------------------------
        }
    }
    
    //"payload" >> TFLApiPlaceChargeStationAdditionalPropertyCategory.payload
    static func categoryForCategoryString(categoryString: String?) -> TFLApiPlaceChargeStation_additionalProperty_category{
        
        if let categoryString = categoryString {
            switch categoryString{
            //---------------------------------------------------------
            case "Description": return .description
            //---------------------------------------------------------
            default: return .unknown
            }
        }else{
            logger.error("categoryString is nil")
            return .unknown
        }
    }
}
