//
//  TFLApiPlaceChargeStationAdditionalPropertyKey.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 08/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation


//    "key": "Name",
//    "key": "Reference",
//    "key": "Location",
//    "key": "Region",
//    "key": "Operator",
//    "key": "Restrictions",
//    "key": "PricingUrl",
//    "key": "PaymentMethods",
//    "key": "AuthenticationMethods",
//    "key": "ElectricalCharacteristics",
//    "key": "IsReservable",
//    "key": "ConnectorCount",
//    "key": "LastUpdated",
//    "key": "Url",

enum TFLApiPlaceChargeStation_additionalProperty_key: String{
    
    case unknown = "UNKNOWN KEY"
    //----------------------------------------------------------------------------------------
    case name = "Name"
    case reference = "Reference"
    case location = "Location"
    case region = "Region"
    case operatorChargeStation = "Operator"
    case restrictions = "Restrictions"
    case pricingUrl = "PricingUrl"
    case paymentMethods = "PaymentMethods"
    case authenticationMethods = "AuthenticationMethods"
    case electricalCharacteristics = "ElectricalCharacteristics"
    case isReservable = "IsReservable"
    case connectorCount = "ConnectorCount"
    case lastUpdated = "LastUpdated"
    case url = "Url"
    //----------------------------------------------------------------------------------------
    
    func displayString() -> String{
        switch self{
            
        case .unknown: return "Unknown"
        //---------------------------------------------------------
        case .name: return "Name"
        case .reference: return "Reference"
        case .location: return "Location"
        case .region: return "Region"
        case .operatorChargeStation: return "Operator"
        case .restrictions: return "Restrictions"
        case .pricingUrl: return "PricingUrl"
        case .paymentMethods: return "PaymentMethods"
        case .authenticationMethods: return "AuthenticationMethods"
        case .electricalCharacteristics: return "ElectricalCharacteristics"
        case .isReservable: return "IsReservable"
        case .connectorCount: return "ConnectorCount"
        case .lastUpdated: return "LastUpdated"
        case .url: return "Url"
        //---------------------------------------------------------
        }
    }
    
    //"RankType" >> TFLApiPlaceChargeStationAdditionalPropertyKey.RankType
    static func keyForKeyString(keyString: String?) -> TFLApiPlaceChargeStation_additionalProperty_key{
        
        if let keyString = keyString {
            switch keyString{
            //---------------------------------------------------------
            case "Name": return .name
            case "Reference": return .reference
            case "Location": return .location
            case "Region": return .region
            case "Operator": return .operatorChargeStation
            case "Restrictions": return .restrictions
            case "PricingUrl": return .pricingUrl
            case "PaymentMethods": return .paymentMethods
            case "AuthenticationMethods": return .authenticationMethods
            case "ElectricalCharacteristics": return .electricalCharacteristics
            case "IsReservable": return .isReservable
            case "ConnectorCount": return .connectorCount
            case "LastUpdated": return .lastUpdated
            case "Url": return .url
            //---------------------------------------------------------
            default:
                return .unknown
            }
        }else{
            logger.error("keyString is nil")
            return .unknown
        }
    }
}
