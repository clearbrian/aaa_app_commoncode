//
//  OpenWithController.swift
//  joyride
//
//  Created by Brian Clear on 14/08/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit


//copied form google
enum OpenWithTravelMode: String{
    
    case driving = "driving" //(default) indicates standard driving directions using the road network.
    case walking = "walking" //requests walking directions via pedestrian paths & sidewalks (where available).
    case bicycling = "bicycling" //requests bicycling directions via bicycle paths & preferred streets (where available).
    case transit = "transit"  //
}





class OpenWithController{
    
    
    
    //--------------------------------------------------------------
    // MARK: -
    //-------------------------------------------------------------------
    
    func open(_ openAppConfig : OpenAppConfig?,
              clkPlaceStart: CLKPlace?,
              clkPlaceEnd: CLKPlace?,
              openWithTravelMode: OpenWithTravelMode,
              presentingViewController: UIViewController){
        
        if let openAppConfig = openAppConfig{
            logger.debug("openWithViewControllerReturned openAppConfig.openWithType.name():\(openAppConfig.openWithType.name())")
            
            //------------------------------------------------
            //SAVE TO DB
            //------------------------------------------------
            self.saveTripToDB(openAppConfig,
                              clkPlaceStart:clkPlaceStart,
                              clkPlaceEnd: clkPlaceEnd)
                              
            
            
            // TODO: - clkPlaceEnd might not be set
            //------------------------------------------------
            if let clkPlaceStart = clkPlaceStart {
                if let clkPlaceEnd = clkPlaceEnd {
                    //------------------------------------------------------------------------------------------------
                    openAppConfig.open(presentingViewController,
                                       clkPlaceStart: clkPlaceStart,
                                       clkPlaceEnd: clkPlaceEnd,
                                       openWithTravelMode: openWithTravelMode,
                                       success:{() -> Void in
                                            //app opened ok
                                        
                        },
                                       failure:{(errMsg) -> Void in
                                        
                                            CLKAlertController.showAlertInVC(presentingViewController, title: "Error opening application", message: errMsg)
                                        
                    })
                    //------------------------------------------------------------------------------------------------
                    
                }else{
                    logger.error("self.clkPlaceEnd is nil")
                }
            }else{
                logger.error("self.clkPlaceStart is nil")
            }
            //------------------------------------------------
            
        }else{
            logger.debug("openWithViewControllerReturned openWithItem is nil")
        }
    }
    
    
    // TODO: - move into CLKTrip
    func saveTripToDB(_ openAppConfig: OpenAppConfig,
                      clkPlaceStart: CLKPlace?,
                      clkPlaceEnd: CLKPlace?){
        
        logger.error("TODO appDelegate.colcDBManager.saveCLKTrip")
        //------------------------------------------------------------------------------------------------
        //START
        //------------------------------------------------------------------------------------------------
//        if let clkPlaceStart = clkPlaceStart{
//            if let clkPlaceEnd = clkPlaceEnd{
//                
//                if let clkTrip = CLKTrip(name: "\(openAppConfig.openWithType.name()) Trip",
//                                         openWithType: openAppConfig.openWithType,
//                                         dateTime: Date(),
//                                         clkPlaceStart: clkPlaceStart, clkPlaceEnd: clkPlaceEnd)
//                {
//                    //---------------------------------------------------------------------
//
//                    //                    appDelegate.colcDBManager.saveCLKTrip(clkTrip, completion: { (dbResultType) -> Void in
////                        switch dbResultType {
////                        case .success:
////                            //-------------------
////                            //SUCCESS
////                            //-------------------
////                            
////                            logger.debug("trip saved ok")
////                            
////                            
////                        case .error(let errorType):
////                            //--------------------
////                            //ERROR
////                            //--------------------
////                            logger.error("\(errorType)")
////                            
////                        }
////                    })
//                    //---------------------------------------------------------------------
//                }else{
//                    logger.error("FAILED TO CREATE CLKTrip from clkPlaceStart/clkPlaceEnd")
//                }
//            }else{
//                logger.error("self.clkPlaceEnd is nil")
//            }
//        }else{
//            logger.error("clkPlaceStart is nil")
//        }
    }
}
