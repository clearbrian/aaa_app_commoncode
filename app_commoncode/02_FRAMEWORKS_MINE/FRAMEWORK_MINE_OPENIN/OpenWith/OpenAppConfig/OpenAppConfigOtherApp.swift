//
//  self.swift
//  joyride
//
//  Created by Brian Clear on 30/05/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import StoreKit

class OpenAppConfigOtherApp: OpenAppConfig, SKStoreProductViewControllerDelegate{
    //------------------------------------------------
    //OPEN APP BY URL SCHEME
    //------------------------------------------------
    //"lyft://"
    //Note must be in Info.plist LSApplicationQueriesSchemes for canOpenURL to work
    var appSchemeString: String = ""

    //------------------------------------------------
    //APP NOT INSTALLED
    //------------------------------------------------
    //------------------------------------------------
    //SEARCH APP STORE
    //------------------------------------------------
    //App Store id of app to open
    //    https://linkmaker.itunes.apple.com/en-gb/
    //    https://linkmaker.itunes.apple.com/en-us/   //use US - and use geo.itunes as will find nearest store
    //    <a href="https://geo.itunes.apple.com/us/app/lyft-taxi-app-alternative/id529379082?mt=8">Lyft - Taxi App Alternative - Lyft, Inc.</a>
    
    var appStoreIdUInt: UInt = 0
    
    //------------------------------------------------
    //ITUNES WEBSITE URL FOR APP
    //------------------------------------------------
    //"https://geo.itunes.apple.com/us/app/lyft-taxi-app-alternative/id529379082?mt=8"
    
    //if app not found on app store - this is web url for itunes - should not happen
    
    //-----------------------------------------------------------------------------------
    var iTunesURLString: String?{
        //https://geo.itunes.apple.com/en/app/id789653936?mt=8
        
        if appStoreIdUInt > 0{
            return "https://geo.itunes.apple.com/en/app/id\(appStoreIdUInt)?mt=8"
        }else{
            logger.error("self.appStoreIdUInt is 0")
            return nil
        }
    }
    
    //-----------------------------------------------------------------------------------
    //Company Website
    //-----------------------------------------------------------------------------------
    //http://www.lyft.com
    var appWebsiteURLString:String = ""
    
    //-----------------------------------------------------------------------------------
    
    var urlStringToOpen: String?{
        logger.error("SUBCLASS SHOULD SET urlStringToOpen")
        return nil
    }
    
    //------------------------------------------------------------------------------------------------
    //init
    //------------------------------------------------------------------------------------------------
    init(openWithType:OpenWithType,
         appSchemeString: String,
         appWebsiteURLString: String,
         appStoreIdUInt: UInt,
         openWithSource: OpenWithSource)
    {
        super.init(openWithType: openWithType, openWithSource: openWithSource)
        
        self.appSchemeString = appSchemeString
        self.appWebsiteURLString = appWebsiteURLString
        self.appStoreIdUInt = appStoreIdUInt
        
    }
    
    override func open( _ presentingViewController: UIViewController,
                        clkPlaceStart: CLKPlace,
                        clkPlaceEnd: CLKPlace,
                        openWithTravelMode: OpenWithTravelMode,
                        success: () -> Void,
                        failure: (_ errorMessage : String) -> Void
        )
    {
        
        self.canOpenInOtherApp(presentingViewController,
                               canOpenInOtherApp_success:{() -> Void in
                                //---------------------------------------------------------------------
                                //App is installed - open the app using deeplinking
                                //---------------------------------------------------------------------
//                                if let urlStringToOpen = self.urlStringToOpen {
//                                    
//                                    if let url = NSURL(string:urlStringToOpen){
//                                        UIApplication.sharedApplication().openURL(url)
//
//                                        
//                                    }else{
//                                        logger.error("url is nil - not valid:")
//                                        failure(errorMessage: "appDeepLinkURLString is invalid")
//                                    }
//                                }else{
//                                    logger.error("self.urlStringToOpen is nil - should be set by subclass")
//                                }
                                //---------------------------------------------------------------
                                //Open the app
                                //---------------------------------------------------------------
                                self.openAppByOpenURL()
                                //---------------------------------------------------------------
                               
            },
                               failure:{(errMsg) -> Void in
                                logger.debug("errMsg:\(errMsg)")
                                
            }
        )
    }
    //--------------------------------------------------------------
    // MARK: - Is app installed - check scheme
    // MARK: -
    //--------------------------------------------------------------

    //seperated out for CMMAAppLauncher
    func canCallAppScheme(_ appSchemeString: String) -> Bool
    {
        var canOpenOtherApp_ = false
        //------------------------------------------------------------------------------------------------
        if self.appSchemeString == "" {
          logger.error("appSchemeString = '' canOpenOtherApp? failed")
        }else{
            //------------------------------------------------
            //Scheme is not blank
            //------------------------------------------------
            logger.debug("CAN OPEN APP?'\(self.appSchemeString)'")
            
            if let appSchemeURL = URL(string:appSchemeString){
                
                if (UIApplication.shared.canOpenURL(appSchemeURL))
                {
                    canOpenOtherApp_ = true
                }
                else
                {
                    logger.error("canOpenURL returned false - app not installed or wrong scheme string - openAppConfig.appSchemeString:'\(self.appSchemeString)' not valid - check Info.plist of the other app AND check scheme is in your allowed schemes to open in this apps Info.plist")
                    //canOpenURL returned false - app not installed or wrong scheme string - check Info.plist of the other app
                    canOpenOtherApp_ = false
                }
            }else{
                logger.error("appSchemeURL is nil - openAppConfig.appSchemeString:'\(self.appSchemeString)' not valid")
            }
        }
        //------------------------------------------------------------------------------------------------
        return canOpenOtherApp_
        
    }
    
    //--------------------------------------------------------------
    // MARK: - OPEN THE APP
    // MARK: -
    //--------------------------------------------------------------

    func openAppByOpenURL(){
        if let urlStringToOpen = self.urlStringToOpen {
            
            if let url = URL(string:urlStringToOpen){
                UIApplication.shared.openURL(url)
                
            }else{
                logger.error("url is nil - not valid:")
                //failure(errorMessage: "appDeepLinkURLString is invalid")
            }
        }else{
            logger.error("self.urlStringToOpen is nil - should be set by subclass")
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - Can device open app - if not check store
    // MARK: -
    //--------------------------------------------------------------

    func canOpenInOtherApp( _ presentingViewController: UIViewController,
                            canOpenInOtherApp_success: () -> Void,
                       failure: (_ errorMessage : String) -> Void
        )
    {
        if canCallAppScheme(self.appSchemeString)
        {
            //---------------------------------------------------------------------
            //App is installed - open the app using deeplinking
            //---------------------------------------------------------------------
            //dont check self.urlStringToOpen till canOpenInOtherApp_success() called
            //CMMAppAppLauncher sets urlString later
            canOpenInOtherApp_success()
            
        }
        else
        {
            //-----------------------------------------------------------------------------------
            //APP is not installed - ask user would they like to search app store
            //-----------------------------------------------------------------------------------
            self.appNotInstalled(presentingViewController,
                                   canOpenInOtherApp_success:{() -> Void in
                                    //-------------------------------
                                    canOpenInOtherApp_success()
                                    //-------------------------------
                },
                                   failure:{(errMsg) -> Void in
                                    logger.error("[appNotInstalled() error] errMsg:\(errMsg)")
                                    failure(errMsg)
                                    
                }
            )
            //-----------------------------------------------------------------------------------
        }//if
    }
    
    func appNotInstalled(_ presentingViewController: UIViewController,
                         canOpenInOtherApp_success: () -> Void,
                         failure: (_ errorMessage : String) -> Void)
    {
        CLKAlertController.showAlertInVCWithCancelAndOkHandler(presentingViewController,
                                                               title: "\(self.appName) is not installed",
                                                               buttonTitleDefault: "Yes",
                                                               buttonTitleCancel: "No",
                                                               message: "Would you like to search for it on the App Store?"
        ){ () -> Void in
            //user pressed OK to search
            //------------------------------------------------
            //Search for app id in Store
            //------------------------------------------------
//..BUg was crashing in iOS 9 - in app store page would load but
//            self.searchInAppStore(self, presentingViewController: presentingViewController,
//                                  success:{() -> Void in
//                                    logger.debug("searchIdInAppStore: success")
//                                    //DONT call here this just means StoreVC has opened successfully
//                                    //canOpenInOtherApp_success()
//                                    
//                },
//                                  failure:{(errorMessage) -> Void in
//                                    failure(errorMessage: errorMessage)
//                },
//                                  failureSearchInAppStore:{(errorMessage) -> Void in
//                                    
//                                    //-----------------------------------------------------------------------------------
//                                    //APP id not found in Store - fallback to website
//                                    //------------------------------------------------
//                                    
//                                    CLKAlertController.showAlertInVCWithCancelAndOkHandler(presentingViewController,
//                                        title: "App \(self.appName) not found in App Store",
//                                        buttonTitleDefault: "Yes",
//                                        buttonTitleCancel: "No",
//                                        message: "Would you like to open the app website in Safari?"
//                                    ){ () -> Void in
//                                        //user pressed OK to search
//                                        //------------------------------------------------
//                                        //Search for app id in Store
//                                        //------------------------------------------------
//                                        //self.openiTunesPageInSafari(openAppConfig: openAppConfig, presentingViewController: presentingViewController)
//                                        self.openAppWebsitePageInSafari(openAppConfigOtherApp: self, presentingViewController: presentingViewController)
//                                        
//                                        //-----------------------------------------------------------------------------------
//                                    }//showAlertInVCWithCancelAndOkHandler
//                }
//                
//                
//            )//searchInAppStore
            
            self.openiTunesPageInSafari(openAppConfigOtherApp: self, presentingViewController: presentingViewController)
            //-----------------------------------------------------------------------------------
        }//showAlertInVCWithCancelAndOkHandler
    }
    
    //--------------------------------------------------------------
    // MARK: - StoreKit
    // MARK: -
    //--------------------------------------------------------------
    
    var storeProductViewController : SKStoreProductViewController?
    
    func searchInAppStore(_ openAppConfigOtherApp: OpenAppConfigOtherApp,
                          presentingViewController: UIViewController,
                          success: @escaping () -> Void,
                          failure: (_ errorMessage : String) -> Void,
                          failureSearchInAppStore: @escaping (_ errorMessage : String) -> Void
        )
    {
        if openAppConfigOtherApp.appStoreIdUInt > 0{
            self.storeProductViewController = SKStoreProductViewController()
            
            if let storeProductViewController = self.storeProductViewController {
                
                storeProductViewController.delegate = self
                //-----------------------------------------------------------------------------------
                storeProductViewController.loadProduct(
                    withParameters: [SKStoreProductParameterITunesItemIdentifier: NSNumber(value: openAppConfigOtherApp.appStoreIdUInt as UInt)],
                    
                    completionBlock: {
                        (result: Bool, error: Error?) -> Void in
                        
                        //-----------------------------------------------------------------------------------
                        //if app not released yet then user will see blank screen - when they hit Cancel they get error domain 0
                        //-----------------------------------------------------------------------------------
                        if let error = error {
                            failureSearchInAppStore("error:[\(error)]")
                            
                        }else{
                            //no error
                            if result == false{
                                print("TODO fall back to iTunes url")
                                //self.handle(iTunesURLString)
                                failureSearchInAppStore("error:[result == false]")
                            }else{
                                logger.error("storeProductViewController. loadProductWithParameters: result is true")
                                success()
                                
                            }
                        }
                        //move down
                        //self.storeProductViewController = nil
                        //-----------------------------------------------------------------------------------
                    }
                )

                
                
                
                //-----------------------------------------------------------------------------------
                presentingViewController.present(storeProductViewController, animated: true, completion: nil)
                //... productViewControllerDidFinish
                //-----------------------------------------------------------------------------------
                
            }else{
                failure("self.storeProductViewController is nil")
            }
        }else{
            failure("[OpenAppConfig error] appStoreIdUInt > 0 FAILED")
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - SKStoreProductViewControllerDelegate
    // MARK: -
    //--------------------------------------------------------------
    func productViewControllerDidFinish(_ storeProductViewController: SKStoreProductViewController) {
        
        
        switch UIApplication.shared.applicationState{
        case .active:
            logger.error("productViewControllerDidFinish called and .Active")
            storeProductViewController.dismiss(animated: true, completion: nil)
            
        case .inactive:
            logger.error("productViewControllerDidFinish called and .Inactive")
            
        case .background:
            //if user hits Store button the the full App Store app is opened and we enter the background
            logger.error("productViewControllerDidFinish called and .Background")
        @unknown default:
            logger.error("@unknown default:")
            
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - OPEN ITUNES PAGE IN SAFARI
    // MARK: -
    //--------------------------------------------------------------
    //not used but should work - if id not in itunes search then opening page is pointless
    func openiTunesPageInSafari(openAppConfigOtherApp: OpenAppConfigOtherApp,
                                                      presentingViewController: UIViewController)
    {
        //needed? if app is installed but wrong
        // //------------------------------------------------
        // //Not installed open website
        // //------------------------------------------------
        if openAppConfigOtherApp.iTunesURLString == ""{
            CLKAlertController.showAlertInVC(presentingViewController, title: "Error opening application", message: "openAppConfigOtherApp.iTunesURLString is blank")
        }else{
            
            if let iTunesURLString = openAppConfigOtherApp.iTunesURLString {
                
                if let iTunesURL = URL(string:iTunesURLString) {
                    UIApplication.shared.openURL(iTunesURL)
                }else{
                    //invalid website URL
                    logger.error("NSURL to urlBackupWebsite is nil - '\(String(describing: openAppConfigOtherApp.iTunesURLString))' not valid")
                }
            }else{
                logger.error("openAppConfig.iTunesURLString is nil - did you get the appStoreIdUInt")
            }
            
        }
    }
    
    
    //--------------------------------------------------------------
    // MARK: - OPEN DEVELOPER WEBSITE IN SAFARI
    // MARK: -
    //--------------------------------------------------------------

    func openAppWebsitePageInSafari(openAppConfigOtherApp: OpenAppConfigOtherApp,
                                                          presentingViewController: UIViewController)
    {
        //needed? if app is installed but wrong
        // //------------------------------------------------
        // //Not installed open website
        // //------------------------------------------------
        if openAppConfigOtherApp.iTunesURLString == ""{
            CLKAlertController.showAlertInVC(presentingViewController, title: "Error opening application", message: "openAppConfigOtherApp.iTunesURLString is blank")
        }else{
            if let appWebsiteURLStringURL = URL(string:openAppConfigOtherApp.appWebsiteURLString) {
                UIApplication.shared.openURL(appWebsiteURLStringURL)
            }else{
                //invalid website URL
                logger.error("NSURL to urlBackupWebsite is nil - '\(openAppConfigOtherApp.appWebsiteURLString)' not valid")
            }
        }
    }
}
