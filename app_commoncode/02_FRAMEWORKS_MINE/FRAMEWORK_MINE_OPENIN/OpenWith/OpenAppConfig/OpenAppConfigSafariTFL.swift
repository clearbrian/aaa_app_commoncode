//
//  OpenAppConfigSafariTFLTFL.swift
//  joyride
//
//  Created by Brian Clear on 07/06/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//
import Foundation
import SafariServices

class OpenAppConfigSafariTFL: OpenAppConfigWebsite, SFSafariViewControllerDelegate{
    
    let tflManager = TFLManager()
    //------------------------------------------------------------------------------------------------
    //init
    //------------------------------------------------------------------------------------------------
    init(openWithType:OpenWithType, appWebsiteURLString: String)
    {
        super.init(openWithType: openWithType, appWebsiteURLString:appWebsiteURLString, openWithSource: .OpenWithSource_Safari)
        
    }
    override func open(_ presentingViewController: UIViewController,
                       clkPlaceStart: CLKPlace?,
                       clkPlaceEnd: CLKPlace?,
                       openWithTravelMode: OpenWithTravelMode,
                       success: () -> Void,
                       failure: (_ errorMessage : String) -> Void)
    {
        
        if let clkPlaceStart = clkPlaceStart {
            if let clkPlaceEnd = clkPlaceEnd {
                //------------------------------------------------------------------------------------------------
                //generic went funny have to hard code it
                
//                if let clkTrip = CLKTrip(clkPlaceStart: clkPlaceStart, clkPlaceEnd: clkPlaceEnd) {
//                    
//                    //class T must implement SFSafariViewControllerDelegate
//                    //was getting error
//                    //Generic parameter 'T' could not be inferred
//                    // self.tflManager.openTripInSafari(clkTrip, viewController: presentingViewController as UIViewController)
//                    
//                    //self.tflManager.openTripInSafari(clkTrip, viewController: presentingViewController)
//                    //self.tflManager.openTripInSafari(clkTrip, viewController: presentingViewController as UIViewController)
//                    
//                }else{
//                    logger.error("CLKTrip() returned nil")
//                }
                
                
                //------------------------------------------------------------------------------------------------
                if let openWithViewController = presentingViewController as? OpenWithViewController {
                    if let clkTrip = CLKTrip(name: "TFL Trip", openWithType: .openWithType_TFL, dateTime: Date(), clkPlaceStart: clkPlaceStart, clkPlaceEnd: clkPlaceEnd) {
                        
                        //class T must implement SFSafariViewControllerDelegate
                        //was getting error
                        //Generic parameter 'T' could not be inferred
                        // self.tflManager.openTripInSafari(clkTrip, viewController: presentingViewController as UIViewController)
                        
                        //self.tflManager.openTripInSafari(clkTrip, viewController: presentingViewController)
                        self.tflManager.openTripInSafari(clkTrip, openWithViewController: openWithViewController)
                        
                    }else{
                        logger.error("CLKTrip() returned nil")
                    }
                }else{
                    logger.error("presentingViewController as? OpenWithViewController FAILED")
                }
                //------------------------------------------------------------------------------------------------
                
            }else{
                logger.error("self.clkPlaceEnd is nil")
                CLKAlertController.showOKAlert("Destination", message:"Please choose a destination", inPresentingViewController: presentingViewController)
            }
        }else{
            logger.error("self.clkPlaceStart is nil")
            CLKAlertController.showOKAlert("Start", message:"Please choose a start location", inPresentingViewController: presentingViewController)
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - SFSafariViewControllerDelegate
    //--------------------------------------------------------------
    
    //    func safariViewController(controller: SFSafariViewController, activityItemsForURL URL: NSURL, title: String?) -> [UIActivity]{
    //        logger.debug("safariViewController: safariViewControllerDidFinish")
    //    }
    
    /*! @abstract Delegate callback called when the user taps the Done button. Upon this call, the view controller is dismissed modally. */
    func safariViewControllerDidFinish(_ controller: SFSafariViewController){
        logger.debug("safariViewController: safariViewControllerDidFinish")
        
        //SafariController button use app tint but has white border so can be hard to see
        // AppearanceManager.appColorNavbarAndTabBar
        // AppearanceManager.appColorTint
        //appDelegate.window?.tintColor = UIColor.redColor()
        
        //appDelegate.window?.tintColor = AppearanceManager.appColorNavbarAndTabBar
        //... change back here
        
        appDelegate.window?.tintColor = AppearanceManager.appColorTintExternal
    }
    
    /*! @abstract Invoked when the initial URL load is complete.
     @param success YES if loading completed successfully, NO if loading failed.
     @discussion This method is invoked when SFSafariViewController completes the loading of the URL that you pass
     to its initializer. It is not invoked for any subsequent page loads in the same SFSafariViewController instance.
     */
    func safariViewController(_ controller: SFSafariViewController, didCompleteInitialLoad didLoadSuccessfully: Bool){
        if didLoadSuccessfully {
            logger.debug("safariViewController: didLoadSuccessfully!")
        }else{
            logger.error("safariViewController: didLoadSuccessfully FAILED")
        }
        
    }
}
