//
//  OpenAppConfigDeepLinkingLyft.swift
//  joyride
//
//  Created by Brian Clear on 04/06/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit

class OpenAppConfigDeepLinkingLyft: OpenAppConfigDeepLinking{
    
    //-----------------------------------------------------------------------------------
    /*
     =======================================================================================================
     =======================================================================================================
     
     https://developer.lyft.com/docs/deeplinking
     
     https://docs.google.com/document/d/1Gn3Fo5-jWkGZumJLlEgK1nAizxCCMm5T0X4qTF7yeLE/edit
     
     Client secret
     dj5FKag_VcaHX-cUfWuU7JmGEgS3wZEc
     clientID
     gadHhTlp1TO7
     
     =======================================================================================================
     =======================================================================================================
     SUPPORTED DEEPLINKS
     Link to the Lyft app map screen
     The following links will open the app and place the user on the map screen.
     
     lyft://
     
     Setting the ride type
     lyft://ridetype?id=<ride_type>
     lyft://ridetype?id=<ride_type>&partner=<client_id>
     Any ride type returned by the /ridetypes API may be used as the ride type parameter in a deeplink.
     
     Ride Type	Description
     lyft	Standard Lyft
     lyft_line	Lyft Line
     lyft_plus	Lyft Plus
     Specifying a pickup and destination location for Lyft rides
     You can choose to specify the pickup and/or destination location for Lyft rides. If no pickup location is specified the user’s current location will be assumed.
     //---------------------------------------------------------------------
     lyft://ridetype?id=lyft&pickup[latitude]=37.764728&pickup[longitude]=-122.422999&destination[latitude]=37.7763592&destination[longitude]=-122.4242038
     //---------------------------------------------------------------------
     lyft://ridetype?id=lyft
     &pickup[latitude]=37.764728
     &pickup[longitude]=-122.422999
     &destination[latitude]=37.7763592
     &destination[longitude]=-122.4242038
     
     //---------------------------------------------------------------------
     Adding a promo code
     Deeplinks can be used to add a promo code to a user's account. This link will land the user on the payments screen with the promo code my_custom_promo_code pre-populated for the user and ready to be submitted.
     
     lyft://payment?credits=<my_custom_promo_code>
     
     Contact partnerships@lyft.com to get access to promo codes to distribute to your users.
     =======================================================================================================
     =======================================================================================================
     */
    
    //-----------------------------------------------------------------------------------
    let kLyft_CLIENT_ID:String = "gadHhTlp1TO7"
    //-----------------------------------------------------------------------------------

    
    init(){
        //"https://geo.itunes.apple.com/us/app/lyft-taxi-app-alternative/id529379082?mt=8"
        super.init(openWithType : OpenWithType.openWithType_Lyft,
            appSchemeString     : "lyft://",                   //make sure you add this to Info.plist LSApplicationQueriesSchemes
            appDeepLinkURLString: "",                          //required if OpenAndDeepLink
            appWebsiteURLString : "http://lyft.com",
            appStoreIdUInt      : 529379082
        )
    }
    
    
    
    override func open( _ presentingViewController: UIViewController,
                        clkPlaceStart: CLKPlace,
                        clkPlaceEnd: CLKPlace,
                        openWithTravelMode: OpenWithTravelMode,
                        success: () -> Void,
                        failure: (_ errorMessage : String) -> Void){
        logger.debug("OpenAppConfigDeepLinkingLyft")
        
        self.openTripInLyft(clkPlaceStart: clkPlaceStart, clkPlaceEnd: clkPlaceEnd, presentingViewController:presentingViewController)
        
    }
    
    func openTripInLyft(clkPlaceStart: CLKPlace?,
                                      clkPlaceEnd: CLKPlace?,
                                      presentingViewController: UIViewController)
    {
        var lyftPlacePickup : LyftPlace?
        var lyftPlaceDestination : LyftPlace?
        
        //------------------------------------------------------------------------------------------------
        //START
        //------------------------------------------------------------------------------------------------
        if let clkPlaceStart = clkPlaceStart{
            lyftPlacePickup = LyftPlace.convert_CLKPlace_To_LyftPlace(clkPlaceStart, lyftPlaceType : .Pickup)
            
        }else{
            logger.error("clkPlaceStart is nil")
        }
        //------------------------------------------------------------------------------------------------
        //DESTINATION
        //------------------------------------------------------------------------------------------------
        
        if let clkPlaceEnd = clkPlaceEnd{
            lyftPlaceDestination = LyftPlace.convert_CLKPlace_To_LyftPlace(clkPlaceEnd, lyftPlaceType : .Destination)
        }else{
            logger.error("self.clkPlaceEnd is nil")
        }
        
        //------------------------------------------------------------------------------------------------
        //OPEN in Lyft
        //------------------------------------------------------------------------------------------------
        
        if let lyftPlacePickup_ = lyftPlacePickup{
            if let lyftPlaceDestination_ = lyftPlaceDestination{
                //------------------------------------------------------------------------------------------------
                self.appDeepLinkURLString = self.deepLinkingURLString(lyftPlacePickup: lyftPlacePickup_, lyftPlaceDestination: lyftPlaceDestination_)
                //------------------------------------------------------------------------------------------------
                if appDeepLinkURLString == ""{
                    logger.error("error: appDeepLinkURLString is ''")
                    
                }else{
                    //-----------------------------------------------------------------------------------
                    self.canOpenInOtherApp(presentingViewController,
                                      canOpenInOtherApp_success:{() -> Void in
                                        //---------------------------------------------------------------------
                                        //App is installed - open the app using deeplinking
                                        //---------------------------------------------------------------------
//                                        if let urlStringToOpen = self.urlStringToOpen {
//                                            
//                                            if let url = NSURL(string:urlStringToOpen){
//                                                UIApplication.sharedApplication().openURL(url)
//                                                
//                                            }else{
//                                                logger.error("url is nil - not valid:")
//                                                //failure(errorMessage: "appDeepLinkURLString is invalid")
//                                            }
//                                        }else{
//                                            logger.error("self.urlStringToOpen is nil - should be set by subclass")
//                                        }
                                        //---------------------------------------------------------------
                                        //Open the app
                                        //---------------------------------------------------------------
                                        self.openAppByOpenURL()
                                        //---------------------------------------------------------------
                                        
                        },
                                      failure:{(errMsg) -> Void in
                                        logger.debug("errMsg:\(errMsg)")
                                        
                        }
                    )
                    //-----------------------------------------------------------------------------------
                    
                }
                
            }else{
                logger.error("lyftPlaceDestination is nil")
            }
        }else{
            logger.error("lyftPlacePickup is nil 333")
        }
    }
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - Deep Linking
    //--------------------------------------------------------------
    
    /*
     //---------------------------------------------------------------------
     lyft://ridetype?id=lyft&pickup[latitude]=37.764728&pickup[longitude]=-122.422999&destination[latitude]=37.7763592&destination[longitude]=-122.4242038
     //---------------------------------------------------------------------
     lyft://ridetype?id=lyft
     &pickup[latitude]=37.764728&pickup[longitude]=-122.422999
     &destination[latitude]=37.7763592&destination[longitude]=-122.4242038
     //---------------------------------------------------------------------
     */
    
    func deepLinkingURLString(lyftPlacePickup:LyftPlace, lyftPlaceDestination:LyftPlace) -> String{
        
        //Uber is installed - open the app
        var urlString = ""

        
        if self.appSchemeString == "" {
            logger.error("appSchemeString is ''")
        }else{
            
            //================================================================================================
            //lyftapp://ridetype?id=lyft
            //urlString = urlString + "lyft://ridetype?id=lyft"
            urlString = urlString + "\(self.appSchemeString)ridetype?id=lyft"
            
            //---------------------------------------------------------------------
            //&pickup[latitude]=37.764728&pickup[longitude]=-122.422999
            //&destination[latitude]=37.7763592&destination[longitude]=-122.4242038
            //---------------------------------------------------------------------
            urlString = urlString + lyftPlacePickup.paramsForLyftPlace
            urlString = urlString + lyftPlaceDestination.paramsForLyftPlace
            //---------------------------------------------------------------------
            //partner=<client_id>
            urlString = urlString + "&partner=\(kLyft_CLIENT_ID)"
            //================================================================================================
            
            //---------------------------------------------------------------------
            //DEBUG
            //---------------------------------------------------------------------
            logger.debug("urlString:\(urlString)")
            //DEBUG
            let lines = urlString.components(separatedBy: "&")
            for line in lines{
                logger.debug("\(line)")
            }
            //================================================================================================
            
        }

        return urlString
    }

}
