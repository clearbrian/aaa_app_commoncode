//
//  OpenAppConfigDeepLinkingHailo.swift
//  joyride
//
//  Created by Brian Clear on 04/06/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit
class OpenAppConfigDeepLinkingHailo: OpenAppConfigDeepLinking{
    
    //-----------------------------------------------------------------------------------
    /*
     https://developer.hailoapp.com/applications
     
     support@cityoflondonconsulting.com
     teV9mef0uT9hoJ0wauT3eM6vE
     
     
     KEY - MUST URLENCODE IT
     cDB21dEEc5S5dUZfokUgXeL5CjSGA+o9bkiM5bC3FSndVihz5S4OkRb9RO0hI5YQ4B2FxGrTipmEsfFW/zYb4CXk2dGoK5TDbaAKTI6b+gIOnoywEVUeJm8/2R78iarX3iqw5e3ruSAVDoMErmw/vKbVrFodQMtjtB+jrAkLkTpO1rZLmfjympUVOQSn/znAR//rUum71aKBOS01QOFXBQ==
     
     //OK - driver/eta
     https://api.hailoapp.com/drivers/eta?latitude=51.510761&longitude=0.1174437&api_token=cDB21dEEc5S5dUZfokUgXeL5CjSGA%2Bo9bkiM5bC3FSndVihz5S4OkRb9RO0hI5YQ4B2FxGrTipmEsfFW%2FzYb4CXk2dGoK5TDbaAKTI6b%2BgIOnoywEVUeJm8%2F2R78iarX3iqw5e3ruSAVDoMErmw%2FvKbVrFodQMtjtB%2BjrAkLkTpO1rZLmfjympUVOQSn%2FznAR%2F%2FrUum71aKBOS01QOFXBQ%3D%3D
     
     //--------------------------------------------------------------------
     {
     "etas": [
     {
     "count": 1,
     "eta": 18,
     "service_type": "regular"
     }
     ]
     }
     */
    //-----------------------------------------------------------------------------------
    
    //can be passed in Authorization header but never got it to work
    let kHAILO_CLIENT_ID:String = "cDB21dEEc5S5dUZfokUgXeL5CjSGA+o9bkiM5bC3FSndVihz5S4OkRb9RO0hI5YQ4B2FxGrTipmEsfFW/zYb4CXk2dGoK5TDbaAKTI6b+gIOnoywEVUeJm8/2R78iarX3iqw5e3ruSAVDoMErmw/vKbVrFodQMtjtB+jrAkLkTpO1rZLmfjympUVOQSn/znAR//rUum71aKBOS01QOFXBQ=="
    
    //USE IN API
    //api_token= kUBER_CLIENT_ID_ENCODED
    let kHAILO_CLIENT_ID_ENCODED:String = "cDB21dEEc5S5dUZfokUgXeL5CjSGA%2Bo9bkiM5bC3FSndVihz5S4OkRb9RO0hI5YQ4B2FxGrTipmEsfFW%2FzYb4CXk2dGoK5TDbaAKTI6b%2BgIOnoywEVUeJm8%2F2R78iarX3iqw5e3ruSAVDoMErmw%2FvKbVrFodQMtjtB%2BjrAkLkTpO1rZLmfjympUVOQSn%2FznAR%2F%2FrUum71aKBOS01QOFXBQ%3D%3D"
    
    //-----------------------------------------------------------------------------------
    
    init(){
        //https://itunes.apple.com/gb/app/hailo-taxi-app.-book-personal/id468420446?mt=8
        super.init(openWithType : OpenWithType.openWithType_Hailo,
                   appSchemeString     : "hailoapp://",
                   appDeepLinkURLString: "",
                   appWebsiteURLString : "https://hailo.com",
                   appStoreIdUInt      : 468420446)
    }
    
    
    
    override func open( _ presentingViewController: UIViewController,
                        clkPlaceStart: CLKPlace,
                        clkPlaceEnd: CLKPlace,
                        openWithTravelMode: OpenWithTravelMode,
                        success: () -> Void,
                        failure: (_ errorMessage : String) -> Void){
        logger.debug("OpenAppConfigDeepLinkingHailo")
        
        self.openTripInHailo(clkPlaceStart: clkPlaceStart, clkPlaceEnd: clkPlaceEnd, presentingViewController:presentingViewController)
        
    }
    
    func openTripInHailo(clkPlaceStart: CLKPlace?,
                                      clkPlaceEnd: CLKPlace?,
                                      presentingViewController: UIViewController)
    {
        var hailoPlacePickup : HailoPlace?
        var hailoPlaceDestination : HailoPlace?
        
        //------------------------------------------------------------------------------------------------
        //START
        //------------------------------------------------------------------------------------------------
        if let clkPlaceStart = clkPlaceStart{
            hailoPlacePickup = HailoPlace.convert_CLKPlace_To_HailoPlace(clkPlaceStart, hailoPlaceType : .Pickup)
            
        }else{
            logger.error("clkPlaceStart is nil")
        }
        //------------------------------------------------------------------------------------------------
        //Dropoff
        //------------------------------------------------------------------------------------------------
        
        if let clkPlaceEnd = clkPlaceEnd{
            hailoPlaceDestination = HailoPlace.convert_CLKPlace_To_HailoPlace(clkPlaceEnd, hailoPlaceType : .Destination)
        }else{
            logger.error("self.clkPlaceSearchResponse_End is nil")
        }
    
        
        //------------------------------------------------------------------------------------------------
        //OPEN in Hailo
        //------------------------------------------------------------------------------------------------
        
        if let hailoPlacePickup_ = hailoPlacePickup{
            if let hailoPlaceDestination_ = hailoPlaceDestination{
                //-----------------------------------------------------------------------------------
                self.appDeepLinkURLString = self.deepLinkingURLString(hailoPlacePickup: hailoPlacePickup_, hailoPlaceDestination: hailoPlaceDestination_)
                //-----------------------------------------------------------------------------------
                if appDeepLinkURLString == ""{
                    logger.error("error: appDeepLinkURLString is ''")
                    
                }else{
                    //-----------------------------------------------------------------------------------
                    //Deep Link url not blank
                    //-----------------------------------------------------------------------------------
                    self.canOpenInOtherApp(presentingViewController,
                                           canOpenInOtherApp_success:{() -> Void in
                                            //---------------------------------------------------------------------
                                            //App is installed - open the app using deeplinking
                                            //---------------------------------------------------------------------
//                                            if let urlStringToOpen = self.urlStringToOpen {
//                                                
//                                                if let url = NSURL(string:urlStringToOpen){
//                                                    UIApplication.sharedApplication().openURL(url)
//                                                    
//                                                }else{
//                                                    logger.error("url is nil - not valid:")
//                                                    //failure(errorMessage: "appDeepLinkURLString is invalid")
//                                                }
//                                            }else{
//                                                logger.error("self.urlStringToOpen is nil - should be set by subclass")
//                                            }
                                            
                                            //---------------------------------------------------------------
                                            //Open the app
                                            //---------------------------------------------------------------
                                            self.openAppByOpenURL()
                                            //---------------------------------------------------------------
                                            
                        },
                                           failure:{(errMsg) -> Void in
                                            logger.debug("errMsg:\(errMsg)")
                                            
                        }
                    )
                    //-----------------------------------------------------------------------------------
                    
                }
                //-----------------------------------------------------------------------------------
            }else{
                logger.error("hailoPlaceDropoff is nil")
            }
        }else{
            logger.error("hailoPlacePickup is nil 3332")
        }
    }
    
    
    //------------------------------------------------------------------------------------------------
    // MARK: -
    // MARK: - Deep Linking
    //------------------------------------------------------------------------------------------------
    //hailoapp://confirm?pickupCoordinate=51.511807,-0.117389&pickupAddress=Somerset%20House,%20Strand&destinationCoordinate=51.514996,-0.098970&destinationAddress=London%20Stock%20Exchange&pickupLocality=London&destinationLocality=London&referrer=your_hailo_application_token"];
    //--------------------------------------------------------------
    //    hailoapp://confirm?pickupCoordinate=51.511807,-0.117389
    //    &pickupAddress=Somerset%20House,%20Strand
    //    &destinationCoordinate=51.514996,-0.098970
    //    &destinationAddress=London%20Stock%20Exchange
    //    &pickupLocality=London
    //    &destinationLocality=London
    //    &referrer=your_hailo_application_token
    //------------------------------------------------------------------------------------------------
    
    func deepLinkingURLString(hailoPlacePickup:HailoPlace, hailoPlaceDestination:HailoPlace) -> String{
        
        var urlString = ""
        
        if self.appSchemeString == "" {
            logger.error("appSchemeString is ''")
        }else{
            //urlString = urlString + "hailoapp://confirm?"
            urlString = urlString + "\(self.appSchemeString)confirm?"
            
            urlString = urlString + hailoPlacePickup.generateParamsForHailoPlace("pickup")
            urlString = urlString + hailoPlaceDestination.generateParamsForHailoPlace("destination")
            
            urlString = urlString + "&referrer=\(kHAILO_CLIENT_ID_ENCODED)"
            
            //---------------------------------------------------------------------
            //DEBUG
            //---------------------------------------------------------------------
            logger.debug("urlString:\(urlString)")
            //DEBUG
            let lines = urlString.components(separatedBy: "&")
            for line in lines{
                logger.debug("\(line)")
            }
            //---------------------------------------------------------------------
        }
        
        return urlString
    }
    
}
