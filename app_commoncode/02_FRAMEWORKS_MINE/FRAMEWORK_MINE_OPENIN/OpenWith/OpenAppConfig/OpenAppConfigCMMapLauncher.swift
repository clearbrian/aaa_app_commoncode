//
//  OpenAppConfigCMMapLauncher.swift
//  joyride
//
//  Created by Brian Clear on 27/05/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit
class OpenAppConfigCMMapLauncher: OpenAppConfigOtherApp{
    
    
    var cmMapApp: CMMapApp
  
    //used in OpenAppConfigOtherApp.open()
    override var urlStringToOpen: String?{
        logger.error("SUBCLASS SHOULD SET urlStringToOpen")
        return nil
    }
    
    //------------------------------------------------------------------------------------------------
    //init
    //------------------------------------------------------------------------------------------------
    init(  openWithType:OpenWithType,
           appSchemeString: String,
           appWebsiteURLString: String,
           appStoreIdUInt: UInt,
           cmMapApp: CMMapApp)
    {
        self.cmMapApp = cmMapApp
        
        super.init(openWithType: openWithType,
                   appSchemeString: appSchemeString,
                   appWebsiteURLString: appWebsiteURLString,
                   appStoreIdUInt: appStoreIdUInt,
                   openWithSource:.OpenWithSource_CMMapLauncher
                   )
    }
    
    
    var colcPlaceAppLauncher = COLCPlaceAppLauncher()
    
    override func open(_ presentingViewController: UIViewController,
                       clkPlaceStart: CLKPlace,
                       clkPlaceEnd: CLKPlace,
                       openWithTravelMode: OpenWithTravelMode,
                       success: () -> Void,
                       failure: (_ errorMessage : String) -> Void){
        
        //CMMMapLauncher doesnt handled app not installed so I use the one we have
        self.canOpenInOtherApp(presentingViewController,
                               canOpenInOtherApp_success:{() -> Void in
                                //---------------------------------------------------------------------
                                //App is installed - open the app using deeplinking
                                //---------------------------------------------------------------------
           
                                self.colcPlaceAppLauncher.openLocationsInApp(self.cmMapApp,
                                    clkPlaceStart: clkPlaceStart,
                                    clkPlaceEnd: clkPlaceEnd,
                                    openWithTravelMode: openWithTravelMode,
                                    success:{() -> Void in
                                        //
                                        
                                    },
                                    failure:{(errMsg) -> Void in
                                        CLKAlertController.showAlertInVC(presentingViewController, title: "Error opening application", message: errMsg)
                                        
                                    },
                                    failureNotInstalled:{() -> Void in
                                        CLKAlertController.showAlertInVC(presentingViewController, title: "Application is not installed", message: "open in Apple Maps?")
                                        
                                })
                                
                                //---------------------------------------------------------------
                                
            },
                               failure:{(errMsg) -> Void in
                                logger.debug("errMsg:\(errMsg)")
                                
            }
        )
        
        
    }
    
   
}
