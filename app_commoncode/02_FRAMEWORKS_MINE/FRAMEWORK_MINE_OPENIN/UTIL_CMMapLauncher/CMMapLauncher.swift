//
//  CMMapLauncher.swift
//  joyride
//
//  Created by Brian Clear on 16/10/2015.
//  Copyright © 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import MapKit

//https://github.com/citymapper/CMMapLauncher
//typedef NS_ENUM(NSUInteger, CMMapApp) {
//    CMMapAppAppleMaps = 0,  // Preinstalled Apple Maps
//    CMMapAppCitymapper,     // Citymapper
//    CMMapAppGoogleMaps,     // Standalone Google Maps App
//    CMMapAppNavigon,        // Navigon
//    CMMapAppTheTransitApp,  // The Transit App
//    CMMapAppWaze,           // Waze
//    CMMapAppYandex,         // Yandex Navigator
//};


enum CMMapApp{
    case cmMapAppAppleMaps  // Preinstalled Apple Maps
    case cmMapAppCitymapper // Citymapper
    case cmMapAppGoogleMaps // Standalone Google Maps App
    case cmMapAppNavigon    // Navigon
    case cmMapAppTheTransitApp  // The Transit App
    case cmMapAppWaze           // Waze
    case cmMapAppYandex         // Yandex Navigator
//    case CMMapAppAddissonLee         // AddissonLee - changed to Clipboard launch in
    
    // TODO: - add here-route
    //https://github.com/Parklyapp/MapLauncher/blob/master/Source/Maps.swift
}

class CMMapLauncher {
    
    
    
    class func urlPrefixForMapApp(_ mapApp: CMMapApp) -> String {
        
        switch mapApp {
        case .cmMapAppAppleMaps:
            return "maps://"            /*BC may not be valid*/
        case .cmMapAppCitymapper:
            return "citymapper://"
        case .cmMapAppGoogleMaps:
            return "comgooglemaps://"
        case .cmMapAppNavigon:
            return "navigon://"
        case .cmMapAppTheTransitApp:
            return "transit://"
        case .cmMapAppWaze:
            return "waze://"
        case .cmMapAppYandex:
            return "yandexnavi://"
//        case .CMMapAppAddissonLee:
            //return "addissonlee://"         //NOT WORKING
//            return "addlee://"         //jun 2016
            
            
            // TODO: - add here-route
            //https://github.com/Parklyapp/MapLauncher/blob/master/Source/Maps.swift
            
//        default:
//            return nil
        }
    }
    
    
    class func urlEncode(_ queryParam: String) -> String {
//        var newString: String = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, queryParam, nil, "!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8)
        //        if queryParamwithPercentEscapes {
        //            return queryParamwithPercentEscapes
        //        }
        
        //if anoy probs return the original string
        var queryParamReturned = queryParam
        
        if let queryParamwithPercentEscapes = queryParam.addingPercentEncoding( withAllowedCharacters: CharacterSet.urlQueryAllowed)
        {
            queryParamReturned = queryParamwithPercentEscapes
            
        }else{
            logger.error("stringByAddingPercentEncodingWithAllowedCharacters is nil")
        }

        return queryParamReturned
    }
    
    class func googleMapsStringForMapPoint(_ mapPoint: CMMapPoint) -> String {
        
        var stringReturned = ""
        if let coordinate = mapPoint.coordinate{
            
            if mapPoint.isCurrentLocation && coordinate.latitude == 0.0 && coordinate.longitude == 0.0 {
                stringReturned = ""
            }
            else if mapPoint.name != "" {
                //OLD - lat lng appended out side this
                //stringReturned = "\(coordinate.latitude),\(coordinate.longitude)+(\(CMMapLauncher.urlEncode(mapPoint.name)))"
                stringReturned = "\(CMMapLauncher.urlEncode(mapPoint.name))"
            }
            else{
                stringReturned = "\(coordinate.latitude),\(coordinate.longitude)"
            }
        }else{
            logger.error("mapPoint.coordinate is nil")
        }

        return stringReturned
        
    }
    
    class func isMapAppInstalled(_ mapApp: CMMapApp) -> Bool {
        
        if mapApp == CMMapApp.cmMapAppAppleMaps {
            return true
        }
        let urlPrefix: String = CMMapLauncher.urlPrefixForMapApp(mapApp)
        logger.debug("urlPrefix:\(urlPrefix)")
        var canOpenURL = false
        
        if let url = URL(string: urlPrefix){
            canOpenURL = UIApplication.shared.canOpenURL(url)
        }else{
            logger.error("NSURL(string: urlPrefix) is nil")
        }
        return canOpenURL
        
    }
    
    class func launchMapApp(_ mapApp: CMMapApp, forDirectionsTo end: CMMapPoint) -> Bool {
        
        
//        CMMapLauncher.launchMapApp(mapApp, forDirectionsFrom: CMMapPoint.currentLocation(), to: end)
        
        
        return false
    }
    open class func testopenMaps(){
        
        let arrayMK = [MKMapItem]()
        
        _ = self.openMaps(with: arrayMK, launchOptions: nil)
    }
    open class func openMaps(with mapItems: [MKMapItem], launchOptions: [String : Any]? = nil) -> Bool{
        let functionName: String = #function
        print("functionName:'\(functionName)'")
        return true
    }
    
    
    class func launchMapApp(_ mapApp: CMMapApp,
           forDirectionsFrom start: CMMapPoint,
                            to end: CMMapPoint,
                            openWithTravelMode: OpenWithTravelMode,
                           success: () -> Void,
                           failure: (_ errorMessage : String) -> Void,
               failureNotInstalled: () -> Void
    )
    {
        //---------------------------------------------------------------------
        var isLaunched = false
        //---------------------------------------------------------------------
        
        if CMMapLauncher.isMapAppInstalled(mapApp) {
            //---------------------------------------------------------------------
            //MAP APP IS INSTALLED
            //---------------------------------------------------------------------
            switch mapApp {
            case .cmMapAppAppleMaps:
                //------------------------------------------------------------------------------------------------
                // TODO: - SWIFT3 bad conversion?
                //SELECTOR'openMaps(with:launchOptions:)'
                
                //if MKMapItem.responds(to: Selector("openMaps:launchOptions:")){
                if MKMapItem.responds(to: #selector(MKMapItem.openMaps(with:launchOptions:)))
                {
                    //default to driving
                     var launchOptions: Dictionary = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
                  
                    if openWithTravelMode == .walking{
                        launchOptions[MKLaunchOptionsDirectionsModeKey] = MKLaunchOptionsDirectionsModeWalking
                    }
                   
                    
                    if let startMapItem = start.mapItemForCMMapPoint(){
                        if let endMapItem = end.mapItemForCMMapPoint(){
                            //If end not set will use current location so will not work on simulator unless both set
                            //------------------------------------------------------------------------------------------------
                            isLaunched = MKMapItem.openMaps(with: [startMapItem, endMapItem], launchOptions: launchOptions)
                            //------------------------------------------------------------------------------------------------
                            
                        }else{
                            print("ERROR:end.mapItemForCMMapPoint() is nil")
                        }
                    }else{
                        print("ERROR:start.mapItemForCMMapPoint() is nil")
                    }
                    
                }
                else {
                    //OPEN IN GOOGLE MAPS ON WEB
                    
                    let urlString: String = "http://maps.google.com/maps?saddr=\(CMMapLauncher.googleMapsStringForMapPoint(start))&daddr=\(CMMapLauncher.googleMapsStringForMapPoint(end))"
                    
                    isLaunched = self.openURLString(urlString)
                    
                }
                //------------------------------------------------------------------------------------------------

                

            case .cmMapAppCitymapper:
                //------------------------------------------------------------------------------------------------
                var params: [String] = [String]()
                
                if !start.isCurrentLocation {
                    if let coordinate = start.coordinate{
                        params.append("startcoord=\(coordinate.latitude),\(coordinate.longitude)")
                    }else{
                        print("ERROR:start.coordinate is nil")
                    }
                    //---------------------------------------------------------------------
                    params.append("startname=\(CMMapLauncher.urlEncode(start.name))")
                    //---------------------------------------------------------------------
                    params.append("startaddress=\(CMMapLauncher.urlEncode(start.address))")
                    
                }
                
                if !end.isCurrentLocation {
                    if let coordinate = end.coordinate{
                        params.append("endcoord=\(coordinate.latitude),\(coordinate.longitude)")
                    }else{
                        print("ERROR:end.coordinate is nil")
                    }
                    //---------------------------------------------------------------------
                    params.append("endname=\(CMMapLauncher.urlEncode(end.name))")
                    //---------------------------------------------------------------------
                    params.append("endaddress=\(CMMapLauncher.urlEncode(end.address))")
                    
                }
                
                let paramsString = params.joined(separator: "&")
                let urlString: String = "citymapper://directions?\(paramsString)"
                
                isLaunched = self.openURLString(urlString)
                //------------------------------------------------------------------------------------------------

    
            case .cmMapAppGoogleMaps:
                //https://developers.google.com/maps/documentation/ios-sdk/urlscheme
                //------------------------------------------------------------------------------------------------

                //comgooglemaps://?saddr=51.4934565,-0.1466567+(VICTORIA)&daddr=51.5043109,-0.0208244+(ISLE%20OF%20DOGS)
                
                //comgooglemapsurl://maps.google.com/maps?f=d&daddr=Tokyo+Tower,+Tokyo,+Japan&sll=35.6586,139.7454&sspn=0.2,0.1&nav=1
                //&x-source=SourceApp
                //&x-success=sourceapp://?resume=true

                
                //OK
//                var urlString: String = "https://www.google.co.uk/maps/dir/The+Box+Works,+4+Worsley+St,+Manchester+M15+4NU/Manchester+Piccadilly,+Manchester/@53.4748856,-2.258252,15z/data=!3m1!4b1!4m13!4m12!1m5!1m1!1s0x487bb1e1a02217cf:0xfe2f77240df3d83b!2m2!1d-2.2609275!2d53.4733493!1m5!1m1!1s0x487a52b13f7d5d27:0xf0c99a43ee1b2864!2m2!1d-2.2309325!2d53.4774029"

                //DOESNT WORK
//                var urlString: String = "comgooglemaps://www.google.co.uk/maps/dir/The+Box+Works,+4+Worsley+St,+Manchester+M15+4NU/Manchester+Piccadilly,+Manchester/@53.4748856,-2.258252,15z/data=!3m1!4b1!4m13!4m12!1m5!1m1!1s0x487bb1e1a02217cf:0xfe2f77240df3d83b!2m2!1d-2.2609275!2d53.4733493!1m5!1m1!1s0x487a52b13f7d5d27:0xf0c99a43ee1b2864!2m2!1d-2.2309325!2d53.4774029"
                
                //OK
//                var urlString: String = "comgooglemaps://?saddr=Google,+1600+Amphitheatre+Parkway,+Mountain+View,+CA+94043&daddr=Google+Inc,+345+Spear+Street,+San+Francisco,+CA&center=37.422185,-122.083898&zoom=10"
                
                //OK
//                var urlString: String = "comgooglemaps://?saddr=The+Box+Works,+4+Worsley+St,+Manchester+M15+4NU&daddr=Manchester+Piccadilly,+Manchester&center=53.4748856,-2.258252&zoom=10"
                
                //---------------------------------------------------------------------
                //OLDvar urlString: String = "comgooglemaps://?saddr=\(CMMapLauncher.googleMapsStringForMapPoint(start))&daddr=\(CMMapLauncher.googleMapsStringForMapPoint(end))"
//                if let coordinate = start.coordinate {
//                    //let urlString: String = "comgooglemaps://?saddr=\(CMMapLauncher.googleMapsStringForMapPoint(start))&daddr=\(CMMapLauncher.googleMapsStringForMapPoint(end))&center=\(coordinate.latitude),\(coordinate.longitude)&zoom=10"
//                    
//                    let urlString: String = "comgooglemaps://?daddr=\(CMMapLauncher.googleMapsStringForMapPoint(end))&center=\(coordinate.latitude),\(coordinate.longitude)&zoom=10"
//                    
//                    
//                    print("urlString:\(urlString)")
//                    isLaunched = self.openURLString(urlString)
//                }else{
//                    print("ERROR:start.coordinate is nil")
//                }
                //----------------------------------------------------------------------------------------
                if let coordinate = end.coordinate {
                    //google maps acting weird - just open dest
                    
                    var urlString: String = "comgooglemaps://?daddr=\(CMMapLauncher.googleMapsStringForMapPoint(end))&center=\(coordinate.latitude),\(coordinate.longitude)&zoom=10"
                    
                    //----------------------------------------------------------------------------------------
                    //directionsmode: Method of transportation. Can be set to: driving, transit, bicycling or walking.
                    //----------------------------------------------------------------------------------------
                    if openWithTravelMode == .driving{
                       urlString = "\(urlString)&directionsmode=driving"
                    }
                    else if openWithTravelMode == .transit{
                        urlString = "\(urlString)&directionsmode=transit"
                    }
                    else if openWithTravelMode == .walking{
                        urlString = "\(urlString)&directionsmode=walking"
                    }
                    else if openWithTravelMode == .bicycling{
                        urlString = "\(urlString)&directionsmode=bicycling"
                    }
                    else {
                        logger.error("UNHANDLED TRANSIT TYPE")
                    }
                    //----------------------------------------------------------------------------------------
                    
                    
                    print("urlString:\(urlString)")
                    isLaunched = self.openURLString(urlString)
                }else{
                    print("ERROR:start.coordinate is nil")
                }

                //------------------------------------------------------------------------------------------------
       
            case .cmMapAppNavigon:
                //------------------------------------------------------------------------------------------------
                var name: String = "Destination"
                if end.name != ""{
                    name = end.name
                }
                if let end_coordinate = end.coordinate{
                    
                    let urlString: String = "navigon://coordinate/\(CMMapLauncher.urlEncode(name))/\(end_coordinate.longitude)/\(end_coordinate.latitude)"
                    
                    isLaunched = self.openURLString(urlString)
                }else{
                    print("ERROR: end.coordinateis nil")
                }
                //------------------------------------------------------------------------------------------------

    
            case .cmMapAppTheTransitApp:
                //------------------------------------------------------------------------------------------------
                var params: [String] = [String]()
                
                if !start.isCurrentLocation {
                    if let coordinate = start.coordinate{
                        params.append("from=\(coordinate.latitude),\(coordinate.longitude)")
                    }else{
                        print("ERROR: start.coordinate is nil")
                    }
                }
                
                if !end.isCurrentLocation {
                    if let coordinate = end.coordinate{
                        params.append("to=\(coordinate.latitude),\(coordinate.longitude)")
                    }else{
                        print("ERROR: end.coordinate is nil")
                    }
                }
                
                let paramsString = params.joined(separator: "&")
                let urlString: String = "transit://directions?\(paramsString)"
                
                isLaunched = self.openURLString(urlString)
                //------------------------------------------------------------------------------------------------
      
            case .cmMapAppWaze:
                //------------------------------------------------------------------------------------------------
                if let end_coordinate = end.coordinate{
                    
                    let urlString = "waze://?ll=\(end_coordinate.latitude),\(end_coordinate.longitude)&navigate=yes"
                    
                    isLaunched = self.openURLString(urlString)
                }else{
                    print("ERROR: end.coordinate is nil")
                }
                //------------------------------------------------------------------------------------------------

  
            case .cmMapAppYandex:
                //------------------------------------------------------------------------------------------------
                var urlString: String? = nil
                
                if let start_coordinate = start.coordinate{
                    if let end_coordinate = end.coordinate{
                        
                        if start.isCurrentLocation {
                            urlString = "yandexnavi://build_route_on_map?lat_to=\(end_coordinate.latitude)&lon_to=\(end_coordinate.longitude)"
                        }
                        else {
                            urlString = "yandexnavi://build_route_on_map?lat_to=\(end_coordinate.latitude)&lon_to=\(end_coordinate.longitude)&lat_from=\(start_coordinate.latitude)&lon_from=\(start_coordinate.longitude)"
                        }
                        isLaunched = self.openURLString(urlString)
                        
                    }else{
                        print("ERROR: end.coordinate is nil")
                    }
                    
                }else{
                    print("ERROR: start.coordinate is nil")
                }
                //------------------------------------------------------------------------------------------------
//Removed from CMMap as it had no params
//            case .CMMapAppAddissonLee:
//                // TODO: - LOAD FROM enum
////                let urlString = "addlee://"
//                let urlString = self.urlPrefixForMapApp(.CMMapAppAddissonLee)
//                isLaunched = self.openURLString(urlString)
                //------------------------------------------------------------------------------------------------

//        default:
//            return nil
            }
            
            
            if isLaunched{
                success()
            }else{
                failure("unknown error")
            }
        }else{
            //APP NOT INSTALLED
           isLaunched  = false
            
            print("ERROR: APP NOT INSTALLED")
    
            failureNotInstalled()
        }
    }
    
    //OPTIONAL VERSION
    class func openURLString(_ urlString: String?) -> Bool{
        var isLaunched = false
        if let urlString = urlString{
            
            isLaunched = self.openURLString(urlString)
            
            
        }else{
            print("ERROR: urlString is nil")
        }
        return isLaunched
    }
    class func openURLString(_ urlString: String) -> Bool{
        
        var isLaunched = false
        if let url = URL(string: urlString){
            isLaunched = UIApplication.shared.openURL(url)
        }else{
            print("ERROR: NSURL(string: urlPrefix) is nil")
        }
        return isLaunched
    }
}

