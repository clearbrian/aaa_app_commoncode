//
//  TestGoogleDirectionsViewController.swift
//  joyride
//
//  Created by Brian Clear on 22/01/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps

class TestGoogleDirectionsViewController: ParentViewController {
    
    @IBOutlet weak var gmsMapView: GMSMapView!
    
    
    //BC added 22mar it locally - too many managers in AppD
    let googleDirectionsController = GoogleDirectionsController()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
    }
    
    @IBAction func buttonTestGoogleDirections_Action(_ sender: AnyObject) {
        callWS_GoogleDirections()
        
    }
    
    var googleDirectionsSearchRunning = false
    func callWS_GoogleDirections()
    {
        //UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        if self.googleDirectionsSearchRunning {
            logger.error("googleDirectionsSearchRunning is TRUE - dont run again")
        }else{
            googleDirectionsSearchRunning = true
            
            
            //------------------------------------------------------------------------------------------------
            //CLKGoogleDirectionsRequest
            //------------------------------------------------------------------------------------------------
            
            
            //------------------------------------------------------------------------------------------------
            //ADDRESS TO ADDRESS
            //------------------------------------------------------------------------------------------------

            //self.getDirections(addressStringFrom: "London", addressStringTo: "Liverpool")
            //self.getDirections(addressStringFrom: "E1 8LG", addressStringTo: "Liverpool")
            //self.getDirections(addressStringFrom: "E1 8LG", addressStringTo: "New York") //ZERO_RESULTS
            
            
            //------------------------------------------------------------------------------------------------
            //PLACEID TO PLACEID
            //------------------------------------------------------------------------------------------------
            //LONDON TO CORK
            //self.getDirections(placeIdFrom: "ChIJdd4hrwug2EcRmSrV3Vo6llI", placeIdTo: "ChIJYbm-kQiQREgR0MUxl6nHAAo")
            //------------------------------------------------------------------------------------------------
            //LATLNG to LATLNG
            //------------------------------------------------------------------------------------------------

            //50.7195341,-3.538888
            //50.8011214,-3.4370039
            //OK
            //self.getDirections( latFrom: NSNumber(double: 50.7195341), lngFrom: NSNumber(double:-3.538888),
            //                  latTo: NSNumber(double: 50.8011214), lngTo: NSNumber(double: -3.4370039))
            
            //OK - TUPLE VERSION
            //self.getDirections(latFrom:(50.7195341, -3.538888),
            //                     latTo:(50.8011214, -3.4370039))
            //---------------------------------------------------------------------
            //EAST LONDON /EALING - checkign traffic
            self.getDirections(latFrom:(51.5119723,-0.3730202),
                                 latTo:(51.4058183,-0.0103344))
        }
    }
    
    func getDirections(
        addressStringFrom: String,
        addressStringTo: String)
    {
        //---------------------------------------------------------------------
        //ADDRESS TO ADDRESSS
        if let clkGoogleDirectionsRequest:CLKGoogleDirectionsRequest
            = CLKGoogleDirectionsRequest(addressStringFrom: addressStringFrom, addressStringTo: addressStringTo)
        {
            
            self.callCLKGoogleDirectionsRequest(clkGoogleDirectionsRequest)
            
        }else{
            logger.error("CLKGoogleDirectionsLocation init failed")
        }
        //---------------------------------------------------------------------
    }
    
    func getDirections(
        placeIdFrom: String,
        placeIdTo: String)
    {
        //---------------------------------------------------------------------
        //ADDRESS TO ADDRESSS
        if let clkGoogleDirectionsRequest:CLKGoogleDirectionsRequest
            = CLKGoogleDirectionsRequest(placeIdFrom: placeIdFrom, placeIdTo: placeIdTo)
        {
            self.callCLKGoogleDirectionsRequest(clkGoogleDirectionsRequest)
            
        }else{
            logger.error("CLKGoogleDirectionsLocation init failed")
        }
        //---------------------------------------------------------------------
    }
    
    func getDirections(latFrom:(lat:Double,lng:Double), latTo:(lat:Double,lng:Double))
    {
        self.getDirections( latFrom: NSNumber(value: latFrom.lat),
                            lngFrom: NSNumber(value: latFrom.lng),
                              latTo: NSNumber(value: latTo.lat),
                              lngTo: NSNumber(value: latTo.lng))
    }
    
    
    
    func getDirections(
                  latFrom: NSNumber, lngFrom: NSNumber,
                  latTo: NSNumber, lngTo: NSNumber)
    {
        //---------------------------------------------------------------------
        //ADDRESS TO ADDRESSS
        if let clkGoogleDirectionsRequest:CLKGoogleDirectionsRequest = CLKGoogleDirectionsRequest(latFrom: latFrom,
                                                                                                  lngFrom: lngFrom,
                                                                                                  latTo: latTo,
                                                                                                  lngTo:lngTo)
        {
            self.callCLKGoogleDirectionsRequest(clkGoogleDirectionsRequest)
            
        }else{
            logger.error("CLKGoogleDirectionsLocation init failed")
        }
        //---------------------------------------------------------------------
    }
    
    
    func callCLKGoogleDirectionsRequest(_ clkGoogleDirectionsRequest : CLKGoogleDirectionsRequest){
        //------------------------------------------------------------------------------------------------
        //mar 22 was appd.googleDirectionsController
        self.googleDirectionsController.findGoogleDirections(clkGoogleDirectionsRequest,
            success:{(clkGoogleDirectionsResponse: CLKGoogleDirectionsResponse?) -> Void in
                
                //---------------------------------------------------------------------
                if let clkGoogleDirectionsResponse = clkGoogleDirectionsResponse {
                    self.handleCLKGoogleDirectionsResponse(clkGoogleDirectionsResponse)
                }else{
                    logger.error("clkGoogleDirectionsResponse is nil")
                }
                //---------------------------------------------------------------------
                //UIApplication.shared.isNetworkActivityIndicatorVisible = false
                //---------------------------------------------------------------------
                self.googleDirectionsSearchRunning = false
            },
            failure:{(error) -> Void in
                logger.error("error:\(String(describing: error))")
                if let nserror = error {
                    self.handleError(nserror)
                    
                }else{
                    logger.error("error is nil")
                }
                
                self.googleDirectionsSearchRunning = false
                //UIApplication.shared.isNetworkActivityIndicatorVisible = false
                self.googleDirectionsSearchRunning = false
            }
        )
        
        //------------------------------------------------------------------------------------------------
    }
    
    func handleCLKGoogleDirectionsResponse(_ clkGoogleDirectionsResponse: CLKGoogleDirectionsResponse){
        if let routes = clkGoogleDirectionsResponse.routes{
            for clkGoogleDirectionRoute : CLKGoogleDirectionRoute in routes{
                //---------------------------------------------------------------------
                //ROUTE
                //---------------------------------------------------------------------
                
                //---------------------------------------------------------------------
                //POLYLINE
                //---------------------------------------------------------------------
                if let overview_polyline: CLKGooglePolyline = clkGoogleDirectionRoute.overview_polyline{
                    if let polyline:Polyline = overview_polyline.polyline {
                        
                        if let coordinates : [CLLocationCoordinate2D] = polyline.coordinates {
                            print("\(coordinates.count)")
                            
                            let path = GMSMutablePath()
                            
                            for location : CLLocationCoordinate2D in coordinates {
                                path.add(location)
                            }
                            let polyline = GMSPolyline(path: path)
                            polyline.strokeWidth = 5.0
                            polyline.strokeColor = AppearanceManager.appColorNavbarAndTabBar
                            polyline.map = self.gmsMapView
                            
                        }else{
                            logger.error("polyline.coordinates is nil")
                        }
                    }else{
                        logger.error("overview_polyline.polyline is nil")
                    }
                }else{
                    logger.error("clkGooglePlaceResult.name is nil")
                }
                //---------------------------------------------------------------------
                //Route Bounds - zoom map
                //---------------------------------------------------------------------
                if let bounds: CLKGooglePlaceViewport = clkGoogleDirectionRoute.bounds{
                    if let bounds_gmsCoordinateBounds = bounds.gmsCoordinateBounds {
                        
                        if let gmsCameraPosition = self.gmsMapView.camera(for: bounds_gmsCoordinateBounds,
                                                                          insets: UIEdgeInsets(top: 50, left: 50, bottom: 50, right: 50)){
                             self.gmsMapView.animate(to: gmsCameraPosition)
                        }else{
                            logger.error("gmsCameraPosition is nil")
                        }
                    }else{
                        logger.error("bounds.gmsCoordinateBounds is nil")
                    }
                }else{
                    logger.error("clkGooglePlaceResult.name is nil")
                }
                //---------------------------------------------------------------------
            }//for
        }else{
            logger.error("clkGoogleDirectionsResponse.result is nil - OK if /api/directions use .results")
        }
    }
    
    func handleError(_ error: Error){
        var showErrorAlert = true
        
        //switch nserror.code{
        switch error{
            //SWIFT3
        //case AppError.googlePlacesResponseStatus_ZERO_RESULTS_DISTANCE.rawValue:
        case AppError.googlePlacesResponseStatus_ZERO_RESULTS_DISTANCE:
            logger.debug("status: ZERO_RESULTS")
            //------------------------------------------------------------------------------------------------
            //DONT SHOW ERROR ALERT FOR ZERO RESULTS
            showErrorAlert = false
            
            //                        //remove all results
            //                        self.nearbySearch_CLKGooglePlaceResultsArray = [CLKGooglePlaceResult]()
            //                        self.tableViewPlacesResults.reloadData()
            //
            //                        //------------------------------------------------------------------------------------------------
            //                        self.labelAddress.text = ""
            //                        //------------------------------------------------------------------------------------------------
            
            //------------------------------------------------------------------------------------------------
            
        case AppError.googlePlacesResponseStatus_OVER_QUERY_LIMIT:
            logger.debug("status: OVER_QUERY_LIMIT")
            
        case AppError.googlePlacesResponseStatus_REQUEST_DENIED:
            logger.debug("status: REQUEST_DENIED")
            
        case AppError.googlePlacesResponseStatus_INVALID_REQUEST:
            
            logger.debug("status: INVALID_REQUEST")
            
        default:
            logger.error("UNKNOWN error: \(error)")
        }

        if showErrorAlert{
            CLKAlertController.showAlertInVCWithCallback(self, title: "Error", message: error.localizedDescription)
                { () -> Void in
                    logger.debug("Ok tapped = alert closed appDelegate.doLogout()")
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
