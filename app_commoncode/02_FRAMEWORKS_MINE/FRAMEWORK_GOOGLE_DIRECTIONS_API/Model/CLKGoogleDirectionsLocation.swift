//
//  CLKGoogleDirectionsLocation.swift
//  joyride
//
//  Created by Brian Clear on 25/01/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import CoreLocation

enum CLKGoogleDirectionsLocationType{
    case clkGoogleDirectionsLocationType_Address
    case clkGoogleDirectionsLocationType_PlaceId
    case clkGoogleDirectionsLocationType_LatLng
}

/*

origin — 
The address, textual latitude/longitude value, or place ID from which you wish to calculate directions.

If you pass an address as a string, the Directions service will geocode the string and convert it to a latitude/longitude coordinate to calculate directions. This coordinate may be different from that returned by the Google Maps Geocoding API, for example a building entrance rather than its center.

If you pass coordinates, they will be used unchanged to calculate directions. Ensure that no space exists between the latitude and longitude values.

Place IDs must be prefixed with place_id:. The place ID may only be specified if the request includes an API key or a Google Maps APIs Premium Plan client ID. You can retrieve place IDs from the Google Maps Geocoding API and the Google Places API (including Place Autocomplete). For an example using place IDs from Place Autocomplete, see Place Autocomplete and Directions. For more about place IDs, see the place ID overview.
destination — The address, textual latitude/longitude value, or place ID to which you wish to calculate directions. The options for the destination parameter are the same as for the origin parameter, described above.
*/
class CLKGoogleDirectionsLocation{
    
    
    
    var clkGoogleDirectionsLocationType : CLKGoogleDirectionsLocationType?
    
    //---------------------------------------------------------------------
    //REQ depending on type
    //---------------------------------------------------------------------
    
    //CLKGoogleDirectionsLocationType_Address
    var addressString : String?
    
    // CLKGoogleDirectionsLocationType_PlaceId
    var placeId : String?
    
    // CLKGoogleDirectionsLocationType_LatLng
    var lat : NSNumber?
    var lng : NSNumber?
    //---------------------------------------------------------------------
    
    required init?( addressString: String) {
        self.addressString = addressString
        self.clkGoogleDirectionsLocationType = .clkGoogleDirectionsLocationType_Address
        
        // super.init()
    }
    
    required init?( placeId: String) {
        self.placeId = placeId
        self.clkGoogleDirectionsLocationType = .clkGoogleDirectionsLocationType_PlaceId
        
        // super.init()
    }
    
    required init?(lat : NSNumber, lng : NSNumber) {
        self.lat = lat
        self.lng = lng
        
        self.clkGoogleDirectionsLocationType = .clkGoogleDirectionsLocationType_LatLng
        
        // super.init()
    }
    required init?(clLocation : CLLocation) {
        
        
        self.lat = NSNumber(value: clLocation.coordinate.latitude as Double)
        self.lng = NSNumber(value: clLocation.coordinate.longitude as Double)
        
        self.clkGoogleDirectionsLocationType = .clkGoogleDirectionsLocationType_LatLng
        
        // super.init()
    }
    
    
    var clkGoogleDirectionsLocationString : String?{
        get{
            var clkGoogleDirectionsLocationString : String?
            if let clkGoogleDirectionsLocationType = self.clkGoogleDirectionsLocationType {
                
                switch clkGoogleDirectionsLocationType{
                case .clkGoogleDirectionsLocationType_Address:
                    if let addressString = self.addressString {
                        clkGoogleDirectionsLocationString = addressString
                        
                    }else{
                        logger.error("addressString is nil")
                    }
                    
                    
                case .clkGoogleDirectionsLocationType_PlaceId:
                    //https:// maps.googleapis.com/maps/api/directions/json?origin=place_id:ChIJ685WIFYViEgRHlHvBbiD5nE&destination=place_id:ChIJA01I-8YVhkgRGJb0fW4UX7Y&key=YOUR_API_KEY
                    if let placeId = self.placeId {
                        clkGoogleDirectionsLocationString = "place_id:\(placeId)"
                        
                    }else{
                        logger.error("placeId is nil")
                    }
                    
                case .clkGoogleDirectionsLocationType_LatLng:
                    //OK:https://maps.googleapis.com/maps/api/directions/json?origin=17.863446657938162,100.26915904062152&destination=17.12999010224565,103.36730357187415&sensor=false&key=AIzaSyCO41p9UNH6K6gwZO-VHY6z8ibsdnquStw
                    if let lat = self.lat {
                        if let lng = self.lng {
                            clkGoogleDirectionsLocationString = "\(lat),\(lng)"
                            
                        }else{
                            logger.error("lat is nil")
                        }
                        
                    }else{
                        logger.error("lat is nil")
                    }
                }
            }else{
                logger.error("clkGoogleDirectionsLocationType is nil")
            }
            return clkGoogleDirectionsLocationString
        }
//        read only - set
//        set{
//            
//        }
    }
}
