//
//  CLKGooglePolyline.swift
//  joyride
//
//  Created by Brian Clear on 25/01/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
//import Polyline
class CLKGooglePolyline: ParentMappable {
    
    var points:String?
    
    // root calls mapping()
    //    required init?(_ map: Map){
    //        mapping(map: map)
    //    }
    
    override func mapping(map: Map){
        points <- map["points"]
    }
    
    
    //
    //        let encodedPolylineString1:String = "y~jyHx}Wn]jhAOr`AmJzKgk@`d@qFdRxCp{Agg@|x@}Jjt@dApiAfVv~@nLzxE_Lvq@e]~ZaZnk@wHlcAq[dyC_StbEst@l}Fc_@feCsFdoEm{@zsFoOp_E`Gj{C_Nf_Cg`@`aBaX`hAw[b_@wc@vl@}VbpAc_AnyE{FbpCbBnlAsQbbAaV|oBmb@lkAaBfpBsYtwBjVviDz@rrAob@n{Awk@lgBaJpjAcK|aDcU~jAqh@p`Amr@ri@uiAbmA_^rbDaG~|Byt@xhAk~@vbAa~Bz~CikBfsF}f@`bDq|@`kDelAtsA{n@hPq\\aDa`A_ZgdAeo@et@{m@yg@~Uw|BncByl@hZ}m@puAwdAblD}Ll~@mn@vzAwwA|cCuaBna@es@|Saq@s@edDaXu_CtLosAt|Ask@n_Dqi@l_B_oBbk@c{Avj@mhAbwA_qAjlA_~Ad[cyC|k@ex@|q@cyB~pEgdCpxCqqBx}Do}Az`B_bA`mAkoAvs@mv@nb@sc@dw@ucAbeBsoBt_FsyAdgE}xBrdG}C|nBgp@|cDu_ApvCwy@rt@ehApsCc~B`uDin@lcAk_@j|B{w@`gFmq@zuBkOv\\oZDir@gXwX}`@}a@uo@yn@u]uxAmqBcw@gmAc[}iBaw@etA}_C}`Au~@ka@}y@~EksB~Dg~@pEyyAlk@gdAzf@{pBtVonBvEknBpdEgc@~cAuj@`[qa@xdAqi@la@ys@nNoj@oBqp@xMgfBp^ct@fl@}j@`aBin@rtCDjeC`MjrBmu@v`CgQp|BlD~}BeFpxCce@j`DqUhfAz@~x@bMtaCp^vgBqYhkA}_BheDy{@jd@aoAcZclA|DybB{VktCn]ct@xx@izArhAow@lmCui@je@il@rDqeAdIwz@m{@{`@kFk^zTi`AbbAmzCnf@_q@pBcy@zg@oiAzcBuzAnx@mrAnpAsyA`d@wgCn]yk@r[{j@h\\iZryAkk@bbDgO`zDq`@zuB}\\be@ce@dSi{@}k@am@hFgh@j_@u_AnGo`DvXafB`s@ikAuAqkCuj@cvC~W{mAxfA}gDbgCsjAvtA{bBxM_hBjo@agBt`@_eBRedBo_@gv@uGkiA`b@_w@~o@{Qn|@}w@ptBql@|iDis@vwBqkBx}B}fAzb@ciAaTqs@}Hax@nPql@rf@yz@j{Bi}@j_BcQ|{AxKjgF~`@bvE~l@l`Mly@t`JmFj~CtX|cClEhkAsU`gA_^tcDaRnp@~BzrAlDdcBwEjq@wNduAdOh}FuZtGxErqArKzaAnAr]"
    //
    //        let polyline = Polyline(encodedPolyline: encodedPolylineString1)
    //        print("\(polyline.coordinates?.count)")
    
    
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - Computed from encoded string
    //--------------------------------------------------------------
    //android has PolyUtil but had to get
    //https://github.com/raphaelmor/Polyline
    //https://developers.google.com/maps/documentation/utilities/polylinealgorithm
    var polyline: Polyline?{
    
        //First this
        get {
            var polyline: Polyline?
            
            if let points = points{
                polyline = Polyline(encodedPolyline: points)
            }else{
                logger.error("error: CLKGooglePolyline.points string is nil - cant build PolyLine")
            }
            return polyline
        }
    }
    /*
    let encodedPolylineString1:String = "y~jyHx}Wn]jhAOr`AmJzKgk@`d@qFdRxCp{Agg@|x@}Jjt@dApiAfVv~@nLzxE_Lvq@e]~ZaZnk@wHlcAq[dyC_StbEst@l}Fc_@feCsFdoEm{@zsFoOp_E`Gj{C_Nf_Cg`@`aBaX`hAw[b_@wc@vl@}VbpAc_AnyE{FbpCbBnlAsQbbAaV|oBmb@lkAaBfpBsYtwBjVviDz@rrAob@n{Awk@lgBaJpjAcK|aDcU~jAqh@p`Amr@ri@uiAbmA_^rbDaG~|Byt@xhAk~@vbAa~Bz~CikBfsF}f@`bDq|@`kDelAtsA{n@hPq\\aDa`A_ZgdAeo@et@{m@yg@~Uw|BncByl@hZ}m@puAwdAblD}Ll~@mn@vzAwwA|cCuaBna@es@|Saq@s@edDaXu_CtLosAt|Ask@n_Dqi@l_B_oBbk@c{Avj@mhAbwA_qAjlA_~Ad[cyC|k@ex@|q@cyB~pEgdCpxCqqBx}Do}Az`B_bA`mAkoAvs@mv@nb@sc@dw@ucAbeBsoBt_FsyAdgE}xBrdG}C|nBgp@|cDu_ApvCwy@rt@ehApsCc~B`uDin@lcAk_@j|B{w@`gFmq@zuBkOv\\oZDir@gXwX}`@}a@uo@yn@u]uxAmqBcw@gmAc[}iBaw@etA}_C}`Au~@ka@}y@~EksB~Dg~@pEyyAlk@gdAzf@{pBtVonBvEknBpdEgc@~cAuj@`[qa@xdAqi@la@ys@nNoj@oBqp@xMgfBp^ct@fl@}j@`aBin@rtCDjeC`MjrBmu@v`CgQp|BlD~}BeFpxCce@j`DqUhfAz@~x@bMtaCp^vgBqYhkA}_BheDy{@jd@aoAcZclA|DybB{VktCn]ct@xx@izArhAow@lmCui@je@il@rDqeAdIwz@m{@{`@kFk^zTi`AbbAmzCnf@_q@pBcy@zg@oiAzcBuzAnx@mrAnpAsyA`d@wgCn]yk@r[{j@h\\iZryAkk@bbDgO`zDq`@zuB}\\be@ce@dSi{@}k@am@hFgh@j_@u_AnGo`DvXafB`s@ikAuAqkCuj@cvC~W{mAxfA}gDbgCsjAvtA{bBxM_hBjo@agBt`@_eBRedBo_@gv@uGkiA`b@_w@~o@{Qn|@}w@ptBql@|iDis@vwBqkBx}B}fAzb@ciAaTqs@}Hax@nPql@rf@yz@j{Bi}@j_BcQ|{AxKjgF~`@bvE~l@l`Mly@t`JmFj~CtX|cClEhkAsU`gA_^tcDaRnp@~BzrAlDdcBwEjq@wNduAdOh}FuZtGxErqArKzaAnAr]"
    
    let polyline = Polyline(encodedPolyline: encodedPolylineString1)
    print("\(polyline.coordinates?.count)")
    */
    
}
//Use Polyline.swift to decode the string to array of coordinates
/*
{
"points" : "y~jyHx}Wn]jhAOr`AmJzKgk@`d@qFdRxCp{Agg@|x@}Jjt@dApiAfVv~@nLzxE_Lvq@e]~ZaZnk@wHlcAq[dyC_StbEst@l}Fc_@feCsFdoEm{@zsFoOp_E`Gj{C_Nf_Cg`@`aBaX`hAw[b_@wc@vl@}VbpAc_AnyE{FbpCbBnlAsQbbAaV|oBmb@lkAaBfpBsYtwBjVviDz@rrAob@n{Awk@lgBaJpjAcK|aDcU~jAqh@p`Amr@ri@uiAbmA_^rbDaG~|Byt@xhAk~@vbAa~Bz~CikBfsF}f@`bDq|@`kDelAtsA{n@hPq\\aDa`A_ZgdAeo@et@{m@yg@~Uw|BncByl@hZ}m@puAwdAblD}Ll~@mn@vzAwwA|cCuaBna@es@|Saq@s@edDaXu_CtLosAt|Ask@n_Dqi@l_B_oBbk@c{Avj@mhAbwA_qAjlA_~Ad[cyC|k@ex@|q@cyB~pEgdCpxCqqBx}Do}Az`B_bA`mAkoAvs@mv@nb@sc@dw@ucAbeBsoBt_FsyAdgE}xBrdG}C|nBgp@|cDu_ApvCwy@rt@ehApsCc~B`uDin@lcAk_@j|B{w@`gFmq@zuBkOv\\oZDir@gXwX}`@}a@uo@yn@u]uxAmqBcw@gmAc[}iBaw@etA}_C}`Au~@ka@}y@~EksB~Dg~@pEyyAlk@gdAzf@{pBtVonBvEknBpdEgc@~cAuj@`[qa@xdAqi@la@ys@nNoj@oBqp@xMgfBp^ct@fl@}j@`aBin@rtCDjeC`MjrBmu@v`CgQp|BlD~}BeFpxCce@j`DqUhfAz@~x@bMtaCp^vgBqYhkA}_BheDy{@jd@aoAcZclA|DybB{VktCn]ct@xx@izArhAow@lmCui@je@il@rDqeAdIwz@m{@{`@kFk^zTi`AbbAmzCnf@_q@pBcy@zg@oiAzcBuzAnx@mrAnpAsyA`d@wgCn]yk@r[{j@h\\iZryAkk@bbDgO`zDq`@zuB}\\be@ce@dSi{@}k@am@hFgh@j_@u_AnGo`DvXafB`s@ikAuAqkCuj@cvC~W{mAxfA}gDbgCsjAvtA{bBxM_hBjo@agBt`@_eBRedBo_@gv@uGkiA`b@_w@~o@{Qn|@}w@ptBql@|iDis@vwBqkBx}B}fAzb@ciAaTqs@}Hax@nPql@rf@yz@j{Bi}@j_BcQ|{AxKjgF~`@bvE~l@l`Mly@t`JmFj~CtX|cClEhkAsU`gA_^tcDaRnp@~BzrAlDdcBwEjq@wNduAdOh}FuZtGxErqArKzaAnAr]"
}


*/
