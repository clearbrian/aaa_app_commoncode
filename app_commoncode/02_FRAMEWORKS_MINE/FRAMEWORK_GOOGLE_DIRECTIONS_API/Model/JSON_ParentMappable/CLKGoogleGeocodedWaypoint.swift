//
//  CLKGoogleGeocodedWaypoint.swift
//  joyride
//
//  Created by Brian Clear on 25/01/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation

class CLKGoogleGeocodedWaypoint: ParentMappable {
    
    var types: [String]?
    var place_id: String?
    var geocoder_status: String?

    // root calls mapping()
    //    required init?(_ map: Map){
    //        mapping(map: map)
    //    }
    
    override func mapping(map: Map){
        types <- map["types"]
        place_id <- map["place_id"]
        geocoder_status <- map["geocoder_status"]
    }
}
/*
{
    "types" : [
        "locality",
        "political"
    ],
    "place_id" : "ChIJt2BwZIrfekgRAW4XP28E3EI",
    "geocoder_status" : "OK"
}
*/
