//
//  GoogleDirectionsController.swift
//  joyride
//
//  Created by Brian Clear on 22/01/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation

class GoogleDirectionsController : ParentSwiftObject{
    
    let googleDirectionsWSController = GoogleDirectionsWSController()
//    let clkGoogleDirectionsTypesManager = CLKGoogleDirectionsTypesManager()
    
//    static let defaultSearchRadius : Double = 75.0
    
    override init(){
        super.init()
        
    }
    
    func findGoogleDirections(_ clkGoogleDirectionsRequest: CLKGoogleDirectionsRequest,
        success: @escaping (_ clkGoogleDirectionsResponse: CLKGoogleDirectionsResponse?) -> Void,
        failure: @escaping (_ error: Error?) -> Void
        )
    {
        //---------------------------------------------------------------------
        
        self.googleDirectionsWSController.get_google_directions(clkGoogleDirectionsRequest,
            success:{
                (clkGoogleDirectionsResponse: CLKGoogleDirectionsResponse?)->Void in
                //---------------------------------------------------------------------
                if let clkGoogleDirectionsResponse = clkGoogleDirectionsResponse{
                    logger.debug("clkGoogleDirectionsResponse returned:\(clkGoogleDirectionsResponse)")
                
                    success(clkGoogleDirectionsResponse)
                }else{
                    logger.error("clkGoogleDirectionsResponse is nil")
                }
                
            },
            failure:{
                (error) -> Void in
                return failure(error)
            }
        )
        //---------------------------------------------------------------------
    }
}
