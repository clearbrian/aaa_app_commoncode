//
//  WSParam.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 21/07/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
/*
 
 {
 "description": "The id of the place, you can use the /Place/Types/{types} endpoint to get a list of places for a given type including their ids",
 "required": true,
 "type": "string",
 "name": "id",
 "in": "path"
 },
 
 */

class WSParam : ParentMappable{
    
    var name : String?
    var required : Bool?
    
    //value to use with param 
    //?name=value or
    //{value}
    var value : String?
    
    convenience init(name : String, required : Bool) {
        self.init()
        
        self.name = name
        self.required = required
        
    }
    
    // root calls mapping()
    //    required init?(map: Map){
    //        mapping(map: map)
    //    }
    
    override func mapping(map: Map){
        name <- map["name"]
        required <- map["required"]

    }
}

enum WSSwaggerParamTypeIn{
    case unknown
    case path
    case query
    
    func wsSwaggerParamTypeInForString(jsonString:String) -> WSSwaggerParamTypeIn{
        switch jsonString{
        case "path":
            return .path
            
        case "query":
            return .query
            
        default:
            logger.error("wsSwaggerParamTypeInForString UNKNOWN jsonString:'\(jsonString)'")
            return .unknown
            
        }
    }
}

/*
 
 {
 "description": "The id of the place, you can use the /Place/Types/{types} endpoint to get a list of places for a given type including their ids",
 "required": true,
 "type": "string",
 "name": "id",
 "in": "path"
 },
 
 */
class WSSwaggerParam : ParentMappable{
    
    var name : String?
    var required : Bool?
    
    var description : String?
    var type : String?
    
    var wsSwaggerParamTypeIn = WSSwaggerParamTypeIn.unknown

    convenience init(       name: String,
                        required: Bool,
                     description: String,
                            type: String,
              wsSwaggerParamTypeIn: WSSwaggerParamTypeIn)
    {
        self.init()
        
        self.name = name
        self.required = required
        self.description = description
        self.type = type
        self.wsSwaggerParamTypeIn = wsSwaggerParamTypeIn
        
    }
    
    // root calls mapping()
    //    required init?(map: Map){
    //        mapping(map: map)
    //    }
    
    override func mapping(map: Map){
        name <- map["name"]
        required <- map["required"]
        description <- map["description"]
        type <- map["type"]
        required <- map["required"]
        wsSwaggerParamTypeIn <- map["in"]

        
    }
}
