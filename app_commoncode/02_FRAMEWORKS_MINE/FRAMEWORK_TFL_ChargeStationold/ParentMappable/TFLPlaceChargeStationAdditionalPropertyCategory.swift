//
//  TFLApiPlaceChargeConnectorAdditionalPropertyCategory.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 08/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

//"category": "payload",
//"category": "cameraView",

enum TFLApiPlaceChargeConnectorAdditionalPropertyCategory: String{
    case unknown = "unknown"
    case payload = "payload"
    case cameraView = "cameraView"
    
    
    func displayString() -> String{
        switch self{
        case .unknown:
            return "Unknown"
            
        case .payload:
            return "payload"
            
        case .cameraView:
            return "cameraView"
            
        }
    }
    
    //"payload" >> TFLApiPlaceChargeConnectorAdditionalPropertyCategory.payload
    
    static func categoryForCategoryString(categoryString: String?) -> TFLApiPlaceChargeConnectorAdditionalPropertyCategory{
        
        if let categoryString = categoryString {
            switch categoryString{
            case "payload":
                return .payload
                
            case "cameraView":
                return .cameraView
                
            default:
                return .unknown
            }
        }else{
            appDelegate.log.error("categoryString is nil")
            return .unknown
        }
    }
}
