//
//  TFLAdditionalPropertyKey.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 13/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

//https://docs.google.com/spreadsheets/d/1ZD7Oty83tidOD-spUpJsMFRFphzVFIY9hqpyef6BrnY/edit#gid=0

/*
 "key": "available",
 "key": "imageUrl",
 "key": "videoUrl",
 "key": "view",

 */

enum TFLAdditionalPropertyKey_ChargeConnector: String{
    
    case unknownPropKey = "Unknown"
    
    case available_PropKey = "available"
    case imageUrl_PropKey = "imageUrl"
    case videoUrl_PropKey = "videoUrl"
    case view_PropKey = "view"

    
    func displayString() -> String{
        switch self{
        case .unknownPropKey:
            return "Unknown"
            
        case .available_PropKey:
            return "Available"
            
        case .imageUrl_PropKey:
            return "Image URL"
            
        case .videoUrl_PropKey:
            return "videoUrl"
            
        case .view_PropKey:
            return "view"
            
        }
    }
    
    //"imageUrl" >> .imageUrl
    static func keyForKeyString(keyString: String?) -> TFLAdditionalPropertyKey_ChargeConnector{
        
        if let keyString = keyString {
            switch keyString{
                
            case "unknown":
                return .unknownPropKey
                
            case "available":
                return .available_PropKey
                
            case "imageUrl":
                return .imageUrl_PropKey
                
            case "videoUrl":
                return .videoUrl_PropKey
                
            case "view":
                return .view_PropKey


            default:
                return .unknownPropKey
            }
        }else{
            appDelegate.log.error("keyString is nil")
            return .unknownPropKey
        }
    }
}
