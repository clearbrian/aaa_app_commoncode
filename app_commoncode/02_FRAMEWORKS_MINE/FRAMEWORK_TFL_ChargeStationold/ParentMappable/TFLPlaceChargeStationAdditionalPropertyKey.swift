//
//  TFLApiPlaceChargeConnectorAdditionalPropertyKey.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 08/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

enum TFLApiPlaceChargeConnectorAdditionalPropertyKey: String{
    case UnknownPropKey = "Unknown"
    case BoroughPropKey = "Borough"
    case NumberOfSpacesPropKey = "NumberOfSpaces"
    case OperationDaysPropKey = "OperationDays"
    case OperationTimesPropKey = "OperationTimes"
    case RankTypePropKey = "RankType"
    case StationAtcoCodePropKey = "StationAtcoCode" //StationAtcoCode,910GSRUISLP - ChargeConnector_3569,PROP,Address,Borough,Hillingdon
    
    func displayString() -> String{
        switch self{
        case .UnknownPropKey:
            return "Unknown"
            
        case .BoroughPropKey:
            return "Borough"
            
        case .NumberOfSpacesPropKey:
            return "Number Of Spaces"
            
        case .OperationDaysPropKey:
            return "Operation Days"
            
        case .OperationTimesPropKey:
            return "Operation Times"
            
        case .RankTypePropKey:
            return "Rank Type"
            
        case .StationAtcoCodePropKey:
            return "Station Atco Code"
            
        }
    }
    
    //"RankType" >> TFLApiPlaceChargeConnectorAdditionalPropertyKey.RankType
    static func keyForKeyString(keyString: String?) -> TFLApiPlaceChargeConnectorAdditionalPropertyKey{
        
        if let keyString = keyString {
            switch keyString{
                
            case "Borough":
                return .BoroughPropKey
                
            case "NumberOfSpaces":
                return .NumberOfSpacesPropKey
                
            case "OperationDays":
                return .OperationDaysPropKey
                
            case "OperationTimes":
                return .OperationTimesPropKey
                
            case "RankType":
                return .OperationTimesPropKey
                
            case "StationAtcoCode":
                return .StationAtcoCodePropKey
            default:
                return .UnknownPropKey
            }
        }else{
            appDelegate.log.error("keyString is nil")
            return .UnknownPropKey
        }
    }
}
