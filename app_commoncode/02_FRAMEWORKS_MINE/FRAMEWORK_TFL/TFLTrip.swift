//
//  TFLTrip.swift
//  joyride
//
//  Created by Brian Clear on 01/12/2015.
//  Copyright © 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation

class TFLTrip : ParentSwiftObject{
    
    var clkTrip: CLKTrip
    
    let website = "https://tfl.gov.uk/plan-a-journey/results?IsAsync=true&JpType=publictransport&InputFrom=51.4930997%2C-0.1464806&DataSetsJson=%5B%5B%22stopPoints%22%2C%22journeyPlannerNoSubmit%3FInput%3D%7B%7Binput%7D%7D%22%5D%2C%5B%22bikePoints%22%2C%22journeyPlannerNoSubmit%3FInput%3D%7B%7Binput%7D%7D%22%5D%2C%5B%22placesExtra%22%2C%22journeyPlannerNoSubmit%3FInput%3D%7B%7Binput%7D%7D%22%5D%5D&Modes=tube%2Cdlr%2Coverground%2Ctflrail%2Cbus%2Criver-bus%2Ctram%2Ccable-car%2Cnational-rail%2Criver-tour&From=51.4930997%2C-0.1464806&FromId=&PreviousFrom=&InputTo=51.50205993652344%2C-0.1093124970793724&DataSetsJson=%5B%5B%22stopPoints%22%2C%22journeyPlannerNoSubmit%3FInput%3D%7B%7Binput%7D%7D%22%5D%2C%5B%22bikePoints%22%2C%22journeyPlannerNoSubmit%3FInput%3D%7B%7Binput%7D%7D%22%5D%2C%5B%22placesExtra%22%2C%22journeyPlannerNoSubmit%3FInput%3D%7B%7Binput%7D%7D%22%5D%5D&Modes=tube%2Cdlr%2Coverground%2Ctflrail%2Cbus%2Criver-bus%2Ctram%2Ccable-car%2Cnational-rail%2Criver-tour&To=51.50205993652344%2C-0.1093124970793724&ToId=&PreviousTo=&Date=20151201&Time=1830&Mode=bus&Mode=tube&Mode=national-rail&Mode=dlr&Mode=overground&Mode=tflrail&Mode=river-bus&Mode=tram&Mode=cable-car&Mode=coach&CyclePreference=AllTheWay&WalkingSpeedWalking=average&JourneyPreference=leasttime&AccessibilityPreference=norequirements&MaxWalkingMinutes=40&WalkingSpeedTransport=average&InputVia=&DataSetsJson=%5B%5B%22stopPoints%22%2C%22journeyPlannerNoSubmit%3FInput%3D%7B%7Binput%7D%7D%22%5D%5D&Modes=tube%2Cdlr%2Coverground%2Ctflrail%2Cbus%2Criver-bus%2Ctram%2Ccable-car%2Cnational-rail&Via=&ViaId=&PreviousVia=&NationalSearch=false&SavePreferences=false&IsMultipleJourneySelection=False&JourneyType=&IsPastWarning=False"
/*
    //from street address to street address - TFL uses google so should match
    https://tfl.gov.uk/plan-a-journey/results?IsAsync=true&JpType=publictransport&InputFrom=14+E+Smithfield&From=14+E+Smithfield&FromId=&PreviousFrom=&InputTo=47+chart+street&DataSetsJson=%5B%5B%22stopPoints%22%2C%22journeyPlannerNoSubmit%3FInput%3D%7B%7Binput%7D%7D%22%5D%2C%5B%22bikePoints%22%2C%22journeyPlannerNoSubmit%3FInput%3D%7B%7Binput%7D%7D%22%5D%2C%5B%22placesExtra%22%2C%22journeyPlannerNoSubmit%3FInput%3D%7B%7Binput%7D%7D%22%5D%5D&Modes=tube%2Cdlr%2Coverground%2Ctflrail%2Cbus%2Criver-bus%2Ctram%2Ccable-car%2Cnational-rail%2Criver-tour&To=47+chart+street&ToId=&PreviousTo=&Date=20160607&Time=1900&Mode=bus&Mode=tube&Mode=national-rail&Mode=dlr&Mode=overground&Mode=tflrail&Mode=river-bus&Mode=tram&Mode=cable-car&Mode=coach&CyclePreference=AllTheWay&WalkingSpeedWalking=average&JourneyPreference=leasttime&AccessibilityPreference=norequirements&MaxWalkingMinutes=40&WalkingSpeedTransport=average&InputVia=&DataSetsJson=%5B%5B%22stopPoints%22%2C%22journeyPlannerNoSubmit%3FInput%3D%7B%7Binput%7D%7D%22%5D%5D&Modes=tube%2Cdlr%2Coverground%2Ctflrail%2Cbus%2Criver-bus%2Ctram%2Ccable-car%2Cnational-rail&Via=&ViaId=&PreviousVia=&NationalSearch=false&WalkingOptimization=false&SavePreferences=false&IsMultipleJourneySelection=False&JourneyType=&IsPastWarning=False
    
    https://tfl.gov.uk/plan-a-journey/results?IsAsync=true&JpType=publictransport&InputFrom=14 E Smithfield&From=14 E Smithfield&FromId=&PreviousFrom=&InputTo=47 chart street&DataSetsJson=[["stopPoints","journeyPlannerNoSubmit?Input={{input}}"],["bikePoints","journeyPlannerNoSubmit?Input={{input}}"],["placesExtra","journeyPlannerNoSubmit?Input={{input}}"]]&Modes=tube,dlr,overground,tflrail,bus,river-bus,tram,cable-car,national-rail,river-tour&To=47 chart street&ToId=&PreviousTo=&Date=20160607&Time=1900&Mode=bus&Mode=tube&Mode=national-rail&Mode=dlr&Mode=overground&Mode=tflrail&Mode=river-bus&Mode=tram&Mode=cable-car&Mode=coach&CyclePreference=AllTheWay&WalkingSpeedWalking=average&JourneyPreference=leasttime&AccessibilityPreference=norequirements&MaxWalkingMinutes=40&WalkingSpeedTransport=average&InputVia=&DataSetsJson=[["stopPoints","journeyPlannerNoSubmit?Input={{input}}"]]&Modes=tube,dlr,overground,tflrail,bus,river-bus,tram,cable-car,national-rail&Via=&ViaId=&PreviousVia=&NationalSearch=false&WalkingOptimization=false&SavePreferences=false&IsMultipleJourneySelection=False&JourneyType=&IsPastWarning=False
     
     
     https://tfl.gov.uk/plan-a-journey/results?IsAsync=true
     &JpType=publictransport
     &InputFrom=14 E Smithfield
     &From=14 E Smithfield
     &FromId=
     &PreviousFrom=
     &InputTo=47 chart street
     &DataSetsJson=[["stopPoints","journeyPlannerNoSubmit?Input={{input}}"],["bikePoints","journeyPlannerNoSubmit?Input={{input}}"],["placesExtra","journeyPlannerNoSubmit?Input={{input}}"]]
     &Modes=tube,dlr,overground,tflrail,bus,river-bus,tram,cable-car,national-rail,river-tour
     &To=47 chart street
     &ToId=
     &PreviousTo=
     &Date=20160607
     &Time=1900
     &Mode=bus
     &Mode=tube
     &Mode=national-rail
     &Mode=dlr
     &Mode=overground
     &Mode=tflrail
     &Mode=river-bus
     &Mode=tram
     &Mode=cable-car
     &Mode=coach
     &CyclePreference=AllTheWay
     &WalkingSpeedWalking=average
     &JourneyPreference=leasttime
     &AccessibilityPreference=norequirements
     &MaxWalkingMinutes=40
     &WalkingSpeedTransport=average
     &InputVia=
     &DataSetsJson=[["stopPoints","journeyPlannerNoSubmit?Input={{input}}"]]
     &Modes=tube,dlr,overground,tflrail,bus,river-bus,tram,cable-car,national-rail
     &Via=
     &ViaId=
     &PreviousVia=
     &NationalSearch=false
     &WalkingOptimization=false
     &SavePreferences=false
     &IsMultipleJourneySelection=False
     &JourneyType=
     &IsPastWarning=False
     
    */
    
//    func init(){
//        
//    }
    init (clkTrip: CLKTrip){
        self.clkTrip = clkTrip
    }
    
    var urlString: String? {
        get {
            
            var urlString_ = ""


            if let formatted_address_first_line_start = self.clkTrip.clkPlaceStart.formatted_address_first_line {
                if let formatted_address_first_line_end = self.clkTrip.clkPlaceEnd.formatted_address_first_line {
                    //------------------------------------------------------------------------------------------------
                    urlString_ = self.generateURLString(startString: formatted_address_first_line_start,
                                                          endString: formatted_address_first_line_end)
                    //------------------------------------------------------------------------------------------------
                    
                }else{
                    logger.error("self.clkTrip.clkPlaceEnd is nil")
                }
            }else{
                logger.error("self.clkTrip.clkPlaceStart is nil")
            }
            //not set then use lat,lng
            if urlString_ == "" {
                if let clLocationStart = self.clkTrip.clkPlaceStart.clLocation {
                    if let clLocationEnd = self.clkTrip.clkPlaceEnd.clLocation {
                        //------------------------------------------------------------------------------------------------
                        urlString_ = self.generateURLString(startString: "\(clLocationStart.coordinate.latitude),\(clLocationStart.coordinate.longitude)",
                                                            endString: "\(clLocationEnd.coordinate.latitude),\(clLocationEnd.coordinate.longitude)")
                        //------------------------------------------------------------------------------------------------
                        
                    }else{
                        logger.error("self.clkTrip.clkPlaceEnd is nil")
                    }
                }else{
                    logger.error("self.clkTrip.clkPlaceStart is nil")
                }
            }else{
                //set using formatted_address_first_line - return
            }
            
            return urlString_
        }
//        set {
//            
//        }
    }
    
    
    func generateURLString(startString: String, endString: String) -> String{
        //------------------------------------------------------------------------------------------------
        var urlString_ = "https://tfl.gov.uk/plan-a-journey/results?IsAsync=true"
        urlString_ = urlString_ + "&JpType=publictransport"


        //-----------------------------------------------------------------------------------
        //urlString_ = urlString_ + "&InputFrom=51.4930997,-0.1464806"
        urlString_ = urlString_ + "&InputFrom=\(startString)"
        //-----------------------------------------------------------------------------------

        urlString_ = urlString_ + "&DataSetsJson=[[\"stopPoints\",\"journeyPlannerNoSubmit?Input={{input}}\"],[\"bikePoints\",\"journeyPlannerNoSubmit?Input={{input}}\"],[\"placesExtra\",\"journeyPlannerNoSubmit?Input={{input}}\"]]"
        urlString_ = urlString_ + "&Modes=tube,dlr,overground,tflrail,bus,river-bus,tram,cable-car,national-rail,river-tour"

        //-----------------------------------------------------------------------------------
        //urlString_ = urlString_ + "&From=51.4930997,-0.1464806"
        urlString_ = urlString_ + "&From=\(startString)"
        //-----------------------------------------------------------------------------------
        urlString_ = urlString_ + "&FromId="
        urlString_ = urlString_ + "&PreviousFrom="

        //-----------------------------------------------------------------------------------
        //urlString_ = urlString_ + "&InputTo=51.50205993652344,-0.1093124970793724"
        urlString_ = urlString_ + "&InputTo=\(endString)"

        //-----------------------------------------------------------------------------------

        urlString_ = urlString_ + "&DataSetsJson=[[\"stopPoints\",\"journeyPlannerNoSubmit?Input={{input}}\"],[\"bikePoints\",\"journeyPlannerNoSubmit?Input={{input}}\"],[\"placesExtra\",\"journeyPlannerNoSubmit?Input={{input}}\"]]"
        urlString_ = urlString_ + "&Modes=tube,dlr,overground,tflrail,bus,river-bus,tram,cable-car,national-rail,river-tour"

        //-----------------------------------------------------------------------------------
        //urlString_ = urlString_ + "&To=51.50205993652344,-0.1093124970793724"
        urlString_ = urlString_ + "&To=\(endString)"
        //-----------------------------------------------------------------------------------
        urlString_ = urlString_ + "&ToId="
        urlString_ = urlString_ + "&PreviousTo="

        /*urlString_ = urlString_ + "&Date=20151201"*/
        urlString_ = urlString_ + "&Date=\(self.dateFormatted)"

        //urlString_ = urlString_ + "&Time=1830"
        urlString_ = urlString_ + "&Time=\(self.timeFormatted)"


        urlString_ = urlString_ + "&Mode=bus"
        urlString_ = urlString_ + "&Mode=tube"
        urlString_ = urlString_ + "&Mode=national-rail"
        urlString_ = urlString_ + "&Mode=dlr"
        urlString_ = urlString_ + "&Mode=overground"
        urlString_ = urlString_ + "&Mode=tflrail"
        urlString_ = urlString_ + "&Mode=river-bus"
        urlString_ = urlString_ + "&Mode=tram"
        urlString_ = urlString_ + "&Mode=cable-car"
        urlString_ = urlString_ + "&Mode=coach"
        urlString_ = urlString_ + "&CyclePreference=AllTheWay"
        urlString_ = urlString_ + "&WalkingSpeedWalking=average"
        urlString_ = urlString_ + "&JourneyPreference=leasttime"
        urlString_ = urlString_ + "&AccessibilityPreference=norequirements"
        urlString_ = urlString_ + "&MaxWalkingMinutes=40"
        urlString_ = urlString_ + "&WalkingSpeedTransport=average"
        urlString_ = urlString_ + "&InputVia="
        urlString_ = urlString_ + "&DataSetsJson=[[\"stopPoints\",\"journeyPlannerNoSubmit?Input={{input}}\"]]"
        urlString_ = urlString_ + "&Modes=tube,dlr,overground,tflrail,bus,river-bus,tram,cable-car,national-rail"
        urlString_ = urlString_ + "&Via="
        urlString_ = urlString_ + "&ViaId="
        urlString_ = urlString_ + "&PreviousVia="
        urlString_ = urlString_ + "&NationalSearch=false"
        urlString_ = urlString_ + "&SavePreferences=false"
        urlString_ = urlString_ + "&IsMultipleJourneySelection=False"
        urlString_ = urlString_ + "&JourneyType="
        urlString_ = urlString_ + "&IsPastWarning=False"
        //------------------------------------------------------------------------------------------------
        return urlString_
    }
    
    var dateFormatted: String{
        var dateFormatted : NSString = ""
        
        let dateFormatterIN = DateFormatter()
        
        //&Date=20151201"
        dateFormatterIN.dateFormat = "yyyyMMdd"
        
        dateFormatted = dateFormatterIN.string(from: Date()) as NSString
        
        return dateFormatted as String
    }
    var timeFormatted: String{
        var timeFormatted : NSString = ""
        
        let timeFormatterIN = DateFormatter()
        
        //&Time=1830"
        timeFormatterIN.dateFormat = "HHmm"
        
        timeFormatted = timeFormatterIN.string(from: Date()) as NSString
        
        return timeFormatted as String
    }


}
