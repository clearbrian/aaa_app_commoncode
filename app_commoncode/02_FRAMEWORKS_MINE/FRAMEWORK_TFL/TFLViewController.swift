//
//  TFLViewController.swift
//  joyride
//
//  Created by Brian Clear on 01/12/2015.
//  Copyright © 2015 City Of London Consulting Limited. All rights reserved.
//

import UIKit
import SafariServices

class TFLViewController: ParentViewController, SFSafariViewControllerDelegate{
    
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - IMPLEMENTATION
    //--------------------------------------------------------------
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
    }
    
    // MARK: -
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonOpenInTFL_Action(_ sender: AnyObject) {
        if let websiteUrl = URL(string: website) {
            let safariViewController = SFSafariViewController(url: websiteUrl)
            safariViewController.delegate = self
            present(safariViewController, animated: true, completion: nil)
        }
        
    }
    
    let website = "https://tfl.gov.uk/plan-a-journey/results?IsAsync=true&JpType=publictransport&InputFrom=51.4930997%2C-0.1464806&DataSetsJson=%5B%5B%22stopPoints%22%2C%22journeyPlannerNoSubmit%3FInput%3D%7B%7Binput%7D%7D%22%5D%2C%5B%22bikePoints%22%2C%22journeyPlannerNoSubmit%3FInput%3D%7B%7Binput%7D%7D%22%5D%2C%5B%22placesExtra%22%2C%22journeyPlannerNoSubmit%3FInput%3D%7B%7Binput%7D%7D%22%5D%5D&Modes=tube%2Cdlr%2Coverground%2Ctflrail%2Cbus%2Criver-bus%2Ctram%2Ccable-car%2Cnational-rail%2Criver-tour&From=51.4930997%2C-0.1464806&FromId=&PreviousFrom=&InputTo=The+Old+Vic%2C+The+Cut%2C+London+SE1+8NB&To=The+Old+Vic%2C+The+Cut%2C+London+SE1+8NB&ToId=51.50205993652344%2C-0.1093124970793724&PreviousTo=&Date=20151201&Time=1730&Mode=bus&Mode=tube&Mode=national-rail&Mode=dlr&Mode=overground&Mode=tflrail&Mode=river-bus&Mode=tram&Mode=cable-car&Mode=coach&CyclePreference=AllTheWay&WalkingSpeedWalking=average&JourneyPreference=leasttime&AccessibilityPreference=norequirements&MaxWalkingMinutes=40&WalkingSpeedTransport=average&InputVia=&DataSetsJson=%5B%5B%22stopPoints%22%2C%22journeyPlannerNoSubmit%3FInput%3D%7B%7Binput%7D%7D%22%5D%5D&Modes=tube%2Cdlr%2Coverground%2Ctflrail%2Cbus%2Criver-bus%2Ctram%2Ccable-car%2Cnational-rail&Via=&ViaId=&PreviousVia=&NationalSearch=false&SavePreferences=false&IsMultipleJourneySelection=False&JourneyType=&IsPastWarning=False"
    
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        print("Closed Safari")
    }
}
