//
//  TFLApiPlaceMapDirectionsViewController.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 19/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps

class TFLApiPlaceMapDirectionsViewController: GenericMapViewController, COLCGoogleDirectionsUIManagerDelegate {
    
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - DIRECTIONS
    //--------------------------------------------------------------
    //init in viewDidLoad AFTER mapView and this VC fully init'd
    var colcGoogleDirectionsUIManager : COLCGoogleDirectionsUIManager?
    
    @IBOutlet weak var segmentedControlTravelMode: UISegmentedControl!
    @IBOutlet weak var labelName: UILabel!
    
    @IBOutlet weak var buttonOpenIn: UIButton!
    
     
    //--------------------------------------------------------------
    // MARK: - VIEW DID LOAD
    // MARK: -
    //--------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Directions"
        
        
        self.buttonOpenIn.layer.cornerRadius = 4.0
        //---------------------------------------------------------------------
        self.clkPlaceStart = appDelegate.currentLocationManager.currentUserPlace
        //---------------------------------------------------------------------
        if let selectedTFLApiPlace = self.selectedTFLApiPlace {
            if let clLocation = selectedTFLApiPlace.clLocation {
                
                self.clkPlaceEnd = CLKPlace.init(clLocation: clLocation, name: Safe.safeString(selectedTFLApiPlace.commonName), clkPlaceWrapperType: .geoCodeLocation)
                
            }else{
                logger.error("selectedTFLApiPlace.clLocation is nil")
            }
        }else{
            logger.error("self.selectedTFLApiPlace is nil")
        }
        
         
        
        
        //---------------------------------------------------------------------
        //init only after map and this VC fully init'd
        //---------------------------------------------------------------------
        if let gmsMapView = self.gmsMapView {
            self.colcGoogleDirectionsUIManager = COLCGoogleDirectionsUIManager(gmsMapView: gmsMapView, parentViewController: self)
            self.colcGoogleDirectionsUIManager?.delegate = self
            
            showOrHideDirectionsOverlayFromWS()
        }else{
            logger.error("self.mapView is nil - cant init COLCGoogleDirectionsUIManager")
        }
        configureNavBar()
    }
    
    func configureNavBar() {
        
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back",
                                                           style: .plain,
                                                           target: self,
                                                           action: #selector(TFLApiPlaceMapDirectionsViewController.backButtonItem_Action))
        
        //navigationItem.bab
    
    }
    
    @objc func backButtonItem_Action() {
        //self.dismiss(animated: true, completion: {
        //    
        //})
        
        if let navigationController = self.navigationController {
            navigationController.popViewController(animated: true)
        }else{
            logger.error("self.navigationController is nil")
        }
        
    }
    var arrayLabelName = ["","",""]
    func displayName(){
        //reset everytime we get directions as dist/duration appended
        self.labelName.text = ""
        //---------------------------------------------------------
        //growing label
        //---------------------------------------------------------
        if let selectedTFLApiPlace = self.selectedTFLApiPlace {
            if let commonName = selectedTFLApiPlace.commonName {
                
                self.addToLabelName(lineString:commonName, row:0)
                self.addToLabelName(lineString:"", row:1)
                self.addToLabelName(lineString:"", row:2)
                
                
            }else{
                logger.error("selectedTFLApiPlace.commonName is nil")
            }
        }else{
            logger.error("self.selectedTFLApiPlace ")
        }
    }
    
    
    //grows
    //check for optional else it appears in the string
    
    //make should label lines is 0 for dynamic text to grow
    func addToLabelName(lineString: String?, row: Int){
        
        
        if let lineString_ = lineString {
            arrayLabelName[row] = lineString_
        }else{
            arrayLabelName[row] = ""
        }
        
        self.labelName.text = "\(arrayLabelName[0])\r\(arrayLabelName[1])\r\(arrayLabelName[2])"
        
        
//        if let labelNameText = self.labelName.text {
//            if let lineString = lineString {
//                
//                if labelNameText == ""{
//                    self.labelName.text = lineString
//                }else{
//                    
//                    self.labelName.text = "\(labelNameText)\r\(lineString)"
//                }
//            }else{
//                logger.error("lineString is nil")
//            }
//        }else{
//            logger.error("self.labelName.text is nil")
//        }
        
    }
    
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    
    //--------------------------------------------------------------
    // MARK: - DIRECTIONS
    // MARK: -
    //--------------------------------------------------------------
    
    var clkPlaceStart: CLKPlace?
    var clkPlaceEnd: CLKPlace?
    
    
    func showOrHideDirectionsOverlayFromWS(){
        //reset label as duration/dist appended
        displayName()
        
        if let colcGoogleDirectionsUIManager = self.colcGoogleDirectionsUIManager {
            
            if let clkPlaceStart = self.clkPlaceStart {
                if let clkPlaceEnd = self.clkPlaceEnd {
                    
                    if clkPlaceStart === clkPlaceEnd{
                        logger.error("origin and destination are same - can happen when you swap")
                    }else{
                        
                        //------------------------------------------------------------------------------------------------
                        //TRAVEL MODE
                        //------------------------------------------------------------------------------------------------
                        var clkGoogleDirectionsTravelMode: CLKGoogleDirectionsTravelMode = .walking
                        
                        if self.segmentedControlTravelMode.selectedSegmentIndex == 0{
                            clkGoogleDirectionsTravelMode = .driving
                            
                        }else if self.segmentedControlTravelMode.selectedSegmentIndex == 1{
                            clkGoogleDirectionsTravelMode = .walking
                        }else{
                            
                            logger.error("UNHANDLED : segmentedControlTravelMode")
                        }
                        //------------------------------------------------------------------------------------------------
                        
                        
                        colcGoogleDirectionsUIManager.getDirections(clkPlaceStart: clkPlaceStart,
                                                                    clkPlaceEnd: clkPlaceEnd,
                                                                    clkGoogleDirectionsTravelMode: clkGoogleDirectionsTravelMode,
                                                                    
                                                                    success:{(clkGoogleDirectionsResponse: CLKGoogleDirectionsResponse?) -> Void in
                                                                        //---------------------------------------------------------------------
                                                                        if let clkGoogleDirectionsResponse = clkGoogleDirectionsResponse {
                                                                            colcGoogleDirectionsUIManager.handleCLKGoogleDirectionsResponse(clkGoogleDirectionsResponse)
                                                                        }else{
                                                                            logger.error("clkGoogleDirectionsResponse is nil")
                                                                        }
                                                                        //---------------------------------------------------------------------
                                                                        //UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                                                        //---------------------------------------------------------------------
                        },
                                                                    failure:{(error) -> Void in
                                                                        
                                                                        logger.error("error:\(String(describing: error))")
                                                                        if let nserror = error {
                                                                            colcGoogleDirectionsUIManager.handleError(nserror)
                                                                            
                                                                        }else{
                                                                            logger.error("error is nil")
                                                                        }
                                                                        
                                                                        //self.googleDirectionsSearchRunning = false
                                                                        //UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        })
                        //------------------------------------------------------------------------------------------------
                        
                    }
                }else{
                    //clkPlaceEnd is nil - do nothing till both retrieved
                    colcGoogleDirectionsUIManager.removeTripFromMap()
                }
            }else{
                //clkPlaceStart is nil - do nothing till both retrieved
                colcGoogleDirectionsUIManager.removeTripFromMap()
            }
        }else{
            logger.error("self.colcGoogleDirectionsUIManager is nil")
        }
    }
    
    
    //--------------------------------------------------------------
    // MARK: - segmentedControlTravelMode
    // MARK: -
    //--------------------------------------------------------------
    
    @IBAction func segmentedControlTravelMode_ValueChanged(_ sender: Any) {
        if let colcGoogleDirectionsUIManager = self.colcGoogleDirectionsUIManager {
            
            colcGoogleDirectionsUIManager.removeTripFromMap()
            
            showOrHideDirectionsOverlayFromWS()
            
        }else{
            logger.error("self.colcGoogleDirectionsUIManager is nil")
        }
        
    }
    

    //--------------------------------------------------------------
    // MARK: - GMSMapViewDelegate - didTap marker
    // MARK: -
    //--------------------------------------------------------------
    @IBAction func buttonOpenIn_Action(_ sender: AnyObject) {
        self.performSegue(withIdentifier: segueTFLApiPlaceMapDirectionsViewControllerTOOpenWithViewController, sender: nil)
    }
    
    //--------------------------------------------------------------
    // MARK: - prepareForSegue
    // MARK: -
    //--------------------------------------------------------------
    //    let segueTFLApiPlaceMapViewControllerTOTFLApiPlaceSingleTableViewController = "segueTFLApiPlaceMapViewControllerTOTFLApiPlaceSingleTableViewController"
    //    let segueTFLApiPlaceMapViewControllerTOTFLApiPlaceStreetViewController = "segueTFLApiPlaceMapViewControllerTOTFLApiPlaceStreetViewController"
    
    
    let segueTFLApiPlaceMapDirectionsViewControllerTOOpenWithViewController = "segueTFLApiPlaceMapDirectionsViewControllerTOOpenWithViewController"
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == segueTFLApiPlaceMapDirectionsViewControllerTOOpenWithViewController){
            
            if(segue.destination.isMember(of: OpenWithViewController.self)){
                
                let openWithViewController = (segue.destination as! OpenWithViewController)
                
                openWithViewController.clkPlaceStart = self.clkPlaceStart
                openWithViewController.clkPlaceEnd = self.clkPlaceEnd
                
                if self.segmentedControlTravelMode.selectedSegmentIndex == 0{
                    openWithViewController.openWithTravelMode = .driving
                    
                }else if self.segmentedControlTravelMode.selectedSegmentIndex == 1{
                    openWithViewController.openWithTravelMode = .walking
                }else{
                    
                    logger.error("UNHANDLED : segmentedControlTravelMode")
                }
                
                //                //openWithViewController.delegate = self
                
            }else{
                logger.error("destinationViewController not OpenWithViewController")
            }
        }
        else{
            logger.error("UNHANDLED SEGUE:\(String(describing: segue.identifier))")
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - COLCGoogleDirectionsUIManagerDelegate
    // MARK: -
    //--------------------------------------------------------------
    func colcGoogleDirectionsUIManager(_ colcGoogleDirectionsUIManager: COLCGoogleDirectionsUIManager, clkGoogleDirectionsResponse : CLKGoogleDirectionsResponse ){
        //if we get route it zooms out to line, but user hasnt moved map so will jump to cur loc
        self.userDidChangeCameraPosition = true
        
        if let routes = clkGoogleDirectionsResponse.routes{
            
            
            //map is update in COLCGoogleDirectionsUIManager
            for clkGoogleDirectionRoute : CLKGoogleDirectionRoute in routes{
                //---------------------------------------------------------------------
                //ROUTE - 1 leg - could never get it to have more than 1 leg - only n steps in 1 leg in 1 route
                //---------------------------------------------------------------------
                
                if let legs: [CLKGoogleLeg] = clkGoogleDirectionRoute.legs{
                    if legs.count > 0{
                        
                        if legs.count != 1{
                            logger.error("legs.count != 1")
                            
                        }else{
                            //----------------------------------------------------------------------------------------
                            //FIRST LOG ONLY
                            //----------------------------------------------------------------------------------------
                            // TODO: - Support multiple legs
                            let legFirst: CLKGoogleLeg = legs[0]
                            //---------------------------------------------------------------------
                            // distance: CLKGoogleMeasurement
                            //---------------------------------------------------------------------
                            if let distance: CLKGoogleMeasurement = legFirst.distance {
                                //----------------------------------------------------------------------------------------
                                logger.debug("distance[distanceMetricSafe]:\(distance.distanceMetricSafe)")
                                logger.debug("distance[distanceMilesSafe]:\(distance.distanceMilesSafe)")
                                
                                self.addToLabelName(lineString:"Distance: \(distance.distanceMilesSafe) \(distance.distanceMetricSafe)", row:1)
                      
                                
                                
                            }else{
                                logger.error("legFirst.distance is nil - cant get trip info")
                            }
                            //---------------------------------------------------------------------
                            // distance: CLKGoogleMeasurement
                            //---------------------------------------------------------------------
                            if let duration: CLKGoogleMeasurement = legFirst.duration {
                                
                                if let text = duration.text {
                                    logger.debug("duration[text]:\(text)")
                                    
                    
                                    self.addToLabelName(lineString:"Duration: \(text)", row:2)
                                    
                                }else{
                                    logger.error("duration.text is nil")
                                }
                                
                                if let value = duration.value {
                                    logger.debug("duration[secs]:\(value)")
                                }else{
                                    logger.error("distance.text is nil")
                                }
                                
                            }else{
                                logger.error("legFirst.duration is nil - cant get trip info")
                            }
                            //----------------------------------------------------------------------------------------
                            //FIRST LEG ONLY - END
                            //----------------------------------------------------------------------------------------
                            
                            
                            //----------------------------------------------------------------------------------------
                            //update start/end with geocoded address - in case we open in google maps
                            //----------------------------------------------------------------------------------------
                            if let start_address = legFirst.start_address {
                                if let start_location = legFirst.start_location {
                                    if let cllocationCoordinate2D = start_location.cllocationCoordinate2D {
                                        
                                        let clLocation = CLLocation.init(latitude: cllocationCoordinate2D.latitude, longitude: cllocationCoordinate2D.longitude)
                                        let clkPlaceStart_ = CLKPlace.init(clLocation: clLocation,
                                                                           name: start_address,
                                                                           clkPlaceWrapperType: .geoCodeLocation)
                                        
                                        self.clkPlaceStart = clkPlaceStart_
                                    }else{
                                        logger.error("start_location.cllocationCoordinate2D is nil")
                                    }
                                }else{
                                    logger.error("legFirst.start_location is nil")
                                }
                            }else{
                                logger.error("legFirst.start_address is nil")
                            }
                            
                            if let end_address = legFirst.end_address {
                                if let end_location = legFirst.end_location {
                                    if let cllocationCoordinate2D = end_location.cllocationCoordinate2D {
                                        
                                        let clLocation = CLLocation.init(latitude: cllocationCoordinate2D.latitude, longitude: cllocationCoordinate2D.longitude)
                                        let clkPlaceEnd_ = CLKPlace.init(clLocation: clLocation,
                                                                         name: end_address,
                                                                         clkPlaceWrapperType: .geoCodeLocation)
                                        
                                        self.clkPlaceEnd = clkPlaceEnd_
                                    }else{
                                        logger.error("end_location.cllocationCoordinate2D is nil")
                                    }
                                }else{
                                    logger.error("legFirst.end_location is nil")
                                }
                            }else{
                                logger.error("legFirst.end_address is nil")
                            }
                        }
                        
                    }else{
                        logger.error("clkGoogleDirectionRoute.legs. count not > 0 - cant get trip info")
                    }
                }else{
                    logger.error("clkGoogleDirectionRoute.legs is nil - cant get trip info")
                }
                
                //---------------------------------------------------------------------
            }//for
        }else{
            logger.error("clkGoogleDirectionsResponse.result is nil - OK if /api/directions use .results")
        }
        
    }
}
