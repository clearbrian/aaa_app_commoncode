//
//  TFLApiPlaceMapDirectionsViewController.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 19/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps

class TFLApiPlaceMapDirectionsViewController: ParentViewController, CurrentLocationManagerDelegate, GMSMapViewDelegate, COLCGoogleDirectionsUIManagerDelegate {
    
    //var tflPlaceResponse = TFLPlaceResponse()
    
    //delegate may send current position and map jumps back
    var userDidChangeCameraPosition : Bool = false
    var mapMovedOkToCallWS = true
    
    //inti in viewDidLoad AFTER mapView and this VC fully init'd
    var colcGoogleDirectionsUIManager : COLCGoogleDirectionsUIManager?
    
    @IBOutlet weak var segmentedControlTravelMode: UISegmentedControl!
    
    
    @IBOutlet weak var gmsMapView: GMSMapView!
    
    
    @IBOutlet weak var labelName: UILabel!
    
    var currentMapLocation: CLLocation?
    //to stop ws called too much get dist in between
    var prevMapLocation: CLLocation?
    
    
    //didTapOnMarker
    var selectedTFLApiPlace : TFLApiPlace?
    
    //--------------------------------------------------------------
    // MARK: - VIEW DID LOAD
    // MARK: -
    //--------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Directions"
        
        //---------------------------------------------------------------------
        self.clkPlaceStart = appDelegate.currentLocationManager.currentUserPlace
        //---------------------------------------------------------------------
        if let selectedTFLApiPlace = self.selectedTFLApiPlace {
            if let clLocation = selectedTFLApiPlace.clLocation {
                self.clkPlaceEnd = CLKPlace.init(clLocation: clLocation, name: selectedTFLApiPlace.commonNameSafe, clkPlaceWrapperType: .geoCodeLocation)
                
            }else{
                appDelegate.log.error("selectedTFLApiPlace.clLocation is nil")
            }
        }else{
            appDelegate.log.error("self.selectedTFLApiPlace is nil")
        }
        
        
        //---------------------------------------------------------
        //map
        //---------------------------------------------------------
        configureMapView()
        
        addSelectedToMap()
        
        //---------------------------------------------------------------------
        //init only after map and this VC fully init'd
        //---------------------------------------------------------------------
        if let gmsMapView = self.gmsMapView {
            self.colcGoogleDirectionsUIManager = COLCGoogleDirectionsUIManager(gmsMapView: gmsMapView, parentViewController: self)
            self.colcGoogleDirectionsUIManager?.delegate = self
            
            showOrHideDirectionsOverlayFromWS()
        }else{
            self.log.error("self.mapView is nil - cant init COLCGoogleDirectionsUIManager")
        }
    }
    
    
    func displayTaxiName(){
        //reset everytime we get directions as dist/duration appended
        self.labelName.text = ""
        //---------------------------------------------------------
        //growing label
        //---------------------------------------------------------
        if let selectedTFLApiPlace = self.selectedTFLApiPlace {
            if let commonName = selectedTFLApiPlace.commonName {
                
                self.addToLabelName(lineString:commonName)
                
            }else{
                self.log.error("selectedTFLApiPlace.commonName is nil")
            }
        }else{
            self.log.error("self.selectedTFLApiPlace ")
        }
    }
    
    
    //grows
    //check for optional else it appears in the string
    
    //make should label lines is 0 for dynamic text to grow
    func addToLabelName(lineString: String?){
        
        if let labelNameText = self.labelName.text {
            if let lineString = lineString {
                
                if labelNameText == ""{
                    self.labelName.text = lineString
                }else{
                    
                    self.labelName.text = "\(labelNameText)\r\(lineString)"
                }
            }else{
                appDelegate.log.error("lineString is nil")
            }
        }else{
            appDelegate.log.error("self.labelName.text is nil")
        }
        
    }
    
    
    func configureMapView(){
        self.gmsMapView.delegate = self
        self.gmsMapView.isMyLocationEnabled = true
        self.gmsMapView.isBuildingsEnabled = true
        self.gmsMapView.isTrafficEnabled = false
        
        self.gmsMapView.settings.compassButton = true;
        self.gmsMapView.settings.myLocationButton = true;
        self.gmsMapView.settings.indoorPicker = true;
        
        //needed for when user taps My location button twice it rotate in direction they are facing
        self.gmsMapView.settings.rotateGestures = true;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //--------------------------------------------------------------
    // MARK: - MAP
    // MARK: -
    //--------------------------------------------------------------
    func addSelectedToMap(){
        if let selectedTFLApiPlace = self.selectedTFLApiPlace {
            if let gmsMarker = selectedTFLApiPlace.gmsMarker {
                gmsMarker.map = self.gmsMapView
                
            }else{
                appDelegate.log.error("tflApiPlace.gmsMarker  is nil")
            }
        }else{
            appDelegate.log.error("self.selectedTFLApiPlace is nil")
        }
    }
    
    
    
    //--------------------------------------------------------------
    // MARK: - DIRECTIONS
    // MARK: -
    //--------------------------------------------------------------
    
    var clkPlaceStart: CLKPlace?
    var clkPlaceEnd: CLKPlace?
    
    
    func showOrHideDirectionsOverlayFromWS(){
        //reset label as duration/dist appended
        displayTaxiName()
        
        if let colcGoogleDirectionsUIManager = self.colcGoogleDirectionsUIManager {
            
            if let clkPlaceStart = self.clkPlaceStart {
                if let clkPlaceEnd = self.clkPlaceEnd {
                    
                    if clkPlaceStart === clkPlaceEnd{
                        self.log.error("origin and destination are same - can happen when you swap")
                    }else{
                        
                        //------------------------------------------------------------------------------------------------
                        //TRAVEL MODE
                        //------------------------------------------------------------------------------------------------
                        var clkGoogleDirectionsTravelMode: CLKGoogleDirectionsTravelMode = .walking
                        
                        if self.segmentedControlTravelMode.selectedSegmentIndex == 0{
                            clkGoogleDirectionsTravelMode = .walking
                            
                        }else if self.segmentedControlTravelMode.selectedSegmentIndex == 1{
                            clkGoogleDirectionsTravelMode = .driving
                            
                        }else{
                            self.log.error("UNHANDLED : segmentedControlTravelMode")
                        }
                        //------------------------------------------------------------------------------------------------
                        
                        
                        colcGoogleDirectionsUIManager.getDirections(clkPlaceStart: clkPlaceStart,
                                                                    clkPlaceEnd: clkPlaceEnd,
                                                                    clkGoogleDirectionsTravelMode: clkGoogleDirectionsTravelMode,
                                                                    
                                                                    success:{(clkGoogleDirectionsResponse: CLKGoogleDirectionsResponse?) -> Void in
                                                                        //---------------------------------------------------------------------
                                                                        if let clkGoogleDirectionsResponse = clkGoogleDirectionsResponse {
                                                                            colcGoogleDirectionsUIManager.handleCLKGoogleDirectionsResponse(clkGoogleDirectionsResponse)
                                                                        }else{
                                                                            self.log.error("clkGoogleDirectionsResponse is nil")
                                                                        }
                                                                        //---------------------------------------------------------------------
                                                                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                                                        //---------------------------------------------------------------------
                        },
                                                                    failure:{(error) -> Void in
                                                                        
                                                                        self.log.error("error:\(error)")
                                                                        if let nserror = error {
                                                                            colcGoogleDirectionsUIManager.handleError(nserror)
                                                                            
                                                                        }else{
                                                                            self.log.error("error is nil")
                                                                        }
                                                                        
                                                                        //self.googleDirectionsSearchRunning = false
                                                                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        })
                        //------------------------------------------------------------------------------------------------
                        
                    }
                }else{
                    //clkPlaceEnd is nil - do nothing till both retrieved
                    colcGoogleDirectionsUIManager.removeTripFromMap()
                }
            }else{
                //clkPlaceStart is nil - do nothing till both retrieved
                colcGoogleDirectionsUIManager.removeTripFromMap()
            }
        }else{
            self.log.error("self.colcGoogleDirectionsUIManager is nil")
        }
    }
    
    
    //--------------------------------------------------------------
    // MARK: - segmentedControlTravelMode
    // MARK: -
    //--------------------------------------------------------------
    
    @IBAction func segmentedControlTravelMode_ValueChanged(_ sender: Any) {
        if let colcGoogleDirectionsUIManager = self.colcGoogleDirectionsUIManager {
            
            colcGoogleDirectionsUIManager.removeTripFromMap()
            
            showOrHideDirectionsOverlayFromWS()
            
        }else{
            self.log.error("self.colcGoogleDirectionsUIManager is nil")
        }
        
    }
    
    
    
    
    
    
    
    
    
    //--------------------------------------------------------------
    // MARK: - CurrentLocationManagerDelegate
    // MARK: -
    //--------------------------------------------------------------
    func currentLocationUpdated(currentLocationManager: CurrentLocationManager, currentLocation: CLLocation){
        //log.error("TODO currentLocationUpdated currentLocation:\n\(currentLocation)")
        
        //self.gmsMapView.animateToLocation(location: currentLocation, atZoom: GoogleMapsZoomLevel.zoomlevel17_STREET)
        // TODO: - move out to show taxi AND current position
        moveToCurrentLocation()
    }
    
    
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - LOCATION/MAP
    //--------------------------------------------------------------
    //nearbysearch is triggered AFTER move finished in idleAtCameraPosition:
    func moveToCurrentLocation(){
        if self.userDidChangeCameraPosition{
            //NOISY
            //self.log.info("moveToCurrentLocation - userDidChangeCameraPosition is true DONT move to new currentLocation")
        }else{
            //NOISY
            //self.log.info("moveToCurrentLocation - userDidChangeCameraPosition is false DO move to new currentLocation")
            //map at default so ok to move to current location
            
            if let currentLocation = appDelegate.currentLocationManager.currentUserPlace.clLocation {
                //NOISY: self.log.info("LOC: [moveToCurrentLocation:]about to call  moveMapToCLLocation(currentLocation:\(currentLocation))")
                moveMapToCLLocation(currentLocation)
                
            }else{
                self.log.error("currentUserPlace.clLocation is nil")
            }
            
        }
    }
    
    //called every time a new device location is recieved - UNLESS user manually moves the map then thats disabled
    //called when you tap on a row in the table
    func moveMapToCLLocation(_ clLocation : CLLocation){
        //------------------------------------------------------------------------------------------------
        if clLocation.isValid{
            //-----------------------------------------------------------------------------------
            //zoom in if very far out - when app starts can be a zoom 4.0
            var zoom: Float = self.gmsMapView.camera.zoom
            if self.gmsMapView.camera.zoom < 17{
                zoom = 17
            }
            //-----------------------------------------------------------------------------------
            let cameraPosition = GMSCameraPosition.camera(withLatitude: clLocation.coordinate.latitude,
                                                          longitude:clLocation.coordinate.longitude,
                                                          zoom:zoom)
            //var mapView = GMSMapView.mapWithFrame(CGRectZero, camera:camera)
            //NOISY self.log.warning("LOC: animateToCameraPosition( cameraWithLatitude:[\(clLocation.coordinate.latitude),\(clLocation.coordinate.longitude)")
            self.gmsMapView.animate(to: cameraPosition)
            //triggered AFTER move complete in idleAtCameraPosition:
            //self.nearbySearch(currentLocation)
            //-----------------------------------------------------------------------------------
        }else{
            self.log.debug("LOC: [moveMapToCLLocation(clLocation:\(clLocation))] clLocation.isValid is false - dont call animateToCameraPosition")
        }
        
    }
    
    
    
    func currentLocationFailed(currentLocationManager: CurrentLocationManager, error: Error?)
    {
        self.log.error("currentLocationFailed: error:\(error) - MAP MAY BE STUCK")
    }
    
    
    
    //--------------------------------------------------------------
    // MARK: - GMSMapViewDelegate - didTap marker
    // MARK: -
    //--------------------------------------------------------------
    public func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool{
        
        appDelegate.log.debug("didTapmarker:\(marker.title)")
        
        //TODO: - put the pop up as text fields under the map view with button VIEW
        //does app doesnt display popup? - no > mapView should
        
        if let _ = marker.userData as? TFLApiPlace {
            //showAlertOptions(tflApiPlace)
            //dont show popup
            return true;
        }else{
            appDelegate.log.error("self.selectedTFLApiPlace is nil")
            // show popup
            return false;
        }
    }
    
    //    func showAlertOptions(_ tflApiPlace : TFLApiPlace){
    //
    //        self.selectedTFLApiPlace = tflApiPlace
    //
    //        let alertActionCancel = UIAlertAction.alertActionCancel
    //
    //        let alertActionDirections = UIAlertAction(title: "Directions", style: UIAlertActionStyle.default) { (alertAction) -> Void in
    //            print("TODO Directions pressed")
    //        }
    //
    //        let alertActionStreetView = UIAlertAction(title: "StreetView", style: UIAlertActionStyle.default) { (alertAction) -> Void in
    //            self.performSegue(withIdentifier: "segueTFLApiPlaceMapViewControllerTOTFLApiPlaceStreetViewController", sender: nil)
    //
    //        }
    //
    //        CLKAlertController.showSheetWithActions(vc: self, title: tflApiPlace.alertTitle,
    //                                                message: tflApiPlace.alertMessage,
    //                                                alertActions: [alertActionCancel, alertActionDirections, alertActionStreetView])
    //    }
    
    @IBAction func buttonOpenIn_Action(_ sender: AnyObject) {
        self.performSegue(withIdentifier: segueTFLApiPlaceMapDirectionsViewControllerTOOpenWithViewController, sender: nil)
        
    }
    
    //--------------------------------------------------------------
    // MARK: - prepareForSegue
    // MARK: -
    //--------------------------------------------------------------
    //    let segueTFLApiPlaceMapViewControllerTOTFLApiPlaceSingleTableViewController = "segueTFLApiPlaceMapViewControllerTOTFLApiPlaceSingleTableViewController"
    //    let segueTFLApiPlaceMapViewControllerTOTFLApiPlaceStreetViewController = "segueTFLApiPlaceMapViewControllerTOTFLApiPlaceStreetViewController"
    
    
    let segueTFLApiPlaceMapDirectionsViewControllerTOOpenWithViewController = "segueTFLApiPlaceMapDirectionsViewControllerTOOpenWithViewController"
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == segueTFLApiPlaceMapDirectionsViewControllerTOOpenWithViewController){
            
            if(segue.destination.isMember(of: OpenWithViewController.self)){
                
                let openWithViewController = (segue.destination as! OpenWithViewController)
                
                openWithViewController.clkPlaceStart = self.clkPlaceStart
                openWithViewController.clkPlaceEnd = self.clkPlaceEnd
                
                //                //openWithViewController.delegate = self
                
            }else{
                self.log.error("destinationViewController not OpenWithViewController")
            }
        }
        else{
            self.log.error("UNHANDLED SEGUE:\(segue.identifier)")
        }
    }
    
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - GMSMapViewDelegate
    //--------------------------------------------------------------
    
    //Called before the camera on the map changes, either due to a gesture, animation (e.g., by a user tapping on the “My Location” button)
    //or by being updated explicitly via the camera or a zero-length animation on layer.
    public func mapView(_ mapView: GMSMapView, willMove gesture: Bool){
        
        
        if gesture{
            //OK NOISY - self.log.info("willMove gesture:\(gesture) self.userDidChangeCameraPosition = true")
            self.userDidChangeCameraPosition = true
            //if user moves map with their hand then should recall ws
            self.mapMovedOkToCallWS = true
        }else{
            
            //or map moving because device location sending notification (and user hasnt manually moved the map to block this)
            ////USER TAPPED ON MYLOCATION BUTTON resets userDidChangeCameraPosition
            //OK NOISY - self.log.info("willMove gesture:\(gesture) - map moved because device location notif sent or user tapped MY LOC button")
        }
        
    }
    /**
     * Called repeatedly during any animations or gestures on the map (or once, if
     * the camera is explicitly set). This may not be called for all intermediate
     * camera positions. It is always called for the final position of an animation
     * or gesture.
     */
    public func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition){
        
        //        self.log.info("didChangeCameraPosition")
        //delegate may send current position and map jumps back, need flag to prevent this
        //
        
    }
    
    func clLocationForCLLocationCoordinate2D(_ clLocationCoordinate2D : CLLocationCoordinate2D) -> CLLocation{
        let location: CLLocation = CLLocation(latitude: clLocationCoordinate2D.latitude, longitude: clLocationCoordinate2D.longitude)
        return location
    }
    /**
     * Called when the map becomes idle, after any outstanding gestures or
     * animations have completed (or after the camera has been explicitly set).
     */
    
    //--------------------------------------------------------------
    // MARK: - MOVE MAP - CALL GOOGLE NEARBY and GEOLOCATE
    // MARK: -
    //--------------------------------------------------------------
    
    public func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition){
        //self.log.warning("LOC: [idleAtCameraPosition] START")
        
        //self.log.info("idleAtCameraPosition: position:\(position.target)")
        
        
        //------------------------------------------------------------------------------------------------
        //UPDATE CURRENT
        //------------------------------------------------------------------------------------------------
        //position.target: CLLocationCoordinate2D
        let location: CLLocation = clLocationForCLLocationCoordinate2D(position.target)
        
        if location.isValid == false{
            self.log.warning("LOC: [idleAtCameraPosition] location.isValid is FALSE - map pointing at blue equator - move it - calling moveToCurrentLocation")
            self.moveToCurrentLocation()
        }else{
            //self.log.warning("LOC: [idleAtCameraPosition] location.isValid - continue")
            
            //------------------------------------------------
            //                position.bearing        //CLLocationDirection :Double
            //                position.target         //CLLocationCoordinate2D
            //                position.viewingAngle   //Double
            //                position.zoom           //Float
            
            //OK just not needed
            //        if let mapView_projection = self.mapView.projection{
            //            let gmsVisibleRegion = mapView_projection.visibleRegion()
            //
            ////            /** Bottom left corner of the camera. */
            ////            var nearLeft: CLLocationCoordinate2D
            ////
            ////            /** Bottom right corner of the camera. */
            ////            var nearRight: CLLocationCoordinate2D
            ////
            ////            /** Far left corner of the camera. */
            ////            var farLeft: CLLocationCoordinate2D
            ////
            ////            /** Far right corner of the camera. */
            ////            var farRight: CLLocationCoordinate2D
            //
            //
            //            self.log.info("  farLeft:\(gmsVisibleRegion.farLeft.latitude), \(gmsVisibleRegion.farLeft.longitude)")
            //            self.log.info(" farRight:\(gmsVisibleRegion.farRight.latitude), \(gmsVisibleRegion.farRight.longitude)")
            //            self.log.info(" nearLeft:\(gmsVisibleRegion.nearLeft.latitude), \(gmsVisibleRegion.nearLeft.longitude)")
            //            self.log.info("nearRight:\(gmsVisibleRegion.nearRight.latitude), \(gmsVisibleRegion.nearRight.longitude)")
            //            self.log.info("")
            //        }else{
            //            self.log.error("self.mapView.projectionis nil")
            //        }
            //------------------------------------------------------------------------------------------------
            //PREVENT TOO MUCH WS CALLS if map only moves small bit
            //------------------------------------------------------------------------------------------------
            var mapMovedEnoughToCallWS = false
            //shut up warning
            print(mapMovedEnoughToCallWS)
            //is this first location retrieved? if not then backup current
            if let currentMapLocation = self.currentMapLocation{
                //---------------------------------------------
                //prev and current set
                //---------------------------------------------
                //Backup current before updating it below
                self.prevMapLocation = currentMapLocation
                
            }else{
                self.log.warning("LOC: [idleAtCameraPosition] currentMapLocation is nil - map first appearing idleAtCameraPosition")
            }
            
            
            //------------------------------------------------------------------------------------------------
            //UPDATE CURRENT
            //------------------------------------------------------------------------------------------------
            //position.target: CLLocationCoordinate2D
            
            //            if location.isValid{
            //                self.log.warning("LOC: [idleAtCameraPosition] location.isValid use as currentMapLocation")
            //            }else{
            //
            //                if let currentLocation = appDelegate.currentLocation {
            //                    self.log.warning("LOC: [idleAtCameraPosition] location.isValid FALSE - using currentLocation:[\(currentLocation.coordinate.latitude),\(currentLocation.coordinate.longitude)")
            //                    location = currentLocation
            //                }else{
            //                    self.log.warning("LOC: [idleAtCameraPosition] location.isValid FALSE - appDelegate.currentLocation is nil - location may be invalid")
            //                }
            //
            //            }
            
            //needed for querysearch / place search - where to center your search from
            self.currentMapLocation = location
            
            //------------------------------------------------------------------------------------------------
            //if dist between prev and current large enough refresh the results
            //------------------------------------------------------------------------------------------------
            
            if let prevMapLocation = self.prevMapLocation{
                if let currentMapLocation = self.currentMapLocation{
                    //------------------------------------------------------------------------------------------------
                    //GET dist between the PREV /CURRENT map locations
                    //------------------------------------------------------------------------------------------------
                    
                    //FROM = e.g. MAP CENTER
                    let from: CLLocationCoordinate2D =  CLLocationCoordinate2D(latitude: prevMapLocation.coordinate.latitude, longitude: prevMapLocation.coordinate.longitude)
                    //print("from:\(from)")
                    
                    //TO = Place result location SELF
                    let to: CLLocationCoordinate2D =  CLLocationCoordinate2D(latitude: currentMapLocation.coordinate.latitude, longitude: currentMapLocation.coordinate.longitude)
                    //print("to:\(from)")
                    //GoogleMaps function - returns meters
                    let distanceMetersDouble : CLLocationDistance =  GMSGeometryDistance(from, to)
                    
                    
                    // TODO: - WHEN ZOOMED IN THIS CAN FAIL TO BE TRIGGERED
                    let maxDist = 10.0
                    if distanceMetersDouble > maxDist{
                        //self.log.debug("distanceMetersDouble:\(distanceMetersDouble) GREATER THAN \(maxDist)m - call WS")
                        mapMovedEnoughToCallWS = true
                    }else{
                        //self.log.debug("distanceMetersDouble:\(distanceMetersDouble) LESS THAN \(maxDist)m - dont call WS")
                        mapMovedEnoughToCallWS = false
                    }
                    
                    //------------------------------------------------------------------------------------------------
                    
                }else{
                    self.log.debug("currentMapLocation is nil - ok to update map")
                    mapMovedEnoughToCallWS = true
                }
            }else{
                self.log.debug("prevMapLocation is nil - ok to update map")
                mapMovedEnoughToCallWS = true
            }
            //-----------------------------------------------------------------------------------
            if mapMovedOkToCallWS{
                //update Map and Table
                
                //self.sonarView.startAnimation()
                //self.log.warning("LOC: [idleAtCameraPosition] CALL nearbySearch()")
                //self.nearbySearch(location)
                //-----------------------------------------------------------------------------------
                //move down - always get
                ///Convert lat/lng to address
                //self.geocodeLocation(location.coordinate)
                //-----------------------------------------------------------------------------------
            }else{
                self.log.warning("LOC: [idleAtCameraPosition] mapMovedOkToCallWS is FALSE - DONT CALL NEARBY SEARCH OR GEOCODE LOCATION")
            }
            
            //------------------------------------------------
            //GEOCODE LOCATION
            //------------------------------------------------
            ///Convert lat/lng to address
            //self.geocodeCenterOfMapLocation(location)
            
            //------------------------------------------------
        }
        
    }
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - TAP/PRESS at COOORD
    //--------------------------------------------------------------
    /**
     * Called after a tap gesture at a particular coordinate, but only if a marker
     * was not tapped.  This is called before deselecting any currently selected
     * marker (the implicit action for tapping on the map).
     */
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D){
        self.log.info("mapView: didTapAtCoordinate")
    }
    /**
     * Called after a long-press gesture at a particular coordinate.
     *
     * @param mapView The map view that was pressed.
     * @param coordinate The location that was pressed.
     */
    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D){
        self.log.info("mapView: didLongPressAtCoordinate")
    }
    
    //--------------------------------------------------------------
    // MARK: - COLCGoogleDirectionsUIManagerDelegate
    // MARK: -
    //--------------------------------------------------------------
    func colcGoogleDirectionsUIManager(_ colcGoogleDirectionsUIManager: COLCGoogleDirectionsUIManager, clkGoogleDirectionsResponse : CLKGoogleDirectionsResponse ){
        //if we get route it zooms out to line, but user hasnt moved map so will jump to cur loc
        self.userDidChangeCameraPosition = true
        
        if let routes = clkGoogleDirectionsResponse.routes{
            
            
            //map is update in COLCGoogleDirectionsUIManager
            for clkGoogleDirectionRoute : CLKGoogleDirectionRoute in routes{
                //---------------------------------------------------------------------
                //ROUTE - 1 leg - could never get it to have more than 1 leg - only n steps in 1 leg in 1 route
                //---------------------------------------------------------------------
                
                if let legs: [CLKGoogleLeg] = clkGoogleDirectionRoute.legs{
                    if legs.count > 0{
                        
                        if legs.count != 1{
                            appDelegate.log.error("legs.count != 1")
                            
                        }else{
                            //----------------------------------------------------------------------------------------
                            //FIRST LOG ONLY
                            //----------------------------------------------------------------------------------------
                            // TODO: - Support multiple legs
                            let legFirst: CLKGoogleLeg = legs[0]
                            //---------------------------------------------------------------------
                            // distance: CLKGoogleMeasurement
                            //---------------------------------------------------------------------
                            if let distance: CLKGoogleMeasurement = legFirst.distance {
                                //----------------------------------------------------------------------------------------
                                self.log.info("distance[distanceMetricSafe]:\(distance.distanceMetricSafe)")
                                self.log.info("distance[distanceMilesSafe]:\(distance.distanceMilesSafe)")
                                
                                self.addToLabelName(lineString:"Distance: \(distance.distanceMilesSafe) \(distance.distanceMetricSafe)")
                                
                            }else{
                                appDelegate.log.error("legFirst.distance is nil - cant get trip info")
                            }
                            //---------------------------------------------------------------------
                            // distance: CLKGoogleMeasurement
                            //---------------------------------------------------------------------
                            if let duration: CLKGoogleMeasurement = legFirst.duration {
                                
                                if let text = duration.text {
                                    self.log.info("duration[text]:\(text)")
                                    
                                    self.addToLabelName(lineString:"Duration: \(text)")
                                    
                                    
                                }else{
                                    appDelegate.log.error("duration.text is nil")
                                }
                                
                                if let value = duration.value {
                                    self.log.info("duration[secs]:\(value)")
                                }else{
                                    appDelegate.log.error("distance.text is nil")
                                }
                                
                            }else{
                                appDelegate.log.error("legFirst.duration is nil - cant get trip info")
                            }
                            //----------------------------------------------------------------------------------------
                            //FIRST LEG ONLY - END
                            //----------------------------------------------------------------------------------------
                            
                            
                            //----------------------------------------------------------------------------------------
                            //update start/end with geocoded address - in case we open in google maps
                            //----------------------------------------------------------------------------------------
                            if let start_address = legFirst.start_address {
                                if let start_location = legFirst.start_location {
                                    if let cllocationCoordinate2D = start_location.cllocationCoordinate2D {
                                        
                                        let clLocation = CLLocation.init(latitude: cllocationCoordinate2D.latitude, longitude: cllocationCoordinate2D.longitude)
                                        let clkPlaceStart_ = CLKPlace.init(clLocation: clLocation,
                                                                           name: start_address,
                                                                           clkPlaceWrapperType: .geoCodeLocation)
                                        
                                        self.clkPlaceStart = clkPlaceStart_
                                    }else{
                                        appDelegate.log.error("start_location.cllocationCoordinate2D is nil")
                                    }
                                }else{
                                    appDelegate.log.error("legFirst.start_location is nil")
                                }
                            }else{
                                appDelegate.log.error("legFirst.start_address is nil")
                            }
                            
                            if let end_address = legFirst.end_address {
                                if let end_location = legFirst.end_location {
                                    if let cllocationCoordinate2D = end_location.cllocationCoordinate2D {
                                        
                                        let clLocation = CLLocation.init(latitude: cllocationCoordinate2D.latitude, longitude: cllocationCoordinate2D.longitude)
                                        let clkPlaceEnd_ = CLKPlace.init(clLocation: clLocation,
                                                                         name: end_address,
                                                                         clkPlaceWrapperType: .geoCodeLocation)
                                        
                                        self.clkPlaceEnd = clkPlaceEnd_
                                    }else{
                                        appDelegate.log.error("end_location.cllocationCoordinate2D is nil")
                                    }
                                }else{
                                    appDelegate.log.error("legFirst.end_location is nil")
                                }
                            }else{
                                appDelegate.log.error("legFirst.end_address is nil")
                            }
                        }
                        
                    }else{
                        appDelegate.log.error("clkGoogleDirectionRoute.legs. count not > 0 - cant get trip info")
                    }
                }else{
                    appDelegate.log.error("clkGoogleDirectionRoute.legs is nil - cant get trip info")
                }
                
                //---------------------------------------------------------------------
            }//for
        }else{
            self.log.error("clkGoogleDirectionsResponse.result is nil - OK if /api/directions use .results")
        }
        
    }
}
