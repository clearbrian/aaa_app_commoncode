//
//  TFLApiPlaceStreetViewController.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 19/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class TFLApiPlaceStreetViewController: COLCGoogleStreetViewController {
    
    let emailManager = EmailManager()
    
    //segue
    var selectedTFLApiPlace : TFLApiPlace?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Street View"
    
        
        if let selectedTFLApiPlace = self.selectedTFLApiPlace {
            
            //----------------------------------------------------------------------------------------
            //genericListTableViewCell.applyCustomFont() - done in super
            //----------------------------------------------------------------------------------------
            self.labelTitle?.text = selectedTFLApiPlace.textLabelText()
            
            self.labelDetail0?.text = selectedTFLApiPlace.detailLabelText0()
            self.labelDetail1?.text = selectedTFLApiPlace.detailLabelText1()
            
            self.labelSubDetail0?.text = selectedTFLApiPlace.subDetailLabelText0()
            self.labelSubDetail1?.text = selectedTFLApiPlace.subDetailLabelText1()
            //----------------------------------------------------------------------------------------
        }else{
            logger.error("self.selectedTFLApiPlace is nil")
        }
        
        //done in super configureNavBar()
    }
    
    
    
 
    
    override var clLocation: CLLocation?{
        var clLocation_: CLLocation? = nil
        
        if let selectedTFLApiPlace = self.selectedTFLApiPlace {
            if let clLocation = selectedTFLApiPlace.clLocation {
                clLocation_ =  clLocation
            }else{
                logger.error("selectedTFLApiPlace.clLocation is nil")
            }
        }else{
            logger.error("self.selectedTFLApiPlace is nil")
        }
        return clLocation_
    }
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - MARKER IMAGE
    //--------------------------------------------------------------
    //Subclass - couldnt get animated images to work on streetview
    //    override var animatedMarkerImageNames: [String]{
    //        return ["arrowdart0", "arrowdart1"]
    //    }
    
    override var singleMarkerImageName: String{
        return "streeviewpole"
    }
    

    
    
    override func showAlertOptions(){
        logger.error("SUBCLASS")
        if let selectedTFLApiPlace = self.selectedTFLApiPlace {
            showAlertOptions(selectedTFLApiPlace)
        }else{
            logger.error("self.selectedTFLApiPlace is nil")
        }
    }
    
    
    func showAlertOptions(_ tflApiPlace : TFLApiPlace){
        
        self.selectedTFLApiPlace = tflApiPlace
        
        let alertActionCancel = UIAlertAction.alertActionCancel
        //----------------------------------------------------------------------------------------
        let alertActionDirections = UIAlertAction(title: "Directions", style: .default) { (alertAction) -> Void in
            
            self.showDirections()
            
        }
        
        
        //---------------------------------------------------------
        //"Report Problem"
        //---------------------------------------------------------
        let alertReportProblem = UIAlertAction(title: "Report Problem", style: .default) { (alertAction) -> Void in
            if let selectedTFLApiPlace = self.selectedTFLApiPlace {
                var placeType_ = "Place"
                
                if let placeType = selectedTFLApiPlace.placeType {
                    placeType_ = placeType
                }else{
                    logger.error("selectedTFLApiPlace.placeType is nil")
                }
                
                self.emailManager.sendEmailFromVC(self, bodyString: "User Reported Error with \(placeType_):\n['\(selectedTFLApiPlace.idSafe)':'\(Safe.safeString(selectedTFLApiPlace.commonName))']\n Please describe your problem below and we will forward it to TFL.\n")
            }else{
                logger.error("self.selectedTFLApiPlace is nil")
            }
        }
        
        
        //----------------------------------------------------------------------------------------
        CLKAlertController.showSheetWithActions(vc: self, title: tflApiPlace.alertTitle,
                                                message: tflApiPlace.alertMessage,
                                                alertActions: [alertActionCancel, alertActionDirections, alertReportProblem])
        //----------------------------------------------------------------------------------------
    }
    
    
    
    
    
    override var segueTo_TFLApiPlaceMapDirectionsViewController: String{
        return "segueTFLApiPlaceStreetViewControllerTOTFLApiPlaceMapDirectionsViewController"
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == segueTo_TFLApiPlaceMapDirectionsViewController){
 
            if(segue.destination.isMember(of: TFLApiPlaceMapDirectionsViewController.self)){
                
                let tflApiPlaceMapDirectionsViewController = (segue.destination as! TFLApiPlaceMapDirectionsViewController)
                //tflApiPlaceMapDirectionsViewController = self
                tflApiPlaceMapDirectionsViewController.selectedTFLApiPlace = self.selectedTFLApiPlace
                
            }
            else{
                print("ERROR:not TFLApiPlaceMapDirectionsViewController")
            }
        }
        else{
            logger.error("UNHANDLED SEGUE:\(String(describing: segue.identifier))")
        }
    }
}
