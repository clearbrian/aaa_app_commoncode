//
//  TFLApiPlaceListViewController.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 17/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import AVKit

class TFLApiPlaceListViewController: GenericListViewController, TFLGeoJSONManagerDelegate, COLCQueryGenericListOptionsViewControllerDelegate{
    
    let emailManager = EmailManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //-----------------------------------------------------------------------------------
        //SUBCLASS - for each TFLApiPlaceType
        //-----------------------------------------------------------------------------------
        configureVC()
        configureNavBar()

        //-----------------------------------------------------------------------------------
        //DYNAMIC TEXT and TABLE ROW HEIGHT
        //-----------------------------------------------------------------------------------
        self.config_tableView_dynamicHeight(estimatedRowHeight: 80.0)
        
        //-----------------------------------------------------------------------------------
        //GET DATA - may be cached
        //-----------------------------------------------------------------------------------
        //ws called once - after that uses cached var - unless you pull to refresh
        self.callWebService(forceNewWSCall: false)
        // TODO: - 24mar
        //wrong called when we Directions then back
        //also get current query called when search in place
        //-----------------------------------------------------------------------------------
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }

    func configureVC(){
        self.title = AppConfig.tflApiPlaceType.title
    }
    
    //SUBLCASS
    override func configureNavBar() {
        super.configureNavBar()
        
        //----------------------------------------------------------------------------------------
        //Menu
        //----------------------------------------------------------------------------------------
        //        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Menu",
        //                                                           style: .plain,
        //                                                           target: self,
        //                                                           action: #selector(TFLApiPlaceListViewController.leftBarButtonItem_Action))
        //        //----------------------------------------------------------------------------------------
        //        //Map
        //        //----------------------------------------------------------------------------------------
        //        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Map",
        //                                                            style: .plain,
        //                                                            target: self,
        //                                                            action: #selector(TFLApiPlaceListViewController.rightBarButtonItem_Action))
        //----------------------------------------------------------------------------------------
        //NOTE IMAGES WILL BE DISPLAY B&W and in neegative so avid colored icons
        //icon_info_triangle
        
        //        if let imageInfoTriangle = UIImage.init(named: "info2_30x30") {
        //            navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: imageInfoTriangle,
        //                                                                     style: .plain,
        //                                                                     target: self,
        //                                                                     action: #selector(TFLApiPlaceListViewController.leftBarButtonItem_Action))
        //
        //            //navigationItem.leftBarButtonItem?.tintColor = UIColor.darkGray
        //            navigationItem.leftBarButtonItem?.tintColor = AppearanceManager.appColorLight0
        //            //navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        //        }else{
        //            logger.error("imageInfoTriangle is nil")
        //        }
        //----------------------------------------------------------------------------------------
        //now in tab
        //    if let imageMap = UIImage.init(named: "icon_map_simple") {
        //        navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: imageMap,
        //                                                                 style: .plain,
        //                                                                 target: self,
        //                                                                 action: #selector(TFLApiPlaceListViewController.rightBarButtonItem_Action))
        //
        //        navigationItem.rightBarButtonItem?.tintColor = AppearanceManager.appColorLight0
        //
        //    }else{
        //        logger.error("imageMap is nil")
        //    
    
        //----------------------------------------------------------------------------------------
        //INFO BUTTON - on nav bar - put on left as easy to hit when you hit search
        //----------------------------------------------------------------------------------------
        
        if let imageInfoTriangle = UIImage.init(named: "info2_30x30") {
            navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: imageInfoTriangle,
                                                                     style: .plain,
                                                                     target: self,
                                                                     action: #selector(leftBarButtonItem_Action))
            navigationItem.leftBarButtonItem?.tintColor = AppearanceManager.appColorLight0
            
        }else{
            logger.error("imageInfoTriangle is nil")
        }
        //----------------------------------------------------------------------------------------
    }
    
    @objc override func leftBarButtonItem_Action() {
       //dont add - need Back button
        
        self.performSegue(withIdentifier: segueTo_SettingsTableViewController, sender: nil)
    }
    
    override func rightBarButtonItem_Action() {
        
 
    }
    
    override func buttonQuerySort_Action() {
        //OK but too detailed - use action sheet instead
        //self.performSegue(withIdentifier: segueTo_COLCQueryGenericListOptionsViewController, sender: nil)
        
        self.showAlertSheetForCOLCQueryCollection()
    }
    
    
    //--------------------------------------------------------------
    // MARK: - SORT BUTTON
    // MARK: -
    //--------------------------------------------------------------
    
    func showAlertSheetForCOLCQueryCollection(){
        var alertActions = [UIAlertAction.alertActionCancel]
    
        for index in 0..<self.genericListViewDataSource.colcQueryCollectionForType.colcQueryDictionaryKeys.count{
            
            var queryName = self.genericListViewDataSource.colcQueryCollectionForType.colcQueryDictionaryKeys[index]
            
            if index == self.genericListViewDataSource.colcQueryCollectionForType.currentQueryIndex{
                queryName = "\(queryName) •"
            }
            
            //----------------------------------------------------------------------------------------
            //UIAlertAction - one button per query
            //----------------------------------------------------------------------------------------
            let alertAction = UIAlertAction(title: queryName,
                                            style: .default) { (alertAction) -> Void in
                
                print("queryName chosen:\(queryName)")
                
                self.change_currentQueryIndex(selectedIndex:index)
                
            }
            //----------------------------------------------------------------------------------------
            alertActions.append(alertAction)
            //----------------------------------------------------------------------------------------
        }
        
        //---------------------------------------------------------
        // TODO: - only add jam cam for movi
        CLKAlertController.showSheetWithActions( vc           : self,
                                                 title        : "Sort",
                                                 message      : "Choose an option to sort and group by",
                                                 alertActions : alertActions)
        //---------------------------------------------------------
        
    }
    
    func change_currentQueryIndex(selectedIndex: Int){
        //----------------------------------------------------------------------------------------
        //Change query
        //----------------------------------------------------------------------------------------
        self.searchBar?.text = ""
        
        self.genericListViewDataSource.colcQueryCollectionForType.delegate = self
        
        self.genericListViewDataSource.colcQueryCollectionForType.currentQueryIndexChanged(selectedIndex: selectedIndex)
        //  >> colcQuery_HasChanged

    }
    
    
    //--------------------------------------------------------------
    // MARK: - ParentWSControllerDelegate
    // MARK: -
    //--------------------------------------------------------------

    override func requestStateAlreadyRunning(parentWSController: ParentWSController){
        logger.error("LIST - requestStateAlreadyRunning - ws in progress - ok if you tap on map/list quickly")
        self.showLoadingOnMain()
    }
    
    
   
    override func response_success(parentWSController: ParentWSController, parsedObject: Any){
        
        if let tflApiPlaceResponse : TFLApiPlaceResponse = parsedObject as? TFLApiPlaceResponse {
            
            //----------------------------------------------------------------------------------------
            //RESPONSE OK
            //----------------------------------------------------------------------------------------
            
            logger.debug("tflApiPlaceResponse returned")
            
            //-------------------------------------------------------------------
            //didSet - does nothing
            self.genericListViewDataSource = tflApiPlaceResponse
            
            
            
            //-------------------------------------------------------------------
            //tflApiPlaceResponse is a new instance - copy over the last current query index
            //-------------------------------------------------------------------
            
            //WS returned after pull to refresh - set the currentquery index so it doesnt jump back to Nearest before requery
            if self.currentQueryIndexBeforePullToRefresh == -1{
                logger.debug("tflApiPlaceResponse called when app starts - not pull to refresh")
            }else{
                if self.currentQueryIndexBeforePullToRefresh > 0{
                    //colcQueryDictionaryKeys
                    tflApiPlaceResponse.colcQueryCollectionForType.currentQueryIndex = self.currentQueryIndexBeforePullToRefresh
                }else{
                    logger.debug("self.currentQueryIndexBeforePullToRefresh > 0 failed")
                }
                
            }
            //-------------------------------------------------------------------
            //REQUERY the results
            //-------------------------------------------------------------------

            //in GenericListViewController
            // self.requeryinBackgroundWithCurrentCOLCQuery()
             self.requeryinBackgroundWithCurrentCOLCQueryAndCurrentSearchString()
            //---------------------------------------------------------
            //REQUERY COMPLETE
            //---------------------------------------------------------
            
            
            //-------------------------------------------------------------------
            // TODO: - MOVE OUT OF LIST to TFLController??? commented out in map too
            //store in AppD for map view
            //appDelegate.tflApiPlaceResponse = tflApiPlaceResponse
            
            //---------------------------------------------------------
            //preload the json for GeoJson
            //---------------------------------------------------------
            
            // TODO: - is this cached - called everytime we change quert
            appDelegate.tflGeoJSONManager.delegate = self
            appDelegate.tflGeoJSONManager.loadTFLStationsAndLines(loadStationsFromJSON: false)
            //----------------------------------------------------------------------------------------
        }else{
            logger.error("parsedObject as? TFLApiPlaceResponse is nil")
        }
    }
    
    override func response_failure(parentWSController: ParentWSController, error: Error){
        //----------------------------------------------------------------------------------------
        print(error)
        
        CLKAlertController.showAlertInVC(self,
                                         title: "Error getting information",
                                         message: "Please check your internet connection.\rIf this problem persists please contact support@cityoflondonconsulting.com with error:\r [\(error.localizedDescription)]")
        //----------------------------------------------------------------------------------------
    }
    
    override func reloadTableOnMain(){
        if Thread.isMainThread{
            self.reloadTableAndUpdateCount()
        }else{
            DispatchQueue.main.async{
                self.reloadTableAndUpdateCount()
            }
        }
    }
    
    fileprivate func reloadTableAndUpdateCount(){
        self.tableView.reloadData()
        //self.labelPlaceCount?.text = "\(self.genericListViewDataSource.totalsCount)"
        
        //self.title = "\(AppConfig.tflApiPlaceType.title) (\(self.genericListViewDataSource.totalsCount))"
        
        self.tableView.setContentOffset(CGPoint.init(x: 0.0, y: 0.0), animated: true)
    }
    
    //pull to refresh - forceNewWSCall: true
    //else false so wll used cached version if possible
    override func callWebService(forceNewWSCall: Bool)
    {
        //dont call super - default implementation does nothing
        //super.callWebService()
        
        //----------------------------------------------------------------------------------------
        self.showLoadingOnMain()
        self.labelPlaceCount?.text = ""
        //---------------------------------------------------------------------

        //----------------------------------------------------------------------------------------
        //GET APP DATA - may be cached
        //----------------------------------------------------------------------------------------
        //appDelegate.tflApiWSController.delegate = self
       //appDelegate.tflApiWSController.request_app_TFLApiPlaceTypeRequest_via_Delegates(forceNewWSCall: forceNewWSCall)
        //----------------------------------------------------------------------------------------
        
 
        //let tflApiPlaceRequest = TFLApiPlaceRequest()
        //let tflUnifiedApiPlaceRequest = TFLUnifiedApiPlaceRequest()
        
        //----------------------------------------------------------------------------------------
        //v3 -
        //----------------------------------------------------------------------------------------
        //Jan 2018 - replaced with Unified api - better subclassing
        //let tflApiPlace_GetAt_Request = TFLApiPlace_GetAt_Request(tflApiPlaceType:.taxiRank)
        //let tflApiPlace_GetAt_Request = TFLApiPlace_GetAt_Request(tflApiPlaceType:.jamCam)
        //---------------------------------------------------------------------------
//        let tflApiPlace_GetAt_Request = TFLApiPlace_GetAt_Request(tflApiPlaceType:AppConfig.tflApiPlaceType)
//        logger.info("tflUnifiedApiPlaceRequest:\(String(describing: tflApiPlace_GetAt_Request.urlString))")
//
//        appDelegate.tflUnifiedApiWSController.delegate = self
//        appDelegate.tflUnifiedApiWSController.call_WithDelegate(tflApiPlace_GetAt_Request, forceNewWSCall: forceNewWSCall)
        //---------------------------------------------------------------------------
        //----------------------------------------------------------------------------------------
        //v4 - multiple calls
        //----------------------------------------------------------------------------------------

        // BC jul 3 - trying to build after moths
        appDelegate.tflUnifiedApiWSController.callWSForTFLApiPlaceTypes(delegate: self,
                                                                        tflApiPlaceTypeArray: AppConfig.tflApiPlaceTypeArray,
                                                                        forceNewWSCall: forceNewWSCall)
        //----------------------------------------------------------------------------------------
        
    }
    
    //--------------------------------------------------------------
    // MARK: - TFLGeoJSONManagerDelegate
    // MARK: -
    //--------------------------------------------------------------
    func tflGeoJSONManagerLinesAndStationsLoaded(tflGeoJSONManager: TFLGeoJSONManager){
        logger.debug("tflGeoJSONManagerLinesAndStationsLoaded - MAP STATIONS and LINES LOADED -  may have been downloaded, loaded from json or cached in memory - see prev logs")
    }
    
    
    
    

    
    //--------------------------------------------------------------
    // MARK: - UITableViewDataSource - TABLEVIEW: CELL FOR ROW
    // MARK: -
    //--------------------------------------------------------------
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
       
        //will be overwritten
        let cellIdentifier = TFLApiPlaceType.cellIdentifierForType(AppConfig.tflApiPlaceType)
        
        let genericListTableViewCell : GenericListTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! GenericListTableViewCell
        
        if let rowDataArray_ : Array<TFLApiPlace> = self.genericListViewDataSource.rowDataArrayForSection(indexPath.section) as? Array<TFLApiPlace>
        {
            
            
            if rowDataArray_.count > indexPath.row{
                //----------------------------------------------------------------------------------------
                let tflApiPlace : TFLApiPlace = rowDataArray_[indexPath.row]
                
                //----------------------------------------------------------------------------------------
                genericListTableViewCell.applyCustomFont()
                //----------------------------------------------------------------------------------------
                
                if let searchBar = self.searchBar {
                    if let searchText = searchBar.text {
                        //----------------------------------------------------------------------------------------
                        if searchText.trim() == ""{
                            genericListTableViewCell.labelTitle?.text = tflApiPlace.textLabelText()
                            
                        } else{
                            //----------------------------------------------------------------------------------------
                            //show search string highlighted
                            //----------------------------------------------------------------------------------------
                            
                            let textLabelText = tflApiPlace.textLabelText()

                            //----------------------------------------------------------------------------------------
                            if let nsMutableAttributedString = NSAttributedString.highlightStringInString(wholeString: textLabelText,
                                                                                                          highlightString: searchText,
                                                                                                          highlightColor: UIColor.appColorCYAN())
                            {
                                if nsMutableAttributedString.length > 0{
                                    genericListTableViewCell.labelTitle?.attributedText = nsMutableAttributedString
                                }else{
                                    genericListTableViewCell.labelTitle?.text = tflApiPlace.textLabelText()
                                }
                            }
                        }
                        //----------------------------------------------------------------------------------------
                    }else{
                        logger.error("searchBar.text is nil")
                    }
                }else{
                    logger.error("self.searchBar is nil")
                }
                //----------------------------------------------------------------------------------------
                
                genericListTableViewCell.labelDetail0?.text = tflApiPlace.detailLabelText0()
                genericListTableViewCell.labelDetail1?.text = tflApiPlace.detailLabelText1()
                
                genericListTableViewCell.labelSubDetail0?.text = tflApiPlace.subDetailLabelText0()
                genericListTableViewCell.labelSubDetail1?.text = tflApiPlace.subDetailLabelText1()
                //----------------------------------------------------------------------------------------
            }else{
                logger.error("if rowDataArray_.count > indexPath.row FAILED")
            }
            
        
        }
        else{
            logger.error("rowDataArrayForSection as? Array<TFLApiPlace> failed - DID you change the genericRepsonse to NOT be DummyGenericResponse [self.genericListViewDataSource = tflApiPlaceResponse]")
        }
        
        return genericListTableViewCell
    }

    
    //--------------------------------------------------------------
    // MARK: - TABLEVIEW: didSelectRowAt
    //--------------------------------------------------------------
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        
        //----------------------------------------------------------------------------------------
        //----------------------------------------------------------------------------------------
        //----------------------------------------------------------------------------------------
        //beware code also in parent
        // TODO: - move up
        //----------------------------------------------------------------------------------------
        //----------------------------------------------------------------------------------------
        //----------------------------------------------------------------------------------------
        //----------------------------------------------------------------------------------------
        
        
     
        
        //----------------------------------------------------------------------------------------
        //GENERIC row Any from dict
        //----------------------------------------------------------------------------------------
        if let rowDataArray_ : Array<TFLApiPlace> = self.genericListViewDataSource.rowDataArrayForSection(indexPath.section) as? Array<TFLApiPlace>{
            
            self.selectedRowItemAny = rowDataArray_[indexPath.row]
            //----------------------------------------------------------------------------------------
            
            if let selectedRowItemAny = self.selectedRowItemAny {
                
                logger.debug("didSelectRowAt:\r\(selectedRowItemAny)")
                
                //self.performSegue(withIdentifier: self.segueTFLApiPlaceListViewControllerTOTFLApiPlaceSingleTableViewController, sender: nil)
                
                showAlertOptions(selectedRowItemAny as! TFLApiPlace)
                
            }else{
                logger.error("sself.selectedTFLApiPlace is nil")
            }
            //----------------------------------------------------------------------------------------
        }else{
            logger.error("rowDataArrayForSection as? Array<TFLApiPlace> failed")
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - didTapOnMarker
    // MARK: -
    //--------------------------------------------------------------
    
    //didTapOnMarker
    var selectedTFLApiPlace : TFLApiPlace?

    
    func showAlertOptions(_ tflApiPlace : TFLApiPlace){
        
        self.selectedTFLApiPlace = tflApiPlace
        
        //---------------------------------------------------------
        //CANCEL
        //---------------------------------------------------------
        let alertActionCancel = UIAlertAction.alertActionCancel
        
        //---------------------------------------------------------
        //Directions
        //---------------------------------------------------------
        let alertActionDirections = UIAlertAction(title: "Directions", style: .default) { (alertAction) -> Void in
            
            self.performSegue(withIdentifier: self.segueTo_TFLApiPlaceMapDirectionsViewController, sender: nil)
            
        }
        
        //---------------------------------------------------------
        //StreetView
        //---------------------------------------------------------
        let alertActionStreetView = UIAlertAction(title: "StreetView", style: .default) { (alertAction) -> Void in
            self.performSegue(withIdentifier: self.segueTo_TFLApiPlaceStreetViewController, sender: nil)
            
        }
        
        
        //---------------------------------------------------------
        //Watch - Jam Cams only
        //---------------------------------------------------------
        let alertActionWatchMovie = UIAlertAction(title: "Watch", style: .default) { (alertAction) -> Void in
            self.performSegue(withIdentifier: self.segueTo_AVPlayerViewController, sender: nil)
            
        }

        //---------------------------------------------------------
        //"Report Problem"
        //---------------------------------------------------------
        let alertReportProblem = UIAlertAction(title: "Report Problem", style: .default) { (alertAction) -> Void in
            if let selectedTFLApiPlace = self.selectedTFLApiPlace {
                var placeType_ = "Place"
                
                if let placeType = selectedTFLApiPlace.placeType {
                    placeType_ = placeType
                }else{
                    logger.error("selectedTFLApiPlace.placeType is nil")
                }
                
                
                self.emailManager.sendEmailFromVC(self, bodyString: "User Reported Error with \(placeType_):\n['\(selectedTFLApiPlace.idSafe)':'\(Safe.safeString(selectedTFLApiPlace.commonName))']\n Please describe your problem below and we will forward it to TFL.\n")
            }else{
                logger.error("self.selectedTFLApiPlace is nil")
            }
        }
        
        
        //---------------------------------------------------------
        //show
        //---------------------------------------------------------
        var alertActions = [UIAlertAction]()
        
        if let _ = self.selectedTFLApiPlace as? TFLApiPlaceJamCam {
            alertActions = [alertActionCancel, alertActionDirections, alertActionStreetView, alertActionWatchMovie, alertReportProblem]
        }else{
           alertActions = [alertActionCancel, alertActionDirections, alertActionStreetView, alertReportProblem]
        }
        
        
        //---------------------------------------------------------
        // TODO: - only add jam cam for movi
        CLKAlertController.showSheetWithActions(          vc: self,
                                                       title: tflApiPlace.alertTitle,
                                                     message: tflApiPlace.alertMessage,
                                                alertActions: alertActions)
        //---------------------------------------------------------
    }
    
    
    //------------------------------------------------
    // MARK: UITableViewDataSource - SECTIONS
    //------------------------------------------------
    //Handled by parent and genericListViewDataSource

    
    //override these -
    var segueTo_TFLApiPlaceMapDirectionsViewController: String{
        return "segueTFLApiPlaceListViewControllerTOTFLApiPlaceMapDirectionsViewController"
    }
    var segueTo_TFLApiPlaceStreetViewController: String{
        return "segueTFLApiPlaceListViewControllerTOTFLApiPlaceStreetViewController"
    }
    
    var segueTo_TFLApiPlaceMapViewController: String{
        return "segueTFLApiPlaceListViewControllerTOTFLApiPlaceMapViewController"
    }
    
    var segueTo_SettingsTableViewController: String{
        return "segueTFLApiPlaceListViewControllerTOSettingsTableViewController"
    }
    
    var segueTo_AVPlayerViewController: String{
        return "segueTFLApiPlaceListViewControllerTOAVPlayerViewController"
    }
    
    var segueTo_COLCQueryGenericListOptionsViewController: String{
        return "segueTFLApiPlaceListViewControllerTOCOLCQueryGenericListOptionsViewController"
    }
    
    
    
    
    
    //--------------------------------------------------------------
    // MARK: - prepare
    // MARK: -
    //--------------------------------------------------------------
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if(segue.identifier == self.segueTo_TFLApiPlaceStreetViewController){
            
            if(segue.destination.isMember(of: TFLApiPlaceStreetViewController.self)){
                
                let tflApiPlaceStreetViewController = (segue.destination as! TFLApiPlaceStreetViewController)
                //tflApiPlaceMapViewController.delegate = self
                tflApiPlaceStreetViewController.selectedTFLApiPlace = self.selectedTFLApiPlace
                
            }else{
                print("ERROR:not TFLApiPlaceMapViewController")
            }
        }
        else if(segue.identifier == segueTo_TFLApiPlaceMapDirectionsViewController){
            
            if(segue.destination.isMember(of: TFLApiPlaceMapDirectionsViewController.self)){
                
                let tflApiPlaceMapDirectionsViewController = (segue.destination as! TFLApiPlaceMapDirectionsViewController)
                
                tflApiPlaceMapDirectionsViewController.selectedTFLApiPlace = self.selectedTFLApiPlace
                
            }
            else{
                print("ERROR:not TFLApiPlaceMapDirectionsViewController")
            }
        }
        else if(segue.identifier == segueTo_SettingsTableViewController){
            // TODO: - may need to pass type for Email subject
        }
        else if(segue.identifier == segueTo_AVPlayerViewController){
            
            if(segue.destination.isMember(of: AVPlayerViewController.self)){
                //import AVFoundation
                //import AVKit
                let avPlayerViewController = (segue.destination as! AVPlayerViewController)
                
                if let tflApiPlaceJamCam = self.selectedTFLApiPlace as? TFLApiPlaceJamCam {
                    if let videoUrlString = tflApiPlaceJamCam.videoUrlString {
                        if videoUrlString.trim() == "" {
                            logger.error("videoUrlString is ''")
                        }else{
                            //https://s3-eu-west-1.amazonaws.com/jamcams.tfl.gov.uk/00001.01409.mp4"
                            if let movieURL = URL(string:videoUrlString){
                                
                                
                                //----------------------------------------------------------------------------------------
                                let avPlayer = AVPlayer.init(url: movieURL)
                                avPlayerViewController.player = avPlayer
                                avPlayerViewController.player?.play()
                                
                                //LOOP
//                                https://developer.apple.com/documentation/avfoundation/avplayerlooper
//
//                                let asset = // AVAsset with its 'duration' property value loaded
//                                let playerItem = AVPlayerItem(asset: asset)
//
//                                // Create a new player looper with the queue player and template item
//                                playerLooper = AVPlayerLooper(player: queuePlayer, templateItem: playerItem)
//
//                                // Begin looping playback
//                                queuePlayer.play()
                                
                                
//                                // Use a new player looper with the queue player and template item
//                                let playerItem = AVPlayerItem(url: movieURL)
//                                let avQueuePlayer = AVQueuePlayer(items: [playerItem])
//                                
//                                let avPlayerLayer = AVPlayerLayer(player: avQueuePlayer)
//                                
//                                let playerLooper = AVPlayerLooper(player: avQueuePlayer as! AVQueuePlayer, templateItem: playerItem)
//                                
//                                self.view.layer.addSublayer(self.playerLayer!)
//                                self.playerLayer?.frame = self.view.frame
//                                self.player?.play()
                                
                                
                            }else{
                                logger.error("URL is nil")
                            }
                        }
                    }else{
                        logger.error("tflApiPlaceJamCam.videoUrlString is nil")
                    }
                }else{
                    logger.error("self.selectedTFLApiPlace as? TFLApiPlaceJamCam is nil")
                }
            }
            else{
                 logger.error(":not AVPlayerViewController")
            }
        }
        else if(segue.identifier == segueTo_COLCQueryGenericListOptionsViewController){
            
            if(segue.destination.isMember(of: COLCQueryGenericListOptionsViewController.self)){
                let colcQueryGenericListOptionsViewController = (segue.destination as! COLCQueryGenericListOptionsViewController)
                colcQueryGenericListOptionsViewController.delegate = self
                // TODO: - UNTESTED
                colcQueryGenericListOptionsViewController.colcQueryCollectionForType = self.genericListViewDataSource.colcQueryCollectionForType
                //---------------------------------------------------------
            }
            else{
                logger.error(":not COLCQueryGenericListOptionsViewController")
            }
        }
        else{
            logger.error("UNHANDLED SEGUE:\(String(describing: segue.identifier))")
        }
        
        //do when user taps button else it triggers viewToolbar to fade
        //self.searchBar.text = ""
        //self.searchBar.resignFirstResponder()
    }
    
    //--------------------------------------------------------------
    // MARK: - COLCQueryGenericListOptionsViewControllerDelegate - removed but works
    // MARK: -
    //--------------------------------------------------------------
    func colcQueryGenericListOptionsViewController(colcQueryGenericListOptionsViewController: COLCQueryGenericListOptionsViewController,
                                                   didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.section == 0{
            //---------------------------------------------------------
            //Change query
            //---------------------------------------------------------
            self.change_currentQueryIndex(selectedIndex: indexPath.section)
            
            
        }else if indexPath.section == 1{
            //---------------------------------------------------------
            //SORT ORDER
            //---------------------------------------------------------
        }else{
            logger.error("UNHANDLED indexPath.section == \(indexPath.section)")
        }
    
    }
    
}
