//
//  TFLApiPlaceSingleTableViewController.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 19/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
import UIKit

class TFLApiPlaceSingleTableViewController: UITableViewController {
    //from segue
    var selectedTFLApiPlace : TFLApiPlace?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //--------------------------------------------------------------
    // MARK: - SEGUE
    // MARK: -
    //--------------------------------------------------------------
    let segueTFLApiPlaceSingleTableViewControllerTOTFLApiPlaceStreetViewController = "segueTFLApiPlaceSingleTableViewControllerTOTFLApiPlaceStreetViewController"
    
    let segueTFLApiPlaceSingleTableViewControllerTOTFLApiPlaceMapDirectionsViewController = "segueTFLApiPlaceSingleTableViewControllerTOTFLApiPlaceMapDirectionsViewController"
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == segueTFLApiPlaceSingleTableViewControllerTOTFLApiPlaceStreetViewController){
            
            if(segue.destination.isMember(of: TFLApiPlaceStreetViewController.self)){
                
                let tflApiPlaceStreetViewController = (segue.destination as! TFLApiPlaceStreetViewController)
                //tflApiPlaceStreetViewController .delegate = self
                tflApiPlaceStreetViewController.selectedTFLApiPlace = self.selectedTFLApiPlace
                
            }
            else{
                print("ERROR:not TFLApiPlaceStreetViewController")
            }
            
        }else if(segue.identifier == segueTFLApiPlaceSingleTableViewControllerTOTFLApiPlaceMapDirectionsViewController){
            
            if(segue.destination.isMember(of: TFLApiPlaceMapDirectionsViewController.self)){
                
                let tflApiPlaceMapDirectionsViewController = (segue.destination as! TFLApiPlaceMapDirectionsViewController)
                //tflApiPlaceMapDirectionsViewController.delegate = self
                tflApiPlaceMapDirectionsViewController.selectedTFLApiPlace = self.selectedTFLApiPlace
                
            }
            else{
                print("ERROR:not TFLApiPlaceMapDirectionsViewController")
            }
        }
        else{
            logger.error("UNHANDLED SEGUE:\(String(describing: segue.identifier))")
        }
    }
}
