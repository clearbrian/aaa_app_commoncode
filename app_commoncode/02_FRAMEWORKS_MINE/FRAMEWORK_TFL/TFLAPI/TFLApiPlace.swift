//
//  TFLAPIPlace.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 13/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

// TODO: - remove this class should be platform agnostic
import CoreLocation
import UIKit

class TFLApiPlace : COLCPlace /*, TableDisplayable, Alertable, MapPinnable, COLCQueryable, COLCSortable, CustomStringConvertible*/{
    
    //--------------------------------------------------------------
    // MARK: - JSON params
    // MARK: -
    //--------------------------------------------------------------
    var type : String?
    var additionalProperties : [TFLApiPlaceAdditionalProperty]?
    var children : [AnyObject]?
    var childrenUrls : [AnyObject]?
    var commonName : String?
    //--------------------------------------------------------------
//    var lat : Float?
//    var lon : Float?
    var lat : Double?
    var lon : Double?
    
    var placeType : String?
    var url : String?
    //--------------------------------------------------------------
    
    //subclass TFLApiPlaceTaxiRank
    var id : String?
    
    
    init(id : String,
         commonName : String,
//         lat : Float,
//         lon : Float,
         lat : Double,
         lon : Double,
         placeType : String,
         url : String?
        )
    {
        super.init()
        self.id = id
        self.type = "Tfl.Api.Presentation.Entities.Place, Tfl.Api.Presentation.Entities"
        self.additionalProperties =  [TFLApiPlaceAdditionalProperty]()
        self.children = [AnyObject]()
        self.childrenUrls = [AnyObject]()
        
        self.commonName = commonName
        self.lat = lat
        self.lon = lon
        self.placeType = placeType
        self.url = url
        
    }
    
    required init?(map: Map){
        
        super.init(map: map)
        
    }
    
    //--------------------------------------------------------------
    //	class func newInstance(map: Map) -> Mappable?{
    //		return TFLTaxiRank()
    //	}
    //	required init?(map: Map){}
    //	private override init(){}
    //--------------------------------------------------------------
    
    override func mapping(map: Map)
    {
        type <- map["$type"]
        additionalProperties <- map["additionalProperties"]
        children <- map["children"]
        childrenUrls <- map["childrenUrls"]
        commonName <- map["commonName"]
        id <- map["id"]
        lat <- map["lat"]
        lon <- map["lon"]
        placeType <- map["placeType"]
        url <- map["url"]
        
    }
    

    //--------------------------------------------------------------
    // MARK: - safe versions of optionals ivars
    // MARK: -
    //--------------------------------------------------------------
    var idSafe: String{
        return Safe.safeString(self.id)
    }


    //---------------------------------------------------------
    var lonStringSafe: String{
        var lon_ = ""
        if let lonFloat = self.lon {
            lon_ = "\(lonFloat)"
        }else{
            
        }
        return lon_
    }
    var latStringSafe: String{
        var lat_ = ""
        if let latFloat = self.lat {
            lat_ = "\(latFloat)"
        }else{
            
        }
        return lat_
    }

    
    
    //--------------------------------------------------------------
    // MARK: - Table cell labels
    // MARK: -
    //--------------------------------------------------------------
    // TODO: - convert to functions  move to extensions propertes cant be overridden from extensions yet
    
    override var descriptionLineMain : String {
        return "\(Safe.safeString(self.commonName))"
    }
    
    var descriptionLineDetail0 : String {
        var description_ = ""
        description_ = description_ +  "\(Safe.safeString(self.placeType))"
        
        return description_
    }
    var descriptionLineDetail1 : String {
        var description_ = ""
        
        description_ = description_ +  "\(self.idSafe)"
        
        return description_
    }
    
    var descriptionLineSubDetail0 : String {
        var description_ = ""
        
        description_ = description_ +  "lat: \(self.latStringSafe)"
        
        return description_
    }
    var descriptionLineSubDetail1 : String {
        var description_ = ""
        description_ = description_ +  "lat: \(self.lonStringSafe)"
        return description_
    }
    
    
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - Desc
    //--------------------------------------------------------------
    //used for summary in alert
    //marker snipper
    override var descriptionOneLine : String {
        return "[id:\(self.idSafe)] \(Safe.safeString(self.commonName)) [\(self.latStringSafe), \(self.lonStringSafe)]"
    }
    
    //--------------------------------------------------------------
    // MARK: - CustomStringConvertible
    // MARK: -
    //--------------------------------------------------------------
    
    override var description : String {
        var description_ = "TFLApiPlaceJamCam:"
        description_ = description_ + " commonName:\(String(describing: self.commonName))\r"
        description_ = description_ + "         id:\(String(describing: self.id))\r"
        description_ = description_ + "        lat:\(String(describing: self.lat))\r"
        description_ = description_ + "        lon:\(String(describing: self.lon))\r"
        description_ = description_ + "  placeType:\(String(describing: self.placeType))\r"
        description_ = description_ + "        url:\(String(describing: self.url))\r"
        //description_ = description_ + "additionalProperties:\r"
        if let additionalProperties = self.additionalProperties {
            for tflApiPlaceAdditionalProperty: TFLApiPlaceAdditionalProperty in additionalProperties{
                description_ = description_ + "\(tflApiPlaceAdditionalProperty.descriptionSimple)\r"
            }
        }else{
            logger.error("self.additionalProperties is nil")
        }
        
        return description_
    }
    
    var descriptionCSVByPropLoop : String {
        
        var description_ = ""
        
        let descriptionRank_ = "RANK,\(self.idSafe)"
        
        if let additionalProperties = self.additionalProperties {
            for tflApiPlaceAdditionalProperty: TFLApiPlaceAdditionalProperty in additionalProperties{
                description_ = description_ + "\(descriptionRank_),\(tflApiPlaceAdditionalProperty.descriptionSimpleCSV)\r"
            }
        }else{
            logger.error("self.additionalProperties is nil")
        }
        
        return description_
    }
    
    //RANK,JamCam_5973,Park Plaza - County Hall Hotel, hotel forecourt,JamCam,-0.116399,51.5014,2,Mon - Sun,24 hours,Working,,
    var descriptionCSV : String {
        
        var description_ = ""
        
        description_ = description_ +  "Place,"
        description_ = description_ +  "\(self.idSafe),"
        description_ = description_ +  "\(Safe.safeString(self.commonName)),"
        description_ = description_ +  "\(Safe.safeString(self.placeType)),"
        description_ = description_ +  "\(self.lonStringSafe),"
        description_ = description_ +  "\(self.latStringSafe),"
        
        return description_
    }
    
    
    //--------------------------------------------------------------
    // MARK: - TableDisplayable
    // MARK: -
    //--------------------------------------------------------------
    override func textLabelText() -> String{
        return self.descriptionLineMain
    }
    
    override func detailLabelText0() -> String?{
        return self.descriptionLineDetail0
    }
    override func detailLabelText1() -> String?{
        return self.descriptionLineDetail1
    }
    
    override func subDetailLabelText0() -> String?{
        return self.descriptionLineSubDetail0
    }
    override func subDetailLabelText1() -> String?{
        return self.descriptionLineSubDetail1
    }
    
    
    
    //--------------------------------------------------------------
    // MARK: - CoreLocatable - in parent
    // MARK: -
    //--------------------------------------------------------------
    //    func distanceTo(otherLocation: CLLocation) -> CLLocationDistance {
    //    func distanceToCurrentLocation() -> CLLocationDistance {
    //    var distanceToCurrentLocationFormatted: String{
    
    
    override var clLocation: CLLocation?{

        var locationReturned: CLLocation? = nil
        
//        if let locationWithFloatsReturned = CLLocation.locationWith(latFloat: self.lat, lonFloat: self.lon ){
//            locationReturned = locationWithFloatsReturned
//        }else{
//            logger.error("locationWithFloatsReturned is nil")
//        }
        
        if let locationWithFloatsReturned = CLLocation.locationWith(latDouble: self.lat, lonDouble: self.lon ){
            locationReturned = locationWithFloatsReturned
        }else{
            logger.error("locationWithFloatsReturned is nil")
        }
        
        return locationReturned
    }
    
    //--------------------------------------------------------------
    // MARK: - Alertable - in parent
    // MARK: -
    //--------------------------------------------------------------
    //MOVED DOWN!! TaxiRank/JamCam
    //    override var alertTitle: String{
    //        return self.descriptionLineMain
    //    }
    //    
    //    override var alertMessage: String{
    //        return self.descriptionLineDetail0
    //    }
    
    
    //--------------------------------------------------------------
    // MARK: - MapPinnable
    // MARK: -
    //--------------------------------------------------------------
    override var markerImageName: String?{
        var markerImageNameRet: String? = nil
        //SUBCLASSED
        markerImageNameRet = nil
        return markerImageNameRet
    }
    
    override var markerImage: UIImage?{
        var markerImageReturned: UIImage? = nil
        
        if let markerImageName = self.markerImageName {
            
            if let imageFound = ImageCache.lazyImageForName(imageName: markerImageName) {
                markerImageReturned = imageFound
            }else{
                logger.error("ImageCache.lazyImageForName(imageName: \(markerImageName)) is nil'")
            }
            
        }else{
            logger.error("markerImageName is nil'")
        }
        
        return markerImageReturned
    }
    
    
    //--------------------------------------------------------------
    // MARK: - AdditionalProperties
    // MARK: -
    //--------------------------------------------------------------
    // TODO: - move up?
    func findStringPropertyForKeyRawValue(_ tflAdditionalPropertyKeyRawValue: String) -> String?{
        
        var stringValue: String? = nil
        if let additionalProperties = self.additionalProperties {
            
            for tflApiPlaceAdditionalProperty: TFLApiPlaceAdditionalProperty in additionalProperties{
                
                if let value = tflApiPlaceAdditionalProperty.valueForKey(keyString: tflAdditionalPropertyKeyRawValue){
                    stringValue = value
                    break;
                    
                }else{
                    //logger.error("tflAdditionalPropertyKey_TaxiRank.rawValue[\(tflAdditionalPropertyKey_TaxiRank.rawValue)] is nil - ok iterating over props")
                }
                
            }
        }else{
            logger.error("self.additionalProperties is nil")
        }
        return stringValue
    }
    
    func findIntPropertyForKeyRawValue(_ tflAdditionalPropertyKeyRawValue: String) -> Int?{
        
        var intValue: Int? = nil
        
        if let stringValue: String = findStringPropertyForKeyRawValue(tflAdditionalPropertyKeyRawValue){
            
            let formatter = NumberFormatter()
            if let numberValue = formatter.number(from: stringValue) {
                intValue = numberValue.intValue
                
            }else{
                logger.error("formatter.number(from: stringValue:'\(stringValue)') is nil")
            }
        }else{
            logger.error("findStringPropertyForKeyRawValue failed")
        }
        
        return intValue
    }
    
    
    //--------------------------------------------------------------
    // MARK: - COLCQueryable
    // MARK: -
    //--------------------------------------------------------------
    //cant override static - so static Factory > concrete instance
    //--------------------------------------------------------------
    //http://stackoverflow.com/questions/29189700/overriding-static-vars-in-subclasses-swift-1-2
    //--------------------------------------------------------------
    class var colcQueryCollectionForType : COLCQueryCollection{
        return TFLApiPlaceCOLCQueryCollection()
        
    }

    //--------------------------------------------------------------
    // MARK: - COLCQueryable
    // MARK: -
    //--------------------------------------------------------------
    //--------------------------------------------------------------
    // TODO: - nned beetter way to many properties - they get inherited so value(forPropertyName TFLApiPlace.propertyName_clLocation even though its in parent
    fileprivate static let propertyName_commonName = "commonName"
    
    
    
    override func value(forPropertyName propertyName: String) -> Any?{
        switch propertyName{
            case TFLApiPlace.propertyName_commonName:
                return self.commonName
            
        default:
            //NOISYlogger.error("UNHANDLED PROPERTY: value:forPropertyName:\(propertyName) - may be in subclass of TFLAPIPLACE")
            return nil
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - Factories for defaultSearchQuery
    // MARK: -
    //--------------------------------------------------------------
    class func COLCQuery_defaultSearchQuery(title: String) -> COLCQuery{
        
        let colcQuery = COLCQuery(title: title, colcSort: COLCSortNone(title:""), searchPropertyName: TFLApiPlace.propertyName_commonName)
        colcQuery.showSearchBar = false
        return colcQuery
    }
    
    //--------------------------------------------------------------
    // MARK: - Factories for defaults
    // MARK: -
    //--------------------------------------------------------------
    class func colcQuery_sortByNearest() -> COLCQuery{
        
        //action sheet title
        //let colcQuery = COLCQuery(title: "Show Nearest Ranks", colcSort: colcSort)
        let colcQuery = TFLApiPlace.COLCQuery_defaultSearchQuery(title: "Show Nearest")
        
        //table header title
        colcQuery.colcSort = COLCSortByCLLocation(title: "Nearest")
        
        return colcQuery
    }
    
    class func colcQuery_sortByCommonName() -> COLCQuery{
        
        let colcQuery = TFLApiPlace.COLCQuery_defaultSearchQuery(title:"Sort by Name A..Z")
        
        colcQuery.colcSort = TFLApiPlace.colcSort_sortByCommonName()
        colcQuery.colcGroupBy = COLCGroupByAZ(title: "Grouped")

        
        return colcQuery
    }
    
    //--------------------------------------------------------------
    // MARK: - COLCSort - factory
    // MARK: -
    //--------------------------------------------------------------
    class func colcSort_sortByCommonName() -> COLCSort{
        
        let colcSort = COLCSortByString(title           : "Search By Name",
                                        propertyName    : TFLApiPlace.propertyName_commonName,
                                        isSorted        : true,
                                        isAscending     : true,
                                        isCaseSensitive : true)
        
        return colcSort
    }

    //--------------------------------------------------------------

}
