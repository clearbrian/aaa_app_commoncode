//
//  TFLApiPlace_GetAt_Request.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 25/01/2018.
//  Copyright © 2018 Brian Clear. All rights reserved.
//

import Foundation

/*
 Operation : Place_GetAt
 https://api.tfl.gov.uk/swagger/ui/index.html?url=/swagger/docs/v1#!/Place/Place_GetAt
 
 
 //----------------------------------------------------------------------------------------
 Used in Taxis / JamCams app
 https://api.tfl.gov.uk/swagger/ui/index.html?url=/swagger/docs/v1#!/Place/Place_GetAt
 //----------------------------------------------------------------------------------------
 

 https://api.tfl.gov.uk/Place/Type/TaxiRank?app_id=eee441d5&app_key=6561ea5d2bc2b50e4eeecc54f81d73e1?
 https://api.tfl.gov.uk/Place/Type/JamCams?app_id=eee441d5&app_key=6561ea5d2bc2b50e4eeecc54f81d73e1?

 //----------------------------------------------------------------------------------------
 https://api.tfl.gov.uk/Place/Meta/PlaceTypes
 [
 "AreaForIntensification",
 "BikePoint",
 "Boroughs",
 "Cabwise",
 "CarClub",
 "CarPark",
 "CensusOutputAreas",
 "CensusSuperOutputAreas",
 "CentralActivityZone",
 "ChargeConnector",
 "ChargeStation",
 "CoachBay",
 "CoachPark",
 "CyclePark",
 "JamCam",
 "OnStreetMeteredBay",
 "OpportunityAreas",
 "OtherCoachParking",
 "OysterTicketShop",
 "RedLightAndSpeedCam",
 "RedLightCam",
 "SpeedCam",
 "TaxiRank",
 "VariableMessageSign",
 "Wards",
 "WaterfreightBridge",
 "WaterfreightDock",
 "WaterfreightJetty",
 "WaterfreightLock",
 "WaterfreightOther Access Point",
 "WaterfreightTunnel",
 "WaterfreightWharf"
 ]
 
 */
class TFLApiPlace_GetAt_Request : TFLUnifiedApiPlaceRequest{
    
    //---------------------------------------------------------
    // TODO: - type is an array in Swagger
    // TODO: - Values come from /Place/Meta/placeTypes
    //---------------------------------------------------------
    // "required": true,
    //    var type : String{
    //        return tflApiPlaceType.rawValue
    //    }
    //---------------------------------------------------------
    
    //---------------------------------------------------------
    // TODO: - CLEANUP after test
    //MOVED UP var tflApiPlaceType: TFLApiPlaceType = .unknown
    //---------------------------------------------------------
    
    convenience init(tflApiPlaceType: TFLApiPlaceType){
        self.init()
        
        self.tflApiPlaceType = tflApiPlaceType
        
    }
    
    override var webServiceEndpoint :String? {
        get {

            var webServiceEndpoint_ : String? = nil

            if let webServiceEndpoint = super.webServiceEndpoint {
                if self.tflApiPlaceType == .unknown{
                    logger.error("[TFLPlaceTypeRequest] self.tflApiPlaceType is .unknown - cant set webServiceEndpoint")
                }else{
                    //webServiceEndpoint_ = "https://api.tfl.gov.uk/Place/Type/\(self.tflApiPlaceType)"
                    //webServiceEndpoint_ = "\(webServiceEndpoint)/Place/Type/\(self.tflApiPlaceType.apiStringForType)"
                    webServiceEndpoint_ = "\(webServiceEndpoint)/Type/\(self.tflApiPlaceType.apiStringForType)"
                    //"https://api.tfl.gov.uk/Place//Type/TaxiRank"
                }
            }else{
                logger.error("super.webServiceEndpoint is nil")
            }

            return webServiceEndpoint_
        }
    }
}
