//
//  TFLApiPlaceRequest.swift
//  app_arkit_gps
//
//  Created by Brian Clear on 15/08/2017.
//  Copyright © 2017 City Of London Consulting. All rights reserved.
//

import Foundation

class TFLUnifiedApiPlaceRequest : TFLUnifiedApiEndpointParentRequest{
    
    //used as key to track multiple ws calls
    var tflApiPlaceType: TFLApiPlaceType = .unknown
    
    var tflApiPlaceArray : [TFLApiPlace]? = nil

    init(){
        super.init(tflUnifiedEndPoint:.place)
    }
    
    override func processResponse(parsedObject: Any?) -> Bool{
        var isProcessedOk = false
        
        // TODO: - move into TFLApiPlace type
        //----------------------------------------------------------------------------------------
        if self.tflApiPlaceType == .taxiRank{
            //------------------------------------------------------------------------------------------------
            if let tflApiPlaceTaxiRankArray : [TFLApiPlaceTaxiRank] = Mapper<TFLApiPlaceTaxiRank>().mapArray(JSONObject:parsedObject)
            {
                //---------------------------------------------------------
                //PROCESS TAXI RANK VERSION - KEEP ONLY THE LATEST
                //---------------------------------------------------------
                let tflApiPlaceTaxiRankArrayProcessed: [TFLApiPlaceTaxiRank] = TFLApiPlaceTaxiRank.processTankRankVersions(tflApiPlaceTaxiRankArray: tflApiPlaceTaxiRankArray)
                //--------------------------------------------------------------------
                self.tflApiPlaceArray = tflApiPlaceTaxiRankArrayProcessed
                
                //--------------------------------------------------------------------
            }
            else{
                logger.error("parsedObject is nil or not [TFLApiPlace]")
            }
        }
        else if self.tflApiPlaceType == .jamCam{
            //------------------------------------------------------------------------------------------------
            if let tflApiPlaceTFLApiPlaceJamCamArray : [TFLApiPlaceJamCam] = Mapper<TFLApiPlaceJamCam>().mapArray(JSONObject:parsedObject)
            {
                self.tflApiPlaceArray = tflApiPlaceTFLApiPlaceJamCamArray
            }
            else{
                logger.error("parsedObject is nil or not [TFLApiPlaceJamCam]")
            }
        }
        else if self.tflApiPlaceType == .chargeStation{
            //------------------------------------------------------------------------------------------------
            if let tflApiPlaceTFLApiPlaceChargeStationArray : [TFLApiPlaceChargeStation] = Mapper<TFLApiPlaceChargeStation>().mapArray(JSONObject:parsedObject)
            {
                self.tflApiPlaceArray = tflApiPlaceTFLApiPlaceChargeStationArray
            }
            else{
                logger.error("parsedObject is nil or not [TFLApiPlaceChargeStation]")
            }
        }
        else if self.tflApiPlaceType == .chargeConnector{
            //------------------------------------------------------------------------------------------------
            if let tflApiPlaceTFLApiPlaceChargeConnectorArray : [TFLApiPlaceChargeConnector] = Mapper<TFLApiPlaceChargeConnector>().mapArray(JSONObject:parsedObject)
            {
                self.tflApiPlaceArray = tflApiPlaceTFLApiPlaceChargeConnectorArray
            }
            else{
                logger.error("parsedObject is nil or not [TFLApiPlaceChargeConnector]")
            }
        }
        else{
            //-------------------------------------------------------------------
            //All other TFLApiPlace subtypes >> TFLApiPlace
            //-------------------------------------------------------------------
            //TFLApiPlaceTaxiRank is TFLApiPlace
            if let tflApiPlaceArray_ : [TFLApiPlace] = Mapper<TFLApiPlace>().mapArray(JSONObject:parsedObject)
            {
                self.tflApiPlaceArray = tflApiPlaceArray_
            }else{
                logger.error("parsedObject is nil or not [TFLApiPlace]")
            }
            //-------------------------------------------------------------------
        }
        
        if let tflApiPlaceArray = self.tflApiPlaceArray{
            logger.debug("processResponse tflApiPlaceArray processed:\(tflApiPlaceArray.count)")
            
        }else{
            logger.error("self.processResponse(tflUnifiedApiPlaceRequest,parsedObject) is nil")
        }
        //----------------------------------------------------------------------------------------
        //empty is ok if youre doing a query
        if let _ = self.tflApiPlaceArray {
            isProcessedOk = true
        }else{
            logger.error("self.tflApiPlaceArray is nil")
        }
        return isProcessedOk
    }
}

 /*
 TFL SWAGGER OPERATIONS /Place/<>   : NOT IMPLEMENTED YET SEE SUBCLASSES
     ======== ======== ======== ======== ======== ========
     paths and their ops
     ======== ======== ======== ======== ======== ========
     "/Place": {
     "operationId": "Place_GetByGeoBox"
     
     
     "/Place/{id}": {
     "operationId": "Place_Get"
     
     "/Place/{type}/At/{Lat}/{Lon}": {
     "operationId": "Place_GetAt"
     
     
     "/Place/{type}/overlay/{z}/{Lat}/{Lon}/{width}/{height}": {
     "operationId": "Place_GetOverlay"
     
     
     "/Place/Address/Streets/{Postcode}": {
     "operationId": "Place_GetStreetsByPostCode"
     
     
     "/Place/Meta/Categories": {
     "operationId": "Place_MetaCategories"
     
     "/Place/Meta/PlaceTypes": {
     "operationId": "Place_MetaPlaceTypes"
     
     "/Place/Search": {
     "operationId": "Place_Search"
     
     "/Place/Type/{types}": {
     "operationId": "Place_GetByType"
 
*/
