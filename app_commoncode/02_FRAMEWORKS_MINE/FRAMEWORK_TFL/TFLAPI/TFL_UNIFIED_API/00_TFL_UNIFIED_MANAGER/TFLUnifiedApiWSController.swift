//
//  TFLUnifiedApiWSController.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 21/07/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

class TFLUnifiedApiWSController : ParentWSController{
    
    
    //CACHED - must be optional else first api call
    var tflApiPlaceResponse : TFLApiPlaceResponse?
    
    func callWSForTFLApiPlaceTypes(delegate: ParentWSControllerDelegate, tflApiPlaceTypeArray: [TFLApiPlaceType], forceNewWSCall: Bool){
        
        self.delegate = delegate
        

        if forceNewWSCall{
            callAllWS(tflApiPlaceTypeArray)
        }else{

            if let tflApiPlaceResponse = self.tflApiPlaceResponse {
                self.delegate_response_success(parsedObject: tflApiPlaceResponse)
                
            }else{
                logger.error("self.tflApiPlaceResponse is nil OK IF FIRST TIME ITS CALLED >> callAllWS()")
                callAllWS(tflApiPlaceTypeArray)
            }
        }
    }
    
    func callAllWS(_ tflApiPlaceTypeArray: [TFLApiPlaceType]){

        self.tflApiPlaceResponse = TFLApiPlaceResponse()

        for tflApiPlaceType: TFLApiPlaceType in tflApiPlaceTypeArray{

            let tflApiPlace_GetAt_Request = TFLApiPlace_GetAt_Request(tflApiPlaceType:tflApiPlaceType)
            logger.info("tflUnifiedApiPlaceRequest:\(String(describing: tflApiPlace_GetAt_Request.urlString))")

            if let tflApiPlaceResponse = self.tflApiPlaceResponse {
                self.call_WithDelegateAlways(tflApiPlace_GetAt_Request, tflApiPlaceResponse: tflApiPlaceResponse)

            }else{
                logger.error("self.tflApiPlaceResponse is nil")
            }
        }
    }

    //CALL WS
    func call_WithDelegateAlways(_ tflUnifiedApiPlaceRequest: TFLUnifiedApiPlaceRequest, tflApiPlaceResponse : TFLApiPlaceResponse)
    {

        //---------------------------------------------------------------------
        //CALL THE WS
        //---------------------------------------------------------------------
        if let _ =  tflUnifiedApiPlaceRequest.urlString{

            //---------------------------------------------------------------------
            //NOISY logger.debug("urlString:\r\(urlString)")
            //---------------------------------------------------------------------
            //Add to diction as well call ws.. then remove them when ws returns - when dictionary empty return response
            //---------------------------------------------------------------------
            tflApiPlaceResponse.storeRequest(tflUnifiedApiPlaceRequest: tflUnifiedApiPlaceRequest)
            
            //---------------------------------------------------------------------
            self.callWS(tflUnifiedApiPlaceRequest,
                        success:{ (parentWSRequest: ParentWSRequest,
                                   parsedObject: Any?)->Void in
                            //----------------------------------------------------------------------------------------
                            //success
                            //----------------------------------------------------------------------------------------
                            if let tflUnifiedApiPlaceRequest:TFLUnifiedApiPlaceRequest = parentWSRequest as? TFLUnifiedApiPlaceRequest{

                                tflUnifiedApiPlaceRequest.parentWSRequestState = .ws_returned_and_processed

                                //----------------------------------------------------------------------------------------
                                // processResponse
                                //----------------------------------------------------------------------------------------
                                if tflUnifiedApiPlaceRequest.processResponse(parsedObject: parsedObject){

                                    if let tflApiPlaceArray = tflUnifiedApiPlaceRequest.tflApiPlaceArray {
                                        logger.info("PROCESSED OK: tflUnifiedApiPlaceRequest.tflApiPlaceArray:\(tflApiPlaceArray.count)")
                                    }else{
                                        logger.error("PROCESSED FAILED: tflUnifiedApiPlaceRequest.tflApiPlaceArray is nil")
                                    }

                                    //----------------------------------------------------------------------------------------
                                    //RESPONSE OK - CHECK OTHERS
                                    //----------------------------------------------------------------------------------------
                                    if let tflApiPlaceResponse = self.tflApiPlaceResponse {

                                        if tflApiPlaceResponse.areAllRequestsProcessed(){
                                            //----------------------------------------------------------------------------------------
                                            //n WS sent - n RETURNED >> move to next step
                                            //----------------------------------------------------------------------------------------
                                            logger.info("tflApiPlaceResponse.allRequestsProcessed(): TRUE - CONTINUE >> delegate_response_success")
                                            self.delegate_response_success(parsedObject: tflApiPlaceResponse)
                                            //----------------------------------------------------------------------------------------
                                        }else{
                                            logger.error("tflApiPlaceResponse.allRequestsProcessed(): FALSE - WAIT FOR OTHER WS TO RETURN")
                                        }
                                    }else{
                                        logger.error("self.tflApiPlaceResponse is nil")
                                    }
                                    //----------------------------------------------------------------------------------------
                                }else{
                                    logger.error("processResponse FAILED")
                                }
                            }else{
                                logger.error("parentWSRequest as? TFLUnifiedApiPlaceRequest is nil")
                            }

                        },
                        failure:{ (parentWSRequest: ParentWSRequest, error) -> Void in
                            parentWSRequest.parentWSRequestState = .ws_returned_and_processed
                            self.delegate_response_failure(error: error)
                    }
            )
            //---------------------------------------------------------------------
        }else{
            logger.error("urlString is nil - 4733")
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - Call WS - managed by block - ok for one vc but not when map and list share same response
    // MARK: -
    //--------------------------------------------------------------
    func call_withBlocks_TFLApiPlaceTypeRequest(_ tflApiPlaceTypeRequest: TFLApiPlaceTypeRequest,
                                                success: @escaping (_ tflApiPlaceResponse : TFLApiPlaceResponse) -> Void,
                                                failure: @escaping (_ error: Error) -> Void
        )
    {
        //----------------------------------------------------------------------------------------
        if let _ =  tflApiPlaceTypeRequest.urlString{
            //---------------------------------------------------------------------
            //NOISY logger.debug("urlString:\r\(urlString)")
            //---------------------------------------------------------------------
            self.callWS(
                tflApiPlaceTypeRequest,
                success:{(parentWSRequest: ParentWSRequest, parsedObject: Any?)->Void in
                    
                    if let tflUnifiedApiPlaceRequest = parentWSRequest as? TFLUnifiedApiPlaceRequest{
                        
                        if tflUnifiedApiPlaceRequest.processResponse(parsedObject:parsedObject){
                            
                            
                            if self.tflApiPlaceResponse == nil {
                                logger.error("self.tflApiPlaceResponse is nil - create it")
                                self.tflApiPlaceResponse = TFLApiPlaceResponse()
                            }
                            
                            if let tflApiPlaceResponse = self.tflApiPlaceResponse {
                                tflApiPlaceResponse.storeRequest(tflUnifiedApiPlaceRequest: tflUnifiedApiPlaceRequest)
                                //---------------------------------------------------------------------
                                //RESPONSE OK
                                //---------------------------------------------------------------------
                                success(tflApiPlaceResponse)
                                //---------------------------------------------------------------------
                            }else{
                                logger.error("self.tflApiPlaceResponse is nil")
                            }
                        }else{
                            logger.error("tflUnifiedApiPlaceRequest.processResponse(parsedObject:parsedObject) FAILED")
                        }
                    }else{
                        logger.error("parentWSRequest as? TFLUnifiedApiPlaceRequest is nil")
                    }
                },
                failure:{(tflApiPlaceTypeRequest, error) -> Void in
                    failure(error)
                }
            )
            //---------------------------------------------------------------------
        }else{
            logger.error("urlString is nil - 4744")
        }
        //----------------------------------------------------------------------------------------
    }
}
