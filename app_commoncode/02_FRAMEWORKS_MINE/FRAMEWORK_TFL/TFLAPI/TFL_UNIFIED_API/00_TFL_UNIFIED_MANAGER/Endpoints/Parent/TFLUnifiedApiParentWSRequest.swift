//
//  ParentTFLWSRequest.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 13/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

class TFLUnifiedApiParentWSRequest: ParentWSRequest{
    
    
    override var webServiceEndpoint :String?{
        get {
            return "https://api.tfl.gov.uk"
        }
    }
    
    //httpResponse.statusCode:429
    let app_id = "eee441d5"
    let app_key = "6561ea5d2bc2b50e4eeecc54f81d73e1"
    
    
    override init(){
        super.init()
        super.query = "app_id=\(app_id)&app_key=\(app_key)"
    }
}
