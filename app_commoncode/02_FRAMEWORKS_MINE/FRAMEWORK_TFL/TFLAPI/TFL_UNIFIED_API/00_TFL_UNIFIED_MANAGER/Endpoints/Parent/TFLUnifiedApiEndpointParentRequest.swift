//
//  TFLApiEndpointParentRequest
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 21/07/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation


class TFLUnifiedApiEndpointParentRequest : TFLUnifiedApiParentWSRequest{
    
    var tflUnifiedEndPoint :TFLUnifiedEndPoint?

    //override init(){
    //    super.init()
    //    
    //    
    //}
    
    init(tflUnifiedEndPoint :TFLUnifiedEndPoint){
        
        super.init()
        
        self.tflUnifiedEndPoint = tflUnifiedEndPoint
        
        //if let query = super.query {
        //    
        //    super.query = "\(tflUnifiedEndPoint.path())"
        //    
        //}else{
        //    logger.error("super.query is nil")
        //}
        
        
    }
    
    //----------------------------------------------------------------------------------------
    override var webServiceEndpoint :String?{
        get {
            
            var webServiceEndpoint_ :String? = nil
            
            //"https://api.tfl.gov.uk"
            if let webServiceEndpoint = super.webServiceEndpoint {
                //----------------------------------------------------------------------------------------
                //\Place\
                //----------------------------------------------------------------------------------------
                if let tflUnifiedEndPoint = self.tflUnifiedEndPoint {
                    webServiceEndpoint_ = "\(webServiceEndpoint)/\(tflUnifiedEndPoint.path())/"
                    
                }else{
                    logger.error("self.tflEndPoint is nil")
                }
                //----------------------------------------------------------------------------------------
                
                
            }else{
                logger.error("super.webServiceEndpoint is nil")
            }
            return webServiceEndpoint_
            
        }
    }
    //----------------------------------------------------------------------------------------

    override var urlString :String? {
        get {
            var urlString_ :String?
            
            if let query = query{
                if let webServiceEndpoint = self.webServiceEndpoint {
                    urlString_ = "\(webServiceEndpoint)?\(query)"
                    //some : "https://api.tfl.gov.uk/Place//Type/TaxiRank?app_id=eee441d5&app_key=6561ea5d2bc2b50e4eeecc54f81d73e1"
                    // TODO: - cleanup
                    let _ = parametersAppendAndCheckRequiredFieldsAreSet()
                    
                }else{
                    logger.error("self.webServiceEndpoint is nil")
                }
                
            }else{
                logger.error("query is nil - should be set by subclass")
            }
            
            return urlString_
        }
    }
    
    override func parametersAppendAndCheckRequiredFieldsAreSet() -> Bool{
        
        var requiredFieldsAreSet = false
        
        let requiredFieldsAreSetInSuper = super.parametersAppendAndCheckRequiredFieldsAreSet()
        if requiredFieldsAreSetInSuper{
            
            //------------------------------------------------
            //REQUIRED
            //------------------------------------------------
            //if let origin = origin{
            //    if let destination = destination{
            //        //------------------------------------------------
            //        // TODO: - DOESNT HANDLED blanks
            //        parameters["origin"] = origin as AnyObject?
            //        parameters["destination"] = destination as AnyObject?
            //        if origin == destination{
            //            logger.error("origin and destination are same")
            //        }else{
            //
            //        }
            //        requiredFieldsAreSet = true
            //        //------------------------------------------------
            //    }else{
            //        logger.error("radius is nil")
            //    }
            //}else{
            //    logger.error("origin is nil")
            //}
            //------------------------------------------------
            requiredFieldsAreSet = true
            //------------------------------------------------
            if requiredFieldsAreSet{
                //---------------------------------------------------------------------
                //OPTIONAL PARAMETERS
                //---------------------------------------------------------------------
                
//                self.addToParametersIfNotNil("scheduledate", string: self.scheduledate)
//                self.addToParametersIfNotNil("scheduletime", string: self.scheduletime)
//                self.addToParametersIfNotNil("flightname", string: self.flightname)
//                self.addToParametersIfNotNil("flightdirection", string:self.flightdirection)
//                self.addToParametersIfNotNil("airline", string:self.airline)
//                self.addToParametersIfNotNil("nvlscode", string:self.nvlscode)
//                self.addToParametersIfNotNil("route", string:self.route)
//                self.addToParametersIfNotNil("includedelays", string:self.includedelays)
//                self.addToParametersIfNotNil("page", string:self.page)
//                self.addToParametersIfNotNil("sort", string:self.sort)
//                self.addToParametersIfNotNil("fromdate", string:self.fromdate)
//                self.addToParametersIfNotNil("todate", string:self.todate)
                
                
                //---------------------------------------------------------------------
            }else{
                logger.error("requiredFieldsAreSet is false - skip OPTIONAL FIELDS")
            }
            //------------------------------------------------
        }else{
            //required param missing in super class
            requiredFieldsAreSet = false
        }
        
        return requiredFieldsAreSet
    }
}
