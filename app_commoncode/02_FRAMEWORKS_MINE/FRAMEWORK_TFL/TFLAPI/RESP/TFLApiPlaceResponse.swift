//
//  TFLApiPlaceResponse.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 20/01/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation


class TFLApiPlaceResponse: ParentGenericListViewDataSource{

    var dictionaryParentWSRequestsSent = [TFLApiPlaceType: TFLUnifiedApiPlaceRequest]()
    
    func storeRequest(tflUnifiedApiPlaceRequest: TFLUnifiedApiPlaceRequest){
        
        self.dictionaryParentWSRequestsSent[tflUnifiedApiPlaceRequest.tflApiPlaceType] = tflUnifiedApiPlaceRequest
        
    }
    
    func tflApiPlaceArrayProcessed(tflApiPlaceType: TFLApiPlaceType) -> [TFLApiPlace]?{
        var tflApiPlaceArrayOut: [TFLApiPlace]? = nil
        
        if let tflUnifiedApiPlaceRequest = self.dictionaryParentWSRequestsSent[tflApiPlaceType]{
            if let tflApiPlaceArray = tflUnifiedApiPlaceRequest.tflApiPlaceArray {
                tflApiPlaceArrayOut = tflApiPlaceArray
                
            }else{
                logger.error("tflUnifiedApiPlaceRequest.tflApiPlaceArray is nil")
            }
            
        }else{
            logger.error("dictionaryParentWSRequestsSent[tflApiPlaceType:'\(tflApiPlaceType)'] FAILED")
        }
        return tflApiPlaceArrayOut
    }
    
    
//    //the objects in the json
//    //some are versioned
//    var tflApiPlaceArrayProcessed1 = [TFLApiPlace](){
//        didSet {
//            // TODO: - CLEANUP after test
//            // TODO: - need a default - show all
//            //WRONG taxi query also called for
//            //requery( colcQuery: TFLApiPlaceTaxiRank.colcQuery_WorkingRanks())
//            //... tflApiPlaceArrayProcessed is ivar in TFLAPiResponse - requery triggered when GeneVC.genericRespon.didSet
//        }
//    }
//
//    var tflApiPlaceDictionaryProcessed1 = [String: TFLApiPlace](){
//        didSet {
//            // TODO: - CLEANUP after test
//            // TODO: - need a default - show all
//            //WRONG taxi query also called for
//            //requery( colcQuery: TFLApiPlaceTaxiRank.colcQuery_WorkingRanks())
//            //... tflApiPlaceArrayProcessed is ivar in TFLAPiResponse - requery triggered when GeneVC.genericRespon.didSet
//        }
//    }

    
    func areAllRequestsProcessed() -> Bool{
        //default to true - if any uncompleted found then set to false and break
        var allRequestsProcessed = true
        
        let keysArray = Array(dictionaryParentWSRequestsSent.keys)
        for key: TFLApiPlaceType in keysArray{
            
            if let tflUnifiedApiPlaceRequest = dictionaryParentWSRequestsSent[key] {
                
                switch tflUnifiedApiPlaceRequest.parentWSRequestState{
                case .ws_not_called_yet:
                    allRequestsProcessed = false
                    break
                    
                case .ws_in_progress:
                    allRequestsProcessed = false
                    break
                    
                case .ws_returned_and_processed:
                    //ok allRequestsProcessed = true
                    print("parentWSRequestState FOUND : wsreturnedandprocessed")
                    
                }
            }else{
                logger.error("dictionaryParentWSRequestsSent[key:'\(key)'] FAILED")
            }
        }
        
        return allRequestsProcessed
    }
    
    //--------------------------------------------------------------
    // MARK: - RUN QUERY
    // MARK: -
    //--------------------------------------------------------------
    
    override func requery(colcQuery: COLCQuery){
        // TODO: - move code in subclass up COLCQuerably
        logger.debug("requery : SUBCLASS - rerun query")
        // TODO: - 4JULY array of types
        self.requery(colcQuery: colcQuery, tflApiPlaceType: .taxiRank)
        
    }
    
    // TODO: - put this method IN COLCQUERY
    func requery(colcQuery: COLCQuery, tflApiPlaceType: TFLApiPlaceType){
        let startDate = Date()
        
        //result and count stay in colcQuery - so store it
        self.colcQuery = colcQuery
        
        logger.debug("**** REQUERY START ****")
        //---------------------------------------------------------
        //RUN THE QUERY:
        //---------------------------------------------------------
        
        if let tflApiPlaceArrayOut = self.tflApiPlaceArrayProcessed(tflApiPlaceType: tflApiPlaceType) {
             colcQuery.requery(arrayCOLCQueryable: tflApiPlaceArrayOut)
            
        }else{
            logger.error("self.tflApiPlaceArrayProcessed(tflApiPlaceType: tflApiPlaceType) is nil")
        }

        //----------------------------------------------------------------------------------------
        //QUERY END
        //----------------------------------------------------------------------------------------
        let endDate = Date()
        let seconds = endDate.timeIntervalSince(startDate)
        logger.debug("**** REQUERY ENDED [seconds:\(seconds)] ****")
        //----------------------------------------------------------------------------------------
    }
    
    //--------------------------------------------------------------
    // MARK: - TOTAL RESULTS COUNT > label
    // MARK: -
    //--------------------------------------------------------------

    //displayed in label - its the filtered results - ungrouped array count
    override var totalsCount: Int {
        if let colcQuery = self.colcQuery {
            return colcQuery.resultsFilteredCount
        }else{
            logger.error("self.colcQuery is nil")
            return 0
        }
    }
    
    override var dictionaryTableData : Dictionary<String, Array<Any>>{
        
        if let colcQuery = self.colcQuery {
            return colcQuery.dictionaryTableData_Result
        }else{
            logger.error("[var dictionaryTableData] getter - self.colcQuery is nil")
            
            return Dictionary<String, Array<Any>>()
        }
    }
}

