//
//  TFLApiWSController.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 13/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation




class TFLApiWSController : ParentWSController{
    
    
    //--------------------------------------------------------------
    // https://api.tfl.gov.uk/Place/Type/TaxiRank
    // https://api.tfl.gov.uk/Place/Type/JamCam
    //--------------------------------------------------------------
    
    
    //--------------------------------------------------------------
    // MARK: - Call WS - managed by delegate and state
    // MARK: -
    //--------------------------------------------------------------
    var tflApiPlaceResponse : TFLApiPlaceResponse?
   
    
    //--------------------------------------------------------------
    // MARK: - request_TFLApiPlaceResponse_via_Delegates
    // MARK: -
    //--------------------------------------------------------------

    //get the response
    //if retrieve already just return it e.g. List called it already now user taps on map
    //if not then call ws 
    //if forceNewWSCall is true then always call ws - used on PullToResfresh
    
    func request_app_TFLApiPlaceTypeRequest_via_Delegates(tflApiPlaceType: TFLApiPlaceType, forceNewWSCall: Bool)
    {
        if tflApiPlaceType == .unknown {
            //ERROR
            logger.error("AppConfig.tflApiPlaceType == .unknown - must be set somewhere before app start")
        }else{
            //----------------------------------------------------------------------------------------
            //CALL WS
            //----------------------------------------------------------------------------------------
            let tflApiPlaceTypeRequest = TFLApiPlaceTypeRequest.init(tflApiPlaceType: tflApiPlaceType)
            
            self.request_TFLApiPlaceResponse_via_Delegates(tflApiPlaceTypeRequest, forceNewWSCall: forceNewWSCall)

        }
        
    }
    fileprivate func request_TFLApiPlaceResponse_via_Delegates(_ tflApiPlaceTypeRequest: TFLApiPlaceTypeRequest, forceNewWSCall: Bool)
    {
        if let tflApiPlaceResponse = self.tflApiPlaceResponse {
            
            if forceNewWSCall{
                //always call WS again - e.g. pull to refresh
                self.call_WithDelegate_TFLApiPlaceTypeRequest(tflApiPlaceTypeRequest)
            }else{
                //local copy found already
                self.delegate_response_success(parsedObject: tflApiPlaceResponse)
            }
            
        }else{
            logger.debug("self.tflApiPlaceResponse is nil - first time - calling ws")
            self.call_WithDelegate_TFLApiPlaceTypeRequest(tflApiPlaceTypeRequest)
        }
    }
    
    fileprivate func call_WithDelegate_TFLApiPlaceTypeRequest(_ tflApiPlaceTypeRequest: TFLApiPlaceTypeRequest)
    {
        //---------------------------------------------------------------------
        //CALL THE WS
        //---------------------------------------------------------------------
        if let _ =  tflApiPlaceTypeRequest.urlString{
            
            //---------------------------------------------------------------------
            //NOISY logger.debug("urlString:\r\(urlString)")
            //---------------------------------------------------------------------
            self.callWS(tflApiPlaceTypeRequest,
                success:{  (parentWSRequest: ParentWSRequest, parsedObject: Any?)->Void in
                    
                    if let tflApiPlaceTypeRequest = parentWSRequest as? TFLApiPlaceTypeRequest {
                        //----------------------------------------------------------------------------------------
                        //success
                        //----------------------------------------------------------------------------------------
                        
                        let tflApiPlaceResponse = TFLApiPlaceResponse()
                        
                        if tflApiPlaceTypeRequest.tflApiPlaceType == .taxiRank{
                            //------------------------------------------------------------------------------------------------
                            if let tflApiPlaceTaxiRankArray : [TFLApiPlaceTaxiRank] = Mapper<TFLApiPlaceTaxiRank>().mapArray(JSONObject:parsedObject)
                            {
                                //---------------------------------------------------------
                                //PROCESS TAXI RANK VERSION - KEEP ONLY THE LATEST
                                //---------------------------------------------------------
                                let tflApiPlaceTaxiRankArrayProcessed: [TFLApiPlaceTaxiRank] = TFLApiPlaceTaxiRank.processTankRankVersions(tflApiPlaceTaxiRankArray: tflApiPlaceTaxiRankArray)
                                
                                //tflApiPlaceResponse.tflApiPlaceArrayUnsorted = tflApiPlaceTaxiRankArrayProcessed
                                tflApiPlaceResponse.tflApiPlaceArrayProcessed = tflApiPlaceTaxiRankArrayProcessed
                                
                                //--------------------------------------------------------------------
                            }
                            else{
                                logger.error("parsedObject is nil or not [TFLApiPlace]")
                            }
                            
                        }else if tflApiPlaceTypeRequest.tflApiPlaceType == .jamCam{
                            //------------------------------------------------------------------------------------------------
                            if let tflApiPlaceTFLApiPlaceJamCamArray : [TFLApiPlaceJamCam] = Mapper<TFLApiPlaceJamCam>().mapArray(JSONObject:parsedObject)
                            {
                                //tflApiPlaceResponse.tflApiPlaceArrayUnsorted = tflApiPlaceTFLApiPlaceJamCamArray
                                tflApiPlaceResponse.tflApiPlaceArrayProcessed = tflApiPlaceTFLApiPlaceJamCamArray
                                
                            }
                            else{
                                logger.error("parsedObject is nil or not [TFLApiPlaceJamCam]")
                            }
                            
                        }else{
                            
                            //All other TFLApiPlace subtypes - JamCam etc
                            
                            //-------------------------------------------------------------------
                            //TFLApiPlaceTaxiRank is TFLApiPlace
                            if let tflApiPlaceArray : [TFLApiPlace] = Mapper<TFLApiPlace>().mapArray(JSONObject:parsedObject)
                            {
                                //--------------------------------------------------------------------
                                //tflApiPlaceResponse.tflApiPlaceArrayUnsorted = tflApiPlaceArray
                                tflApiPlaceResponse.tflApiPlaceArrayProcessed = tflApiPlaceArray
                                //--------------------------------------------------------------------
                                
                            }else{
                                logger.error("parsedObject is nil or not [TFLApiPlace]")
                            }
                            //-------------------------------------------------------------------
                        }
                        
                        
                        //logger.debug("tflApiPlaceResponse.tflApiPlaceArrayUnsorted returned:\(tflApiPlaceResponse.tflApiPlaceArrayUnsorted.count)")
                        logger.debug("tflApiPlaceResponse.tflApiPlaceArrayProcessed returned:\(tflApiPlaceResponse.tflApiPlaceArrayProcessed.count)")
                        //---------------------------------------------------------------------
                        //RESPONSE OK - CACHE IT
                        //---------------------------------------------------------------------
                        self.tflApiPlaceResponse = tflApiPlaceResponse
                        
                        self.delegate_response_success(parsedObject: tflApiPlaceResponse)
                        //---------------------------------------------------------------------
                    }else{
                        logger.error("success: but parentWSRequest as? TFLApiPlaceTypeRequest is nil")
                    }
                },
                failure:{ (tflApiPlaceTypeRequest, error) -> Void in
                    self.delegate_response_failure(error: error)
                }
            )
            //---------------------------------------------------------------------
        }else{
            logger.error("urlString is nil - 4755")
        }
    }
    
    
    
    //--------------------------------------------------------------
    // MARK: - Call WS - managed by block - ok for one vc but not when map and list share same response
    // MARK: -
    //--------------------------------------------------------------
    func call_withBlocks_TFLApiPlaceTypeRequest(_ tflApiPlaceTypeRequest: TFLApiPlaceTypeRequest,
                                     success: @escaping (_ tflApiPlaceResponse : TFLApiPlaceResponse) -> Void,
                                     failure: @escaping (_ error: Error) -> Void
        )
    {
        
        //---------------------------------------------------------------------
        //CALL THE WS
        //---------------------------------------------------------------------
        if let _ =  tflApiPlaceTypeRequest.urlString{
            //---------------------------------------------------------------------
            //NOISY logger.debug("urlString:\r\(urlString)")
            //---------------------------------------------------------------------
            self.callWS(
                tflApiPlaceTypeRequest,
                success:{ (parentWSRequest, parsedObject: Any?)->Void in
                    //----------------------------------------------------------------------------------------
                    //success
                    //----------------------------------------------------------------------------------------
                    if let tflApiPlaceTypeRequest = parentWSRequest as? TFLApiPlaceTypeRequest {
                        
                        let tflApiPlaceResponse = TFLApiPlaceResponse()
                        
                        if tflApiPlaceTypeRequest.tflApiPlaceType == .taxiRank{
                            
                            //------------------------------------------------------------------------------------------------
                            if let tflApiPlaceTaxiRankArray : [TFLApiPlaceTaxiRank] = Mapper<TFLApiPlaceTaxiRank>().mapArray(JSONObject:parsedObject)
                            {
                                //---------------------------------------------------------
                                //PROCESS TAXI RANK VERSION - KEEP ONLY THE LATEST
                                //---------------------------------------------------------
                                let tflApiPlaceTaxiRankArrayProcessed: [TFLApiPlaceTaxiRank] = TFLApiPlaceTaxiRank.processTankRankVersions(tflApiPlaceTaxiRankArray: tflApiPlaceTaxiRankArray)
                                
                                tflApiPlaceResponse.tflApiPlaceArrayProcessed = tflApiPlaceTaxiRankArrayProcessed
                                
                                //--------------------------------------------------------------------
                            }
                            else{
                                logger.error("parsedObject is nil or not [TFLApiPlace]")
                            }
                            
                        }else if tflApiPlaceTypeRequest.tflApiPlaceType == .jamCam{
                            //------------------------------------------------------------------------------------------------
                            if let tflApiPlaceTFLApiPlaceJamCamArray : [TFLApiPlaceJamCam] = Mapper<TFLApiPlaceJamCam>().mapArray(JSONObject:parsedObject)
                            {
                                tflApiPlaceResponse.tflApiPlaceArrayProcessed = tflApiPlaceTFLApiPlaceJamCamArray
                                
                            }
                            else{
                                logger.error("parsedObject is nil or not [TFLApiPlaceJamCam]")
                            }
                            
                        }else{
                            
                            //All other TFLApiPlace subtypes - JamCam etc
                            
                            //-------------------------------------------------------------------
                            //TFLApiPlaceTaxiRank is TFLApiPlace
                            if let tflApiPlaceArray : [TFLApiPlace] = Mapper<TFLApiPlace>().mapArray(JSONObject:parsedObject)
                            {
                                //--------------------------------------------------------------------
                                tflApiPlaceResponse.tflApiPlaceArrayProcessed = tflApiPlaceArray
                                //--------------------------------------------------------------------
                                
                            }else{
                                logger.error("parsedObject is nil or not [TFLApiPlace]")
                            }
                            //-------------------------------------------------------------------
                        }
                        
                        
                        logger.debug("tflApiPlaceResponse.tflApiPlaceArrayUnsorted returned:\(tflApiPlaceResponse.tflApiPlaceArrayProcessed.count)")
                        //---------------------------------------------------------------------
                        //RESPONSE OK
                        //---------------------------------------------------------------------
                        success(tflApiPlaceResponse)
                        //---------------------------------------------------------------------
                        
                    }else{
                        logger.error("parentWSRequest as? TFLApiPlaceTypeRequest is nil")
                    }
                },
                failure:{(parentWSRequest,error) -> Void in
                    failure(error)
                }
            )
            //---------------------------------------------------------------------
        }else{
            logger.error("urlString is nil - 4766")
        }
    }
    
    
//    func processTankRankVersions(tflApiPlaceTaxiRankArray : [TFLApiPlaceTaxiRank]) -> [TFLApiPlaceTaxiRank]{
//        var tflApiPlaceTaxiRankDictionary = [String : TFLApiPlaceTaxiRank]()
//
//        //VERSIONED:TaxiRank_4849-1 - ver:(Optional(1) - rankIdUnversioned:(Optional("TaxiRank_4849"))
//        //VERSIONED:TaxiRank_4849-2 - ver:(Optional(2) - rankIdUnversioned:(Optional("TaxiRank_4849"))
//        //VERSIONED:TaxiRank_4849-3 - ver:(Optional(3) - rankIdUnversioned:(Optional("TaxiRank_4849"))
//        var countVersioned = 0
//        var countUnVersioned = 0
//
//        for tflApiPlaceTaxiRankNEW: TFLApiPlaceTaxiRank in tflApiPlaceTaxiRankArray{
//
//            if let tflApiPlaceTaxiRankInArrayVersionInfoNEW = tflApiPlaceTaxiRankNEW.tflApiPlaceTaxiRankVersionInfo {
//
//                if tflApiPlaceTaxiRankNEW.isVersioned{
//
//
//                    //print("VERSIONED:\(tflApiPlaceTaxiRankNEW.idSafe) - ver:(\(tflApiPlaceTaxiRankNEW.tflApiPlaceTaxiRankInfo?.version) - rankIdUnversioned:(\(tflApiPlaceTaxiRankNEW.tflApiPlaceTaxiRankInfo?.rankIdUnversioned))")
//
//                    //----------------------------------------------------------------------------------------
//                    //EXISTING
//                    //----------------------------------------------------------------------------------------
//                    if let tflApiPlaceTaxiRankEXISTING = tflApiPlaceTaxiRankDictionary[tflApiPlaceTaxiRankInArrayVersionInfoNEW.rankIdUnversioned]{
//                        //a version with this rankid is already in this spot -
//                        //check the version int - if lower then replace with new on
//                        if let tflApiPlaceTaxiRankInDictVersionInfoEXISTING = tflApiPlaceTaxiRankEXISTING.tflApiPlaceTaxiRankVersionInfo {
//
//                            //logger.debug("tflApiPlaceTaxiRankEXISTING :\(tflApiPlaceTaxiRankInDictVersionInfoEXISTING.version)")
//                            //logger.debug("tflApiPlaceTaxiRankNEW      :\(tflApiPlaceTaxiRankInArrayVersionInfoNEW.version)")
//
//                            if tflApiPlaceTaxiRankInDictVersionInfoEXISTING.version < tflApiPlaceTaxiRankInArrayVersionInfoNEW.version{
//                                //logger.debug("REPLACING VERSION WITH HIGHER")
//                                tflApiPlaceTaxiRankDictionary[tflApiPlaceTaxiRankInArrayVersionInfoNEW.rankIdUnversioned] = tflApiPlaceTaxiRankNEW
//                            }else{
//                                //logger.error("DONT REPLACE VERSION - SAME OR LOWER Version")
//                            }
//
//                        }else{
//                            logger.error("tflApiPlaceTaxiRankInDict.tflApiPlaceTaxiRankInfo is nil")
//                        }
//
//
//                    }else{
//                        //all versions will be inserted at least once - after that entry will be replace so total version count wont inc
//
//                        countVersioned += 1
//                        //versioned - but this is first time this unversioned key found - safe to insert
//                        tflApiPlaceTaxiRankDictionary[tflApiPlaceTaxiRankInArrayVersionInfoNEW.rankIdUnversioned] = tflApiPlaceTaxiRankNEW
//                    }
//                    //----------------------------------------------------------------------------------------
//
//                }else{
//                    //not versioned - just add to dictionary
//
//                    if let _ = tflApiPlaceTaxiRankDictionary[tflApiPlaceTaxiRankInArrayVersionInfoNEW.rankIdUnversioned]{
//                        logger.error("CANT ADD OBJ AT KEY ALREADY:\(tflApiPlaceTaxiRankInArrayVersionInfoNEW.rankIdUnversioned)")
//                    }else{
//                        countUnVersioned += 1
//                        tflApiPlaceTaxiRankDictionary[tflApiPlaceTaxiRankInArrayVersionInfoNEW.rankIdUnversioned] = tflApiPlaceTaxiRankNEW
//                    }
//                }
//
//            }else{
//                logger.error("tflApiPlaceTaxiRank.tflApiPlaceTaxiRankInfo is nil - cant process")
//            }
//
//        }//for
//
//        //print("WS             : tflApiPlaceTaxiRankArray.count               :\(tflApiPlaceTaxiRankArray.count)")
//        //print("a. Versions       : countVersions (even if 3 v only 1 is left in dict)  :\(countVersioned)")
//        //print("b. Versions found : countUnVersioned                                    :\(countUnVersioned)")
//        //print("tflApiPlaceTaxiRankDictionary.keys.count:\(tflApiPlaceTaxiRankDictionary.keys.count)")
//
//        if (countVersioned + countUnVersioned) == tflApiPlaceTaxiRankDictionary.keys.count{
//            //OK - each version only inserted once
//            //logger.debug("SUCCESS (countVersioned:\(countVersioned) + countUnVersioned:\(countUnVersioned)) == tflApiPlaceTaxiRankDictionary.keys.count:\(tflApiPlaceTaxiRankDictionary.keys.count) - each version inserted once and highest version used")
//        }else{
//            logger.error("ERROR; (countVersioned:\(countVersioned) + countUnVersioned:\(countUnVersioned)) == tflApiPlaceTaxiRankDictionary.keys.count:\(tflApiPlaceTaxiRankDictionary.keys.count) FAILED")
//        }
//
//        //--------------------------------------------------------------
//        // DICT - array sorted by key(TaxiRankId)
//        //--------------------------------------------------------------
//        let keysArrayUnsorted = Array(tflApiPlaceTaxiRankDictionary.keys)
//
//        let keysArraySorted = keysArrayUnsorted.sorted { (string0, string1) -> Bool in
//            if string0.compare(string1, options: .numeric) == .orderedAscending{
//                return true
//
//            }else{
//                return false
//            }
//        }
//
//        var tflApiPlaceTaxiRankArrayProcessed = [TFLApiPlaceTaxiRank]()
//        for key_rankId in keysArraySorted{
//            //print("key_rankId: \(key_rankId)")
//
//            if let tflApiPlaceTaxiRank: TFLApiPlaceTaxiRank = tflApiPlaceTaxiRankDictionary[key_rankId] {
//
//
//                //----------------------------------------------------------------------------------------
//                //FIX BROKEN ranks - two in north sea
//                //----------------------------------------------------------------------------------------
//
//                //https://blog.tfl.gov.uk/2016/12/13/unified-api-taxi-ranks-added-to-places/comment-page-1/#comment-27049
//
//                /*
//                 ============================
//                 "id": "TaxiRank_4045",
//                 "url": "https://api-argon.tfl.gov.uk/Place/TaxiRank_4045",
//                 "commonName": "Grosvenor Street",
//                 "placeType": "TaxiRank",
//                 "lat": 54.637877,
//                 "lon": -0.005901
//
//                 54.637877, -0.005901
//                 INCORRECT LOCATION
//                 https://www.google.co.uk/maps/place/54%C2%B038'16.4%22N+0%C2%B000'21.2%22W/@54.6378801,-0.008095,17z/data=!3m1!4b1!4m5!3m4!1s0x0:0x0!8m2!3d54.637877!4d-0.005901
//
//                 Correct – 51.5120928,-0.1457605
//                 https://www.google.co.uk/maps/place/51%C2%B030'43.5%22N+0%C2%B008'42.8%22W/@51.5120928,-0.1457605,19z/data=!3m1!4b1!4m5!3m4!1s0x0:0x0!8m2!3d51.512092!4d-0.145212
//
//                 ============================
//
//                 "id": "TaxiRank_4693",
//                 "url": "https://api-argon.tfl.gov.uk/Place/TaxiRank_4693",
//                 "commonName": "Station Road (Harold Wood Station), Harold Wood",
//
//                 "lat": 54.863954,
//                 "lon": 0.410287
//
//                 54.863954, 0.410287
//                 https://www.google.co.uk/maps/place/54%C2%B051'50.2%22N+0%C2%B024'37.0%22E/@54.8639571,0.408093,17z/data=!3m1!4b1!4m5!3m4!1s0x0:0x0!8m2!3d54.863954!4d0.410287
//                 CORRECT LOCATION
//                 51.5934154,0.2335873
//                 https://www.google.co.uk/maps/place/Harold+Wood/@51.5934154,0.2335873,21z/data=!4m5!3m4!1s0x47d8bb78d96aa989:0x49d1c6ed3bcfeeb3!8m2!3d51.59277!4d0.2331
//
//                 ============================
//
//                 */
//                // TODO: - CHECK THESE SAID THEY WERE FIXED - in north sea - check for versions
//                if let rankId = tflApiPlaceTaxiRank.id  {
//                    if rankId == "TaxiRank_4045"{
//                        //noisy logger.error("HACK - two taxi ranks hard coded - TaxiRank_4045")
//                        tflApiPlaceTaxiRank.lat = 51.5120928
//                        tflApiPlaceTaxiRank.lon = -0.1457605
//
//                    }else if rankId == "TaxiRank_4693"{
//                        //noisy logger.error("HACK - two taxi ranks hard coded - TaxiRank_4693")
//                        tflApiPlaceTaxiRank.lat = 51.5934154
//                        tflApiPlaceTaxiRank.lon = 0.2335873
//
//                    }else if rankId == "TaxiRank_5948"{
//                        //noisy logger.error("HACK - London Bridge Bus Station (London Bridge Station) streetview underground - TaxiRank_5948")
//                        tflApiPlaceTaxiRank.lat = 51.5049307
//                        tflApiPlaceTaxiRank.lon = -0.0866951
//
//                    }
//                    else{
//                        //other rank - ignore
//                    }
//                }else{
//                    //other rank - ignore
//                }
//
//
//                //----------------------------------------------------------------------------------------
//                //APPEND
//                //----------------------------------------------------------------------------------------
//                tflApiPlaceTaxiRankArrayProcessed.append(tflApiPlaceTaxiRank)
//
//                //----------------------------------------------------------------------------------------
//            }else{
//                logger.error("tflApiPlaceTaxiRankDictionary[key_rankId:'\(key_rankId)']  is nil")
//            }
//        }
//
//        //----------------------------------------------------------------------------------------
//        //ADD MISSING rank reported by drivers
//        //----------------------------------------------------------------------------------------
//        //https://www.google.co.uk/maps/@51.4741891,-0.1831795,3a,75y,277.97h,85.77t/data=!3m6!1e1!3m4!1sPukqQJmqcfCgHoBWAjquLg!2e0!7i13312!8i6656!6m1!1e1
//
//        let tflApiPlaceTaxiRankNovotel = TFLApiPlaceTaxiRank.createWorkingRank(id:"TaxiRank_9999",
//                                                                               commonName: "DoubleTree Imperial Wharf - Chelsea (Driver Reported)",
//                                                                               lat: 51.4741891,
//                                                                               lon: -0.183179,
//                                                                               Borough: "Chelsea")
//
//        tflApiPlaceTaxiRankArrayProcessed.append(tflApiPlaceTaxiRankNovotel)
//
//
//
//
//
//        //----------------------------------------------------------------------------------------
//        //15 feb - 580 in json + 1 added = 581
//        //NOISYprint("FINAL: tflApiPlaceTaxiRankArrayProcessed :\(tflApiPlaceTaxiRankArrayProcessed.count)")
//        return tflApiPlaceTaxiRankArrayProcessed
//
//    }
}
