//
//  TFLApiPlaceRequest.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 13/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

//https://blog.tfl.gov.uk/2016/12/13/unified-api-taxi-ranks-added-to-places/#more-3308
//https://api.tfl.gov.uk/Place/Type/TaxiRank?app_id=eee441d5&app_key=6561ea5d2bc2b50e4eeecc54f81d73e1
//https://api.tfl.gov.uk/Place/Type/JamCams?app_id=eee441d5&app_key=6561ea5d2bc2b50e4eeecc54f81d73e1

class TFLApiPlaceTypeRequest : TFLApiParentWSRequest{

    //let webServiceEndpoint_TFLPlaceType: String = "https://api.tfl.gov.uk/Place/Type/TaxiRank"
    //let webServiceEndpoint_TFLPlaceType: String = "https://api.tfl.gov.uk/Place/Type/"
    
    var tflApiPlaceType : TFLApiPlaceType = .unknown

    //should be set by subclass
    override init(){
        super.init()
    
    }
//    
//    override var webServiceEndpoint :String? {
//        get {
//            
//            var webServiceEndpoint_ : String? = nil
//            
//            if let webServiceEndpoint = super.webServiceEndpoint {
//                if self.tflApiPlaceType == .unknown{
//                    logger.error("[TFLPlaceTypeRequest] self.tflApiPlaceType is .unknown - cant set webServiceEndpoint")
//                    
//                }else{
//                    //webServiceEndpoint_ = "https://api.tfl.gov.uk/Place/Type/\(self.tflApiPlaceType)"
//                    
//                    webServiceEndpoint_ = "\(webServiceEndpoint)/Place/Type/\(self.tflApiPlaceType.apiStringForType)"
//                }
//            }else{
//                logger.error("super.webServiceEndpoint is nil")
//            }
//            
//            return webServiceEndpoint_
//        }
//    }
//    
    convenience init(tflApiPlaceType: TFLApiPlaceType){
        self.init()
        self.tflApiPlaceType = tflApiPlaceType
       
        
    }
//
//    override var urlString :String? {
//        get {
//            var urlString_ :String?
//            
//            if let query = query{
//                if let webServiceEndpoint = self.webServiceEndpoint {
//                    urlString_ = "\(webServiceEndpoint)?\(query)"
//                    
//                    // TODO: - cleanup
//                    let _ = parametersAppendAndCheckRequiredFieldsAreSet()
//                    
//                }else{
//                    logger.error("self.webServiceEndpoint is nil")
//                }
//               
//            }else{
//                logger.error("query is nil - should be set by subclass")
//            }
//            
//            return urlString_
//        }
//    }
//    
//    override func parametersAppendAndCheckRequiredFieldsAreSet() -> Bool{
//        var requiredFieldsAreSet = false
//        
//        requiredFieldsAreSet = super.parametersAppendAndCheckRequiredFieldsAreSet()
//        if requiredFieldsAreSet{
//            
//            //DEBUG - comment out "key" to trigger "REQUEST_DENIED"
//            //status = "REQUEST_DENIED";
//            
//            //ADD THE GOOGLE API KEYS - stored in Delegate
//            //Two key WEB and IOS - I use WEB
//            //parameters["key"] = GoogleApiKeys.googleMapsApiKey_Web
//            //parameters["key"] = GoogleApiKeys.googleMapsApiKey_iOS
//            //press GET A KEY on API docs created server key not ios key
//            
//            //SWIFT 3 parameters["key"] = GoogleApiKeys.googleMapsApiKey_Serverkey
//            //parameters["key"] = GoogleApiKeys.googleMapsApiKey_Serverkey as AnyObject
//            
//            
//            requiredFieldsAreSet = true
//        }else{
//            //required param missing in super class
//        }
//        
//        return requiredFieldsAreSet
//    }
}
