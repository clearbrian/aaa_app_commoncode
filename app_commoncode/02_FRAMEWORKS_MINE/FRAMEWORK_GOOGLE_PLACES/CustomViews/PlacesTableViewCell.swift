//
//  PlacesTableViewCell.swift
//  joyride
//
//  Created by Brian Clear on 18/08/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit

class PlacesTableViewCell : UITableViewCell{
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDetail: UILabel!
    
    @IBOutlet weak var labelSubDetail: UILabel!
    @IBOutlet weak var imageViewPlaceResult: UIImageView!
    @IBOutlet weak var labelDistanceMeters: UILabel!
    
    var customFontApplied = false

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //print("PlacesTableViewCell init coder")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        
        super.init(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: reuseIdentifier)
        
    }
    
    func setup(){
        
    }
    
    override func draw(_ rect: CGRect) {
        
        //print("drawRect")
        //didnt work here only for cells off screen
        //applyCustomFont()
    }
    
    func applyCustomFont(){
        if customFontApplied{
            //cell created already - BODY is replace with customfont - if we reuse it and call this again it will try and convert font again
        }else{
            self.labelTitle?.applyCustomFontForCurrentTextStyle()
            self.labelDetail?.applyCustomFontForCurrentTextStyle()
            self.labelSubDetail?.applyCustomFontForCurrentTextStyle()
            self.labelDistanceMeters?.applyCustomFontForCurrentTextStyle()
            customFontApplied = true
        }
    }
}
