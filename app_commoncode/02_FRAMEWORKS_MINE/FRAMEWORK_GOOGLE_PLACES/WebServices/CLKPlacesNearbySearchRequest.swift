//
//  CLKPlacesNearbySearchRequest.swift
//  joyride
//
//  Created by Brian Clear on 03/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation

/*
//---------------------------------------------------------------------
Required parameters
//---------------------------------------------------------------------
key
— Your application's API key.

location
— The latitude/longitude around which to retrieve place information. This must be specified as latitude,longitude.

radius
— Defines the distance (in meters) within which to return place results.
The maximum allowed radius is 50 000 meters.
Note that radius must not be included if rankby=distance (described under Optional parameters below) is specified.

If rankby=distance (described under Optional parameters below) is specified, then one or more of
keyword,
name, or
types is required.


//---------------------------------------------------------------------
Optional parameters
//---------------------------------------------------------------------
keyword
— A term to be matched against all content that Google has indexed for this place, including but not limited to name, type, and address, as well as customer reviews and other third-party content.
language
— The language code, indicating in which language the results should be returned, if possible. See the list of supported languages and their codes. Note that we often update supported languages so this list may not be exhaustive.
minprice and maxprice (optional)
— Restricts results to only those places within the specified range. Valid values range between 0 (most affordable) to 4 (most expensive), inclusive. The exact amount indicated by a specific value will vary from region to region.
name
— One or more terms to be matched against the names of places, separated with a space character. Results will be restricted to those containing the passed name values. Note that a place may have additional names associated with it, beyond its listed name. The API will try to match the passed name value against all of these names. As a result, places may be returned in the results whose listed names do not match the search term, but whose associated names do.
opennow
— Returns only those places that are open for business at the time the query is sent. Places that do not specify opening hours in the Google Places database will not be returned if you include this parameter in your query.
rankby
— Specifies the order in which results are listed. Possible values are:
prominence (default). This option sorts results based on their importance. Ranking will favor prominent places within the specified area. Prominence can be affected by a place's ranking in Google's index, global popularity, and other factors.
distance. This option sorts results in ascending order by their distance from the specified location. When distance is specified, one or more of keyword, name, or types is required.
types
— Restricts the results to places matching at least one of the specified types. Types should be separated with a pipe symbol (type1|type2|etc). See the list of supported types.
pagetoken
— Returns the next 20 results from a previously run search. Setting a pagetoken parameter will execute a search with the same parameters used previously
— all parameters other than pagetoken will be ignored.
zagatselected
— Add this parameter (just the parameter name, with no associated value) to restrict your search to locations that are Zagat selected businesses. This parameter must not include a true or false value. The zagatselected parameter is experimental, and is only available to Google Places API for Work customers.
https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-33.8670522,151.1957362&radius=500&types=food&name=cruise&key=API_KEY
*/

//https://maps.googleapis.com/maps/api/place/nearbysearch/output?parameters
class CLKPlacesNearbySearchRequest: CLKPlacesParentRequest {
    //---------------------------------------------------------------------
    //Required parameters
    //---------------------------------------------------------------------
    //in parent
    //var key:String?

    //"-33.8670522,151.1957362"
    var location:String?

    
    var radius:Double? = GooglePlacesController.defaultSearchRadius
    
    //---------------------------------------------------------------------
    //Optional parameters
    //---------------------------------------------------------------------
    var keyword:String?
    
    //-----------------------------------------------------------------------------------
    // LANGUAGE - moved up
    //https://developers.google.com/maps/faq#languagesupport
    //var language:String? //declared in CLKGoogleDirectionsParentRequest/CLKPlacesParentRequest
    //-----------------------------------------------------------------------------------
    
    var minprice:String?
    var maxprice:String?
    var name:String?
    var opennow:String?
    var rankby:String?
    var prominence:String?
    var distance:String?
    var types:String?
    var pagetoken:String?
    var zagatselected:String?


    //"https://maps.googleapis.com/maps/api/place/details/json?placeid=\(placeId)&key=\(appDelegate.googleMapsApiKey_Web)"
    
    override init(){
        super.init()
        query = "/nearbysearch/json"        /* must start with /  */
        
    }
    
    override func parametersAppendAndCheckRequiredFieldsAreSet() -> Bool{
        var requiredFieldsAreSet = false
        
        let requiredFieldsAreSetInSuper = super.parametersAppendAndCheckRequiredFieldsAreSet()
        if requiredFieldsAreSetInSuper{
            
            //------------------------------------------------
            if let location = location{
                if let radius = radius{
                    //------------------------------------------------
                    // TODO: - DOESNT HANDLED blanks
                    parameters["location"] = location as AnyObject?
                    parameters["radius"] = radius as AnyObject?
                    
                    requiredFieldsAreSet = true
                    //------------------------------------------------
                }else{
                    logger.error("radius is nil")
                }
            }else{
                logger.error("location_longitude is nil")
            }
           

            
            
            if requiredFieldsAreSet{

                //---------------------------------------------------------------------
                //OPTIONAL PARAMETERS
                //---------------------------------------------------------------------
                if let keyword:String = keyword{
                    parameters["keyword"] = keyword as AnyObject?
                }else{
                    //logger.debug("keyword:String is nil - OK - optional")
                }
                //---------------------------------------------------------------------
                if let language = language{
                    parameters["language"] = language as AnyObject?
                }else{
                    //logger.debug("language is nil - OK - optional")
                }
                //---------------------------------------------------------------------
                if let minprice = minprice{
                    parameters["minprice"] = minprice as AnyObject?
                }else{
                    //logger.debug("minprice is nil - OK - optional")
                }
                //---------------------------------------------------------------------
                if let maxprice = maxprice{
                    parameters["maxprice"] = maxprice as AnyObject?
                }else{
                    //logger.debug("maxprice is nil - OK - optional")
                }
                //---------------------------------------------------------------------
                if let name = name{
                    parameters["name"] = name as AnyObject?
                }else{
                    //logger.debug("name is nil - OK - optional")
                }
                //---------------------------------------------------------------------
                if let opennow = opennow{
                    parameters["opennow"] = opennow as AnyObject?
                }else{
                    //logger.debug("opennow is nil - OK - optional")
                }
                //---------------------------------------------------------------------
                if let rankby = rankby{
                    parameters["rankby"] = rankby as AnyObject?
                }else{
                    //logger.debug("rankby is nil - OK - optional")
                }
                //---------------------------------------------------------------------
                if let prominence = prominence{
                    parameters["prominence"] = prominence as AnyObject?
                }else{
                    //logger.debug("prominence is nil - OK - optional")
                }
                //---------------------------------------------------------------------
                if let distance = distance{
                    parameters["distance"] = distance as AnyObject?
                }else{
                    //logger.debug("distance is nil - OK - optional")
                }
                //---------------------------------------------------------------------
                if let types = types{
                    parameters["types"] = types as AnyObject?
                }else{
                    //logger.debug("types is nil - OK - optional")
                }
                //---------------------------------------------------------------------
                if let pagetoken = pagetoken{
                    parameters["pagetoken"] = pagetoken as AnyObject?
                }else{
                    //logger.debug("pagetoken is nil - OK - optional")
                }
                //---------------------------------------------------------------------
                if let zagatselected = zagatselected{
                    parameters["zagatselected"] = zagatselected as AnyObject?
                }else{
                    //logger.debug("zagatselected is nil - OK - optional")
                }
                //---------------------------------------------------------------------
            }else{
                logger.error("requiredFieldsAreSet is false - skip OPTIONAL FIELDS")
            }
            //------------------------------------------------
        }else{
            //required param missing in super class
            requiredFieldsAreSet = false
        }
        
        return requiredFieldsAreSet
    }
}
