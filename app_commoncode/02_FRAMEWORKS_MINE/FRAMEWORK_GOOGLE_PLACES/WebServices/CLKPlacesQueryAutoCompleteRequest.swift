//
//  CLKPlacesQueryAutoCompleteRequest.swift
//  joyride
//
//  Created by Brian Clear on 31/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation


//https://developers.google.com/places/webservice/autocomplete
//https://maps.googleapis.com/maps/api/place/autocomplete/output?parameters


//------------------------------------------------------------------------------------------------
//Required parameters
//------------------------------------------------------------------------------------------------
//input — The text string on which to search. The Places service will return candidate matches based on this string and order results based on their perceived relevance.
//key — Your application's API key. This key identifies your application for purposes of quota management. Visit the Google Developers Console to select an API project and obtain your key. Google Maps API for Work customers must use the API project created for them as part of their Google Places API for Work purchase.

//------------------------------------------------------------------------------------------------
//Optional parameters
//------------------------------------------------------------------------------------------------
//offset — The character position in the input term at which the service uses text for predictions. For example, if the input is 'Googl' and the completion point is 3, the service will match on 'Goo'. The offset should generally be set to the position of the text caret. If no offset is supplied, the service will use the entire term.

//location — The point around which you wish to retrieve place information. Must be specified as latitude,longitude.

//radius — The distance (in meters) within which to return place results. Note that setting a radius biases results to the indicated area, but may not fully restrict results to the specified area. See Location Biasing for more information.

//    language — The language in which to return results. See the supported list of domain languages. If language is not supplied, the Places service will attempt to use the native language of the domain from which the request is sent.


class CLKPlacesQueryAutoCompleteRequest: CLKPlacesParentRequest {
    //------------------------------------------------------------------------------------------------
    //Required parameters
    //------------------------------------------------------------------------------------------------
    var input:String?
    //---------------------------------------------------------------------
    //in parent
    //var key:String?
    
    
    //------------------------------------------------------------------------------------------------
    ////Optional parameters
    //------------------------------------------------------------------------------------------------
    var offset:String?
    //---------------------------------------------------------------------
    //"-33.8670522,151.1957362"
    var location:String?
    //---------------------------------------------------------------------
    var radius:Double? = GooglePlacesController.defaultSearchRadius
    //---------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    // LANGUAGE - moved up
    //https://developers.google.com/maps/faq#languagesupport
    //var language:String? //declared in CLKGoogleDirectionsParentRequest/CLKPlacesParentRequest
    //---------------------------------------------------------------------
    
    
    // TODO: - ADD LANGUAGES
    //https://developers.google.com/maps/faq#languagesupport
    
    override init(){
        super.init()
        //https://maps.googleapis.com/maps/api/place/queryautocomplete/output?parameters
        
        query = "/queryautocomplete/json"        /* must start with /  */
        
    }
    
    override func parametersAppendAndCheckRequiredFieldsAreSet() -> Bool{
        var requiredFieldsAreSet = false
        
        let requiredFieldsAreSetInSuper = super.parametersAppendAndCheckRequiredFieldsAreSet()
        if requiredFieldsAreSetInSuper{
            
            //------------------------------------------------
            if let input = input{
                //------------------------------------------------
                // TODO: - DOESNT HANDLED blanks
                parameters["input"] = input as AnyObject?
                
                requiredFieldsAreSet = true
                //------------------------------------------------
                
            }else{
                logger.error("input is nil")
            }
        
            if requiredFieldsAreSet{
                //---------------------------------------------------------------------
                //OPTIONAL PARAMETERS
                //---------------------------------------------------------------------
                if let offset = offset{
                    parameters["offset"] = offset as AnyObject?
                }else{
                    //logger.debug("offset is nil - OK - optional")
                }
                //---------------------------------------------------------------------
                if let location = location{
                    parameters["location"] = location as AnyObject?
                }else{
                    //logger.debug("location is nil - OK - optional")
                }
                //---------------------------------------------------------------------
                if let radius = radius{
                    parameters["radius"] = radius as AnyObject?
                }else{
                    //logger.debug("radius is nil - OK - optional")
                }
                //---------------------------------------------------------------------
                if let language = language{
                    parameters["language"] = language as AnyObject?
                }else{
                    //logger.debug("language is nil - OK - optional")
                }
                
            }else{
                logger.error("requiredFieldsAreSet is false - skip OPTIONAL FIELDS")
            }
            //------------------------------------------------
        }else{
            //required param missing in super class
            requiredFieldsAreSet = false
        }
        
        return requiredFieldsAreSet
    }
}
