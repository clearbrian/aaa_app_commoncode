//
//  CLKPlacesPlaceDetailsRequest.swift
//  joyride
//
//  Created by Brian Clear on 03/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation
//import Alamofire



//https://maps.googleapis.com/maps/api/place/nearbysearch/output?parameters
//OK
//https://maps.googleapis.com/maps/api/place/details/json?placeid=ChIJZeLN3lMDdkgR5Zx5syC5NgI&key=AIzaSyCh_0tfBGDU7l_RHXuq5zol9amft7e3szg

class CLKPlacesPlaceDetailsRequest: CLKPlacesParentRequest {
    
    //https://maps.googleapis.com/maps/api/place/details/output?parameters
    
    //---------------------------------------------------------------------
    //Required parameters
    //---------------------------------------------------------------------

    //in parent
    //var key:String?

    //---------------------------------------------------------------------
//    key (required) — Your application's API key. This key identifies your application for purposes of quota management and so that places added from your application are made immediately available to your app. Visit the Google Developers Console to create an API Project and obtain your key.
    //---------------------------------------------------------------------
    //(you must supply one of these, but not both):
    var placeid:String?
    var reference:String?
    
    
//    Either placeid or reference (you must supply one of these, but not both):
    
//    placeid — A textual identifier that uniquely identifies a place, returned from a Place Search. For more information about place IDs, see the place ID overview.
//    reference — A textual identifier that uniquely identifies a place, returned from a Place Search. Note: The reference is now deprecated in favor of placeid. See the deprecation notice on this page.
    
    //---------------------------------------------------------------------
//    Optional Parameters
//    
//    extensions (optional) — Indicates if the Place Details response should include additional fields. Additional fields may include premium data, requiring an additional license, or values that are not commonly requested. Extensions are currently experimental. Supported values for the extensions parameter are:
//    review_summary includes a rich and concise review curated by Google's editorial staff.
//    
//    language (optional) — The language code, indicating in which language the results should be returned, if possible. Note that some fields may not be available in the requested language. See the list of supported languages and their codes. Note that we often update supported languages so this list may not be exhaustive.

    
    
    //Optional parameters
    var extensions:String? //experimental ignore
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    // LANGUAGE - moved up
    //https://developers.google.com/maps/faq#languagesupport
    //var language:String? //declared in CLKGoogleDirectionsParentRequest/CLKPlacesParentRequest
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //"https://maps.googleapis.com/maps/api/place/details/json?placeid=\(placeId)&key=\(appDelegate.googleMapsApiKey_Web)"
    override init(){
        super.init()
        query = "/details/json"
        
    }
    
    override func parametersAppendAndCheckRequiredFieldsAreSet() -> Bool{
        var requiredFieldsAreSet = false
        
        let requiredFieldsAreSetInSuper = super.parametersAppendAndCheckRequiredFieldsAreSet()
        if requiredFieldsAreSetInSuper{
            //------------------------------------------------
            if let placeid = placeid{
                if let _ = reference{
                    logger.error("reference AND placeid set can only use 1")
                }else{
                    //"0x4876034996f07b5b"
                    //ONLY placeid set - OK - cant be BOTH placeid AND reference
                    parameters["placeid"] = placeid as AnyObject?
                    requiredFieldsAreSet = true
                }
            }else{
                //placeid is nil
                if let reference = reference{
                    
                    //placeid is nil
                    //reference is set - use it
                    
                    parameters["reference"] = reference as AnyObject?
                    requiredFieldsAreSet = true
                }else{
                    
                    //placeid is nil
                    //reference is nil
                    
                    logger.error("reference AND placeid are nil - at least one required")
                }
            }
            
            
            if requiredFieldsAreSet{
                //---------------------------------------------------------------------
                //OPTIONAL PARAMETERS
                //---------------------------------------------------------------------
                if let extensions = extensions{
                    //params = params + "&extensions=\(extensions)"
                    parameters["extensions"] = extensions as AnyObject?
                }else{
                    //logger.debug("extensions is nil - OK - optional")
                }
                //---------------------------------------------------------------------
                // TODO: - handle diff languages
                if let language = language{
                    //params = params + "&language=\(language)"
                    parameters["language"] = language as AnyObject?
                }else{
                    //logger.debug("language is nil- OK - optional")
                }
                //---------------------------------------------------------------------
                
            }else{
                logger.error("requiredFieldsAreSet is false - skip OPTIONAL FIELDS")
            }
            //------------------------------------------------
        }else{
            //required param missing in super class
            requiredFieldsAreSet = false
        }
        
        return requiredFieldsAreSet
    }
    
}
