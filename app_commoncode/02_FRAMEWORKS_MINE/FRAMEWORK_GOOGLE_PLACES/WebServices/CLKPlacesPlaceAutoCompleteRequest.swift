//
//  CLKPlacesAutoCompleteRequest.swift
//  joyride
//
//  Created by Brian Clear on 31/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation


//https://developers.google.com/places/webservice/autocomplete
//https://maps.googleapis.com/maps/api/place/autocomplete/output?parameters


//where output may be either of the following values:
//
//json (recommended) indicates output in JavaScript Object Notation (JSON)
//xml indicates output as XML
//Certain parameters are required to initiate a Place Autocomplete request. As is standard in URLs, all parameters are separated using the ampersand (&) character. The list of parameters and their possible values are enumerated below.
//
//Required parameters
//
//input — The text string on which to search. The Place Autocomplete service will return candidate matches based on this string and order results based on their perceived relevance.

//key — Your application's API key. This key identifies your application for purposes of quota management. Visit the Google Developers Console to select an API project and obtain your key. Google Maps API for Work customers must use the API project created for them as part of their Google Places API for Work purchase.
//---------------------------------------------------------------------
//    Optional parameters
//---------------------------------------------------------------------
//offset — The position, in the input term, of the last character that the service uses to match predictions. For example, if the input is 'Google' and the offset is 3, the service will match on 'Goo'. The string determined by the offset is matched against the first word in the input term only. For example, if the input term is 'Google abc' and the offset is 3, the service will attempt to match against 'Goo abc'. If no offset is supplied, the service will use the whole term. The offset should generally be set to the position of the text caret.

//location — The point around which you wish to retrieve place information. Must be specified as latitude,longitude.

//radius — The distance (in meters) within which to return place results. Note that setting a radius biases results to the indicated area, but may not fully restrict results to the specified area. See Location Biasing below.

//language — The language in which to return results. See the supported list of domain languages. Note that we often update supported languages so this list may not be exhaustive. If language is not supplied, the Place Autocomplete service will attempt to use the native language of the domain from which the request is sent.

//types — The types of place results to return. See Place Types below. If no type is specified, all types will be returned.

//components — A grouping of places to which you would like to restrict your results. Currently, you can use components to filter by country. The country must be passed as a two character, ISO 3166-1 Alpha-2 compatible country code. For example: components=country:fr would restrict your results to places within France.



class CLKPlacesAutoCompleteRequest: CLKPlacesParentRequest {
    //------------------------------------------------------------------------------------------------
    //Required parameters
    //------------------------------------------------------------------------------------------------
    //in parent
    //var key:String?
    var input:String?
    
    
    
    
    //------------------------------------------------------------------------------------------------
    ////Optional parameters
    //------------------------------------------------------------------------------------------------
    var offset:String?
    //---------------------------------------------------------------------
    //"-33.8670522,151.1957362"
    var location:String?
    //---------------------------------------------------------------------
    var radius:Double? = GooglePlacesController.defaultSearchRadius
    //---------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    // LANGUAGE - moved up
    //https://developers.google.com/maps/faq#languagesupport
    //var language:String? //declared in CLKGoogleDirectionsParentRequest/CLKPlacesParentRequest
    //-----------------------------------------------------------------------------------
    //---------------------------------------------------------------------
    var types:String?
    //---------------------------------------------------------------------
    var components:String?
    //------------------------------------------------------------------------------------------------



    //https://maps.googleapis.com/maps/api/place/autocomplete/output?parameters
    
    override init(){
        super.init()
        //https://maps.googleapis.com/maps/api/place/autocomplete/json?parameters
        
        query = "/autocomplete/json"        /* must start with /  */
    
    }
    
    override func parametersAppendAndCheckRequiredFieldsAreSet() -> Bool{
        var requiredFieldsAreSet = false
        
        let requiredFieldsAreSetInSuper = super.parametersAppendAndCheckRequiredFieldsAreSet()
        if requiredFieldsAreSetInSuper{
            
            //------------------------------------------------
            if let input = input{
                //------------------------------------------------
                // TODO: - DOESNT HANDLED blanks
                parameters["input"] = input as AnyObject?
                
                requiredFieldsAreSet = true
                //------------------------------------------------
                
            }else{
                logger.error("input is nil")
            }
            
            
            if requiredFieldsAreSet{
                //---------------------------------------------------------------------
                //OPTIONAL PARAMETERS
                //---------------------------------------------------------------------
                if let offset = offset{
                    parameters["offset"] = offset as AnyObject?
                }else{
                    //logger.debug("offset is nil - OK - optional")
                }
                //---------------------------------------------------------------------
                if let location = location{
                    parameters["location"] = location as AnyObject?
                }else{
                    //logger.debug("location is nil - OK - optional")
                }
                //---------------------------------------------------------------------
                if let radius = radius{
                    parameters["radius"] = radius as AnyObject?
                }else{
                    //logger.debug("radius is nil - OK - optional")
                }
                //---------------------------------------------------------------------
                if let language = language{
                    parameters["language"] = language as AnyObject?
                }else{
                    //logger.debug("language is nil - OK - optional")
                }
                //---------------------------------------------------------------------
                if let types = types{
                    parameters["types"] = types as AnyObject?
                }else{
                    //logger.debug("types is nil - OK - optional")
                }
                //---------------------------------------------------------------------
                if let components = components{
                    parameters["components"] = components as AnyObject?
                }else{
                    //logger.debug("components is nil - OK - optional")
                }
                
                
            }else{
                logger.error("requiredFieldsAreSet is false - skip OPTIONAL FIELDS")
            }
            //------------------------------------------------
        }else{
            //required param missing in super class
            requiredFieldsAreSet = false
        }
        
        return requiredFieldsAreSet
    }
}
