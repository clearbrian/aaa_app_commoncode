//
//  CLKPlacesWSQuery.swift
//  joyride
//
//  Created by Brian Clear on 03/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation
//import Alamofire

//404 - let kWEBSERVICE_ENDPOINT_GooglePlaces:String = "https://maxxxps.googleapis.com/maps/api/place"
//404 - let kWEBSERVICE_ENDPOINT_GooglePlaces:String = "https://maps.googleapis.com/maps/api/placeXXX"

//OK
let kWEBSERVICE_ENDPOINT_GooglePlaces:String = "https://maps.googleapis.com/maps/api/place"

//e.g. 
//https://maps.googleapis.com/maps/api/place/details/output?parameters
class CLKPlacesParentRequest : ParentWSRequest{
    
    //should be set by subclass
    
    override init(){
        super.init()

    }
    //CLKPlacesParentRequest/CLKGoogleDirectionsParentRequest
    var language: String? {
        var language_: String?
        
        if let googleSupportedLanguageCodeForDeviceLanguage = GoogleApi.find_googleSupportedLanguageCodeForDeviceLanguage(){
            
            language_ = googleSupportedLanguageCodeForDeviceLanguage
            
        }else{
            logger.error("googleSupportedLanguageCodeForDeviceLanguage is nil - should be set in appD")
        }
        
        return language_
    }
    override var urlString :String? {
        get {
            var urlString_ :String?
            
            if let query = query{

                urlString_ = "\(kWEBSERVICE_ENDPOINT_GooglePlaces)\(query)"
                
                var _ = parametersAppendAndCheckRequiredFieldsAreSet()
                //print("requiredFieldsAreSet:\(requiredFieldsAreSet)")
                
            }else{
                logger.error("query is nil - should be set by subclass")
            }
            
            return urlString_
        }
    }
    
    override func parametersAppendAndCheckRequiredFieldsAreSet() -> Bool{
        var requiredFieldsAreSet = false
        
        requiredFieldsAreSet = super.parametersAppendAndCheckRequiredFieldsAreSet()
        if requiredFieldsAreSet{
            
            //DEBUG - comment out "key" to trigger "REQUEST_DENIED"
            //status = "REQUEST_DENIED";
            
            //ADD THE GOOGLE API KEYS - stored in Delegate
            //Two key WEB and IOS - I use WEB
            //parameters["key"] = GoogleApiKeys.googleMapsApiKey_Web as AnyObject?
            parameters["key"] = GoogleApiKeys.googleMapsApiKey_iOS as AnyObject?
            //Optional check in subclass parameters["language"] = self.language
            
            requiredFieldsAreSet = true
        }else{
            //required param missing in super class
        }
        
        return requiredFieldsAreSet
    }
}
