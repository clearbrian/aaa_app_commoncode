//
//  CLKPlacesPickerResultPlace.swift
//  joyride
//
//  Created by Brian Clear on 24/10/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
//--------------------------------------------------------------
// MARK: - level 2. CLKPlacesPickerResultPlace: CLKPlacesPickerResult
// MARK: -
//--------------------------------------------------------------
class CLKPlacesPickerResultPlace: CLKPlacesPickerResult{
    
    //    // TODO: - make private?
    var clkPlaceSelected: CLKPlace?{
        didSet {
            print("Old value is \(String(describing: oldValue)), new value is \(String(describing: clkPlaceSelected))")
        }
    }
    
    override var clkPlaceSelectedReturned: CLKPlace?{
        return self.clkPlaceSelected
    }
    
    override var hasOtherAddress: Bool{
        
        if let clkPlaceSelected = self.clkPlaceSelected {
            return clkPlaceSelected.isGeocoded
            
        }else{
            return false
            
        }
    }
    
    
    override var name :String?{
        var name_: String?
        
        if let clkPlaceSelected = self.clkPlaceSelected {
            //name_ = clkPlaceSelected.name
            //.name should always return CLKPLace.name
            //if theres special rules like in CLKCOntactPlace.formatted_name then change in formatted_name
            name_ = clkPlaceSelected.formatted_name
            
            
            
        }else{
            logger.error("self.clkPlaceSelected is nil - cant set name_formatted_address")
        }
        return name_
    }
    
    override var formatted_address_main :String?{
        var formatted_address_main_ :String = ""
        
        if let clkPlaceSelected = self.clkPlaceSelected {
            
            if let formatted_address = clkPlaceSelected.formatted_address {
                formatted_address_main_ = formatted_address
                
            }else{
                logger.error("clkPlaceSelected.formatted_address is nil")
            }
        }else{
            logger.error("self.clkPlaceSelected is nil - cant set name_formatted_address")
        }
        
        return formatted_address_main_
    }
    
    // TODO: - needed? ever called?
    override var formatted_address_geocoded :String?{
        var formatted_address_geocoded_ :String = "formatted_address_geocoded 87878"
        
        if let clkPlaceSelected = self.clkPlaceSelected {
            if let formatted_address_geocoded = clkPlaceSelected.formatted_address_geocoded {
                formatted_address_geocoded_ = formatted_address_geocoded
                
            }else{
                logger.error("sclkPlaceSelected.formatted_address is nil")
            }
        }else{
            logger.error("self.clkPlaceSelected is nil - cant set name_formatted_address")
        }
        
        return formatted_address_geocoded_
    }
    
    
    
    
    
    //--------------------------------------------------------------
    // EXTERNAL
    //--------------------------------------------------------------
    override var mainStringNAME : String{
        if let name = self.name {
            return name
        }else{
            logger.error("self.name is nil - mainStringNAME >> ''")
            return " "
        }
    }
    
    override var mainStringADDRESS : String{
        if let formatted_address_main = self.formatted_address_main {
            return formatted_address_main
        }else{
            logger.error("formatted_address_main is nil - mainStringNAME >> ''")
            return " "
        }
    }
    // TODO: - needed? anywhere where the name diff when geocoded?
    override var geoCodedStringNAME : String{
        return self.mainStringNAME
    }
    
    override var geoCodedStringADDRESS : String{
        
        if let formatted_address_geocoded = self.formatted_address_geocoded {
            return formatted_address_geocoded
        }else{
            logger.error("formatted_address_geocoded is nil")
            return " "
        }
    }
}
