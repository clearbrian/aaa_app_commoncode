//
//  CLKPlacesPickerResultNearbyGooglePlace.swift
//  joyride
//
//  Created by Brian Clear on 24/10/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation

//--------------------------------------------------------------
// MARK: - level 3. CLKPlacesPickerResultNearbyBusiness : CLKPlacesPickerResultPlace: CLKPlacesPickerResult
// MARK: -
//--------------------------------------------------------------

//class CLKPlacesPickerResultNearbyGooglePlace : CLKPlacesPickerResultPlace{
class CLKPlacesPickerResultNearbyGooglePlace : CLKPlacesPickerResult{
    
    //map address
    var clkGooglePlaceSelected: CLKGooglePlace?
    
    var clkLocationPlaceMapCenter: CLKLocationPlace?
    
    
    override var clkPlaceSelectedReturned: CLKPlace?{
        return self.clkGooglePlaceSelected
    }
    
    override var hasOtherAddress: Bool{
        //Place may not have good address e.g. bus stop even after get_detail so need option to choose nearest street address
        if let _ = self.clkLocationPlaceMapCenter {
            return true
            
        }else{
            return false
            
        }
    }
    
    
    override var name :String?{
        var name_: String?
        
        if let clkGooglePlaceSelected = self.clkGooglePlaceSelected {
            name_ = clkGooglePlaceSelected.name
            
        }else{
            logger.error("self.clkPlaceSelected is nil - cant set name_formatted_address")
        }
        return name_
    }
    
    // TODO: - OK
    override var formatted_address_main :String?{
        var formatted_address_main_ :String = "formatted_address_main 5555"
        
        if let clkGooglePlaceSelected = self.clkGooglePlaceSelected {
            
            if let formatted_address = clkGooglePlaceSelected.formatted_address {
                formatted_address_main_ = formatted_address
                
            }else{
                logger.error("clkPlaceSelected.formatted_address is nil")
            }
        }else{
            logger.error("self.clkPlaceSelected is nil - cant set name_formatted_address")
        }
        
        return formatted_address_main_
    }
    
    
    override var formatted_address_geocoded :String?{
        var formatted_address_geocoded_ :String = ""
        
        if let clkLocationPlaceMapCenter = self.clkLocationPlaceMapCenter {
            if clkLocationPlaceMapCenter.isGeocoded {
                
                if let formatted_address = clkLocationPlaceMapCenter.formatted_address {
                    formatted_address_geocoded_ = formatted_address
                }else{
                    logger.error("clkLocationPlaceMapCenter.formatted_address is nil")
                }
            }else{
                formatted_address_geocoded_ = "clkLocationPlaceMapCenter NOT GEOCODED YET"
                logger.error("useGeocodedAddress is YES but NOT GEOCODED YET isGeocoded is false")
            }
        }else{
            logger.error("useGeocodedAddress is YES but NOT GEOCODED YET isGeocoded is false")
        }
        return formatted_address_geocoded_
    }
    
    
    
    
    
    //    override var name_formatted_address :String{
    //        var name_formatted_address_ :String = ""
    //
    ////        if let clkPlaceSelected = self.clkPlaceSelected {
    ////            if useGeocodedAddress{
    ////
    ////                if let clkGooglePlaceSelected = self.clkGooglePlaceSelected {
    ////                    let name = clkPlaceSelected.name
    ////                    let formatted_address = clkGooglePlaceSelected.formatted_address
    ////
    ////                    name_formatted_address_ = "\(name), \(formatted_address)"
    ////
    ////                }else{
    ////                    name_formatted_address_ = "ERROR useGeocodedAddress true but clkPlaceSelectedGeocodeLocation nil"
    ////                    logger.error("[ERROR useGeocodedAddress true but clkPlaceSelectedGeocodeLocation nil")
    ////                }
    ////
    ////            }else{
    ////                //dont geocode use name/address from clkPlaceSelected
    ////                name_formatted_address_  = clkPlaceSelected.name_formatted_address
    ////
    ////            }
    ////        }else{
    ////            logger.error("self.clkPlaceSelected is nil - cant set name_formatted_address")
    ////        }
    //        if let clkGooglePlaceSelected = self.clkGooglePlaceSelected {
    ////            if useGeocodedAddress{
    //                // TODO: - do i need a geocoded_formatted address?
    //                if let clkGoogleGeocoderResult = clkGooglePlaceSelected.clkGoogleGeocoderResult {
    //
    //                    let name = clkGooglePlaceSelected.name
    //                    let formatted_address = clkGoogleGeocoderResult.addressString
    //
    //                    name_formatted_address_ = "\(name), \(formatted_address)"
    //
    //                }else{
    //                    name_formatted_address_ = "ERROR useGeocodedAddress true but clkPlaceSelectedGeocodeLocation nil"
    //                    logger.error("[ERROR useGeocodedAddress true but clkPlaceSelectedGeocodeLocation nil")
    //                }
    //
    ////            }else{
    ////                //dont geocode use name/address from clkPlaceSelected
    ////                name_formatted_address_  = clkGooglePlaceSelected.name_formatted_address
    ////
    ////            }
    //
    //        }else{
    //            logger.error("self.clkPlaceSelected is nil - cant set name_formatted_address")
    //        }
    //        return name_formatted_address_
    //    }
    
    
    //--------------------------------------------------------------
    // EXTERNAL
    //--------------------------------------------------------------
    override var mainStringNAME : String{
        if let name = self.name {
            return name
        }else{
            logger.error("self.name is nil - mainStringNAME >> ''")
            return " "
        }
    }
    
    override var mainStringADDRESS : String{
        if let formatted_address_main = self.formatted_address_main {
            return formatted_address_main
        }else{
            logger.error("self.formatted_address_main is nil - mainStringADDRESS >> ''")
            return " "
        }
    }
    
    // TODO: - needed? anywhere where the name diff when geocoded?
    override var geoCodedStringNAME : String{
        return self.mainStringNAME
    }
    
    override var geoCodedStringADDRESS : String{
        var formatted_address_ :String = ""
        
        if let clkLocationPlaceMapCenter = self.clkLocationPlaceMapCenter {
            
            if clkLocationPlaceMapCenter.isGeocoded {
                
                if let formatted_address = clkLocationPlaceMapCenter.formatted_address {
                    formatted_address_ = formatted_address
                }else{
                    logger.error("clkLocationPlaceMapCenter.formatted_address is nil")
                }
            }else{
                formatted_address_ = "NOT GEOCODED YET 8876"
                logger.error("useGeocodedAddress is YES but NOT GEOCODED YET isGeocoded is false")
            }
            
        }else{
            logger.error("self.clkLocationPlaceMapCenter is nil - cant set name_formatted_address")
        }
        return formatted_address_
    }
    
}
