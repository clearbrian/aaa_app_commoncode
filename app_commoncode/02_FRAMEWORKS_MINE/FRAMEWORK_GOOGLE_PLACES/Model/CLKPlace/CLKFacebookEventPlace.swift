//
//  CLKFacebookEventPlace.swift
//  joyride
//
//  Created by Brian Clear on 14/10/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import CoreLocation

class CLKFacebookEventPlace : CLKPlace{
    
//    fileprivate var colcFacebookEvent :COLCFacebookEvent?
//    
//    
//    init(colcFacebookEvent: COLCFacebookEvent) {
//        //REMOVE PArentSwiftObject from CLKPlace
//        //super.init()
//        // TODO: - needed? we get clLocation from colcFacebookEvent in colcFacebookEvent.get below
//        if let clLocation = colcFacebookEvent.clLocation {
//            if let name = colcFacebookEvent.name as? String{
//                //clLocation is set
//                //name is set
//                super.init(clLocation:clLocation, name: name, clkPlaceWrapperType: .colcFacebookEvent)
//                
//            }else{
//                //clLocation is set
//                //name is nil
//                super.init(clLocation:clLocation, name: nil, clkPlaceWrapperType: .colcFacebookEvent)
//                
//            }
//        }else{
//            //clLocation is nil
//            //name is nil
//            super.init(clLocation:nil, name: nil, clkPlaceWrapperType: .colcFacebookEvent)
//
//        }
//        //------------------------------------------------------------------------------------------------
//        //store subclass data
//        self.colcFacebookEvent = colcFacebookEvent
//        //------------------------------------------------------------------------------------------------
//
//    }
//    
//    //--------------------------------------------------------------
//    // MARK: - name
//    // MARK: -
//    //--------------------------------------------------------------
//
//    fileprivate var _name: String?
//
//    override var name :String? {
//        get {
//            //---------------------------------------------------------
//            var nameReturned :String?
//            
//            //changed by SET - eg Edit Name
//            if let _name = self._name {
//                nameReturned = _name
//            }else{
//                //------------------------------------------------------------------------------------------------
//                //GET FROM INTERNAL TYPE
//                //------------------------------------------------------------------------------------------------
//                if let colcFacebookEvent = self.colcFacebookEvent{
//                    //---------------------------------------------------------------------
//                    //Wrong the place name is not the event name
//                    //nameReturned = colcFacebookEvent.name as String?
//                    //---------------------------------------------------------------------
//                    if let place = colcFacebookEvent.place {
//                        nameReturned = place.name as String?
//                    }else{
//                        logger.error("colcFacebookEvent.place is nil")
//                    }
//                }else{
//                    logger.error("self.place is nil")
//                }
//            }
//            //------------------------------------------------------------------------------------------------
//            return nameReturned
//            //------------------------------------------------------------------------------------------------
//        }//get
//    }
//    
//    
//    fileprivate var _formatted_address: String?
//    override var formatted_address :String? {
//        get {
//            //---------------------------------------------------------
//            var formatted_addressReturned :String?
//            
//            //------------------------------------------------------------------------------------------------
//            //GET FROM INTERNAL TYPE
//            //------------------------------------------------------------------------------------------------
//            if let colcFacebookEvent = self.colcFacebookEvent{
//                formatted_addressReturned = colcFacebookEvent.formattedAddress
//            }else{
//                logger.error("[COLCFacebookEvent] self.colcFacebookEvent is nil - cant return formatted_addressReturned")
//            }
//            //------------------------------------------------------------------------------------------------
//            return formatted_addressReturned
//            //------------------------------------------------------------------------------------------------
//        }
//        set {
//            // TODO: - RealDB load from DB - sets address = REMOVE?
//            //------------------------------------------------------------------------------------------------
//            //PASS INTO INTERNAL TYPE
//            //------------------------------------------------------------------------------------------------
//            _formatted_address = newValue
//            //------------------------------------------------------------------------------------------------
//        }
//    }
//    
//    override var clLocation :CLLocation? {
//        get {
//            //---------------------------------------------------------
//            var clLocationReturned :CLLocation?
//            
//            //---------------------------------------------------------------------
//            if let colcFacebookEvent = self.colcFacebookEvent {
//                if let clLocation = colcFacebookEvent.clLocation {
//                    clLocationReturned = clLocation
//                    
//                }else{
//                    //clLocation is nil
//                    logger.error("self.colcFacebookEvent.clLocation is nil - need to FIRWARD GEOCODE THE ADDRESS STRING")
//                }
//            }else{
//                logger.error("self.colcFacebookEvent is nil")
//            }
//            //------------------------------------------------------------------------------------------------
//            return clLocationReturned
//            //------------------------------------------------------------------------------------------------
//        }
//        
//    }
}
