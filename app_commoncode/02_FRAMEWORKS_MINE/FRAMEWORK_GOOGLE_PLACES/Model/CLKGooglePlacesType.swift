//
//  CLKPlaceType.swift
//  joyride
//
//  Created by Brian Clear on 24/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit
public enum GooglePlacesType : String {
    
    //GENERAL TYPE - commented out mean GENERAL TYPE REPEATED IN SPECIFIC TYPES
    case Accommodation = "Accommodation"
    case Business = "Business"
    case Entertainment = "Entertainment"
//    case Food = "Food"
    case Food_Drink = "Food & Drink"
//    case Health = "Health"
    case Location = "Location"
    case Money = "Money"
//    case Museum = "Museum"
    case Post = "Post"
    case Public_Building = "Public Building"
    case Point_Of_Interest = "PointOf Interest"
    case Religious = "Religious"
    case Shopping = "Shopping"
    case Transport = "Transport"
    
    //SPECIFIC TYPES
    case Accounting = "Accountancy"
    case Administrative_Area_Level_1 = "Administrative Area Level 1"
    case Administrative_Area_Level_2 = "Administrative Area Level 2"
    case Administrative_Area_Level_3 = "Administrative Area Level 3"
    case Administrative_Area_Level_4 = "Administrative Area Level 4"
    case Administrative_Area_Level_5 = "Administrative Area Level 5"
    case Airport = "Airport"
    case Amusement_Park = "Amusement Park"
    case Aquarium = "Aquarium"
    case Art_Gallery = "Art Gallery"
    case Atm = "Atm"
    case Bakery = "Bakery"
    case Bank = "Bank"
    case Bar = "Bar"
    case Beauty_Salon = "Beauty Salon"
    case Bicycle_Store = "Bicycle Store"
    case Book_Store = "Book Store"
    case Bowling_Alley = "Bowling Alley"
    case Bus_Station = "Bus Station"
    case Cafe = "Cafe"
    case Campground = "Campground"
    case Car_Dealer = "Car Dealer"
    case Car_Rental = "Car Rental"
    case Car_Repair = "Car Repair"
    case Car_Wash = "Car Wash"
    case Casino = "Casino"
    case Cemetery = "Cemetery"
    case Church = "Church"
    case City_Hall = "City Hall"
    case Clothing_Store = "Clothing Store"
    case Colloquial_Area = "Colloquial Area"
    case Convenience_Store = "Convenience Store"
    case Country = "Country"
    case Courthouse = "Courthouse"
    case Dentist = "Dentist"
    case Department_Store = "Department Store"
    case Doctor = "Doctor"
    case Electrician = "Electrician"
    case Electronics_Store = "Electronics Store"
    case Embassy = "Embassy"
    case Establishment = "Establishment"
    case Finance = "Finance"
    case Fire_Station = "Fire Station"
    case Floor = "Floor"
    case Florist = "Florist"
    case Food = "Food"
    case Funeral_Home = "Funeral Home"
    case Furniture_Store = "Furniture Store"
    case Gas_Station = "Gas Station"
    case General_Contractor = "General Contractor"
    case Geocode = "Geocode"
    case Grocery_Or_Supermarket = "Grocery Or Supermarket"
    case Gym = "Gym"
    case Hair_Care = "Hair Care"
    case Hardware_Store = "Hardware Store"
    case Health = "Health"
    case Hindu_Temple = "Hindu Temple"
    case Home_Goods_Store = "Home Goods Store"
    case Hospital = "Hospital"
    case Insurance_Agency = "Insurance Agency"
    case Intersection = "Intersection"
    case Jewelry_Store = "Jewelry Store"
    case Laundry = "Laundry"
    case Lawyer = "Lawyer"
    case Library = "Library"
    case Liquor_Store = "Liquor Store"
    case Local_Government_office = "Local Government office"
    case Locality = "Locality"
    case Locksmith = "Locksmith"
    case Lodging = "Lodging"
    case Meal_Delivery = "Meal Delivery"
    case Meal_takeaway = "Meal takeaway"
    case Mosque = "Mosque"
    case Movie_Rental = "Movie Rental"
    case Movie_theater = "Movie theater"
    case Moving_Company = "Moving Company"
    case Museum = "Museum"
    case Natural_Feature = "Natural Feature"
    case Neighborhood = "Neighborhood"
    case Night_Club = "Night Club"
    case Painter = "Painter"
    case Park = "Park"
    case Parking = "Parking"
    case Pet_Store = "Pet Store"
    case Pharmacy = "Pharmacy"
    case Physiotherapist = "Physiotherapist"
    case Place_Of_Worship = "Place Of Worship"
    case Plumber = "Plumber"
    case Point_Of_interest = "Point Of interest"
    case Police = "Police"
    case Political = "Political"
    case Post_Box = "Post Box"
    case Post_Office = "Post Office"
    case Postal_Code = "Postal Code"
    case Postal_Code_Prefix = "Postal Code Prefix"
    case Postal_Code_Suffix = "Postal Code Suffix"
    case Postal_town = "Postal town"
    case Premise = "Premise"
    case Real_Estate_Agency = "Real Estate Agency"
    case Restaurant = "Restaurant"
    case Roofing_Contractor = "Roofing Contractor"
    case Room = "Room"
    case Route = "Route"
    case Rv_Park = "Rv Park"
    case School = "School"
    case Shoe_Store = "Shoe Store"
    case Shopping_Mall = "Shopping Mall"
    case Spa = "Spa"
    case Stadium = "Stadium"
    case Storage = "Storage"
    case Store = "Store"
    case Street_Address = "Street Address"
    case Street_Number = "Street Number"
    case Sublocality = "Sublocality"
    case Sublocality_Level_1 = "Sublocality Level 1"
    case Sublocality_Level_2 = "Sublocality Level 2"
    case Sublocality_Level_3 = "Sublocality Level 3"
    case Sublocality_Level_4 = "Sublocality Level 4"
    case Sublocality_Level_5 = "Sublocality Level 5"
    case SubPremise = "SubPremise"
    case Subway_Station = "Subway Station"
    case Synagogue = "Synagogue"
    case Taxi_Stand = "Taxi Stand"
    case Train_Station = "Train Station"
    case Transit_Station = "Transit Station"
    case Travel_Agency = "Travel Agency"
    case University = "University"
    case Veterinary_Care = "Veterinary Care"
    case Zoo = "Zoo"

    
    case Business_Or_POI = "Business Or Point of Interest"

    
    
    func colorForGooglePlacesType() -> UIColor{
        var returnedColor = UIColor.appColorCYAN()
        switch self {
        case .Accommodation:
            returnedColor = UIColor.appColorFlat_Alizarin_Red()
            
        case .Business:
            returnedColor = UIColor.appColorFlat_Amethyst()
            
        case .Entertainment:
            returnedColor = UIColor.appColorFlat_Carrot()
            
        case .Food:
            returnedColor = UIColor.appColorFlat_Concrete()
            
        case .Food_Drink:
            returnedColor = UIColor.appColorFlat_Emerald()
            
        case .Health:
            returnedColor = UIColor.appColorFlat_Orange()
            
        case .Location:
            returnedColor = UIColor.appColorFlat_Peter_River()
            
        case .Money:
            returnedColor = UIColor.appColorFlat_Pomegranate()
            
        case .Museum:
            returnedColor = UIColor.appColorFlat_Purple_SEANCE()
            
        case .Post:
            returnedColor = UIColor.appColorFlat_WetAsphalt()
            
        case .Public_Building:
            returnedColor = UIColor.appColorFlat_Medium_Purple()
            
        case .Point_Of_Interest:
            returnedColor = UIColor.appColorFlat_Picton_Blue()
            
        case .Religious:
            returnedColor = UIColor.appColorFlat_Gossip()
            
        case .Shopping:
            returnedColor = UIColor.appColorFlat_Buttercup()
            
        case .Transport:
            returnedColor = UIColor.appColorFlat_SunsetOrange()
            
        case .Accounting:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Administrative_Area_Level_1:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Administrative_Area_Level_2:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Administrative_Area_Level_3:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Administrative_Area_Level_4:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Administrative_Area_Level_5:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Airport:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Amusement_Park:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Aquarium:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Art_Gallery:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Atm:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Bakery:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Bank:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Bar:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Beauty_Salon:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Bicycle_Store:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Book_Store:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Bowling_Alley:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Bus_Station:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Cafe:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Campground:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Car_Dealer:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Car_Rental:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Car_Repair:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Car_Wash:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Casino:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Cemetery:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Church:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .City_Hall:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Clothing_Store:
            returnedColor = UIColor.appColorFlat_Emerald()
            
        case .Colloquial_Area:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Convenience_Store:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Country:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Courthouse:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Dentist:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Department_Store:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Doctor:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Electrician:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Electronics_Store:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Embassy:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Establishment:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Finance:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Fire_Station:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Floor:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Florist:
            returnedColor = UIColor.appColorFlat_Emerald()
//        case Food:
//            returnedColor = UIColor.appColorFlat_Emerald()
        case .Funeral_Home:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Furniture_Store:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Gas_Station:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .General_Contractor:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Geocode:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Grocery_Or_Supermarket:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Gym:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Hair_Care:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Hardware_Store:
            returnedColor = UIColor.appColorFlat_Emerald()
//        case Health:
//            returnedColor = UIColor.appColorFlat_Emerald()
        case .Hindu_Temple:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Home_Goods_Store:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Hospital:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Insurance_Agency:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Intersection:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Jewelry_Store:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Laundry:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Lawyer:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Library:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Liquor_Store:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Local_Government_office:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Locality:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Locksmith:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Lodging:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Meal_Delivery:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Meal_takeaway:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Mosque:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Movie_Rental:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Movie_theater:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Moving_Company:
            returnedColor = UIColor.appColorFlat_Emerald()
//        case Museum:
//            returnedColor = UIColor.appColorFlat_Emerald()
        case .Natural_Feature:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Neighborhood:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Night_Club:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Painter:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Park:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Parking:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Pet_Store:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Pharmacy:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Physiotherapist:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Place_Of_Worship:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Plumber:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Point_Of_interest:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Police:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Political:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Post_Box:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Post_Office:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Postal_Code:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Postal_Code_Prefix:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Postal_Code_Suffix:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Postal_town:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Premise:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Real_Estate_Agency:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Restaurant:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Roofing_Contractor:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Room:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Route:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Rv_Park:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .School:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Shoe_Store:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Shopping_Mall:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Spa:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Stadium:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Storage:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Store:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Street_Address:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Street_Number:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Sublocality:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Sublocality_Level_1:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Sublocality_Level_2:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Sublocality_Level_3:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Sublocality_Level_4:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Sublocality_Level_5:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .SubPremise:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Subway_Station:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Synagogue:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Taxi_Stand:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Train_Station:
            returnedColor = UIColor.appColorFlat_Emerald()
        case .Transit_Station:
            returnedColor = UIColor.appColorFlat_Emerald()											
        case .Travel_Agency:
            returnedColor = UIColor.appColorFlat_Emerald()											
        case .University:
            returnedColor = UIColor.appColorFlat_Emerald()												
        case .Veterinary_Care:
            returnedColor = UIColor.appColorFlat_Emerald()											
        case .Zoo:
            returnedColor = UIColor.appColorFlat_Emerald()
            
        case .Business_Or_POI:
            returnedColor = UIColor.appColorFlat_YellowOrange_California()
            
            
            
//        default:
//            returnedColor = UIColor.appColorCYAN()
        }
        
        return returnedColor
    }
    func locationTypeNameForGooglePlacesType() -> String{
        var returnedValue = "Business"
        

        
        
//        switch self {
//        case Accommodation:
//            returnedValue = "Accommodation"
//        case Business:
//            returnedValue = "Business"
//        case Entertainment:
//            returnedValue = "Entertainment"
//        case Food:
//            returnedValue = "Food"
//        case Food_Drink:
//            returnedValue = "Food & Drink"
//        case Health:
//            returnedValue = "Health"
//        case Location:
//            returnedValue = "Location"
//        case Money:
//            returnedValue = "Money"
//        case Museum:
//            returnedValue = "Museum"
//        case Post:
//            returnedValue = "Post"
//        case Public_Building:
//            returnedValue = "Public Building"
//        case Point_Of_Interest:
//            returnedValue = "Point Of Interest"
//        case Religious:
//            returnedValue = "Religious"
//        case Shopping:
//            returnedValue = "Shopping"
//        case Transport:
//            returnedValue = "Transport"
//            //        default:
//            //            returnedValue = UIColor.appColorCYAN()
//        }
        returnedValue = self.rawValue
        return returnedValue
    }
}

class CLKGooglePlacesType : ParentSwiftObject{
    
    var name : String = ""
    var googlePlacesType: GooglePlacesType = .Location
    var googlePlacesGeneralType: GooglePlacesType = .Location
    
   
    //"restaurant", "bar", "establishment"
    //establishment is too general but ok to show others
    var isOKToShowTypeName = false
    
    override init(){
        super.init()

    }
    init(name: String, googlePlacesType: GooglePlacesType, googlePlacesGeneralType: GooglePlacesType, isOKToShowTypeName: Bool ){
        super.init()
        self.name = name
        self.googlePlacesType = googlePlacesType
        self.googlePlacesGeneralType = googlePlacesGeneralType
        self.isOKToShowTypeName = isOKToShowTypeName
    }
}
