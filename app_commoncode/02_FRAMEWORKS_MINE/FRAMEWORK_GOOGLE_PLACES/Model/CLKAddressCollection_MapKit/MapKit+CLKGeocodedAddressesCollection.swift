//
//  MapKit+CLKGeocodedAddressesCollection.swift
//  joyride
//
//  Created by Brian Clear on 09/11/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import MapKit

extension MKMapView{
    
    func showbestOrFirstFromCollectionOnMap(clkGeocodedAddressesCollection: CLKGeocodedAddressesCollection){
        
        //---------------------------------------------------------------------
        //NOTE: clPlacemark has weird inits and MKPlacemark can only be created from
        //Note: tried putting CLPlacemark in CLKAddress but was only really used in CLAddressApple
        //and its only here where we do a FORWARD Apple geocode then REVERSE GOOGLE GEOCODE
        //---------------------------------------------------------------------
        //to test google address Contacts > address with just post code 
//TODO - ADDRESSES
        
//        Contact pick osle/postcode showing only geocoded address cos formatted_address will choose bestaddress is its there
//        BUT dont fix this do the MODALADDRESGEOCODEVC
//        this could help get rid of the
//        TAP ON COANCT > "Finding address on map " >> finding better address > not foudn > would you like to edit
//        
//        
//        MON forward* reverse geocode is slow
//        
//        User taps on Contact
//        Belfast
//         BT34 17g
//         MODAL 
//         Title:FINDING Address on map
//         forward ("BT34 17g")
//         "Searching Google Maps for address ;;"
//         show spinner - test on 
//         
//         FORWARD GOEOCDE FAILED
//            Egypt - NOT found > would you like to EDIT the address here > edit in text field
//            or in Contacts - open contact
//         
//         Forward ok lat/lng foudn but reverse Geocode recommnded
//         
//         "address forund on map"
//         show half address
//
//         in background reversegeocode the lat/lng
//         
//         Found nearest street address
//         
//         this could help in losing the USE STREET button the MODALADDRES GEOCODE could let them pick ONE ADDRESS
//         NAV CONTROLLER in PLACEPICKER 
//         ======
//         also need option to move pin and find better address - eagle in fb event returns v weird address
//        =====
//        does NEARBY search place by name usable?
//        
//        
//         
//         
        
 
        if let clkAddress = clkGeocodedAddressesCollection.bestOrFirstCLKAddress {
            showAddressOnMap(clkAddress)
        }else{
            logger.error("bestOrFirstCLKAddress is nil - cant add pin to map")
        }
    }
    
    func showAddressOnMap(_ clkAddress: CLKAddress){
        
        //MKAnnotation is a protocol so any class that implements it best is COLCMKAnnotation
        if let mkAnnotation = clkAddress.mkAnnotation {
            if let regionToZoomTo = clkAddress.mkCoordinateRegionToZoomMapTo() {
                
                self.showMKAnnotationOnMap(mkAnnotation: mkAnnotation, regionToZoomTo:regionToZoomTo)
            }else{
                logger.error("clkAddress.mkCoordinateRegionToZoomMapTo() is nil")
            }
        }else{
            logger.error("clkAddress.mkAnnotation is nil")
        }
    }

    
    //MKAnnotation is a protocol implemented by the following classes
    //MKUserLocation
    //MKPlacemark - title comes from internal CLPlacemary.addressDictionary - use COLCMKAnnotation if you can
    //COLCMKAnnotation - easier to set title
    func showMKAnnotationOnMap(mkAnnotation: MKAnnotation, regionToZoomTo: MKCoordinateRegion){

        DispatchQueue.main.async{
            //clear map
            self.removeAnnotations(self.annotations)

            //DROP PIN
            self.addAnnotation(mkAnnotation)
            
            //ZOOM TO PIN
            self.setRegion(regionToZoomTo, animated: true)
            
        }//main
    }
}
