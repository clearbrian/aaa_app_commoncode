//
//  CLKPlacesPickerResult.swift
//  joyride
//
//  Created by Brian Clear on 20/10/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit 
//USE CASES
/*
 Nearby > Street Address > Name: none         / Address: lat/lng > street address
 Nearby > Business       > Name: businessname / Address: businessaddress + get_detail
 
 */

//--------------------------------------------------------------
// MARK: - level 1. CLKPlacesPickerResult
// MARK: -
//--------------------------------------------------------------
class CLKPlacesPickerResult{

    
    //used to show hide Use Street Switch
    var hasOtherAddress: Bool{
        logger.error("SHOULD SUBCLASS hasOtherAddress")
        return false
    }
    
    
    var clkPlaceSelectedReturned: CLKPlace?{
        return nil
    }

    
    //---------------------------------------------------------------------
    var useGeocodedAddress:Bool = false
    //---------------------------------------------------------------------
    
    //--------------------------------------------------------------
    // MARK: - GET FROM INTERNAL CLASSES
    // MARK: -
    //--------------------------------------------------------------
// TODO: - make private
    var name :String?{
        return "ERROR SUBCLASS name 366"
    }
    
    var formatted_address_main :String?{
        return "ERROR SUBCLASS formatted_address_main 337"
    }
    
    var formatted_address_geocoded :String?{
        return "ERROR SUBCLASS formatted_address_geocoded 3467"
    }
    
    func attributedString_name_formatted_address(useGeocoded:Bool) -> NSAttributedString? {
        var attributedString: NSAttributedString?
        
        //switch is on LocationVC but not TripVC
        self.useGeocodedAddress = useGeocoded
        
        
        if useGeocoded{
            
            if self.mainStringNAME == " " || self.mainStringNAME == ""{
                //tap on Business, then Nearest, then business there was bug where business got stuck
                //I was copying the name from MAIN to GEOCODED but this was wrong cos when you clicked BAck to only nearest the business name was stuck
                //I commented out the copy so here I need to use mainStringNAME
                //Tap on business, then nearest then business, press use street should be ok
                if self.mainStringNAME == " "{
                    let arrayStringsAndColors: [(String, UIColor)] = [(self.geoCodedStringADDRESS, UIColor.purple)]
                    //---------------------------------------------------------------------
                    attributedString = AttributedStringCreator.attributedStringFromArray(arrayStringsAndColors, seperator: "")
                    //---------------------------------------------------------------------
                }else{
                    // user main name if its set
                    let arrayStringsAndColors: [(String, UIColor)]  = [(self.mainStringNAME, UIColor.black),
                                                                       (self.geoCodedStringADDRESS, UIColor.purple)]
                    //---------------------------------------------------------------------
                    attributedString = AttributedStringCreator.attributedStringFromArray(arrayStringsAndColors, seperator: "")
                    //---------------------------------------------------------------------
                }
                
            }else{
                //mainStringNAME is not blank
                let arrayStringsAndColors: [(String, UIColor)] = [  (self.mainStringNAME, UIColor.black),
                                                                    (self.geoCodedStringADDRESS, UIColor.purple)]
                //---------------------------------------------------------------------
                attributedString = AttributedStringCreator.attributedStringFromArray(arrayStringsAndColors, seperator: ",")
                //---------------------------------------------------------------------
            }
        }else{
            //UNGEOCODED
            if self.mainStringNAME == " " || self.mainStringNAME == ""{
                let arrayStringsAndColors: [(String, UIColor)] = [ (self.mainStringADDRESS, UIColor.black)]
                //---------------------------------------------------------------------
                attributedString = AttributedStringCreator.attributedStringFromArray(arrayStringsAndColors, seperator: "")
                //---------------------------------------------------------------------
            }else{
                if self.mainStringADDRESS == " " || self.mainStringADDRESS == ""{
                    let arrayStringsAndColors: [(String, UIColor)] = [ (self.mainStringNAME, UIColor.black) ]
                    //---------------------------------------------------------------------
                    attributedString = AttributedStringCreator.attributedStringFromArray(arrayStringsAndColors, seperator: "")
                    //---------------------------------------------------------------------
                }else{
                    let arrayStringsAndColors: [(String, UIColor)] = [ (self.mainStringNAME   , UIColor.black),
                                                                       (self.mainStringADDRESS, UIColor.black)]
                    //---------------------------------------------------------------------
                    attributedString = AttributedStringCreator.attributedStringFromArray(arrayStringsAndColors, seperator: ",")
                    //---------------------------------------------------------------------
                }
            }
        }
        return attributedString
    }
    
    var name_formatted_address: String {
        var name_formatted_address: String = ""
        
        //Set in LocationVC swith useStreet
        if self.useGeocodedAddress{
            
            if self.mainStringNAME == " " || self.mainStringNAME == ""{
                //tap on Business, then Nearest, then business there was bug where business got stuck
                //I was copying the name from MAIN to GEOCODED but this was wrong cos when you clicked BAck to only nearest the business name was stuck
                //I commented out the copy so here I need to use mainStringNAME
                //Tap on business, then nearest then business, press use street should be ok
                if self.mainStringNAME == " "{
                    name_formatted_address = self.geoCodedStringADDRESS
                    
                }else{
                    name_formatted_address = "\(self.mainStringNAME), \(self.geoCodedStringADDRESS)"
                }
                
            }else{
                name_formatted_address = "\(self.mainStringNAME), \(self.geoCodedStringADDRESS)"
                
            }
        }else{
            if self.mainStringNAME == " " || self.mainStringNAME == ""{
                name_formatted_address = self.mainStringADDRESS
                
            }else{
                if self.mainStringADDRESS == " " || self.mainStringADDRESS == ""{
                    name_formatted_address = self.mainStringNAME
                    
                }else{
                    name_formatted_address = "\(self.mainStringNAME), \(self.mainStringADDRESS)"
                    
                }
            }
        }
        return name_formatted_address
    }
    
    //--------------------------------------------------------------
    // MARK: - EXTERNAL
    // MARK: -
    //--------------------------------------------------------------

    var mainStringNAME : String{
        return "mainStringNAME SUBCLASS"
    }
    var mainStringADDRESS : String{
        return "mainStringNAME SUBCLASS"
    }
    var geoCodedStringNAME : String{
        return "mainStringNAME SUBCLASS"
    }
    
    var geoCodedStringADDRESS : String{
        return "mainStringNAME SUBCLASS"
    }

}

