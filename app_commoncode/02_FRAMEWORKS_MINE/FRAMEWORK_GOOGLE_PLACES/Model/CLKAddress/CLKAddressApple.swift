//
//  CLKAddressApple.swift
//  joyride
//
//  Created by Brian Clear on 26/10/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//
import Foundation
import CoreLocation
import MapKit

class CLKAddressApple: CLKAddress{
    
    var clPlacemark: CLPlacemark?

    init(clPlacemark: CLPlacemark) {
        super.init()
        
        self.clPlacemark = clPlacemark
    }
    
    override var name: String? {
        var name_: String? = nil
        if let clPlacemark = self.clPlacemark {
            
            if let name = clPlacemark.name {
                name_ = name
                
            }else{
                logger.error("clPlacemark.name is nil")
            }
            
        }else{
            logger.error("self.clPlacemark is nil - cant get name")
            
        }
        return name_
    }

    // Street number and name. 
    override var street: String? {
        return self.thoroughfare
    }
    
    //CLPlacemark only
    var thoroughfare: String? {
        var thoroughfare_: String? = nil
        if let clPlacemark = self.clPlacemark {
            
            if let thoroughfare = clPlacemark.thoroughfare {
                thoroughfare_ = thoroughfare
                
            }else{
                logger.error("clPlacemark.thoroughfare is nil")
            }
            
        }else{
            logger.error("self.clPlacemark is nil - cant get thoroughfare")
            
        }
        return thoroughfare_
    }
    
    //CLPlacemark only
    var subThoroughfare: String? {
        var subThoroughfare_: String? = nil
        if let clPlacemark = self.clPlacemark {
            
            if let subThoroughfare = clPlacemark.subThoroughfare {
                subThoroughfare_ = subThoroughfare
                
            }else{
                logger.error("clPlacemark.subThoroughfare is nil")
            }
        }else{
            logger.error("self.clPlacemark is nil - cant get subThoroughfare")
        }
        return subThoroughfare_
    }
    
    
    
    // Locality or city. 
    //locality: String? { get }
    override var city: String  {
        //beware is optional - can crash SourceKitService
        if let locality = self.locality {
            return locality
        }else{
            logger.error("self.locality is nil")
            return ""
        }
    }
    
    //CLPlacemark
    
    ///beware - super.locality is an String? can cause SourceKitService to crash if you over ride it with non optional
    override var locality: String?  {
        var locality_: String?
        if let clPlacemark = self.clPlacemark {
            
            if let locality = clPlacemark.locality {
                locality_ = locality
            }else{
                logger.error("clPlacemark.locality is nil")
            }
        }else{
            logger.error("self.clPlacemark is nil - cant get locality_")
        }
        return locality_
    }
    
    
    // Subdivision of locality, district or park. 
    //subLocality: String? { get }
    
    override var subLocality: String  {
        var subLocality_: String = ""
        if let clPlacemark = self.clPlacemark {
            
            if let subLocality = clPlacemark.subLocality {
                subLocality_ = subLocality
            }else{
                logger.error("clPlacemark.subLocality is nil")
            }
            
        }else{
            logger.error("self.clPlacemark is nil - cant get city")
        }
        return subLocality_
    }
    
    //--------------------------------------------------------------
    // MARK: - state/administrativeArea
    // MARK: -
    //--------------------------------------------------------------
    
    //CLPlacemark
    //administrativeArea: String? { get } // state, eg. CA
    //England - coutry is United Kingdom
    override var state: String  {
        return self.administrativeArea
    }
    
    override var administrativeArea: String  {
        var administrativeArea_: String = ""
        if let clPlacemark = self.clPlacemark {
            
            if let administrativeArea = clPlacemark.administrativeArea {
                administrativeArea_ = administrativeArea
            }else{
                logger.error("clPlacemark.administrativeArea is nil")
            }
        }else{
            logger.error("self.clPlacemark is nil - cant get city")
            
        }
        return administrativeArea_
    }
    
    //CLPlacemark only
    var subAdministrativeArea: String  {
        var subAdministrativeArea_: String = ""
        if let clPlacemark = self.clPlacemark {
            
            if let subAdministrativeArea = clPlacemark.subAdministrativeArea {
                subAdministrativeArea_ = subAdministrativeArea
            }else{
                logger.error("clPlacemark.subAdministrativeArea is nil")
            }
        }else{
            logger.error("self.clPlacemark is nil - cant get city")
            
        }
        return subAdministrativeArea_
    }
    //--------------------------------------------------------------
    // MARK: - postalCode
    // MARK: -
    //--------------------------------------------------------------
    
    // Postal/Zip code. 
    //postalCode: String? { get } // zip code, eg. 95014
    override var postalCode: String  {
        var postalCode_: String = ""
        if let clPlacemark = self.clPlacemark {
            
            if let postalCode = clPlacemark.postalCode {
                postalCode_ = postalCode
            }else{
                logger.error("clPlacemark.postalCode is nil")
            }
        }else{
            logger.error("self.clPlacemark is nil - cant get postalCode")
        }
        return postalCode_
    }
    
    
    //--------------------------------------------------------------
    // MARK: - country
    // MARK: -
    //--------------------------------------------------------------
    
    // country: String? { get } // eg. United States
    override var country: String  {
        var country_: String = ""
        if let clPlacemark = self.clPlacemark {
            
            if let country = clPlacemark.country {
                country_ = country
            }else{
                logger.error("clPlacemark.country is nil")
            }
        }else{
            logger.error("self.clPlacemark is nil - cant get country")
        }
        return country_
    }
    
    //   isoCountryCode: String? { get } // eg. US
    override var isoCountryCode: String  {
        if let clPlacemark = self.clPlacemark {
            if let isoCountryCode = clPlacemark.isoCountryCode {
                return isoCountryCode
            }else{
                logger.error("clPlacemark.isoCountryCode is nil")
            }
            
        }else{
            logger.error("self.clPlacemark is nil - cant get isoCountryCode")
        }
        return ""
    }
    
    //--------------------------------------------------------------
    // MARK: - CLPlacemark only
    // MARK: -
    //--------------------------------------------------------------
    //inlandWater: String? { get } // eg. Lake Tahoe
    var inlandWater: String  {
        
        if let clPlacemark = self.clPlacemark {
            if let inlandWater = clPlacemark.inlandWater {
                return inlandWater
            }else{
                logger.error("clPlacemark.inlandWater is nil")
                return ""
            }
        }else{
            logger.error("self.clPlacemark is nil - cant get isoCountryCode")
            return ""
        }
    }
    
    // ocean: String? { get } // eg. Pacific Ocean
    var ocean: String  {
        
        if let clPlacemark = self.clPlacemark {
            if let ocean = clPlacemark.ocean {
                return ocean
            }else{
                logger.error("clPlacemark.ocean is nil")
                return ""
            }
        }else{
            logger.error("self.clPlacemark is nil - cant get ocean")
            return ""
        }
    }
    
    
    //--------------------------------------------------------------
    // MARK: - MISC
    // MARK: -
    //--------------------------------------------------------------
    
    // areasOfInterest: [String]? { get } // eg. Golden Gate Park
    var areasOfInterest: [String]?  {
        
        if let clPlacemark = self.clPlacemark {
            if let areasOfInterest = clPlacemark.areasOfInterest {
                return areasOfInterest
            }else{
                logger.error("clPlacemark.areasOfInterest is nil")
                return nil
            }
            
        }else{
            logger.error("self.clPlacemark is nil - cant get areasOfInterest")
            return nil
        }
    }
    
    var timeZone: TimeZone? {
        
        if let clPlacemark = self.clPlacemark {
            if let timeZone = clPlacemark.timeZone {
                return timeZone
            }else{
                logger.error("clPlacemark. timeZone is nil")
                return nil
            }
        }else{
            logger.error("self.clPlacemark is nil - cant get timeZone")
            return nil
        }
    }
    //region: CLRegion? { get }
    var region: CLRegion? {
        
        if let clPlacemark = self.clPlacemark {
            if let region = clPlacemark.region {
                return region
            }else{
                logger.error("clPlacemark. region is nil")
                return nil
            }
            
        }else{
            logger.error("self.clPlacemark is nil - cant get region")
            return nil
        }
    }
    

    
    
    
    
    
    //--------------------------------------------------------------
    // MARK: - addressDictionary
    // MARK: -
    //--------------------------------------------------------------
    //addressDictionary: [AnyHashable : Any]? { get }
    var addressDictionary: [AnyHashable : Any]? {
        
        if let clPlacemark = self.clPlacemark {
            if let addressDictionary = clPlacemark.addressDictionary {
                return addressDictionary
            }else{
                logger.error("clPlacemark.addressDictionary is nil")
                return nil
            }
            
        }else{
            logger.error("self.clPlacemark is nil - cant get addressDictionary")
            return nil
        }
    }
    //--------------------------------------------------------------
    // MARK: - OVERRIDED clLocation
    // MARK: -
    //--------------------------------------------------------------
    
    override var clLocation: CLLocation?{
        
        if let clPlacemark = self.clPlacemark {
            if let location = clPlacemark.location {
                return location
            }else{
                logger.error("clPlacemark. location is nil")
                return nil
            }
            
        }else{
            logger.error("self.clPlacemark is nil - cant get region")
            return nil
        }
    }
    

    
    
    override var address_lines: [String]? {
        
        ////built in subclass as some filed shared so can get dups if we do this in CLKAddress
        ////        open var name: String? { get } // eg. Apple Inc.
        ////        open var thoroughfare: String? { get } // street name, eg. Infinite Loop
        ////        open var subThoroughfare: String? { get } // eg. 1
        ////        open var locality: String? { get } // city, eg. Cupertino
        ////        open var subLocality: String? { get } // neighborhood, common name, eg. Mission District
        ////        open var administrativeArea: String? { get } // state, eg. CA
        ////        open var subAdministrativeArea: String? { get } // county, eg. Santa Clara
        ////        open var postalCode: String? { get } // zip code, eg. 95014
        ////        open var isoCountryCode: String? { get } // eg. US
        ////        open var country: String? { get } // eg. United States
        ////        open var inlandWater: String? { get } // eg. Lake Tahoe
        ////        open var ocean: String? { get } // eg. Pacific Ocean
        //        
        //        
        //        
        //        //---------------------------------------------------------------------
        //        //TEST CASES:
        //        //---------------------------------------------------------------------
        //        //test Ewings belfast only showing postcode
        //        //should this be reverse geocoded by lat/lng
        //        //[INFO ]  [CLKAppleGeocoder.swift:44 forwardGeocodeAddressDictionary(_:success:failure:)] arrayCLPlacemark.count :1
        //        //BT1 3AJ, Belfast, BT1 3AJ, Northern Ireland @ <+54.60484100,-5.92249400> +/- 100.00m, region CLCircularRegion (identifier:'<+54.60484099,-5.92249403> radius 125.14', center:<+54.60484099,-5.92249403>, radius:125.14m)
        //        //---------------------------------------------------------------------
        //        //---------------------------------------------------------------------
        //        // TODO: - test all addresses
        //        //https://www.wikiwand.com/en/Address_(geography)
        //        //APPLE MAPS > search for country > capital > tap on any business > Create contact//
        //        //test with phone in japanese
        //        //---------------------------------------------------------------------
        //        var address_lines_ = [String]()
        //        
        //        if let name = self.name {
        //            
        //            if let subThoroughfare = self.subThoroughfare {
        //                
        //                if let thoroughfare = self.thoroughfare {
        //                    
        //                    //BOTH set
        //                    if name == thoroughfare{
        //                        //only add one of them
        //                        //address_lines_ = safeAppend(address_lines_, lineString: self.name)
        //                        address_lines_ = safeAppend(address_lines_, lineString: self.thoroughfare)
        //                    }else if name == "\(subThoroughfare) \(thoroughfare)"{
        //                        //name: 5 Maple Drive
        //                        //subThoroughfare: 5
        //                        //thoroughfare: Maple Drive
        //                        //only add one of name
        //                        address_lines_ = safeAppend(address_lines_, lineString: self.name)
        //                        //address_lines_ = safeAppend(address_lines_, lineString: self.thoroughfare)
        //                    }else{
        //                        //diff - add all
        //                        address_lines_ = safeAppend(address_lines_, lineString: self.name)
        //                        address_lines_ = safeAppend(address_lines_, lineString: self.subThoroughfare)
        //                        address_lines_ = safeAppend(address_lines_, lineString: self.thoroughfare)
        //                    }
        //                }else{
        //                    //name set/thoroughfare blank - add both the safe append will skip if blank
        //                    address_lines_ = safeAppend(address_lines_, lineString: self.name)
        //                    address_lines_ = safeAppend(address_lines_, lineString: self.subThoroughfare)
        //                    address_lines_ = safeAppend(address_lines_, lineString: self.thoroughfare)
        //                }
        //            }else{
        //                //self.subThoroughfare is nil
        //                if let thoroughfare = self.thoroughfare {
        //                    
        //                    //BOTH set
        //                    if name == thoroughfare{
        //                        //only add one of them
        //                        //address_lines_ = safeAppend(address_lines_, lineString: self.name)
        //                        address_lines_ = safeAppend(address_lines_, lineString: self.thoroughfare)
        //                    }
        //                    else{
        //                        //diff - add all
        //                        address_lines_ = safeAppend(address_lines_, lineString: self.name)
        //                        //address_lines_ = safeAppend(address_lines_, lineString: self.subThoroughfare)
        //                        address_lines_ = safeAppend(address_lines_, lineString: self.thoroughfare)
        //                    }
        //                }else{
        //                    
        //                    //name set/thoroughfare blank - add both the safe append will skip if blank
        //                    address_lines_ = safeAppend(address_lines_, lineString: self.name)
        //                    address_lines_ = safeAppend(address_lines_, lineString: self.thoroughfare)
        //                }
        //            }
        //            
        //            
        //        }else{
        //            //name is nil
        //            address_lines_ = safeAppend(address_lines_, lineString: self.subThoroughfare)
        //            address_lines_ = safeAppend(address_lines_, lineString: self.thoroughfare)
        //        }
        //        //---------------------------------------------------------------------
        //        //subLocality
        //        //---------------------------------------------------------------------
        //        //gettting St katherines Dock for Contacts > Brian Clear > 15 Royal Mint st
        //        logger.error("DONT ADD self.subLocality:\(self.subLocality)")
        //        //address_lines_ = safeAppend(address_lines_, lineString: self.subLocality)
        //        //---------------------------------------------------------------------
        //        
        //        //London
        //        address_lines_ = safeAppend(address_lines_, lineString: self.locality)
        //        
        //        //London
        //        address_lines_ = safeAppend(address_lines_, lineString: self.subAdministrativeArea)
        //        //should be after London
        //        address_lines_ = safeAppend(address_lines_, lineString: self.postalCode)
        //        
        //        //England
        //        address_lines_ = safeAppend(address_lines_, lineString: self.administrativeArea)
        //
        //        //address_lines_=  safeAppend(address_lines_, self.country)
        //        
        ////THIS is ok but apple fields different
        ////        addressDictionary_?[CNPostalAddressStreetKey] = cnPostalAddress.street
        ////        addressDictionary_?[CNPostalAddressCityKey] = cnPostalAddress.city
        ////        addressDictionary_?[CNPostalAddressStateKey] = cnPostalAddress.state
        ////        addressDictionary_?[CNPostalAddressPostalCodeKey] = cnPostalAddress.postalCode
        ////        addressDictionary_?[CNPostalAddressCountryKey] = cnPostalAddress.country
        //    
        //    // TODO: - TEST https://personal.help.royalmail.com/app/answers/detail/a_id/115
        //        
        //
         //---------------------------------------------------------------------
        //FormattedAddressLines
        //---------------------------------------------------------------------
        //safer way to get the address as a string - getting street/locality etc can produce
        //CLPlacemark > addressDictionary["FormattedAddressLines"]
        //---------------------------------------------------------------------
        
        print("addressDictionary:\(String(describing: self.addressDictionary))")
        var address_lines_: [String]?
        
        if let clPlacemark = self.clPlacemark {
            address_lines_ = clPlacemark.formattedAddressLines
            
        }else{
            logger.error("self.clPlacemark is nil")
        }
        
        return address_lines_
        
    }
    
    
    //--------------------------------------------------------------
    // MARK: - description
    // MARK: -
    //--------------------------------------------------------------
    override var description : String {
        let description_ = ""
        //description_ = description_ + "[========== CLKAddressApple =================================]\r"
        //description_ = description_ + "[CL                  name]:\(String(describing: self.name))\r"
        //description_ = description_ + "[                  street]:\(String(describing: self.street))\r"
        //description_ = description_ + "[CL          thoroughfare]:\(String(describing: self.thoroughfare))\r"
        //description_ = description_ + "[CL       subThoroughfare]:\(String(describing: self.subThoroughfare))\r"
        //description_ = description_ + "[                    city]:\(self.city)\r"
        //description_ = description_ + "[CL              locality]:\(String(describing: self.locality))\r"
        //description_ = description_ + "[             subLocality]:\(self.subLocality)\r"
        //description_ = description_ + "[                   state]:\(self.state)\r"
        //description_ = description_ + "[      administrativeArea]:\(self.administrativeArea)\r"
        //description_ = description_ + "[CL subAdministrativeArea]:\(self.subAdministrativeArea)\r"
        
        //description_ = description_ + "[              postalCode]:\(self.postalCode)\r"
        
        //description_ = description_ + "[                 country]:\(self.country)\r"
        //description_ = description_ + "[          isoCountryCode]:\(self.isoCountryCode)\r"
        //description_ = description_ + "[CL           inlandWater]:\(self.inlandWater)\r"
        //description_ = description_ + "[CL                 ocean]:\(self.ocean)\r"
        
        //description_ = description_ + "[CL              timeZone]:\(String(describing: self.timeZone))\r"
        //description_ = description_ + "[CL                region]:\(String(describing: self.region))\r"
        //description_ = description_ + "[              clLocation]:\(String(describing: self.clLocation))\r"
        //description_ = description_ + "[CL     areasOfInterest[]]:\(String(describing: self.areasOfInterest))\r"
        //description_ = description_ + "[           address_lines]:\r\(String(describing: self.address_lines))\r"
        //description_ = description_ + "[CL     addressDictionary]:\r\(String(describing: self.addressDictionary))\r"
        //description_ = description_ + "[========== CLKAddressApple =================================]\r"
        
        return description_
    }
    
    
    
    //--------------------------------------------------------------
    // MARK: - MapKit
    // MARK: -
    //--------------------------------------------------------------
    override var mkAnnotation: MKAnnotation?{
        var mkAnnotation_: MKAnnotation?
        
        if let clPlacemark = self.clPlacemark {
            
            //---------------------------------------------------------------------
            //v1 OK but mkAnnotation_pinTitle comes from MKPlacemark.addressDict
            //---------------------------------------------------------------------
            //MKPlacemark implements MKAnnotation
            //    let mkPlacemark = MKPlacemark(placemark:clPlacemark)
            //    mkAnnotation_ = mkPlacemark
            
            //---------------------------------------------------------------------
            //v2 wrap COLCMKAnnotation so we can set pin title better
            //---------------------------------------------------------------------
            //COLCMKAnnotation.placemark: MKPlacemark internally has no title so need to use COLCMKAnnotation which wraps it but can handle title
            if let clLocation = self.clLocation {
                let mkPlacemark = MKPlacemark(placemark:clPlacemark)

                mkAnnotation_ = COLCMKAnnotation.init(title: self.mkAnnotation_pinTitle(), coordinate: clLocation.coordinate, placemark: mkPlacemark)
                
            }else{
                logger.error("self.self.clLocation is nil")
            }
            //---------------------------------------------------------------------
        }else{
            logger.error("self.clPlacemar is nil >> mkAnnotation is nil")
        }
        return mkAnnotation_
    }
    
    
    override func mkAnnotation_pinTitle() -> String{
        
        var pinTitle = "Apple Geocoded Address"
        
        if let clLocation = self.clLocation {
            if let name = self.name {
                
                
                
//                if let thoroughfare = self.thoroughfare {
//                    
//                    if let locality = self.locality {
//                        //city
//                        pinTitle =  "\(pinTitle)\r\(name)\r\(thoroughfare), \(locality)\r(\(clLocation.formattedLatLng()))"
//                        
//                    }else{
//                        pinTitle =  "\(pinTitle)\r\(name)\r\(thoroughfare)\r(\(clLocation.formattedLatLng()))"
//                    }
//                    
//                }else{
//                    if let locality = self.locality {
//                        //city
//                        pinTitle =  "\(pinTitle)\r\(name)\r\(locality)\r(\(clLocation.formattedLatLng()))"
//                        
//                    }else{
//                        pinTitle =  "\(pinTitle)\r\(name)\r(\(clLocation.formattedLatLng()))"
//                    }
//                }
                //maple drive thoroughfare and name the same
                if let locality = self.locality {
                    //city
                    pinTitle =  "\(pinTitle)\r\(name)\r\(locality)\r(\(clLocation.formattedLatLng()))"
                    
                }else{
                    pinTitle =  "\(pinTitle)\r\(name)\r\(clLocation.formattedLatLng()))"
                }

            }else{
                //Apple uses thoroughfare
                //Google uses street
                
                if let thoroughfare = self.thoroughfare {
                    
                    if let locality = self.locality {
                        //city
                        pinTitle =  "\(pinTitle)\r\(thoroughfare), \(locality)\r(\(clLocation.formattedLatLng()))"
                        
                    }else{
                         pinTitle =  "\(pinTitle)\r\(thoroughfare)\r(\(clLocation.formattedLatLng()))"
                    }
                    
                }else{
                    if let locality = self.locality {
                        //city
                        pinTitle =  "\(pinTitle)\r\(locality)\r(\(clLocation.formattedLatLng()))"
                        
                    }else{
                        pinTitle =  "\(pinTitle)\r(\(clLocation.formattedLatLng()))"
                    }
                }
            }
        }else{
            logger.error("self.clLocation is nil")
        }
        
        return pinTitle
    }
}
