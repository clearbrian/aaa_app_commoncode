//
//  CLKGeocodedAddressesGoogleResponse.swift
//  joyride
//
//  Created by Brian Clear on 26/10/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation

//--------------------------------------------------------------
// MARK: - ADDRESS COLLECTION
// MARK: -
//--------------------------------------------------------------
//class CLKGeocodedAddressesCollection: ParentSwiftObject{
//    
//}

class CLKGeocodedAddressesGoogleCollection: CLKGeocodedAddressesCollection{
    
    //wrapped for GMSAddress
    var clkAddressGoogleArray = [CLKAddressGoogle]()
    
    override var firstAddress: CLKAddressGoogle?{
        var firstAddress_: CLKAddressGoogle? = nil
        
        //at least 1
        if clkAddressGoogleArray.count > 0 {
            firstAddress_ = clkAddressGoogleArray[0]
            
        }else{
            logger.error("clkAddressGoogleArray.count > 0 FAILED: addresses.count:\(clkAddressGoogleArray.count)")
            
        }
        
        return firstAddress_
    }
    
    
    override var bestOrFirstCLKAddress: CLKAddressGoogle?{
        var bestOrFirstCLKAddress: CLKAddressGoogle? = nil
        
        for clkAddressGoogle : CLKAddressGoogle in clkAddressGoogleArray {
            
            //NOISY logger.debug("address for currentLocation:\(address)")
            
            //find FIRST address which has 'thoroughfare' / street
            //if has a street so most likely to be full address - though not 100% accurate
            //if let _ = gmsAddress.thoroughfare {
            //if let _ = clkAddressGoogle.street {
            if let _ = clkAddressGoogle.thoroughfare {
                
                
                //---------------------------------------------------------------------
                /*
                 //has a street so most likely to be full address - though not 100% accurate
                 GMSAddress {
                 coordinate: (51.511054, -0.041803)
                 lines: St Paul's Ave, London E14 8DL, UK
                 thoroughfare: St Paul's Ave
                 locality: London
                 postalCode: E14 8DL
                 country: United Kingdom
                 }
                 */
                //---------------------------------------------------------------------
                
                bestOrFirstCLKAddress = clkAddressGoogle
                break;
                
            }else{
                logger.error("Has no address.thoroughfare - skip")
            }
            
        }//for
        
        //---------------------------------------------------------------------
        // Check two - if address with thorougfare not find fall back to the first GMSAddress
        //---------------------------------------------------------------------
        if let _ = bestOrFirstCLKAddress {
            //address with throrough found - skip
        }else{
            logger.debug("no GMSAddress with address.thoroughfare - using .firstResult() ")
            
            if clkAddressGoogleArray.count > 0{
                bestOrFirstCLKAddress = clkAddressGoogleArray[0]
            }else{
                logger.error("clkAddressGoogleArray.count > 0 FAILED:\(clkAddressGoogleArray.count)")
            }
        }//check 2 - use firstResult
        //-----------------------------------------------------------------------------------
        return bestOrFirstCLKAddress
    }
}
