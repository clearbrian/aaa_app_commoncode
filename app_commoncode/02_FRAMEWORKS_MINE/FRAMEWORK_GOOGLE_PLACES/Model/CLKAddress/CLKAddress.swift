//
//  CLKAddress.swift
//  joyride
//
//  Created by Brian Clear on 24/10/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
//import Contacts
import MapKit

//USE CASE - Move map > center of map           > CLLocation > reverseGeocode > GMSAddress > Address String > Locality/FirstLine
//USE CASE > Contact Address                    > CLLocation > reverseGeocode > GMSAddress > Address String > Locality/FirstLine
//USE CASE > FB Event Address(Location not set) > CLLocation > reverseGeocode > GMSAddress > Address String > Locality/FirstLine

//USE CASE SE> Geocode with Mapkit > Fails > try Google Geo Code
//USE CASE > Geocode with Mapkit   > Fails > try Google Geo Code

//TODO GMSReverseGeocodeResponse has multiple results see below
// TODO: - RealmDB - store address in db in seperate table?


//--------------------------------------------------------------
// MARK: - Mapkit / Google can return multiple results - this is generic wrapped for them
// MARK: -
//--------------------------------------------------------------
//open class GMSReverseGeocodeResponse : NSObject, NSCopying {
//    
//    
//    /** Returns the first result, or nil if no results were available. */
//    open func firstResult() -> GMSAddress?
//    
//    
//    /** Returns an array of all the results (contains GMSAddress), including the first result. */
//    open func results() -> [GMSAddress]?
//}

// TODO: -  create a contact with Jride First/ JRise second name . street ,,,etc



//--------------------------------------------------------------
// MARK: - SINGLE ADDRESS
// MARK: -
//--------------------------------------------------------------

class CLKAddress: CustomStringConvertible{
    //CLPlacemark has a name
    var name: String? {
        return nil
    }
    var street: String? {
        return nil
    }
    
    //GMSAddress
    var subLocality: String  {
        return ""
    }
    //GMSAddress
    var administrativeArea: String  {
        return ""
    }

    var city: String {
        return ""
    }

    var state: String {
        return ""
    }

    var postalCode: String {
        return ""
    }

    var country: String {
        return ""
    }
    
    //--------------------------------------------------------------
    // MARK: - CLLocation
    // MARK: -
    //--------------------------------------------------------------

    var clLocation: CLLocation?{
        //getter only - should subclas
        return nil
    }
    
    //--------------------------------------------------------------
    // MARK: - OTHER
    // MARK: -
    //--------------------------------------------------------------

    var locality: String?{
        logger.error("CLKAddress.locality not calculated")
        return nil
    }
     var isoCountryCode: String {
        return ""
    }
    
    //--------------------------------------------------------------
    // MARK: - GMSAddress lines
    // MARK: -
    //--------------------------------------------------------------

    var address_lines: [String]? {
        
        //var address_lines_ = [String]()
        
//        //---------------------------------------------------------------------
//        if let street = self.street {
//            address_lines_.append(street)
//        }else{
//            logger.error("self.street is nil")
//        }
//        //---------------------------------------------------------------------
//        address_lines_.append(self.city)
//        address_lines_.append(self.state)
//        address_lines_.append(self.subLocality)
//        address_lines_.append(self.administrativeArea)
//        address_lines_.append(self.postalCode)
//        //dont add country - dont need to see it in address strings
//        //address_lines_.append(self.country)
        
        
        logger.error("SUBCLASS SHOULD CREATE address lines varies per apple/google")
        //return address_lines_
        return nil
    
    }
    
    var addressString: String?{
        var addressString_: String?
        
            if let address_lines = self.address_lines{
                
                addressString_ = UtilArray.appendToAllExceptLast(array: address_lines)
                
            }else{
                logger.error("address.lines is nil")
            }
        return addressString_
    }
    
    var address_lines_first: String? {
        var address_lines_first_ : String? = nil
        
        if let address_lines = self.address_lines {
            if address_lines.count > 0 {
                address_lines_first_ = address_lines[0]
            }else{
                logger.error("address_lines.count > 0 FAILED: count:\(address_lines.count)")
            }
        }else{
            logger.error("self.address_lines is nil")
        }
        
        return address_lines_first_
    }
    
    var description : String {
        let description_ = "CLKGooglePlaceLocation:"
        //description_ = description_ + "            street:\(String(describing: self.street))\r"
        //description_ = description_ + "              city:\(self.city)\r"
        //description_ = description_ + "             state:\(self.state)\r"
        //description_ = description_ + "        postalCode:\(self.postalCode)\r"
        //description_ = description_ + "           country:\(self.country)\r"
        
        // TODO: - we need Locality for uber but is usally city which is in .city not .locality
        //description_ = description_ + "          locality:\(String(describing: self.locality))\r"
        //description_ = description_ + "       subLocality:\(self.subLocality)\r"
        //description_ = description_ + "administrativeArea:\(self.administrativeArea)\r"
        //description_ = description_ + "    isoCountryCode:\(self.isoCountryCode)\r"
        //description_ = description_ + "        clLocation:\(String(describing: self.clLocation))\r"
        //description_ = description_ + "     address_lines:\r\(String(describing: self.address_lines))\r"

        return description_
    }
    
    //--------------------------------------------------------------
    // MARK: - UTIL - used by subclasses
    // MARK: -
    //--------------------------------------------------------------

    //append if non nil and not blank
    func safeAppend(_ address_lines: [String], lineString: String? ) -> [String]{
        
        //internal mutable copy
        var address_lines_mutable = address_lines
        
        if let lineString = lineString {
            if lineString != "" {
                
                //dont add it twice you get London, London
                var isInArrayAlready = false
                for address_linesInArray in address_lines{
                    if address_linesInArray == lineString{
                        isInArrayAlready = true
                        break
                    }
                }
                if isInArrayAlready == false{
                    address_lines_mutable.append(lineString)
                }else{
                    logger.error("lineString is in array already - skip append")
                }
                
                
                
            }else{
                //NOISY logger.error("lineString is '' - skip append")
            }
        }else{
            //NOISY logger.error("lineString is nil - skip append")
        }
        return address_lines_mutable
    }
    
    
    
    var reverseGeocodeRecommended: Bool{
        var reverseGeocodeRecommended_ = false
    
        if let _ = self.street {
            //street is set
            logger.error("street:\(String(describing: self.street)) >> reverseGeocodeRecommended_ stay at false")
        }else{
            //street is blank try google reverser geocode (lat/lng) > GMSAddress
            if let clLocation = self.clLocation {
                if clLocation.isValid{
                    //street is blank but location is set so try and approve the address
                    logger.debug("street:\(String(describing: self.street)) cllocation is valid >> reverseGeocodeRecommended_ >> TRUE")
                    reverseGeocodeRecommended_ = true
                }else{
                    logger.error("street:\(String(describing: self.street)) cllocation is INVALID >> reverseGeocodeRecommended_ >> FALSE")
                }
               
            }else{
                logger.error("self.clLocation is nil")
            }
        }

        return reverseGeocodeRecommended_
        
    }
    
//    var clPlacemark: CLPlacemark?
    
    //--------------------------------------------------------------
    // MARK: - MapKit
    // MARK: -
    //--------------------------------------------------------------
    //tried to add to extension but as of Swift3 externtions cant override
    var mkAnnotation: MKAnnotation?{
        logger.error("CLKAddress.mkAnnotation SUBCLASS")
        return nil
    }
    
    func mkAnnotation_pinTitle() -> String{
        return "Geocoded Address"
    }
    
    //differ for Apple/Google cos no clLocation in one just coordinate
    func mkCoordinateRegionToZoomMapTo() -> MKCoordinateRegion?{
        
        if let clLocation = self.clLocation{
            
            let region = MKCoordinateRegion(center: clLocation.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
            // center map on new pin
            //self.setRegion(region, animated: true)
            return region
            
        }else{
            logger.error("colMKAnnotation.clLocation is nil")
            return nil
        }
    }
    
    
}

