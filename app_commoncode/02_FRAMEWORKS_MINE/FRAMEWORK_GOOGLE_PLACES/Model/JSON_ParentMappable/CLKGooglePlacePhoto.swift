//
//  CLKGooglePlacePhoto.swift
//  joyride
//
//  Created by Brian Clear on 09/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//



class CLKGooglePlacePhoto: ParentMappable {
    
    var height:Int?
    var html_attributions:String?
    var photo_reference:String?
    var width:Int?
    
    
    override func mapping(map: Map){
        height <- map["height"]
        html_attributions <- map["html_attributions"]
        photo_reference <- map["photo_reference"]
        width <- map["width"]
    }
}
/*
photos =         (
    {
        height = 2322;
        "html_attributions" =                 (
            "<a href=\"https://www.google.com/maps/views/profile/107252953636064841537\">William Stewart</a>"
        );
        "photo_reference" = "CmRdAAAAyb84Ki1ycMoSIwwAkW-dRiPyEveA9im3K-Yt0uJyKUlyJnjEDQzZ-j62eGRXvzB4-d_Mlu-6lmKN7p-H11RtSA3GD_QCsnIMstUL1dxejQ_GjXHptxrpgunRri5TldhaEhAtPVLsZg2M9EQvuDOujALoGhQZDaT-p_dPGnHh6pWfRIzZY668PA";
        width = 4128;
    },
    {
        height = 960;
        "html_attributions" =                 (
            "<a href=\"https://www.google.com/maps/views/profile/100919424873665842845\">Donnie Piercey</a>"
        );
        "photo_reference" = "CmRdAAAAKBajG2-WJTnd5UGsiZA2bpbOCLxTZiRI18s7wGCPMvB0eM9nChSR1HUi0EhvWO5YOb97CAo5gtDML-KUvtIfqUniiZzA_oeSs5533Ve02hKTAccWMTX7RmDwhrZwQt22EhALPnRTmNaXpbeJjv1SyrJGGhQ3iFBZz0Gzci8pYgCJ21OaB4IIxQ";
        width = 1280;
},
*/
