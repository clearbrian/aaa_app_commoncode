//
//  CLKGooglePlaceOpeningHoursPeriod.swift
//  joyride
//
//  Created by Brian Clear on 03/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

class CLKGooglePlaceOpeningHoursPeriod: ParentMappable {
    
    var close:CLKGooglePlaceOpeningHoursPeriodTime?
    var open:CLKGooglePlaceOpeningHoursPeriodTime?

    // root calls mapping()
    //    required init?(_ map: Map){
    //        mapping(map: map)
    //    }

    override func mapping(map: Map){
        close <- map["close"]
        open <- map["open"]
    }
}
