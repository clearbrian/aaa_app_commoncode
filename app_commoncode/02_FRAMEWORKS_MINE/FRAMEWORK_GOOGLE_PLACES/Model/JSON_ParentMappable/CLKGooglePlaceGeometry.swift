//
//  CLKGooglePlaceGeometry.swift
//  joyride
//
//  Created by Brian Clear on 03/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//


class CLKGooglePlaceGeometry: ParentMappable {
    
    var location:CLKGooglePlaceLocation?
    
    var viewport:CLKGooglePlaceViewport?
  
    // root calls mapping()
    //    required init?(_ map: Map){
    //        mapping(map: map)
    //    }
    override func mapping(map: Map){
        location <- map["location"]
        viewport <- map["viewport"]
    }
}

/*
    "geometry":{
        "location":{
            "lat":-33.8695456,
            "lng":151.1945404
        },
        "viewport":{
            "northeast":{
                "lat":-33.8623818,
                "lng":151.1989734
            },
            "southwest":{
                "lat":-33.8762122,
                "lng":151.1860651
            }
        }
    },

*/
