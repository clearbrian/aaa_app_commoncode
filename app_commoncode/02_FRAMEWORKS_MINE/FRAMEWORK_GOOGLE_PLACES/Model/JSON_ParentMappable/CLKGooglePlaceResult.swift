//
//  CLKGooglePlaceResult.swift
//  joyride
//
//  Created by Brian Clear on 02/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//
import GoogleMaps

//result:[id, website, address_components, vicinity, formatted_address, international_phone_number, url, adr_address, scope, formatted_phone_number, geometry, icon, place_id, opening_hours, reference, types, name, utc_offset]


class CLKGooglePlaceResult: ParentMappable, CustomStringConvertible {

    var address_components : [CLKGooglePlaceAddressComponent]?
    
    var adr_address :String?
    var formatted_address :String?
    var formatted_phone_number :String?
    
    var geometry :CLKGooglePlaceGeometry?

    
    var icon :String?
    var id :String?
    var international_phone_number :String?
    var name :String?
    
    var opening_hours :CLKGooglePlaceOpeningHours?
    
    var place_id :String?
    var photos : [CLKGooglePlacePhoto]?
    var price_level :Int?
    
    
    var reference :String?
    var rating :String?
    var reviews : [CLKGooglePlaceReview]?
    
    var scope :String?
    
    
    
    //------------------------------------------------------------------------------------------------
    //TYPES - from json to enum
    //------------------------------------------------------------------------------------------------
    
    //one main type is needed for the pin color so use the top one
    //set in clkGooglePlacesTypeArray.didSet
    var clkGooglePlacesTypeMainType : CLKGooglePlacesType?
    
    //set when type didSet called
    var showPlaceInResults = true
    
    //set when type didSet called
    var clkGooglePlacesTypeArray : [CLKGooglePlacesType]?{
        
        //First this
        willSet {
            //print("Old value is \(clkGooglePlacesTypeArray), new value is \(newValue)")
        }
        
        //value is set
        
        //Finaly this
        didSet {
            //print("Old value is \(oldValue), new value is \(clkGooglePlacesTypeArray)")
            if let clkGooglePlacesTypeArray = clkGooglePlacesTypeArray{
                self.clkGooglePlacesTypeMainType = clkGooglePlacesTypeArray[0]
            }else{
                logger.error("clkGooglePlacesTypeArray is nil - in didSet")
                self.clkGooglePlacesTypeMainType = nil
            }
        }
    }
    var clkGooglePlacesTypeArrayString: String {
        get {
            var clkGooglePlacesTypeArrayStringReturned = ""
            
            if let clkGooglePlacesTypeArray = self.clkGooglePlacesTypeArray{
                for clkGooglePlacesType : CLKGooglePlacesType in clkGooglePlacesTypeArray{
                    if clkGooglePlacesTypeArrayStringReturned == ""{
                        // empty string - just add
                        clkGooglePlacesTypeArrayStringReturned = "\(clkGooglePlacesType.googlePlacesType.rawValue)"
                    }else{
                        //append
                        clkGooglePlacesTypeArrayStringReturned = "\(clkGooglePlacesTypeArrayStringReturned), \(clkGooglePlacesType.googlePlacesType.rawValue)"
                    }
                   
                }
            }else{
                logger.error("self.clkGooglePlacesTypeArray is nil")
            }
            
            return clkGooglePlacesTypeArrayStringReturned
        }
        //set {
        //    //self = CGRectMake(newValue, self.minY, self.width, self.height)
        //}
    }
    
    //---------------------------------------------------------------------
    //if type array only contains types we dont want to see then hide it
    //if ANY are true then return true
    func canShowPlaceInResults(_ clkGooglePlacesTypeArrayReturned : [CLKGooglePlacesType]) -> Bool{
        var showPlaceInResults_ = false
        for clkGooglePlacesType in clkGooglePlacesTypeArrayReturned{
            if clkGooglePlacesType.isOKToShowTypeName{
                //at least one type is ok to show
                showPlaceInResults_ = true
                break
                
            }else{
                //dont set to false
            }
        }
        return showPlaceInResults_
    }
    
    // TODO: - CLEANUP after test - change call below to static method
    let googlePlacesController = GooglePlacesController()
    //---------------------------------------------------------------------
    var types : [String]? {
        
        //First this
        willSet {
            //print("Old value is \(types), new value is \(newValue)")
        }
        
        //value is set
        
        //Finally this
        didSet {
            //print("Old value is \(oldValue), new value is \(types)")
            if let types = types{
                if types.count > 0{
                    if let clkGooglePlacesTypeArrayReturned : [CLKGooglePlacesType]
                        = self.googlePlacesController.clkGooglePlacesTypesManager.finalTypesForGooglePlaceTypes(types){
                        self.clkGooglePlacesTypeArray = clkGooglePlacesTypeArrayReturned
                        
                    
                        //["neighborhood", "political"] - hide in results on map and table
                        self.showPlaceInResults = self.canShowPlaceInResults(clkGooglePlacesTypeArrayReturned)
                        
                        
                    }else{
                        logger.error("CLKGooglePlacesType is nil for \(String(describing: self.name)):")
                    }
                }else{
                    logger.error("types.count > 0 failed")
                }
            }else{
                logger.error("self.types is nil")
            }
        }
    }
    
    //------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------

    var url :String?
    var user_ratings_total :Int?
    

    var utc_offset :String?
    var vicinity :String?
    var website :String?


    
    //[html_attributions, result, status]
    var html_attributions: [AnyObject]?
    var result:AnyObject?
    var status:String?

    // root calls mapping()
    //    required init?(_ map: Map){
    //        mapping(map: map)
    //    }

    override func mapping(map: Map){
        address_components <- map["address_components"]
        adr_address <- map["adr_address"]
        formatted_address <- map["formatted_address"]
        formatted_phone_number <- map["formatted_phone_number"]
        geometry <- map["geometry"]
        icon <- map["icon"]
        id <- map["id"]
        international_phone_number <- map["international_phone_number"]
        name <- map["name"]
        opening_hours <- map["opening_hours"]
        place_id <- map["place_id"]
        
        price_level <- map["price_level"]
        photos <- map["photos"]
        rating <- map["rating"]
        reference <- map["reference"]
        reviews <- map["reviews"]
        scope <- map["scope"]
        types <- map["types"]
        url <- map["url"]
        user_ratings_total <- map["user_ratings_total"]
        utc_offset <- map["utc_offset"]
        vicinity <- map["vicinity"]
        website <- map["website"]

    }
    
    
    var clLocation : CLLocation?{
        get {
            //---------------------------------------------------------------------
            var clLocation : CLLocation? = nil
            
            //CLKGooglePlaceGeometry?
            if let geometry = self.geometry{
//                var location:CLKGooglePlaceLocation?
                if let location = geometry.location{
                    //var lat:NSNumber?
                    //var lng:NSNumber?
                    if let lat = location.lat{
                        if let lng = location.lng{
                            //CLLocationDegrees Double
                            clLocation = CLLocation(latitude: lat.doubleValue, longitude: lng.doubleValue)
                        }else{
                            logger.error("location.lng is nil")
                        }
                    }else{
                        logger.error("location.lat is nil")
                    }
                }else{
                    logger.error("location is nil")
                }
            }else{
                logger.error("geometry is nil")
            }
            
            return clLocation
            //---------------------------------------------------------------------
        }
    }
    
    
    func addToMap(_ map: GMSMapView, pinText: String){
        //---------------------------------------------------------------------
        //CLLocationCoordinate2D
        
        if let clLocation_ = self.clLocation{
            //location is ok
            let marker = GMSMarker(position: clLocation_.coordinate)
            //TITLE
            marker.title = self.name
            
            //IMAGE - also used in icon on table so it matches the map
            marker.icon = self.mapPinImageWithText(pinText)
            //the image should be in the center - so when we tap on result in table - it moves the map to the center of the pin image
            marker.groundAnchor = CGPoint(x: 0.5,y: 0.5)
        
            //------------------------------------------------------------------------------------------------
            //store the CLKGooglePlaceResult with the marker
            marker.userData = self
            //------------------------------------------------------------------------------------------------
            marker.map = map
        }else{
            logger.error("clLocation_ is nil for \(String(describing: name))")
        }
        //---------------------------------------------------------------------
    }
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - DISTANCE TO MAP CENTER / ANY LOCATION
    //--------------------------------------------------------------
    //static var NotFound : CLLocationDistance = -1.0
    
    //used in sorting
    var distanceToMapCenter : CLLocationDistance = GoogleMapsDistance.NotFound
    
    func updateDistanceToMapCenter(mapCenterLocation: CLLocation){
  
        self.distanceToMapCenter = self.distanceToLocation(location: mapCenterLocation)
//        if distanceToMapCenter == CLKGooglePlaceResult.NotFound{
//            logger.debug("\(clkGooglePlaceResult) DIST:[NOT FOUND]")
//        }else{
//            logger.debug("\(clkGooglePlaceResult) DIST:[\(distanceToMapCenter) meters]")
//        }
    }
    
    
    //gets the distance from this result to a location - used where location is map center
    func distanceToLocation(location: CLLocation) -> CLLocationDistance{
        var  distanceReturned : CLLocationDistance = GoogleMapsDistance.NotFound
        
        if let geometry = self.geometry{
            if let clkGooglePlaceLocation = geometry.location{
                if let latNumber = clkGooglePlaceLocation.lat{
                    if let lngNumber = clkGooglePlaceLocation.lng{
                        
                        //typedef double CLLocationDegrees;
                        let latitude : Double = latNumber.doubleValue
                        let longitude : Double = lngNumber.doubleValue
                        
                        //FROM = e.g. MAP CENTER
                        let from: CLLocationCoordinate2D =  CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
                        //print("from:\(from)")
                        
                        //TO = Place result location SELF
                        let to: CLLocationCoordinate2D =  CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                        //print("to:\(from)")
                        //GoogleMaps function - returns meters
                        let distanceMetersDouble : CLLocationDistance =  GMSGeometryDistance(from, to)
                        //print("distanceMetersDouble:\(distanceMetersDouble)")
                        distanceReturned = distanceMetersDouble
                    }else{
                        logger.error("clkGooglePlaceLocation.lng is nil")
                    }
                }else{
                    logger.error("clkGooglePlaceLocation.lat is nil")
                }
            }else{
                logger.error("geometry.location is nil")
            }
        }else{
            logger.error("clkGooglePlaceResult.geometry is nil")
        }
        return distanceReturned
    }
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - DESCRIPTION
    //--------------------------------------------------------------
    var description: String {
        
        var descriptionReturned: String = ""
        
        //---------------------------------------------------------------------
        var distanceToMapCenterString = ""
        
        if self.distanceToMapCenter == GoogleMapsDistance.NotFound{
            distanceToMapCenterString = "[NOT FOUND]"
        }else{

            distanceToMapCenterString = "[\(distanceToMapCenter) meters]"
        }
        //---------------------------------------------------------------------
        
        
        if let name = self.name{
            if let types = self.types{
                if let place_id = self.place_id{
                    descriptionReturned = "PLACE1:place_id:[\(place_id)] name:\(name) TYPE:\(types) DIST:\(distanceToMapCenterString)"
                }else{
                    descriptionReturned = "PLACE2:place_id:[nil] name:\(name) TYPE:\(types) DIST:\(distanceToMapCenterString)"
                }
                
            }else{
                logger.error("clkGooglePlaceResult.types is nil")
                descriptionReturned = "PLACE3:\(name) TYPE:nil place_id:[\(String(describing: self.place_id))] DIST:\(distanceToMapCenterString)"
            }
        }else{
            logger.error("clkGooglePlaceResult.name is nil")
        }
    
        return descriptionReturned
    }
    
    
    
    func colorForType() -> UIColor{
        
        var colorForGooglePlacesType_ = UIColor.red
        if let clkGooglePlacesTypeMainType = self.clkGooglePlacesTypeMainType{
            
            //let iconFillColor = clkGooglePlacesType.googlePlacesType.colorForGooglePlacesType()
            colorForGooglePlacesType_ = clkGooglePlacesTypeMainType.googlePlacesGeneralType.colorForGooglePlacesType()
        
        }else{
            logger.error("clkGooglePlaceResult.clkGooglePlacesTypeMainType is nil")
            
        }
        return colorForGooglePlacesType_;
    }
    
    
    func mapPinImageWithText(_ text:String) -> UIImage?{
        
        return mapPinImage(text, fillColor: self.colorForType(), textColor: UIColor.white)
    }
}




//--------------------------------------------------------------
// MARK: -
// MARK: - GLOBAL FUNCTIONS
//--------------------------------------------------------------
//func pinImage() -> UIImage?{
//    var mapIconView_ = MapIconView(frame: CGRectMake(0, 0, 50, 50))
//    mapIconView_.stringToDisplay = "33"
//
//    return mapIconView_.viewAsImage()
//
//}
//Global
func mapPinImage(_ pinText: String, fillColor:UIColor, textColor: UIColor) -> UIImage?{
    var returnedImage : UIImage? = nil;
    
    // Draw the Circle
   //var circleFillColor = fillColor
//    let circleStrokeColor = UIColor.darkGray
    //let circleStrokeColor = AppearanceManager.appColorLight2
    let circleStrokeColor = fillColor
    
    
    
    
//    let circleStrokeColor = UIColor.whiteColor()
    
    //let circleStrokeColor = UIColor.blackColor().colorWithAlphaComponent(0.8)
    //let circleStrokeColor = UIColor.blackColor()
    //let circleStrokeColor = UIColor.appBaseColor()
    
    let iconWidth: CGFloat = 24
    let iconHeight: CGFloat = 24
    
    let lineWidth: CGFloat = 1.0

    
//    let returnedRect_bounds =  CGRectMake(0, 0, iconWidth, iconHeight)
    
    let returnedRect_bounds_circle =  CGRect(
        x: 0 + lineWidth,
        y: 0 + lineWidth,
        width: iconWidth-(lineWidth*2),
        height: iconHeight-(lineWidth*2))
    //------------------------------------------------
    let border: CGFloat = 6.0
    let returnedRect_bounds_text =  CGRect(
        x: 0,  /* center horz done with .Center below */
        y: 0 + border,  /* move down to center vertically */
        width: iconWidth,
        height: iconHeight)

    
    //------------------------------------------------
    //DRAW CIRCLE
    //------------------------------------------------
    UIGraphicsBeginImageContextWithOptions(CGSize(width: iconWidth, height: iconHeight), false, 0.0)
    //SWIFT2.0
    //let ctx : CGContextRef = UIGraphicsGetCurrentContext()
    if let ctx = UIGraphicsGetCurrentContext(){
        //    let centerPoint : CGPoint = CGPointMake(returnedRect_bounds.size.height/2.0,
        //                                            returnedRect_bounds.size.width/2.0)
        ctx.saveGState()
        
        ctx.setLineWidth(lineWidth)
        
        
        ctx.setFillColor(fillColor.cgColor)
        ctx.fillEllipse(in: returnedRect_bounds_circle);
        
        ctx.setStrokeColor(circleStrokeColor.cgColor)
        ctx.strokeEllipse(in: returnedRect_bounds_circle)
        
        //------------------------------------------------
        //Draw text
        //------------------------------------------------
        //let attributes = [NSFontAttributeName: UIFont.systemFontOfSize(15)]
        
        //Center horiz in rect
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        
        let attributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 10),
                          NSAttributedString.Key.foregroundColor: textColor,
                          NSAttributedString.Key.paragraphStyle: paragraphStyle]
        
        let attributedText = NSAttributedString(string: pinText, attributes: attributes)
        attributedText.draw(in: returnedRect_bounds_text)
        //------------------------------------------------
        
        ctx.restoreGState()
        returnedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    }else{
        print("ERROR: ctx is nil")
    }
    

    
    //------------------------------------------------
    return returnedImage
}

////gets the distance from this result to a location - used where location is map center
//func distanceToLocation(location location: CLLocation){
//    
////    let distanceToMapCenter : CLLocationDistance = CLKGooglePlaceResultUtils.distanceToLocation(location: location, clkGooglePlaceResult: clkGooglePlaceResult)
////    if distanceToMapCenter == CLKGooglePlaceResultUtils.NotFound{
////        logger.debug("PLACE1:place_id:[\(place_id)] name:\(name) TYPE:\(types) DIST:[NOT FOUND]")
////    }else{
////        logger.debug("PLACE1:place_id:[\(place_id)] name:\(name) TYPE:\(types) DIST:[\(distanceToMapCenter) meters]")
////    }
////    
//    
//    
//
//}



//OK
//func circleByApplyingAlpha(alpha:CGFloat) -> UIImage?{
//
//    var opaqueCircle : UIImage? = nil;
//
//    var circleFillColor = UIColor.whiteColor()
//    var circleStrokeColor = UIColor.darkGrayColor()
//    
//    UIGraphicsBeginImageContextWithOptions(CGSizeMake(20.0, 20.0), false, 0.0)
//
//    var ctx : CGContextRef = UIGraphicsGetCurrentContext()
//    
//    CGContextSaveGState(ctx)
//    
//    CGContextSetBlendMode(ctx, kCGBlendModeMultiply)
//    
//    var rect = CGRectMake(0, 0, 20, 20)
//    CGContextSetAlpha(ctx, alpha)
//    CGContextSetFillColorWithColor(ctx, circleFillColor.CGColor)
//    CGContextFillEllipseInRect(ctx, rect)
//    
//    CGContextRestoreGState(ctx)
//    opaqueCircle = UIGraphicsGetImageFromCurrentImageContext()
//    UIGraphicsEndImageContext()
//    
//    
//    return opaqueCircle
//}


/*
stringJson:
{
"html_attributions" : [],
"result" : {
"address_components" : [
{
"long_name" : "30",
"short_name" : "30",
"types" : [ "street_number" ]
},
{
"long_name" : "Fish Street Hill",
"short_name" : "Fish St Hill",
"types" : [ "route" ]
},
{
"long_name" : "London",
"short_name" : "London",
"types" : [ "postal_town" ]
},
{
"long_name" : "United Kingdom",
"short_name" : "GB",
"types" : [ "country", "political" ]
},
{
"long_name" : "EC3R 6DN",
"short_name" : "EC3R 6DN",
"types" : [ "postal_code" ]
}
],
"adr_address" : "Monument, \u003cspan class=\"street-address\"\u003e30 Fish Street Hill\u003c/span\u003e, \u003cspan class=\"locality\"\u003eLondon\u003c/span\u003e \u003cspan class=\"postal-code\"\u003eEC3R 6DN\u003c/span\u003e, \u003cspan class=\"country-name\"\u003eUnited Kingdom\u003c/span\u003e",
"formatted_address" : "Monument, 30 Fish Street Hill, London EC3R 6DN, United Kingdom",
"formatted_phone_number" : "020 3371 3349",
"geometry" : {
"location" : {
"lat" : 51.509664,
"lng" : -0.086253
}
},
"icon" : "http://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png",
"id" : "6b63b757489eecbc16b0a73d9e69b667a6e3da94",
"international_phone_number" : "+44 20 3371 3349",
"name" : "American Golf",
"opening_hours" : {
"open_now" : true,
"periods" : [
{
"close" : {
"day" : 0,
"time" : "1600"
},
"open" : {
"day" : 0,
"time" : "1000"
}
},
{
"close" : {
"day" : 1,
"time" : "2000"
},
"open" : {
"day" : 1,
"time" : "0800"
}
},
{
"close" : {
"day" : 2,
"time" : "2000"
},
"open" : {
"day" : 2,
"time" : "0800"
}
},
{
"close" : {
"day" : 3,
"time" : "2000"
},
"open" : {
"day" : 3,
"time" : "0800"
}
},
{
"close" : {
"day" : 4,
"time" : "2000"
},
"open" : {
"day" : 4,
"time" : "0800"
}
},
{
"close" : {
"day" : 5,
"time" : "2000"
},
"open" : {
"day" : 5,
"time" : "0800"
}
},
{
"close" : {
"day" : 6,
"time" : "1700"
},
"open" : {
"day" : 6,
"time" : "0900"
}
}
],
"weekday_text" : [
"Monday: 8:00 am – 8:00 pm",
"Tuesday: 8:00 am – 8:00 pm",
"Wednesday: 8:00 am – 8:00 pm",
"Thursday: 8:00 am – 8:00 pm",
"Friday: 8:00 am – 8:00 pm",
"Saturday: 9:00 am – 5:00 pm",
"Sunday: 10:00 am – 4:00 pm"
]
},
"place_id" : "ChIJZeLN3lMDdkgR5Zx5syC5NgI",
"reference" : "CmRgAAAAWMBa7BDU_GuD3x1X-tCfGlHXv4Shz72L8PTwGeHMFqKQmz9NJbpop4ImEwR8fKAhF0QeodfNXzxWHTUIR1KSClWOXlZ4LIZbr0mtzT61W6S7wIDrPwlDOqaOIvJb0dADEhBxf2XNmBkd1miexCAMxLlNGhTLFLZY06Sn7VL-ItN7wEGCKxIweg",
"scope" : "GOOGLE",
"types" : [ "store", "establishment" ],
"url" : "https://plus.google.com/116012597509910198766/about?hl=en-US",
"utc_offset" : 60,
"vicinity" : "Monument, 30 Fish Street Hill, London",
"website" : "http://www.americangolf.co.uk/"
},
"status" : "OK"
}

jsonResultNSDictionary.allKeys:
[html_attributions, result, status]
done!
done!
result:[id, website, address_components, vicinity, formatted_address, international_phone_number, url, adr_address, scope, formatted_phone_number, geometry, icon, place_id, opening_hours, reference, types, name, utc_offset]
(lldb)

*/
