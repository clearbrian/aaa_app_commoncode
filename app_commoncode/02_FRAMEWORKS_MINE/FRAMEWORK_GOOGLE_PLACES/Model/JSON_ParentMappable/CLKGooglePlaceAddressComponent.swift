//
//  CLKGooglePlaceAddressComponent.swift
//  joyride
//
//  Created by Brian Clear on 03/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation
//import ObjectMapper

class CLKGooglePlaceAddressComponent: ParentMappable {
    

    var long_name:String?
    var short_name:String?
    var types: [String]?
    
    // root calls mapping()
    //    required init?(_ map: Map){
    //        mapping(map: map)
    //    }
    override func mapping(map: Map){
        long_name <- map["long_name"]
        short_name <- map["short_name"]
        types <- map["types"]
    }
}
/*
stringJson:
{
    "html_attributions" : [],
    "result" : {
        "address_components" : [
        {
        "long_name" : "30",
        "short_name" : "30",
        "types" : [ "street_number" ]
        },


*/
