//
//  CLKGooglePlaceReview.swift
//  joyride
//
//  Created by Brian Clear on 09/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//


class CLKGooglePlaceReview: ParentMappable {
    
    
    var aspects : [CLKGooglePlaceAspect]?
    var author_name:String?
    var author_url:String?
    var language:String?
    var rating:Int?
    var text:String?
    var time:Int64?
    var profile_photo_url:String?
    
    
    override func mapping(map: Map){
        aspects <- map["aspects"]
        author_name <- map["author_name"]
        author_url <- map["author_url"]
        language <- map["language"]
        rating <- map["rating"]
        text <- map["text"]
        time <- map["time"]
        profile_photo_url <- map["profile_photo_url"]
    }
}
/*
reviews =         (
    {
        aspects =                 (
            {
                rating = 3;
                type = overall;
            }
        );
        "author_name" = "Danielle Lonnon";
        "author_url" = "https://plus.google.com/118257578392162991040";
        language = en;
        rating = 5;
        text = "As someone who works in the theatre, I don't find the Google offices nerdy, I find it magical and theatrical. Themed rooms  with useful props and big sets with unique and charismatic characters. You sure this isn't a theatre company? Oh no wait Google has money, while the performing art does not.";
        time = 1425790392;
    },
    {
        aspects =                 (
            {
                rating = 3;
                type = overall;
            }
        );
        "author_name" = "Rob Mulally";
        "author_url" = "https://plus.google.com/100839435712919930388";
        language = en;
        rating = 5;
        text = "What can I say, what a great building, cool offices and friendly staff!\nonly had a quick tour but there isn't much missing from this world class modern office.\n\nIf your staff who work here I hope you take advantage of all that it offers , because as a visitor it was a very impressive setup. \n\nThe thing that stood out besides the collaborative area's and beds for resting, was the food availability.\n\nImpressed. 5 Stars.\n";
        time = 1408284830;
},
*/
