//
//  CLKGooglePlace.swift
//  joyride
//
//  Created by Brian Clear on 02/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation



class CLKGooglePlaceAutocompleteResponse: ParentMappable {

    //------------------------------------------------
    //place/details has 1 result
    //------------------------------------------------
    
    //[predictions, status]
    var predictions: [CLKGooglePlaceAutocompletePrediction]?
    var status:String?
    
    //https://github.com/Hearst-DD/ObjectMapper
    override func mapping(map: Map){
        
        predictions <- map["predictions"]
        status <- map["status"]

    }
}


/*
================JSON RETURNED START =====================
    {
        
        "predictions" : [
        
        {
        
        "description" : "Causton Street, London, United Kingdom",
        
        "id" : "727c79727187f936bea8c885c822c2a1ad28b6d2",
        
        "matched_substrings" : [
        
        {
        
        "length" : 3,
        
        "offset" : 0
        
        }
        
        ],
        
        "place_id" : "EiZDYXVzdG9uIFN0cmVldCwgTG9uZG9uLCBVbml0ZWQgS2luZ2RvbQ",
        
        "reference" : "CjQqAAAAId2e5dc30sPcsWoJazSUHZNZvvbTEh4VzAFQQ_-hJjsR1_sID-cNqvI8OHXdoUG_EhDulpl2LqKF3ljvgBNm7S9dGhRirMydQPmof-CJbhrpKocdgvYz5w",
        
        "terms" : [
        
        {
        
        "offset" : 0,
        
        "value" : "Causton Street"
        
        },
        
        {
        
        "offset" : 16,
        
        "value" : "London"
        
        },
        
        {
        
        "offset" : 24,
        
        "value" : "United Kingdom"
        
        }
        
        ],
        
        "types" : [ "route", "geocode" ]
        
        },
        
        {
        
        "description" : "Caucasus, London, United Kingdom",
        
        "id" : "9485c7abed06dd91e24f311816ed42e8047beb3c",
        
        "matched_substrings" : [
        
        {
        
        "length" : 3,
        
        "offset" : 0
        
        }
        
        ],
        
        "place_id" : "ChIJL6IWjywbdkgRmH3kIGAJGNE",
        
        "reference" : "CjQvAAAAgmpuw7LNIfA9umqdtp1gBnsUwNn_6wqChZJRSgXfCcPGu9L02jbDVIjAgRNaUm-tEhAuXSFKFdClluR9UeVMrtCFGhSuEGjMuL0m6fkV_ToTCOYFsUWLYQ",
        
        "terms" : [
        
        {
        
        "offset" : 0,
        
        "value" : "Caucasus"
        
        },
        
        {
        
        "offset" : 10,
        
        "value" : "London"
        
        },
        
        {
        
        "offset" : 18,
        
        "value" : "United Kingdom"
        
        }
        
        ],
        
        "types" : [ "establishment" ]
        
        },
        
        {
        
        "description" : "CAU St Katharine Docks, London, United Kingdom",
        
        "id" : "53db4ef3bb4de36e964482cddf83dfbeca77ce87",
        
        "matched_substrings" : [
        
        {
        
        "length" : 3,
        
        "offset" : 0
        
        }
        
        ],
        
        "place_id" : "ChIJ5SjWvUkDdkgRpf6_IqLvKbk",
        
        "reference" : "CkQ9AAAAp8hMFmPnvcgSAOyRWVC6-OXlQpkser3j56G3ESgcBPoSk6aRFzZN65HX5p4qEpLquuWNu6122NGw_W-sl9ThgxIQ8d0fZJ5zJ9F61-H5wVE1fBoUB_Ockvoo59HKsXtLf_uFDdTP_yg",
        
        "terms" : [
        
        {
        
        "offset" : 0,
        
        "value" : "CAU St Katharine Docks"
        
        },
        
        {
        
        "offset" : 24,
        
        "value" : "London"
        
        },
        
        {
        
        "offset" : 32,
        
        "value" : "United Kingdom"
        
        }
        
        ],
        
        "types" : [ "establishment" ]
        
        },
        
        {
        
        "description" : "CAU Blackheath, Royal Parade, London, United Kingdom",
        
        "id" : "33f1262464c817a6c0314662eab00bcf9739c51d",
        
        "matched_substrings" : [
        
        {
        
        "length" : 3,
        
        "offset" : 0
        
        }
        
        ],
        
        "place_id" : "ChIJF23SZtGp2EcRgtUJp5ZS6Ts",
        
        "reference" : "ClRCAAAAPUjSJvC-RYCyX7gLv8wsvZOWTu5dAMf5lqoHrtGPv1bIkMLxRMA9vAbUCdX6291qKmYeo8nXgJplPqSvdxhvaJYDwQuRYiuWs4OlLeVHQ2ISEAMh7CXKNwJdZJYqdsqDgOkaFFqGm21PZQqjMzHr7OlBoq1idoVX",
        
        "terms" : [
        
        {
        
        "offset" : 0,
        
        "value" : "CAU Blackheath"
        
        },
        
        {
        
        "offset" : 16,
        
        "value" : "Royal Parade"
        
        },
        
        {
        
        "offset" : 30,
        
        "value" : "London"
        
        },
        
        {
        
        "offset" : 38,
        
        "value" : "United Kingdom"
        
        }
        
        ],
        
        "types" : [ "establishment" ]
        
        },
        
        {
        
        "description" : "CAU Kingston, Riverside Walk, Kingston upon Thames, United Kingdom",
        
        "id" : "6edde256489fea02947de73562ad49c3b6a3c010",
        
        "matched_substrings" : [
        
        {
        
        "length" : 3,
        
        "offset" : 0
        
        }
        
        ],
        
        "place_id" : "ChIJDWuMnpULdkgRT-diQTiY8-c",
        
        "reference" : "CmRRAAAANzUbcWivXZrU6Wsmf4a4CDvz2ONycaGkIR_hZzaN4s0hdV2bQ1CFmlzko-aBvcapAaicZsTz4-5T4F87ktTrAqcHfju8H5OmGysSmkmneu5jArsXOLIcwwddRTDZrsMXEhABeZZ8GG4C5ewiazahb7dUGhRNAyvLXiYoHRpNGzotFx7jN4qpMg",
        
        "terms" : [
        
        {
        
        "offset" : 0,
        
        "value" : "CAU Kingston"
        
        },
        
        {
        
        "offset" : 14,
        
        "value" : "Riverside Walk"
        
        },
        
        {
        
        "offset" : 30,
        
        "value" : "Kingston upon Thames"
        
        },
        
        {
        
        "offset" : 52,
        
        "value" : "United Kingdom"
        
        }
        
        ],
        
        "types" : [ "establishment" ]
        
        }
        
        ],
        
        "status" : "OK"
        
}


================JSON RETURNED END=====================
*/
