//
//  CLKGooglePlaceViewport.swift
//  joyride
//
//  Created by Brian Clear on 08/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//
//used in places api
//used in Directions api
import GoogleMaps

class CLKGooglePlaceViewport: ParentMappable {
    
    var northeast:CLKGooglePlaceLocation?
    var southwest:CLKGooglePlaceLocation?
    
    // root calls mapping()
    //    required init?(_ map: Map){
    //        mapping(map: map)
    //    }
    
    override func mapping(map: Map){
        northeast <- map["northeast"]
        southwest <- map["southwest"]
    }

    var gmsCoordinateBounds : GMSCoordinateBounds?{
        get{
            var gmsCoordinateBounds_ : GMSCoordinateBounds?
            //-------------------------------------------------------------------
            
            if let northeast = self.northeast {
                //print("gmsCoordinateBounds northeast:\(northeast)")
                
                if let southwest = self.southwest {
                    //print("gmsCoordinateBounds southwest:\(southwest)")

                    //---------------------------------------------------------------------
                    if let northeast_cllocationCoordinate2D = northeast.cllocationCoordinate2D {
                        if let southwest_cllocationCoordinate2D = southwest.cllocationCoordinate2D {
                            //------------------------------------------------------------------------------------------------
                            
                            gmsCoordinateBounds_ = GMSCoordinateBounds(
                                coordinate: northeast_cllocationCoordinate2D,
                                coordinate: southwest_cllocationCoordinate2D)
                            //------------------------------------------------------------------------------------------------

                        }else{
                            logger.error("southwest.cllocationCoordinate2D is nil")
                        }
                    }else{
                        logger.error("northeast.cllocationCoordinate2D is nil")
                    }
                    //---------------------------------------------------------------------
                }else{
                    logger.error("southwest is nil")
                }
            }else{
                logger.error("northeast is nil")
            }
            //-------------------------------------------------------------------

            return gmsCoordinateBounds_
        }
    }
}
/*
"viewport":{
    "northeast":{
        "lat":-33.8623818,
        "lng":151.1989734
    },
    "southwest":{
        "lat":-33.8762122,
        "lng":151.1860651
    }
}
*/
