//
//  COLCOAuth2ViewController.swift
//  joyride
//
//  Created by Brian Clear on 06/07/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit

class COLCOAuth2ViewController: ParentViewController, COLCOAuth2ManagerDelegate  {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonGetAuthorization_Action(_ sender: AnyObject) {
        
        appDelegate.colcOAuth2Manager.delegate = self
        //---------------------------------------------------------------------
        //Comment in ONLY one
        //---------------------------------------------------------------------
//        appDelegate.colcOAuth2Manager.authorizeWithExternalSafari(.Github)
//        appDelegate.colcOAuth2Manager.authorizeWithSafariInViewController(self, oauth2API: .Github)
        //---------------------------------------------------------------------
//        appDelegate.colcOAuth2Manager.authorizeWithExternalSafari(.Foursquare)
        appDelegate.colcOAuth2Manager.authorizeWithSafariInViewController(self, oauth2API: .Foursquare)
        //---------------------------------------------------------------------
    }
    
    //--------------------------------------------------------------
    // MARK: - COLCOAuth2ManagerDelegate
    // MARK: -
    //--------------------------------------------------------------

    func accessTokenReceivedOK(_ colcOAuth2Manager: COLCOAuth2Manager, access_token: String, oAuthContext: OAuthContext){

        if let _ = oAuthContext as? OAuthContextGithub {
            //callApi_GithubUser()
            callApi_GithubUserRepos()
            
        }else if let _ = oAuthContext as? OAuthContextFoursquare {
            callApi_FoursquareUser()
            
        }else{
            logger.error("self.OAuthContextGithub is nil")
        }
       
    
        
        
        
//        todo - fix JSONExport app
//        
//        use SFSafariController
//        
//        foursquare
//        https://github.com/search?l=Objective-C&p=2&q=foursquare&type=Repositories&utf8=%E2%9C%93
//        yelp
//        meetup
//        do meetup api - meetup requires http referer not joyride://
//        
//        eventbrite
//        tripadvisor
//        google calendar
//        lanyrd
//        eventjoy
//        
//        http://www.mashery.com/blog/meeting-hotel-industry-challenges-apis-check
//        expedia
//        orbitz
//        KAYAK
//        hipmunk
    }
    
    func callApi_GithubUser(){
        //not asynch
        //appDelegate.colcOAuth2Manager.delegate = self
        appDelegate.colcOAuth2Manager.callApi_GithubUser(
            
            success:{ (githubUser: GithubUser)->Void in
                logger.info("githubUser:\(String(describing: githubUser.login))")
                
                
            },
            failure:{ (error) -> Void in
                self.handleError(error)
            }
        )
    }
    
    func callApi_FoursquareUser(){
        
        //---------------------------------------------------------------------
        //not asynch
        //appDelegate.colcOAuth2Manager.delegate = self
        //---------------------------------------------------------------------
        appDelegate.colcOAuth2Manager.callApi_FoursquareUser(
            
            success:{ (foursquareUser: FoursquareUser)->Void in
               // logger.info("foursquareUser:\(foursquareUser.firstName ?? <#default value#>) \(String(describing: foursquareUser.lastName))")
                
                
            },
            failure:{ (error) -> Void in
                self.handleError(error)
            }
        )
    }
    func callApi_FoursquareVenuesSearch(){
        
        //---------------------------------------------------------------------
        //not asynch
        //appDelegate.colcOAuth2Manager.delegate = self
        //---------------------------------------------------------------------
        appDelegate.colcOAuth2Manager.callApi_FoursquareVenuesSearch(
            
            success:{ (foursquareVenuesSearch : FoursquareVenuesSearch)->Void in
                
                logger.info("foursquareVenuesSearch:TODO")
                
                
            },
            failure:{ (error) -> Void in
                self.handleError(error)
            }
        )

    }
    
    
    func callApi_GithubUserRepos(){
        //not asynch
        //appDelegate.colcOAuth2Manager.delegate = self
        
        
        appDelegate.colcOAuth2Manager.callApi_GithubUserRepos(
            
            success:{ (githubUserRepoArray: [GithubUserRepo])->Void in
                logger.info("githubUserRepoArray returned:\(githubUserRepoArray.count)")
                for githubUserRepo in githubUserRepoArray{
                    logger.info("REPO:\(githubUserRepo.name)")
                }
                
            },
            failure:{ (error) -> Void in
                self.handleError(error)
            }
        )
    }
    
    
    
    func accessTokenFailed(_ colcOAuth2Manager: COLCOAuth2Manager, error: Error){
        self.handleError(error)
    }
    
    func handleError(_ error: Error){
        var showErrorAlert = true
        
        switch error{

        case AppError.googlePlacesResponseStatus_ZERO_RESULTS_DISTANCE:
            logger.info("status: ZERO_RESULTS")
            //------------------------------------------------------------------------------------------------
            //DONT SHOW ERROR ALERT FOR ZERO RESULTS
            showErrorAlert = false

            //                        //remove all results
            //                        self.nearbySearch_CLKGooglePlaceResultsArray = [CLKGooglePlaceResult]()
            //                        self.tableViewPlacesResults.reloadData()
            //
            //                        //------------------------------------------------------------------------------------------------
            //                        self.labelAddress.text = ""
            //                        //------------------------------------------------------------------------------------------------

            //------------------------------------------------------------------------------------------------

        case AppError.googlePlacesResponseStatus_OVER_QUERY_LIMIT:
            logger.info("status: OVER_QUERY_LIMIT")

        case AppError.googlePlacesResponseStatus_REQUEST_DENIED:
            logger.info("status: REQUEST_DENIED")

        case AppError.googlePlacesResponseStatus_INVALID_REQUEST:

            logger.info("status: INVALID_REQUEST")

        default:
            logger.error("UNKNOWN error: \(error)")
        }
        
        if showErrorAlert{
            //if let parentViewController = self.parentViewController {
                CLKAlertController.showAlertInVCWithCallback(self, title: "Error", message: error.localizedDescription)
                { () -> Void in
                    logger.info("Ok tapped = alert closed appDelegate.doLogout()")
                }
            //}else{
            //    logger.error("parentViewController is nil")
            //}
        }
    }
}
