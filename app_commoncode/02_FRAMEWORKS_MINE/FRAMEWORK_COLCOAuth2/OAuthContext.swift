//
//  OAuthContext.swift
//  colcoauth2
//
//  Created by Brian Clear on 05/07/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation

class OAuthContext{
    //---------------------------------------------------------------------
//    let settings : [String : String] = [String : String]()
    
    
    
    //---------------------------------------------------------------------
    //AUTHORIZE
    //---------------------------------------------------------------------
    //e.g. "https://github.com/login/oauth/authorize"
    //---------------------------------------------------------------------
    var oauth_url_authorize: String?
    
    var oauth_client_id : String?
    var oauth_client_secret : String?
    
    var oauth_redirect_uri: String?

    var oauth_authorize_code: String?
    
    //---------------------------------------------------------------------
    //TOKEN request
    //---------------------------------------------------------------------
    //"token_uri": "https://github.com/login/oauth/access_token",
    var oauth_url_request_access_token: String?
    
    
    //---------------------------------------------------------------------
    //SHould be subclasses
    //---------------------------------------------------------------------
    var access_token: String? {
       
        logger.error("var access_token: String? shoudl be subclassed ")
        return nil
    }
    
    //---------------------------------------------------------------------
    //state=
    //a generated GUID
    //sent with first call must be checked when returned
    //optional in the OAUTH2 and so far only seen in github
    //---------------------------------------------------------------------
    var stateIsRequired = false
    
    
    //---------------------------------------------------------------------
    //STATE
    //---------------------------------------------------------------------
    //https://developer.apple.com/library/ios/documentation/Swift/Conceptual/Swift_Programming_Language/AccessControl.html
    
    // /Users/gbxc/Documents/00_ME/00_BITBUCKET_COLC/app_github_v3_oauth_tester_v2/app_github_v3_oauth_tester/00_GITHUB/p2/OAuth2PodApp-origFromGithubWORKING_USING_CUSTOM_OAUTH/OAuth2PodApp/p2.OAuth2/Sources/Base/OAuth2.swift
    
    
    /// The current state.
    internal(set) var _state = ""
    
    /**
     The state sent to the server when requesting a token.
     
     We internally generate a UUID and use the first 8 chars if `_state` is empty.
     */
    var state: String {
        if _state.isEmpty {
            _state = UUID().uuidString
            _state = String(_state[_state.startIndex..<_state.characters.index(_state.startIndex, offsetBy: 8)])		// only use the first 8 chars, should be enough
        }
        return _state
    }
    
    /**
     Checks that given state matches the internal state.
     
     - parameter state: The state to check (may be nil)
     - returns: true if state matches, false otherwise or if given state is nil.
     */
    func matchesState(_ state: String?) -> Bool {
        if let st = state {
            return st == _state
        }
        return false
    }
    
    /**
     Resets current state so it gets regenerated next time it's needed.
     */
    func resetState() {
        _state = ""
    }
    
    
    //--------------------------------------------------------------
    // MARK: - init
    // MARK: -
    //--------------------------------------------------------------

    init(oauth_url_authorize : String,
         oauth_client_id : String,
         oauth_client_secret : String,
         oauth_redirect_uri : String,
         oauth_url_request_access_token : String,
         stateIsRequired: Bool)
    {
        //step2: request authorization grant
        self.oauth_url_authorize = oauth_url_authorize
        
        self.oauth_client_id = oauth_client_id
        self.oauth_client_secret = oauth_client_secret
        self.oauth_redirect_uri = oauth_redirect_uri

        //step2: request access_token
        self.oauth_url_request_access_token = oauth_url_request_access_token
        self.stateIsRequired = stateIsRequired
    }
    
    //--------------------------------------------------------------
    // MARK: - isValid
    // MARK: -
    //--------------------------------------------------------------
//use nested if instead easier to track
//    func isValid() -> Bool{
//        
//        guard let  _ = self.oauth_url_authorize else {
//            logger.error("[isValid] oauth_url_authorize is nil")
//            return false
//        }
//        
//        guard let  _ = self.oauth_client_id else {
//            logger.error("[isValid] oauth_client_id is nil")
//            return false
//        }
//        
//        guard let  _ = self.oauth_client_secret else {
//            logger.error("[isValid] oauth_client_secret is nil")
//            return false
//        }
//        
//        guard let  _ = self.oauth_redirect_uri else {
//            logger.error("[isValid] oauth_redirect_uri is nil")
//            return false
//        }
//        
//        return true
//        
//    }
    
    
    
    //---------------------------------------------------------------------
    //https://github.com/login/oauth/authorize?
    //e.g. "https://github.com/login/oauth/authorize?client_id=be45feb27af1729e6312&redirect_uri=joyrider://oauth/callback_github&scope=user&state=8E7F9061&allow_signup=false"
    //---------------------------------------------------------------------
    var urlStringAuthorize : String?{
        
        //all are requires so if any missing return nil
        var urlStringAuthorize_ : String?
        
        //easier to append if not optional
        var urlStringAuthorizeToAppend = ""
        //---------------------------------------------------------------------
        if let oauth_url_authorize = self.oauth_url_authorize {
            
            urlStringAuthorizeToAppend = urlStringAuthorizeToAppend + "\(oauth_url_authorize)?"
            
            
            //---------------------------------------------------------------------
            //client_id=be45feb27af1729e6312
            if let oauth_client_id = self.oauth_client_id {
                urlStringAuthorizeToAppend = urlStringAuthorizeToAppend + "client_id=\(oauth_client_id)"
                
                //---------------------------------------------------------------------
                //&redirect_uri=joyrider://oauth/callback_github
                if let oauth_redirect_uri = self.oauth_redirect_uri {
                    urlStringAuthorizeToAppend = urlStringAuthorizeToAppend + "&redirect_uri=\(oauth_redirect_uri)"
                    
                    //---------------------------------------------------------------------
                    //all required paramters present - note subclasses may require their own
                    //---------------------------------------------------------------------
                    urlStringAuthorize_ = urlStringAuthorizeToAppend
                    //---------------------------------------------------------------------
                }else{
                    logger.error("oauth_redirect_uri is nil")
                }
                //---------------------------------------------------------------------
            }else{
                logger.error("oauth_client_id is nil")
            }
            
        }else{
            logger.error("self.oauth_url_authorize is nil")
        }

        //if nil then some missing
        return urlStringAuthorize_
    }
    
    
    //---------------------------------------------------------------------
    //should subclass
    // http://stackoverflow.com/questions/24111356/swift-class-method-which-must-be-overridden-by-subclass
    var bodyStringRequestAccessToken : String?{
         //should subclass
        return ""
    }
    
    //---------------------------------------------------------------------
    //should subclass
    func parseAccessTokenResponse(_ parsedObject: Any,
                                  success: (_ access_token: String) -> Void,
                                  failure: (_ error: Error) -> Void)
    {
        logger.error("SHOULD SUBCLASS parseAccessTokenResponse")
        
        failure(NSError.errorWithDescription("SHOULD SUBCLASS parseAccessTokenResponse", code:379487))
        
    }
    
    func buildAccessTokenRequest() -> OAuth2ParentWSRequest?{
        //oauth2ParentWSRequest: OAuth2ParentWSRequest
        
        logger.error("SHOULD SUBCLASS buildAccessTokenRequest")
        
        return nil
    }
    
}
