//
//  OAuth2AccessTokenFoursquareResponse.swift
//  joyride
//
//  Created by Brian Clear on 07/07/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation

// {"access_token": "ZPCT1JPZB3RQXM0Q3M5BAALVYNPASFK22IW40CVO0OBX5NQN"}
class OAuth2AccessTokenFoursquareResponse: ParentMappable, CustomStringConvertible {
    
    var access_token: String?
    
    // root calls mapping()
    //    required init?(_ map: Map){
    //        mapping(map: map)
    //    }
    
    override func mapping(map: Map){
        access_token <- map["access_token"]
    }
    
    var description: String {
        let description_ = "**** \(type(of: self)) *****\r"
        //description_ = description_ + "accessToken: \(access_token)\r"
        return description_
    }
}
