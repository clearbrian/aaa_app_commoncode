//
//	FoursquareVenuesSearch.swift
//
//	Create by Brian Clear on 19/7/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class FoursquareVenuesSearch : ParentMappable{

	var meta : FoursquareVenuesSearchMeta?
	var notifications : [FoursquareVenuesSearchNotification]?
	var response : FoursquareVenuesSearchResponse?



	override func mapping(map: Map)
	{
		meta <- map["meta"]
		notifications <- map["notifications"]
		response <- map["response"]
		
	}
	var description: String
	{
		let description_ = "**** \(type(of: self)) *****\r"
		//description_ = description_ + "meta: \(meta)\r"
		//description_ = description_ + "notifications: \(notifications)\r"
		//description_ = description_ + "response: \(response)\r"
		return description_
	}

}
