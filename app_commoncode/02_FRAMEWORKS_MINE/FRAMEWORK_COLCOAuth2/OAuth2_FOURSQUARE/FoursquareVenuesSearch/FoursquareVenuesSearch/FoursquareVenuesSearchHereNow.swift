//
//	FoursquareVenuesSearchHereNow.swift
//
//	Create by Brian Clear on 19/7/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class FoursquareVenuesSearchHereNow : ParentMappable{

	var count : Int?
	var groups : [String]?
	var summary : String?



	override func mapping(map: Map)
	{
		count <- map["count"]
		groups <- map["groups"]
		summary <- map["summary"]
		
	}
	var description: String
	{
		let description_ = "**** \(type(of: self)) *****\r"
		//description_ = description_ + "count: \(count)\r"
		//description_ = description_ + "groups: \(groups)\r"
		//description_ = description_ + "summary: \(summary)\r"
		return description_
	}

}
