//
//	FoursquareVenuesSearchLocation.swift
//
//	Create by Brian Clear on 19/7/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class FoursquareVenuesSearchLocation : ParentMappable{

	var cc : String?
	var country : String?
	var distance : Int?
	var lat : Float?
	var lng : Float?



	override func mapping(map: Map)
	{
		cc <- map["cc"]
		country <- map["country"]
		distance <- map["distance"]
		lat <- map["lat"]
		lng <- map["lng"]
		
	}
	var description: String
	{
		let description_ = "**** \(type(of: self)) *****\r"
		//description_ = description_ + "cc: \(cc)\r"
		//description_ = description_ + "country: \(country)\r"
		//description_ = description_ + "distance: \(distance)\r"
		//description_ = description_ + "lat: \(lat)\r"
		//description_ = description_ + "lng: \(lng)\r"
		return description_
	}

}
