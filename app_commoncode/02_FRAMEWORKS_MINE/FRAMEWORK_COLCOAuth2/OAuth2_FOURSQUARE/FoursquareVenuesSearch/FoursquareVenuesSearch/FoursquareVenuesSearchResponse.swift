//
//	FoursquareVenuesSearchResponse.swift
//
//	Create by Brian Clear on 19/7/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class FoursquareVenuesSearchResponse : ParentMappable{

	var confident : Bool?
	var venues : [FoursquareVenuesSearchVenue]?



	override func mapping(map: Map)
	{
		confident <- map["confident"]
		venues <- map["venues"]
		
	}
	var description: String
	{
		let description_ = "**** \(type(of: self)) *****\r"
		//description_ = description_ + "confident: \(confident)\r"
		//description_ = description_ + "venues: \(venues)\r"
		return description_
	}

}
