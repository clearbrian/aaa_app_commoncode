//
//  FoursquareAccessTokenGetRequest
//  joyride
//
//  Created by Brian Clear on 08/07/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation

class FoursquareAccessTokenGetRequest : OAuth2ParentAccessTokenWSRequest{
    
    override init(access_token : String) {
        super.init(access_token: access_token)
        
        
        //token is sent in GET
        self.colcHTTPMethod = .Get
        
        //---------------------------------------------------------------------
        //&v=20160708
        
        let formatterOUT_ = DateFormatter()
        formatterOUT_.dateFormat = "yyyyMMdd"
        let versionDate = formatterOUT_.string( from: Date())
        //---------------------------------------------------------------------
        
        
        self.query = "oauth_token=\(access_token)&v=\(versionDate)"
        
        
        
        
    }
}
