//
//	FoursquareIcon.swift
//
//	Create by Brian Clear on 8/7/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class FoursquareIcon : ParentMappable{

	var prefix : String?
	var suffix : String?



	override func mapping(map: Map)
	{
		prefix <- map["prefix"]
		suffix <- map["suffix"]
		
	}
	var description: String
	{
		let description_ = "**** \(type(of: self)) *****\r"
		//description_ = description_ + "prefix: \(prefix)\r"
		//description_ = description_ + "suffix: \(suffix)\r"
		return description_
	}

}
