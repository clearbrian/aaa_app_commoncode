//
//	FoursquareItem.swift
//
//	Create by Brian Clear on 8/7/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class FoursquareItem : ParentMappable{

	var unreadCount : Int?
	var comments : FoursquareComment?
	var createdAt : Int?
	var id : String?
	var isMayor : Bool?
	var like : Bool?
	var likes : FoursquareLike?
	var photos : FoursquarePhoto?
	var posts : FoursquarePost?
	var source : FoursquareSource?
	var timeZoneOffset : Int?
	var type : String?
	var venue : FoursquareVenue?
	var canonicalUrl : String?
	var collaborative : Bool?
	var descriptionField : String?
	var editable : Bool?
	var listItems : FoursquareComment?
	var name : String?
	var publicField : Bool?
	var url : String?



	override func mapping(map: Map)
	{
		unreadCount <- map["unreadCount"]
		comments <- map["comments"]
		createdAt <- map["createdAt"]
		id <- map["id"]
		isMayor <- map["isMayor"]
		like <- map["like"]
		likes <- map["likes"]
		photos <- map["photos"]
		posts <- map["posts"]
		source <- map["source"]
		timeZoneOffset <- map["timeZoneOffset"]
		type <- map["type"]
		venue <- map["venue"]
		canonicalUrl <- map["canonicalUrl"]
		collaborative <- map["collaborative"]
		descriptionField <- map["description"]
		editable <- map["editable"]
		listItems <- map["listItems"]
		name <- map["name"]
		publicField <- map["public"]
		url <- map["url"]
		
	}
	var description: String
	{
		let description_ = "**** \(Swift.type(of: self)) *****\r"
		//description_ = description_ + "unreadCount: \(unreadCount)\r"
		//description_ = description_ + "comments: \(comments)\r"
		//description_ = description_ + "createdAt: \(createdAt)\r"
		//description_ = description_ + "id: \(id)\r"
		//description_ = description_ + "isMayor: \(isMayor)\r"
		//description_ = description_ + "like: \(like)\r"
		//description_ = description_ + "likes: \(likes)\r"
		//description_ = description_ + "photos: \(photos)\r"
		//description_ = description_ + "posts: \(posts)\r"
		//description_ = description_ + "source: \(source)\r"
		//description_ = description_ + "timeZoneOffset: \(timeZoneOffset)\r"
		//description_ = description_ + "type: \(type)\r"
		//description_ = description_ + "venue: \(venue)\r"
		//description_ = description_ + "canonicalUrl: \(canonicalUrl)\r"
		//description_ = description_ + "collaborative: \(collaborative)\r"
		//description_ = description_ + "descriptionField: \(descriptionField)\r"
		//description_ = description_ + "editable: \(editable)\r"
		//description_ = description_ + "listItems: \(listItems)\r"
		//description_ = description_ + "name: \(name)\r"
		//description_ = description_ + "publicField: \(publicField)\r"
		//description_ = description_ + "url: \(url)\r"
		return description_
	}

}
