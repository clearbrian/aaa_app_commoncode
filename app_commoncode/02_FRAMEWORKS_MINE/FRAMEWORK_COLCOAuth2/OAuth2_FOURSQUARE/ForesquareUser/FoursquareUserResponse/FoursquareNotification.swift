//
//	FoursquareNotification.swift
//
//	Create by Brian Clear on 8/7/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class FoursquareNotification : ParentMappable{

	var item : FoursquareItem?
	var type : String?



	override func mapping(map: Map)
	{
		item <- map["item"]
		type <- map["type"]
		
	}
	var description: String
	{
		let description_ = "**** \(Swift.type(of: self)) *****\r"
		//description_ = description_ + "item: \(item)\r"
		//description_ = description_ + "type: \(type)\r"
		return description_
	}

}
