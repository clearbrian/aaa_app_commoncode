//
//	FoursquareGroup.swift
//
//	Create by Brian Clear on 8/7/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class FoursquareGroup : ParentMappable{

	var count : Int?
	var items : [String]?
	var name : String?
	var type : String?
//	var items : [FoursquareItem]?



	override func mapping(map: Map)
	{
		count <- map["count"]
		items <- map["items"]
		name <- map["name"]
		type <- map["type"]
		items <- map["items"]
		
	}
	var description: String
	{
		let description_ = "**** \(Swift.type(of: self)) *****\r"
		//description_ = description_ + "count: \(count)\r"
		//description_ = description_ + "items: \(items)\r"
		//description_ = description_ + "name: \(name)\r"
		//description_ = description_ + "type: \(type)\r"
//		//description_ = description_ + "items: \(items)\r"
		return description_
	}

}
