//
//	FoursquareUserResponse.swift
//
//	Create by Brian Clear on 8/7/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class FoursquareUserResponse : ParentMappable{

	var meta : FoursquareMeta?
	var notifications : [FoursquareNotification]?
	var response : FoursquareResponse?



	override func mapping(map: Map)
	{
		meta <- map["meta"]
		notifications <- map["notifications"]
		response <- map["response"]
		
	}
	var description: String
	{
		let description_ = "**** \(type(of: self)) *****\r"
		//description_ = description_ + "meta: \(meta)\r"
		//description_ = description_ + "notifications: \(notifications)\r"
		//description_ = description_ + "response: \(response)\r"
		return description_
	}

}
