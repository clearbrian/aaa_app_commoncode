//
//	FoursquareResponse.swift
//
//	Create by Brian Clear on 8/7/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class FoursquareResponse : ParentMappable{

	var user : FoursquareUser?



	override func mapping(map: Map)
	{
		user <- map["user"]
		
	}
	var description: String
	{
		let description_ = "**** \(type(of: self)) *****\r"
		//description_ = description_ + "user: \(user)\r"
		return description_
	}

}
