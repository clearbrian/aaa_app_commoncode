//
//	FoursquareContact.swift
//
//	Create by Brian Clear on 8/7/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class FoursquareContact : ParentMappable{

	var email : String?
	var verifiedPhone : Bool?



	override func mapping(map: Map)
	{
		email <- map["email"]
		verifiedPhone <- map["verifiedPhone"]
		
	}
	var description: String
	{
		let description_ = "**** \(type(of: self)) *****\r"
		//description_ = description_ + "email: \(email)\r"
		//description_ = description_ + "verifiedPhone: \(verifiedPhone)\r"
		return description_
	}

}
