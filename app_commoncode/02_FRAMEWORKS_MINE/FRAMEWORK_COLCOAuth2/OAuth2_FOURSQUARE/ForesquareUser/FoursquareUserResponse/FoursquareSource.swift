//
//	FoursquareSource.swift
//
//	Create by Brian Clear on 8/7/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class FoursquareSource : ParentMappable{

	var name : String?
	var url : String?



	override func mapping(map: Map)
	{
		name <- map["name"]
		url <- map["url"]
		
	}
	var description: String
	{
		let description_ = "**** \(type(of: self)) *****\r"
		//description_ = description_ + "name: \(name)\r"
		//description_ = description_ + "url: \(url)\r"
		return description_
	}

}
