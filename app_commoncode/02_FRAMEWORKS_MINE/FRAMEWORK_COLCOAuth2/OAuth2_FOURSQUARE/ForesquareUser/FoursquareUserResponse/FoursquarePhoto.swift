//
//	FoursquarePhoto.swift
//
//	Create by Brian Clear on 8/7/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class FoursquarePhoto : ParentMappable{

	var count : Int?
	var items : [String]?
	var defaultField : Bool?
	var prefix : String?
	var suffix : String?



	override func mapping(map: Map)
	{
		count <- map["count"]
		items <- map["items"]
		defaultField <- map["default"]
		prefix <- map["prefix"]
		suffix <- map["suffix"]
		
	}
	var description: String
	{
		let description_ = "**** \(type(of: self)) *****\r"
		//description_ = description_ + "count: \(count)\r"
		//description_ = description_ + "items: \(items)\r"
		//description_ = description_ + "defaultField: \(defaultField)\r"
		//description_ = description_ + "prefix: \(prefix)\r"
		//description_ = description_ + "suffix: \(suffix)\r"
		return description_
	}

}
