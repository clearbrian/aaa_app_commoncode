//
//  OAuth2UtilsExtensions.swift
//  colcoauth2
//
//  Created by Brian Clear on 05/07/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
//OAuth2PodApp/p2.OAuth2/Sources/Base/OAuth2Base.swift
public typealias OAuth2StringDict = [String: String]

open class OAuth2Utils{
    /**
     Parse a query string into a dictionary of String: String pairs.
     
     If you're retrieving a query or fragment from NSURLComponents, use the `percentEncoded##` variant as the others
     automatically perform percent decoding, potentially messing with your query string.
     
     - parameter query: The query string you want to have parsed
     - returns: A dictionary full of strings with the key-value pairs found in the query
     */
    public final class func paramsFromQuery(_ query: String) -> OAuth2StringDict {
        let parts = query.characters.split() { $0 == "&" }.map() { String($0) }
        var params = OAuth2StringDict(minimumCapacity: parts.count)
        for part in parts {
            let subparts = part.characters.split() { $0 == "=" }.map() { String($0) }
            if 2 == subparts.count {
                params[subparts[0]] = subparts[1].wwwFormURLDecodedString
            }
        }
        return params
    }
}

extension String
{
    fileprivate static var wwwFormURLPlusSpaceCharacterSet: CharacterSet = NSMutableCharacterSet.wwwFormURLPlusSpaceCharacterSet() as CharacterSet
    
    /// Encodes a string to become x-www-form-urlencoded; the space is encoded as plus sign (+).
    var wwwFormURLEncodedString: String {
        let characterSet = String.wwwFormURLPlusSpaceCharacterSet
        return (addingPercentEncoding(withAllowedCharacters: characterSet) ?? "").replacingOccurrences(of: " ", with: "+")
    }
    
    /// Decodes a percent-encoded string and converts the plus sign into a space.
    var wwwFormURLDecodedString: String {
        let rep = replacingOccurrences(of: "+", with: " ")
        
        //SWIFT3
        //return rep.stringByRemovingPercentEncoding ?? rep
        
        return rep.removingPercentEncoding ?? rep
        
    }
}


extension NSMutableCharacterSet
{
    /**
	    Return the character set that does NOT need percent-encoding for x-www-form-urlencoded requests INCLUDING SPACE.
	    YOU are responsible for replacing spaces " " with the plus sign "+".
	    
	    RFC3986 and the W3C spec are not entirely consistent, we're using W3C's spec which says:
	    http://www.w3.org/TR/html5/forms.html#application/x-www-form-urlencoded-encoding-algorithm
     
	    > If the byte is 0x20 (U+0020 SPACE if interpreted as ASCII):
	    > - Replace the byte with a single 0x2B byte ("+" (U+002B) character if interpreted as ASCII).
	    > If the byte is in the range 0x2A (*), 0x2D (-), 0x2E (.), 0x30 to 0x39 (0-9), 0x41 to 0x5A (A-Z), 0x5F (_),
	    > 0x61 to 0x7A (a-z)
	    > - Leave byte as-is
     */
    class func wwwFormURLPlusSpaceCharacterSet() -> NSMutableCharacterSet {
        let set = NSMutableCharacterSet.alphanumeric()
        set.addCharacters(in: "-._* ")
        return set
    }
}
