//
//  OAuth2AccessTokenFoursquareErrorResponse.swift
//  joyride
//
//  Created by Brian Clear on 07/07/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
/*
 Foursquare
 
 WRONG or old code=
 400 - Bad Request
 {
 "error": "invalid_grant"
 }
 
 //wrong clientid or clientsecret
 401 - Bad Request
 
 {
 "error": "invalid_client"
 }
 
 400
 &grant_type=xxauthorization_code
 //should be
 &grant_type=authorization_code
 {
 "error": "unsupported_grant_type"
 }
 
 400
 &redirect_uri=xxjoyrider://oauth/callback_foursquare
 &redirect_uri=joyrider://oauth/callback_foursquare
 {
 "error": "redirect_uri_mismatch"
 }
 */


enum OAuth2AccessTokenFoursquareError : String {
    
    case error = "error"
    
    func nsError() -> NSError{
        
        switch self{
        case .error:
            return NSError.appError(AppError.oAuth2_Foursquare_error)
        }
    }
}
/*
{
    "error": "invalid_grant"
}
*/
class OAuth2AccessTokenFoursquareErrorResponse: ParentMappable {
    
    var error: String?
    
    // root calls mapping()
    //    required init?(_ map: Map){
    //        mapping(map: map)
    //    }
    
    override func mapping(map: Map){
        error <- map["error"]
        
    }
    var description: String
    {
        let description_ = "**** \(type(of: self)) *****\r"
        //description_ = description_ + "error: \(error)\r"
        
        return description_
    }
}
