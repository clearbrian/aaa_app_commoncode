//
//  OAuth2ParentAccessTokenWSRequest.swift
//  joyride
//
//  Created by Brian Clear on 06/07/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
//all api calls that require

class OAuth2ParentAccessTokenWSRequest : OAuth2ParentWSRequest{
    
    
    var access_token : String?
    
    init(access_token : String) {
        super.init()
        
        self.access_token = access_token
        //---------------------------------------------------------------------
        //GET https://api.github.com/user?access_token=...
        //You can pass the token in the query params like shown above, but a cleaner approach is to include it in the Authorization header
        //
        //Authorization: token OAUTH-TOKEN
        //---------------------------------------------------------------------
        self.headers["Authorization"] = "token \(access_token)"
        
    }
}
