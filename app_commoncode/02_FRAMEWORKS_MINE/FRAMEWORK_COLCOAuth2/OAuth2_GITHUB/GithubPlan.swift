//
//	GithubPlan.swift
//
//	Create by Brian Clear on 6/7/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GithubPlan : ParentMappable{
    
    var collaborators : Int?
    var name : String?
    var privateRepos : Int?
    var space : Int?
    
    
    override func mapping(map: Map)
    {
        collaborators <- map["collaborators"]
        name <- map["name"]
        privateRepos <- map["private_repos"]
        space <- map["space"]
        
    }
    var description: String
    {
        let description_ = "**** \(type(of: self)) *****\r"
        //description_ = description_ + "collaborators: \(collaborators)\r"
        //description_ = description_ + "name: \(name)\r"
        //description_ = description_ + "privateRepos: \(privateRepos)\r"
        //description_ = description_ + "space: \(space)\r"
        return description_
    }
    
}
