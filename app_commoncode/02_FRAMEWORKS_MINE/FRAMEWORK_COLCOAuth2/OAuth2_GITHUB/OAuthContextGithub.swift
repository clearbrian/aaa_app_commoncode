//
//  OAuthContextGITHUB.swift
//  joyride
//
//  Created by Brian Clear on 06/07/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
//--------------------------------------------------------------
// MARK: - OAuthContextGITHUB
// MARK: -
//--------------------------------------------------------------

// private var urlStringAuthorize:String = "https://github.com/login/oauth/authorize?client_id=be45feb27af1729e6312&redirect_uri=joyrider://oauth/callback_github&scope=user&state=8E7F9061&allow_signup=false"
//    oauth_client_id=be45feb27af1729e6312
//    &redirect_uri=joyrider://oauth/callback_github
//    &github_scope=user
//    &state=8E7F9061
//    &github_allow_signup=false"

class OAuthContextGithub: OAuthContext{
    
    //---------------------------------------------------------------------
    let oauthAPI: OAuth2API = .Github
    
    //------------------------------------------------
    //Github only settings
    //https://developer.github.com/v3/oauth/#web-application-flow
    //------------------------------------------------
    
    // TODO: - https://developer.github.com/v3/oauth/#scopes
    //"user repo:status"
    var github_scope: String?
    
    var github_allow_signup: String?
    
    //---------------------------------------------------------------------
    //access_token is in here
    var oauth2AccessTokenGithubResponse: OAuth2AccessTokenGithubResponse?
    
    override var access_token: String? {
        var access_token_: String? = nil
        
        if let oauth2AccessTokenGithubResponse = self.oauth2AccessTokenGithubResponse {
            if let access_token = oauth2AccessTokenGithubResponse.access_token {
                access_token_ = access_token
                
            }else{
                logger.error("oauth2AccessTokenGithubResponse.access_token is nil")
            }
            
        }else{
            logger.error("self.oauth2AccessTokenGithubResponse is nil")
        }
        return access_token_
    }
    
    //--------------------------------------------------------------
    // MARK: - init
    // MARK: -
    //--------------------------------------------------------------
    
    init(oauth_redirect_uri  : String,
         oauth_client_id : String,
         oauth_client_secret : String,
         github_scope : String = "user repo:status",
         github_allow_signup : String)
    {
        //---------------------------------------------------------------------
        //pass up non github settings
        super.init(oauth_url_authorize : "https://github.com/login/oauth/authorize",
                   oauth_client_id : oauth_client_id,
                   oauth_client_secret : oauth_client_secret,
                   oauth_redirect_uri : oauth_redirect_uri,
                   oauth_url_request_access_token : "https://github.com/login/oauth/access_token",
                   stateIsRequired: false)
        //---------------------------------------------------------------------
        self.github_scope = github_scope
        self.github_allow_signup = github_allow_signup
        //---------------------------------------------------------------------
    }

    //--------------------------------------------------------------
    // MARK: - urlStringAuthorize
    // MARK: -
    //--------------------------------------------------------------
    override var urlStringAuthorize : String?{
        
        var urlStringAuthorizeReturned : String? = nil
        
        //---------------------------------------------------------------------
        //"https://github.com/login/oauth/authorize?client_id=be45feb27af1729e6312&redirect_uri=joyrider://oauth/callback_github&scope=user&state=8E7F9061&allow_signup=false"
        //---------------------------------------------------------------------
    
        if let urlStringAuthorizeFromSuper = super.urlStringAuthorize{
           
            //String easier to append to
            var urlStringAuthorizeGithub = urlStringAuthorizeFromSuper
           
            
            //---------------------------------------------------------------------
            //CHECK ALL REQUIRED PARAMS
            //---------------------------------------------------------------------
            //&scope=user
            if let github_scope = self.github_scope {
                urlStringAuthorizeGithub = urlStringAuthorizeGithub + "&scope=\(github_scope)"
                
                
                //---------------------------------------------------------------------
                //&state=8E7F9061
                //SPECIAL CASE NOT OPTIONAL btu may be empty - though getter shoud fill it from UUID
                //---------------------------------------------------------------------
                if self.state.isEmpty {
                    //---------------------------------------------------------------------
                    //ERROR
                    //---------------------------------------------------------------------
                    logger.error("self.state.isEmpty is true")
                    
                }else{
                    //---------------------------------------------------------------------
                    //state ok
                    //---------------------------------------------------------------------
                    urlStringAuthorizeGithub = urlStringAuthorizeGithub + "&state=\(state)"
                    
                    //---------------------------------------------------------------------
                    //&allow_signup=false"
                    if let github_allow_signup = self.github_allow_signup {
                        urlStringAuthorizeGithub = urlStringAuthorizeGithub + "&allow_signup=\(github_allow_signup)"
                        
                        //---------------------------------------------------------------------
                        //ALL REQUIRED PARAMS SET return string, else return nil
                        //---------------------------------------------------------------------
                         urlStringAuthorizeReturned = urlStringAuthorizeGithub
                        
                        
                    }else{
                        logger.error("self.oauth_url_authorize is nil")
                    }
                    //---------------------------------------------------------------------
                }
            }else{
                logger.error("github_scope is nil")
            }
        }else{
            logger.error("super.urlStringAuthorize is nil")
        }
        
        //To test copy URL into Safari
        return urlStringAuthorizeReturned
    }
    
    
    override var bodyStringRequestAccessToken : String?{
        
        var bodyStringReturned : String? = nil
        
//        if self.isValid(){
        
            //---------------------------------------------------------------------
            //client_id=be45feb27af1729e6312&client_secret=bbf4bde86a4a3fa193b821cc4a6c610079c306d2&code=5c11a5a28052e8acc149&redirect_uri=joyrider://oauth/callback_github&state=8E7F9061
            //    client_id=be45feb27af1729e6312
            //    &client_secret=bbf4bde86a4a3fa193b821cc4a6c610079c306d2
            //    &code=5c11a5a28052e8acc149
            //    &redirect_uri=joyrider://oauth/callback_github
            //    &state=8E7F9061
            //---------------------------------------------------------------------
            ///super.bodyStringRequestAccessToken returns "" added so its future proof 
            if let bodyStringRequestAccessTokenFromSuper = super.bodyStringRequestAccessToken{
                
                var bodyStringRequestAccessTokenGithub = bodyStringRequestAccessTokenFromSuper
                
                //---------------------------------------------------------------------
                //ALL REQUIRED SO NESTED - IF ANY MISSING THEN ERRORS
                //---------------------------------------------------------------------
                
                //---------------------------------------------------------------------
                //client_id=be45feb27af1729e6312
                //---------------------------------------------------------------------
                if let oauth_client_id = self.oauth_client_id {
                    bodyStringRequestAccessTokenGithub = bodyStringRequestAccessTokenGithub + "client_id=\(oauth_client_id)"
                    
                    //---------------------------------------------------------------------
                    //&client_secret=bbf4bde86a4a3fa193b821cc4a6c610079c306d2
                    //---------------------------------------------------------------------
                    if let oauth_client_secret = self.oauth_client_secret {
                        bodyStringRequestAccessTokenGithub = bodyStringRequestAccessTokenGithub + "&client_secret=\(oauth_client_secret)"
                        
                        //---------------------------------------------------------------------
                        //&code=5c11a5a28052e8acc149
                        //generated by /authorize step
                        //---------------------------------------------------------------------
                        if let oauth_authorize_code = self.oauth_authorize_code {
                            bodyStringRequestAccessTokenGithub = bodyStringRequestAccessTokenGithub + "&code=\(oauth_authorize_code)"
                            
                            //---------------------------------------------------------------------
                            // &redirect_uri=joyrider://oauth/callback_github
                            //---------------------------------------------------------------------
                            if let oauth_redirect_uri = self.oauth_redirect_uri {
                                bodyStringRequestAccessTokenGithub = bodyStringRequestAccessTokenGithub + "&oauth_redirect_uri=\(oauth_redirect_uri)"
                                
                                //---------------------------------------------------------------------
                                //&state=8E7F9061
                                //---------------------------------------------------------------------
                                //should be filled by getter
                                if self.state.isEmpty {
                                    logger.error("self.state.isEmpty is true")
                                    
                                }else{
                                    //State is set
                                    bodyStringRequestAccessTokenGithub = bodyStringRequestAccessTokenGithub + "&state=\(state)"
                                    
                                    //---------------------------------------------------------------------
                                    //ALL REQUIRED PARAMETERS SET
                                    //---------------------------------------------------------------------
                                    bodyStringReturned = bodyStringRequestAccessTokenGithub
                                    //---------------------------------------------------------------------
                                }
                                //---------------------------------------------------------------------
                            }else{
                                logger.error("oauth_redirect_uri is nil")
                            }
                        }else{
                            logger.error("oauth_authorize_code is nil - should be set after step1 /authorize returns with code=")
                        }
                    }else{
                        logger.error("oauth_client_secret is nil")
                    }
                }else{
                    logger.error("oauth_client_id is nil")
                }
            }else{
                logger.error("super.bodyStringRequestAccessToken is nil")
                bodyStringReturned = nil
            }
            
            
//        }else{
//            logger.error("isValid() is false - returning ")
//            bodyStringReturned = nil
//        }
        
        //To test copy URL into Safari
        return bodyStringReturned
    }
    
    
    
    override func buildAccessTokenRequest() -> OAuth2ParentWSRequest?{
        var oauth2ParentWSRequestReturned: OAuth2ParentWSRequest?

        if let oauth_url_request_access_token = self.oauth_url_request_access_token {
            
            //------------------------------------------------
            let oauth2AccessTokenGithubRequest = OAuth2AccessTokenGithubRequest()
            
            oauth2AccessTokenGithubRequest.webServiceEndpoint = oauth_url_request_access_token
            oauth2AccessTokenGithubRequest.query = nil
            oauth2AccessTokenGithubRequest.colcHTTPMethod = .Post
            
            //let stringPost="client_id=be45feb27af1729e6312&client_secret=bbf4bde86a4a3fa193b821cc4a6c610079c306d2&code=853e05b59a1c75bf9b5e&redirect_uri=joyrider://oauth/callback_github&state=8E7F9061"
            //---------------------------------------------------------------------
            if let bodyStringRequestAccessToken = self.bodyStringRequestAccessToken{
                //---------------------------------------------------------------------
                //BODY
                //---------------------------------------------------------------------
                oauth2AccessTokenGithubRequest.bodyString = bodyStringRequestAccessToken
                //---------------------------------------------------------------------
                
                oauth2ParentWSRequestReturned = oauth2AccessTokenGithubRequest
                
                //---------------------------------------------------------------------
            }else{
                logger.error("oAuthContext.bodyStringRequestAccessToken is nil")
            }
        }else{
            logger.error("oAuthContext.oauth_url_request_access_token is nil")
        }
        
        return oauth2ParentWSRequestReturned
    }
    
    
    
    
    override func parseAccessTokenResponse(_ parsedObject: Any,
                                           success: (_ access_token: String) -> Void,
                                           failure: (_ error: Error) -> Void)
    {
        //---------------------------------------------------------------------
        switch parsedObject{
        case let number as NSNumber:
            logger.error("UNEXPECTED JSON RESPONSE: NSNumber:\(number)")
            
        case let array as NSArray:
            logger.error("UNEXPECTED JSON RESPONSE: NSArray:\(array)")
            
        case let nsDictionary as NSDictionary:
            //---------------------------------------------------------------------
            logger.info("NSDictionary:\(nsDictionary)")
            //---------------------------------------------------------------------
            if let _ = nsDictionary["error"] {
                //---------------------------------------------------------------------
                //ERROR DICTIONARY
                //---------------------------------------------------------------------
                //    NSDictionary:{
                //        error = "bad_verification_code";
                //        "error_description" = "The code passed is incorrect or expired.";
                //        "error_uri" = "https://developer.github.com/v3/oauth/#bad-verification-code";
                //    }
                //---------------------------------------------------------------------
                logger.error("Error NSDictionary:\(nsDictionary)")
                
                if let oauth2AccessTokenGithubErrorResponse : OAuth2AccessTokenGithubErrorResponse = Mapper<OAuth2AccessTokenGithubErrorResponse>().map(JSONObject:parsedObject){
                    
                    logger.info("oauth2AccessTokenGithubResponse returned:[\r\(oauth2AccessTokenGithubErrorResponse)]")
                    
                    //---------------------------------------------------------------------
                    //check response status
                    if let error = oauth2AccessTokenGithubErrorResponse.error{
                        switch error{
                            
                        case OAuth2AccessTokenGithubError.bad_verification_code.rawValue:
                            
                            logger.error(OAuth2AccessTokenGithubError.bad_verification_code.rawValue)
                            
                            failure(OAuth2AccessTokenGithubError.bad_verification_code.nsError())
                            
                        default:
                            logger.error("UNHANDLED oauth2AccessTokenGithubErrorResponse: \(oauth2AccessTokenGithubErrorResponse)")
                            
                            failure(NSError.unexpectedResponseObject("UNHANDLED oauth2AccessTokenGithubErrorResponse: \(oauth2AccessTokenGithubErrorResponse)"))
                        }
                    }else{
                        logger.error("oauth2AccessTokenGithubErrorResponse.error is nil")
                        
                        failure(NSError.unexpectedResponseObject("oauth2AccessTokenGithubErrorResponse.error is nil"))
                    }
                    //---------------------------------------------------------------------
                }else{
                    logger.error("clkPlaceSearchResponse is nil or not CLKGooglePlaceSearchResponse")
                }
                
            }else if let _ = nsDictionary["access_token"] {
                //---------------------------------------------------------------------
                //OK: Parsed Dictionary contains "access_token"
                //---------------------------------------------------------------------
                //{"access_token":"5e493186a5c9f4bdbfab1bcf68eb05780731790d","token_type":"bearer","scope":"user"}
                logger.error("OK NSDictionary:\(nsDictionary)")
                
                if let oauth2AccessTokenGithubResponse : OAuth2AccessTokenGithubResponse = Mapper<OAuth2AccessTokenGithubResponse>().map(JSONObject:parsedObject){
                    //------------------------------------------------------------------------------------------------
                    logger.info("oauth2AccessTokenGithubResponse returned:\(oauth2AccessTokenGithubResponse.description)")
                    //------------------------------------------------------------------------------------------------
                    // TODO: - save access_token to keychain using key "com.github"
                    
                    //---------------------------------------------------------------------
                    //STEP 2 - get access_token complete - you may now call any API calls using the token
                    //---------------------------------------------------------------------
                    self.oauth2AccessTokenGithubResponse = oauth2AccessTokenGithubResponse
                    
                    if let access_token = self.access_token {
                        logger.info("access_token returned:\(access_token)")
                        
                        success(access_token)
                        
                    }else{
                        logger.error("self.access_token is nil")
                    }
                }else{
                    logger.error("clkPlaceSearchResponse is nil or not CLKGooglePlaceSearchResponse")
                }
                
                
            }else{
                logger.error("Parsed Object doesnt contain key 'error' or 'access_token' cant map to ParentMappable: NSDictionary:\(nsDictionary)")
            }
        //---------------------------------------------------------------------
        default:
            logger.error("UNEXPECTED JSON RESPONSE: UNKNOWN TYPE")
        }
    }
    
}
