//
//  GithubUserRepoOwner.swift
//  joyride
//
//  Created by Brian Clear on 06/07/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
//
//	GithubUserRepoOwner.swift
//
//	Create by Brian Clear on 6/7/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GithubUserRepoOwner : ParentMappable{
    
    var avatarUrl : String?
    var eventsUrl : String?
    var followersUrl : String?
    var followingUrl : String?
    var gistsUrl : String?
    var gravatarId : String?
    var htmlUrl : String?
    var id : Int?
    var login : String?
    var organizationsUrl : String?
    var receivedEventsUrl : String?
    var reposUrl : String?
    var siteAdmin : Bool?
    var starredUrl : String?
    var subscriptionsUrl : String?
    var type : String?
    var url : String?
    
    
    
    override func mapping(map: Map)
    {
        avatarUrl <- map["avatar_url"]
        eventsUrl <- map["events_url"]
        followersUrl <- map["followers_url"]
        followingUrl <- map["following_url"]
        gistsUrl <- map["gists_url"]
        gravatarId <- map["gravatar_id"]
        htmlUrl <- map["html_url"]
        id <- map["id"]
        login <- map["login"]
        organizationsUrl <- map["organizations_url"]
        receivedEventsUrl <- map["received_events_url"]
        reposUrl <- map["repos_url"]
        siteAdmin <- map["site_admin"]
        starredUrl <- map["starred_url"]
        subscriptionsUrl <- map["subscriptions_url"]
        type <- map["type"]
        url <- map["url"]
        
    }
    var description: String
    {
        let description_ = "**** \(Swift.type(of: self)) *****\r"
        //description_ = description_ + "avatarUrl: \(avatarUrl)\r"
        //description_ = description_ + "eventsUrl: \(eventsUrl)\r"
        //description_ = description_ + "followersUrl: \(followersUrl)\r"
        //description_ = description_ + "followingUrl: \(followingUrl)\r"
        //description_ = description_ + "gistsUrl: \(gistsUrl)\r"
        //description_ = description_ + "gravatarId: \(gravatarId)\r"
        //description_ = description_ + "htmlUrl: \(htmlUrl)\r"
        //description_ = description_ + "id: \(id)\r"
        //description_ = description_ + "login: \(login)\r"
        //description_ = description_ + "organizationsUrl: \(organizationsUrl)\r"
        //description_ = description_ + "receivedEventsUrl: \(receivedEventsUrl)\r"
        //description_ = description_ + "reposUrl: \(reposUrl)\r"
        //description_ = description_ + "siteAdmin: \(siteAdmin)\r"
        //description_ = description_ + "starredUrl: \(starredUrl)\r"
        //description_ = description_ + "subscriptionsUrl: \(subscriptionsUrl)\r"
        //description_ = description_ + "type: \(type)\r"
        //description_ = description_ + "url: \(url)\r"
        return description_
    }
    
}
