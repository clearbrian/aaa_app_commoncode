//
//  COLCOAuth2Manager+Github.swift
//  joyride
//
//  Created by Brian Clear on 06/07/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation

//--------------------------------------------------------------
// MARK: - GITHUB API
// MARK: -
//--------------------------------------------------------------

extension COLCOAuth2Manager{
    
    

    func callApi_GithubUser(
         success: @escaping (_ githubUser : GithubUser) -> Void,
                 failure: @escaping (_ error: Error) -> Void
        )
    {
        
        if let access_token = self.accessTokenForOAuth2API(.Github) {
            
            //---------------------------------------------------------------------
            let githubUserRequest = GithubUserRequest(access_token:access_token)
            
            //DEBUG
            //let githubUserRequest = GithubUserRequest(access_token:"CRAP_TOKEN")
            //---------------------------------------------------------------------
            
            self.callApi_Github(githubUserRequest,
                                responseNSDictionary: { (nsDictionary) in
                                    //---------------------------------------------------------------------
                                    //200 and no key "error"
                                    //---------------------------------------------------------------------
                                    if let githubUser : GithubUser = Mapper<GithubUser>().map(JSONObject:nsDictionary){
                                        //--------------------------------------------------------------------
                                        logger.info("githubUser returned:\(githubUser.description)")
                                        //--------------------------------------------------------------------
                                        //success(githubUser : githubUser)
                                        success(githubUser)
                                        
                                    }else{
                                        logger.error("parsedObject is nil or not GithubUser")
                                    }
                                    //---------------------------------------------------------------------
                },
                                responseNSArray:{ (nsArray: NSArray)->Void in
                                    logger.error("UNEXPECTED NSArray should be single GithubUser")
                },
                                failure: { (error: Error)-> Void in
                                    //failure(error)
                                    failure(error)
 
                })
            //---------------------------------------------------------------------
            
        }else{
            logger.error("self.accessTokenForOAuth2API(.Github) is nil")
        }
    }
    
    
    
    func callApi_GithubUserRepos(
        success: @escaping (_ githubUserRepoArray : [GithubUserRepo]) -> Void,
                failure: @escaping (_ error: Error) -> Void
        )
    {
        
        if let access_token = self.accessTokenForOAuth2API(.Github) {
            
            //---------------------------------------------------------------------
            let githubUserReposRequest = GithubUserReposRequest(access_token:access_token)
            
            //DEBUG
            //let githubUserReposRequest = GithubUserReposRequest(access_token:"CRAP_TOKEN")
            //---------------------------------------------------------------------
            
            self.callApi_Github(githubUserReposRequest,
                                responseNSDictionary: { (nsDictionary) in
                                    logger.error("UNEXPECTED single parsedObject should be array of GithubUserRepo")
                                },
                                responseNSArray:{ (nsArray: NSArray)->Void in
    
                                    //---------------------------------------------------------------------
                                    if let githubUserRepoArray : [GithubUserRepo] = Mapper<GithubUserRepo>().mapArray(JSONObject:nsArray){
                                        //--------------------------------------------------------------------
                                        //logger.info("githubUserRepoArray returned:\(githubUserRepoArray.count)")
                                        //--------------------------------------------------------------------
                                        success(githubUserRepoArray)
                
                                    }else{
                                        logger.error("nsArray is nil or not GithubUserRepo")
                                    }
                                    //---------------------------------------------------------------------
                                },
                                //SWIFT3
                //            failure:{ (parsedObject: Any) -> Void in
                //                            failure(self.parseErrorFoursquare(parsedObject))
                //            }
                
                failure:{ (error: Error) -> Void in
                    failure(error)
                }
            )
            //---------------------------------------------------------------------
        }else{
            logger.error("self.accessTokenForOAuth2API(.Github) is nil")
        }
    }
    
    
    //--------------------------------------------------------------
    // MARK: - CALL GITHUB API - handle common error for all API calls
    // MARK: -
    //--------------------------------------------------------------
    
    //Call any Github API call - handle common errors such as 401 etc
    func callApi_Github(_ oauth2ParentWSRequest: OAuth2ParentWSRequest,
                          responseNSDictionary: @escaping (_ nsDictionary : NSDictionary) -> Void,
                               responseNSArray: @escaping (_ nsArray : NSArray) -> Void,
                                      failure: @escaping (_ error: Error) -> Void
        )
    {
        
        self.callOAuthWS(oauth2ParentWSRequest,
                         responseNSDictionary:{ (nsDictionary: NSDictionary, statusCode: Int) -> Void in
                            //---------------------------------------------------------------------
                            //NSDictionary
                            //---------------------------------------------------------------------
                            if statusCode == 200
                            {
                                //can be 200 but valid json has error message
                                if let _ = nsDictionary["error"]
                                {
                                    failure(self.parseErrorFoursquare(nsDictionary))
                                }
                                else{
                                    responseNSDictionary(nsDictionary)
                                }
                            }
                            else{
                                //---------------------------------------------------------------------
                                //not a 200 - definately an error
                                //---------------------------------------------------------------------
                                //Access token may have expired or been revoked
                                if statusCode == 400
                                {
                                    failure(NSError.appError(AppError.oAuth2_400_access_token_error))
                                }
                                else if statusCode == 401{
                                    failure(NSError.appError(AppError.oAuth2_Github_401_not_authorized))
                                }
                                else{
                                    failure(NSError.errorWithDescription("UNHANDLED Github statusCode:\(statusCode):[\(nsDictionary)]", code: 2342))
                                }
                            }
                            //---------------------------------------------------------------------
                            
                            
            },
                         responseNSArray:{ (nsArray: NSArray, statusCode: Int)->Void in
                            //---------------------------------------------------------------------
                            //NSArray
                            //---------------------------------------------------------------------
                            if statusCode == 200
                            {
                                //if error then would be NSDictionary above
                                responseNSArray(nsArray)
                            }
                            else{
                                //---------------------------------------------------------------------
                                //not a 200 - definately an error
                                //---------------------------------------------------------------------
                                //Access token may have expired or been revoked
                                if statusCode == 400
                                {
                                    failure(NSError.appError(AppError.oAuth2_400_access_token_error))
                                }
                                else if statusCode == 401{
                                    failure(NSError.appError(AppError.oAuth2_Github_401_not_authorized))
                                }
                                else{
                                    failure(NSError.errorWithDescription("UNHANDLED Github statusCode:\(statusCode):[\(nsArray)]", code: 2342))
                                }
                            }
                            //---------------------------------------------------------------------

                            
            },
//SWIFT3
//            failure:{ (parsedObject: Any) -> Void in
//                            failure(self.parseErrorFoursquare(parsedObject))
//            }

            failure:{ (error: Error) -> Void in
                failure(error)
            }
        )
    }

    
    
    func parseErrorGithub(_ parsedObject: Any) -> NSError{
        //---------------------------------------------------------------------
        //ERROR DICTIONARY
        //---------------------------------------------------------------------
        //    NSDictionary:{
        //        error = "bad_verification_code";
        //        "error_description" = "The code passed is incorrect or expired.";
        //        "error_uri" = "https://developer.github.com/v3/oauth/#bad-verification-code";
        //    }
        //---------------------------------------------------------------------
        if let oauth2AccessTokenGithubErrorResponse : OAuth2AccessTokenGithubErrorResponse = Mapper<OAuth2AccessTokenGithubErrorResponse>().map(JSONObject:parsedObject){
            
            logger.info("oauth2AccessTokenGithubResponse returned:[\r\(oauth2AccessTokenGithubErrorResponse)]")
            
            //---------------------------------------------------------------------
            //check response status
            if let error = oauth2AccessTokenGithubErrorResponse.error{
                switch error{
                    
                case OAuth2AccessTokenGithubError.bad_verification_code.rawValue:
                    
                    logger.error(OAuth2AccessTokenGithubError.bad_verification_code.rawValue)
                    return OAuth2AccessTokenGithubError.bad_verification_code.nsError()
                    
                default:
                    logger.error("UNHANDLED oauth2AccessTokenGithubErrorResponse: \(oauth2AccessTokenGithubErrorResponse)")
                    return NSError.unexpectedResponseObject("UNHANDLED oauth2AccessTokenGithubErrorResponse: \(oauth2AccessTokenGithubErrorResponse)")
                }
            }else{
                logger.error("oauth2AccessTokenGithubErrorResponse.error is nil")
                return NSError.unexpectedResponseObject("oauth2AccessTokenGithubErrorResponse.error is nil")
                
            }
            //---------------------------------------------------------------------
        }else{
            logger.error("parsedObject is nil or not OAuth2AccessTokenGithubErrorResponse")
            return NSError.unexpectedResponseObject("parsedObject is nil or not OAuth2AccessTokenGithubErrorResponse")
        }
        //---------------------------------------------------------------------
        //---------------------------------------------------------------------
    }

    
}
