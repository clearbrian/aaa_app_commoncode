//
//  OAuth2GithubAccessTokenBody.swift
//  joyride
//
//  Created by Brian Clear on 06/07/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation


//client_id=be45feb27af1729e6312&client_secret=bbf4bde86a4a3fa193b821cc4a6c610079c306d2&oauth_redirect_uri=joyrider://oauth/callback_github&state=8E7F9061
class OAuth2GithubAccessTokenBody: ParentMappable {
    
    var client_id: String?
    var client_secret: String?
    var oauth_redirect_uri: String?
    var state: String?

    
    // root calls mapping()
    //    required init?(_ map: Map){
    //        mapping(map: map)
    //    }
    
    override func mapping(map: Map){
        client_id <- map["client_id"]
        client_secret <- map["client_secret"]
        oauth_redirect_uri <- map["oauth_redirect_uri"]
        state <- map["state"]
    }
}
