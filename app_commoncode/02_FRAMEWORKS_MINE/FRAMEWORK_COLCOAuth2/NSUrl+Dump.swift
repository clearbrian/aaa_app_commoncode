//
//  NSUrl+Dump.swift
//  colcoauth2
//
//  Created by Brian Clear on 05/07/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
extension URL{
    func dumpProperties() -> String{
        var outputString = ""
        
        outputString = outputString + "---------------------------------------\r"
        outputString = outputString + "\(self)\r"
        outputString = outputString + "---------------------------------------\r"
        outputString = outputString + "          absoluteString:\(safeString(self.absoluteString))\r"
        outputString = outputString + "             absoluteURL:\(self.absoluteURL)\r"
        outputString = outputString + "                 baseURL:\(String(describing: self.baseURL))\r"
        outputString = outputString + "fileSystemRepresentation:\((self as NSURL).fileSystemRepresentation)\r"
        outputString = outputString + "                fragment:\(safeString(self.fragment))\r"
        outputString = outputString + "                    host:\(safeString(self.host))\r"
        outputString = outputString + "       lastPathComponent:\(safeString(self.lastPathComponent))\r"
        outputString = outputString + "         parameterString:\(safeString(self.path))\r"
        outputString = outputString + "                password:\(safeString(self.password))\r"
        outputString = outputString + "                    path:\(safeString(self.path))\r"
        outputString = outputString + "          pathComponents:\(self.pathComponents)\r"
        outputString = outputString + "           pathExtension:\(safeString(self.pathExtension))\r"
        outputString = outputString + "                    port:\(String(describing: (self as NSURL).port))\r"
        outputString = outputString + "                   query:\(safeString(self.query))\r"
        outputString = outputString + "            relativePath:\(safeString(self.relativePath))\r"
        outputString = outputString + "          relativeString:\(safeString(self.relativeString))\r"
        //swift 3 outputString = outputString + "       resourceSpecifier:\(safeString(self.resourceSpecifier))\r"
        outputString = outputString + "                  scheme:\(safeString(self.scheme))\r"
        outputString = outputString + "         standardizedURL:\(self.standardized)\r"
        outputString = outputString + "                    user:\(safeString(self.user))\r"
        outputString = outputString + "---------------------------------------"
        
        return outputString
    }
    
    //output "" if nil
    func safeString(_ str: String?) -> String{
        var safeString_ = ""
        if let str = str {
            safeString_ = str
        }else{
            //dont output nil
        }
        return safeString_
    }
}
