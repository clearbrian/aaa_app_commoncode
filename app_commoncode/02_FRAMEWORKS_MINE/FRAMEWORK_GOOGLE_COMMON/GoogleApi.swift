//
//  GoogleApi.swift
//  MyReminders
//
//  Created by Brian Clear on 22/03/2017.
//  Copyright © 2017 City Of London Consulting. All rights reserved.
//

import Foundation


class GoogleApi{

    //--------------------------------------------------------------
    // MARK: - Google API Languages
    // MARK: -
    //--------------------------------------------------------------
    
    class func find_googleSupportedLanguageCodeForDeviceLanguage() -> String?{
        var googleSupportedLanguageCodeForDeviceLanguage: String?
        
        //NOTE
        //Languages use hyphens en_GB
        //localeIdentifiers use underscore:en_GB
        //let languageArray = Locale.preferredLanguages
        //print("languageArray:\r\(languageArray)")
        //["en-GB", "ko-KR"]
        //I had korean keyboard installed
        
        let locale: Locale = Locale.current
        //mem address : print("NSLocale.currentLocale():\(locale)")
        
        let localeIdentifier = locale.identifier
        print("NSLocale.currentLocale().localeIdentifier:\(localeIdentifier)")
        
        
        //use in api calls
        if let googleSupportedLanguageCodeForDeviceLanguage_ = Locale.deviceLanguageSupportedByGooglePlacesApi() {
            //logger.debug("googleSupportedLanguageCodeForDeviceLanguage_:\(googleSupportedLanguageCodeForDeviceLanguage_)")
            googleSupportedLanguageCodeForDeviceLanguage = googleSupportedLanguageCodeForDeviceLanguage_
            //-----------------------------------------------------------------------------------
            //DEBUG - force device language
            //not really noticable as london businesses would have name in english regardless of whose searching e.g. korean
            //the place info does have info
            //googleSupportedLanguageCodeForDeviceLanguage = "ko-KR"
            //in nearby search Google Address the 'UK' part will bein Korean
            //-----------------------------------------------------------------------------------
        }else{
            logger.error("googleSupportedLanguageCodeForDeviceLanguage is nil")
        }
        return googleSupportedLanguageCodeForDeviceLanguage
    }
    
}
