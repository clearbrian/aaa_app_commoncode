//
//  ContactAuthorizer.swift
//  SharedCode
//
//  Created by Vandad on 7/9/15.
//  Copyright © 2015 Pixolity. All rights reserved.
//

import Foundation
import Contacts



let kContacts_AuthErrorMessage_NotDetermined_NotGranted = "User has refused access to Contacts. If this was in error you may change it in Settings > Privacy > Contacts"

/* The application is not authorized to access contact data.
 *  The user cannot change this application’s status, possibly due to active restrictions such as parental controls being in place. */
let kContacts_AuthErrorMessage_Restricted = "Access to Contacts is restricted. The user cannot change this access, possibly due to restrictions such as parental controls. Please check \rSettings > Privacy > Contacts\ror\rSettings > General > restrictions > Contacts"

/*! The user explicitly denied access to contact data for the application. */
let kContacts_AuthErrorMessage_Denied =  "Access to Contacts is denied. The user has previously denied access.\rYou may change it in Settings > Privacy > Contacts"




//iOS-9-Swift-Programming-Cookbook
//https://github.com/vandadnp/iOS-9-Swift-Programming-Cookbook/tree/128140f55b8432dce1e767ed7f594903f3d3cc45/contacts

@available(iOS 9.0, *)
public final class ContactAuthorizer{
    
    public let cnContactStore = CNContactStore()
    
    
    var currentCNAuthorizationStatus: CNAuthorizationStatus = .notDetermined
    
    public func authorizeContactsWithCompletionHandler(_ completionHandler: @escaping (_ succeeded: Bool, _ errorMessage : String?) -> Void){
        
        let currentCNAuthorizationStatus = CNContactStore.authorizationStatus(for: .contacts)
        
        switch currentCNAuthorizationStatus{
        case .authorized:
            completionHandler(true, nil)
            
        case .notDetermined:
            //first time app runs request permission
            print(".NotDetermined")
            //Request it
//            CNContactStore().requestAccessForEntityType(.Contacts){succeeded, err in
//                
//                if succeeded{
//                    //------------------------------------------------------------------------------------------------
//                    //NotDetermined >> Requested access : User said YES
//                    //------------------------------------------------------------------------------------------------
//
//                    //user granted access in alert
//                    completionHandler(succeeded: true, errorMessage: nil)
//                    
//                }else{
//                    //------------------------------------------------------------------------------------------------
//                    //NotDetermined >> Requested access : User said no
//                    //------------------------------------------------------------------------------------------------
//
//                    logger.error("requestAccessForEntityType FAILED: err:[\(err)]")
//                    
//                    completionHandler(succeeded: false, errorMessage: kContacts_AuthErrorMessage_NotDetermined_NotGranted)
//                    
//                }
//            }
            
            //Request it
            self.requestContactsAccess{succeeded, errorMessageReq in
                
                if succeeded{
                    //------------------------------------------------------------------------------------------------
                    //Calendar ACCESS AUTHORIZED - Show Picker
                    //------------------------------------------------------------------------------------------------
                    //user granted access in alert
                    completionHandler(true, nil)
                    
                } else {
                    //------------------------------------------------------------------------------------------------
                    //Calendar ACCESS NOT AUTHORIZED
                    //------------------------------------------------------------------------------------------------
                    
                    if let errorMessageReq = errorMessageReq{
                        
                        completionHandler(false, errorMessageReq)
                        
                    }else{
                        logger.error("errorMessage is nil but succeeded is false")
                        
                        completionHandler(false, kContacts_AuthErrorMessage_NotDetermined_NotGranted)
                    }
                }
            }
        
        case .restricted:
            //print(".Restricted")
            completionHandler(false, kContacts_AuthErrorMessage_Restricted)
        case .denied:
            //print(".Denied")
            completionHandler(false, kContacts_AuthErrorMessage_Denied)
        @unknown default:
            logger.error("@unknown default:")
        }
    }
    
    //need to do this EVERY time in case access was DENIED then user goes into settings and changes switch to on in Provacy > Contactss or in Restrictions
    func requestContactsAccess(_ completionHandler: @escaping (_ succeeded: Bool,_ errorMessage : String?) -> Void){
        
        CNContactStore().requestAccess(for: .contacts) { (granted: Bool, error: Error?) in
            if (granted) {
                print("permission granted for contacts")
                //completionHandler(succeeded: true, errorMessage: nil)
                completionHandler(true, nil)
                
            }
            else {
                if let error = error{
                    
                    completionHandler(false, error.localizedDescription)
                    
                }else{
                    //---------------------------------------------------------------------
                    logger.error("errorMessage is nil but succeeded is false")
                    //completionHandler(succeeded: false, errorMessage: kContacts_AuthErrorMessage_NotDetermined_NotGranted)
                    //---------------------------------------------------------------------
                    //WHEN User has turned off permission then goes into settings and turns it on you need to request access again
                    //First time its
                    switch self.currentCNAuthorizationStatus{
                    case .authorized:
                        print(".Authorized")
                        completionHandler(true, nil)
                        
                    case .notDetermined:
                        print(".NotDetermined")
                        completionHandler(false, kContacts_AuthErrorMessage_NotDetermined_NotGranted)
                        
                    case .restricted:
                        print(".Restricted")
                        completionHandler(false,  kContacts_AuthErrorMessage_Restricted)
                        
                    case .denied:
                        print(".Denied")
                        completionHandler(false, kContacts_AuthErrorMessage_Denied)
                    @unknown default:
                        logger.error("@unknown default:")
                    }
                    //---------------------------------------------------------------------
                }
            }
        }
        
        
    
        
        
    }
    
    
    
    
}
