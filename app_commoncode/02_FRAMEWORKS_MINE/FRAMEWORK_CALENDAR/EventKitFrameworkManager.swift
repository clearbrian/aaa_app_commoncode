//
//  EventKitFrameworkManager.swift
//  joyride
//
//  Created by Brian Clear on 15/10/2015.
//  Copyright © 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation

import EventKit
import EventKitUI
//--------------------------------------------------------------
// MARK: -
// MARK: - EventKitFrameworkManagerDelegate
//--------------------------------------------------------------

protocol EventKitFrameworkManagerDelegate {
    
    func getEventsReturned(_ eventKitFrameworkManager: EventKitFrameworkManager)
    func getEventsReturnedWithError(_ eventKitFrameworkManager: EventKitFrameworkManager, failedWithErrorMessage: String)
    
}


//--------------------------------------------------------------
// MARK: -
// MARK: - EventKitFrameworkManager
//--------------------------------------------------------------

class EventKitFrameworkManager: NSObject/*,  EKEventPickerDelegate*/{
    
    var delegate: EventKitFrameworkManagerDelegate?
    
    fileprivate let calendarAuthorizer = CalendarAuthorizer()
    

    //DONT REMOVE - needed for geoocde
    fileprivate static let kFormattedAddressKey = "kFormattedAddressKey"
    

    //--------------------------------------------------------------
    // MARK: - PUBLIC
    // MARK: -
    //--------------------------------------------------------------
    var eventsList = [EKEvent](){
        didSet {
            print("DIDSET eventsList.count:\(eventsList.count)")
        }
    }
    let eventStore = EKEventStore()
    var selectedCalendarArray: [EKCalendar]?
    
    //---------------------------------------------------------------------
    //range of calendar events to show
    var startDate = Date()
    var endDate = Date()
    
    override init(){
        super.init()
       
        self.selectedCalendarArray = [self.eventStore.defaultCalendarForNewEvents!]
        configureDateRangeForCalendarSearch()
        
    }
    
    //--------------------------------------------------------------
    // MARK: - getEvents
    // MARK: -
    //--------------------------------------------------------------
    func getEvents(presentingViewController : UIViewController){
        
        
        calendarAuthorizer.authorizeCalendarsWithCompletionHandler{succeeded, errorMessage in
            
            if succeeded{
                //------------------------------------------------------------------------------------------------
                //CALENDAR ACCESS AUTHORIZED
                //------------------------------------------------------------------------------------------------
                if let selectedCalendarArray = self.selectedCalendarArray {
                    if selectedCalendarArray.count > 0{
                        self.changeCalendar(selectedCalendarArray)
                        
                    }else{
                        logger.error("selectedCalendarArray.count > 1 FAILED")
                    }
                    
                }else{
                    logger.error("self.selectedCalendarArray  is nil")
                }
                
            } else {
                //------------------------------------------------------------------------------------------------
                //CONTACT ACCESS NOT AUTHORIZED
                //------------------------------------------------------------------------------------------------
                if let errorMessage = errorMessage{
                    self.delegate_getEventsReturnedWithError(errorMessage)
                    
                }else{
                    logger.error("errorMessage is nil but succeeded is false")
                    self.delegate_getEventsReturnedWithError("AUTH FAILED - UNKNOWN ERROR")
                }
            }
        }
    }


    func changeCalendar(_ newCalendarArray : [EKCalendar]){
        self.selectedCalendarArray = newCalendarArray
        self.eventsList = self.fetchEvents()
        
        self.delegate_getEventsReturned()
        
    }

    
    func fetchEvents() -> [EKEvent]{
        var eventsToReturn = [EKEvent]()
        //---------------------------------------------------------------------
        //    if let predicate: NSPredicate = self.searchPredicate_TodayAndTomorrow(){
        //
        //        events = self.eventStore.eventsMatchingPredicate(predicate)
        //        
        //    }else{
        //        logger.error("self.searchPredicate_TodayAndTomorrow predicate is nil")
        //    }
        //---------------------------------------------------------------------
        //+-10 days form today
        //        if let predicate: NSPredicate = self.searchPredicate_PlusMinusDays(){
        //            
        //            events = self.eventStore.eventsMatchingPredicate(predicate)
        //            
        //        }else{
        //            logger.error("self.searchPredicate_TodayAndTomorrow predicate is nil")
        //        }
        //---------------------------------------------------------------------
        
        
        if let predicate: NSPredicate = self.buildPredicateForDateRange(){
            //---------------------------------------------------------------------
            //return all
            //eventsToReturn = self.eventStore.events(matching: predicate)
            //---------------------------------------------------------------------
            //return only ones with address
            let eventsAllMatchingPredicate = self.eventStore.events(matching: predicate)
            
            for ekEvent: EKEvent in eventsAllMatchingPredicate{
                print(ekEvent)
                
                //if structuredLocation set then location is set to  structuredLocation.title
                if let location = ekEvent.location {
                    if location == ""{
                        logger.error("self.location is '' - skip: [\(ekEvent.title)]")
                    }else{
                        eventsToReturn.append(ekEvent)
                    }
                    
                }else{
                    logger.error("self.location is nil - skip: [\(ekEvent.title)]")
                }

            }
        }else{
            logger.error("self.searchPredicate_TodayAndTomorrow predicate is nil")
        }
        return eventsToReturn
    }
    
    func searchPredicate_TodayAndTomorrow() -> NSPredicate?{
        var predicate: NSPredicate?
        
        let startDate = Date()
        
        var tomorrowDateComponents = DateComponents()
        tomorrowDateComponents.day = 1
        
        let endDate = (Calendar.current as NSCalendar).date(byAdding: tomorrowDateComponents, to: startDate, options:.wrapComponents)
        
        if let selectedCalendarArray = self.selectedCalendarArray {
    
            if let endDate = endDate {
                predicate = self.eventStore.predicateForEvents(withStart: startDate, end: endDate, calendars: selectedCalendarArray)

            }else{
                logger.error("endDate is nil")
            }
            
        }else{
            logger.error("self.selectedCalendar is nil")
        }
        return predicate
    
    }
    
    
//    func searchPredicate_PlusMinusDays() -> NSPredicate?{
//        var predicate: NSPredicate?
//        
//        let nowDate = NSDate()
//        
//        let fromDateComponents = NSDateComponents()
//        let toDateComponents = NSDateComponents()
//        
//        fromDateComponents.day = -10
//        toDateComponents.day = 10
//        
//        //---------------------------------------------------------------------
//        //v1 - dateByAddingComponents
//        //---------------------------------------------------------------------
//        //dont use .WrapComponents. its value is 0 but seems to return no results use NSCalendarOptions(rawValue: 0)
//        if let startDate = NSCalendar.currentCalendar().dateByAddingComponents(fromDateComponents, toDate: nowDate, options:NSCalendarOptions(rawValue: 0)){
//            if let endDate = NSCalendar.currentCalendar().dateByAddingComponents(toDateComponents, toDate: nowDate, options:NSCalendarOptions(rawValue: 0)){
//                //---------------------------------------------------------------------
//                print("nowDate:\(nowDate)")
//                print("startDate:\(startDate)")
//                print("endDate:\(endDate)")
//                
//                //---------------------------------------------------------------------
//                if let selectedCalendar = self.selectedCalendar {
//                    let calendarArray = [selectedCalendar]
//                    
//                    
//                    predicate = self.eventStore.predicateForEventsWithStartDate(startDate, endDate: endDate, calendars: calendarArray)
//                    
//                    
//                }else{
//                    logger.error("self.selectedCalendar is nil")
//                }
//                //---------------------------------------------------------------------
//            }else{
//                logger.error("START: dateByAddingComponents is nil")
//            }
//        }else{
//            logger.error("START: dateByAddingComponents is nil")
//        }
//        
//        //---------------------------------------------------------------------
//        //v2 - had problems with dateByAddingComponents was using options: .WrapComponents worked with options: NSCalendarOptions(rawValue: 0) even though WrapComponents is 0
//        //---------------------------------------------------------------------
//        //    print("nowDate:\(nowDate)")
//        //    //beware options: .WrapComponents may not work
//        //    if let startDate = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: -10, toDate: nowDate, options: NSCalendarOptions(rawValue: 0)){
//        //        if let endDate = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: +10, toDate: nowDate, options: NSCalendarOptions(rawValue: 0)){
//        //            
//        //            print("nowDate:\(nowDate)")
//        //            print("startDate:\(startDate)")
//        //            print("endDate:\(endDate)")
//        //
//        //            //---------------------------------------------------------------------
//        //            if let selectedCalendar = self.selectedCalendar {
//        //                let calendarArray = [selectedCalendar]
//        //                
//        //                
//        //                predicate = self.eventStore.predicateForEventsWithStartDate(startDate, endDate: endDate, calendars: calendarArray)
//        //                
//        //                
//        //            }else{
//        //                logger.error("self.selectedCalendar is nil")
//        //            }
//        //            //---------------------------------------------------------------------
//        //        }else{
//        //            logger.error("START: dateByAddingComponents is nil")
//        //        }
//        //    }else{
//        //        logger.error("START: dateByAddingComponents is nil")
//        //    }
//        //---------------------------------------------------------------------
//
//        return predicate
//        
//    }
    
    //--------------------------------------------------------------
    // MARK: - v3 - configure dates seperate to predicate
    // MARK: -
    //--------------------------------------------------------------

    func configureDateRangeForCalendarSearch(){
        
        //---------------------------------------------------------------------
        //v2 - default to now +10 / -10
        //---------------------------------------------------------------------
        //        let nowDate = NSDate()
        //        
        //        let fromDateComponents = NSDateComponents()
        //        let toDateComponents = NSDateComponents()
        //        
        //        fromDateComponents.day = -10
        //        toDateComponents.day = 10
        //        
        //        //---------------------------------------------------------------------
        //        //v1 - dateByAddingComponents
        //        //---------------------------------------------------------------------
        //        //dont use .WrapComponents. its value is 0 but seems to return no results use NSCalendarOptions(rawValue: 0)
        //        if let startDate = NSCalendar.currentCalendar().dateByAddingComponents(fromDateComponents, toDate: nowDate, options:NSCalendarOptions(rawValue: 0)){
        //            if let endDate = NSCalendar.currentCalendar().dateByAddingComponents(toDateComponents, toDate: nowDate, options:NSCalendarOptions(rawValue: 0)){
        //                //---------------------------------------------------------------------
        //                print("nowDate:\(nowDate)")
        //                print("startDate:\(startDate)")
        //                print("endDate:\(endDate)")
        //                
        //                self.startDate = startDate
        //                self.endDate = endDate
        //                
        //                //---------------------------------------------------------------------
        //            }else{
        //                logger.error("START: dateByAddingComponents is nil")
        //            }
        //        }else{
        //            logger.error("START: dateByAddingComponents is nil")
        //        }
        
        //---------------------------------------------------------------------
        //v2 - default to TODAY's events
        //---------------------------------------------------------------------
        if let startDate = RangeDatePickerController.dateAtMidnight(Date()) {
            self.startDate = startDate
        }else{
            logger.error("startDate: dateAtMidnight is nil")
        }
        let startDatePlus30Days = startDate.addDays(30)
        
        if let endDate = RangeDatePickerController.dateAt2359(startDatePlus30Days) {
            self.endDate = endDate
        }else{
            logger.error("endDate: dateAt1200 is nil")
        }
    }

    //must call configureDateRangeForCalendarSearch() elese startDate and endDate default to same value
    func buildPredicateForDateRange() -> NSPredicate?{
        var predicate: NSPredicate?

        //---------------------------------------------------------------------
        if let selectedCalendarArray = self.selectedCalendarArray {
            print("selectedCalendarArray:\(selectedCalendarArray.count)")
            
            //let  ekCalendar0 = selectedCalendarArray[0]
            //clearbrian@googlemail.com
            //print("ekCalendar0:\(ekCalendar0.title)")
            for ekCalendar in selectedCalendarArray{
                print("EKCalendar:\(ekCalendar.title)")
                
            }
            logger.info("PREDICATE[startDate]:\(self.startDate)")
            logger.info("PREDICATE[endDate  ]:\(self.endDate)")
            
            predicate = self.eventStore.predicateForEvents(withStart: self.startDate, end: self.endDate, calendars: selectedCalendarArray)
          
            logger.info("")
            
        }else{
            logger.error("self.selectedCalendar is nil")
        }
        //---------------------------------------------------------------------
        
        return predicate
        
    }

    //--------------------------------------------------------------
    // MARK: - DELEGATE
    // MARK: -
    //--------------------------------------------------------------

    func delegate_getEventsReturned(){
        
        if let delegate = self.delegate{
            //------------------------------------------------
            delegate.getEventsReturned(self)
            //------------------------------------------------
            
        }else{
            logger.error("delegate is nil")
        }
    }
    
    
    func delegate_getEventsReturnedWithError(_ errorMessage: String){
        
        if let delegate = self.delegate{
            //------------------------------------------------
            //errorIN may be nil
            delegate.getEventsReturnedWithError(self, failedWithErrorMessage: errorMessage)
            //------------------------------------------------
            
        }else{
            logger.error("delegate is nil")
        }
    }
}
