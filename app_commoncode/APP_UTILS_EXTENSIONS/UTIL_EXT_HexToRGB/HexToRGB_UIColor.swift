//
//  HexToRGB.swift
//  kuler_adobecolor_uiappearance
//
//  Created by Brian Clear on 18/08/2015.
//  Copyright (c) 2015 Clarksons. All rights reserved.
//

import Foundation
import UIKit
//http://stackoverflow.com/questions/24263007/how-to-use-hex-colour-values-in-swift-ios

extension UIColor {
    
    //------------------------------------------------------------------------------------------------
    //var color = UIColor(red: 0xFF, green: 0xFF, blue: 0xFF)
    //var color2 = UIColor(netHex:0xFFFFFF)
    //------------------------------------------------------------------------------------------------

    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
    
    
    
    //------------------------------------------------------------------------------------------------
    //You can enter hex strings with either format: #ffffff or ffffff
    //------------------------------------------------------------------------------------------------

    //https://stackoverflow.com/questions/24263007/how-to-use-hex-color-values/27203691#27203691
    //Swift 5
    static func hexStringToUIColor (_ hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
// TODO: - NOT TESTED
        //DEPRECATED var rgbValue:UInt32 = 0
        //DEPRECATED:scanHexInt32 - Scanner(string: cString).scanHexInt32(&rgbValue)
        
        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

