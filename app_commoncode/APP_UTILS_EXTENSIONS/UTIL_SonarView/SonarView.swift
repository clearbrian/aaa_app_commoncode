//
//  SonarView.swift
//  MapLocationAnimation
//
//  Created by Brian Clear on 05/05/2016.
//  Copyright © 2016 Larry Natalicio. All rights reserved.
//

import Foundation
import UIKit

class SonarView: UIView, CAAnimationDelegate {
    
    // MARK: - Types
    
//    struct Constants {
//        struct ColorPalette {
//            static let green = UIColor(red:0.00, green:0.87, blue:0.71, alpha:1.0)
//        }
//    }
    

    
    
    // MARK: - Initializers
    
    //    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
    //        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
    //        startAnimation()
    //    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    // MARK: - NSCoding
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        //        fatalError("init(coder:) has not been implemented")
        
        //wait till view Did Load in container else is in wrong position - think its position in storyboard
        //startAnimation()
    }
//    override func layoutSubviews(){
//        print("layoutSubviews self.frame[\(self.frame)]")
//        print("layoutSubviews self.bounds[\(self.bounds)]")
//        print("layoutSubviews self.center[\(self.center)]")
//        startAnimation()
//    }
    
    // MARK: - Convenience
    
    func sonar(_ beginTime: CFTimeInterval) {
        //wrong - offset not correct
        //print("UIBezierPath self.center[\(self.center)]")
        //ok iphoen 6 plus
        //let centerPoint = CGPoint(x: 100,y: 100)
        let centerPoint = CGPoint(x: self.bounds.size.width / 2.0 , y: self.bounds.size.height / 2.0)
        
        //ANIMATE FROM THIS CIRCLE SIZE....
        // The circle in its smallest size.
        //let circlePath1 = UIBezierPath(arcCenter: self.center, radius: CGFloat(3), startAngle: CGFloat(0), endAngle:CGFloat(M_PI * 2), clockwise: true)
        let circlePath1 = UIBezierPath(arcCenter: centerPoint, radius: CGFloat(3), startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
        //....TO THIS CIRCLE SIZE
        // The circle in its largest size.
        //let circlePath2 = UIBezierPath(arcCenter: self.center, radius: CGFloat(80), startAngle: CGFloat(0), endAngle:CGFloat(M_PI * 2), clockwise: true)
        let circlePath2 = UIBezierPath(arcCenter: centerPoint, radius: CGFloat(80), startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
        
        // Configure the layer.
        let shapeLayer = CAShapeLayer()
        
//        AppearanceManager.appColorTint = UIColor.hexStringToUIColor("#CBFE98")//NEON GREEN
//        //--------------------------------------
//        //423960 from https://dribbble.com/shots/2561642-Player-List
//        AppearanceManager.appColorNavbarAndTabBar = UIColor.hexStringToUIColor("#423960") //PURPLE
        
        
        //shapeLayer.strokeColor = Constants.ColorPalette.green.CGColor
        shapeLayer.strokeColor = AppearanceManager.appColorNavbarAndTabBar.cgColor
        shapeLayer.fillColor = AppearanceManager.appColorTintInternal.cgColor
        
        
        // This is the path that's visible when there'd be no animation.
        shapeLayer.path = circlePath1.cgPath
        self.layer.addSublayer(shapeLayer)
        
        // Animate the path.
        let pathAnimation = CABasicAnimation(keyPath: "path")
        pathAnimation.fromValue = circlePath1.cgPath
        pathAnimation.toValue = circlePath2.cgPath
        
        // Animate the alpha value.
        let alphaAnimation = CABasicAnimation(keyPath: "opacity")
        alphaAnimation.fromValue = 0.8
        alphaAnimation.toValue = 0
        
        // We want both path and alpha animations to run together perfectly, so
        // we put them into an animation group.
//        let animationGroup = CAAnimationGroup()
//        animationGroup.beginTime = beginTime
//        animationGroup.animations = [pathAnimation, alphaAnimation]
//        //animationGroup.duration = 2.76
//        animationGroup.duration = 5.0
//        animationGroup.repeatCount = FLT_MAX
//        animationGroup.delegate = self
//        animationGroup.removedOnCompletion = false
//        animationGroup.fillMode = kCAFillModeForwards
        
        // we put them into an animation group.
        let animationGroup = CAAnimationGroup()
        animationGroup.beginTime = beginTime
        animationGroup.animations = [pathAnimation, alphaAnimation]
        //animationGroup.duration = 2.76
        animationGroup.duration = 1.0
        animationGroup.repeatCount = 1
        animationGroup.delegate = self
        animationGroup.isRemovedOnCompletion = true
        animationGroup.fillMode = CAMediaTimingFillMode.forwards
        
        
        // Add the animation to the layer.
        shapeLayer.add(animationGroup, forKey: "sonar")
        
        // TODO: - replace with /Users/gbxc/Documents/00_ME/00_BITBUCKET_COLC/quartzcode/06_AnimatedBetweenPaths_MorphLetters SonarGreenCircle.qc
    }
    
    func startAnimation() {
        sonar(CACurrentMediaTime())
//        sonar(CACurrentMediaTime() + 0.92)
//        sonar(CACurrentMediaTime() + 1.84)
    }
    
    func stopAnimation() {
        //self.layer.removeAllAnimations()
    }
    
}
