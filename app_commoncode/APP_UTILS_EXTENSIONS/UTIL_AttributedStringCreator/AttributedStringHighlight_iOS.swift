//
//  AttributedStringHighlight_iOS.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 29/03/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
import UIKit

extension NSAttributedString{
    
    /*
     //----------------------------------------------------------------------------------------
    if let nsMutableAttributedString = NSAttributedString.highlightStringInString(wholeString: textLabelText,
                                                                                  highlightString: searchText,
                                                                                  highlightColor: UIColor.appColorCYAN())
    {
        if nsMutableAttributedString.length > 0{
            genericListTableViewCell.labelTitle?.attributedText = nsMutableAttributedString
        }else{
            genericListTableViewCell.labelTitle?.text = tflApiPlace.textLabelText()
        }
    }
     */
    
    class func highlightStringInString(wholeString: String, highlightString: String, highlightColor: UIColor) -> NSMutableAttributedString?{
        
        var nsMutableAttributedString : NSMutableAttributedString? = nil
        
        
        if highlightString.trim() == ""{
            //return nil - use labelTitle?.text = NOT labelTitle?.attributedText
            
        }else{
            do{
                nsMutableAttributedString = NSMutableAttributedString(string: wholeString)
                
                let expression = try  NSRegularExpression(pattern: highlightString, options: .caseInsensitive)
                
                let range = NSRange.init(location: 0, length: wholeString.length)
                
                expression.enumerateMatches(in: wholeString,
                                            options:  [],
                                            range: range,
                                            using: { (result, flags, stop) in
                                                //---------------------------------------------------------
                                                if let substringRange = result?.range(at: 0){
                                                    
                                                    if let nsMutableAttributedString = nsMutableAttributedString {
                                                        nsMutableAttributedString.addAttributes([NSAttributedString.Key.foregroundColor : highlightColor], range: substringRange)
                                                        
                                                    }else{
                                                        logger.error("nsMutableAttributedString is nil")
                                                    }
                                                    
                                                }else{
                                                    logger.error("result?.rangeAt(0) is nil")
                                                }
                                                
                                                //---------------------------------------------------------
                })
            }catch let nserror as NSError {
                print("nserror\(nserror)")
            }
        }
        return nsMutableAttributedString
    }
    
    
    
    
}
