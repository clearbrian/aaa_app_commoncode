//
//  AttributedStringCreator.swift
//  ClarksonsArc
//
//  Created by Brian Clear on 16/06/2016.
//  Copyright © 2016 Clarksons. All rights reserved.
//

import Foundation
import UIKit


class AttributedStringCreator{
    
    //--------------------------------------------------------------
    // MARK: - GENERATE NSAttributedString
    // MARK: -
    //--------------------------------------------------------------
    static func attributedString1(nameString: String, addressString: String) -> NSAttributedString{
        let finalAttributedString = NSMutableAttributedString()
        //---------------------------------------------------------------------
        //func
        //OK      finalAttributedString.appendAttributedString(coloredAttributedString(nameString, foregroundColor: UIColor.blackColor()))
        //        finalAttributedString.appendAttributedString(coloredAttributedString(addressString, foregroundColor: UIColor.purpleColor()))
        //---------------------------------------------------------------------
        //same func as extension to String
        finalAttributedString.append(nameString.coloredAttributedString(UIColor.black))
        finalAttributedString.append(nameString.coloredAttributedString(UIColor.purple))
        finalAttributedString.append("hello".coloredAttributedString(UIColor.green))
        //---------------------------------------------------------------------
        return finalAttributedString
    }
    
    
    static func attributedStringFromArray(_ arrayStringsAndColors: [(String, UIColor)], seperator: String) -> NSAttributedString{
        
        //---------------------------------------------------------------------
        let finalAttributedString = NSMutableAttributedString()
        //---------------------------------------------------------------------
        if arrayStringsAndColors.count > 0{
            //LAST ROW - dont add seperator
            
            for row in 0..<arrayStringsAndColors.count {
                let (stringIn, foregroundColor) = arrayStringsAndColors[row]
                
                //---------------------------------------------------------------------
                var stringOut = "\(stringIn)\(seperator) "
                if row == arrayStringsAndColors.count - 1{
                    //LAST ROW - dont add seperator
                    stringOut = "\(stringIn)"
                }else{
                    //set above
                }
                //---------------------------------------------------------------------
                finalAttributedString.append(stringOut.coloredAttributedString(foregroundColor))
            }
        }
        
        //---------------------------------------------------------------------
        return finalAttributedString
    }
    
    //--------------------------------------------------------------
    // MARK: - COLORED NSAttributedString
    // MARK: -
    //--------------------------------------------------------------
    
    //or use String.coloredAttributedString extension below
    static func coloredAttributedString(_ stringIn: String, foregroundColor: UIColor) -> NSAttributedString{
        
        //---------------------------------------------------------------------
        //SWIFT 3
        //var attributesArray = [String : NSObject]()
        //SWIFT 4
        var attributesArray = [NSAttributedString.Key : Any]()
        attributesArray[NSAttributedString.Key.foregroundColor] = foregroundColor
        //---------------------------------------------------------------------
        let coloredAttributedString = NSAttributedString(string: stringIn, attributes: attributesArray)
        
        return coloredAttributedString
    }
}

//--------------------------------------------------------------
// MARK: - extension String.coloredAttributedString
// MARK: -
//--------------------------------------------------------------
extension String {
    func coloredAttributedString(_ foregroundColor: UIColor) -> NSAttributedString{
        
        //---------------------------------------------------------------------
        //SWIFT 3
        //var attributesArray = [String : NSObject]()
        //SWIFT 4
        var attributesArray = [NSAttributedString.Key : Any]()
        attributesArray[NSAttributedString.Key.foregroundColor] = foregroundColor
        //---------------------------------------------------------------------
        let coloredAttributedString = NSAttributedString(string: self, attributes: attributesArray)
        
        return coloredAttributedString
    }
}

//--------------------------------------------------------------
// MARK: - EXAMPLE USAGE
// MARK: -
//--------------------------------------------------------------
/*
 
 
 class AttributedStringViewController: UIViewController  {
 
 @IBOutlet weak var textFieldName: UITextField!
 
 @IBOutlet weak var textFieldAddress: UITextField!
 
 
 @IBOutlet weak var textViewAttributed: UITextView!
 @IBOutlet weak var textViewUnAttributed: UITextView!
 
 override func viewDidLoad() {
 super.viewDidLoad()
 
 self.textFieldName.text = "더웰예인치과의원"
 self.textFieldAddress.text = "32 Euljiro 1(il)-ga, Jung-gu, Seoul, South Korea"
 
 
 if let nameString = self.textFieldName.text {
 if let addressString = self.textFieldAddress.text {
 
 self.textViewUnAttributed.text = "\(nameString), \(addressString)"
 //---------------------------------------------------------------------
 //OK self.textViewAttributed.attributedText = self.attributedString2(nameString:nameString, addressString:addressString)
 //---------------------------------------------------------------------
 //OK let arrayStringsAndColors: [(String, UIColor)]  = []
 //---------------------------------------------------------------------
 let arrayStringsAndColors: [(String, UIColor)]
 = [
 (nameString   , UIColor.blackColor()),
 (addressString, UIColor.purpleColor()),
 ("ASIA", UIColor.redColor())
 ]
 //---------------------------------------------------------------------
 self.textViewAttributed.attributedText = AttributedStringCreator.attributedStringFromArray(arrayStringsAndColors, seperator: ",")
 //---------------------------------------------------------------------
 
 }else{
 logger.error("self.textFieldName.text is nil")
 }
 }else{
 logger.error("self.textFieldName.text is nil")
 }
 }
 }
 */
