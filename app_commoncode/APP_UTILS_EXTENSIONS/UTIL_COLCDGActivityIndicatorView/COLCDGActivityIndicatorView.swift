//
//  COLCDGActivityIndicatorView.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 18/01/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
import UIKit
import DGActivityIndicatorView

public class COLCDGActivityIndicatorView: DGActivityIndicatorView {
    
    public init() {
        //default
        let frame = CGRect(x: 0, y: 0, width: 100.0, height: 100.0)
        //designated init - so must pass default frame even in init
        super.init(frame: frame)
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    //from IB
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    func setupView(){
        //wehn you drag UIView from IB it will be white - this should turn it red so you know this code has run
        backgroundColor = .clear
        //self.tintColor = AppearanceManager.appColorDark0
        self.tintColor = AppearanceManager.appColorLight0
//        self.layer.borderColor = UIColor.black.cgColor
//        self.layer.borderWidth = 1.0
        setupLoadingRandom()
    }
    //---------------------------------------------------------
    //LOADING ANIMATION - DBActivityIndicator
    //---------------------------------------------------------
    private func setupLoadingRandom(){
        //---------------------------------------------------------------------
        //Random Loader
        //---------------------------------------------------------------------
        
        //default but may be changed in
        //OK but eli only wanted on1 - 
        //self.type = chooseRandomAnimationType()
        
        self.type = .nineDots
    
        // TODO: - COMMENT
        //self.tintColor = UIColor.appColorYELLOW()
        
        //requires else it can draw out of view bounds
        self.size = self.frame.size.width
        
        //DEBUG
        //dgActivityIndicatorView.layer.borderColor = UIColor.cyan.cgColor
        //dgActivityIndicatorView.layer.borderWidth = 1.0
        //----------------------------------------------------------------------------------------
        
        showLoadingOnMain()
    }
    
    private func chooseRandomAnimationType() -> DGActivityIndicatorAnimationType{

        var randomType_ = DGActivityIndicatorAnimationType.ballSpinFadeLoader
        let randomNVActivityIndicatorTypeRawInt = Int(arc4random_uniform(UInt32(DGActivityIndicatorAnimationType.ballSpinFadeLoader.rawValue)))

        if let randomAnimationType = DGActivityIndicatorAnimationType(rawValue: UInt(randomNVActivityIndicatorTypeRawInt)){
            
            //BUG:pack man seend to be outside the box
            if randomAnimationType == .cookieTerminator{
                randomType_ = .nineDots
            }else{
                randomType_ = randomAnimationType
            }
            
        }else{
            logger.error("randomAnimationType is nil")
        }
        return randomType_
    }
    
    //--------------------------------------------------------------
    // MARK: - SHOW HIDE
    // MARK: -
    //--------------------------------------------------------------
    func showLoadingOnMain(){
        if Thread.isMainThread{
            showLoading()
        }else{
            DispatchQueue.main.async{
                self.showLoading()
            }
        }
    }
    fileprivate func showLoading(){
        //self.type = self.chooseRandomAnimationType()
        
        self.isHidden = false
        self.startAnimating()
    }
    
    func hideLoadingOnMain(){
        if Thread.isMainThread{
            hideLoading()
        }else{
            DispatchQueue.main.async{
                self.hideLoading()
            }
        }
    }
    fileprivate func hideLoading(){
        self.isHidden = true
        self.stopAnimating()

    }
    
    
    
}
