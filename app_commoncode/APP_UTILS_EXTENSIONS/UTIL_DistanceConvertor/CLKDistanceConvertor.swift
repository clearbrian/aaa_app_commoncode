//
//  DistanceConvertor.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 30/01/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

class CLKDistanceConvertor{
    
    
    //----------------------------------------------------------------------------------------
    //meters to Miles
    //----------------------------------------------------------------------------------------
    //http://stackoverflow.com/questions/22395917/change-from-km-to-miles-in-cllocationdistance
    //----------------------------------------------------------------------------------------
    //https://www.google.co.uk/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8#q=what+is+35422+meters+in+miles
    class func metersToMiles(distanceValueMeters: NSNumber?) -> NSNumber?{
        var distMilesNumber_ : NSNumber? = nil
        if let distanceValueMeters: NSNumber = distanceValueMeters {
            //logger.debug("distance[meters]:\(distanceValueMeters)")
            //logger.debug("distance[kms   ]:\(distanceValueMeters.doubleValue/1000)")
            
            
            //---------------------------------------------------------
            
            let distMilesDouble = distanceValueMeters.doubleValue * 0.000621371
            
            //---------------------------------------------------------
            //distance[meters]:35422  * 0.000621371 >>distance[miles]:22.01
            distMilesNumber_ = NSNumber(value: distMilesDouble)
            //logger.debug("distance[miles]:\(distMilesDouble)")
            //---------------------------------------------------------
            
        }else{
            logger.error("distance.text is nil")
        }
        return distMilesNumber_
    }
    
    class func metersToMilesString2DecPlaces(distanceValueMeters: NSNumber?) -> String?{
        
        var distMilesString_ : String? = nil
        
        if let distMilesNumber_ = CLKDistanceConvertor.metersToMiles(distanceValueMeters: distanceValueMeters) {
            
            let formatter = NumberFormatter()
            formatter.numberStyle = .decimal
            formatter.minimumFractionDigits = 2
            formatter.maximumFractionDigits = 2
            
            if let distMiles2DecimalPlacesString = formatter.string(from: distMilesNumber_){
                logger.debug("distance[miles]:\(distMiles2DecimalPlacesString)")
                
                distMilesString_ = distMiles2DecimalPlacesString
                
            }else{
                logger.error("distMiles2DecimalPlacesString is nil - could not format: distMilesNumber:\(distMilesNumber_)")
            }
            //----------------------------------------------------------------------------------------
            
        }else{
            logger.error("distance.text is nil")
        }
        return distMilesString_
        
    }
    
//    class func metersToMiles(distanceMeters: Double) -> Double{
//        
//        if distanceMeters > 1000.0{
//            //KM
//            let distInKM = distanceMeters/1000.0
//            let distInKM_2DecimalPlacesString = COLCFormatter.formatToTwoDecimalPlaces(doubleValue: distInKM)
//            
//            return "\(distInKM_2DecimalPlacesString) km"
//            
//        }else{
//            //METERS
//            let distanceToCurrentLocation2DecimalPlaces = COLCFormatter.formatToTwoDecimalPlaces(doubleValue: distanceMeters)
//            return  "\(distanceToCurrentLocation2DecimalPlaces) m"
//        }
//    }
    
    
//    class func metersToKMString(distanceMeters: Double) -> String{
//        
//        if distanceMeters > 1000.0{
//            //KM
//            let distInKM = distanceMeters/1000.0
//            let distInKM_2DecimalPlacesString = COLCFormatter.formatToTwoDecimalPlaces(doubleValue: distInKM)
//            
//            return "\(distInKM_2DecimalPlacesString) km"
//            
//        }else{
//            //METERS
//            let distanceToCurrentLocation2DecimalPlaces = COLCFormatter.formatToTwoDecimalPlaces(doubleValue: distanceMeters)
//            return  "\(distanceToCurrentLocation2DecimalPlaces) m"
//        }
//    }
}
