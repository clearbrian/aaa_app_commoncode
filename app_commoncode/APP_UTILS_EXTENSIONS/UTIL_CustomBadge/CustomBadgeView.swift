//
//  CustomBadgeView.swift
//  joyride
//
//  Created by Brian Clear on 19/08/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation


import UIKit

//NSUInteger
enum BadgeStyleFontType : Int {
    case badgeStyleFontTypeHelveticaNeueMedium = 0
    case badgeStyleFontTypeHelveticaNeueLight = 1

}
//enum BadgeStyleFontType : Int {
//    case BadgeStyleFontTypeHelveticaNeueMedium = 0
//    case BadgeStyleFontTypeHelveticaNeueLight = 1
//    
//}



class CustomBadgeView: UIView {
    
    
    var badgeFont : UIFont?
    
    var badgeText : NSString?
    var badgeCornerRoundness : CGFloat
    var badgeScaleFactor : CGFloat
    //var badgeStyle : BadgeStyle
    
    override init(frame: CGRect) {
        
        self.badgeCornerRoundness = 5.0
        self.badgeScaleFactor = 1.0
        self.badgeText = "99"
         self.badgeFont = UIFont.systemFont(ofSize: 10.0)
        
        super.init(frame: frame)
        
    }
    
    //Added to IB
    required init?(coder aDecoder: NSCoder) {
        self.badgeCornerRoundness = 5.0
        self.badgeScaleFactor = 1.0
        self.badgeText = "99"
        
        self.badgeFont = UIFont.systemFont(ofSize: 10.0)
        super.init(coder: aDecoder)
        
    }
    func setup(){
        
    }
    
    override func draw(_ rect: CGRect) {
        
        //------------------------------------------------
        //DRAW CIRCLE
        //------------------------------------------------
        if let ctx : CGContext = UIGraphicsGetCurrentContext(){
            drawRoundedRectWithContext(ctx, rect:rect)
        }else{
            print("ERROR: UIGraphicsGetCurrentContext() is nil")
        }
    }
    
    func drawRoundedRectWithContext(_ context: CGContext, rect: CGRect)
    {
        
        context.saveGState();
    
//        var radius : CGFloat = CGRectGetMaxY(rect)*self.badgeCornerRoundness
//        var puffer : CGFloat = CGRectGetMaxY(rect)*0.10;
//        var maxX : CGFloat = CGRectGetMaxX(rect) - puffer;
//        var maxY : CGFloat = CGRectGetMaxY(rect) - puffer;
//        var minX : CGFloat = CGRectGetMinX(rect) + puffer;
//        var minY : CGFloat = CGRectGetMinY(rect) + puffer;
        
        context.beginPath();
        
        // TODO: - COMMENT
        //CGContextSetFillColorWithColor(context, self.badgeStyle.badgeInsetColor.CGColor());
        context.setFillColor(UIColor.appColorCYAN().cgColor)
        
        
//        
//        CGContextAddArc(context, maxX-radius, minY+radius, radius, M_PI+(M_PI/2), 0, 0);
//        CGContextAddArc(context, maxX-radius, maxY-radius, radius, 0, M_PI/2, 0);
//        CGContextAddArc(context, minX+radius, maxY-radius, radius, M_PI/2, M_PI, 0);
//        CGContextAddArc(context, minX+radius, minY+radius, radius, M_PI, M_PI+M_PI/2, 0);
        // TODO: - <#COMMENT#>
//        if (self.badgeStyle.badgeShadow) {
//            
//        CGContextSetShadowWithColor(context, CGSizeMake(1.0,1.0), 3, [[UIColor blackColor] CGColor]);
//        }
        context.fillPath();
        
        context.restoreGState();
    
    }
    
    
    
    static func customBadgeWithString(_ badgeString : NSString) -> CustomBadgeView{
        let customBadgeView = CustomBadgeView()
        //return [[self alloc] initWithString:badgeString withScale:1.0 withStyle:[BadgeStyle defaultStyle]];
        // TODO: - <#COMMENT#>
        return customBadgeView
    }

}
