//
//  StringJS.swift
//  Swift extension of String class that implements most of JavaScript String functions
//
//  Created by Oleg Berman on 2014-07-05.
//  Copyright (c) 2014 Oleg Berman. All rights reserved.
//
//  https://github.com/olegberman/StringJS
//

import Foundation

extension String {
    
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - HANDLE MULTIPLE WILDCARDS as = on keyboard takes too many presses
    //--------------------------------------------------------------
    
    //var s1 = "EMMA MAERSK"
    //logger.debug("s1:\(s1.removeWildCards())")
    //var s2 = "EMMA MAERSK?"
    //logger.debug("s2:\(s2.removeWildCards())")
    //var s3 = "=EMMA MAERSK*"
    //logger.debug("s3:\(s3.removeWildCards())")
    //var s4 = "=EMMA=MAERSK?=*"
    //logger.debug("s4:\(s4.removeWildCards())")
    //[bg255,255,255;[fg140,105,251;[Info_4] s1:EMMA MAERSK [;
    //[bg255,255,255;[fg140,105,251;[Info_4] s2:EMMA MAERSK= [;
    //[bg255,255,255;[fg140,105,251;[Info_4] s3:=EMMA MAERSK= [;
    //[bg255,255,255;[fg140,105,251;[Info_4] s4:=EMMA=MAERSK=== [;
    public func removeWildCards() -> String {
        
        //"==?*?*EMMA*=**?"  << EDGE CASE BUT should work rather than returning 401 query error
        let Name1 = self.replacingOccurrences(of: "?", with: "=", options: NSString.CompareOptions.caseInsensitive, range: nil)
        
        //"===*=*EMMA*=**="
        let Name2 = Name1.replacingOccurrences(of: "*", with: "=", options: NSString.CompareOptions.caseInsensitive, range: nil)

        //repeat a few times to reduc it down
        //"======EMMA====="
        let Name3 = Name2.replacingOccurrences(of: "==", with: "=", options: NSString.CompareOptions.caseInsensitive, range: nil)
        let Name4 = Name3.replacingOccurrences(of: "==", with: "=", options: NSString.CompareOptions.caseInsensitive, range: nil)
        let Name5 = Name4.replacingOccurrences(of: "==", with: "=", options: NSString.CompareOptions.caseInsensitive, range: nil)
        
        //"=EMMA="
        return Name5
    }

    public func removeTrailingComma() -> String {
        
//        var stringIN :String = "N,T,"
//        var stringOUT = ""
//        
//        if stringIN.hasSuffix(","){
//            var lengthIN_ = stringIN.length
//            stringOUT = stringIN.substring(0, end: lengthIN_-1)
//            
//        }else{
//            stringOUT = stringIN
//        }
//        
//        logger.debug("stringIN:\(stringIN)")
//        logger.debug("stringOUT:\(stringOUT)")
//        logger.debug()
        
        
        
        
        var stringOUT = ""
        
        if self.hasSuffix(","){
            let lengthIN_ = self.length
            stringOUT = self.substring(0, end: lengthIN_-1)
            
        }else{
            stringOUT = self
        }
        return stringOUT
    }
    
    public var length: Int {
            return (self as NSString).length
    }
//    
//    public func charAt(index: Int) -> String {
//        return String(Array(self)[index])
//    }
//    
//    public func charCodeAt(index: Int) -> Int {
//        return Int((self as NSString).characterAtIndex(index))
//    }
//    
//    public func concat(strings: String...) -> String {
//        var out = self
//        for str in strings {
//            out += str
//        }
//        return out
//    }
//    
//    public func fromCharCode() -> String {
//        return String(NSString(format: "%c", self))
//    }
//    
//    public func indexOf(string: String, startFrom: Int = 0) -> Int? {
//        var str = self
//        if startFrom != 0 {
//            str = str.substring(startFrom, end: self.length)
//        }
//        var index = (str as NSString).rangeOfString(string).location
//        if index == NSNotFound {
//            return nil
//        }
//        return index + startFrom
//    }
//    
//    public func lastIndexOf(string: String) -> Int? {
//        
//        if var index:Int? = self.indexOf(string) {
//            var lastIndex:Int? = index!
//            
//            while (index != nil) {
//                if var loopIndex:Int? = self.indexOf(string, startFrom: index! + string.length) {
//                    index = loopIndex
//                    lastIndex = loopIndex
//                } else {
//                    index = nil
//                }
//            }
//            return lastIndex
//        } else {
//            return nil
//        }
//    }
//    
//    public func localeCompare() {
//        
//    }
//    
//    public func match(pattern:String) -> Array<String>? {
//        var regex = NSRegularExpression(pattern: pattern, options: nil, error: nil)
//        var matches:[AnyObject] = regex!.matchesInString(self, options: nil, range: NSRange(location: 0, length: self.length))
//        if matches.count > 0 {
//            var result:[String] = []
//            for match:NSTextCheckingResult in matches as! [NSTextCheckingResult] {
//                result.append(self.substr(match.range.location, length: match.range.length))
//            }
//            return result
//        } else {
//            return nil
//        }
//    }
//SWIFT1.2
//    public func replace(what:String, with:String) -> String {
//        var exp = NSRegularExpression(pattern: what, options: nil, error: nil)
//        return exp!.stringByReplacingMatchesInString(self, options: nil, range: NSMakeRange(0, self.length), withTemplate: with)
//    }
//SWIFT2.0
    public func replace(_ what:String, with:String) -> String {
        var stringReturned = what
        
//        let exp = try! NSRegularExpression(pattern: what, options: .CaseInsensitive)
//        return exp.stringByReplacingMatchesInString(self, options: [], range: NSMakeRange(0, self.length), withTemplate: with)

        do {
            let regularExpression = try NSRegularExpression(pattern: what, options: .caseInsensitive)
            stringReturned = regularExpression.stringByReplacingMatches(in: self, options: [], range: NSMakeRange(0, self.length), withTemplate: with)
        }
        catch
        {
            print("Error [StringJS] dataWithJSONObject failed")
        }
        
        return stringReturned
    }
    
    public func search(_ what:String) -> Int? {
        
        var matchReturned: Int? = nil
        
        //---------------------------------------------------------------------
        //    //var exp = NSRegularExpression(pattern: what, options: nil, error: nil)
        //    let exp = try! NSRegularExpression(pattern: what, options: .CaseInsensitive)
        //
        //    //var match = exp!.firstMatchInString(self, options: nil, range: NSMakeRange(0, self.length))
        //    let match = exp.firstMatchInString(self, options: [], range: NSMakeRange(0, self.length))
        //    return match!.range.location
        //---------------------------------------------------------------------

        do {
            let exp = try NSRegularExpression(pattern: what, options: .caseInsensitive)
            
            let match = exp.firstMatch(in: self, options: [], range: NSMakeRange(0, self.length))
            matchReturned = match!.range.location
        }
        catch
        {
            print("Error [StringJS] dataWithJSONObject failed")
        }
        
        return matchReturned
    }

//    public func splice(start: Int) -> String? {
//        return self.substring(start, end: self.length)
//    }
//    
//    public func splice(start: Int, end: Int) -> String? {
//        return self.substring(start, end: end)
//    }
    
//    public func split(_ withWhat:String) -> NSArray {
//
//        return (self as NSString).components(separatedBy: withWhat)
//    }
    public func split(_ withWhat:String) -> [String] {
        
        return (self as NSString).components(separatedBy: withWhat)
    }
    
    //BCpublic func substr(var index: Int) -> String {
    public func substr(_ index: Int) -> String {
        var index_ = index
        if index_ < 0 {
            index_ = self.length - abs(index)
        }
        //return (self as NSString).substringWithRange(NSMakeRange(index, self.length))
        //BC
        return (self as NSString).substring(with: NSMakeRange(index_, self.length-1))
    }
    
    public func substr(_ start: Int, length: Int) -> String {
        var start_ = start
        var length_ = length
        length_ = abs(length)
        if start_ < 0 {
            start_ = self.length - abs(start_)
        }
        return (self as NSString).substring(with: NSMakeRange(start_, length_))
    }
    
    //    var Built = ""
    //    var sub = Built.substring(0, end: 4)
    //    Built = "0"
    //    sub = Built.substring(0, end: 4)
    //    Built = "01"
    //    sub = Built.substring(0, end: 4)
    //    Built = "012"
    //    sub = Built.substring(0, end: 4)
    //    Built = "0123"
    //    sub = Built.substring(0, end: 4)
    //    Built = "01234"
    //    sub = Built.substring(0, end: 4)
    //    Built = "012345"
    //    sub = Built.substring(0, end: 4)
    public func substring( _ start: Int, end: Int) -> String {
        var start_ = start
        var end_ = end
        if start_ < 0 { start_ = 0 }
        if end_ < 0 { end_ = 0 }
        if start_ > end_ {
            let _start_ = start_
            start_ = end_
            end_ = _start_
        }
        end_ = end_ - start_
        let len_ = self.length
        if end_ > len_{
            end_ = len_
        }
        return (self as NSString).substring(with: NSMakeRange(start_, end_))
    }
    

//    public func toLocaleLowerCase() -> String {
//        return self.lowercaseStringWithLocale(NSLocale.currentLocale())
//    }
//    
//    public func toLocaleUpperCase() -> String {
//        return self.uppercaseStringWithLocale(NSLocale.currentLocale())
//    }
//    
//    public func toLowerCase() -> String {
//        return self.lowercaseString
//    }
//    
//    public func toUpperCase() -> String {
//        return self.uppercaseString
//    }
//    
//    public func trim() -> String {
//        var tmp = self
//        while tmp.substr(0, length: 1) == " " {
//            tmp = tmp.substring(1, end: tmp.length)
//        }
//        while tmp.substr(0, length: 1) ==  " " {
//            tmp = tmp.substring(0, end: tmp.length - 1)
//        }
//        return tmp
//    }
    
    func trim() -> String {
        //swift3
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
        
    }
    
}
