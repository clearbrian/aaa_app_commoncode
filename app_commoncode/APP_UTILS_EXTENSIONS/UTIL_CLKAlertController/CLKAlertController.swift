//
//  CLKAlertController.swift
//  SandPforiOS
//
//  Created by Brian Clear on 09/06/2015.
//  Copyright (c) 2015 CLARKSONS RESEARCH SERVICES LIMITED. All rights reserved.
//

import Foundation
import UIKit

protocol Alertable {
    var alertTitle: String {get}
    var alertMessage: String {get}
}

extension UIAlertAction{
    //--------------------------------------------------------------
    // MARK: - FACTORY - Basic UIAlertActions
    // MARK: -
    //--------------------------------------------------------------
    
    
    // TODO: - the VC has the handler code
    //    class var alertActionOK : UIAlertAction{
    //        let alertActionOK = UIAlertAction(title: "OK", style: .default) { (alertAction) -> Void in
    //            print("OK pressed")
    //        }
    //        
    //        return alertActionOK
    //    }
    //----------------------------------------------------------------------------------------

    
    class var alertActionCancel : UIAlertAction{
        let alertActionCancel = UIAlertAction(title: "Cancel", style: .cancel) { (alertAction) -> Void in
            //print("Cancel pressed")
        }
        
        return alertActionCancel
    }
    //----------------------------------------------------------------------------------------
    //    class var alertActionYes : UIAlertAction{
    //        let alertActionYes = UIAlertAction(title: "Yes", style: .default) { (alertAction) -> Void in
    //            print("Yes pressed")
    //        }
    //        
    //        return alertActionYes
    //    }
    //----------------------------------------------------------------------------------------
    //NO
    //----------------------------------------------------------------------------------------
    class var alertActionNo : UIAlertAction{
        let alertActionNo = UIAlertAction(title: "No", style: .cancel) { (alertAction) -> Void in
            print("No pressed")
        }
        
        return alertActionNo
    }
    //----------------------------------------------------------------------------------------
}

open class CLKAlertController {
    

    //--------------------------------------------------------------
    // MARK: - Pass UIAlertActions in array
    // MARK: -
    //--------------------------------------------------------------

    open class func showAlertWithActions(vc: UIViewController, title: String , message: String, alertActions:[UIAlertAction]){
        CLKAlertController.showWithActionsInVC(vc, title: title, message: message, fallbackStyle: .alert, alertActions: alertActions)
    }
    open class func showSheetWithActions(vc: UIViewController, title: String , message: String, alertActions:[UIAlertAction]){
        CLKAlertController.showWithActionsInVC(vc, title: title, message: message, fallbackStyle: .actionSheet, alertActions: alertActions)
    }
    
    open class func showWithActionsInVC(_ vc: UIViewController, title: String , message: String, fallbackStyle: UIAlertController.Style, alertActions:[UIAlertAction]){
        
        let actualStyle: UIAlertController.Style = CLKAlertController.alertControllerStyleForIdiom(preferredStyle: fallbackStyle)

        let alertController = UIAlertController(title:title , message: message, preferredStyle: actualStyle)
    
        for alertAction in alertActions{
            alertController.addAction(alertAction)
        }
        
         vc.present(alertController, animated: true, completion: nil)
        
       
        
        //this is just for the buttons and cancel not the title and message - dont make it red it looks like errors
        //alertController.view.tintColor = AppearanceManager.appColorNavbarAndTabBar
        //alertController.view.tintColor = AppearanceManager.appColorTintExternal
        //alertController.view.tintColor = UIColor.appColorCYAN()
        //alertController.view.tintColor = AppearanceManager.appColorLight2
        //IF not set this will use app windown.tintColor - but oNLY to tint the button - not the title
        //can do with sttributedString but may be hidden
        //http://stackoverflow.com/questions/26460706/uialertcontroller-custom-font-size-color
        
        
        //----------------------------------------------------------------------------------------
        //
        //----------------------------------------------------------------------------------------
//        let fontAwesomeHeart = "H"
//        //let fontAwesomeFont = UIFont(name: "FontAwesome", size: 17)!
//        let customTitle:NSString = "TAXI RANKS" // Use NSString, which lets you call rangeOfString()
//        let systemBoldAttributes:[String : AnyObject] = [
//            // setting the attributed title wipes out the default bold font,
//            // so we need to reconstruct it.
//            NSFontAttributeName : UIFont.boldSystemFont(ofSize: 17)
//        ]
//        let attributedString = NSMutableAttributedString(string: customTitle as String, attributes:systemBoldAttributes)
//        let fontAwesomeAttributes = [
//            NSFontAttributeName: "Arial",
//            NSForegroundColorAttributeName : UIColor.red
//            ] as [String : Any]
//        let matchRange = customTitle.range(of: fontAwesomeHeart)
//        attributedString.addAttributes(fontAwesomeAttributes, range: matchRange)
//        alertController.setValue(attributedString, forKey: "attributedTitle")
//        
//        vc.present(alertController, animated: true, completion: nil)
    }
    
    //--------------------------------------------------------------
    // MARK: - IDIOM check - sheet on ipad requires popup context - fall back to alert
    // MARK: -
    //--------------------------------------------------------------
    
    //for iPhone - choose .alert/.actionSheet returns same
    //for iPad   - choose .alert/.actionSheet always returns .alert as sheet require popup context
    open class func alertControllerStyleForIdiom(preferredStyle: UIAlertController.Style) -> UIAlertController.Style{
        
        //default - if iPhone use the one the user wants
        var returnedStyle: UIAlertController.Style = preferredStyle
        
        //if iPad always replace it with alert
        if UIDevice.current.userInterfaceIdiom == .pad {
            returnedStyle = .alert
        }else{
            //iphone/appletv etc.
        }
        
        return returnedStyle
    }

    
    //alerts in libraries -
    open class func showAlertInRootViewController(title: String?, message: String?){
        if let window = appDelegate.window {
            if let rootViewController = window.rootViewController {
                CLKAlertController.showAlertInVC(rootViewController, title: title, message: message)
            }else{
                logger.error("window.rootViewController is nil - cant show error:[\(String(describing: title))][\(String(describing: message))]")
            }
        }else{
            logger.error("appDelegate.window is nil")
        }
    }
    
    
    open class func showAlertInVC(_ vc: UIViewController, title: String?, message: String?){
        //not 
        DispatchQueue.main.async{
            
            let alertController = UIAlertController(title:title , message: message, preferredStyle: UIAlertController.Style.alert)
            
            
            //must have at least one action or you get an Alert you can close
            let alertAction = UIAlertAction(title: "Ok", style: .cancel) { (alertAction) -> Void in
                //print("Ok tapped = alert closed")
            }
            alertController.addAction(alertAction)
            
            vc.present(alertController, animated: true, completion: nil)
            //------------------------------------------------
            //tint color too bright and alert is white
            //------------------------------------------------
            //if app tint color is bright hard to see
            //from stackoverlfow - to fix issue with tint too pale - set it AFTER you present the alert
            //alertController.view.tintColor = UIColor.blackColor()
            //gmsCircle.strokeColor = UIColor.appBaseColor()
            alertController.view.tintColor = AppearanceManager.appColorNavbarAndTabBar
            //-----------------------------------------------------------------------------------
        }
    }
    
    open class func showAlertInVCWithCallback(_ vc:UIViewController, title: String?, message: String?, handler: (() -> Void)!){
        //not
        DispatchQueue.main.async{
            
            let alertController = UIAlertController(title:title , message: message, preferredStyle: UIAlertController.Style.alert)
            
            //must have at least one action or you get an Alert you can close
            let alertAction = UIAlertAction(title: "Ok", style: .cancel) { (alertAction) -> Void in
                //print("Ok tapped = alert closed")
                
                handler()
            }
            alertController.addAction(alertAction)
            
            vc.present(alertController, animated: true, completion: nil)
            //------------------------------------------------
            //tint color too bright and alert is white
            //------------------------------------------------
            //if app tint color is bright hard to see
            //from stackoverlfow - to fix issue with tint too pale - set it AFTER you present the alert
            //alertController.view.tintColor = UIColor.blackColor()
            //gmsCircle.strokeColor = UIColor.appBaseColor()
            alertController.view.tintColor = AppearanceManager.appColorNavbarAndTabBar
            //-----------------------------------------------------------------------------------
        }
    }
    
    //Cancel does nothing
    open class func showAlertInVCWithCancelAndOkHandler(_ vc:UIViewController,
                                                          title: String,
                                                          buttonTitleDefault: String = "Ok",
                                                          buttonTitleCancel: String = "Cancel",
                                                          message: String?,
                                                          okActionHandler: (() -> Void)!)
    {
        //not
        DispatchQueue.main.async{
            
            let alertController = UIAlertController(title:title , message: message, preferredStyle: UIAlertController.Style.alert)
            //-----------------------------------------------------------------------------------
            //must have at least one action or you get an Alert you can close
            let alertAction = UIAlertAction(title: buttonTitleDefault, style: .default) { (alertAction) -> Void in
                
                okActionHandler()
                
            }
            alertController.addAction(alertAction)
            //-----------------------------------------------------------------------------------

            let alertActionCancel = UIAlertAction(title: buttonTitleCancel, style: .cancel) { (alertAction) -> Void in
                logger.debug("User pressed cancel - just hide alert")
            }
            alertController.addAction(alertActionCancel)
            //-----------------------------------------------------------------------------------

            vc.present(alertController, animated: true, completion: nil)
            //------------------------------------------------
            //tint color too bright and alert is white
            //------------------------------------------------
            //if app tint color is bright hard to see
            //from stackoverlfow - to fix issue with tint too pale - set it AFTER you present the alert
            //alertController.view.tintColor = UIColor.blackColor()
            //gmsCircle.strokeColor = UIColor.appBaseColor()
            alertController.view.tintColor = AppearanceManager.appColorNavbarAndTabBar
            //alertController.view.tintColor = UIColor.redColor()
            //-----------------------------------------------------------------------------------
        }
    }
//    public class func showActionSheetInVCWithCallback(vc:UIViewController, title: String?, message: String?, handler: (() -> Void)!){
//        //not
//        dispatch_async(dispatch_get_main_queue()){
//            
//            let alertController = UIAlertController(title:title , message: message, preferredStyle: UIAlertController.Style.ActionSheet)
//            
//            //must have at least one action or you get an Alert you can close
//            let alertAction = UIAlertAction(title: "Ok", style: UIAlertController.Style.Cancel) { (alertAction) -> Void in
//                //print("Ok tapped = alert closed")
//                
//                handler()
//            }
//            alertController.addAction(alertAction)
//            
//            vc.presentViewController(alertController, animated: true, completion: nil)
//            //------------------------------------------------
//            //tint color too bright and alert is white
//            //------------------------------------------------
//            //if app tint color is bright hard to see
//            //from stackoverlfow - to fix issue with tint too pale - set it AFTER you present the alert
//            //alertController.view.tintColor = UIColor.blackColor()
//            //gmsCircle.strokeColor = UIColor.appBaseColor()
//            alertController.view.tintColor = AppearanceManager.appColorNavbarAndTabBar
//            //-----------------------------------------------------------------------------------
//        }
//    }
    
    
    //--------------------------------------------------------------
    // MARK: - ALERT
    //--------------------------------------------------------------
    static func showOKAlert(_ title: String, message: String, inPresentingViewController viewController: UIViewController){
        
        let alertController = UIAlertController(title:title , message: message, preferredStyle: UIAlertController.Style.alert)
        
        //must have at least one action or you get an Alert you can close
        let alertAction = UIAlertAction(title: "Ok", style: .cancel) { (alertAction) -> Void in
            //print("Ok tapped = alert closed")
        }
        alertController.addAction(alertAction)
        
        viewController.present(alertController, animated: true, completion: nil)
    }

}

