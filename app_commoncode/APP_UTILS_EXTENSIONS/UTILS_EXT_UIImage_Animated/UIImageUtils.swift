//
//  UIImageUtils.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 18/01/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
import UIKit
extension UIImage{
    
    func animatedImageTest() -> UIImage?{
        var animatedImageReturned : UIImage? = nil
        
        let imageNames = ["loading_1", "loading_2", "loading_3"]
        if let animatedImage = UIImage.animatedImageWithNames(imageNames, duration: 3.0){
            animatedImageReturned = animatedImage
        }else{
            logger.error("animatedImageWithNames is nil")
        }
        return animatedImageReturned
    }
    
    //--------------------------------------------------------------
    // MARK: - animatedImageWithNames
    // MARK: -
    //--------------------------------------------------------------
    //UIImage.animatedImageWithNames(["loading_1", "loading_2", "loading_3"], duration: 3.0)
    class func animatedImageWithNames(_ imageNames:[String], duration: TimeInterval) -> UIImage?{
        var animatedImageReturned : UIImage? = nil
        
        if let images = UIImage.arrayFromImageNames(imageNames){
            animatedImageReturned = UIImage.animatedImage(with: images, duration: 3.0)
        }else{
            logger.error("arrayFromImageNames is nil")
        }
        return animatedImageReturned
    }
    
    //if image matching name is found it will be added to array
    //if non found the returns nil
    class func arrayFromImageNames(_ imageNames: [String]) -> [UIImage]?{
        var images : [UIImage]? = nil
        
        
        for imageName in imageNames{
            if let imageFound = UIImage(named: imageName){
                //----------------------------------------------------------------------------------------
                //initialise only if at least one image found
                if images == nil{
                    images = [UIImage]()
                }
                //----------------------------------------------------------------------------------------
                images?.append(imageFound)
                
            }else{
                logger.error("imageName:\(imageName) not found")
            }
        }
        return images
    }
}
