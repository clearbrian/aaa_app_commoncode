//
//  UIFont+CustomFonts.swift
//  test_customfontrobottogooglemaps
//
//  Created by Brian Clear on 22/02/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit


//--------------------------------------------------------------
// MARK: -
// MARK: - ANY UIVIEW SUBCLASS THAT TAKES a font
//--------------------------------------------------------------

extension UIView{
    
    
    // TODO: - fix this need
    func applyCustomFontToApplicableSubViews(){
        //logger.debug("============= applyCustomFontToApplicableSubViews - iterate over subviews =====================")
        
        //traverse view hierarchy
        applyCustomFontToView(self)
        
    }
    func applyCustomFontToView(_ viewIN:UIView){
        //logger.debug("============= applyCustomFontToApplicableSubViews - iterate over subviews =====================")
        
        //let mirror_viewIN = Mirror(reflecting: viewIN)
        //logger.debug("CURRENT VIEW:\(mirror_viewIN.subjectType)")
        
        
        //call appropriate extension methods
        for subView in viewIN.subviews{
            
            //let mirror_subView = Mirror(reflecting: subView)
            //logger.debug("subview:\(mirror_subView.subjectType)")
            
            switch subView{
            case let label as UILabel:
                label.applyCustomFontForCurrentTextStyle()
            case let textField as UITextField:
                textField.applyCustomFontForCurrentTextStyle()
                
            case let textView as UITextView:
                textView.applyCustomFontForCurrentTextStyle()
            default:
                //recurse
                applyCustomFontToView(subView)
            }
        }
    }
}


extension UISegmentedControl{
    
     func applyCustomFontForCurrentTextStyle(){
        var currentFont : UIFont?
        var customFont : UIFont?
        
        
        if let attributes = titleTextAttributes(for: UIControl.State()){
            
            if let currentFont_ = attributes[NSAttributedString.Key.font] as? UIFont {
                //logger.error("currentFont:\(currentFont_)")
                //<UICTFont: 0x12ceefa10> font-family: "AvenirNext-Regular"; font-weight: normal; font-style: normal; font-size: 13.00pt
                currentFont = currentFont_
                
            }else {
                logger.error("attributes[NSFontAttributeName] is nil or not UIFont")
            }
 
        }else{
            //logger.debug("no NSFontAttributeName set - no current font - so change it to whatever you want")
        }
        
        
        //titleTextAttributesForState may also be nil so keep this outside above if..
        if let currentFont = currentFont {
            //current font is set in IB
            //this can failed for tabbed swipe
            if let titleLabelFontCustom = UIFont.applyCustomFontReplacingCurrentFont(currentFont){
                customFont = titleLabelFontCustom
                //logger.error("customFont:\(customFont)")
                
            }else{
                //find default for Body
                
                //------------------------------------------------------------------------------------------------
                //CHANGE STYLE TO PICK DIFFERENT FONT SIZE.....
                //------------------------------------------------------------------------------------------------

                let customFontName = UIFont.lookupCustomFontNameForTextStyle(UIFont.TextStyle.headline)
                if customFontName == "" {
                    logger.error("lookupCustomFontNameForTextStyle failed")
                }else{

                    //Default segmented control label size is 13.0
                    let titleLabelFontCustom = UIFont(name:customFontName, size:13.0)
                    
                    customFont = titleLabelFontCustom
                }
            }
            
        }else{
            //-----------------------------------------------------------------------------------
            //set to any to check its converted to custom
            //let titleLabelFont = UIFont(name:"Helvetica", size:13.0)
            //let titleLabelFontCustom = UIFont.applyCustomFontReplacingCurrentFont(titleLabelFont!)
      
            //-----------------------------------------------------------------------------------
            let customFontName = UIFont.lookupCustomFontNameForTextStyle(UIFont.TextStyle.body)
            if customFontName == "" {
                logger.error("lookupCustomFontNameForTextStyle failed")
            }else{
                
                //Default segmented control label size is 13.0
                let titleLabelFontCustom = UIFont(name:customFontName, size:13.0)
                
                customFont = titleLabelFontCustom
            }
            //-----------------------------------------------------------------------------------
            //forces Dynamic Text to be handled
            self.invalidateIntrinsicContentSize()
            //-----------------------------------------------------------------------------------
        }
        //------------------------------------------------
        //FONT COLOR
        //------------------------------------------------
        if let customFont = customFont {
            //------------------------------------------------------------------------------------------------
            //NORMAL FONT AND COLOR
            //------------------------------------------------------------------------------------------------

            self.setTitleTextAttributes([NSAttributedString.Key.font: customFont], for: UIControl.State())
            
            //THIS SET FONT COLOR FOR SEGMENTED CONTROL - NORMAL

//THE UNSELECTED COLOR OF TAB TEXT FOR ALL TABS EXCEPT THE SELECTED ONE
            //see PLACEPICKER_TABCOLOR : UNSELECTED COLOR WHEN YOU TAP ON TAB
            self.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: AppearanceManager.appColorLight0], for: UIControl.State())
            //self.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.whiteColor()], forState: .Normal)
            
            
            //------------------------------------------------------------------------------------------------
            //SELECTED FONT AND COLOR
            //------------------------------------------------------------------------------------------------

//done else where  - see appColorLight_SelectedColor
            self.setTitleTextAttributes([NSAttributedString.Key.font: customFont], for: .selected)
        
            //dont use on home screen the whole control is tint
//THIS SET FONT COLOR FOR SEGMENTED CONTROL - SELECTED
//WRONG NOT DONE HERE            self.setTitleTextAttributes([NSForegroundColorAttributeName: AppearanceManager.appColorNavbarAndTabBar], forState: .Selected)
            //self.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.whiteColor()], forState: .Selected)
            
            
            //------------------------------------------------------------------------------------------------
            //note -  self.colcCarbonTabSwipeNavigationViewControllerInputs?.carbonSegmentedControl hardcoded to white in LocationPickerViewController
            //cos its transparent
            //------------------------------------------------------------------------------------------------

        }else{
             logger.error("UISegmentedControl applyCustomFontForCurrentTextStyle failed - customFont is nil")
        }
    }
}



extension UIButton {
    func applyCustomFontForCurrentTextStyle(){
        
        //logger.debug("============= applyCustomFontForCurrentTextStyle: UIButton START tag[\(self.tag)]=============")
        
        if let titleLabel = self.titleLabel {
            if let titleLabelFont = titleLabel.font{
                //font:<UICTFont: 0x15097f530> font-family: ".SFUIText-Regular"; font-weight: normal; font-style: normal; font-size: 15.00pt
                //HEadline
                //font:<UICTFont: 0x1355c55e0> font-family: ".SFUIText-Semibold"; font-weight: bold; font-style: normal; font-size: 17.00pt
                
                //print("titleLabelFont:\(titleLabelFont)")
                
                if let font = UIFont.applyCustomFontReplacingCurrentFont(titleLabelFont){
                    self.titleLabel?.font = font
                    //forces Dynamic Text to be handled
                    self.invalidateIntrinsicContentSize()
                }else{
                    logger.error("UILabel.applyCustomFontReplacingCurrentFont FAILED")
                }
                
            }else{
                logger.error("buttonFont not found")
            }
        }else{
            logger.error("UIButton.titleLabel is nil")
        }
        
        

        //logger.debug("============= applyCustomFontForCurrentTextStyle: UILabel END tag[\(self.tag)]=============")
    }
}

extension UILabel {
    func applyCustomFontForCurrentTextStyle(){
        
        //logger.debug("======[\(self.text)]======= applyCustomFontForCurrentTextStyle: UILabel START tag[\(self.tag)]=============")
        if let font = UIFont.applyCustomFontReplacingCurrentFont(self.font){
            self.font = font
            //forces Dynamic Text to be handled
            self.invalidateIntrinsicContentSize()
        }else{
            logger.error("UILabel.applyCustomFontReplacingCurrentFont FAILED")
        }
        //logger.debug("============= applyCustomFontForCurrentTextStyle: UILabel END tag[\(self.tag)]=============")
    }
}
extension UITextField{
    func applyCustomFontForCurrentTextStyle(){
        //logger.debug("============= applyCustomFontForCurrentTextStyle: UITextField START tag[\(self.tag)]=============")
        //font is optional in UITextField
        if let currentFont = self.font {
            if let font = UIFont.applyCustomFontReplacingCurrentFont(currentFont){
                self.font = font
                //forces Dynamic Text to be handled
                self.invalidateIntrinsicContentSize()
            }else{
                logger.error("UITextField.applyCustomFontReplacingCurrentFont FAILED")
            }
        }else{
            logger.error("self.font is not set")
        }
        //logger.debug("============= applyCustomFontForCurrentTextStyle: UITextField END tag[\(self.tag)]=============")
    }
}
extension UITextView{
    func applyCustomFontForCurrentTextStyle(){
        //logger.debug("============= applyCustomFontForCurrentTextStyle: UITextView START tag[\(self.tag)]=============")
        if let currentFont = self.font {
            if let font = UIFont.applyCustomFontReplacingCurrentFont(currentFont){
                self.font = font
                //forces Dynamic Text to be resized on next view layout
                self.invalidateIntrinsicContentSize()
            }else{
                logger.error("UITextView.applyCustomFontReplacingCurrentFont FAILED")
            }
        }else{
            logger.error("self.font is not set")
        }
        //logger.debug("============= applyCustomFontForCurrentTextStyle: UITextView END tag[\(self.tag)]=============")
        
    }
}


//--------------------------------------------------------------
// MARK: -
// MARK: - UIFont + Custom Fonts
//--------------------------------------------------------------

extension UIFont{
    
    class func applyCustomFontReplacingCurrentFont(_ currentFont : UIFont) -> UIFont?{
        var fontReturned : UIFont?
        //To get the TextStyle for a text view
        //Will either have a TextStyle or a Font Name
        //------------------------------------------------------------------------------------------------
        //Control with font set to Text Style : Body
        //["NSCTFontUIUsageAttribute": UICTFontTextStyleBody, "NSFontSizeAttribute": 17]
        
        //Control with Font Name set in IB
        //["NSFontNameAttribute": HelveticaNeue, "NSFontSizeAttribute": 27]
        //------------------------------------------------------------------------------------------------
        
        let fontAttributes = currentFont.fontDescriptor.fontAttributes
        //logger.debug("fontAttributes:\(fontAttributes)")
        
        for (key, anyObject) in fontAttributes{
            
            if key.rawValue == "NSCTFontUIUsageAttribute"{
                //print("SWIFT3:\(anyObject)")
                
                if let styleString = anyObject as? String{
                    //logger.debug("[NSCTFontUIUsageAttribute] LABEL is using TextStyle:\(style)")
                    
                    //user can change the size of the font for a Text Style in Settings > Accessibility > General > Larger Fonts
                    //UIApplication.sharedApplication().preferredContentSizeCategory:UICTContentSizeCategoryL
                    //Get the current prefered size for a TextStyle
                    //e.g. Body
                    //its always the system font name but size varies e.g. San Francisco
                    //We take the fontSize but use our custom font name
                    
                    //let fontTextStyle: UIFont.TextStyle = UIFont.TextStyle.fontTextStyleForName(name: styleString)
                    
                    let fontTextStyle: UIFont.TextStyle = UIFont.TextStyle(rawValue:styleString)
                    
                    if let font = UIFont.changeCustomFontForTextStyle(fontTextStyle:fontTextStyle){
                        fontReturned = font
                        //MUST DO  else wont resize - forces Dynamic Text to be handled
                        ///self.invalidateIntrinsicContentSize()
                    }else{
                        logger.error(" changeCustomFontForTextStyle failed for[\(fontTextStyle.rawValue)]")
                    }
                }else{
                    print(Mirror(reflecting: anyObject))
                    logger.debug("LABEL is using TextStyle:<Error:NOT String>:\(anyObject)")
                }

            }else if key.rawValue == "NSFontNameAttribute"{
                    //["NSFontNameAttribute": HelveticaNeue, "NSFontSizeAttribute": 27]
                
                    //logger.debug("[NSFontNameAttribute] LABEL is using a font name set in IB:\r\(currentFont) - try to find nearest TextStyle")
                    
                    //user can change the size of the font for a Text Style in Settings > Accessibility > General > Larger Fonts
                    //UIApplication.sharedApplication().preferredContentSizeCategory:UICTContentSizeCategoryL
                    //Get the current prefered size for a TextStyle
                    //e.g. Body
                    //its always the system font name but size varies e.g. San Francisco
                    //We take the fontSize but use our custom font name
                    
                    let nearestTextStyle = UIFont.replaceHardcodedFontNameWithTextStyle(currentFont)
                    //logger.debug("nearestTextStyle\(nearestTextStyle)")

                    if let font = UIFont.changeCustomFontForTextStyle(fontTextStyle:nearestTextStyle){
                        fontReturned = font
                        //MUST DO  else wont resize - forces Dynamic Text to be handled
                        ///self.invalidateIntrinsicContentSize()
                    }else{
                        logger.error("changeCustomFontForTextStyle(\(nearestTextStyle)) FAILED")
                    }
            }else{
                
            }
        }
        
        return fontReturned
    }
    
    
    //Check if font exists else returns nil
    class func fontWithCustomName(_ customFontName : String, pointSize: CGFloat) -> UIFont?{
        
        var fontWithCustomName : UIFont?
        
        if UIFont.findFontInSystem(customFontName){
            //Create a custom font with the same size
            //---------------------------------------------------------------------
            // TODO: - cleanup
            //exampled used UIDescriptor but not sure its needed
            //newFont = UIFont.fontUsingDescriptor(customFontName, pointSize: preferredSystemFontForTextStyle.pointSize)
            //---------------------------------------------------------------------
            fontWithCustomName = UIFont(name: customFontName, size:pointSize)
        }else{
            logger.error("[findFontInSystem] FAILED - Custom Font Not Installed:\(customFontName)")
            logger.error("[findFontInSystem] FAILED - checking Info.plist - UIAppFonts?")
            
            if UIFont.findFontInInfoPlistUIAppFonts(customFontName){
                logger.error("FONT NAME [\(customFontName)] NOT FOUND IN System but was found in UIAppFonts in Info.plist - is font file ok")
                
            }else{
                logger.error("FONT NAME [\(customFontName)] NOT FOUND IN System AND NOT found in UIAppFonts in Info.plist - Please add full font name to Info.plist under UIAppFonts: e.g. '\(customFontName).ttf' or file extension e.g. otf ttf")
            }
            
        }
        return fontWithCustomName
    }
    
    //UICTFontTextStyleBody
    class func changeCustomFontForTextStyle(fontTextStyle: UIFont.TextStyle) -> UIFont?{
        var newFont : UIFont?
        

//        //------------------------------------------------------------------------------------------------
//        //User can grow or shrink fonts in Accessibility
//        //Find the system font iOS will use for a specific Text Style e.g. title
//        //Usually San Franciso and a font size
//        //We take the font size and then get the custom font name for the same style and create a new UIFont with that size
//        //Basically we replace a font with
//        //---------------------------------------------------------------------
//        //let preferredSystemFontForTextStyle = UIFont.preferredFont(forTextStyle: UIFont.TextStyle(rawValue: style))
//        //swift3

        let preferredSystemFontForTextStyle: UIFont = UIFont.preferredFont(forTextStyle: fontTextStyle)
        //let preferredSystemFontForTextStyle = UIFont.preferredFont(forTextStyle: .body)

        //<UICTFont: 0x7f9631658a00> font-family: ".SFUIDisplay-Regular"; font-weight: normal; font-style: normal; font-size: 53.00pt
        //------------------------------------------------------------------------------------------------
        //iOS9 - i thought you could just drag any ttf into the project no need to ass UIAPPFonts to Info.plist done when ipa built by XCode
        //but didnt always work - so make sure your font is in UIAppFonts in Info.plist
        
        let customFontName = UIFont.lookupCustomFontNameForTextStyle(fontTextStyle)
        //logger.debug("lookupCustomFontNameForTextStyle['\(style)'] RETURNED [\(customFontName)] - CHECK IF FONT INSTALLED?")
        
        if let customFont = UIFont.fontWithCustomName(customFontName, pointSize:preferredSystemFontForTextStyle.pointSize) {
            newFont = customFont
        }else{
            logger.error(" fontWithCustomName(\(customFontName)) FAILED - FONT NOT INSTALLED - CANT APPLY CUSTOM FONT - check name of font in Lookup method matches UIAppFonts and font filename ")
        }
        
        return newFont
    }
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - CUSTOM FONTS
    //--------------------------------------------------------------
    
    /*
        Lookup for mapping TextStyle to custom font
        App wide lookup to replace any UILabel/UITextView/UITextArea font with custom font
        Font should be a Text Style e.g. Body/Title1 etc
        Font size should only be set by user in Accessibility
    
    
        To find out what fonts are installed use:
        UIFont.dumpFontsInApp()
        then run in Simulator or Device
    
        TO INCLUDE a CUSTOM FONT
        Drag ttf into project
        Change
        UIAppFonts in If.plist to full font file name "Chalkduster.ttf"
        Then change fontName = "Chalkduster" below
        run UIFont.dumpFontsInApp() in app delegate to check new font installed
    
        To support Dynamic Text
        - never set height constraint for any control with Dynamic Text
        - never set specific Font Name/Size in IB
        - only choose Text Style - Body. Title etc
        - set only Margins AROUND a control and BETWEEN all controls and containing view
        - never set a height constraint!! on a control with Text Style applied it will not grow if user changes size in Accessability
    
    */
    class func lookupCustomFontNameForTextStyle(_ style: UIFont.TextStyle) -> String{
        
        
        //IF FONT DOESNT LOAD -
        //Set all to "Chalkduster" so you can see if this code is working
        //Then change string to your custom font to see if it changes
        
        //GoogleMaps cocoapod has Roboto fonts
        //I manually had to drag the fonts from the Resour
        ///Drag in to XCode
        //..../joyride/Pods/GoogleMaps/Frameworks/GoogleMaps.framework/Versions/A/Resources/GoogleMaps.bundle/GMSCoreResources.bundle/Roboto-Regular.ttf
        
        // then I added the FUll font name to Info.plist >> UIAppFonts
        
        //then in her
        //SET TO TTF FILE NAME minus the ttf
        //fontName = "Roboto" - didnt work had to use
        //fontName = "Roboto-Regular"
        
        //Ugly font so we know something is wrong
        var fontName = ""
        
        //------------------------------------------------------------------------------------------------
        //TEST 1 - comment this in and all other switch{..} out
        //Then call on any label
        // cell.textLabel?.applyCustomFontForCurrentTextStyle()
        
        //Label must have a text STYLE set in IB not a font e.g. Body, Caption1 etc NOT Helvetica 13
        //------------------------------------------------------------------------------------------------

     
        //------------------------------------------------------------------------------------------------
        //TEST 3 - FNAL
        //------------------------------------------------------------------------------------------------
        switch style{
        case UIFont.TextStyle.title1:
            fontName = "AvenirNext-DemiBold" //LARGEST
            
        case UIFont.TextStyle.title2:
            fontName = "AvenirNext-Medium" //
            
        case UIFont.TextStyle.title3:
            fontName = "AvenirNext-Regular" //
            
        case UIFont.TextStyle.headline:
            fontName = "AvenirNext-DemiBold" // same height as body just Bold
            
        case UIFont.TextStyle.subheadline:
            fontName = "AvenirNext-DemiBold"// smaller than Headline Body
            
        case UIFont.TextStyle.body:
            fontName = "AvenirNext-Medium"  //non bold version of Body
            
        case UIFont.TextStyle.callout:
            fontName = "AvenirNext-Regular" //smaller than Body / larger than Subhead
            
        case UIFont.TextStyle.footnote:
            fontName = "AvenirNext-Italic" //smaller than Callout
            
        case UIFont.TextStyle.caption1:
            fontName = "AvenirNext-Regular"
            
        case UIFont.TextStyle.caption2:
            //fontName = "AvenirNext-Italic"
            //AUG 2019 - removed italic
            fontName = "AvenirNext-Regular"

        default:
            //Ugly font so we know something is wrong
            fontName = "AvenirNext-Medium" //Body
        }
        
        //------------------------------------------------------------------------------------------------
        //TEST 3 - HACK - to see if code is applying custome font choose a distinct system one
        //------------------------------------------------------------------------------------------------
        //fontName = "Chalkduster"
        //------------------------------------------------------------------------------------------------

        
        //---------------------------------------------------------------------
        //        Avenir Next : AvenirNext-Bold
        //        Avenir Next : AvenirNext-BoldItalic
        //        Avenir Next : AvenirNext-DemiBold
        //        Avenir Next : AvenirNext-DemiBoldItalic
        //        Avenir Next : AvenirNext-Heavy
        //        Avenir Next : AvenirNext-HeavyItalic
        //        Avenir Next : AvenirNext-Italic
        //        Avenir Next : AvenirNext-Medium
        //        Avenir Next : AvenirNext-MediumItalic
        //        Avenir Next : AvenirNext-Regular
        //        Avenir Next : AvenirNext-UltraLight
        //        Avenir Next : AvenirNext-UltraLightItalic
        //---------------------------------------------------------------------
        return fontName
    }
    
    
    class func arrayOfTextStylesAndPointSizes() -> [(UIFont.TextStyle, CGFloat)]{
        
        //Font sizes will vary depending on how large the Accessibility > Larger font is set in Settings
        //one Style isnt always the largest
        //this combines them all in array and sorts by point size
        
        
        //will vary depending on what user has set in Accessibility
//OK
//        //logger.debug("======== pointSize for Text Style START ===================")
//        //logger.debug("(UIFont.TextStyleTitle1).pointSize     :\(UIFont.preferredFontForTextStyle(UIFont.TextStyleTitle1).pointSize)")
//        //logger.debug("(UIFont.TextStyleTitle2).pointSize     :\(UIFont.preferredFontForTextStyle(UIFont.TextStyleTitle2).pointSize)")
//        //logger.debug("(UIFont.TextStyleTitle3).pointSize     :\(UIFont.preferredFontForTextStyle(UIFont.TextStyleTitle3).pointSize)")
//        //logger.debug("(UIFont.TextStyleHeadline).pointSize   :\(UIFont.preferredFontForTextStyle(UIFont.TextStyleHeadline).pointSize)")
//        //logger.debug("(UIFont.TextStyleSubheadline).pointSize:\(UIFont.preferredFontForTextStyle(UIFont.TextStyleSubheadline).pointSize)")
//        //logger.debug("(UIFont.TextStyleBody).pointSize       :\(UIFont.preferredFontForTextStyle(UIFont.TextStyleBody).pointSize)")
//        //logger.debug("(UIFont.TextStyleCallout).pointSize    :\(UIFont.preferredFontForTextStyle(UIFont.TextStyleCallout).pointSize)")
//        //logger.debug("(UIFont.TextStyleFootnote).pointSize   :\(UIFont.preferredFontForTextStyle(UIFont.TextStyleFootnote).pointSize)")
//        //logger.debug("(UIFont.TextStyleCaption1).pointSize   :\(UIFont.preferredFontForTextStyle(UIFont.TextStyleCaption1).pointSize)")
//        //logger.debug("(UIFont.TextStyleCaption2).pointSize   :\(UIFont.preferredFontForTextStyle(UIFont.TextStyleCaption2).pointSize)")
//        //logger.debug("======== pointSize for Text Style END ===================")

        
        
        let arrayOfStylesAndPointSizes = [
            (UIFont.TextStyle.title1, UIFont.preferredFont(forTextStyle: .title1).pointSize),
            (UIFont.TextStyle.title2, UIFont.preferredFont(forTextStyle: .title2).pointSize),
            (UIFont.TextStyle.title3, UIFont.preferredFont(forTextStyle: .title3).pointSize),
            (UIFont.TextStyle.headline, UIFont.preferredFont(forTextStyle: .headline).pointSize),
            (UIFont.TextStyle.subheadline, UIFont.preferredFont(forTextStyle: .subheadline).pointSize),
            (UIFont.TextStyle.body, UIFont.preferredFont(forTextStyle: .body).pointSize),
            (UIFont.TextStyle.callout, UIFont.preferredFont(forTextStyle: .callout).pointSize),
            (UIFont.TextStyle.footnote, UIFont.preferredFont(forTextStyle: .footnote).pointSize),
            (UIFont.TextStyle.caption1, UIFont.preferredFont(forTextStyle: .caption1).pointSize),
            (UIFont.TextStyle.caption2, UIFont.preferredFont(forTextStyle: .caption2).pointSize),
        ]
        
//        print(arrayOfStylesAndPointSizes)
        //------------------------------------------------------------------------------------------------
        //SORT BY pointSize
        //------------------------------------------------------------------------------------------------
        
        let arrayOfStylesAndPointSizesSorted = arrayOfStylesAndPointSizes.sorted {$0.1 == $1.1 ? $0.1 > $1.1 : $0.1 > $1.1 }
        //print(arrayOfStylesAndPointSizesSorted)
//---------------------------------------------------------------------
        //OK - too noisy
//        //logger.debug("======== sorted by pointSize START ===================")
//        for (style, pointSize) in arrayOfStylesAndPointSizesSorted{
//            //logger.debug("\(style)\t\(pointSize)")
//        }
//        //logger.debug("======== sorted by pointSize END ===================")

        //[("UICTFontTextStyleTitle1", 28.0), ("UICTFontTextStyleTitle2", 22.0), ("UICTFontTextStyleTitle3", 20.0), ("UICTFontTextStyleHeadline", 17.0), ("UICTFontTextStyleBody", 17.0), ("UICTFontTextStyleCallout", 16.0), ("UICTFontTextStyleSubhead", 15.0), ("UICTFontTextStyleFootnote", 13.0), ("UICTFontTextStyleCaption1", 12.0), ("UICTFontTextStyleCaption2", 11.0)]
        //------------------------------------------------------------------------------------------------
        //USER HAS FONT IN Accessibility set to the max
        //UIApplication.sharedApplication().preferredContentSizeCategory:UICTContentSizeCategoryAccessibilityXXXL
        //------------------------------------------------------------------------------------------------
        //    UICTFontTextStyleBody     53.0   << BODY is largest at higher zooms
        //    UICTFontTextStyleTitle1	34.0
        //    UICTFontTextStyleTitle2	28.0
        //    UICTFontTextStyleTitle3	26.0
        //    UICTFontTextStyleHeadline	23.0
        //    UICTFontTextStyleCallout	22.0
        //    UICTFontTextStyleSubhead	21.0
        //    UICTFontTextStyleFootnote	19.0
        //    UICTFontTextStyleCaption1	18.0
        //    UICTFontTextStyleCaption2	17.0
        
        //------------------------------------------------------------------------------------------------
        //DEFAULT
        //UIApplication.sharedApplication().preferredContentSizeCategory:UICTContentSizeCategoryL
        //------------------------------------------------------------------------------------------------
        //    UICTFontTextStyleTitle1	28.0
        //    UICTFontTextStyleTitle2	22.0
        //    UICTFontTextStyleTitle3	20.0
        //    UICTFontTextStyleHeadline	17.0
        //    UICTFontTextStyleBody     17.0
        //    UICTFontTextStyleCallout	16.0
        //    UICTFontTextStyleSubhead	15.0
        //    UICTFontTextStyleFootnote	13.0
        //    UICTFontTextStyleCaption1	12.0
        //    UICTFontTextStyleCaption2	11.0
        
        
        //------------------------------------------------------------------------------------------------
        ////UIApplication.sharedApplication().preferredContentSizeCategory:UICTContentSizeCategoryXS
        //------------------------------------------------------------------------------------------------
        //    UICTFontTextStyleTitle1	25.0
        //    UICTFontTextStyleTitle2	19.0
        //    UICTFontTextStyleTitle3	17.0
        //    UICTFontTextStyleHeadline	14.0
        //    UICTFontTextStyleBody     14.0
        //    UICTFontTextStyleCallout	13.0
        //    UICTFontTextStyleSubhead	12.0
        //    UICTFontTextStyleFootnote	12.0
        //    UICTFontTextStyleCaption1	11.0
        //    UICTFontTextStyleCaption2	11.0
        //------------------------------------------------------------------------------------------------

        return arrayOfStylesAndPointSizesSorted
    }
    
    
    /*
    <UICTFont: 0x7fa1b962d7f0> font-family: "Helvetica"; font-weight: normal; font-style: normal; font-size: 12.00pt - try to find nearest TextStyle
    ======== sorted by pointSize END ===================
    [UICTFontTextStyleTitle1	28.0]  COMPARED TO givenFont.pointSize[12.0]
    [givenFont.pointSize >= pointSize] FAILED LOOP TO NEXT LOWEST pointSize
    [UICTFontTextStyleTitle2	22.0]  COMPARED TO givenFont.pointSize[12.0]
    [givenFont.pointSize >= pointSize] FAILED LOOP TO NEXT LOWEST pointSize
    [UICTFontTextStyleTitle3	20.0]  COMPARED TO givenFont.pointSize[12.0]
    [givenFont.pointSize >= pointSize] FAILED LOOP TO NEXT LOWEST pointSize
    [UICTFontTextStyleHeadline	17.0]  COMPARED TO givenFont.pointSize[12.0]
    [givenFont.pointSize >= pointSize] FAILED LOOP TO NEXT LOWEST pointSize
    [UICTFontTextStyleBody	17.0]  COMPARED TO givenFont.pointSize[12.0]
    [givenFont.pointSize >= pointSize] FAILED LOOP TO NEXT LOWEST pointSize
    [UICTFontTextStyleCallout	16.0]  COMPARED TO givenFont.pointSize[12.0]
    [givenFont.pointSize >= pointSize] FAILED LOOP TO NEXT LOWEST pointSize
    [UICTFontTextStyleSubhead	15.0]  COMPARED TO givenFont.pointSize[12.0]
    [givenFont.pointSize >= pointSize] FAILED LOOP TO NEXT LOWEST pointSize
    [UICTFontTextStyleFootnote	13.0]  COMPARED TO givenFont.pointSize[12.0]
    [givenFont.pointSize >= pointSize] FAILED LOOP TO NEXT LOWEST pointSize
    [UICTFontTextStyleCaption1	12.0]  COMPARED TO givenFont.pointSize[12.0]
    [givenFont.pointSize >= pointSize] returning UICTFontTextStyleCaption1
    nearestTextStyleUICTFontTextStyleCaption1
    CUSTOM FONT FOUND [Helvetica]
    
    
    */
    //SWIFT3
    class func guessTextStyle(_ givenFont: UIFont) -> UIFont.TextStyle?{
        //logger.debug("givenFont.fontName:\(givenFont.fontName)")
        
        //swift3
        var guessedTextStyle : UIFont.TextStyle?
            
        let arrayOfSortedTextStylesAndPointSizes = arrayOfTextStylesAndPointSizes()
        
        //if size in givenFont is lower than last then use the last one in the list
        //swift3
        var lastStyle : UIFont.TextStyle?
        
        for (style, pointSize) in arrayOfSortedTextStylesAndPointSizes{
            //logger.debug("[\(style)\t\(pointSize)]  COMPARED TO givenFont.pointSize[\(givenFont.pointSize)]")
            
            if givenFont.pointSize >= pointSize {
                //logger.debug("[givenFont.pointSize >= pointSize] returning \(style)")
                guessedTextStyle =  style
                break
            }else{
                //logger.debug("[givenFont.pointSize >= pointSize] FAILED LOOP TO NEXT LOWEST pointSize")
            }
            //if we leave loop without finding any e.g. font is label is 2points
            lastStyle = style
        }
        
        if guessedTextStyle == nil{
            //logger.debug("[givenFont.pointSize >= pointSize] returning lastStyle \(lastStyle)")
            guessedTextStyle = lastStyle //smallest size
        }
        
        return guessedTextStyle
    }
    
    //If control has a font name set instead of a Text Style then we can still replace it with a custom font
    //but we need to know which Text Style to replace it with
    //So find one with same pointSize or nearest pointSize
    class func replaceHardcodedFontNameWithTextStyle(_ hardcodedFont: UIFont) -> UIFont.TextStyle{
        var testStyleStringReturned = UIFont.TextStyle.body
        
        if let textStyle = UIFont.guessTextStyle(hardcodedFont) {
            testStyleStringReturned = textStyle
            
        }else{
            logger.error("Could not guessTextStyle for:\r\(hardcodedFont) - default to UIFont.TextStyleBody")
            testStyleStringReturned = UIFont.TextStyle.body
        }

        return testStyleStringReturned
    }
}



//--------------------------------------------------------------
// MARK: -
// MARK: - UIFont - dump Fonts in system
//--------------------------------------------------------------
extension UIFont{
    
    /*
    ["Academy Engraved LET", "Al Nile", "American Typewriter", "Apple Color Emoji", "Apple SD Gothic Neo", "Arial", "Arial Hebrew", "Arial Rounded MT Bold", "Avenir", "Avenir Next", "Avenir Next Condensed", "Bangla Sangam MN", "Baskerville", "Bodoni 72", "Bodoni 72 Oldstyle", "Bodoni 72 Smallcaps", "Bodoni Ornaments", "Bradley Hand", "Chalkboard SE", "Chalkduster", "Cochin", "Copperplate", "Courier", "Courier New", "Damascus", "Devanagari Sangam MN", "Didot", "Euphemia UCAS", "Farah", "Futura", "Geeza Pro", "Georgia", "Gill Sans", "Gujarati Sangam MN", "Gurmukhi MN", "Heiti SC", "Heiti TC", "Helvetica", "Helvetica Neue", "Hiragino Mincho ProN", "Hiragino Sans", "Hoefler Text", "Iowan Old Style", "Kailasa", "Kannada Sangam MN", "Khmer Sangam MN", "Kohinoor Bangla", "Kohinoor Devanagari", "Kohinoor Telugu", "Lao Sangam MN", "Malayalam Sangam MN", "Marker Felt", "Menlo", "Mishafi", "Noteworthy", "Optima", "Oriya Sangam MN", "Palatino", "Papyrus", "Party LET", "PingFang HK", "PingFang SC", "PingFang TC", "Savoye LET", "Sinhala Sangam MN", "Snell Roundhand", "Symbol", "Tamil Sangam MN", "Telugu Sangam MN", "Thonburi", "Times New Roman", "Trebuchet MS", "Verdana", "Zapf Dingbats", "Zapfino"]
    75
    */
    
    /*
    
    Academy Engraved LET : AcademyEngravedLetPlain
    Al Nile : AlNile
    Al Nile : AlNile-Bold
    American Typewriter : AmericanTypewriter
    American Typewriter : AmericanTypewriter-Bold
    American Typewriter : AmericanTypewriter-Condensed
    */
    class func dumpFontsInApp(){
        //logger.debug("********* dumpFontsInApp START *************")
//        var fontCount = 0
//        for familyName in UIFont.familyNames.sorted(){
//            
//            for fontName in UIFont.fontNames(forFamilyName: familyName).sorted(){
//                //logger.debug(" \(familyName) : \(fontName)")
//                fontCount = fontCount + 1
//            }
//        }
        //logger.debug("********* dumpFontsInApp   *************")
        
        //logger.debug("UIFont.familyNames().count:\(UIFont.familyNames().count)")
        //logger.debug("Fonts count:\(fontCount)")
        
        //logger.debug("********* dumpFontsInApp END *************")
        
    }
    
    //dumpFontsInApp to see font in the system
    class func findFontInSystem(_ fontName : String) -> Bool{
        var fontFound = false
        for familyName in UIFont.familyNames.sorted(){
            
            for fontNameInApp in UIFont.fontNames(forFamilyName: familyName).sorted(){
                
                if fontNameInApp == fontName{
                    fontFound = true
                    //logger.debug("CUSTOM FONT FOUND [\(fontName)]")
                    break
                }
            }
            //breka out of outerloop if inner break called
            if fontFound{
                break
            }
        }
        return fontFound
    }
    
    

    /*
    UIAppFonts:(
    "Cantarell-Regular.ttf",
    "Chalkduster.ttf"
    )
    
    findFontInInfoPlistUIAppFonts("Chalkduster")
    findFontInInfoPlistUIAppFonts("Chalkduster.ttf")
    
    */
    class func findFontInInfoPlistUIAppFonts(_ customFontName: String) -> Bool{
        let fontNameFoundInAppPlist = false
        //[[NSBundle mainBundle] infoDictionary]:
        if let infoDictionary : [String : AnyObject] = Bundle.main.infoDictionary as [String : AnyObject]? {
            
            //    <key>UIAppFonts</key>
            //    <array>
            //        <string>Cantarell-Regular.ttf</string>
            //        <string>Chalkduster.ttf</string>
            //    </array>
            
            if let anyObject_UIAppFonts = infoDictionary["UIAppFonts"]{
            
                //print(anyObject_UIAppFonts)
// TODO: - SWIFT3
//                for fontNameInPlistNSString : NSString in anyObject_UIAppFonts as! [String] {
//                    //print(fontNameInPlistNSString)
//
//                    if fontNameInPlistNSString.hasPrefix(customFontName){
//                        fontNameFoundInAppPlist = true
//                        //logger.debug("FOUND customFontName[\(customFontName)] in ['UIAppFonts']:\(fontNameInPlistNSString)")
//                        break
//                    }
//                }
                
                if fontNameFoundInAppPlist{
                    //no error - font found
                }else{
                    logger.error("'\(customFontName)' NOT FOUND IN Info.Plist 'UIAppFonts'\r\(anyObject_UIAppFonts)")
                }
                
            }else{
                //logger.debug("infoDictionary['UIAppFonts'] is nil")
            }
        }else{
            //logger.debug("NSBundle.mainBundle().infoDictionary is nil")
        }

        return fontNameFoundInAppPlist
    }
    
    
    /*
    UIAppFonts:(
    "Cantarell-Regular.ttf",
    "Chalkduster.ttf"
    )
    
    */
    class func dumpFontInInfoPlistUIAppFonts(){
        //[[NSBundle mainBundle] infoDictionary]:
        //noisy
//        if let infoDictionary = NSBundle.mainBundle().infoDictionary {
//            if let arrayOfAppFonts = infoDictionary["UIAppFonts"] {
//                
//                logger.debug("UIAppFonts:\(arrayOfAppFonts)")
//                
//            }else{
//                //logger.debug("infoDictionary['UIAppFonts'] is nil")
//            }
//        }else{
//            //logger.debug("NSBundle.mainBundle().infoDictionary is nil")
//        }
        
    }
}



//--------------------------------------------------------------
// MARK: -
// MARK: - OTHER UTILS
//--------------------------------------------------------------

//https://github.com/WeZZard/Waxing/blob/c5096a0a79d3f0381554c9c8fba5ef7eaf7da627/PlatformDependent/Waxing-iOS/Extension/UIFont/UIFont%2BModernize.swift
//https://gist.github.com/Tylerc230/54c875c73bafd42b71cd

//BEWARE UIFont.TextStyle is struct in UIFontDescriptor
//public struct UIFont.TextStyle : RawRepresentable, Equatable, Hashable, Comparable {
//    
//    public init(_ rawValue: String)
//    
//    public init(rawValue: String)
//}
//.body >>

//extension UIFont.TextStyle {
//    
//    
//    // Font text styles, semantic descriptions of the intended use for a font returned by +[UIFont preferredFontForTextStyle:]
//    @available(iOS 9.0, *)
//    public static let title1: UIFont.TextStyle
//    
//    @available(iOS 9.0, *)
//    public static let title2: UIFont.TextStyle
//    
//    @available(iOS 9.0, *)
//    public static let title3: UIFont.TextStyle
//    
//    @available(iOS 7.0, *)
//    public static let headline: UIFont.TextStyle
//    
//    @available(iOS 7.0, *)
//    public static let subheadline: UIFont.TextStyle
//    
//    @available(iOS 7.0, *)
//    public static let body: UIFont.TextStyle
//    
//    @available(iOS 9.0, *)
//    public static let callout: UIFont.TextStyle
//    
//    @available(iOS 7.0, *)
//    public static let footnote: UIFont.TextStyle
//    
//    @available(iOS 7.0, *)
//    public static let caption1: UIFont.TextStyle
//    
//    @available(iOS 7.0, *)
//    public static let caption2: UIFont.TextStyle
//}



//public enum UIFont.TextStyle : String {
//
//    @available(iOS 9.0, *)
//    case title1 = "UICTFontTextStyleTitle1"
//    case title2 = "UICTFontTextStyleTitle2"
//    case title3 = "UICTFontTextStyleTitle3"
//    case headline = "UICTFontTextStyleHeadline"
//    case subheadline = "UICTFontTextStyleSubhead"
//    case body = "UICTFontTextStyleBody"
//    case footnote = "UICTFontTextStyleFootnote"
//    case caption1 = "UICTFontTextStyleCaption1"
//    case caption2 = "UICTFontTextStyleCaption2"
//    @available(iOS 9.0, *)
//    case callout = "UICTFontTextStyleCallout"
//    //no new ones in iOS10 - add a label in IB and change font to system to see all types
//    
//    //style stored as string in FontDescriptor
//    static func fontTextStyleForName(name:String) -> UIFont.TextStyle{
//        switch name{
//        case "UICTFontTextStyleTitle1":
//            return .title1
//            
//        case "UICTFontTextStyleTitle2":
//            return .title2
//            
//        case "UICTFontTextStyleTitle3":
//            return .title3
//            
//        case "UICTFontTextStyleHeadline":
//            return .headline
//            
//        case "UICTFontTextStyleSubhead":
//            return .subheadline
//            
//        case "UICTFontTextStyleBody":
//            return .body
//            
//        case "UICTFontTextStyleFootnote":
//            return .footnote
//            
//        case "UICTFontTextStyleCaption1":
//            return .caption1
//            
//        case "UICTFontTextStyleCaption2":
//            return .caption2
//
//        case "UICTFontTextStyleCallout":
//            return .callout
//        default:
//            logger.error("[fontTextStyleForName]UNKNOWN font style string:\(name) defaulting to .body")
//            return .body
//        }
//    }
//}

extension UIFont {
//    public class func preferredFontForTextStyle(_ style: UIFont.TextStyle)
//        -> UIFont
//    {
//        
//        //MUST use
//        //UIFont.TextStyle.body NOT .body as theyre static
//        
//        switch style {
//        case UIFont.TextStyle.body:
//            return preferredFont(forTextStyle: .body)
//            
//        case UIFont.TextStyle.caption1:
//            return preferredFont(forTextStyle: .caption1)
//            
//        case UIFont.TextStyle.caption2:
//            return preferredFont(forTextStyle: .caption2)
//            
//        case UIFont.TextStyle.footnote:
//            return preferredFont(forTextStyle: .footnote)
//            
//        case UIFont.TextStyle.headline:
//            return preferredFont(forTextStyle: .headline)
//            
//        case UIFont.TextStyle.subheadline:
//            return preferredFont(forTextStyle: .subheadline)
//            
//        case UIFont.TextStyle.title1:
//            return preferredFont(forTextStyle: .title1)
//            
//        case UIFont.TextStyle.title2:
//            return preferredFont(forTextStyle: .title2)
//            
//        case UIFont.TextStyle.title3:
//            return preferredFont(forTextStyle: .title3)
//            
//        case UIFont.TextStyle.callout:
//            return preferredFont(forTextStyle: .callout)
//        }
//    }
}

extension UIFontDescriptor {
    
    public convenience init(textStyle: UIFont.TextStyle) {

        //MUST use 
        //UIFont.TextStyle.body NOT .body as theyre static
        
        switch textStyle {
        case UIFont.TextStyle.body:
            self.init(fontAttributes:[UIFontDescriptor.AttributeName.textStyle: UIFont.TextStyle.body])
            
        case UIFont.TextStyle.caption1:
            self.init(fontAttributes:[UIFontDescriptor.AttributeName.textStyle: UIFont.TextStyle.caption1])
            
        case UIFont.TextStyle.caption2:
            self.init(fontAttributes:[UIFontDescriptor.AttributeName.textStyle: UIFont.TextStyle.caption2])
            
        case UIFont.TextStyle.footnote:
            self.init(fontAttributes:[UIFontDescriptor.AttributeName.textStyle: UIFont.TextStyle.footnote])
            
        case UIFont.TextStyle.headline:
            self.init(fontAttributes:[UIFontDescriptor.AttributeName.textStyle: UIFont.TextStyle.headline])
            
        case UIFont.TextStyle.subheadline:
            self.init(fontAttributes:[UIFontDescriptor.AttributeName.textStyle: UIFont.TextStyle.subheadline])
            
        case UIFont.TextStyle.title1:
            self.init(fontAttributes:[UIFontDescriptor.AttributeName.textStyle: UIFont.TextStyle.title1])
            
        case UIFont.TextStyle.title2:
            self.init(fontAttributes:[UIFontDescriptor.AttributeName.textStyle: UIFont.TextStyle.title2])
            
        case UIFont.TextStyle.title3:
            self.init(fontAttributes:[UIFontDescriptor.AttributeName.textStyle: UIFont.TextStyle.title3])
            
        case UIFont.TextStyle.callout:
            self.init(fontAttributes:[UIFontDescriptor.AttributeName.textStyle: UIFont.TextStyle.callout])
        default:
            logger.error("UIFontDescriptor init - default to .body")
            self.init(fontAttributes:[UIFontDescriptor.AttributeName.textStyle: UIFont.TextStyle.body])
        }
    }
}






//--------------------------------------------------------------
// MARK: -
// MARK: - CustomFontManager
//--------------------------------------------------------------

class CustomFontManager{
    

    
//    UIFont.TextStyleTitle1
//    UIFont.TextStyleTitle2
//    UIFont.TextStyleTitle3
//    UIFont.TextStyleHeadline
//    UIFont.TextStyleSubheadline
//    UIFont.TextStyleBody
//    UIFont.TextStyleCallout
//    UIFont.TextStyleFootnote
//    UIFont.TextStyleCaption1
//    UIFont.TextStyleCaption2

    //WORKS But not accurate - use attribute
 

    
}
