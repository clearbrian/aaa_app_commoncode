//
//  ParentWSController.swift
//  SandPforiOS
//
//  Created by Brian Clear on 25/06/2015.
//  Copyright (c) 2015 CLARKSONS RESEARCH SERVICES LIMITED. All rights reserved.
//

import Foundation


enum RequestState{
    case running    //caller should wait till it finishes - if app starts ,gets ws then map or list chosen UI should wait (show progress spinner)
    case not_running
}

enum HTTPMethod : Int {
    
    case httpMethod_NOT_SET
    case httpMethod_GET
    case httpMethod_POST

    //------------------------------------------------
    func HTTPMethodAsString() -> String{
        switch self{
            
        case .httpMethod_NOT_SET:
            return "HTTPMethod_NOT_SET"
            
        case .httpMethod_GET:
            return "GET"
            
        case .httpMethod_POST:
            return "POST"
            
        }
    }
    //------------------------------------------------
}

protocol ParentWSControllerDelegate {
    //if List and Map VC share same Response - one must call ws and other should wait till it returns
    func requestStateAlreadyRunning(parentWSController: ParentWSController)
    
    func response_success(parentWSController: ParentWSController, parsedObject: Any)
    func response_failure(parentWSController: ParentWSController, error: Error)
    
}

class ParentWSController: ParentNSObject, XMLParserDelegate{
   
    //any ParentViewController/ParentTableViewController
    //all protocols should be extension of ParentWSControllerDelegate
    var delegate: ParentWSControllerDelegate?
    
    //var requestState = RequestState.not_running
    
    
    
    //--------------------------------------------------------------
    // MARK: - DELEGATE RESPONSES
    // MARK: -
    //--------------------------------------------------------------

    //we should get data once but response may be shared between Map and List - if
    func delegate_requestAlreadyRunning(){
        
        if let delegate = self.delegate{
            delegate.requestStateAlreadyRunning(parentWSController: self)
            
        }else{
            logger.error("delegate is nil")
        }
    }
    
    func delegate_response_success(parsedObject: Any){
        // TODO: - CLEANUP after test
//        self.requestState = .not_running
        
        if let delegate = self.delegate{
            delegate.response_success(parentWSController: self, parsedObject: parsedObject)
            
        }else{
            logger.error("delegate is nil")
        }
    }
    
    func delegate_response_failure(error: Error){
        // TODO: - CLEANUP after test
//        self.requestState = .not_running
        
        if let delegate = self.delegate{
            delegate.response_failure(parentWSController: self, error: error)
            
        }else{
            logger.error("delegate is nil")
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - callWS
    // MARK: -
    //--------------------------------------------------------------
    func callWS(_ parentWSRequest: ParentWSRequest,
                success: @escaping (_ parentWSRequest: ParentWSRequest, _ parsedObject: Any) -> Void,
                failure: @escaping (_ parentWSRequest: ParentWSRequest, _ error: Error) -> Void
        )
    {
         // TODO: - CLEANUP after test: allow multiples
//        if requestState == .running{
//
//            //----------------------------------------------------------------------------------------
//            //WS ALREADY RUNNING
//            //----------------------------------------------------------------------------------------
//            logger.error("requestState == .running - second callWS skipped - waiting for first")
//            //----------------------------------------------------------------------------------------
//            delegate_requestAlreadyRunning()
//            //----------------------------------------------------------------------------------------
//
//        }else{
        
            //----------------------------------------------------------------------------------------
            //NOT RUNNING - OK TO START NEW ONE
            //----------------------------------------------------------------------------------------
        // TODO: - CLEANUP after test: allow multiples
//            requestState = .running
        
            //----------------------------------------------------------------------------------------
            //CALL WS - START
            //----------------------------------------------------------------------------------------
            
            if let urlString = parentWSRequest.urlString{
                //----------------------------------------------------------------------------------------
                //PARAMETERS
                //----------------------------------------------------------------------------------------
                
                let parameters: [String: AnyObject]? = parentWSRequest.parameters
                
                //WRONG - causing two ?..? one in the key getting in valid key
                //var urlStringFinal = "\(urlString)?"
                var urlStringFinal = "\(urlString)"
                
                var paramString: String? = nil
                //------------------------------------------------------
                //PARAMETERS
                //------------------------------------------------------
                /*
                 Printing description of parameters:
                 ([String : AnyObject]?) parameters = 2 key/value pairs {
                 [0] = {
                 key = "placeid"
                 value = (instance_type = 0x0000000142ea8580)
                 }
                 [1] = {
                 key = "key"
                 value = (instance_type = 0x0000000142c662d0 -> 0x0000000100f768a0 (void *)0x000001a100f82651)
                 }
                 }
                 */
                
                //------------------------------------------------------
                if let parameters = parameters{
                    for parameter in parameters{
                        //---------------------------------------------------------
                        //NOISY logger.debug("parameter.0:\(parameter.0)")
                        //NOISY logger.debug("parameter.1:\(parameter.1)")
                        //---------------------------------------------------------
                        let key = parameter.0
                        let value = parameter.1
                        //---------------------------------------------------------
                        if let paramStringCurrent = paramString {
                            paramString = "\(paramStringCurrent)\(key)=\(value)&"
                        }else{
                            paramString = "\(key)=\(value)&"
                        }
                        
                    }
                }else{
                    logger.error("parameters is nil")
                }
                //only add ? if there is params
                if let paramString = paramString {
                    urlStringFinal = "\(urlStringFinal)?\(paramString)"
                }else{
                    logger.info("no params - dont append uneeded ? urlStringFinal:\(urlStringFinal)")
                }
                
                //------------------------------------------------------------------------------------------------
                //ESCAPE URL STRING
                //------------------------------------------------------------------------------------------------
                let urlwithPercentEscapes = urlStringFinal.addingPercentEncoding( withAllowedCharacters: CharacterSet.urlQueryAllowed)
                
                if let urlStringwithPercentEscapes = urlwithPercentEscapes{
                    
                    //------------------------------------------------------
                    //HIDE API KEY FROM LOG
                    //------------------------------------------------------
                    //key=
                    //app_key
                    // TODO: - RELEASE COMMENT OUT
                    if urlStringFinal.contains("key"){
                        logger.debug("CALLING URL [BEFORE]:\r[REDACTED]")
                    }else{
                        logger.highlight("CALLING URL [BEFORE]:\r[\(urlString)]")
                    }
                
                    //------------------------------------------------------------------------------------------------
                    //URL
                    //------------------------------------------------------------------------------------------------
                    if let url: URL = URL(string: urlStringwithPercentEscapes){
                        
                        //------------------------------------------------------------
                        logger.debug("URL ESCAPED:[\(urlStringwithPercentEscapes)]")
                        //------------------------------------------------------------
                        
                        //------------------------------------------------------------------------------------------------
                        //REQUEST
                        //------------------------------------------------------------------------------------------------
                        let request: NSMutableURLRequest = NSMutableURLRequest(url: url)
                        
                        
                        //------------------------------------------------------------------------------------------------
                        //REQUEST: METHOD : GET or POST
                        //------------------------------------------------------------------------------------------------
                        // TODO: - HARDCODED
                        request.httpMethod = "GET"
                        //-------------------------------------------------------------------------------
                        switch request.httpMethod{
                        case "GET":
                            logger.debug("GET")
                        case "POST":
                            logger.debug("POST - add the body")
                            //---------------------------------------------------------------------------
                            //POST not implemented
                            //---------------------------------------------------------------------------
                            //                        if let bodyJsonData = parentWSRequest.bodyJsonData{
                            //                            request.HTTPBody = bodyJsonData
                            //
                            //                            let bodystring = NSString(data:bodyJsonData, encoding:NSUTF8StringEncoding) as! String
                            //                            print("====== BODY =====")
                            //                            if urlString.containsString("Authenticate"){
                            //                                print("{\"password\":\"<REDACTED>\",\"username\":\"<REDACTED>\"}")
                            //                            }else{
                            //                                print(bodystring)
                            //                            }
                            //
                            //                            print("=================")
                            //
                            //
                            //                        }else{
                            //                            logger.error("bodyJsonData is nil")
                            //                        }
                            logger.error("TODO: POST NOT IMPLEMENTED")
                            //---------------------------------------------------------------------------
                            
                        default:
                            logger.error("NOT GET or POST")
                        }
                        
                        //------------------------------------------------------------------------------------------------
                        //REQUEST: Referer
                        //------------------------------------------------------------------------------------------------
                        //add header for all ws calls
                        //nov2 -
                        /*
                         {
                         "error_message" : "This IP, site or mobile application is not authorized to use this API key. Request received from IP address 213.133.153.253, with empty referer",
                         "html_attributions" : [],
                         "status" : "REQUEST_DENIED"
                         }
                         
                         DIRECTIONS uses IOS KEY
                         PLACES API uses WEB API
                         -- basic ap used IOS API but i call /place getdetails uses web api
                         
                         */
                        //------------------------------------------------------------------------------------------------
                        logger.debug("Referer hardcocded for web api - http://www.cityoflondonconsulting.com")
                        //------------------------------------------------------------------------------------------------
                        
                        //    var bundleId = "com.cityoflondonconsulting.APPNAME"
                        //
                        //    if let bundleID_ = Bundle.main.bundleIdentifier {
                        //        bundleId = bundleID_
                        //
                        //    }else{
                        //        logger.error("Bundle.main.bundleIdentifier is nil")
                        //    }
                        //    request.setValue(bundleId, forHTTPHeaderField: "Referer")
                        //---------------------------------------------------------
                        //Couldnt get ioS app to work - this is a web call so changed key to
                        //HTTP referrers (web sites)
                        //*.cityoflondonconsulting.com/*
                        request.setValue("http://www.cityoflondonconsulting.com", forHTTPHeaderField: "Referer")
                        //------------------------------------------------------------------------------------------------
                        //REQUEST: Referer - END
                        //------------------------------------------------------------------------------------------------
                        //------------------------------------------------------------------------------------------------
                        
                        
                        //------------------------------------------------------------------------------------------------
                        //REQUEST: Content-Type
                        //------------------------------------------------------------------------------------------------
                        //---------------------------------------------------------------------
                        //request.setValue("application/json;charset=UTF-8", forHTTPHeaderField: "Content-Type")
                        //---------------------------------------------------------------------
                        
                        //------------------------------------------------------------------------------------------------
                        //CALL WEBSERVICE
                        //------------------------------------------------------------------------------------------------
                        
                        parentWSRequest.parentWSRequestState = .ws_in_progress
                        
                        let session: URLSession = URLSession.shared
                        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
                            //---------------------------------------------------------------------
                            //WEBSERVICE RETURNED
                            //---------------------------------------------------------------------
                            sharedApplication.hideNetworkActivity()
                            
                            //BEWARE - this claass can call ws in blocks or in delegates
                            //self.requestState = .not_running
                            
                            if let error = error{
                                //---------------------------------------------------------------------
                                //WEBSERVICE RETURNED : ERROR
                                //---------------------------------------------------------------------
                                failure(parentWSRequest,error)
                                //---------------------------------------------------------------------
                            }else{
                                
                                //---------------------------------------------------------------------
                                //WEBSERVICE RETURNED : NO ERROR > Check Status
                                //---------------------------------------------------------------------
                                if let data = data{
                                    if let httpResponse = response as? HTTPURLResponse{
                                        
                                        //---------------------------------------------------------------------
                                        //WEBSERVICE RETURNED : Hide API key from log
                                        //---------------------------------------------------------------------
                                        // TODO: - COMMENT OUT ON RELEASE
                                        //debug for urlString is above
//                                        let requestUrlString = "\(String(describing: request.url))"
//                                        //app_key=
//                                        //key=
//                                        if requestUrlString.contains("key"){
                                            print("====== [0. STATUS CODE:[\(httpResponse.statusCode)]=====")
//                                        }else{
//                                            print("====== [1. STATUS CODE:[\(httpResponse.statusCode)] \(String(describing: request.url))=====")
//                                        }
                                        
                                        //---------------------------------------------------------------------
                                        //WEBSERVICE RETURNED : GET RESPONSE STRING
                                        //---------------------------------------------------------------------
                                        if let dataStringResponse = NSString(data:data, encoding:String.Encoding.utf8.rawValue){
                                            
                                            //---------------------------------------------------------------------
                                            //Response String
                                            //---------------------------------------------------------------------
                                            // TODO: - RELEASE
                                            //NOISY 
                                            print(dataStringResponse)
                                            print("dataStringResponse.length:\(dataStringResponse.length)")
                                            print("================")
                                            
                                            //---------------------------------------------------------------------
                                            //DEBUG: Header Fields
                                            //---------------------------------------------------------------------
                                            //print("====== [RESPONSE: allHeaderFields =====")
                                            //print("httpResponse.allHeaderFields:\(httpResponse.allHeaderFields)")
                                            //print("================")
                                            
                                            //-----------------------------------------------------------------------------------
                                            //WEBSERVICE RETURNED : GET STATUS CODE
                                            //-----------------------------------------------------------------------------------
                                            
                                            if httpResponse.statusCode ==  200
                                            {
                                                //-----------------------------------------------------------------------------------
                                                //ERROR: 200 - START
                                                //-----------------------------------------------------------------------------------
                                                do {
                                                    //----------------------------------------------------------------------------------------
                                                    // PARSE JSON STRING TO OBJECT
                                                    //----------------------------------------------------------------------------------------
                                                    //TODO: - Moya says json conversion should be seperate object
                                                    //----------------------------------------------------------------------------------------
                                                    let parsedObject: Any = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments)
                                                    
                                                    //---------------------------------------------------------
                                                    //JSON PARSED OK - RETURN
                                                    //---------------------------------------------------------
                                                    DispatchQueue.main.async{
                                                        success(parentWSRequest, parsedObject)
                                                    }
                                                    //----------------------------------------------------------------------------------------
                                                    
                                                } catch let error {
                                                    
                                                    //---------------------------------------------------------
                                                    //JSON PARSED ERROR
                                                    //---------------------------------------------------------
                                                    failure(parentWSRequest,error)
                                                }
                                                //-----------------------------------------------------------------------------------
                                                //ERROR: 200 - END
                                                //-----------------------------------------------------------------------------------
                                            }
                                            else{
                                                //-----------------------------------------------------------------------------------
                                                //ERROR: other status code
                                                //-----------------------------------------------------------------------------------
                                                logger.error("UNHANDLED httpResponse.statusCode:\(httpResponse.statusCode)")
                                                //-----------------------------------------------------------------------------------
                                                
                                                //    if dataStringResponse.contains("An error has occurred"){
                                                //
                                                //        //------------------------------------------------------------------------------------------------
                                                //        //API SERIOUS ERROR
                                                //
                                                //        //Optional({"message":"An error has occurred."})
                                                //        //------------------------------------------------------------------------------------------------
                                                //        //failure(error: NSError(type: .Arc_APIError_message_An_error_has_occurred))
                                                //
                                                //        logger.error("TODO: failure 22222")
                                                //    }
                                                //    else
                                                //    {
                                                //        //success(data: data)
                                                //
                                                //        logger.error("TODO: success 2222333332")
                                                //    }
                                                //----------------------------------------------------------------------------------------
                                                failure(parentWSRequest,
                                                        NSError.errorWithMessage(dataStringResponse as String, code: httpResponse.statusCode)
                                                )
                                                //----------------------------------------------------------------------------------------
                                            }
                                            
                                            //-----------------------------------------------------------------------------------
                                            //ERROR: 400
                                            //-----------------------------------------------------------------------------------
                                            //   else if httpResponse.statusCode ==  400 || httpResponse.statusCode ==  401
                                            //    {
                                            //        let dataStringResponse = dataStringResponse as String
                                            //        logger.error("TODO: 8787989")
                                            //        failure(error: NSError(      errorString : dataStringResponse,
                                            //                               descriptionString : "A login error has occurred",
                                            //                                      statusCode : httpResponse.statusCode))
                                            //
                                            //    }
                                            //-----------------------------------------------------------------------------------
                                            
                                        }else{
                                            //ERROR
                                            logger.error("Failed to convert Data response to string")
                                        }
                                        //------------------------------------------------------------------------------------------------
                                        
                                    }else{
                                        logger.error("response as? NSHTTPURLResponse is nil")
                                    }
                                    
                                }else{
                                    //data response is set
                                    logger.error("Error: http response : Data is nil")
                                }
                            }// is..error
                        }) //dataTask
                        
                        
                        //------------------------------------------------------------------------------------------------
                        //CALL THE dataTask > return through completionHandler above
                        //------------------------------------------------------------------------------------------------
                        sharedApplication.showNetworkActivity()
                        task.resume()
                        //------------------------------------------------------------------------------------------------
                        
                    }else{
                        logger.error("Create URL FAILED with urlStringwithPercentEscapes:'\(urlStringwithPercentEscapes)'")
                    }
                }else{
                    logger.error("urlwithPercentEscapes is nil")
                }
                
                
                
            }else{
                logger.error("failure 777777: webServiceRequest.urlString is nil - did you subclass it")
                //failure(error: NSError(type:.Arc_APIError_QueryString_Throwing_Error))
            }
            //----------------------------------------------------------------------------------------
            //CALL WS END
            //----------------------------------------------------------------------------------------
//        }
    }
    

    //--------------------------------------------------------------
    // MARK: - log error and changes state to not_running if error
    // MARK: -
    //--------------------------------------------------------------

    func logErrorChangeState(_ errorString: String){
//        requestState = .not_running
        
        logger.error(errorString)
        //failure(error: NSError(type:.Arc_APIError_QueryString_Throwing_Error))
     
        
    }
}
