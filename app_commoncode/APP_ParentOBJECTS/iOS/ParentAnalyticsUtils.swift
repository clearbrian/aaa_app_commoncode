//
//  ParentAnalyticsUtils.swift
//  SandPforiOS
//
//  Created by Brian Clear on 28/07/2015.
//  Copyright (c) 2015 CLARKSONS RESEARCH SERVICES LIMITED. All rights reserved.
//

import Foundation
import Flurry_iOS_SDK

class ParentAnalyticsUtils : ParentSwiftObject {
    //call using 
    
//    static func logInstanceToFlurry(instanceInfoString : String){
//        //log the VC as flurry event name - quickest way to track whats screens are being called
//        //<SandPforiOS.VesselSearchOptionsViewController: 0x135590f30>
//        
//        //---------------------------------------------------------------------
//        //NEVER call self in here gives you ParentAnalyticsUtils not calling VC or TVC
//        //var instanceInfoString = "\(self)"
//        //Printing description of instanceInfoString:
//        //"SandPforiOS.ParentAnalyticsUtils"
//        //Printing description of instanceInfoString:
//        //"<SandPforiOS.LoginViewController: 0x7fee941a9f30>"
//        
//        var loggedString = ""
//        if instanceInfoString.hasPrefix("<SandPforiOS."){
//            if let positionOfColon = instanceInfoString.search(":") {
//                loggedString = instanceInfoString.substring(13, end: positionOfColon)
//                logger.debug("FLURRY:'\(loggedString)'")
//                Flurry.logEvent(loggedString)
//            }else{
//                //dont use self is  ParentAnalyticsUtils not the calling VC or TVC
//                //Flurry.logEvent("\(self)")
//                logger.error("unexpected instance id - logging all to Flurry")
//            }
//        }else{
//            logger.error("instanceInfoString doesnt start with <SandPforiOS. - log all to flurry")
//            //dont 
//            
//            //dont use self is  ParentAnalyticsUtils not the calling VC or TVC
//            //Flurry.logEvent("\(self)")
//            logger.error("CANT LOG FLURRY - doesnt start with SandPforiOS")
//        }
//    }
    
    
    static func logToFlurry(_ loggedString : String){
        Flurry.log(eventName: loggedString)
    }
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - GOOGLE ANALYTICS
    //--------------------------------------------------------------
    
    static func logToGoogleAnalytics(_ loggedString : String){
        logger.debug("TODO logToGoogleAnalytics")
//        //---------------------------------------------------------------------
//        //https://github.com/googlesamples/google-services/blob/master/ios/analytics/AnalyticsExampleSwift/ViewController.swift
//        //---------------------------------------------------------------------
//        let name = "\(loggedString)"
//        //---------------------------------------------------------------------
//        // The UA-XXXXX-Y tracker ID is loaded automatically from the
//        // GoogleService-Info.plist by the `GGLContext` in the AppDelegate.
//        // If you're copying this to an app just using Analytics, you'll
//        // need to configure your tracking ID here.
//        //---------------------------------------------------------------------
//        // [START screen_view_hit_swift]
//        if let tracker = GAI.sharedInstance().defaultTracker{
//            tracker.set(kGAIScreenName, value: name)
//            
//            if let builder = GAIDictionaryBuilder.createScreenView(){
//                //SWIFT3 tracker?.send(builder?.build() as [AnyHashable: Any])
//                tracker.send(builder.build() as [NSObject : AnyObject])
//                // [END screen_view_hit_swift]
//            }else{
//                print("ERROR:builder is nil")
//            }
//        }else{
//            print("ERROR:tracker is nil")
//        }
    }
    
    
    //IN:<SandPforiOS.VesselSearchOptionsViewController: 0x135590f30>
    //OUT:VesselSearchOptionsViewController
    static func extractInstanceClassName(_ selfString : String) -> String{
        
        var instanceName = ""
        
        ///let classNamePrefix = "<app_pubtranslon_jamcams."
     
        let classNamePrefix = AppConfig.Flurry_classNamePrefix
        
        if selfString.hasPrefix(classNamePrefix){
            if let positionOfColon = selfString.search(":") {
                //instanceName = selfString.substring(14, end: positionOfColon)
                instanceName = selfString.substring(classNamePrefix.length, end: positionOfColon)
            }else{
                //dont use self is  ParentAnalyticsUtils not the calling VC or TVC
                //Flurry.logEvent("\(self)")
                print("ERROR: [extractInstanceClassName FAILED]  selfString doesnt include : - \(selfString)")
            }
        }else{
            print("ERROR: [extractInstanceClassName FAILED] selfString doesnt start with '\(classNamePrefix)' - \(selfString)")
        }
        return instanceName
    }
}
