//
//  ParentViewController.swift
//  SandPforiOS
//
//  Created by Brian Clear on 09/06/2015.
//  Copyright (c) 2015 CLARKSONS RESEARCH SERVICES LIMITED. All rights reserved.
//

import UIKit

class ParentViewController: UIViewController, ParentWSControllerDelegate {
    //------------------------------------------------
    //LOGGING - Requires XCodeColors plugin
    //MUST BE DECLARED IN EACH CLASS THAT USES IT
    
    //let log = MyXCodeColorsLogger.defaultInstance
    
    //create seperate instance for each VC so we can turn them on/off as we develop them

    //------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //self.navigationController?.title = "Vessel Search"
        
        //to blend in behind the status bar
        self.view.backgroundColor = AppearanceManager.appColorNavbarAndTabBar
        
        
        configureLogging()
    }
    
    func configureLogging(){
        //log = MyXCodeColorsLogger is declared above @ UIApplicationMain
        
        //------------------------------------------------
        //LOG LEVEL
        //------------------------------------------------
        //case Verbose = 1 << SHOW EVERYTHING
        //case Debug
        //case Info
        //case Warning
        //case Error    << RELEASE
        //case None     << ALL OFF
        //------------------------------------------------
//        logger.outputLogLevel = .Verbose_5
        //------------------------------------------------
        //[fg220,50,47;[Error] This is Error Log [;[fg88,110,117;[AppDelegate.swift:34] testLogging() 2015-06-09 11:14:15.811[;
        
        //------------------------------------------------
        //LOG FORMAT
        //------------------------------------------------
//        logger.showFileInfo = true
//        logger.showDate = true
//        logger.showLogLevel = true
//        logger.showFunctionName = true
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Do any additional setup after loading the view, typically from a nib.
        
        //var selfDescription_ = reflect(self)
        print("=================================================")
        print("=========  VIEW APPEARING: \(self) ============== ")
        print("=================================================")

        logToFlurry()
        logToGoogleAnalytics()
        
    }
    
    //dynamic type - if user changes type size in setting we must refresh top most VC
    func applicationDidBecomeActive_refreshTableView(){
        logger.debug("[applicationDidBecomeActive_refreshTableView] MUST SUBCLASS IN CLASS WITH TABLE THAT HAD DYNAMIC TYPE")
    }
    
    
    //OVERRIDE TO ADD MORE INFO OTHER THAN CLASS NAME
    func logToFlurry(){
        //log the VC as flurry event name - quickest way to track whats screens are being called
        //<SandPforiOS.VesselSearchOptionsViewController: 0x135590f30>
        let instanceInfoString = "\(self)"
        let instanceClassName = ParentAnalyticsUtils.extractInstanceClassName(instanceInfoString)
        if instanceClassName == ""{
            logger.error("logToFlurry failed - instanceClassName is ''")
            
        }else{
            ParentAnalyticsUtils.logToFlurry(instanceClassName)
        }
    }
    func logToGoogleAnalytics(){
        //log the VC as flurry event name - quickest way to track whats screens are being called
        //<SandPforiOS.VesselSearchOptionsViewController: 0x135590f30>
        
        let instanceInfoString = "\(self)"
        let instanceClassName = ParentAnalyticsUtils.extractInstanceClassName(instanceInfoString)
        ParentAnalyticsUtils.logToGoogleAnalytics(instanceClassName)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        //return .Default
        return AppearanceManager.preferredStatusBarStyle
    }
    
    //--------------------------------------------------------------
    // MARK: - ParentWSControllerDelegate
    // MARK: -
    //--------------------------------------------------------------

    func requestStateAlreadyRunning(parentWSController: ParentWSController){
        logger.error("requestStateAlreadyRunning - wait for response_success/response_failure")
    }
    
    func response_success(parentWSController: ParentWSController, parsedObject: Any){
        logger.error("response_success - subclass to handle")
    }
    
    func response_failure(parentWSController: ParentWSController, error: Error){
        logger.error("response_failure:\(error) - subclass to handle")
    }
}
