//
//  PlaceType.swift
//  joyride
//
//  Created by Brian Clear on 21/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation

public enum PlaceType : String {
    case Start = "Start"
    case End = "End"
}
