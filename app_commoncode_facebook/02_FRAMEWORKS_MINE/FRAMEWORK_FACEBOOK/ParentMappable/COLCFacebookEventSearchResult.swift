//
//  FBEventResult.swift
//  joyride
//
//  Created by Brian Clear on 07/03/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation

class COLCFacebookEventSearchResult: ParentMappable {
    
    //var id: NSNumber?
    var id: NSString?
    
    
    //var events: [String: AnyObject]?
    //var events: [String : COLCFacebookResultEvents]?
   // var events: [String : COLCFacebookResultEvents]?  = [:]
    
    //OK 
    //
    //var events: [String : AnyObject] = [:]
    //var events: [String : COLCFacebookResultEvents] = [:]
    //WRONG var events: [String : [String : AnyObject]] = [:]
    var events: [String : AnyObject] = [:]
    //WRONG var events: [String : [AnyObject]] = [:]
    
    //---------------------------------------------------------------------
    // root calls mapping()
    //    required init?(_ map: Map){
    //        mapping(map: map)
    //    }
    //---------------------------------------------------------------------
    
    
    //https://github.com/Hearst-DD/ObjectMapper
    override func mapping(map: Map){
        
        id <- map["id"]
        events <- map["events"]
        print("")
    }
    
}
