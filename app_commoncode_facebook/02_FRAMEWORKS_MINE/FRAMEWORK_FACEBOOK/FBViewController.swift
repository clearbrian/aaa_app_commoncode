//
//  FBViewController.swift
//  joyride
//
//  Created by Brian Clear on 20/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import GoogleMaps
import UIKit

import FBSDKCoreKit
import FBSDKLoginKit
import MapKit


protocol FBViewControllerDelegate {
    func fbViewControllerSelectFacebookEvent(_ fbViewController: FBViewController, clkPlacesPickerResultPlace: CLKPlacesPickerResultPlace)
    func fbViewControllerCancelled(_ fbViewController: FBViewController)
}

//removed from project and storyboard backed up AND from bridging header

class FBViewController: ParentPickerViewController, FBSDKLoginButtonDelegate, UITableViewDataSource, UITableViewDelegate
{

    var delegate: FBViewControllerDelegate?
    
    var accessToken :FBSDKAccessToken?
    
    var arrayCOLCFacebookEvent = [COLCFacebookEvent]()
    
    
    @IBOutlet weak var tableView: UITableView!

    @IBOutlet weak var buttonLogin: UIButton!
    @IBOutlet weak var fbSDKLoginButton: FBSDKLoginButton!
    
//    @IBOutlet weak var labelNearestAddress: UILabel!
    
    func preferredContentSizeChanged(_ notification: Notification){
        self.tableView.invalidateIntrinsicContentSize()
        self.tableView.reloadData()
        
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //------------------------------------------------------------------------------------------------
        //to call applicationDidBecomeActive_refreshTableView
        //preferredContentSizeChanged only works sometimes
        appDelegate.topParentViewController = self
        //-----------------------------------------------------------------------------------------------
        //NSNotificationCenter.defaultCenter().addObserver(self,
        //                                                 selector: #selector(FBViewController.preferredContentSizeChanged(_:)),
        //                                                 name: UIContentSizeCategoryDidChangeNotification, object: nil)
        
        //------------------------------------------------------------------------------------------------
        //FB
        //------------------------------------------------------------------------------------------------
        //works but not sure how to ask for events persmission - use loginbutton
        //OK but tried it in IB but wasnt sure how to set non default permissions
        //UNTESTED loginButton.readPermissions = ["public_profile", "email", "user_events", "user_likes", "user_tagged_places", "user_hometown", "user_videos"]
        //let loginButton = FBSDKLoginButton()
        //loginButton.delegate = self
        //loginButton.center = self.view.center
        //self.view.addSubview(loginButton)
    
        
        
        //-----------------------------------------------------------------------------------
        //DYNAMIC TEXT and TABLE ROW HEIGHT
        //-----------------------------------------------------------------------------------
        ////can done in DataSource methods
        //self.tableView.estimatedRowHeight = 170.0
        ////also done in heightForRowAtIndexPath
        //self.tableView.rowHeight = UITableViewAutomaticDimension;
     
        //also done in heightForRowAtIndexPath
        //self.tableView.rowHeight = 130.0
        
        //        self.tableView.sectionHeaderHeight = UITableViewAutomaticDimension;
        //        self.tableView.estimatedSectionHeaderHeight = 80.0f;
        //------------------------------------------------------------------------------------
        
        self.buttonLogin.layer.cornerRadius = 4.0
 
        //------------------------------------------------------------------------------------
        //FACEBOOK if logged into FB then get events immediately
        //------------------------------------------------------------------------------------
        if let _ = FBSDKAccessToken.current(){
            changeButtonToLogout()
            
            dumpCurrentAccessToken()
            
            self.getUserEvents()
            
        }else{
            self.log.info("No FB token - tap button to login")
        }
        
        //------------------------------------------------------------------------------------
    }
    
    //dynamic type - if changed in iOS setting need to reload the table
    override func applicationDidBecomeActive_refreshTableView(){
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //--------------------------------------------------------------
    // MARK: - FBSDKLoginButtonDelegate
    // MARK: -
    //--------------------------------------------------------------
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!){
        self.log.info("loginButtonDidLogOut")
         clearTable()
    }
    
    func clearTable(){
        DispatchQueue.main.async{
            self.arrayCOLCFacebookEvent = [COLCFacebookEvent]()
            self.tableView.reloadData()
        }
    }
    func changeButtonToLogout(){
        DispatchQueue.main.async{
            self.buttonLogin.setTitle("Logout", for: UIControlState())
        }
    }
    func changeButtonToLogin(){
        DispatchQueue.main.async{
            self.buttonLogin.setTitle("Login to Facebook", for: UIControlState())
        }
    }
   
    //--------------------------------------------------------------
    // MARK: - FB DELEGATE
    // MARK: -
    //--------------------------------------------------------------

    //public func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!
    //public func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!)

    //optional public func loginButtonWillLogin(_ loginButton: FBSDKLoginButton!) -> Bool
    
    //func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: NSError!){
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!){
        self.log.info("Coming back from Facebook signin")
        
        if error != nil{
            self.log.error("Error: \(error)")
        }else{
            //LOGIN OK
            self.log.info("result: \(result) isCancelled: \(result.isCancelled) token: \(result.token)")
            self.log.info("declinedPermissions: \(result.declinedPermissions) ")
            self.log.info("grantedPermissions: \(result.grantedPermissions)")
            
            self.accessToken = result.token
            
            let parameters = ["fields" : "id,name,email"]
            //let facebookReadPermissions = ["public_profile", "email", "user_friends"]
            let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: parameters)
            graphRequest.start { (connection, result, error) -> Void in
                if error != nil {
                    //FBSDKLoginManager().logOut()
                    
                    self.log.error("FBSDKGraphRequest error:\(error)")
                
                } else if result != nil {
                    
                    self.log.info("result:\(result)")
                    
                    /*
                    [bg255,255,255;[fg204,204,204;17:01 [;[bg255,255,255;[fg204,204,204;INFO__4 [;[bg255,255,255;[fg140,105,251;result:{
                    id = 1026916504013745;
                    name = "Brian De Cl\U00e8ir";
                    }
                    */
                }
            }
        }
        //[Info_4] Coming back from Facebook signin
        //[Error_2] Error: nil
        //[Info_4] result: <FBSDKLoginManagerLoginResult: 0x7f90924d2750> isCancelled: false token: <FBSDKAccessToken: 0x7f90924cc580>
        //[Info_4] declinedPermissions: [] grantedPermissions: [public_profile]
        
    }
    

    //
    //            // Login with read permssions
    //            //FBSDKAccessToken *accessToken = [FBSDKAccessToken currentAccessToken];
    //
    //        if (![accessToken.permissions containsObject:@"user_friends"]) {
    //                FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
    //                [loginManager logInWithReadPermissions:@[@"user_friends"]
    //                                               handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
    //                                                   if (error) {
    //                                                       NSLog(@"Failed to login:%@", error);
    //                                                       return;
    //                                                   }
    //
    //todo
    //                                                   FBSDKAccessToken *newToken = [FBSDKAccessToken currentAccessToken];
    //                                                   if (![newToken.permissions containsObject:@"user_friends"]) {
    //                                                       // Show alert
    //                                                       UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Login Failed"
    //                                                                                                           message:@"You must login and grant access to your firends list to use this feature"
    //                                                                                                          delegate:self
    //                                                                                                 cancelButtonTitle:@"OK"
    //                                                                                                 otherButtonTitles:nil];
    //                                                       [alertView show];
    //                                                       [self.navigationController popToRootViewControllerAnimated:YES];
    //                                                       return;
    //                                                   }
    //
    //                                                   [self updateFriendsTable];
    //                                               }];
    //
    //            } else {
    //                [self updateFriendsTable];
    //            }
    
    
    //---------------------------------------------------------------------
    //LOGIN WITH READ PERMISSION
    
    //FBSDKLoginManagerRequestTokenHandler
    //typealias FBSDKLoginManagerRequestTokenHandler = (FBSDKLoginManagerLoginResult!, NSError!) -> Void
    //FBSDKLoginManagerLoginResult
    //        login.logInWithReadPermissions(["public_profile", "user_events"], handler: { (result, error) -> Void in
    //            if error == nil {
    //
    //                //LOGIN OK
    //                if result.isCancelled{
    //                    self.log.error("User hit Cancel on Request Permission web page")
    //                }else{
    //                    //User granted all/some permission
    //                    self.log.info("result: \(result) isCancelled: \(result.isCancelled) token: \(result.token)")
    //                    self.log.info("declinedPermissions: \(result.declinedPermissions)")
    //                    self.log.info("grantedPermissions: \(result.grantedPermissions)")
    //
    //                    self.accessToken = result.token
    //
    //                    //[Info_4] result: <FBSDKLoginManagerLoginResult: 0x7f9a64a07800> isCancelled: false token: <FBSDKAccessToken: 0x7f9a64a00d10>
    //                    //[Info_4] declinedPermissions: [] grantedPermissions: [user_friends]
    //                }
    //
    //
    //            } else {
    //                //print("There was an error logging in. Error: \(error)")
    //            }
    //        })

    
    
    //--------------------------------------------------------------
    // MARK: - buttonLogin_TouchUpInside
    // MARK: -
    //--------------------------------------------------------------
    @IBAction func buttonLogin_TouchUpInside(_ sender: AnyObject) {
        //------------------------------------------------------------------------------------------------
        //LOGIN / GET EVENTS
        //------------------------------------------------------------------------------------------------
        if let _ = FBSDKAccessToken.current(){
            //-------------------------------------------------------------------
            //LOGOUT > LOGIN
            //-------------------------------------------------------------------
            FBSDKLoginManager().logOut()
            clearTable()
            changeButtonToLogin()
            
        }else{
            //Not logged in
            //too early - login may fail changeButtonToLogout()

            loginAndGetEvents()
            
        }
    }

    //-----------------------------------------------------------------------------------
    //        https://github.com/search?utf8=%E2%9C%93&q=user_events+extension%3Aswift
    //        https://github.com/vgorloff/AWLFacebookProfileViewer
    //        https://github.com/rebeccagoldman/FacebookFeed
    //-----------------------------------------------------------------------------------
    ////        https://developers.facebook.com/docs/facebook-login/testing-your-login-flow
    //-----------------------------------------------------------------------------------
    //        "public_profile", "email", "user_friends", "user_birthday", "user_location", "user_education_history", "user_events"
    //
    //        loginButton.readPermissions = ["public_profile", "email", "user_events", "user_likes", "user_tagged_places", "user_hometown", "user_videos"]
    //
    //        https://github.com/lonetazdevil20/FusionBand/blob/7764c105cf7cadf1272f774ca1187e3a2f7b5015/FusionBand/HomeViewController.swift
    //
    //
    //        @IBOutlet weak var fbLoginView: FBLoginView?
    //        @IBOutlet weak var fbProfilePicView: FBProfilePictureView?
    //        @IBOutlet weak var fbLikeControl: FBLikeControl?
    //-----------------------------------------------------------------------------------
    //        FBSDKProfilePictureView
    //        https://github.com/RichFell/WhoAreYou
    //-----------------------------------------------------------------------------------
    //https://github.com/felix-dumit/dailybond-ios/blob/4e16d7d0c4481e37e731cb906a611b82dbce8732/DailyBond/DailyBond/FBRequests.swift
    //        return getRequest("me/events", params : ["fields": "place,name,start_time,id,rsvp_status,cover"], method: "GET")
    //-----------------------------------------------------------------------------------
    

    func loginAndGetEvents(){
        //currentAccessToken : FBSDKAccessToken
        if let _ = FBSDKAccessToken.current(){
            //---------------------------------------------------------------------
            //HAS TOKEN - GET EVENTS
            //---------------------------------------------------------------------
            changeButtonToLogout()
            dumpCurrentAccessToken()

            self.getUserEvents()

        }else{
            //---------------------------------------------------------------------
            //NO TOKEN - LOGIN
            //---------------------------------------------------------------------
            //print("FBSDKAccessToken.currentAccessToken() is nil = needs to login")

            let login = FBSDKLoginManager()
            //If `[FBSDKAccessToken currentAccessToken]` is not nil, it will be treated as a reauthorization for that user
            //and will pass the "rerequest" flag to the login diaself.log.
            
            login.logIn(withReadPermissions: ["public_profile", "user_events"],
                from: self,
                handler:  { (result, error) -> Void in
                    //---------------------------------------------------------------------
                    //Will open Safari
                    //which will redirect to  fb1083378095037687://authorize/#state=%7B%22
                    //Which will enter through AppDelegate iOS9 openURL CALLED:
                    //---------------------------------------------------------------------
                    if let error = error{
                        self.log.error("User hit Cancel/Done on Request Permission web page: error:\(error)")
                    }else{
                        //---------------------------------------------------------------------
                        //LOGIN OK
                        //---------------------------------------------------------------------
                        
                        if let result = result{
                            if result.isCancelled{
                                self.log.error("User hit Cancel/Done on Request Permission web page")
                                
                            }else{
                                //-------------------------------------------------------------------
                                //User granted all/some permission
                                //-------------------------------------------------------------------
                                self.log.info("result: \(result) isCancelled: \(result.isCancelled) token: \(result.token)")
                                self.log.info("declinedPermissions: \(result.declinedPermissions)")
                                self.log.info("grantedPermissions: \(result.grantedPermissions)")
                                //-------------------------------------------------------------------
                                self.accessToken = result.token
                                //-------------------------------------------------------------------
                                //[Info_4] result: <FBSDKLoginManagerLoginResult: 0x7f9a64a07800> isCancelled: false token: <FBSDKAccessToken: 0x7f9a64a00d10>
                                //[Info_4] declinedPermissions: [] grantedPermissions: [user_friends]
                                //-------------------------------------------------------------------
                                self.getUserEvents()
                                //-------------------------------------------------------------------
                                
                            }
                        }else{
                            self.log.error("result is nil")
                        }
                    }

            })
        }
    }
    
    
    @IBAction func buttonRefresh_Action(_ sender: AnyObject) {
        getUserEvents()
    }
    
    
    func getUserEvents() {
        if FBSDKAccessToken.current() != nil {
 // TODO: -
//            if FBSDKAccessToken.currentAccessToken().hasGranted("email") && FBSDKAccessToken.currentAccessToken().hasGranted("public_profile") {
//                35
//                36	        }
            
            //---------------------------------------------------------------------
            //print(FBSDKAccessToken.currentAccessToken())
            //---------------------------------------------------------------------

            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "events"]).start(completionHandler: { (connection, result, error) -> Void in
                if error == nil {
                    //print("----------------------")
                    //print(result)
                    //print("----------------------")
                    //print(result as! NSDictionary)
                    
                    if let resultNSDictionary = result as? NSDictionary {
                        //print("resultNSDictionary:\(resultNSDictionary)")
                        //print("resultNSDictionary.allKeys:\(resultNSDictionary.allKeys)")
                        //resultNSDictionary.allKeys:[id, events]
                        //print("----------------------")
                        
                        if let colcFacebookEventSearchResult : COLCFacebookEventSearchResult = Mapper<COLCFacebookEventSearchResult>().map(JSONObject:resultNSDictionary){
                          
                            if colcFacebookEventSearchResult.events.count > 0{
                                //for (key, value) in colcFacebookEventSearchResult.events {
                                for ( _ , value) in colcFacebookEventSearchResult.events {
                                    //print("key[\(key)]")
                                    //print("value[\(value)]")
                                    
                                    switch value{
                                    case let number as NSNumber:
                                        print("NSNumber:\(number)")
                                    case let array as NSArray:
                                        //print("NSArray:\(array)")
                                        
                                        for valueInArray in array {
                                            
                                            //print("valueInArray[\(valueInArray)]")
                                            
                                            switch valueInArray{
                                            case let number as NSNumber:
                                                print("NSNumber:\(number)")
                                            case let array as NSArray:
                                                print("NSArray:\(array)")
                                                
                                                
                                            case let nsDictionary as NSDictionary:
                                                //print("NSDictionary:\(nsDictionary)")
                                                //print("nsDictionary.allKeys:\(nsDictionary.allKeys)")
                                                //nsDictionary.allKeys:[end_time, id, start_time, rsvp_status, description, place, name]
                                                //print("")
                                                
                                                if let colcFacebookEvent : COLCFacebookEvent = Mapper<COLCFacebookEvent>().map(JSONObject:nsDictionary){
                                                    
                                                    self.arrayCOLCFacebookEvent.append(colcFacebookEvent)
                                                    
                                                    //print("colcFacebookEvent:\(colcFacebookEvent)")
                                                }else{
                                                    self.log.error("clkPlaceSearchResponse is nil or not CLKGooglePlaceSearchResponse")
                                                }
                                            default:
                                                print("UNKNOWN TYPE")
                                            }
                                            
                                            //print("*****************")
                                        }
                                        
                                    case let nsDictionary as NSDictionary:
                                        print("NSDictionary:\(nsDictionary)")
                                    default:
                                        print("UNKNOWN TYPE")
                                    }
                                    
                                    //print("*****************")
                                }
                            }else{
                                CLKAlertController.showAlertInVC(self, title: "No events found", message: "No events found for logged in user.\rPlease log in and out of Facebook using the Logout button to reauthorise this app and allow access to User's Events")
                            }

                        }else{
                            self.log.error("clkPlaceSearchResponse is nil or not CLKGooglePlaceSearchResponse")
                        }
                    
                    }else{
                        self.log.error("result as? NSDictionary is nil")
                    }
                    
                     //print("----------------------")
                    
                    self.tableView.reloadData()
                }else{
                     self.log.error("FBSDKGraphRequest FAILED:\(error)")
                }
            })
            
            //---------------------------------------------------------------------
            // TODO: - get creator of event so we can show whos birthday it is
            //---------------------------------------------------------------------
            //FBSDKGraphRequest(graphPath: "/330742683932626", parameters: ["fields" : "owner"]).start(completionHandler: { (connection, result, error) -> Void in
            //    //---------------------------------------------------------------------
            //    if error == nil {
            //        //print("----------------------")
            //        print(result)
            //        //print("----------------------")
            //        print(result as! NSDictionary)
            //        
            //        if let resultNSDictionary = result as? NSDictionary {
            //            print("resultNSDictionary:\(resultNSDictionary)")
            //            
            //        }else{
            //            self.log.error("result as? NSDictionary is nil")
            //        }
            //        
            //        //print("----------------------")
            //        
            //        self.tableView.reloadData()
            //    }else{
            //        self.log.error("FBSDKGraphRequest FAILED:\(error)")
            //    }
            //    //---------------------------------------------------------------------
            //})
            //---------------------------------------------------------------------
        }else{
            self.log.error("FBSDKAccessToken.currentAccessToken() is nil")
        }
    }

    
    
    func dumpCurrentAccessToken(){
        //        public var appID: String! { get }
        //
        //        /*!
        //        @abstract Returns the known declined permissions.
        //        */
        //        public var declinedPermissions: Set<NSObject>! { get }
        //
        //        /*!
        //        @abstract Returns the expiration date.
        //        */
        //        @NSCopying public var expirationDate: NSDate! { get }
        //
        //        /*!
        //        @abstract Returns the known granted permissions.
        //        */
        //        public var permissions: Set<NSObject>! { get }
        //
        //        /*!
        //        @abstract Returns the date the token was last refreshed.
        //        */
        //        @NSCopying public var refreshDate: NSDate! { get }
        //
        //        /*!
        //        @abstract Returns the opaque token string.
        //        */
        //        public var tokenString: String! { get }
        //
        //        /*!
        //        @abstract Returns the user ID.
        //        */
        //        public var userID: String! { get }
        
        if let currentAccessToken : FBSDKAccessToken = FBSDKAccessToken.current(){
            var tokenValues = "\rcurrentAccessToken *******\r"
            tokenValues = tokenValues + "tokenString   :[\(currentAccessToken.tokenString)]\r"
            tokenValues = tokenValues + "userID        :[\(currentAccessToken.userID)]\r"
            tokenValues = tokenValues + "appID         :[\(currentAccessToken.appID)]\r"
            tokenValues = tokenValues + "refreshDate   :[\(currentAccessToken.refreshDate)][Date last refreshed]\r"
            tokenValues = tokenValues + "expirationDate:[\(currentAccessToken.expirationDate)]\r"
            tokenValues = tokenValues + "permissions   :\r[\(currentAccessToken.permissions)]\r"
            tokenValues = tokenValues + "declinedPermissions   :\r[\(currentAccessToken.declinedPermissions)]\r"
            tokenValues = tokenValues + "currentAccessToken *******\r"
            self.log.info("\(tokenValues)")
            
        }else{
            self.log.error("currentAccessToken is nil")
        }
    }
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - TABLEVIEW
    //--------------------------------------------------------------
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat{
        return 170.0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return UITableViewAutomaticDimension
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCOLCFacebookEvent.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cellIdentifier = "COLCFacebookEventTableViewCell"
        
        let colcFacebookEventTableViewCell : COLCFacebookEventTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! COLCFacebookEventTableViewCell
        
        colcFacebookEventTableViewCell.applyCustomFont()
        //------------------------------------------------
        let colcFacebookEvent : COLCFacebookEvent = arrayCOLCFacebookEvent[(indexPath as NSIndexPath).row]
        //------------------------------------------------
        colcFacebookEventTableViewCell.labelId?.text = "..."
        if let id = colcFacebookEvent.id as? String{
            colcFacebookEventTableViewCell.labelId?.text = id
            
        }else{
            self.log.error("colcFacebookEvent.id as? String is nil")
        }
        //------------------------------------------------
        colcFacebookEventTableViewCell.labelName?.text = "..."
        if let name = colcFacebookEvent.name as? String{
            colcFacebookEventTableViewCell.labelName?.text = name
            
        }else{
            self.log.error("colcFacebookEvent.name as? String is nil")
        }
        //------------------------------------------------
        colcFacebookEventTableViewCell.labelPlace?.text = "..."
        if let colcFacebookPlace : COLCFacebookPlace = colcFacebookEvent.place{
            if let name = colcFacebookPlace.name as? String{
                
                if colcFacebookPlace.formattedAddress == "" {
                    colcFacebookEventTableViewCell.labelPlace?.text = name
                    
                }else{
                    colcFacebookEventTableViewCell.labelPlace?.text = "\(name), \(colcFacebookPlace.formattedAddress)"
                }
            }else{
                self.log.error("colcFacebookPlace.name is nil")
            }
        }else{
            self.log.error("colcFacebookEvent.place is nil")
        }
        //------------------------------------------------
        if let rsvp_status = colcFacebookEvent.rsvp_status as? String{
            colcFacebookEventTableViewCell.labelRSVPStatus?.text = "RSVP: \(rsvp_status)"
            
        }else{
            self.log.error("colcFacebookEvent.rsvp_status as? String is nil")
            colcFacebookEventTableViewCell.labelRSVPStatus?.text = "..."
        }
        //------------------------------------------------
        if let start_time = colcFacebookEvent.start_time as? String{
            
            //"2016-05-03T00:00:00"
            if let nsDate =  UTCStringToNSDate(start_time) {
                if let dateString =  dateStringInDisplayFormat(nsDate) as? String {
                    
                    if nsDate.compare(Date()) == ComparisonResult.orderedDescending {
                        colcFacebookEventTableViewCell.labelStartTime?.text = "\(dateString) [Upcoming]"
                        colcFacebookEventTableViewCell.contentView.backgroundColor = UIColor.white
                    }else{
                        colcFacebookEventTableViewCell.labelStartTime?.text = "\(dateString) [Past]"
                        colcFacebookEventTableViewCell.contentView.backgroundColor = AppearanceManager.appColorLight_DeSelectedColor
                    }
                    
                    
                }else{
                     colcFacebookEventTableViewCell.labelStartTime?.text = ""
                }
            }else{
                 colcFacebookEventTableViewCell.labelStartTime?.text = ""
            }
            
        }else{
            self.log.error("colcFacebookEvent.start_time as? String is nil")
            colcFacebookEventTableViewCell.labelStartTime?.text = "..."
        }
        //------------------------------------------------
        if let end_time = colcFacebookEvent.end_time as? String{
            
            //"2016-05-03T00:00:00"
            if let nsDate =  UTCStringToNSDate(end_time) {
                if let dateString =  dateStringInDisplayFormat(nsDate) {
                    colcFacebookEventTableViewCell.labelEndTime?.text = dateString as String
                }else{
                    colcFacebookEventTableViewCell.labelEndTime?.text = ""
                }
            }else{
                colcFacebookEventTableViewCell.labelEndTime?.text = ""
            }
            
        }else{
            //not always set self.log.error("colcFacebookEvent.end_time as? String is nil")
            colcFacebookEventTableViewCell.labelEndTime?.text = "..."
        }
        //------------------------------------------------
        colcFacebookEventTableViewCell.labelDescription?.text = "..."
        return colcFacebookEventTableViewCell
    }
    
    
    //--------------------------------------------------------------
    // MARK: - FACEBOOK DATE
    // MARK: -
    //--------------------------------------------------------------
    func UTCStringToNSDate(_ utcDateString: String) -> Date?{
        
        var dateReturned : Date? = nil
        
        let dateFormatterIN = DateFormatter()
        //"2016-06-13T19:45:00+0100"
        //dateFormatterIN.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        //dateFormatterIN.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZ"
        dateFormatterIN.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZ"
        
        //dateFormatterIN.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
        let dateInUserTimeZone = dateFormatterIN.date(from: utcDateString)
        
        if let dateInUserTimeZone = dateInUserTimeZone{
            ////print("dateInUserTimeZone:[\(dateInUserTimeZone)")
            dateReturned = dateInUserTimeZone
        }else{
            self.log.error("dateFromStringUTC is nil")
        }
        
        return dateReturned
    }
    
    
    func dateStringInDisplayFormat(_ dateIN: Date) -> NSString?{
        
        var dateStringReturned : NSString? = nil
        
        let dateFormatterIN = DateFormatter()
        
        //"11 Feb 2016"
        dateFormatterIN.dateFormat = "dd MMM yyyy HH:mm"
        
        // TODO: - Fixtures do we need to support timeZone
        //dateFormatterIN.timeZone = self.userTimeZone()
        dateStringReturned = dateFormatterIN.string(from: dateIN) as NSString?
        
        return dateStringReturned
        
    }
    
    
    
    
    
    //--------------------------------------------------------------
    // MARK: - TABLE: DID SELECT ROW
    // MARK: -
    //--------------------------------------------------------------
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        
        //print("\(colcFacebookEvent.name)")

        //---------------------------------------------------------------------
        //GEOCODE
        //---------------------------------------------------------------------
        //get the address
        //you can have plce set but not lat lng so need to do a mapkit search for address
        //user entered address as string not picked on map
        //---------------------------------------------------------------------
        /*
        events =     {
            data =         (
                {
                    id = 330742683932626;
                    name = "Birthday drinks";
                    place =                 {
                        name = "The Old George, 379 Bethnal Green Rd, London E2 0AN";
                    };
                    "rsvp_status" = attending;
                    "start_time" = "2016-10-08T20:00:00+0100";
                },
                */
        
        //---------------------------------------------------------------------
        print("FB api removed form TFLApiPlace app - gets commented in when you drag common code into a new app - CLKFacebookEventPlace() fails")
        
        /*
        let selectedCOLCFacebookEvent = arrayCOLCFacebookEvent[(indexPath as NSIndexPath).row]
        
   
        let clkFacebookEventPlace = CLKFacebookEventPlace(colcFacebookEvent: selectedCOLCFacebookEvent)
        
        //in parent class
        geocodeCLKPlace(clkPlace:clkFacebookEventPlace)
 */

    }
    
    
    //should subclass - as mention Facebook or calendar
    override func showErrorGettingAddress(){
        //SUBCLASSED THIS DONT MENTION FB IN PARENT:
        CLKAlertController.showAlertInVC(self, title: "Error getting exact address. Please check the event on Facebook", message: "Facebook event address is empty")
    }
    
    // TODO: - make generic move up
    override func returnToDelegate(clkPlace: CLKPlace){
        if let delegate = self.delegate {

            //---------------------------------------------------------------------
            let clkPlacesPickerResultPlace = CLKPlacesPickerResultPlace()
            clkPlacesPickerResultPlace.clkPlaceSelected = clkPlace
            
            delegate.fbViewControllerSelectFacebookEvent(self, clkPlacesPickerResultPlace:clkPlacesPickerResultPlace)
            //---------------------------------------------------------------------
            
        }else{
            self.log.error("self.delegate is nil")
        }

    }
    
    @IBAction func buttonCancel_Action(_ sender: AnyObject) {
        
        //self.navigationController?.popViewControllerAnimated(true)
        
        self.dismiss(animated: true, completion: {
            
            //------------------------------------------------
            if let delegate_ = self.delegate {
                //containerView is hidden by the delegate
                delegate_.fbViewControllerCancelled(self)
            }else{
                self.log.error("self.delegate is nil")
            }
            //------------------------------------------------
        })
    }
    
}

