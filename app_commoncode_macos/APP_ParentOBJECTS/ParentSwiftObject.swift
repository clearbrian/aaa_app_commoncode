//
//  ParentSwiftObject.swift
//  SandPforiOS
//
//  Created by Brian Clear on 17/06/2015.
//  Copyright (c) 2015 CLARKSONS RESEARCH SERVICES LIMITED. All rights reserved.
//

import Foundation

class ParentSwiftObject{
    
    
    
    //------------------------------------------------
    //LOGGING - Requires XCodeColors plugin
    //MUST BE DECLARED IN EACH CLASS THAT USES IT
    
    //let log = MyXCodeColorsLogger.defaultInstance
    let log = MyXCodeEmojiLogger.defaultInstance
    
    //create seperate instance for each VC so we can turn them on/off as we develop them

    //------------------------------------------------
    
    //SUB CLASS MUST CALL super.init else loggin not configured
    init() {
        //super.init()
        
        configureLogging()
    }
    
    func configureLogging(){
        //log = MyXCodeColorsLogger is declared above @ UIApplicationMain
        
        //------------------------------------------------
        //LOG LEVEL
        //------------------------------------------------
        //case Verbose = 1 << SHOW EVERYTHING
        //case Debug
        //case Info
        //case Warning
        //case Error    << RELEASE
        //case None     << ALL OFF
        //------------------------------------------------
//        self.log.outputLogLevel = .Verbose_5
        //------------------------------------------------
        //[fg220,50,47;[Error] This is Error Log [;[fg88,110,117;[AppDelegate.swift:34] testLogging() 2015-06-09 11:14:15.811[;
        
        //------------------------------------------------
        //LOG FORMAT
        //------------------------------------------------
//        self.log.showFileInfo = true
//        self.log.showDate = true
//        self.log.showLogLevel = true
//        self.log.showFunctionName = true
    }
}
