//
//  UIColor+RandomColor.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 27/01/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
import UIKit

// http://stackoverflow.com/questions/29779128/how-to-make-a-random-background-color-with-swift
//USAGE self.view.backgroundColor = .randomColor()
extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}

extension UIColor {
    static func randomColor() -> UIColor {
        return UIColor(red:   .random(),
                       green: .random(),
                       blue:  .random(),
                       alpha: 1.0)
    }
}
