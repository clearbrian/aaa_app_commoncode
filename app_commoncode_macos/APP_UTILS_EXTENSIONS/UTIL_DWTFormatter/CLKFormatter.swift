//
//  CLKFormatter.swift
//  SandPforiOS
//
//  Created by Brian Clear on 17/06/2015.
//  Copyright (c) 2015 CLARKSONS RESEARCH SERVICES LIMITED. All rights reserved.
//

import Foundation

class CLKFormatter: ParentSwiftObject{
    static func formatDWT(_ numIN:NSNumber?) -> String?{
        var formattedText_ : String? = nil
        
        if let num_ = numIN{
            let formatter = NumberFormatter()
            formatter.numberStyle = NumberFormatter.Style.decimal //need 1000000 >> 1,000,000
            
            var longValueToFormat_: Int64 = -1
            
            //get Int64 to format
            if num_.doubleValue > 0.0 && num_.doubleValue < 701{
                
                longValueToFormat_ = num_.int64Value * 1000
                
            }else if num_.doubleValue > 999999999999.0{
                //dont format can go to negative num due to rounding error
                //longValueToFormat_ stays at  -1 for formatting skipped
            }else{
                // >  700 leave as is
                //e.g. 1000 is usually 1000 DWT
                //but 10 > 10,000,000
                
                //dont add ,000 use original value
                longValueToFormat_ = num_.int64Value
                
            }
            //------------------------------------------------
            if longValueToFormat_ == -1{
                //dont format - return as is
                print("CLKFormatter: formatDWT: STATIC ERROR:longValueToFormat_ not set")
            }else{
                let number_: NSNumber = NSNumber(value: longValueToFormat_ as Int64)
                
                //pass through as Decimal
                if let numString = formatter.string(from: number_){
                    //FORMATTED NUMBER 1000000 >>> 1,000,000
                    formattedText_ = numString
                }else{
                    print("CLKFormatter: formatDWT: STATIC ERROR:numString is nil")
                }
            }
        }else{
            print("CLKFormatter: formatDWT: STATIC ERROR:numIN is nil")
        }
        
        //can be nil
        return formattedText_
    }
    
    //1000000 >> 1,000,000
    static func formatAsDecimalString(_ numString: String) -> String{
        var decimalNumString_ = ""
        
        if numString.trim() == ""{
            print("ERROR: formatAsDecimalString: numString.trim() == ''")
        }else{

            //------------------------------------------------
            let formatterIN_ = NumberFormatter()
            //can take 1000000 or 1,000,000
            formatterIN_.numberStyle = NumberFormatter.Style.decimal

            if let num_ = formatterIN_.number(from: numString){
                
                if let decimalNumString = formatterIN_.string(from: num_){
                    decimalNumString_ = decimalNumString
                }else{
                    print("ERROR: decimalNumString is nil for numString:\(numString)")
                }
                
                
            }else{
                print("ERROR:num_ is nil for numString:\(numString)")
            }
            //------------------------------------------------
        }
        return decimalNumString_
    }
    static func formatAsDecimalString(_ numString: String,  maximumFractionDigits: Int) -> String{
        var decimalNumString_ = ""
        
        if numString.trim() == ""{
            print("POSSIBLE ERROR: formatAsDecimalString:maximumFractionDigits: numString.trim() == ''")
        }else{
            
            //------------------------------------------------
            let formatterIN_ = NumberFormatter()
            //can take 1000000 or 1,000,000
            formatterIN_.numberStyle = NumberFormatter.Style.decimal
            
            //formatterIN_.maximumFractionDigits = 2
            formatterIN_.maximumFractionDigits = maximumFractionDigits
            
            
            if let num_ = formatterIN_.number(from: numString){
                
                if let decimalNumString = formatterIN_.string(from: num_){
                    decimalNumString_ = decimalNumString
                }else{
                    print("ERROR: decimalNumString is nil for numString:\(numString)")
                }
                
                
            }else{
                print("ERROR:num_ is nil for numString:\(numString)")
            }
            //------------------------------------------------
        }
        return decimalNumString_
    }
    
    
    
    
    //DIDNT WORK 00 in date was throwing for the formatter
    //Didnt want to replace 00 with 01 as this isnt what broker had entered
    //use other version of the  method below this it trims the first 4 chars from
    //1945-01-00 >> 1945
    //2003-00-00 >> 2003
    
    //THIS VERSION - DONT USE
    //BuiltDate has two formats
    //1990-00-00 - NAME: ZVYOZDOCHKA
    //1990-01-00
    //both should return year 1990
    //KNOWN ISSUE - real dates dont include 00 so formatting 1945-01-00 >> YYYY-MM-00 >> 1944-12-23 23:00 but >>YYYY will return correct year
    //KNOWN ISSUE - real dates dont include 00 so formatting 2003-00-00 >> YYYY-MM-00 >> 2002-12-22 00:00 but >>YYYY DOESNT return correct year
    //so
    //so basic rule is only trust YYYY
    static func parseBuiltDateYear(_ dateString: String, formatString:String) -> String{
        var builtDateYear = ""
        
        if dateString.trim() == ""{
            print("ERROR:error")
        }else{
            if formatString.trim() == ""{
                print("ERROR:error")
            }else{
                //------------------------------------------------
                let formatterIN_ = DateFormatter()
                //formatterIN_.dateFormat = "YYYY-MM-00"
                //formatterIN_.dateFormat = "YYYY-00-00"
                formatterIN_.dateFormat = formatString
                
                //let date_ = formatterIN_.dateFromString(vesselSearchResult.Built)
                let date_ = formatterIN_.date(from: dateString)
                if(date_ != nil){
                    let formatterOUT_ = DateFormatter()
                    formatterOUT_.dateFormat = "YYYY"
                    
                    let dateYYYYString_ = formatterOUT_.string(from: date_!)
                    if dateYYYYString_.trim() == "" {
                        //cant parse so just return ""
                        //externally will show the orig string
                    }else{
                        builtDateYear = dateYYYYString_
                    }
                }else{
                    print("ERROR:date_ is nil for dateString:\(dateString) formatString:\(formatString) ")
                }
                //------------------------------------------------
            }
        }
        return builtDateYear
    }
    

    //CASE 0:1984-05-04 >> "Mar 1984"(Eng) or >> "Mai 1984"(Nor)
    //CASE 1:1984-05-00 >> "Mar 1984"(Eng) or >> "Mai 1984"(Nor)
    //CASE 2:1984-00-00 >> "1984" (Eng)/(Nor)
    
    //var s1 = CLKFormatter.parseBuiltDateYear("1984-03-04")
    //var s2 = CLKFormatter.parseBuiltDateYear("1984-03-00")
    //var s3 = CLKFormatter.parseBuiltDateYear("1984-00-00")
    //        (lldb) po s1
    //        "Mar 1984"
    //
    //        (lldb) po s2
    //        "Mar 1984"
    //
    //        (lldb) po s3
    //

    static func parseBuiltDateYear(_ dateString: String) -> String{
        //any errors just retun original
        var builtDateYear = dateString
        
        if dateString.trim() == ""{
            print("ERROR:error")
        }else{
            if dateString.length == 4{
                //sales search result year often '1984'
                builtDateYear = dateString
            }
            else if dateString.length == 10{
                
                if dateString.hasSuffix("-00-00"){
                    //---------------------------------------------------------------------
                    //CASE 2:1984-00-00
                    //---------------------------------------------------------------------
                    let YYYYString_ = dateString.substr(0, length: 4)
                    //it it a number
                    
                    let formatter = NumberFormatter()
                    formatter.numberStyle = NumberFormatter.Style.none
                    let num_ = formatter.number(from: YYYYString_)
                    if let num = num_{
                        //is a valid number
                        if num.int32Value > 1800 && num.int32Value < 2070{
                            //is a valid year
                            builtDateYear = YYYYString_
                        }else{
                            print("ERROR:can't parse YYYY not between 1900 and 2070:\(YYYYString_)")
                        }
                    }else{
                        print("ERROR:can't parse YYYY as Number:\(YYYYString_)")
                    }
                }else{
                    //---------------------------------------------------------------------
                    //CASE 0:1984-03-04
                    //CASE 1:1984-03-00
                    //---------------------------------------------------------------------
                    var YYYY_MM_DD_String_ = ""
                    
                    if dateString.hasSuffix("-00"){
                        //CASE 1:1984-03-00
                        
                        //1984-03-00
                        let YYYY_MM_String_ = dateString.substr(0, length: 7)
                        
                        //change 00 to 01 else wont parse as date correctly
                         YYYY_MM_DD_String_ = "\(YYYY_MM_String_)-01"
                        
                        
                    }else{
                        //CASE 0:1984-03-04
                        YYYY_MM_DD_String_ = dateString
                    }
                    //should be valid date string
                    //CASE 0:1984-03-04
                    //CASE 1:1984-03-00 >> CASE 1:1984-03-01
                    let dateFormatterIN = DateFormatter()
                    dateFormatterIN.dateFormat = "yyyy-MM-dd"
                    let dateIN_ = dateFormatterIN.date(from: YYYY_MM_DD_String_)
                    if let dateIN_ = dateIN_{
                        
                        let dateFormatterOUT = DateFormatter()
                        //dont use string need to output in device local for norwegian
                        //dateFormatterOUT.dateStyle = .MediumStyle
                        
                        dateFormatterOUT.dateFormat = "MMM yyyy"
                        let dateOUTSTRING_ = dateFormatterOUT.string(from: dateIN_)
                        builtDateYear = dateOUTSTRING_
                        
                    }else{
                        print("ERROR:can't parse YYYY_MM_DD_String_ as Number:\(YYYY_MM_DD_String_)")
                    }
                    
                    
                }
            }else{
                print("ERROR:can't parse dateString as Number:\(dateString) [dateString.length not 4[1986] or 10[1987-00-00]] fails")
            }
        }
        return builtDateYear
    }
    
    //<BuiltEnd>2021-01-01T00:00:00</BuiltEnd>
    //<BuiltStart>2021-01-01T00:00:00</BuiltEnd>
    static func parseDate(_ dateIN: Date?) -> String{
        var dateStringOUT_ = ""

        if let dateIN = dateIN{
            let formatterOUT_ = DateFormatter()
            formatterOUT_.dateFormat = "yyyy-MM-dd'T'00:00:00"
            //"" if fails
            dateStringOUT_ = formatterOUT_.string(from: dateIN)
            
        }else{
            print("STATIC ERROR:dateIN is nil")
        }
        
                //------------------------------------------------

        return dateStringOUT_
    }
    //<Date>2015-12-26T00:00:00</Date>
    //returns string with Date in local mediumDate
    static func parseDateToDisplay(_ dateStringIN: String?) -> String{
        var dateStringReturned_ = ""
        
        if let dateStringIN = dateStringIN{
            let formatterIN_ = DateFormatter()
            formatterIN_.dateFormat = "yyyy-MM-dd'T'00:00:00"
            //"" if fails
            //"2015-12-26T00:00:00" >> NSDate
            if let dateIN_ = formatterIN_.date(from: dateStringIN){
                let formatterOUT_ = DateFormatter()
                
                //MUST USE STANDARD STYLE not format string so it appears correctly in norwegian
                formatterOUT_.dateStyle = .medium
                
                //"" if fails
                //"2015-12-26T00:00:00" >> NSDate
                let dateStringOUT_ = formatterOUT_.string(from: dateIN_)
                
                if dateStringOUT_.trim() == ""{
                    print("STATIC ERROR: cant output medium date")
                }else{
                    dateStringReturned_ = dateStringOUT_
                }
            }else{
                print("STATIC ERROR: cant parse \(dateStringIN) using \(formatterIN_.dateFormat) >> NSDate nil")
            }
        }else{
            print("STATIC ERROR:dateIN is nil")
        }
        //------------------------------------------------
        
        return dateStringReturned_
    }
    
    

    //Get 1 jan of current year for NSDate
    //used for VesselSalesSearch - Start/End - show sales in this year
    static func parseDateForStartOfCurrentYear() -> Date?{
        var dateOUT_ :Date?
        
        //get only the yyyy for current year
        let currentDate = Date()

        let formatterOUT_ = DateFormatter()
        formatterOUT_.dateFormat = "yyyy"
        //"" if fails
        let currentDate_YYYY = formatterOUT_.string(from: currentDate)
        
        if currentDate_YYYY.trim() == ""{
            print("static error: cant parseDateForStartOfYear -  currentDate_YYYY is ''")
        }else{
            //year is in stringYYYY_
            //jan 1
            let stringJan1 = "01-01-\(currentDate_YYYY)"
            
            let formatterOUT_ = DateFormatter()
            formatterOUT_.dateFormat = "dd-MM-yyyy"
            dateOUT_ = formatterOUT_.date(from: stringJan1)
        }
        //------------------------------------------------
        
        return dateOUT_
    }
    static func parseDateForEndOfCurrentYear() -> Date?{
        var dateOUT_ :Date?
        
        //get only the yyyy for current year
        let currentDate = Date()
        
        let formatterOUT_ = DateFormatter()
        formatterOUT_.dateFormat = "yyyy"
        //"" if fails
        let currentDate_YYYY = formatterOUT_.string(from: currentDate)
        
        if currentDate_YYYY.trim() == ""{
            print("static error: cant parseDateForStartOfYear -  currentDate_YYYY is ''")
        }else{
            //year is in stringYYYY_
            //dec 31
            let stringJan1 = "31-12-\(currentDate_YYYY)"
            
            let formatterOUT_ = DateFormatter()
            formatterOUT_.dateFormat = "dd-MM-yyyy"
            dateOUT_ = formatterOUT_.date(from: stringJan1)
        }
        //------------------------------------------------
        
        return dateOUT_
    }
}
