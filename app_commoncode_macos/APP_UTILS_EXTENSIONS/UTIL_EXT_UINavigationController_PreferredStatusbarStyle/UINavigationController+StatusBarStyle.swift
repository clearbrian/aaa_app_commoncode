//
//  UINavigationController+StatusBarStyle.swift
//  joyride
//
//  Created by Brian Clear on 01/03/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation

//
//  UINavigationController+StatusBarStyle.swift
//  ClarksonsArc
//
//  Created by Brian Clear on 03/09/2015.
//  Copyright (c) 2015 Clarksons. All rights reserved.
//

import Foundation
import UIKit

//  UINavigationController+StatusBarStyle.h:

extension UINavigationController{
    
    open override var preferredStatusBarStyle : UIStatusBarStyle {
        //should be set in
        //ParentTableViewController
        //ParentViewController
        //extension UINavigationController
        //return .LightContent
        return AppearanceManager.preferredStatusBarStyle
    }
}
