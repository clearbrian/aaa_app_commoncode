//
//  Array+Append.swift
//  joyride
//
//  Created by Brian Clear on 15/07/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
extension Array{
    static func appendToAllExceptLast(array: Array<String>, char: String = ",") -> String{
        var stringToAppendTo = ""
        for item in array{
            //CollectionType.last
            if array.last == item{
                stringToAppendTo = stringToAppendTo + "\(item)"
            }else{
                stringToAppendTo = stringToAppendTo + "\(item)\(char) "
            }
        }
        return stringToAppendTo
    }
}


class UtilArray{
    
    static func appendToAllExceptLast(array: Array<String>, char: String = ",") -> String{
        var stringToAppendTo = ""
        for item in array{
            if item == ""{
                //skip blank lines
            }else{
                //CollectionType.last
                if array.last == item{
                    stringToAppendTo = stringToAppendTo + "\(item)"
                }else{
                    stringToAppendTo = stringToAppendTo + "\(item)\(char) "
                }
            }

        }
        return stringToAppendTo
    }
}
