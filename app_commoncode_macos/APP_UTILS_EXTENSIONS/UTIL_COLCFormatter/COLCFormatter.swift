//
//  COLCFormatter.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 17/01/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
class COLCFormatter{
    class func formatToTwoDecimalPlaces(doubleValue: Double) -> String{
        
        var resultString = "\(doubleValue)"
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        numberFormatter.maximumFractionDigits = 2
        
        let doubleNumber = NSNumber.init(value: doubleValue)
        if let resultString_ = numberFormatter.string(from: doubleNumber){
            resultString = resultString_
        }else{
            appDelegate.log.error("numberFormatter.string(from: doubleNumber) is nil")
        }
        
        return resultString
    }
}
