//
//  Safe.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 19/01/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
// TODO: - COMMENT ass safe() to all Optionals
/*
 https://github.com/ethan605/dev-with-ethan/tree/5975efab1663a8a0724c86c4bfcdb3208f3f1933/assets/media/snippets/posts/2016-07-29
 extension Optional<T> where T : String{
 
 }
 */

class Safe{
    //--------------------------------------------------------------
    // MARK: - debugging - used to dump optionsal quickly
    // MARK: -
    //--------------------------------------------------------------
    class func safeString(_ stringOpt: String?) -> String{
        var safeString = ""
        if let iVarSafe = stringOpt {
            safeString = iVarSafe
        }else{
            // use def
        }
        return "\(safeString)"
    }
    
    // "123.567" >> 123.456 or if nil ""
    class func safeFloatAsString(_ floatOpt: Float?) -> String{
        var safeString = ""
        if let iVarSafe = floatOpt {
            safeString = "\(iVarSafe)"
        }else{
            // use def
        }
        return safeString
    }
    // "123.567" >> 123.456 or if nil ""
    class func safeDoubleAsString(_ doubleOpt: Double?) -> String{
        var safeString = ""
        if let iVarSafe = doubleOpt {
            safeString = "\(iVarSafe)"
        }else{
            // use def
        }
        return safeString
    }
    // "123" >> 123 or if nil ""
    class func safeIntAsString(_ intOpt: Int?) -> String{
        var safeString = ""
        if let iVarSafe = intOpt {
            safeString = "\(iVarSafe)"
        }else{
            // use def
        }
        return safeString
    }
}
