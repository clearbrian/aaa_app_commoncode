//
//  CLKTrip.swift
//  joyride
//
//  Created by Brian Clear on 01/12/2015.
//  Copyright © 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation
class CLKTrip : ParentSwiftObject{
    var name: String = ""
    var openWithTypeId: Int //OpenWithType.rawvalue
    var dateTime: Date = Date()
    var clkPlaceStart: CLKPlace
    var clkPlaceEnd: CLKPlace
    
    required init?(name: String, openWithType: OpenWithType, dateTime: Date, clkPlaceStart: CLKPlace, clkPlaceEnd: CLKPlace){
        self.name = name
        self.openWithTypeId = openWithType.rawValue
        self.clkPlaceStart = clkPlaceStart
        self.clkPlaceEnd = clkPlaceEnd
    }
}
