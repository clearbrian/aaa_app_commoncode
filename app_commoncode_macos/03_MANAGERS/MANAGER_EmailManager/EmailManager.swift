//
//  EmailManager.swift
//  ClarksonsArc
//
//  Created by Brian Clear on 08/09/2015.
//  Copyright (c) 2015 Clarksons. All rights reserved.
//

import Foundation
import MessageUI

class EmailManager : ParentNSObject, MFMailComposeViewControllerDelegate{
    var viewController:UIViewController?
    
    func sendEmailFromVC(_ viewController:UIViewController){
        //blank body
        sendEmailFromVC(viewController,bodyString: nil)
        
    }
    
    func sendEmailFromVC(_ viewController:UIViewController, bodyString: String?){
    
        //mail composer requires a VC to display modally from
        self.viewController = viewController
        
        //-----------------------------------------------------------------------------------
        //Set the nav bar background to white for MFMailComposeViewController
        //As I had trouble tinting the Cancel and Send in MFMailComposeViewController
        //BUt if I set app nav bar to white BEFORE [MFMailComposeViewController alloc] the MFMailComposeViewController takes up the app nav bar color
        //need to set it back when cancelled
        //        UIImage *backgroundwhite44x44   = [[UIImage imageNamed:@"backgroundwhite44x44.png"]
        //                                                              resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)]
        //
        //        [[UINavigationBar appearance] setBackgroundImage:backgroundwhite44x44 forBarMetrics:UIBarMetricsDefault]
        //-----------------------------------------------------------------------------------
        
        //            [appDelegate setUIAppearance_UINavigationBar_for_MFMailComposeViewControllerON]
        //---------------------------------------------------------------------
        //werid bug where value in mailCntl was 0000000
        //---------------------------------------------------------------------
        //let mailCntl = MFMailComposeViewController()
        //----------------------------------------------------
        let mailCntlOpt: MFMailComposeViewController? = MFMailComposeViewController()
        //test for 00000
        if let mailCntl = mailCntlOpt{
            //mailCntl.delegate            = self
            mailCntl.mailComposeDelegate = self
            
            //mailCntl.setSubject("Email from Taxi Ranks - Public Transport London v \(AppVersionManager.bundleShortVersionString())")
            mailCntl.setSubject(AppConfig.EmailManager_subject)
            
            
            
            //            var ownerStr       = ""
            //            var fixStr         = ""
            //            var saleStr        = ""
            //            var specialisedStr = ""
            //            var detailsStr     = ""
            //            var AdditionalInfo = ""
            //
            //            ownerStr = [NSString stringWithFormat:@"<h3>Owner Manager Details</h3><P align=left style='color:gray'>%@</P>",
            //            self.info_Vessel.OwnerManagerDetails ? self.info_Vessel.OwnerManagerDetails : @""]
            //
            //            fixStr = [NSString stringWithFormat:@"<h3>Reported Fixture History</h3><P align=left style='color:gray'>%@</P>",
            //            self.info_Vessel.ReportedFixtureHistory ? self.info_Vessel.ReportedFixtureHistory : @""]
            //
            //            saleStr = [NSString
            //            stringWithFormat:@"<h3>Reported Sale And Purchase History</h3><P align=left style='color:gray'>%@</P>",
            //            self.info_Vessel.ReportedSaleAndPurchaseHistory ? self.info_Vessel.ReportedSaleAndPurchaseHistory : @""]
            //
            //            specialisedStr = [NSString
            //            stringWithFormat:@"<h3>Specialised Details</h3><P align=left style='color:gray'>%@</P>",
            //            self.info_Vessel.SpecialisedDetails ? self.info_Vessel.SpecialisedDetails : @""]
            //
            //            detailsStr = [NSString stringWithFormat:@"<h3>Standard Details</h3><P align=left style='color:gray'>%@</P>",
            //            self.info_Vessel.StandardDetails ? self.info_Vessel.StandardDetails : @""]
            //
            //            AdditionalInfo = [NSString
            //            stringWithFormat:@"<h3>Additional Information</h3><P align=left style='color:gray'>%@</P>",
            //            self.info_Vessel.AdditionalInformation ? self.info_Vessel.AdditionalInformation : @""]
            //
            //            NSString *str_Title = [NSString stringWithFormat:@"<table border=\"1\">\n"
            //            "<tr>\n"
            //            "<td>Name: %@</td>\n"
            //            "<td>Type: %@</td>\n"
            //            "<td>Subtype: %@</td>\n"
            //            "</tr>\n"
            //            "<tr>\n"
            //            "<td>IMO: %@</td>\n"
            //            "<td>Built: %@</td>\n"
            //            "<td>Owner: %@</td>\n"
            //            "</tr>\n"
            //            "<tr>\n"
            //            "<td>DWT: %@</td>\n"
            //            "<td>Bld: %@</td>\n"
            //            "<td>Status: %@</td>\n"
            //            "</tr>\n"
            //            "</table>",
            //            [[self.info_Vessel.VesselSpecs objectForKey:@"Name"] objectForKey:@"text"]      ? [[self.info_Vessel.VesselSpecs objectForKey:@"Name"] objectForKey:@"text"]      : @"",
            //            [[self.info_Vessel.VesselSpecs objectForKey:@"Type"] objectForKey:@"text"]      ? [[self.info_Vessel.VesselSpecs objectForKey:@"Type"] objectForKey:@"text"]      : @"",
            //            [[self.info_Vessel.VesselSpecs objectForKey:@"Subtype"] objectForKey:@"text"]   ? [[self.info_Vessel.VesselSpecs objectForKey:@"Subtype"] objectForKey:@"text"]   : @"",
            //            [[self.info_Vessel.VesselSpecs objectForKey:@"IMONumber"] objectForKey:@"text"] ? [[self.info_Vessel.VesselSpecs objectForKey:@"IMONumber"] objectForKey:@"text"] : @"",
            //            [[self.info_Vessel.VesselSpecs objectForKey:@"Built"] objectForKey:@"text"]     ? [[self.info_Vessel.VesselSpecs objectForKey:@"Built"] objectForKey:@"text"]     : @"",
            //            [[self.info_Vessel.VesselSpecs objectForKey:@"Owner"] objectForKey:@"text"]     ? [[self.info_Vessel.VesselSpecs objectForKey:@"Owner"] objectForKey:@"text"]     : @"",
            //            [[self.info_Vessel.VesselSpecs objectForKey:@"DWT"] objectForKey:@"text"]       ? [[self.info_Vessel.VesselSpecs objectForKey:@"DWT"] objectForKey:@"text"]       : @"",
            //            [[self.info_Vessel.VesselSpecs objectForKey:@"Builder"] objectForKey:@"text"]   ? [[self.info_Vessel.VesselSpecs objectForKey:@"Builder"] objectForKey:@"text"]   : @"",
            //            [[self.info_Vessel.VesselSpecs objectForKey:@"Status"] objectForKey:@"text"]    ? [[self.info_Vessel.VesselSpecs objectForKey:@"Status"] objectForKey:@"text"]    : @""]
            //
            //            //------------------------------------------------
            //            NSString *emailHtmlString = [NSString
            //            stringWithFormat:@"<html><head><STYLE TYPE=\"text/css\"><!--BODY{color:blackfont-family:sans-seriffont-size:14pt}A:link{color:blue}A:visited{color:red}--></STYLE></head><body>%@%@%@%@%@%@%@</body></html>",
            //            str_Title, ownerStr, fixStr, saleStr, specialisedStr, detailsStr, AdditionalInfo]
            //
            //            [mailCntl setMessageBody:emailHtmlString isHTML:YES]
            //---------------------------------------------------------
            //BODY : HTML - optional
            //---------------------------------------------------------
            mailCntl.setToRecipients(["support@cityoflondonconsulting.com"])
            
            //---------------------------------------------------------
            //BODY : HTML - optional
            //---------------------------------------------------------
            //mailCntl.setMessageBody:emailHtmlString isHTML:YES]
            //var bodyString: String
            //mailCntl.setMessageBody(bodyString, isHTML:false)
            
            //---------------------------------------------------------
            //BODY : TEXT - optional
            //---------------------------------------------------------
            if let bodyString = bodyString {
                mailCntl.setMessageBody(bodyString, isHTML:false)
                
            }else{
                //not error just no body
            }
            
            
            self.viewController?.present(mailCntl, animated:true, completion:{
                //prevent ws call again when viewWillAppear triggered when MFMailComposeViewController closes
                //            self.mailVCShownAlready = true
            })
            mailCntl.view.tintColor = AppearanceManager.appColorNavbarAndTabBar
        }else{
            self.log.error("MFMailComposeViewController() returned nil - iOS will display NO MAIL Accounts alert")
        }
        
        
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?){
        if error == nil {
            //------------------------------------------------
            var feedbackMsg : String? = nil
            
            switch result {
            case .cancelled:
                //feedbackMsg = "Mail sending canceled"
                self.log.debug("Mail sending canceled")
                
            case .saved:
                feedbackMsg = "Mail saved"
                
            case .sent:
                feedbackMsg = "Mail sent";
                
            case .failed:
                feedbackMsg = "Mail sent failure: \(String(describing: error?.localizedDescription))"
//            default:
//                break
            }
            if let feedbackMsg = feedbackMsg {
                //CLKAlertController.showAlertInVC(self, title: "Email", message: "\(feedbackMsg)")
                if let viewController = self.viewController{
                    CLKAlertController.showAlertInVC(viewController, title: "Email", message: "\(feedbackMsg)")
                }else{
                    self.log.error("self.viewController is nil")
                }
            }else{
                self.log.error("feedbackMsg is nil")
            }
            
            //------------------------------------------------
        }else{
            self.log.error("Error: \(String(describing: error))")
            
            if let viewController = self.viewController{
                CLKAlertController.showAlertInVC(viewController, title: "Error sending email", message: "Please try again.\rIf this problem persists please contact support with error:\r [\(String(describing: error?.localizedDescription))]")
            }else{
                self.log.error("self.viewController is nil")
            }
        }
        
        controller.dismiss(animated: true, completion: {
            
        })
        
    }
}


extension MFMailComposeViewController{
    
    open override var preferredStatusBarStyle : UIStatusBarStyle {
        //should be set in
        //ParentTableViewController
        //ParentViewController
        //extension UINavigationController
        return .lightContent //called but has not affect
        //return AppearanceManager.preferredStatusBarStyle()
    }
}

