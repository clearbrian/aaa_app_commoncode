//
//  AppearanceManager.swift
//  SandPforiOS
//
//  Created by Brian Clear on 10/06/2015.
//  Copyright (c) 2015 CLARKSONS RESEARCH SERVICES LIMITED. All rights reserved.
//

import Foundation
import UIKit

let kwindow_tintColor = "8900F6"

class AppearanceManager {
    let log = MyXCodeEmojiLogger.defaultInstance
    //---------------------------------------------------------------------
    //---------------------------------------------------------------------
    init(){
        //super.init()
        
        // TODO: - COMMENT BACK IN - too blue for work
        configureUIAppearance()
    }
    //---------------------------------------------------------------------
    //---------------------------------------------------------------------
    //UIBarButtonItem font and title text font(titleTextAttributes)
    
    /*
    
    (lldb) po [UIFont fontNamesForFamilyName:@"Helvetica Neue"]
    
    (id) $1 = 0x079d8670 <__NSCFArray 0x79d8670>(
    HelveticaNeue-Bold,
    HelveticaNeue-CondensedBlack,
    HelveticaNeue-Medium,
    HelveticaNeue,
    HelveticaNeue-Light,
    HelveticaNeue-CondensedBold,
    HelveticaNeue-LightItalic,
    HelveticaNeue-UltraLightItalic,
    HelveticaNeue-UltraLight,
    HelveticaNeue-BoldItalic,
    HelveticaNeue-Italic
    )
    THERE IS NO HelveticaNeue-Regular its 'HelveticaNeue'
    
    */
    //https://color.adobe.com/Fun-but-Understated-color-theme-6642983/?showPublished=true
    //    <li title="#225D66" style="background: #225D66"></li>
    //    <li title="#159587" style="background: #159587"></li>
    //    <li title="#9BF0D0" style="background: #9BF0D0"></li>
    //    <li title="#F2FFE3" style="background: #F2FFE3"></li>
    //    <li title="#F57600" style="background: #F57600"></li></ul>
    
    var navigationBarFont = UIFont(name: UIFont.lookupCustomFontNameForTextStyle(.body), size: 18)
    
    
    //------------------------------------------------
    //SET DEFAULT SCHEME - note this will be overwritten in updateHexColorsForTheme
    //------------------------------------------------
    //key color used throughout the app
    
    //NOTE TO set WINDOW.tintColor this value is used
    static var appColorTintInternal: UIColor = UIColor.hexStringToUIColor(kwindow_tintColor) //FACET APP Purple
    static var appColorTintExternal: UIColor = UIColor.hexStringToUIColor(kwindow_tintColor) //FACET APP Purple
    
    
    //---------------------------------------------------------------------
    static var appColorNavbarAndTabBar: UIColor = UIColor.hexStringToUIColor("FFFFFF")   // IGNORE NO EFFECT - UPDATED BELOW
    
    //lightest
    static var appColorLight0: UIColor = UIColor.hexStringToUIColor("F2FFE3")   // IGNORE NO EFFECT - UPDATED BELOW
    static var appColorLight1: UIColor = UIColor.appColorCYAN()   // IGNORE NO EFFECT - UPDATED BELOW
    static var appColorLight2: UIColor = UIColor.appColorCYAN()   // IGNORE NO EFFECT - UPDATED BELOW
    static var appColorLight3: UIColor = UIColor.appColorCYAN()   // IGNORE NO EFFECT - UPDATED BELOW
    static var appColorLight_SelectedColor: UIColor = UIColor.appColorCYAN()   // IGNORE NO EFFECT - UPDATED BELOW
    static var appColorLight_DeSelectedColor: UIColor = UIColor.appColorCYAN()   // IGNORE NO EFFECT - UPDATED BELOW
    
    static var appColorLight_ErrorColor: UIColor = UIColor.appColorCYAN()   // IGNORE NO EFFECT - UPDATED BELOW
    
    //-------------------------------------------------------------------------
    static var appColorDark1: UIColor = UIColor.appColorCYAN()   // IGNORE NO EFFECT - UPDATED BELOW
    //darkest - used for nav bars etc
    static var appColorDark0: UIColor = UIColor.hexStringToUIColor("225D66")   // IGNORE NO EFFECT - UPDATED BELOW
    //-------------------------------------------------------------------------
    //didnt work cos VCs in TABBARCOntroller of UINav controlles so subclasssed them
    static var preferredStatusBarStyle : UIStatusBarStyle = .lightContent
    

    
    static var appColorTintRed: UIColor = UIColor.hexStringToUIColor("#FF6047")
    static var appColorTintYellow: UIColor = UIColor.hexStringToUIColor("#F8B91D")
    
    
    
    
    //------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------
    //IGNORE ABOVE COLOR SCHEME ACTUALLY SET in updateHexColorsForTheme7
    //------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------

    func updateHexColorsForTaxiRank(){
        //FLAT BLACK
        //let appDarkColorMainBackground = UIColor.hexStringToUIColor("#1E2120")
        //let appDarkColorMainBackground = UIColor.hexStringToUIColor("#45413A")
        //https://dribbble.com/shots/3233185-Bespoke
        let appDarkColorMainBackground = UIColor.hexStringToUIColor("#394551")
        //
        
        //light grey
        let appDarkColorTint = UIColor.hexStringToUIColor("#EEEEEE")
        
        //cab yellow
        let appDarkColorTintMain = UIColor.hexStringToUIColor("#F8B91D")
        
        //FLAT ORANGE RED
        let appDarkColorTintHighight = UIColor.hexStringToUIColor("#FF6047")
        
        //dark red flat
        let appDarkColorTintHighight1 = UIColor.hexStringToUIColor("#C14550")
        
        
        //------------------------------------------------------------------------------------------------
        AppearanceManager.appColorDark0 = appDarkColorMainBackground //DARKEST PURPLE
        AppearanceManager.appColorDark1 = appDarkColorMainBackground //toolbar color - lighter material purple
        //AppearanceManager.appColorDark1 = UIColor.cyanColor()//toolbar color - lighter material purple
        
        //------------------------------------------------------------------------------------------------
        //AppearanceManager.appColorTint = UIColor.hexStringToUIColor("#CBFE98")//NEON GREEN
        
        AppearanceManager.appColorTintInternal = appDarkColorTint //CREAM - neon too bright on Safari and mail composer
        AppearanceManager.appColorTintExternal = AppearanceManager.appColorDark1  //CREAM - neon too bright on Safari and mail composer
        
        
        //------------------------------------------------------------------------------------------------
        AppearanceManager.appColorLight0 = appDarkColorTint
        AppearanceManager.appColorLight1 = appDarkColorTintMain
        AppearanceManager.appColorLight2 = appDarkColorTintHighight
        AppearanceManager.appColorLight3 = appDarkColorTintHighight1 //circle backgrounds
        
        //cal/fb events in the past
        //AppearanceManager.appColorLight_DeSelectedColor = UIColor.hexStringToUIColor("#F7F6FF")//pale purple
        AppearanceManager.appColorLight_DeSelectedColor = UIColor.hexStringToUIColor("#F1F0ED")//1% sat pale purple
        
        
        //--------------------------------
        //see PLACEPICKER_TABCOLOR : SELECTED COLOR WHEN YOU TAP ON TAB
        //AppearanceManager.appColorLight_SelectedColor = UIColor.hexStringToUIColor("#CBFE98")//NEON GREEN
        AppearanceManager.appColorLight_SelectedColor = UIColor.white
        
        AppearanceManager.appColorLight_ErrorColor = appDarkColorTint
        
        
        
        //------------------------------------------------------------------------------------------------
        //423960 from https://dribbble.com/shots/2561642-Player-List
        AppearanceManager.appColorNavbarAndTabBar = AppearanceManager.appColorDark0 //DARKEST PURPLE

        //------------------------------------------------------------------------------------------------
        //may be nil on app start
        //see
        //extension UINavigationController{...}
        //may not work if embedded in tabcontroller
        AppearanceManager.preferredStatusBarStyle = .lightContent
        //AppearanceManager.preferredStatusBarStyle = .default
        //self.preferredStatusBarStyle = .Default //black
        //-----------------------------------------------------------------------------------
        
    }


    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - APP APPEARANCE SETTINGS
    //THAT ARE SET ELSEWHERE but configured here
    //--------------------------------------------------------------
    //called in two places but set once here
    //should also be set in ParentTableViewController
    //should also be set in ParentViewController
    //Also used for NavigationControllers
    static func preferredStatusBarStyleForViewController() -> UIStatusBarStyle {
        return self.preferredStatusBarStyle
    }
    
    
    func configureUIAppearance(){
//        //DEBUG - turn off UIAppearance to see if color is set in Storyboard or in VC code
//        return
        
        //------------------------------------------------
        //------------------------------------------------
        //IGNORE IVAR COLORS - SCHEME ACTUALLY SET HERE
        //------------------------------------------------
        //------------------------------------------------
        //------------------------------------------------

        //joyride -updateHexColorsForTheme7()
        updateHexColorsForTaxiRank()
        //------------------------------------------------
        //------------------------------------------------
        //------------------------------------------------
        //IGNORE ABOVE COLOR SCHEME ACTUALLY SET HERE
        //------------------------------------------------
        //------------------------------------------------
        //------------------------------------------------
        //------------------------------------------------
        //------------------------------------------------
        //------------------------------------------------
        //------------------------------------------------
        
        
        //title text done in setUIAppearance_UINavigationBar_titleText
        setUIAppearance_UINavigationBar()

        //========== UIToolbar ======================
        setUIAppearance_UIToolbar()
        
        
        
        
        //========== UILabel ======================
        //[[UILabel appearance] setFont:[UIFont fontWithName:kFontName size:15.0f]()
        
        setUIAppearance_UIBarButtonItem()
        
        setUIAppearance_UISegmentedControl()
        
        setUIAppearance_UITabBar()
      
        setUIAppearance_UISearchBar()
        
        setUIAppearance_UIButton()
        
        setUIAppearance_UISwitch()
        
        setUIAppearance_UITextField()
        
        setUIAppearance_UITableView()
        
        setUIAppearance_UITableViewHeaderFooterView()
        
        setUIAppearance_UINavigationBar_titleText()

        setUIAppearance_UITableViewCell()
        
        //note text in controls is configured in 
        //UIFont+CustomFonts.swift extension UISegmentedControl etc
    }
    
    static func window_tintColor() -> UIColor{
        
        //For Alerts this only set the BUTTON tint - not the title and only if not set before alert shown
        let color = AppearanceManager.appColorTintExternal
        //let color = UIColor.appColorCYAN()
        
        return color
    }
    
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - SET APPEARENCE
    //--------------------------------------------------------------

    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - NAV BAR
    //--------------------------------------------------------------
    //title text done in setUIAppearance_UINavigationBar_titleText
    func setUIAppearance_UINavigationBar(){
        //Text color on Nav bar - if not set will be window_tintColor()
        
        //no effect
        //the text on nav bar


        //------------------------------------------------------------------------------------------------
        //let color = AppearanceManager.appColorLight0
        UINavigationBar.appearance().tintColor = AppearanceManager.appColorTintInternal

        
        //let color1 = AppearanceManager.appColorNavbarAndTabBar
        UINavigationBar.appearance().barTintColor = AppearanceManager.appColorNavbarAndTabBar
        //------------------------------------------------------------------------------------------------
        //tried to mak nav bar see through
        //        UINavigationBar.appearance().tintColor = UIColor.clearColor()
        //        UINavigationBar.appearance().backgroundColor = UIColor.clearColor()
        //        UINavigationBar.appearance().barTintColor = UIColor.clearColor()
        
        //if translucent can make tint color washed out
        UINavigationBar.appearance().isTranslucent = false
        
        
        
        
    }
    
    static func setUIAppearance_UIViewController_view_backgroundColor() -> UIColor{
        return AppearanceManager.appColorLight0
        
    }
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - NAVIGATION BAR / BAR BUTTON ITEMS
    //--------------------------------------------------------------
    
    func setUIAppearance_UINavigationBar_titleText(){
        
        //        //-----------------------------------------------------------------------------------
        //        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        //            [titleTextAttributes_ setValue:[UIFont fontWithName:kFontNameMedium size:17] forKey:NSFontAttributeName];
        //
        //        }else{
        //            //iphone
        //            [titleTextAttributes_ setValue:[UIFont fontWithName:kFontNameMedium size:12] forKey:NSFontAttributeName];
        //        }
        //if you add another
        //      it should be between then [] and
        //       each item seperated by ,
        //      key and value in an item seperated by :
        //
        UINavigationBar.appearance().titleTextAttributes
            = ([NSFontAttributeName: navigationBarFont!,
            NSForegroundColorAttributeName: UIColor.appColorFlat_Sunflower()])
        
    }
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - TAB BAR
    //--------------------------------------------------------------
    func setUIAppearance_UITabBar(){
        
        //tints the icons
        //UITabBar.appearance().tintColor = AppearanceManager.appColorTintYellow
        //UITabBar.appearance().tintColor = UIColor.yellow
        UITabBar.appearance().tintColor = UIColor.white
        
        //TINTS WHOLE BAR
        UITabBar.appearance().barTintColor = AppearanceManager.appColorNavbarAndTabBar
        //DEPRECATED use tintColorUITabBar.appearance().selectedImageTintColor = AppearanceManager.appColorDark1


        
        //not used - maybe works if translucent
        //UITabBar.appearance().backgroundColor = AppearanceManager.appColorDark0
        //UITabBar.appearance().backgroundColor = UIColor.cyan
        UITabBar.appearance().backgroundColor = AppearanceManager.appColorNavbarAndTabBar
        

        
        //see alse UITabBarItem.appearance()
    }
    
    func setUIAppearance_UITabBarItem(){
        //tried to set the UNSELECTED color but
        //        //tints the icons
        //        UITabBarItem.appearance().tintColor = UIColor.whiteColor()
        //        
        //        UITabBarItem.appearance().barTintColor = AppearanceManager.appColorNavbarAndTabBar
        //        //DEPRECATED use tintColorUITabBar.appearance().selectedImageTintColor = AppearanceManager.appColorDark1
        //        
        //        
        //        
        //        //not used - maybe works if translucent
        //        //UITabBar.appearance().backgroundColor = AppearanceManager.appColorDark0
        //        UITabBarItem.appearance().backgroundColor = UIColor.cyanColor()
        //        
        //        //see alse UITabBarItem.appearance()
    }
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - UIToolbar
    //--------------------------------------------------------------
    func setUIAppearance_UIToolbar(){
        //Text color on Nav bar - if not set will be window_tintColor()
        //UIToolbar.appearance().tintColor = AppearanceManager.appColorLight0
        UIToolbar.appearance().tintColor = AppearanceManager.appColorTintInternal
        //UIToolbar.appearance().barTintColor = AppearanceManager.appColorDark1
        //UIToolbar.appearance().backgroundColor = AppearanceManager.appColorDark1
        
        //Darkest blend in with background
        UIToolbar.appearance().barTintColor = AppearanceManager.appColorDark0
        UIToolbar.appearance().backgroundColor = AppearanceManager.appColorDark0
        
        //        //debug
        //        UIToolbar.appearance().tintColor = UIColor.cyanColor()
        //        UIToolbar.appearance().barTintColor = UIColor.redColor()
        //        UIToolbar.appearance().backgroundColor = UIColor.purpleColor()
        
        
        UIToolbar.appearance().isTranslucent = false
    }
    
    func setUIAppearance_UIBarButtonItem(){
        
        UIBarButtonItem.appearance().setTitleTextAttributes(
            [NSFontAttributeName: navigationBarFont!],
            for: UIControlState())
        
        //Text color on Nav bar - if not set will be window_tintColor()
        //UIToolbar.appearance().tintColor = AppearanceManager.appColorLight0
        UIBarButtonItem.appearance().tintColor = AppearanceManager.appColorTintInternal
        
    }

    //--------------------------------------------------------------
    // MARK: -
    // MARK: - UISearchBar
    //--------------------------------------------------------------
    
    func setUIAppearance_UISearchBar(){
        
//        UISearchBar.appearance().tintColor = AppearanceManager.appColorTintYellow
        //dont make these the same the cancel button disappears
//     UISearchBar.appearance().barTintColor = AppearanceManager.appColorDark1
//        UISearchBar.appearance().backgroundColor = AppearanceManager.appColorDark1
    }
    
    //used in two screens
    func configureSearchBarButtonItem(_ barButtonItem : UIBarButtonItem?){
        if let barButtonItem = barButtonItem{
            //---------------------------------------------------------------------
            barButtonItem.tintColor = AppearanceManager.appColorTintInternal
            
            let navigationBarFont = UIFont(name: "HelveticaNeue-Bold", size: 17)
            
            barButtonItem.setTitleTextAttributes( [NSFontAttributeName: navigationBarFont!],
                for: UIControlState())
            //---------------------------------------------------------------------
        }else{
            self.log.error("barButtonItem is nil")
        }
    }
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - OTHER
    //--------------------------------------------------------------
    
    func setUIAppearance_UISegmentedControl(){
        //        //seg control in nav
        //        //UISegmentedControl.appearance().tintColor = AppearanceManager.appColorTint
        //        UISegmentedControl.appearance().tintColor = UIColor.whiteColor()
        //        
        //        UISegmentedControl.appearance().backgroundColor = AppearanceManager.appColorNavbarAndTabBar
        //        
        //        
        ////        //debug
        ////        UISegmentedControl.appearance().tintColor = UIColor.cyanColor()
        ////        //UISegmentedControl.appearance().barTintColor = UIColor.redColor()
        ////        UISegmentedControl.appearance().backgroundColor = UIColor.purpleColor()
        //        
        //        
        ////        let font = UIFont(name:"AvenirNext-Regular", size:13.0)
        ////        UISegmentedControl.appearance().setTitleTextAttributes([NSFontAttributeName: font!], forState: .Normal)
    }

    func setUIAppearance_UIButton(){
        //do on perscreen too many hidden buttons
        //        UIButton.appearance().tintColor = AppearanceManager.appColorTint
        //        UIButton.appearance().backgroundColor = AppearanceManager.appColorLight0
                
        //        //get current title, make it mutable, change the font name - keep everything else
        //        if let currentAttributedTitle : NSAttributedString = UIButton.appearance().currentAttributedTitle{
        //            
        //            let currentAttributedTitleMutable  = NSMutableAttributedString(attributedString: currentAttributedTitle)
        //            
        //            //        let text = "Welcome to Attributed String Creator"
        //            //        let attributedString = NSMutableAttributedString(string:text)
        //            let font = UIFont(name:"AvenirNext-Regular", size:10.0)
        //            currentAttributedTitleMutable.addAttribute(NSFontAttributeName, value:font!, range:NSMakeRange(0,currentAttributedTitle.length))
        //            
        //            UIButton.appearance().setAttributedTitle(currentAttributedTitleMutable, forState: .Normal)
        //            
        //        }else{
        //            print("setUIAppearance_UIButton: UIButton.appearance().currentAttributedTitle is nil")
        //        }
        //        
        //        if let buttonFont = UIButton.appearance().titleLabel?.font{
        //             print("buttonFont:\(buttonFont)")
        //        }else{
        //            print("buttonFont not found")
        //        }
    
        //use self.buttonOpenIn.applyCustomFontForCurrentTextStyle() in VC
    
    }
    
    func setUIAppearance_UISwitch(){
        UISwitch.appearance().onTintColor = AppearanceManager.appColorTintYellow
    }
    
    func setUIAppearance_UITextField(){
        
    }
    
    func setUIAppearance_UITableView(){
        //UITableView.appearance().backgroundColor = UIColor.whiteColor()
        
    }
    
    func setUIAppearance_UITableViewHeaderFooterView(){
        
    }
    
    func setUIAppearance_UITableViewCell(){
        
    }
    
}
