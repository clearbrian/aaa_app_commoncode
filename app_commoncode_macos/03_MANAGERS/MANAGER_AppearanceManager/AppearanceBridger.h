//
//  AppearanceBridger.h
//  joyride
//
//  Created by Brian Clear on 06/08/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AppearanceBridger : NSObject
+ (void)setAppearance;
@end
