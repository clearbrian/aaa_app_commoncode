//
//  CurrentLocationManager.swift
//  joyride
//
//  Created by Brian Clear on 19/10/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import CoreLocation
//import GoogleMaps
//import Flurry_iOS_SDK

protocol CurrentLocationManagerDelegate {
    func currentLocationUpdated(currentLocationManager: CurrentLocationManager, currentLocation: CLLocation)
    func currentLocationFailed(currentLocationManager: CurrentLocationManager, error: Error?)
}


class CurrentLocationManager: ParentNSObject, CLLocationManagerDelegate{
    
    var delegate: CurrentLocationManagerDelegate?
    
    var clLocationManager = CLLocationManager()
    
    var currentLocationStatus: CLAuthorizationStatus = .notDetermined
    
    //---------------------------------------------------------------------
    //geocoded address for current location
    //required ad Uber requires locality
    //var currentLocation_googleGeocodedGMSAddress: GMSAddress?
    var currentLocation_CLKAddress: CLKAddress?
    

    var distanceMetersDouble : CLLocationDistance = -1.0
    
    
    func requestLocation(){
        //default to london in case location turned off
        
        let currentLocation = CLLocation.init(latitude: 51.5073629, longitude: -0.1276495)
        self.currentLocation = currentLocation
        geocodeCurrentLocation(currentLocation)
        
        
        self.log.info("CALLING requestLocation()")
        self.clLocationManager.delegate = self;
        
        //---------------------------------------------------------------------
        if (CLLocationManager.locationServicesEnabled()) {
            self.log.info("CLLocationManager.locationServicesEnabled() - true");
            
            self.log.info("self.clLocationManager.requestWhenInUseAuthorization()");
            
            //self.clLocationManager.requestAlwaysAuthorization()
            //not on mac self.clLocationManager.requestWhenInUseAuthorization()
            
            
            // By default we use the best accuracy setting (kCLLocationAccuracyBest)
            //
            // You may instead want to use kCLLocationAccuracyBestForNavigation, which is the highest possible
            // accuracy and combine it with additional sensor data.  Note that level of accuracy is intended
            // for use in navigation applications that require precise position information at all times and
            // are intended to be used only while the device is plugged in.
            //
            self.clLocationManager.desiredAccuracy = kCLLocationAccuracyBest
            
            // start tracking the user's location
            self.log.info("self.clLocationManager.startUpdatingLocation()")
            
            self.clLocationManager.startUpdatingLocation()
            
        }else{
            self.log.error("CLLocationManager.locationServicesEnabled() - false");
        }
    }
    
    
    
    
    //--------------------------------------------------------------
    // MARK: - fileprivate Current CLLocations
    // MARK: -
    //--------------------------------------------------------------

    //make private user currentUserLocation
    fileprivate var prevLocation : CLLocation?
    
    //fileprivate var currentLocation : CLLocation?{
    var currentLocation : CLLocation?{
        didSet {
            //-----------------------------------------------------------------------------------
            if let currentLocation = currentLocation {
                //NOISY 
                print("currentLocation.didSet: TO:[\(currentLocation.coordinate.latitude), \(currentLocation.coordinate.longitude)]")
                
            }else{
                print("currentLocation.didSet: TO: nil")
            }
            
            //-----------------------------------------------------------------------------------
            if let currentLocation = currentLocation {
                
//                if let prevLocation = prevLocation{
//                    
//                    if let  _ = self.currentLocation_CLKAddress {
//                        //------------------------------------------------
//                        //address retrieved at least once
//                        //------------------------------------------------
//                        print("ERROR:GMSGeometryDistance no on mac")
////                        //only geocode if user location has moved else we'll hammer the google api
////                        //------------------------------------------------
////                        //GoogleMaps function - returns meters
////                        self.distanceMetersDouble =  GMSGeometryDistance(prevLocation.coordinate, currentLocation.coordinate)
////                        //NOISY self.log.info("[prevLocation to currentLocation] distanceMetersDouble:\(distanceMetersDouble)")
////                        //------------------------------------------------
////                        if self.distanceMetersDouble > 10.0 {
////                            //flags in geocodeCurrentLocation will prevent multiple calls
//                            self.prevLocation = currentLocation
//                            geocodeCurrentLocation(currentLocation)
//                            flurryCurrentLocation(currentLocation)
//                            
//                            //sendToFromVC_currentLocationUpdated()
////                            
////                            
////                        }else{
////                            //NOISY self.log.info("[prevLocation to currentLocation] less than 10.0m SKIP GEOCODE - distanceMetersDouble:\(distanceMetersDouble)")
////                        }
//                        
//
//                        //---------------------------------------------------------------------
//                        self.callDelegate_currentLocationUpdated(currentLocation)
//                        //---------------------------------------------------------------------
//                        
//                    }else{
//                        
//                        //geocode till we get an address- flags in geocodeCurrentLocation will prevent multiple calls
//                        //but this will retry if prev failed
//                        geocodeCurrentLocation(currentLocation)
//                        flurryCurrentLocation(currentLocation)
//                    }
//                    
//                }else{
//                    //first time app opened and TripVC appears - need to zoom into local
//                    //prevLocation - nil - first time currentLocation set so geocode
//                    //save for next iteration
//                    prevLocation = currentLocation
//                    //---------------------------------------------------------------------
//                    self.callDelegate_currentLocationUpdated(currentLocation)
//                    //---------------------------------------------------------------------
//                }
                
                
                self.callDelegate_currentLocationUpdated(currentLocation)
                
            }else{
                self.log.error("currentLocation is nil - cant geocodeCurrentLocation")
            }
        }
    }
    
    func callDelegate_currentLocationUpdated(_ currentLocation: CLLocation){
        if let delegate = self.delegate{
            
            //------------------------------------------------
            //errorIN may be nil
            delegate.currentLocationUpdated(currentLocationManager: self, currentLocation: currentLocation)
            //------------------------------------------------
            
        }else{
            appDelegate.log.error("delegate is nil")
        }
    }
    
    func flurryCurrentLocation(_ currentLocation: CLLocation){
// TODO: - macos
        //        Flurry.setLatitude(currentLocation.coordinate.latitude,
//                           longitude: currentLocation.coordinate.longitude,
//                           horizontalAccuracy: Float(currentLocation.horizontalAccuracy),
//                           verticalAccuracy: Float(currentLocation.verticalAccuracy))
    }
    
    
    // TODO: - remove store in currentUserLocation
    //var currentLocation_geocodedAddressString: String?
    
    //uber requires locality e.g London
    // TODO: - test uber in non london locality
    var currentLocation_locality:String?{
        
        var currentLocation_locality_:String? = nil
        if let currentLocation_CLKAddress = self.currentLocation_CLKAddress {
            if let locality = currentLocation_CLKAddress.locality {
                currentLocation_locality_ = locality
                
            }else{
                self.log.error("currentLocation_googleGeocodedGMSAddress.locality is nil")
            }
        }else{
            self.log.error("self.currentLocation_googleGeocodedGMSAddress is nil")
        }
        return currentLocation_locality_
    }
    
    
    
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - CLLocationManagerDelegate
    //--------------------------------------------------------------
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        
        //self.log.info("locationManager: didUpdateLocations:")
        for clLocation in locations{
            //OK but noisy in the logs
            self.log.warning("locationManager: didUpdateLocations: clLocation:\(clLocation)")
            
            self.currentLocation = clLocation
            
            //*ViewController.moveToCurrentLocation() moved into setter for currentLocation
            
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - currentUserPlace : CLKLocationPlace
    // MARK: -
    //--------------------------------------------------------------
    
    //pass it nil for name else it will display it in Nearest address e.g. "Nearest Address, 15 Royal Mint st..." which we dont want
    var currentUserPlace = CLKLocationPlace()
    
    // MARK: -
    func geocodeCurrentLocation(_ currentLocation: CLLocation){
        //locations will come in over and over. if less 10 meters will geocode to address but not if one in progress
        //self.log.info("[GEOCODE CURRENT LOCATION]: currentLocation(\(currentLocation))")

        
    //        //---------------------------------------------------------------------
    //        currentUserPlace.updateLocationAndReverseGeocode(clLocation: currentLocation,
    //                                                         success:{ (clkGeocodedAddressesGoogleCollection: CLKGeocodedAddressesGoogleCollection) -> Void in
    //                                                            //---------------------------------------------------------------------
    //                                                            //GEOCODE OK
    //                                                            //---------------------------------------------------------------------
    //                                                            if let bestOrFirstCLKAddress = clkGeocodedAddressesGoogleCollection.bestOrFirstCLKAddress {
    //                                                                
    //                                                                //required ad Uber requires locality
    //                                                                self.currentLocation_CLKAddress = bestOrFirstCLKAddress
    //                                                                
    //                                                                //---------------------------------------------------------------------
    //                                                                self.log.info("[GEOCODE CURRENT LOCATION OK]: currentLocation(\(currentLocation)) \r\(bestOrFirstCLKAddress.address_lines)")
    //                                                                
    //                                                                if let delegate = self.delegate {
    //                                                                    delegate.currentLocationUpdated(currentLocationManager: self, currentLocation: currentLocation)
    //                                                                }else{
    //                                                                    appDelegate.log.error("self.delegate is nil - cant return location to map")
    //                                                                }
    //                                                                
    //                                                            }else{
    //                                                                appDelegate.log.error("clkGeocodedAddressesResponse.bestOrFirstCLKAddress is nil")
    //                                                                
    //                                                            }
    //                                                            //---------------------------------------------------------------------
    //        },
    //                                                         failure:{
    //                                                            (error: Error?) -> Void in
    //                                                            self.log.error("[GEOCODE CURRENT LOCATION FAILED] currentLocation(\(currentLocation)) error:\(error) ")
    //                                                            
    //                                                            if let delegate = self.delegate {
    //                                                                delegate.currentLocationFailed(currentLocationManager: self, error: error)
    //                                                            }else{
    //                                                                appDelegate.log.error("self.delegate is nil - cant return error to appd")
    //                                                            }
    //                                                            
    //        }
    //        )
        //---------------------------------------------------------------------
        
        
        
        
        //---------------------------------------------------------------------
        if currentLocation.isValid{
            currentUserPlace.updateLocation(clLocation: currentLocation)
            
            //------------------------------------------------------------------------------------------------
            //CALL REVERSE GEOCODE - alway if you user this method
            //------------------------------------------------------------------------------------------------
//            appDelegate.colcGeocoder.reverseGeocodePlace(clkPlace: currentUserPlace,
//                                                         success:{(clkPlace: CLKPlace) -> Void in
//                                                                //---------------------------------------------------------------------
//                                                                //GEOCODE OK
//                                                                //---------------------------------------------------------------------
//                                                                if let clkGeocodedAddressesCollection = clkPlace.clkGeocodedAddressesCollection {
//                                                                    if let bestOrFirstCLKAddress = clkGeocodedAddressesCollection.bestOrFirstCLKAddress {
//                                                                        // TODO: - remove use currentUserPlace.clkGeocodedAddressesCollection.bestOrFirstCLKAddress
//                                                                        //required ad Uber requires locality
//                                                                        self.currentLocation_CLKAddress = bestOrFirstCLKAddress
//                                                                        
//                                                                        //---------------------------------------------------------------------
//                                                                        self.log.info("[GEOCODE CURRENT LOCATION OK]: currentLocation(\(currentLocation)) \r\(bestOrFirstCLKAddress.address_lines)")
//                                                                        
//                                                                        if let delegate = self.delegate {
//                                                                            delegate.currentLocationUpdated(currentLocationManager: self, currentLocation: currentLocation)
//                                                                        }else{
//                                                                            appDelegate.log.error("self.delegate is nil - cant return location to map")
//                                                                        }
//                                                                        
//                                                                    }else{
//                                                                        appDelegate.log.error("clkGeocodedAddressesResponse.bestOrFirstCLKAddress is nil")
//                                                                        
//                                                                    }
//                                                                    //---------------------------------------------------------------------
//                                                                }else{
//                                                                    appDelegate.log.error("clkPlace.clkGeocodedAddressesCollection is nil")
//                                                                }
//                                                        },
//                                                        failure:{ (error: Error?) -> Void in
//                                                                self.log.error("[GEOCODE CURRENT LOCATION FAILED] currentLocation(\(currentLocation)) error:\(error) ")
//    
//                                                                if let delegate = self.delegate {
//                                                                    delegate.currentLocationFailed(currentLocationManager: self, error: error)
//                                                                }else{
//                                                                    appDelegate.log.error("self.delegate is nil - cant return error to appd")
//                                                                }
//                                                        }
//            )
            
            //print("ERROR: appDelegate.colcGeocoder none on mac os")
            //------------------------------------------------------------------------------------------------

        }else{
            appDelegate.log.info("clLocation.isValid is false - skip geocode of new location")
        }
        
        
        


        //---------------------------------------------------------------------
    }
    
    
    
    //--------------------------------------------------------------
    // MARK: - Other Location Delegates
    // MARK: -
    //--------------------------------------------------------------
    /*
     *  locationManager:didUpdateHeading:
     *
     *  Discussion:
     *    Invoked when a new heading is available.
     */
    //@availability(iOS, introduced=3.0)
    //    func locationManager(manager: CLLocationManager!, didUpdateHeading newHeading: CLHeading!){
    //        self.log.info("locationManager: didUpdateHeading")
    //    }
    
    /*
     *  locationManagerShouldDisplayHeadingCalibration:
     *
     *  Discussion:
     *    Invoked when a new heading is available. Return YES to display heading calibration info. The display
     *    will remain until heading is calibrated, unless dismissed early via dismissHeadingCalibrationDisplay.
     */
    //@availability(iOS, introduced=3.0)
//not on mac
    //    func locationManagerShouldDisplayHeadingCalibration(_ manager: CLLocationManager) -> Bool{
//        self.log.info("locationManager: locationManagerShouldDisplayHeadingCalibration")
//        return true
//    }
    
    /*
     *  locationManager:didDetermineState:forRegion:
     *
     *  Discussion:
     *    Invoked when there's a state transition for a monitored region or in response to a request for state via a
     *    a call to requestStateForRegion:.
     */
    //@availability(iOS, introduced=7.0)
    func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion){
        self.log.info("locationManager: didDetermineState")
    }
    
    
    //---------------------------------------------------------------------
    //BEACONS - only comment in if need may cause App Store rejection
    //---------------------------------------------------------------------
    /*
     *  locationManager:didRangeBeacons:inRegion:
     *
     *  Discussion:
     *    Invoked when a new set of beacons are available in the specified region.
     *    beacons is an array of CLBeacon objects.
     *    If beacons is empty, it may be assumed no beacons that match the specified region are nearby.
     *    Similarly if a specific beacon no longer appears in beacons, it may be assumed the beacon is no longer received
     *    by the device.
     */
    //@availability(iOS, introduced=7.0)
    //SWIFT2.0 - commented out - changed in swift2
    //    func locationManager(manager: CLLocationManager!, didRangeBeacons beacons: [AnyObject]!, inRegion region: CLBeaconRegion!){
    //        self.log.info("locationManager:didRangeBeacons")
    //    }
    
    /*
     *  locationManager:rangingBeaconsDidFailForRegion:withError:
     *
     *  Discussion:
     *    Invoked when an error has occurred ranging beacons in a region. Error types are defined in "CLError.h".
     */
    //@availability(iOS, introduced=7.0)
    //SWIFT2.0 - commented out - changed in swift2
    //    func locationManager(manager: CLLocationManager!, rangingBeaconsDidFailForRegion region: CLBeaconRegion!, withError error: NSError!){
    //        self.log.info("locationManager: rangingBeaconsDidFailForRegion")
    //    }
    
    /*
     *  locationManager:didEnterRegion:
     *
     *  Discussion:
     *    Invoked when the user enters a monitored region.  This callback will be invoked for every allocated
     *    CLLocationManager instance with a non-nil delegate that implements this method.
     */
    //@availability(iOS, introduced=4.0)
    //SWIFT2.0 - commented out - changed in swift2
    //    func locationManager(manager: CLLocationManager!, didEnterRegion region: CLRegion!){
    //        self.log.info("locationManager: didEnterRegion")
    //    }
    
    /*
     *  locationManager:didExitRegion:
     *
     *  Discussion:
     *    Invoked when the user exits a monitored region.  This callback will be invoked for every allocated
     *    CLLocationManager instance with a non-nil delegate that implements this method.
     */
    //@availability(iOS, introduced=4.0)
    //    func locationManager(manager: CLLocationManager!, didExitRegion region: CLRegion!){
    //        self.log.info("locationManager: didExitRegion")
    //    }
    
    /*
     *  locationManager:didFailWithError:
     *
     *  Discussion:
     *    Invoked when an error has occurred. Error types are defined in "CLError.h".
     */
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error){
        
        self.log.error("locationManager: didFailWithError:\(error)")
    }
    
    /*
     *  locationManager:monitoringDidFailForRegion:withError:
     *
     *  Discussion:
     *    Invoked when a region monitoring error has occurred. Error types are defined in "CLError.h".
     */
    //@availability(iOS, introduced=4.0)
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error){
        self.log.error("locationManager: monitoringDidFailForRegion:\(error)")
    }
    
    /*
     *  locationManager:didChangeAuthorizationStatus:
     *
     *  Discussion:
     *    Invoked when the authorization status changes for this application.
     */
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus){
        switch status {
        case .notDetermined:
            
            // This application is not authorized to use location services.  Due
            // to active restrictions on location services, the user cannot change
            // this status, and may not have personally denied authorization
            
            self.log.info("locationManager: didChangeAuthorizationStatus: NotDetermined")
            
            //CLKAlertController.showAlertInVC(self, title: "Location Not Determined", message: "Access to device location is disabled in Settings")
            
        case .restricted:
            
            // User has explicitly denied authorization for this application, or
            // location services are disabled in Settings.
            self.log.info("locationManager: didChangeAuthorizationStatus: Restricted")
        case .denied:
            
            // User has granted authorization to use their location at any time,
            // including monitoring for regions, visits, or significant location changes.
            //@availability(iOS, introduced=8.0)
            self.log.info("locationManager: didChangeAuthorizationStatus: Denied")
        case .authorizedAlways:
            
            // User has granted authorization to use their location only when your app
            // is visible to them (it will be made visible to them if you continue to
            // receive location updates while in the background).  Authorization to use
            // launch APIs has not been granted.
            self.log.info("locationManager: didChangeAuthorizationStatus: AuthorizedAlways")
            
        case .authorizedWhenInUse:
            self.log.info("didChangeAuthorizationStatus: AuthorizedWhenInUse")
            //        default:
            //            self.log.error("didChangeAuthorizationStatus: UNHANDLED CLAuthorizationStatus")
        }
    }
    
    /*
     *  locationManager:didStartMonitoringForRegion:
     *
     *  Discussion:
     *    Invoked when a monitoring for a region started successfully.
     */
    //dont comment in as AppStore rejects seanet cos it had this on so they wanted more info
    //    func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion){
    //        self.log.info("")
    //    }
    
    /*
     *  Discussion:
     *    Invoked when location updates are automatically paused.
     */
// TODO: - not on mac
//    func locationManagerDidPauseLocationUpdates(_ manager: CLLocationManager){
//        self.log.info("")
//    }
    
    /*
     *  Discussion:
     *    Invoked when location updates are automatically resumed.
     *
     *    In the event that your application is terminated while suspended, you will
     *	  not receive this notification.
     */
 // TODO: - not on mac
//    func locationManagerDidResumeLocationUpdates(_ manager: CLLocationManager){
//        self.log.info("")
//    }
    
    /*
     *  locationManager:didFinishDeferredUpdatesWithError:
     *
     *  Discussion:
     *    Invoked when deferred updates will no longer be delivered. Stopping
     *    location, disallowing deferred updates, and meeting a specified criterion
     *    are all possible reasons for finishing deferred updates.
     *
     *    An error will be returned if deferred updates end before the specified
     *    criteria are met (see CLError).
     */
    func locationManager(_ manager: CLLocationManager, didFinishDeferredUpdatesWithError error: Error?){
        self.log.info("")
    }


}
