//
//  COLCCarbonTabSwipeNavigationViewControllerInputs
//  joyride
//
//  Created by Brian Clear on 13/01/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//
import Foundation
import UIKit

protocol COLCCarbonTabSwipeNavigationViewControllerInputsDelegate {
    func willMoveAtIndex(_ colcCarbonTabSwipeNavigationViewControllerInputs: COLCCarbonTabSwipeNavigationViewControllerInputs, index: UInt)
}

class COLCCarbonTabSwipeNavigationViewControllerInputs:
    CarbonTabSwipeNavigation,
    CarbonTabSwipeNavigationDelegate,
    COLCPlacePickerViewControllerDelegate,
    ContactsViewControllerDelegate,
    SelectPlaceViewControllerDelegate,
    FBViewControllerDelegate,
    FoursquareViewControllerDelegate,
    CalendarEventsViewControllerDelegate
{
    let log = MyXCodeEmojiLogger.defaultInstance
    
    //---------------------------------------------------------------------
    var delegate_ContainerVC: COLCCarbonTabSwipeNavigationViewControllerInputsDelegate?
    
    //------------------------------------------------------------------------------------------------
    //user can tap on START/EDIT and it will edit
    //passed in from TripPlannerVc > LocationPicker >> COLCCarbonTabSwipeNavigationViewControllerInputs >> colcPlacePickerViewController
    var clkPlaceSelected: CLKPlace?{
        willSet {
            print("[clkPlaceSelected]1 willSet: Current value: '\(clkPlaceSelected)', New value: '\(newValue)'")
        }
        //Finally this
        didSet {
            
            //---------------------------------------------------------------------
//            //NOISY print("[clkPlaceSelected]2 didSet: Old value: '\(oldValue)', clkPlaceSelected['\(clkPlaceSelected?.name_formatted_address)']")
//            
//            if let colcPlacePickerViewController = self.colcPlacePickerViewController {
//                colcPlacePickerViewController.clkPlaceSelected = clkPlaceSelected
//            }else{
//                self.log.error("self.colcPlacePickerViewController is nil")
//            }
//            // TODO: - REMOVED YELP - cocopods not working
////            // TODO: - untested
////            if let colcYelpPlacePickerViewController = self.colcYelpPlacePickerViewController {
////                colcYelpPlacePickerViewController.clkPlaceSelected = clkPlaceSelected
////            }else{
////                self.log.error("self.colcYelpPlacePickerViewController is nil")
////            }
//            
            //---------------------------------------------------------------------
            // TODO: - COMMENT
            appDelegate.log.error("pass clkSelected into colcPlacePickerViewController.clkPlaceSelected from COLCCarbon is off")
        }
    }
    
    //------------------------------------------------------------------------------------------------
    //instantiate asap - else causes nav to stutter when it first load large screen like
    //---------------------------------------------------------------------
    var colcPlacePickerViewController : COLCPlacePickerViewController? = nil
    var delegate_COLCPlacePickerViewControllerDelegate: COLCPlacePickerViewControllerDelegate?
    
    //---------------------------------------------------------------------
//workign but something wrong with Cocopods when i rebuilt for XCode8 - cant find import file
//    var colcYelpPlacePickerViewController : COLCYelpPlacePickerViewController? = nil
//    var delegate_COLCYelpPlacePickerViewControllerDelegate: COLCYelpPlacePickerViewControllerDelegate?
    
    //---------------------------------------------------------------------
    //CONTACTS
    var contactsViewController : ContactsViewController? = nil
    var delegate_ContactsViewControllerDelegate: ContactsViewControllerDelegate?
    
    //---------------------------------------------------------------------
    //CALENDAR EVENTS
    var calendarEventsViewController : CalendarEventsViewController? = nil
    var delegate_CalendarEventsViewControllerDelegate: CalendarEventsViewControllerDelegate?
    
    //---------------------------------------------------------------------
    //    var savedTripsTableViewController : SavedTripsTableViewController? = nil
    //    var delegate_SavedTripsTableViewControllerDelegate: SavedTripsTableViewControllerDelegate?
    //replaced with simple Nav controller
    var savedTripsUINavigationController : UINavigationController? = nil
    
    //---------------------------------------------------------------------
    //list of saved places
    var selectPlaceViewController : SelectPlaceViewController? = nil
    var delegate_SelectPlaceViewControllerDelegate: SelectPlaceViewControllerDelegate?
    
    //---------------------------------------------------------------------
    //FACEBOOK
    var fbViewController : FBViewController? = nil
    var delegate_FBViewControllerDelegate: FBViewControllerDelegate?
    
    //---------------------------------------------------------------------
    //Foursquare
    var foursquareViewController : FoursquareViewController? = nil
    var delegate_FoursquareViewControllerDelegate: FoursquareViewControllerDelegate?
    
    //---------------------------------------------------------------------

    override func viewDidLoad() {
        
        super.viewDidLoad()

        
        //------------------------------------------------------------------------------------------------
        //Nearby: COLCPlacePickerViewController
        self.colcPlacePickerViewController = self.storyboard!.instantiateViewController(withIdentifier: "COLCPlacePickerViewController") as? COLCPlacePickerViewController
        self.colcPlacePickerViewController?.delegate = self
        
//        //Nearby: COLCYelpPlacePickerViewController
//        self.colcYelpPlacePickerViewController = self.storyboard!.instantiateViewControllerWithIdentifier("COLCYelpPlacePickerViewController") as? COLCYelpPlacePickerViewController
//        self.colcYelpPlacePickerViewController?.delegate = self

        //------------------------------------------------------------------------------------------------
        //CONTACTS
        self.contactsViewController = self.storyboard!.instantiateViewController(withIdentifier: "ContactsViewController") as? ContactsViewController
        self.contactsViewController?.delegate = self
        
        //------------------------------------------------------------------------------------------------
        //CALENDAR & EVENT LOCATION
        self.calendarEventsViewController = self.storyboard!.instantiateViewController(withIdentifier: "CalendarEventsViewController") as? CalendarEventsViewController
        self.calendarEventsViewController?.delegate = self
        
        //------------------------------------------------------------------------------------------------
        //TAB: Saved Trips
        //------------------------------------------------------------------------------------------------
        //self.savedTripsTableViewController = self.storyboard!.instantiateViewControllerWithIdentifier("SavedTripsTableViewController") as? SavedTripsTableViewController
        //self.savedTripsTableViewController?.delegate = self
        //-------------------------------------------------------------------
        //self.savedTripsUINavigationController = self.storyboard!.instantiateViewControllerWithIdentifier("SavedTripsUINavigationController") as? UINavigationController
        
        self.selectPlaceViewController = self.storyboard!.instantiateViewController(withIdentifier: "SelectPlaceViewController") as? SelectPlaceViewController
        self.selectPlaceViewController?.delegate = self
        //------------------------------------------------------------------------------------------------
        //TAB: FACEBOOK EVENTS
        //------------------------------------------------------------------------------------------------
        self.fbViewController = self.storyboard!.instantiateViewController(withIdentifier: "FBViewController") as? FBViewController
        self.fbViewController?.delegate = self
        //------------------------------------------------------------------------------------------------
        //TAB: Foursquare EVENTS
        //------------------------------------------------------------------------------------------------
        self.foursquareViewController = self.storyboard!.instantiateViewController(withIdentifier: "FoursquareViewController") as? FoursquareViewController
        self.foursquareViewController?.delegate = self
        
        //------------------------------------------------------------------------------------------------
        //Configure the swipe control
        //------------------------------------------------------------------------------------------------
        
        //let items = [UIImage(named: "home")!, UIImage(named: "hourglass")!, UIImage(named: "premium_badge")!, "Categories", "Top Free", "Top New Free", "Top Paid", "Top New Paid"]
        //let items = [UIImage(named: "home")!, "Nearby", "Contacts", "Saved Trips"]
        //---------------------------------------------------------------------
        //SET WIDTH BELOW
        //---------------------------------------------------------------------
        let items = ["Nearby", "Contacts", "Facebook Events", "Recent Places", "Calendar",]//, "Yelp", "Foursquare"]
        
    
        //Tab colors are set in CarbonTabSwipeSegmentedControl. changeBackgroundColors
        //obj-C so hardcoded to flatuicolors
        
        //if you change ORDER of tabs checn updateLabelsForTab - it hides the USE STREET
        
        //---------------------------------------------------------------------
        self.configure(withItems: items as [AnyObject], delegate:self)
        self.style()
        
        
    }
    
    func style() {
//        let color: UIColor = UIColor(red: 24.0 / 255, green: 75.0 / 255, blue: 152.0 / 255, alpha: 1)
//---------------------------------------------------------------------
//        self.navigationController?.navigationBar.translucent = false
//        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
//        self.navigationController?.navigationBar.barTintColor = color
//        self.navigationController?.navigationBar.barStyle = .BlackTranslucent
        //---------------------------------------------------------------------
        self.toolbar.isTranslucent = false

        //---------------------------------------------------------------------
        //        self.setTabExtraWidth(30)
        //        self.setTabExtraWidth(4)
        //---------------------------------------------------------------------
        
        let width: CGFloat = 130.0
        let widthLast: CGFloat = 150.0
        self.carbonSegmentedControl!.setWidth(width, forSegmentAt: 0)
        self.carbonSegmentedControl!.setWidth(width, forSegmentAt: 1)
        self.carbonSegmentedControl!.setWidth(width, forSegmentAt: 2)
        self.carbonSegmentedControl!.setWidth(width, forSegmentAt: 3)
        self.carbonSegmentedControl!.setWidth(widthLast, forSegmentAt: 4)
        
        //---------------------------------------------------------------------
//        self.carbonSegmentedControl!.setWidth(width, forSegmentAtIndex: 4)
//        self.carbonSegmentedControl!.setWidth(widthLast, forSegmentAtIndex: 5)
//         carbonTabSwipeNavigation.setTabExtraWidth(30)
        
        //-----------------------------------------------------------------------------------
        //toolbar color is set in setUIAppearance_UIToolbar()
        //self.view.backgroundColor = AppearanceManager.appColorNavbarAndTabBar
        //self.toolbar.backgroundColor = AppearanceManager.appColorNavbarAndTabBar
        
        //-----------------------------------------------------------------------------------
        //also done in applyCustomFontForCurrentTextStyle
        //self.setNormalColor(AppearanceManager.appColorLight0)
        //self.setNormalColor(AppearanceManager.appColorNavbarAndTabBar)
//        self.setNormalColor(UIColor.whiteColor())
        
        //-----------------------------------------------------------------------------------
        //NOTE MAY NOT WORK - set also in //also done in initWithItems
        //UIappearance for SegmentedControl may also override it
        //self.setSelectedColor(AppearanceManager.appColorTint, font: UIFont.boldSystemFontOfSize(14))
        //self.setSelectedColor(UIColor.whiteColor(), font: UIFont.boldSystemFontOfSize(14))
        //self.setSelectedColor(AppearanceManager.appColorLight_SelectedColor, font: UIFont.boldSystemFontOfSize(14))
//        self.setSelectedColor(UIColor.whiteColor(), font: UIFont.boldSystemFontOfSize(14))
        //-----------------------------------------------------------------------------------
        //BAR UNDER SELECTED ITEM
    
        //color of bar that move below the tab names
        //self.setIndicatorColor(AppearanceManager.window_tintColor())
        //self.setIndicatorColor(appDelegate.window?.tintColor)
        self.setIndicatorColor(AppearanceManager.appColorLight2)
        //self.setIndicatorColor(AppearanceManager.appColorLight_SelectedColor)
       // self.setIndicatorColor(UIColor.whiteColor())
        
        //-----------------------------------------------------------------------------------
 
    }
    
    func carbonTabSwipeNavigation_buttonCancel_Action(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation) {
        print("carbonTabSwipeNavigation_buttonCancel_Action")
        
        self.dismiss(animated: true, completion: {

        })
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        //------------------------------------------------------------------------------------------------
        //switch index {
        //case 0:
        //    return self.viewControllerOne!
        //case 1:
        //    return self.colcPlacePickerViewController!
        //case 2:
        //    return self.contactsViewController!
        //case 3:
        //    //return self.savedTripsTableViewController!
        //    return self.savedTripsUINavigationController!
        //default:
        //    return self.viewControllerOne!
        //}
        //------------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------------
        switch index {
        case 0:
            return self.colcPlacePickerViewController!
            
        case 1:
            return self.contactsViewController!
            
        case 2:
            
            return self.fbViewController!
        case 3:
            return self.selectPlaceViewController!
            
        case 4:
            return self.calendarEventsViewController!

//        case 5:
            //return self.savedTripsTableViewController!
            //return self.savedTripsUINavigationController!
            //return self.placesViewController!
//            return self.colcYelpPlacePickerViewController!
//            return self.foursquareViewController!
//            return self.savedTripsTableViewController!
//            return self.savedTripsUINavigationController!
//            return self.placesViewController!
            

            
        default:
            return self.colcPlacePickerViewController!
        }
        //------------------------------------------------------------------------------------------------

    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, willMoveAt index: UInt) {
        //self.log.debug("willMoveAtIndex:\(index)")
        if let delegate_ContainerVC = self.delegate_ContainerVC{
            
            //------------------------------------------------
            //errorIN may be nil
            delegate_ContainerVC.willMoveAtIndex(self, index: index)
            //------------------------------------------------
            
        }else{
            self.log.error("delegate_ContainerVC is nil")
        }
    }
    
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - COLCPlacePickerViewControllerDelegate
    //--------------------------------------------------------------
    // TODO: - CLEANUP after test

    //    func colcPlacePickerViewController_businessSelected(_ colcPlacePickerViewController: COLCPlacePickerViewController, clkPlaceSelected: CLKPlace?, currentMapCenterNearestPlace: CLKLocationPlace){
//        if let delegate_COLCPlacePickerViewControllerDelegate = self.delegate_COLCPlacePickerViewControllerDelegate {
//            
//            delegate_COLCPlacePickerViewControllerDelegate.colcPlacePickerViewController_businessSelected(colcPlacePickerViewController,
//                                                                                                 clkPlaceSelected: clkPlaceSelected,
//                                                                                     currentMapCenterNearestPlace:currentMapCenterNearestPlace)
//            
//        }else{
//            self.log.error("self.delegate_COLCPlacePickerViewControllerDelegate is nil - 6868")
//        }
//    }
//    
//    func colcPlacePickerViewController_nearestAddressSelected(_ colcPlacePickerViewController: COLCPlacePickerViewController, currentMapCenterNearestPlace: CLKLocationPlace){
//        
//        if let delegate_COLCPlacePickerViewControllerDelegate = self.delegate_COLCPlacePickerViewControllerDelegate {
//            
//            delegate_COLCPlacePickerViewControllerDelegate.colcPlacePickerViewController_nearestAddressSelected(colcPlacePickerViewController,
//                                                                                                          currentMapCenterNearestPlace:currentMapCenterNearestPlace)
//            
//        }else{
//            self.log.error("self.delegate_COLCPlacePickerViewControllerDelegate is nil - 6868")
//        }
//    }
    
    func colcPlacePickerViewController_clkPlacesPickerResult(_ colcPlacePickerViewController: COLCPlacePickerViewController, clkPlacesPickerResult: CLKPlacesPickerResult){
        if let delegate_COLCPlacePickerViewControllerDelegate = self.delegate_COLCPlacePickerViewControllerDelegate {
            
            delegate_COLCPlacePickerViewControllerDelegate.colcPlacePickerViewController_clkPlacesPickerResult(colcPlacePickerViewController,
                                                                                                                clkPlacesPickerResult:clkPlacesPickerResult)
            
        }else{
            self.log.error("self.delegate_COLCPlacePickerViewControllerDelegate is nil - 6868")
        }
    }
    
    
    func colcPlacePickerViewControllerCancelled(_ colcPlacePickerViewController: COLCPlacePickerViewController){
        if let delegate_COLCPlacePickerViewControllerDelegate = self.delegate_COLCPlacePickerViewControllerDelegate {
            
            delegate_COLCPlacePickerViewControllerDelegate.colcPlacePickerViewControllerCancelled(colcPlacePickerViewController)
            
            
        }else{
            self.log.error("self.delegate_COLCPlacePickerViewControllerDelegate is nil 7878")
        }
    }
   
    
    
    
    
    
    
    
    
    
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - COLCYelpPlacePickerViewControllerDelegate
    //--------------------------------------------------------------
    // TODO: - REMOVED YELP - COCOAPODS ERROR
//    func colcYelpPlacePickerViewControllerReturned(colcYelpPlacePickerViewController: COLCYelpPlacePickerViewController, clkPlaceSelected: CLKPlace, clkPlaceSelectedGeocodeLocation: CLKPlace?){
//        
//        
//        if let delegate_COLCYelpPlacePickerViewControllerDelegate = self.delegate_COLCYelpPlacePickerViewControllerDelegate {
//            delegate_COLCYelpPlacePickerViewControllerDelegate.colcYelpPlacePickerViewControllerReturned(colcYelpPlacePickerViewController, clkPlaceSelected: clkPlaceSelected, clkPlaceSelectedGeocodeLocation: clkPlaceSelectedGeocodeLocation)
//            
//        }else{
//            self.log.error("self.delegate_COLCYelpPlacePickerViewControllerDelegate is nil - 6868")
//        }
//        
//    }
//    
//    func colcYelpPlacePickerViewControllerCancelled(colcYelpPlacePickerViewController: COLCYelpPlacePickerViewController){
//        if let delegate_COLCYelpPlacePickerViewControllerDelegate = self.delegate_COLCYelpPlacePickerViewControllerDelegate {
//            
//            delegate_COLCYelpPlacePickerViewControllerDelegate.colcYelpPlacePickerViewControllerCancelled(colcYelpPlacePickerViewController)
//            
//            
//        }else{
//            self.log.error("self.delegate_COLCYelpPlacePickerViewControllerDelegate is nil 7878")
//        }
//    }
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - ContactsViewControllerDelegate
    //--------------------------------------------------------------
    func pickContactAddressReturned(_ contactsViewController: ContactsViewController, clkPlacesPickerResultPlace: CLKPlacesPickerResultPlace){
        
        if let delegate_ContactsViewControllerDelegate = self.delegate_ContactsViewControllerDelegate {
            delegate_ContactsViewControllerDelegate.pickContactAddressReturned(contactsViewController, clkPlacesPickerResultPlace : clkPlacesPickerResultPlace)
            
        }else{
            self.log.error("delegate_ContactsViewControllerDelegate is nil - 4444")
        }
    }
    
    func contactsViewControllerCancelled(_ contactsViewController: ContactsViewController){
        
        if let delegate_ContactsViewControllerDelegate = self.delegate_ContactsViewControllerDelegate {
            
            delegate_ContactsViewControllerDelegate.contactsViewControllerCancelled(contactsViewController)
            
        }else{
            self.log.error("delegate_ContactsViewControllerDelegate is nil - 4444")
        }
    }
    //--------------------------------------------------------------
    // MARK: - CalendarEventsViewControllerDelegate
    // MARK: -
    //--------------------------------------------------------------
    
    func controllerDidSelectCalendarEvent(_ calendarEventsViewController: CalendarEventsViewController, clkPlacesPickerResultPlace: CLKPlacesPickerResultPlace){
        if let delegate_CalendarEventsViewControllerDelegate = self.delegate_CalendarEventsViewControllerDelegate {
            
            delegate_CalendarEventsViewControllerDelegate.controllerDidSelectCalendarEvent(calendarEventsViewController, clkPlacesPickerResultPlace: clkPlacesPickerResultPlace)
            
        }else{
            self.log.error("delegate_CalendarEventsViewControllerDelegate is nil - 356343")
        }
    }
    
    func calendarEventsViewControllerCancelled(_ calendarEventsViewController: CalendarEventsViewController){
        if let delegate_CalendarEventsViewControllerDelegate = self.delegate_CalendarEventsViewControllerDelegate {
            
            delegate_CalendarEventsViewControllerDelegate.calendarEventsViewControllerCancelled(calendarEventsViewController)
            
        }else{
            self.log.error("delegate_ContactsViewControllerDelegate is nil - 436456")
        }

    }
    
    //--------------------------------------------------------------
    // MARK: - SelectPlaceViewControllerDelegate
    // MARK: -
    //--------------------------------------------------------------
    func controllerDidSelectPlace(_ selectPlaceViewController: SelectPlaceViewController, realmPlace: RealmPlace){
        //print("controllerDidSelectPlace:\(realmPlace)")
        /*
         controllerDidSelectPlace:Place {
         name = Get Licensed;
         address = Tower Bridge Business Center, 46-48 E Smithfield, Bank E1W 1AW, United Kingdom;
         place_id = ChIJZa-dB6tydkgR-ct8ncb30vA;
         latitude = 51.50871000000001;
         longitude = -0.06937600000000001;
         }
         */
        
        if let delegate_SelectPlaceViewControllerDelegate = self.delegate_SelectPlaceViewControllerDelegate {
            delegate_SelectPlaceViewControllerDelegate.controllerDidSelectPlace(selectPlaceViewController, realmPlace: realmPlace)
            
        }else{
            self.log.error("delegate_SelectPlaceViewControllerDelegate is nil - 33")
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - FBViewControllerDelegate
    // MARK: -
    //--------------------------------------------------------------

    func fbViewControllerSelectFacebookEvent(_ fbViewController: FBViewController, clkPlacesPickerResultPlace: CLKPlacesPickerResultPlace){
        
        if let delegate_FBViewControllerDelegate = self.delegate_FBViewControllerDelegate {
            delegate_FBViewControllerDelegate.fbViewControllerSelectFacebookEvent(fbViewController, clkPlacesPickerResultPlace: clkPlacesPickerResultPlace)
            
        }else{
            self.log.error("delegate_SelectPlaceViewControllerDelegate is nil - 33")
        }
    }
    
    func fbViewControllerCancelled(_ fbViewController: FBViewController){
        //print("viewControllerCancelled: fbViewController")
        if let delegate_FBViewControllerDelegate = self.delegate_FBViewControllerDelegate {
            delegate_FBViewControllerDelegate.fbViewControllerCancelled(fbViewController)
            
        }else{
            self.log.error("delegate_SelectPlaceViewControllerDelegate is nil - 33")
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - FoursquareViewControllerDelegate
    // MARK: -
    //--------------------------------------------------------------
    
    func controllerDidSelectCOLCFoursquareEvent(_ foursquareViewController: FoursquareViewController, colcFoursquareEvent: COLCFoursquareEvent){
        print("controllerDidSelectPlace:\(colcFoursquareEvent)")
        if let delegate_FoursquareViewControllerDelegate = self.delegate_FoursquareViewControllerDelegate {
            delegate_FoursquareViewControllerDelegate.controllerDidSelectCOLCFoursquareEvent(foursquareViewController, colcFoursquareEvent: colcFoursquareEvent)
            
        }else{
            self.log.error("delegate_SelectPlaceViewControllerDelegate is nil - 33")
        }
    }
    
    func foursquareViewControllerCancelled(_ foursquareViewController: FoursquareViewController){
        //print("viewControllerCancelled: foursquareViewController")
        if let delegate_FoursquareViewControllerDelegate = self.delegate_FoursquareViewControllerDelegate {
            delegate_FoursquareViewControllerDelegate.foursquareViewControllerCancelled(foursquareViewController)
            
        }else{
            self.log.error("delegate_SelectPlaceViewControllerDelegate is nil - 33")
        }
    }
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - LIFECYCLE
    //--------------------------------------------------------------
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
