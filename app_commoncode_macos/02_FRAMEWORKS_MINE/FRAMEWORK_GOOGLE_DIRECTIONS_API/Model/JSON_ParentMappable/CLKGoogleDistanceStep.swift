//
//  CLKGoogleDistanceStep.swift
//  joyride
//
//  Created by Brian Clear on 25/01/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation

class CLKGoogleDistanceStep: ParentMappable {
    var polyline: CLKGooglePolyline?
    var duration: CLKGoogleMeasurement?
    var html_instructions: String?
    var start_location: CLKGooglePlaceLocation?
    var end_location: CLKGooglePlaceLocation?
    var travel_mode: String?
    var distance: CLKGoogleMeasurement?
    var maneuver: String?
    
    
    // root calls mapping()
    //    required init?(_ map: Map){
    //        mapping(map: map)
    //    }
    
    override func mapping(map: Map){
        polyline <- map["polyline"]
        duration <- map["duration"]
        html_instructions <- map["html_instructions"]
        start_location <- map["start_location"]
        end_location <- map["end_location"]
        travel_mode <- map["travel_mode"]
        distance <- map["distance"]
        maneuver <- map["maneuver"]
        
    }
}
