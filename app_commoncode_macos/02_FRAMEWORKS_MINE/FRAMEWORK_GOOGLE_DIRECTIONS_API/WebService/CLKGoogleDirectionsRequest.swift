//
//  CLKGoogleDirectionsRequest.swift
//  joyride
//
//  Created by Brian Clear on 22/01/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation

//BEWARE this is mode=
//there is also a //transit_mode if your want Transit/Tube only
enum CLKGoogleDirectionsTravelMode: String{

    case driving = "driving" //(default) indicates standard driving directions using the road network.
    case walking = "walking" //requests walking directions via pedestrian paths & sidewalks (where available).
    case bicycling = "bicycling" //requests bicycling directions via bicycle paths & preferred streets (where available).
    case transit = "transit"  //
}


//Docs at bottom
class CLKGoogleDirectionsRequest: CLKGoogleDirectionsParentRequest {

    //===============================
    //Required parameters
    //===============================
    var origin:String?
    var destination:String?
    //in parent
    //var key:String?
    
    //===============================
    //Optional parameters
    //===============================
    var mode:String?
    var waypoints:String?
    var alternatives:String?
    var avoid:String?
    
    //-----------------------------------------------------------------------------------
    // TODO: - HANDLE LANGUAGE
    //var language:String? //declared in CLKGoogleDirectionsParentRequest
    //-----------------------------------------------------------------------------------
    var units:String?
    var region:String?
    var arrival_time:String?
    var departure_time:String?
    var traffic_model:String?
    //-----------------------------------------------------------------------------------
    
    //use with mode
    var transit_mode:String?
    var transit_routing_preference:String?
    
    var clkGoogleDirectionsTravelMode: CLKGoogleDirectionsTravelMode = .walking{
        didSet {
            print("[clkGoogleDirectionsTravelMode]didSet: Old value is \(oldValue), new value is \(clkGoogleDirectionsTravelMode)")
            transit_mode = clkGoogleDirectionsTravelMode.rawValue
        }
    }
    
    //------------------------------------------------
    //NON JSON MEMBERS
    //------------------------------------------------
    var clkGoogleDirectionsLocation_origin: CLKGoogleDirectionsLocation?
    var clkGoogleDirectionsLocation_destination: CLKGoogleDirectionsLocation?

    
    

    
    // TODO: - ADD LANGUAGES
    override init(){
        super.init()
        query = "/directions/json"        /* must start with /  */
        
    }
    
    //default to Driving
    convenience init(
        clkGoogleDirectionsLocation_origin: CLKGoogleDirectionsLocation,
        clkGoogleDirectionsLocation_destination: CLKGoogleDirectionsLocation
        )
    {
        self.init(clkGoogleDirectionsLocation_origin: clkGoogleDirectionsLocation_origin,
                  clkGoogleDirectionsLocation_destination: clkGoogleDirectionsLocation_destination,
                  clkGoogleDirectionsTravelMode: CLKGoogleDirectionsTravelMode.walking)
        
    }
    
    convenience init(
        clkGoogleDirectionsLocation_origin: CLKGoogleDirectionsLocation,
        clkGoogleDirectionsLocation_destination: CLKGoogleDirectionsLocation,
        clkGoogleDirectionsTravelMode: CLKGoogleDirectionsTravelMode
        )
    {
        self.init()
        //-----------------------------------------------------------------------------------
        //set default = also set json transit_mode
        self.clkGoogleDirectionsTravelMode = clkGoogleDirectionsTravelMode
        //issue didSet not triggered have to set it manually here
        self.transit_mode = clkGoogleDirectionsTravelMode.rawValue
        //-----------------------------------------------------------------------------------
        self.clkGoogleDirectionsLocation_origin = clkGoogleDirectionsLocation_origin
        self.clkGoogleDirectionsLocation_destination = clkGoogleDirectionsLocation_destination
        
        // &origin=  varies per type
        self.origin = self.clkGoogleDirectionsLocation_origin?.clkGoogleDirectionsLocationString
        
        // &destination=  varies per type
        self.destination = self.clkGoogleDirectionsLocation_destination?.clkGoogleDirectionsLocationString
        
    }
    
    
    
    
    convenience init?(
        addressStringFrom: String,
        addressStringTo: String)
    {
    
        if let clkGoogleDirectionsLocation_origin = CLKGoogleDirectionsLocation(addressString: addressStringFrom){
            if let clkGoogleDirectionsLocation_destination = CLKGoogleDirectionsLocation(addressString: addressStringTo){
                //------------------------------------------------------------------------------------------------
                
                self.init(clkGoogleDirectionsLocation_origin: clkGoogleDirectionsLocation_origin,
                          clkGoogleDirectionsLocation_destination: clkGoogleDirectionsLocation_destination)
                
                //------------------------------------------------------------------------------------------------
                
            }else{
                //cant use log ivar in init
                MyXCodeEmojiLogger.defaultInstance.error("CLKGoogleDirectionsLocation init failed")
                return nil
            }
        }else{
            //cant use log ivar in init
            MyXCodeEmojiLogger.defaultInstance.error("CLKGoogleDirectionsLocation init failed")
            return nil
        }
    }
    
    convenience init?(
        placeIdFrom: String,
        placeIdTo: String)
    {
        
        if let clkGoogleDirectionsLocation_origin = CLKGoogleDirectionsLocation(placeId: placeIdFrom){
            if let clkGoogleDirectionsLocation_destination = CLKGoogleDirectionsLocation(placeId: placeIdTo){
                //------------------------------------------------------------------------------------------------
                
                self.init(clkGoogleDirectionsLocation_origin: clkGoogleDirectionsLocation_origin,
                    clkGoogleDirectionsLocation_destination: clkGoogleDirectionsLocation_destination)
                
                //------------------------------------------------------------------------------------------------
                
            }else{
                //cant use log ivar in init
                print("CLKGoogleDirectionsLocation init failed")
                return nil
            }
        }else{
            //cant use log ivar in init
            print("CLKGoogleDirectionsLocation init failed")
            return nil
        }
    }
    
     //lat : NSNumber, lng : NSNumber
    convenience init?(
        latFrom: NSNumber, lngFrom: NSNumber,
        latTo: NSNumber, lngTo: NSNumber)
    {
        
        if let clkGoogleDirectionsLocation_origin = CLKGoogleDirectionsLocation(lat: latFrom, lng: lngFrom){
            if let clkGoogleDirectionsLocation_destination = CLKGoogleDirectionsLocation(lat: latTo, lng: lngTo){
                //------------------------------------------------------------------------------------------------
                
                self.init(clkGoogleDirectionsLocation_origin: clkGoogleDirectionsLocation_origin,
                     clkGoogleDirectionsLocation_destination: clkGoogleDirectionsLocation_destination,
                               clkGoogleDirectionsTravelMode: CLKGoogleDirectionsTravelMode.walking
                    
                )
                
                //------------------------------------------------------------------------------------------------
                
            }else{
                //cant use log ivar in init
                print("CLKGoogleDirectionsLocation init failed")
                return nil
            }
        }else{
            //cant use log ivar in init
            print("CLKGoogleDirectionsLocation init failed")
            return nil
        }
    }
    

    convenience init?(
        clkPlaceStart: CLKPlace,
        clkPlaceEnd: CLKPlace)
    {
        
        if let clLocationStart = clkPlaceStart.clLocation{
            if let clLocationEnd = clkPlaceEnd.clLocation{
                //---------------------------------------------------------------------
                if let clkGoogleDirectionsLocationStart = CLKGoogleDirectionsLocation(clLocation: clLocationStart){
                    if let clkGoogleDirectionsLocationEnd = CLKGoogleDirectionsLocation(clLocation: clLocationEnd){
                        //------------------------------------------------------------------------------------------------
                        self.init(clkGoogleDirectionsLocation_origin: clkGoogleDirectionsLocationStart,
                                  clkGoogleDirectionsLocation_destination: clkGoogleDirectionsLocationEnd)
                        //------------------------------------------------------------------------------------------------
                    }else{
                        appDelegate.log.error("CLKGoogleDirectionsLocation(clLocation: clLocationEnd) is nil")
                        return nil
                    }
                }else{
                    appDelegate.log.error("CLKGoogleDirectionsLocation(clLocation: clLocationStart) is nil")
                    return nil
                }
                //---------------------------------------------------------------------
            }
            else
            {
                appDelegate.log.error("UNABLE TO CONVERT clkPlaceStart to CLKGoogleDirectionsLocation")
                return nil
            }
        }
        else
        {
            appDelegate.log.error("UNABLE TO CONVERT clkPlaceStart to CLKGoogleDirectionsLocation")
            return nil
        }
    }
    
    override func parametersAppendAndCheckRequiredFieldsAreSet() -> Bool{
        var requiredFieldsAreSet = false
        
        let requiredFieldsAreSetInSuper = super.parametersAppendAndCheckRequiredFieldsAreSet()
        if requiredFieldsAreSetInSuper{
            
            //------------------------------------------------
            //address will be geocoded first
            //https:// maps.googleapis.com/maps/api/directions/json?origin=Toronto&destination=Montreal&key=YOUR_API_KEY
            //https:// maps.googleapis.com/maps/api/directions/json?origin=place_id:ChIJ685WIFYViEgRHlHvBbiD5nE&destination=place_id:ChIJA01I-8YVhkgRGJb0fW4UX7Y&key=YOUR_API_KEY
            //------------------------------------------------
            //------------------------------------------------
            if let origin = origin{
                if let destination = destination{
                    //------------------------------------------------
                    // TODO: - DOESNT HANDLED blanks
                    parameters["origin"] = origin as AnyObject?
                    parameters["destination"] = destination as AnyObject?
                    if origin == destination{
                        self.log.error("origin and destination are same")
                    }else{
                        
                    }
                    requiredFieldsAreSet = true
                    //------------------------------------------------
                }else{
                    self.log.error("radius is nil")
                }
            }else{
                self.log.error("origin is nil")
            }

            if requiredFieldsAreSet{
                
                //---------------------------------------------------------------------
                //OPTIONAL PARAMETERS
                //---------------------------------------------------------------------
                if let mode:String = mode{
                    parameters["mode"] = mode as AnyObject?
                }else{
                    //self.log.debug("mode is nil - OK - optional")
                }
                //---------------------------------------------------------------------
                if let waypoints = waypoints{
                    parameters["waypoints"] = waypoints as AnyObject?
                }else{
                    //self.log.debug("waypoints is nil - OK - optional")
                }
                //---------------------------------------------------------------------
                if let alternatives = alternatives{
                    parameters["alternatives"] = alternatives as AnyObject?
                }else{
                    //self.log.debug("alternatives is nil - OK - optional")
                }
                //---------------------------------------------------------------------
                if let avoid = avoid{
                    parameters["avoid"] = avoid as AnyObject?
                }else{
                    //self.log.debug("avoid is nil - OK - optional")
                }
                //---------------------------------------------------------------------
                //checked in parent
                if let language = language{
                    parameters["language"] = language as AnyObject?
                }else{
                    //self.log.debug("language is nil - OK - optional")
                }
                //---------------------------------------------------------------------
                if let units = units{
                    parameters["units"] = units as AnyObject?
                }else{
                    //self.log.debug("units is nil - OK - optional")
                }
                //---------------------------------------------------------------------
                if let region = region{
                    parameters["region"] = region as AnyObject?
                }else{
                    //self.log.debug("region is nil - OK - optional")
                }
                //---------------------------------------------------------------------
                if let arrival_time = arrival_time{
                    parameters["arrival_time"] = arrival_time as AnyObject?
                }else{
                    //self.log.debug("arrival_time is nil - OK - optional")
                }
                //---------------------------------------------------------------------
                if let departure_time = departure_time{
                    parameters["departure_time"] = departure_time as AnyObject?
                }else{
                    //self.log.debug("departure_time is nil - OK - optional")
                }
                //---------------------------------------------------------------------
                if let traffic_model = traffic_model{
                    parameters["traffic_model"] = traffic_model as AnyObject?
                }else{
                    //self.log.debug("traffic_model is nil - OK - optional")
                }
                //---------------------------------------------------------------------
                if let transit_mode = transit_mode{
                    parameters["transit_mode"] = transit_mode as AnyObject?
                }else{
                    //self.log.debug("transit_mode is nil - OK - optional")
                }
                //---------------------------------------------------------------------
                if let transit_routing_preference = transit_routing_preference{
                    parameters["transit_routing_preference"] = transit_routing_preference as AnyObject?
                }else{
                    //self.log.debug("transit_routing_preference is nil - OK - optional")
                }
                //---------------------------------------------------------------------
            }else{
                self.log.error("requiredFieldsAreSet is false - skip OPTIONAL FIELDS")
            }
            //------------------------------------------------
        }else{
            //required param missing in super class
            requiredFieldsAreSet = false
        }
        
        return requiredFieldsAreSet
    }
}
/*

    https://developers.google.com/maps/documentation/directions/intro#DirectionsRequests


    ===============================
    Required parameters
    ===============================
    origin —
    destination —
    key —

    ===============================
    Optional parameters
    ===============================
    mode (defaults to driving) —
    waypoints —
    alternatives —
    avoid —
    language —
    units —
    region —
    arrival_time —
    departure_time —
    traffic_model (defaults to best_guess) —
    transit_mode —
    transit_routing_preference —




    ===============================
    Required parameters
    ===============================
    origin —

    The address, textual latitude/longitude value, or place ID from which you wish to calculate directions.

    If you pass an address as a string, the Directions service will geocode the string and convert it to a latitude/longitude coordinate to calculate directions.
    This coordinate may be different from that returned by the Google Maps Geocoding API, for example a building entrance rather than its center.

    If you pass coordinates, they will be used unchanged to calculate directions.
    Ensure that no space exists between the latitude and longitude values.
    Place IDs must be prefixed with place_id:.
    The place ID may only be specified if the request includes an API key or a Google Maps APIs Premium Plan client ID.
    You can retrieve place IDs from the Google Maps Geocoding API and the Google Places API (including Place Autocomplete).
    For an example using place IDs from Place Autocomplete, see Place Autocomplete and Directions.
    For more about place IDs, see the place ID overview.

    destination —
    The address, textual latitude/longitude value, or place ID to which you wish to calculate directions.
    The options for the destination parameter are the same as for the origin parameter, described above.

    key —
    Your application's API key.
    This key identifies your application for purposes of quota management.
    Learn how to get a key.
    Note: Google Maps APIs Premium Plan customers may use either an API key, or a valid client ID and digital signature, in your Directions requests.
    Get more information on authentication parameters for Premium Plan customers.

    ===============================
    Optional parameters
    ===============================
    mode (defaults to driving) —
    Specifies the mode of transport to use when calculating directions.
    Valid values and other request details are specified in Travel Modes.

    waypoints —
    Specifies an array of waypoints.
    Waypoints alter a route by routing it through the specified location(s).
    A waypoint is specified as either a latitude/longitude coordinate, as a place ID, or as an address which will be geocoded.
    Place IDs must be prefixed with place_id:.
    The place ID may only be specified if the request includes an API key or a Google Maps APIs Premium Plan client ID.
    Waypoints are only supported for driving, walking and bicycling directions.
    (For more information on waypoints, see Using Waypoints in Routes below.)
    alternatives —
    If set to true, specifies that the Directions service may provide more than one route alternative in the response.
    Note that providing route alternatives may increase the response time from the server.

    avoid —
    Indicates that the calculated route(s) should avoid the indicated features.

    This parameter supports the following arguments:
    tolls:
    indicates that the calculated route should avoid toll roads/bridges.
    highways:
    indicates that the calculated route should avoid highways.
    ferries:
    indicates that the calculated route should avoid ferries.
    indoor:
    indicates that the calculated route should avoid indoor steps for walking and transit directions.
    Only requests that include an API key or a Google Maps APIs Premium Plan client ID will receive indoor steps by default.
    For more information see Route Restrictions below.

    language —
    Specifies the language in which to return results.
    See the list of supported domain languages.
    Note that we often update supported languages so this list may not be exhaustive.
    If language is not supplied, the service will attempt to use the native language of the domain from which the request is sent.

    units —
    Specifies the unit system to use when displaying results.
    Valid values are specified in Unit Systems below.

    region —
    Specifies the region code, specified as a ccTLD ("top-level domain") two-character value.
    (For more information see Region Biasing below.)

    arrival_time —
    Specifies the desired time of arrival for transit directions, in seconds since midnight, January 1, 1970 UTC.
    You can specify either departure_time or arrival_time, but not both.
    Note that
    arrival_time must be specified as an integer.

    departure_time —
    Specifies the desired time of departure.
    You can specify the time as an integer in seconds since midnight, January 1, 1970 UTC.
    Alternatively, you can specify a value of now, which sets the departure time to the current time (correct to the nearest second).
    The departure time may be specified in two cases:
    For requests where the travel mode is transit: You can optionally specify one of departure_time or arrival_time.
    If neither time is specified, the departure_time defaults to now (that is, the departure time defaults to the current time).
    For requests where the travel mode is driving: You can specify the departure_time to receive a route and trip duration (response field: duration_in_traffic) that take traffic conditions 		into account.
    This option is only available if the request contains a valid API key, or a valid Google Maps APIs Premium Plan client ID and signature.
    The departure_time must be set to the current time or some time in the future.
    It cannot be in the past.

    traffic_model (defaults to best_guess) —
    Specifies the assumptions to use when calculating time in traffic.
    This setting affects the value returned in the duration_in_traffic field in the response, which contains the predicted time in traffic based on historical averages.
    The traffic_model parameter may only be specified for driving directions where the request includes a departure_time, and only if the request includes an API key or a Google Maps APIs Premium Plan client ID.


    The available values for this parameter are:
    best_guess (default):
    indicates that the returned duration_in_traffic should be the best estimate of travel time given what is known about both historical traffic conditions and live traffic.
    Live traffic becomes more important the closer the departure_time is to now.
    pessimistic:
    indicates that the returned duration_in_traffic should be longer than the actual travel time on most days, though occasional days with particularly bad traffic conditions may exceed this value.
    optimistic:
    indicates that the returned duration_in_traffic should be shorter than the actual travel time on most days, though occasional days with particularly good traffic conditions may be faster than this value.
    The default value of best_guess will give the most useful predictions for the vast majority of use cases.
    The best_guess travel time prediction may be shorter than optimistic, or alternatively, longer than pessimistic, due to the way the best_guess prediction model integrates live traffic information.


    transit_mode —
    Specifies one or more preferred modes of transit.
    This parameter may only be specified for transit directions, and only if the request includes an API key or a Google Maps APIs Premium Plan client ID.
    The parameter supports the following arguments:
    bus:
    indicates that the calculated route should prefer travel by bus.
    subway:
    indicates that the calculated route should prefer travel by subway.
    train:
    indicates that the calculated route should prefer travel by train.
    tram:
    indicates that the calculated route should prefer travel by tram and light rail.
    rail:
    indicates that the calculated route should prefer travel by train, tram, light rail, and subway.
    This is equivalent to transit_mode=train|tram|subway.

    transit_routing_preference —
    Specifies preferences for transit routes.
        
        
        Using this parameter, you can bias the options returned, rather than accepting the default best route chosen by the API.
    This parameter may only be specified for transit directions, and only if the request includes an API key or a Google Maps APIs Premium Plan client ID.
    The parameter supports the following arguments:
    less_walking:
    indicates that the calculated route should prefer limited amounts of walking.
    fewer_transfers:
    indicates that the calculated route should prefer a limited number of transfers.

*/
