//
//  GoogleDirectionsWSController.swift
//  joyride
//
//  Created by Brian Clear on 22/01/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation

//https://developers.google.com/maps/documentation/directions/intro#DirectionsRequests
class GoogleDirectionsWSController: ParentWSController{
    
    //https://maps.googleapis.com/maps/api/directions/json?origin=-0.1278113,53.4244671&destination=Liverpool&&key=AIzaSyCO41p9UNH6K6gwZO-VHY6z8ibsdnquStw
    
   // OK:https:// maps.googleapis.com/maps/api/directions/json?origin=17.863446657938162,100.26915904062152&destination=17.12999010224565,103.36730357187415&sensor=false&key=AIzaSyCO41p9UNH6K6gwZO-VHY6z8ibsdnquStw
    
    func get_google_directions(_ clkGoogleDirectionsRequest: CLKGoogleDirectionsRequest,
        success: @escaping (_ clkGoogleDirectionsResponse: CLKGoogleDirectionsResponse?) -> Void,
        failure: @escaping (_ error: Error?) -> Void
        )
    {

        //https://maps.googleapis.com/maps/api/directions/json?origin=Toronto&destination=Montreal&key=AIzaSyCh_0tfBGDU7l_RHXuq5zol9amft7e3szg
        //---------------------------------------------------------------------
        //CALL THE WS
        //---------------------------------------------------------------------
        //if let urlString =  clkGoogleDirectionsRequest.urlString{
        if let _ =  clkGoogleDirectionsRequest.urlString{
            
            //NOISY self.log.debug("urlString:\r\(urlString)")
            //---------------------------------------------------------------------
            self.callWS(
                clkGoogleDirectionsRequest,
                
                success:{
                    (parsedObject: Any?)->Void in
                    //------------------------------------------------------------------------------------------------
                    //---------------------------------------------------------------------
                    //response handler takes CLKGoogleDirectionsResponse? so use
                    //if let clkGoogleDirectionsResponse : CLKGoogleDirectionsResponse? =
                    //instead of
                    //if let clkGoogleDirectionsResponse =
                    //as this is a CLKGoogleDirectionsResponse not CLKGoogleDirectionsResponse? so response(clkGoogleDirectionsResponse) wont take it
                    if let clkGoogleDirectionsResponse : CLKGoogleDirectionsResponse = Mapper<CLKGoogleDirectionsResponse>().map(JSONObject:parsedObject){
                        //---------------------------------------------------------------------
                        self.log.debug("clkGoogleDirectionsRequest returned")
                        //---------------------------------------------------------------------
                        //check response status
                        if let status = clkGoogleDirectionsResponse.status{
                            switch status{
                            case GooglePlacesResponseStatus.OK.rawValue:
                                self.log.debug("status: OK")
                                //---------------------------------------------------------------------
                                //RESPONSE OK
                                //---------------------------------------------------------------------
                                success(clkGoogleDirectionsResponse)
                                //---------------------------------------------------------------------
                                // TODO: - HANDLE EACH OF THESE PROPERLY
                            case GooglePlacesResponseStatus.ZERO_RESULTS.rawValue:
                                self.log.debug("status: ZERO_RESULTS")
                                self.log.debug("parsedObject:\r\(String(describing: parsedObject))")
                                //return failure( NSError.appError(.GooglePlacesResponseStatus_ZERO_RESULTS_PLACES))
                                return failure( NSError.appError(.googlePlacesResponseStatus_ZERO_RESULTS_DISTANCE))
                                
                            case GooglePlacesResponseStatus.OVER_QUERY_LIMIT.rawValue:
                                self.log.debug("status: OVER_QUERY_LIMIT")
                                self.log.debug("parsedObject:\r\(String(describing: parsedObject))")
                                return failure( NSError.appError(.googlePlacesResponseStatus_OVER_QUERY_LIMIT))
                                
                            case GooglePlacesResponseStatus.REQUEST_DENIED.rawValue:
                                self.log.debug("status: REQUEST_DENIED")
                                self.log.debug("parsedObject:\r\(String(describing: parsedObject))")
                                
                                return failure( NSError.appError(.googlePlacesResponseStatus_REQUEST_DENIED))
                                
                                
                                /*
                                 https://console.developers.google.com/apis/credentials?project=tfl-taxi-ranks
                                 {
                                 "error_message" : "Browser API keys cannot have referer restrictions when used with this API.",
                                 "routes" : [],
                                 "status" : "REQUEST_DENIED"
                                 }
                                 ..fixed by changing it to iOS app only
                                 
                                 
                                 {
                                 "error_message" : "This IP, site or mobile application is not authorized to use this API key. Request received from IP address 213.133.153.253, with empty referer",
                                 "routes" : [],
                                 "status" : "REQUEST_DENIED"
                                 }
                                 ..tried from website but not allowed as changed to ios app
                                 */
                                
                            case GooglePlacesResponseStatus.INVALID_REQUEST.rawValue:
                                
                                self.log.debug("status: INVALID_REQUEST")
                                self.log.debug("parsedObject:\r\(String(describing: parsedObject))")
                                return failure( NSError.appError(.googlePlacesResponseStatus_INVALID_REQUEST))
                                
                            default:
                                //HANDLE ALL ERRORS - if you need to handle specific ones such as NO RESULT can comment out here but probably should handle in delegate/caller
                                self.log.error("UNKNOWN status: \(status)")
                                self.log.debug("parsedObject:\r\(String(describing: parsedObject))")
                                return failure( NSError.googlePlacesAPIStatusError(status))
                            }
                            
                        }else{
                            self.log.error("clkGoogleDirectionsResponse.status is nil")
                            return failure( NSError.unexpectedResponseObject("clkGoogleDirectionsResponse.status is nil"))
                        }
                        
                        //------------------------------------------------------------------------------------------------
                        
                    }else{
                        self.log.error("clkGoogleDirectionsResponse is nil or not CLKGoogleDirectionsResponse")
                    }
                    //------------------------------------------------------------------------------------------------
                    
                },
                failure:{
                    (error) -> Void in
                    //NO WS ERROR but may be status errors see success:
                    self.log.error("error:\(error)")
                    failure( error)
                }
            )
            //---------------------------------------------------------------------
        }else{
            self.log.error("urlString is nil - 33333")
        }
    }
}
