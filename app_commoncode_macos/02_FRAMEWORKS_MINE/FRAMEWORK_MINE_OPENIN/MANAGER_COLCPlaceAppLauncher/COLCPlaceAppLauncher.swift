//
//  COLCPlaceAppLauncher.swift
//  joyride
//
//  Created by Brian Clear on 13/11/2015.
//  Copyright © 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation
class COLCPlaceAppLauncher :ParentSwiftObject{


    //end can be option
    func openLocationsInApp(_ mapApp: CMMapApp,
        clkPlaceStart: CLKPlace,
        clkPlaceEnd: CLKPlace,
        openWithTravelMode: OpenWithTravelMode,
        success: @escaping () -> Void,
        failure: @escaping (_ errorMessage : String) -> Void,
        failureNotInstalled: @escaping () -> Void
        
        )
    {
       
        var locationDataErrorMessage = ""
        
        let cmMapPointFROM = CMMapPoint()
        let cmMapPointTO = CMMapPoint()
        //---------------------------------------------------------------------
        //cmMapPointFROM.name = clkPlaceStart.name(fallbackNameString:"START")
        
        if let name = clkPlaceStart.name {
            cmMapPointFROM.name = name
        }else{
            appDelegate.log.error("clkPlaceStart.name is nil: using 'PICKUP'")
            cmMapPointFROM.name = "PICKUP"
        }
        
        //------------------------------------------------
        if let clLocation = clkPlaceStart.clLocation {
            cmMapPointFROM.coordinate = clLocation.coordinate
            
        }else{
            self.log.error("clkPlaceStart.clLocation is nil")
            locationDataErrorMessage = "clkPlaceStart.clLocation is not set - cannot Open App"
        }
        //---------------------------------------------------------------------
        //---------------------------------------------------------------------
        // TODO: - removed fallback method - use .name only
        // cmMapPointTO.name = clkPlaceEnd.name(fallbackNameString:"END")
        
        
        if let name = clkPlaceEnd.name {
            cmMapPointTO.name = name
        }else{
            appDelegate.log.error("clkPlaceStart.name is nil: using 'DESTINATION'")
            cmMapPointTO.name = "DESTINATION"
        }
        //---------------------------------------------------------------------
        
        if let clLocation = clkPlaceEnd.clLocation {
            cmMapPointTO.coordinate = clLocation.coordinate
            
        }else{
            self.log.error("clkPlaceStart.clLocation is nil")
            locationDataErrorMessage = "clkPlaceEnd.clLocation is not set - cannot Open App"
        }
       
        //---------------------------------------------------------------------
        //---------------------------------------------------------------------
        //---------------------------------------------------------------------
        
        if locationDataErrorMessage == ""{
            
            //uses currentLocation for other point so wont work in Simulator
            // TODO: - if end is nil
//            CMMapLauncher.launchMapApp(.CMMapAppAppleMaps, forDirectionsTo: cmMapPoint)
            //CMMapLauncher.launchMapApp(mapApp, forDirectionsFrom: cmMapPointFROM, to: cmMapPointTO)
            
            
            CMMapLauncher.launchMapApp(mapApp,
                                       forDirectionsFrom: cmMapPointFROM,
                                       to: cmMapPointTO,
                                       openWithTravelMode: openWithTravelMode,
                success:{() -> Void in
                    success()
                    
                },
                failure:{(errMsg) -> Void in
                    //CLKAlertController.showAlertInVC(self, title: "Error opening application", message: errMsg)
                    
                    failure(errMsg)
                    
                },
                failureNotInstalled:{() -> Void in
                    //CLKAlertController.showAlertInVC(self, title: "Application is not installed", message: "open in Apple Maps?")
                    failureNotInstalled()
                }
            )//launchMapApp
        }else{
            self.log.error("locationDataErrorMessage is ''")
        }
    }
}
