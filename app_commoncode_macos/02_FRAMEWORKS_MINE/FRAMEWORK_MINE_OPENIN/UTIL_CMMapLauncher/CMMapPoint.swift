//
//  CMMapPoint.swift
//  joyride
//
//  Created by Brian Clear on 16/10/2015.
//  Copyright © 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import MapKit

class CMMapPoint {
    
    let log = MyXCodeEmojiLogger.defaultInstance
    /**
    Determines whether this map point represents the user's current location.
    */
    var isCurrentLocation : Bool = false
    
    /**
    The geographical coordinate of the map point.
    */
    var coordinate : CLLocationCoordinate2D?
    
    /**
    The user-visible name of the given map point (optional, may be nil).
    */
    //@property (nonatomic, copy) NSString *name;
    var name: String = ""
    /**
    The address of the given map point (optional, may be nil).
    */
    //@property (nonatomic, copy) NSString *address;
    var address: String = ""
    /**
    Gives an MKMapItem corresponding to this map point object.
    */
    //@property (nonatomic, readonly) MKMapItem *MKMapItem;
    
    var mkMapItem: MKMapItem?
    
    init(){
        //super.init()
        
    }
    
    
    class func currentLocation() -> CMMapPoint {
        let mapPoint: CMMapPoint = CMMapPoint()
        mapPoint.isCurrentLocation = true
        return mapPoint
    }
    
    class func mapPointWithCoordinate(_ coordinate: CLLocationCoordinate2D) -> CMMapPoint {
        let mapPoint: CMMapPoint = CMMapPoint()
        mapPoint.coordinate = coordinate
        return mapPoint
    }
    
    class func mapPointWithName(_ name: String, coordinate: CLLocationCoordinate2D) -> CMMapPoint {
        let mapPoint: CMMapPoint = CMMapPoint()
        mapPoint.name = name
        mapPoint.coordinate = coordinate
        return mapPoint
    }
    
    class func mapPointWithName(_ name: String, address: String, coordinate: CLLocationCoordinate2D) -> CMMapPoint {
        let mapPoint: CMMapPoint = CMMapPoint()
        mapPoint.name = name
        mapPoint.address = address
        mapPoint.coordinate = coordinate
        return mapPoint
    }
    
    class func mapPointWithAddress(_ address: String, coordinate: CLLocationCoordinate2D) -> CMMapPoint {
        let mapPoint: CMMapPoint = CMMapPoint()
        mapPoint.address = address
        mapPoint.coordinate = coordinate
        return mapPoint
    }
    
    func name1() -> String {
        if self.isCurrentLocation {
            return "Current Location"
        }
        return self.name
    }
    
    func mapItemForCMMapPoint() -> MKMapItem? {
        var mapItemReturned: MKMapItem? = nil
        
        if self.isCurrentLocation {
            mapItemReturned = MKMapItem.forCurrentLocation()
        }
        else if let coordinate = self.coordinate
        {
            let placemark: MKPlacemark = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
            let item: MKMapItem = MKMapItem(placemark: placemark)
            item.name = self.name
            mapItemReturned = item

        }else{
            self.log.error("self.coordinate is nil")
        }
        
        return mapItemReturned
    }
}
