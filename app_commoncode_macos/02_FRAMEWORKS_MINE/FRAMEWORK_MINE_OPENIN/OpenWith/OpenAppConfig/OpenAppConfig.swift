//
//  OpenAppConfig.swift
//  joyride
//
//  Created by Brian Clear on 27/05/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit

class OpenAppConfig: ParentNSObject{
    var openWithType : OpenWithType
    
    var openWithSource: OpenWithSource = .OpenWithSource_DeepLinking //Default
    
    var appName: String{
        return self.openWithType.name()
    }
    
    init(openWithType: OpenWithType, openWithSource: OpenWithSource){
        self.openWithType = openWithType
        self.openWithSource = openWithSource
    }
    
    func open(_ presentingViewController: UIViewController,
               clkPlaceStart: CLKPlace,
               clkPlaceEnd: CLKPlace,
               openWithTravelMode: OpenWithTravelMode,
               success: () -> Void,
               failure: (_ errorMessage : String) -> Void)
    {
        //---------------------------------------------------------------------
        self.log.error("MUST SUBCLASS OpenAppConfig")
        //---------------------------------------------------------------------
        
    }
    
}
