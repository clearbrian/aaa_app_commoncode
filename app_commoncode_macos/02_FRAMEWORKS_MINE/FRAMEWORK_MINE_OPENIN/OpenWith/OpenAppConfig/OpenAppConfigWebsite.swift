//
//  OpenAppConfigWebsite.swift
//  joyride
//
//  Created by Brian Clear on 30/05/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
class OpenAppConfigWebsite: OpenAppConfig{
    
    var appWebsiteURLString: String
    
    //------------------------------------------------------------------------------------------------
    //init
    //------------------------------------------------------------------------------------------------
    init(openWithType:OpenWithType, appWebsiteURLString: String, openWithSource: OpenWithSource)
    {
        self.appWebsiteURLString = appWebsiteURLString

        super.init(openWithType: openWithType, openWithSource: openWithSource)
        
    }
    
}
