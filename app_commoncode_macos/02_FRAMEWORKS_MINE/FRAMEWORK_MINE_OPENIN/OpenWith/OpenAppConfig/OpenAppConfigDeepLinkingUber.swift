//
//  OpenAppConfigDeepLinkingUber.swift
//  joyride
//
//  Created by Brian Clear on 04/06/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit

class OpenAppConfigDeepLinkingUber: OpenAppConfigDeepLinking{
    
    //-----------------------------------------------------------------------------------
    //https://developer.uber.com/apps/clients/knbZslCizsL_ae_2geHPEr4l5ouMoMu0
    //-----------------------------------------------------------------------------------
    let kUBER_CLIENT_ID:String = "knbZslCizsL_ae_2geHPEr4l5ouMoMu0"
    //-----------------------------------------------------------------------------------
    // TODO: - call ws getProductsForLocation
    ///Users/gbxc/Documents/00_ME/00_BITBUCKET_COLC/app_uber_contacts/00_CODE/00_OTHER_CODE/00_GITHUB/sachinkesiraju/UberKit/UberKitDemo/UberKitDemo/ViewController.m
    //-----------------------------------------------------------------------------------
    let kUBER_PRODUCT_ID:String = "a1111c8c-c720-46c3-8534-2fcdd730040d"
    
    init(){
        super.init(openWithType : OpenWithType.openWithType_Uber,
            appSchemeString     : "uber://",
            appDeepLinkURLString: "",
            appWebsiteURLString : "https://uber.com",
            appStoreIdUInt      : 368677368)
    }
    
    
    
    override func open( _ presentingViewController: UIViewController,
                        clkPlaceStart: CLKPlace,
                        clkPlaceEnd: CLKPlace,
                        openWithTravelMode: OpenWithTravelMode,
                        success: () -> Void,
                        failure: (_ errorMessage : String) -> Void){
        self.log.debug("OpenAppConfigDeepLinkingUber")
        
        self.openTripInUber(clkPlaceStart: clkPlaceStart, clkPlaceEnd: clkPlaceEnd, presentingViewController:presentingViewController)
        
    }
    
    func openTripInUber(clkPlaceStart: CLKPlace?,
                                      clkPlaceEnd: CLKPlace?,
                                      presentingViewController: UIViewController)
    {
        var uberPlacePickup : UberPlace?
        var uberPlaceDropoff : UberPlace?
        
        //------------------------------------------------------------------------------------------------
        //START
        //------------------------------------------------------------------------------------------------
        if let clkPlaceStart = clkPlaceStart{
            uberPlacePickup = UberPlace.convert_CLKPlace_To_UberPlace(clkPlaceStart, uberPlaceType : .Pickup)
            
        }else{
            self.log.error("clkPlaceStart is nil")
        }
        //------------------------------------------------------------------------------------------------
        //Dropoff
        //------------------------------------------------------------------------------------------------
        
        if let clkPlaceEnd = clkPlaceEnd{
            uberPlaceDropoff = UberPlace.convert_CLKPlace_To_UberPlace(clkPlaceEnd, uberPlaceType : .Dropoff)
        }else{
            self.log.error("self.clkPlaceSearchResponse_End is nil")
        }
        
        //------------------------------------------------------------------------------------------------
        //OPEN in Uber
        //------------------------------------------------------------------------------------------------
        
        if let uberPlacePickup_ = uberPlacePickup{
            if let uberPlaceDropoff_ = uberPlaceDropoff{
                //------------------------------------------------------------------------------------------------
                self.appDeepLinkURLString = self.deepLinkingURLString(uberPlacePickup: uberPlacePickup_, uberPlaceDropoff: uberPlaceDropoff_)
               //------------------------------------------------------------------------------------------------
                if appDeepLinkURLString == ""{
                    self.log.error("error: appDeepLinkURLString is ''")
                    
                }else{
                    //-----------------------------------------------------------------------------------
                    //Deep Link url not blank
                    //-----------------------------------------------------------------------------------
                    self.canOpenInOtherApp(presentingViewController,
                                           canOpenInOtherApp_success:{() -> Void in
                                            //---------------------------------------------------------------------
                                            //App is installed - open the app using deeplinking
                                            //---------------------------------------------------------------------
                                            if let urlStringToOpen = self.urlStringToOpen {
                                                
                                                if let url = URL(string:urlStringToOpen){
                                                    UIApplication.shared.openURL(url)
                                                    
                                                }else{
                                                    self.log.error("url is nil - not valid:")
                                                    //failure(errorMessage: "appDeepLinkURLString is invalid")
                                                }
                                            }else{
                                                self.log.error("self.urlStringToOpen is nil - should be set by subclass")
                                            }
                                            //---------------------------------------------------------------
                                            //Open the app
                                            //---------------------------------------------------------------
                                            self.openAppByOpenURL()
                                            //---------------------------------------------------------------
                                            
                        },
                                           failure:{(errMsg) -> Void in
                                            self.log.debug("errMsg:\(errMsg)")
                                            
                        }
                    )
                    //-----------------------------------------------------------------------------------
                    
                }
                //-----------------------------------------------------------------------------------
            }else{
                self.log.error("uberPlaceDropoff is nil")
            }
        }else{
            self.log.error("uberPlacePickup is nil 3333")
        }
    }
    
    //------------------------------------------------------------------------------------------------
    //DEEP LINK URL
    //------------------------------------------------------------------------------------------------
    /*
     uber://?client_id=YOUR_CLIENT_ID&action=setPickup&pickup[latitude]=37.775818&pickup[longitude]=-122.418028&pickup[nickname]=UberHQ&pickup[formatted_address]=1455%20Market%20St%2C%20San%20Francisco%2C%20CA%2094103&dropoff[latitude]=37.802374&dropoff[longitude]=-122.405818&dropoff[nickname]=Coit%20Tower&dropoff[formatted_address]=1%20Telegraph%20Hill%20Blvd%2C%20San%20Francisco%2C%20CA%2094133&product_id=a1111c8c-c720-46c3-8534-2fcdd730040d
     
     uber://?client_id=YOUR_CLIENT_ID
     &action=setPickup
     &pickup[latitude]=37.775818
     &pickup[longitude]=-122.418028
     &pickup[nickname]=UberHQ
     &pickup[formatted_address]=1455 Market St, San Francisco, CA 94103    << ENCODED
     &dropoff[latitude]=37.802374
     &dropoff[longitude]=-122.405818
     &dropoff[nickname]=Coit Tower
     &dropoff[formatted_address]=1 Telegraph Hill Blvd, San Francisco, CA 94133 << ENCODED
     &product_id=a1111c8c-c720-46c3-8534-2fcdd730040d  << GET FROM WS getProductsForLocation
     */
    //------------------------------------------------------------------------------------------------
    
    
    func deepLinkingURLString(uberPlacePickup:UberPlace, uberPlaceDropoff:UberPlace) -> String{
        
        //Uber is installed - open the app
        var urlString = ""

        if self.appSchemeString == "" {
            self.log.error("appSchemeString is ''")
        }else{
            
            //==================================================================================================
            //uber://
            urlString = urlString + "\(self.appSchemeString)?client_id=\(kUBER_CLIENT_ID)"
            urlString = urlString + "&action=setPickup"
            //---------------------------------------------------------------------
            urlString = urlString + uberPlacePickup.paramsForUberPlace()
            urlString = urlString + uberPlaceDropoff.paramsForUberPlace()
            //---------------------------------------------------------------------
            urlString = urlString + "&product_id=\(kUBER_PRODUCT_ID)"
            //==================================================================================================
            
            //---------------------------------------------------------------------
            //DEBUG
            //---------------------------------------------------------------------
            self.log.debug("urlString:\(urlString)")
            //DEBUG
            let lines = urlString.components(separatedBy: "&")
            for line in lines{
                self.log.debug("\(line)")
            }
            //==================================================================================================
            
        }
        
        return urlString
    }

}
