//
//  self.swift
//  joyride
//
//  Created by Brian Clear on 27/05/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit
class OpenAppConfigClipboard: OpenAppConfigOtherApp{
    
    //used in OpenAppConfigOtherApp.open()
    override var urlStringToOpen: String?{
        //only need scheme to switch to app e.g. "lyft://"
        return self.appSchemeString
    }
    
    //------------------------------------------------------------------------------------------------
    //init
    //------------------------------------------------------------------------------------------------
    init(openWithType:OpenWithType,
         appSchemeString: String,
         appWebsiteURLString: String,
         appStoreIdUInt: UInt)
    {
        super.init(openWithType: openWithType,
                   appSchemeString: appSchemeString,
                   appWebsiteURLString: appWebsiteURLString,
                   appStoreIdUInt: appStoreIdUInt,
                   openWithSource:.OpenWithSource_Clipboard)
    }
    
    override func open(_ presentingViewController: UIViewController,
                       clkPlaceStart: CLKPlace,
                       clkPlaceEnd: CLKPlace,
                       openWithTravelMode: OpenWithTravelMode,
                       success: () -> Void,
                       failure: (_ errorMessage : String) -> Void)
    {

        
        
        //CMMMapLauncher doesnt handled app not installed so I use the one we have
        self.canOpenInOtherApp(presentingViewController,
                               canOpenInOtherApp_success:{() -> Void in
                                //---------------------------------------------------------------------
                                //App is installed - open the app using deeplinking
                                //---------------------------------------------------------------------
    
                                self.showAlertToSwapToApp(presentingViewController,
                                                          clkPlaceStart: clkPlaceStart,
                                                            clkPlaceEnd: clkPlaceEnd)
                                
                                //---------------------------------------------------------------
                                
            },
                               failure:{(errMsg) -> Void in
                                self.log.debug("errMsg:\(errMsg)")
                                
            }
        )
        
    }
    
    
    func showAlertToSwapToApp(_ presentingViewController: UIViewController,
                              clkPlaceStart: CLKPlace,
                              clkPlaceEnd: CLKPlace){
        //-----------------------------------------------------------------------------------
        let alertController = UIAlertController(title:"Switch to \(self.openWithType.name())" ,
                                                message:"Copy Start to Clipboard and switch to the app to paste it.\rSwitch back to copy the End address" , preferredStyle: UIAlertControllerStyle.actionSheet)
        //-----------------------------------------------------------------------------------
        //must have at least one action or you get an Alert you can close
        
        
        //-----------------------------------------------------------------------------------
        //START
        //-----------------------------------------------------------------------------------
        let alertActionStart = UIAlertAction(title: "Copy START and Switch to \(self.openWithType.name())", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            
            let pasterboard = UIPasteboard.general
            pasterboard.string = clkPlaceStart.name_formatted_address
            
            self.switchToClipboardApp(presentingViewController)
            
        }
        alertController.addAction(alertActionStart)
        
        //-----------------------------------------------------------------------------------
        //END
        //-----------------------------------------------------------------------------------
        let alertActionEnd = UIAlertAction(title: "Copy END and Switch to \(self.openWithType.name())", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            
            let pasterboard = UIPasteboard.general
            pasterboard.string = clkPlaceEnd.name_formatted_address
            self.switchToClipboardApp(presentingViewController)
            
        }
        alertController.addAction(alertActionEnd)
        
        //-----------------------------------------------------------------------------------
        //OPEN APP -
        //-----------------------------------------------------------------------------------
        let alertActionSwitchTo = UIAlertAction(title: "Switch to \(self.openWithType.name())", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            self.switchToClipboardApp(presentingViewController)
        }
        alertController.addAction(alertActionSwitchTo)
        
        //-----------------------------------------------------------------------------------
        //CANCEL
        //-----------------------------------------------------------------------------------
        let alertActionCancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) { (alertAction) -> Void in
            print("alertActionCancel tapped")
        }
        alertController.addAction(alertActionCancel)
        
        //-----------------------------------------------------------------------------------
        
        presentingViewController.present(alertController, animated: true, completion: nil)
        //------------------------------------------------
        //tint color too bright and alert is white
        //------------------------------------------------
        //if app tint color is bright hard to see
        //from stackoverlfow - to fix issue with tint too pale - set it AFTER you present the alert
        //alertController.view.tintColor = UIColor.blackColor()
        //gmsCircle.strokeColor = UIColor.appBaseColor()
        alertController.view.tintColor = AppearanceManager.appColorNavbarAndTabBar
        //-----------------------------------------------------------------------------------

    }
    
    func switchToClipboardApp(_ presentingViewController: UIViewController){
        
//        appDelegate.openAppURLKit.openAppNoClosures(self, presentingViewController: presentingViewController)
        
        //---------------------------------------------------------------
        //Open the app
        //---------------------------------------------------------------
        self.openAppByOpenURL()
        //---------------------------------------------------------------
    }
    
    
    
    
    
}
