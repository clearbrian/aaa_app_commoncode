//
//  OpenAppConfig.swift
//  joyride
//
//  Created by Brian Clear on 27/05/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
//--------------------------------------------------------------
// MARK: - OpenAppConfig
// MARK: -
//--------------------------------------------------------------


class OpenAppConfigDeepLinking: OpenAppConfigOtherApp{
    
    
    //------------------------------------------------
    //OPEN APP BY DEEPLINKING
    //------------------------------------------------
    //if app installed the full deeplinking url to open the app and show route
    //lyft://ridetype?id=lyft&pickup[latitude]=37.764728&pickup[longitude]=-122.422999&destination[latitude]=37.7763592&destination[longitude]=-122.4242038
    var appDeepLinkURLString: String?
    
    //-----------------------------------------------------------------------------------
    //used in OpenAppConfigOtherApp.open()
    override var urlStringToOpen: String?{
        
        return self.appDeepLinkURLString
        
    }
    
    //------------------------------------------------------------------------------------------------
    //init
    //------------------------------------------------------------------------------------------------
    init(openWithType:OpenWithType,
        appSchemeString: String,
        appDeepLinkURLString: String,               /*  if app installed the full url to open the app using deep linking */
        appWebsiteURLString: String,
        appStoreIdUInt: UInt)
    {
        super.init(openWithType: openWithType,
                   appSchemeString: appSchemeString,
                   appWebsiteURLString: appWebsiteURLString,
                   appStoreIdUInt: appStoreIdUInt,
                   openWithSource:.OpenWithSource_DeepLinking)
        
        self.appDeepLinkURLString = appDeepLinkURLString

    }
    

}

