//
//  SchipolApiParentWSRequest.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 31/03/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
class SchipolApiParentWSRequest : ParentWSRequest{
    
    override var webServiceEndpoint :String?{
        get {
            return "https://api.schiphol.nl/public-flights"
        }
    }
    
    //httpResponse.statusCode:429
    let app_id = "69674e7b"
    let app_key = "842864977d6c203283bcca199ffc9944"
    
    
    override init(){
        super.init()
        //super.query = "app_id=\(app_id)&app_key=\(app_key)"
    }
    
    
    override func parametersAppendAndCheckRequiredFieldsAreSet() -> Bool{
        
        var requiredFieldsAreSet = false
        
        let requiredFieldsAreSetInSuper = super.parametersAppendAndCheckRequiredFieldsAreSet()
        if requiredFieldsAreSetInSuper{
            
            //------------------------------------------------
            //REQUIRED
            //------------------------------------------------
            //    if let app_id = app_id{
            //        if let app_key = app_key{
            //            //------------------------------------------------
            //            // TODO: - DOESNT HANDLED blanks
            //            parameters["app_id"] = app_id as AnyObject?
            //            parameters["app_key"] = app_key as AnyObject?
            //            }
            //            requiredFieldsAreSet = true
            //            //------------------------------------------------
            //        }else{
            //            self.log.error("radius is nil")
            //        }
            //    }else{
            //        self.log.error("origin is nil")
            //    }
            
            
            parameters["app_id"] = self.app_id as AnyObject
            parameters["app_key"] = self.app_key as AnyObject
            
            if requiredFieldsAreSet{
                
                //---------------------------------------------------------------------
                //OPTIONAL PARAMETERS
                //---------------------------------------------------------------------
                //if let mode:String = mode{
                //    parameters["mode"] = mode as AnyObject?
                //}else{
                //    //self.log.debug("mode is nil - OK - optional")
                //}
                
                //---------------------------------------------------------------------
            }else{
                self.log.error("requiredFieldsAreSet is false - skip OPTIONAL FIELDS")
            }
            //------------------------------------------------
        }else{
            //required param missing in super class
            requiredFieldsAreSet = false
        }
        
        return requiredFieldsAreSet
    }
}
