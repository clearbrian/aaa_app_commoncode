//
//  ContactsFrameworkManager.swift
//  joyride
//
//  Created by Brian Clear on 15/10/2015.
//  Copyright © 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation

import Contacts
import ContactsUI
//--------------------------------------------------------------
// MARK: -
// MARK: - ContactsFrameworkManagerDelegate
//--------------------------------------------------------------
@available(iOS 9.0, *)
protocol ContactsFrameworkManagerDelegate {
    
    func pickContactAddressReturned(_ contactsFrameworkManager: ContactsFrameworkManager, contactPropertyAddress: CNContactProperty)
    
    func pickContactAddressFailed(_ contactsFrameworkManager: ContactsFrameworkManager, errorMessage: String)
    
    func contactPickerDidCancel(_ contactsFrameworkManager: ContactsFrameworkManager)
    
}


//--------------------------------------------------------------
// MARK: -
// MARK: - ContactsFrameworkManager
//--------------------------------------------------------------
@available(iOS 9.0, *)
class ContactsFrameworkManager: NSObject,  CNContactPickerDelegate{
    let log = MyXCodeEmojiLogger.defaultInstance
    var delegate: ContactsFrameworkManagerDelegate?
    
    let contactAuthorizer = ContactAuthorizer()
    
    static let kFormattedAddressKey = "kFormattedAddressKey"
    
    static let kPersonName = "kPersonName"
    static let kOrgName = "kOrgName"
    
    func pickContactAddress(presentingViewController : UIViewController){
        
        
        contactAuthorizer.authorizeContactsWithCompletionHandler{succeeded, errorMessage in
            
            if succeeded{
                //------------------------------------------------------------------------------------------------
                //CONTACT ACCESS AUTHORIZED - Show Picker
                //------------------------------------------------------------------------------------------------

                let contactPicker = CNContactPickerViewController()
                contactPicker.delegate = self
                
                //We only want to show the Addresses for users to pick
                contactPicker.displayedPropertyKeys = [CNContactPostalAddressesKey]
                
        
                //contactPicker.predicateForSelectionOfContact = [NSPredicate predicateWithFormat:"%K.@count > 1", ABPersonPhoneNumbersProperty];
                
                //contactPicker.predicateForSelectionOfContact = NSPredicate(format: "%K.@count > 1", ABPersonPostalAddressesProperty)
                contactPicker.predicateForEnablingContact = NSPredicate(format: "postalAddresses.@count > 0")
                //contactPicker.predicateForSelectionOfContact = NSPredicate(format: "postalAddresses.@count > 1")
                
                presentingViewController.present(contactPicker, animated: true, completion: nil)
                
            } else {
                //------------------------------------------------------------------------------------------------
                //CONTACT ACCESS NOT AUTHORIZED
                //------------------------------------------------------------------------------------------------
                
                if let errorMessage = errorMessage{
                    self.failedWithErrorMessage(errorMessage)
                    
                }else{
                    self.log.error("errorMessage is nil but succeeded is false")
                    self.failedWithErrorMessage("AUTH FAILED - UNKNOWN ERROR")
                }
            }
        }
    }
    
    func failedWithErrorMessage(_ errorMessage: String){
        
        if let delegate = self.delegate{
            
            //------------------------------------------------
            //errorIN may be nil
            delegate.pickContactAddressFailed(self, errorMessage: errorMessage)
            //------------------------------------------------
            
        }else{
            self.log.error("delegate is nil")
        }
    }
    
    
    
    
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - CNContactPickerDelegate
    //--------------------------------------------------------------
    //Invoked when the picker is closed.
    func contactPickerDidCancel(_ picker: CNContactPickerViewController){
        self.log.debug("contactPickerDidCancel:")
        if let delegate = self.delegate{
            
            //------------------------------------------------
            //errorIN may be nil
            delegate.contactPickerDidCancel(self)
            //------------------------------------------------
            
        }else{
            self.log.error("delegate is nil")
        }
        
    }
    
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - CONTACT
    //--------------------------------------------------------------
    //If this and didSelectContactProperty: commented in then didSelectContactProperty never called
    // TODO: - CLEANUP after test
        //    func contactPicker(picker: CNContactPickerViewController, didSelectContact contact: CNContact){
        //        print("didSelectContact:\(contact)")
        //        
        //        //        didSelectContact:<CNContact: 0x7fcd63e4d590: identifier=18521153-488A-4C46-AA6D-E1AE80BA60C4, givenName=John, familyName=Appleseed, organizationName=, phoneNumbers=(
        //        //        "<CNLabeledValue: 0x7fcd63c1b940: identifier=D8DBBEDC-6DDC-4F67-A44E-FF81FD2BA683, label=_$!<Mobile>!$_, value=<CNPhoneNumber: 0x7fcd63c24590: countryCode=us, digits=8885555512>>",
        //        //        "<CNLabeledValue: 0x7fcd63c34f10: identifier=1C3BE915-6584-4DB3-B5BD-128F15AB2ACF, label=_$!<Home>!$_, value=<CNPhoneNumber: 0x7fcd63c2aa20: countryCode=us, digits=8885551212>>"
        //        //        ), emailAddresses=(
        //        //        "<CNLabeledValue: 0x7fcd65c36290: identifier=5B2BFEF1-9A8D-43B5-9848-B9CD2A6AAEC5, label=_$!<Work>!$_, value=John-Appleseed@mac.com>"
        //        //        ), postalAddresses=(
        //        //        "<CNLabeledValue: 0x7fcd63c36970: identifier=F1DC1742-6A61-433F-9B83-2C39661ADFBC, label=_$!<Work>!$_, value=<CNPostalAddress: 0x7fcd63c36470: street=3494 Kuhl Avenue, city=Atlanta, state=GA, postalCode=30303, country=USA, countryCode=ca, formattedAddress=(null)>>",
        //        //        "<CNLabeledValue: 0x7fcd63c1b2e0: identifier=0918DABF-6E33-4B3F-A114-095C41908DD0, label=_$!<Home>!$_, value=<CNPostalAddress: 0x7fcd63c1b470: street=1234 Laurel Street, city=Atlanta, state=GA, postalCode=30303, country=USA, countryCode=us, formattedAddress=(null)>>"
        //        //        )>
//    }
    
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - CONTACT PROPERTY
    //--------------------------------------------------------------
    //If didSelectContact: is also commented in then this wont be called
    
    //IF YOU DONT IMPLEMENT THIS or didSelectContact: IT WILL OPEN THE ADDRESS IN MAPS
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contactProperty: CNContactProperty){

        //---------------------------------------------------------------------
        //v2
        //---------------------------------------------------------------------
        print("--------- didSelectContactProperty: ")
        print("contactProperty:\(contactProperty)")
        
        
        if contactProperty.key == CNContactPostalAddressesKey{
            if let delegate = self.delegate {
                delegate.pickContactAddressReturned(self, contactPropertyAddress: contactProperty)
            }else{
                appDelegate.log.error("self.delegate is nil")
            }
        }else{
            self.log.error("UNHANDLED CNContactProperty:\r \(contactProperty)")
        }
    }
    
    // TODO: - ok can only pick one contact address
    //    /*!
    //    * @abstract Plural delegate methods.
    //    * @discussion These delegate methods will be invoked when the user is done selecting multiple contacts or properties.
    //    * Implementing one of these methods will configure the picker for multi-selection.
    //    */
    //    optional public func contactPicker(picker: CNContactPickerViewController, didSelectContacts contacts: [CNContact]){
    //        
    //    }
    //    optional public func contactPicker(picker: CNContactPickerViewController, didSelectContactProperties contactProperties: [CNContactProperty]){
    //        
    //    }
    
    
}
