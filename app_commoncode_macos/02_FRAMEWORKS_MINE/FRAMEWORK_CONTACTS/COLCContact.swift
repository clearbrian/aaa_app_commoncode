//
//  COLCContact.swift
//  joyride
//
//  Created by Brian Clear on 07/02/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import Contacts
import CoreLocation
class COLCContact{
    
    let log = MyXCodeEmojiLogger.defaultInstance
    
    fileprivate var contactPropertyAddress: CNContactProperty
    
    init(contactPropertyAddress: CNContactProperty){
        self.contactPropertyAddress = contactPropertyAddress
        
    }
    
    var addressDictionary: [AnyHashable : Any]?{
     
        var addressDictionary_:[AnyHashable : Any]? = [:]
        
        //------------------------------------------------------------------------------------------------
        //CNContactPostalAddressesKey  self.contactPropertyAddress.value >> <CNPostalAddress
        //------------------------------------------------------------------------------------------------
        if let cnPostalAddress = contactPropertyAddress.value as? CNPostalAddress{

            //---------------------------------------------------------------------
            //CoreLocation - CLGeocoder() requires dictionary in Address Book format - Contacts framework used after iOS9 as AB deprecated
            //open func geocodeAddressDictionary(_ addressDictionary: [AnyHashable : Any]
            //---------------------------------------------------------------------
            //<CNPostalAddress: 0x7f9bd1411020: street=3494 Kuhl Avenue, city=Atlanta, state=GA, postalCode=30303, country=USA, countryCode=ca, formattedAddress=(null)>)
            
            //<CNContactProperty: 0x170028740: contact identifier=48638D59-A354-4FA9-A06A-272EBCF71DED, contact name=Clarkson Shipping Agency, Egypt, 
            //key=postalAddresses, identifier=C3FBB8A7-9FDC-4108-8481-29CA48F8CF5B, 
            //value=<CNPostalAddress: 0x171a71f00: street=31 Ahmed AbdelAziz St, Kasr el Ahlam bld, Apart 305, KafrAbdou, city=Alexandria, state=, postalCode=, country=Egypt, countryCode=, formattedAddress=(null)>>
            //----------------------------------------------------------------------------------------
            //CITY ONLY - will return address but then lat/lng converted to point in middle of leeds
            //<CNContactProperty: 0x174638880: contact identifier=CFC5FD26-F6C8-4988-AF46-D047222D7BBB, contact name=CLK - City Only, key=postalAddresses, identifier=016F33F3-009A-4E76-AAF6-8F3D7EA23E9F, value=<CNPostalAddress: 0x175a7e240: street=, city=Leeds, state=, postalCode=, country=United Kingdom, countryCode=, formattedAddress=(null)>>
            //---------------------------------------------------------------------
            //    self.log.debug("cnPostalAddress.street:\(cnPostalAddress.street)")
            //    self.log.debug("cnPostalAddress.city:\(cnPostalAddress.city)")
            //    self.log.debug("cnPostalAddress.state:\(cnPostalAddress.state)")
            //    self.log.debug("cnPostalAddress.postalCode:\(cnPostalAddress.postalCode)")
            //    self.log.debug("cnPostalAddress.country:\(cnPostalAddress.country)")
            //    self.log.debug("cnPostalAddress.ISOCountryCode:\(cnPostalAddress.isoCountryCode)")
            
            
            //---------------------------------------------------------------------
            //Note cnPostalAddress.formattedAddress seems to be always null - formattedAddress=(null)
            //to use it use formatter - THIS IS FOR DISPLAY - this method requires an  >> Apple CLGeocoder addressDictionary
              let formatter = CNPostalAddressFormatter()
              let formattedAddress = formatter.string(from: cnPostalAddress)
              print("CNPostalAddressFormatter: address is \(formattedAddress)")
            //  //    The address is 3494 Kuhl Avenue
            //  //    Atlanta GA 30303
            //  //    USA
            //---------------------------------------------------------------------

            addressDictionary_?[CNPostalAddressStreetKey] = cnPostalAddress.street
            addressDictionary_?[CNPostalAddressCityKey] = cnPostalAddress.city
            addressDictionary_?[CNPostalAddressStateKey] = cnPostalAddress.state
            addressDictionary_?[CNPostalAddressPostalCodeKey] = cnPostalAddress.postalCode
            addressDictionary_?[CNPostalAddressCountryKey] = cnPostalAddress.country
            addressDictionary_?[CNPostalAddressISOCountryCodeKey] = cnPostalAddress.isoCountryCode
            
            
            self.log.debug("addressDictionary_:\(String(describing: addressDictionary_))")
            
        }else{
            self.log.error("self.contactPropertyAddress.value is nil or not CNPostalAddress")
        }
        return addressDictionary_
    }
    
    
    
    /*
    ["city": "Atlanta", "ISOCountryCode": "ca", "country": "USA", "street": "3494 Kuhl Avenue", "postalCode": "30303", "kFormattedAddressKey": "3494 Kuhl Avenue\nAtlanta GA 30303\nUSA", "kPersonOrOrgName": "John Appleseed", "state": "GA"]
    */
    
    
    //Returned when you geocode the dictionaryAddress
    //doesnt rwturn ALL the address street seems to be missing so need to create this class to storee the dict and the lat/lng together
    var clPlacemark : CLPlacemark?
    
    var formattedAddress : String{
        let formattedAddress_ = "ERROR formattedAddress 45345345"
//        if let dictionaryAddress = self.dictionaryAddress{
//            if let formattedAddressInArray = dictionaryAddress[ContactsFrameworkManager.kFormattedAddressKey]{
//                
//                //formattedAddress_ = formattedAddressInArray
//                formattedAddress_ = formattedAddressInArray.replacingOccurrences(of: "\n", with:", ")
//   
//            }else{
//                print("ERROR: kFormattedAddressKey returned nil - 965767")
//            }
//        }else{
//            print("ERROR: dictionaryAddress is nil")
//        }
        return formattedAddress_;
    }
    
    
    //note name_formatted address has different rule it only shows kOrgName it its set - doesnt show kPersonName
    //but here we show kPersonName or kOrgName
    //because if you tap on a Contact business you want to be picked up at an office
    //but if you tap on a person you only want to send the addres to the person
  
    var name :String? {
        //---------------------------------------------------------
        var name_ :String?
        
        if self.contactPropertyAddress.contact.givenName == "" {

            if self.contactPropertyAddress.contact.familyName == "" {
                self.log.debug("givenName is empty")
                self.log.debug("familyName is empty")
                self.log.debug("givenName/familyName are empty - maybe Org only - return nil for name")
                name_ = nil
                
            }else{
                self.log.debug("givenName is empty")
                self.log.debug("familyName:\(self.contactPropertyAddress.contact.familyName)")
                name_ = "\(self.contactPropertyAddress.contact.familyName)"
            }
            
        }else{

            self.log.debug("givenName:\(self.contactPropertyAddress.contact.givenName)")
            
            if self.contactPropertyAddress.contact.familyName == "" {
                self.log.debug("familyName is empty")
                name_ = "\(self.contactPropertyAddress.contact.givenName)"
                
            }else{
                //both set
                name_ = "\(self.contactPropertyAddress.contact.givenName) \(self.contactPropertyAddress.contact.familyName)"
                
            }
        }
        return name_
    }
    
    //use nil to tell if org not set
    var orgName: String?{
        
        //---------------------------------------------------------------------
        //ORGANISATION name
        //---------------------------------------------------------------------
        //CNPostalAddress parent is CNContact
        //never nil - set or ""
        
        if self.contactPropertyAddress.contact.organizationName == "" {
            return nil
            
        }else{
            return self.contactPropertyAddress.contact.organizationName
            
        }
        //---------------------------------------------------------------------
    }
    
    var isWorkAddress:Bool{
        var isWorkAddressReturned = false
        if let label = self.contactPropertyAddress.label{

            if label == CNLabelWork{
                self.log.debug("AddressType: WORK")
                isWorkAddressReturned = true
            }
            else if label == CNLabelHome{
                self.log.debug("AddressType: HOME")
            }
            else if label == CNLabelOther{
                self.log.debug("AddressType: OTHER")
            }else{
                self.log.debug("CUSTOM Address Label:[\(label)]")
            }
        }else{
            self.log.error("self.contactPropertyAddress.label is nil")
        }
        return isWorkAddressReturned
    }
    

//    //contacts - only show name(orgname) if its a business
//    var name_for_contact :String {
//        //If GiveName/FamilyName set - dont show
//        
//        if you tap on PERSON / WORK address then check if ORG set and include it - may need to ask user
//        
//        
//        
//        
////        get {
////            //---------------------------------------------------------
////            var name_for_contact_returned :String = ""
////            //---------------------------------------------------------------------
//////            if let dictionaryAddress = dictionaryAddress{
//////                //Graham has personal name AND ORG HMRC but we only want address
//////                
//////                if let personName = dictionaryAddress[ContactsFrameworkManager.kPersonName]{
//////                    if let orgName = dictionaryAddress[ContactsFrameworkManager.kOrgName]{
//////                        name_for_contact_returned = "\(personName) - \(orgName)"
//////                        
//////                    }
//////                    else{
//////                        //org name is not set - personal name maybe but dont show it
//////                        name_for_contact_returned = "\(personName)"
//////                    }
//////                    
//////                }else{
//////                    //person name not set - org is e.g.  CLARKSONS OFFICE
//////                    if let orgName = dictionaryAddress[ContactsFrameworkManager.kOrgName]{
//////                        name_for_contact_returned = orgName
//////                    }
//////                    else{
//////                        //personn/ org name is not set
//////                        // TODO: - do i return nil and then use first line of address
//////                        name_for_contact_returned = ""
//////                    }
//////                }
//////                
//////                //let street = dictionaryAddress["street"]
//////                
//////                
//////                
//////            }else{
//////                self.log.error("dictionaryAddress is nil - 2453")
//////            }
////            
////            //------------------------------------------------------------------------------------------------
////            return name_for_contact_returned
////            //------------------------------------------------------------------------------------------------
////        }
////    }

   
    
    // TODO: - what if location turned off
    var locality :String?
    //was causing infinite loop
//    {
//        //---------------------------------------------------------
//        var locality_ :String?
//
//        if let locality = self.locality{
//            locality_ = locality
//        }else{
//            self.log.error("dictionaryAddress is nil - 9090")
//        }
//        return locality_
//        //----------------------------------------------------------
//    }

    var clLocation : CLLocation?{
        var clLocation :CLLocation?
        if let clPlacemark = clPlacemark{
            if let locationInPlacemark = clPlacemark.location{
                clLocation = locationInPlacemark
            }else{
                self.log.error("clPlacemark.location is nil")
            }            
        }else{
            self.log.error("dictionaryAddress is nil - 3333")
        }
        return clLocation
    }
}



//---------------------------------------------------------------------
//---------------------------------------------------------------------



//        print("--------- didSelectContactProperty: ")
//        print("contactProperty:\(contactProperty)")


//        var dictionaryContactAndAddressReturned = [String: String]()



//        print("       self.contactPropertyAddress.key:\(self.contactPropertyAddress.key)")
//        print("self.contactPropertyAddress.identifier:\(self.contactPropertyAddress.identifier)")
//        print("     self.contactPropertyAddress.label:\(self.contactPropertyAddress.label)")
//        print("     self.contactPropertyAddress.value:\(self.contactPropertyAddress.value)")
//
//        /*
//         didSelectContactProperty:
//         <CNContactProperty: 0x174e25460:
//            contact identifier=D9C17BFD-3920-40B3-BD72-6D1615A1CA79,
//            contact name=Brian Clear,
//            key=postalAddresses, identifier=9DA8C700-BB74-40ED-873D-6520A2ABD2CB,
//            value=<CNPostalAddress: 0x17687ac00: street=15 Royal Mint Street, city=London, state=, postalCode=E1 8LG, country=United Kingdom, countryCode=, formattedAddress=(null)>>
//
//         */
//
//
//        print("---------")
//        //------------------------------------------------------------------------------------------------
//
//
//        //------------------------------------------------------------------------------------------------
//        //PARENT  CONTACT -  attached to the property
//        //------------------------------------------------------------------------------------------------
//        //CNContactProperty.contact >> CNContact
//        print("CNContactProperty.contact >> CNContact:\r\(self.contactPropertyAddress.contact)")
//        //---------------------------------------------------------------------
//        //PARENT CONTACT of the address picked
//        //self.contactPropertyAddress.contact >> CNContact
//        //---------------------------------------------------------------------
//        /*
//
//         CNContactProperty.contact >> CNContact:
//         <CNContact: 0x1148a6150:
//             identifier=D9C17BFD-3920-40B3-BD72-6D1615A1CA79,
//             givenName=Brian,
//             familyName=Clear,
//             organizationName=,
//             phoneNumbers=(
//                "<CNLabeledValue: 0x17146e2c0:
//                    identifier=FE900BD9-3A0A-491E-9F2C-06182E58A064,
//                    label=_$!<Mobile>!$_,
//                    value=<CNPhoneNumber: 0x17123e240: countryCode=gb,digits=07787791179>>",
//
//                "<CNLabeledValue: 0x176c75140: identifier=211A7620-8A06-4796-8886-93884A6829E1,
//                    label=Android,
//                    value=<CNPhoneNumber: 0x174e21600: countryCode=gb, digits=07762189011>>"
//             ),
//             emailAddresses=(
//                "<CNLabeledValue: 0x17127c100: identifier=C725A4BD-FB63-4654-8994-B86E2240BA1E,
//                    label=(null),
//                    value=clearbrian@googlemail.com>"
//             ),
//             postalAddresses=(
//                    "<CNLabeledValue: 0x176877900: identifier=9DA8C700-BB74-40ED-873D-6520A2ABD2CB,
//                        label=_$!<Home>!$_,
//                        value=<CNPostalAddress: 0x17687ac00:
//                                        street=15 Royal Mint Street,
//                                        city=London,
//                                        state=,
//                                        postalCode=E1 8LG,
//                                        country=United Kingdom,
//                                        countryCode=,
//                                        formattedAddress=(null)>>",
//                    "<CNLabeledValue: 0x176c78000:
//                        identifier=C1BA2E92-3DC8-4EAA-9B2B-8FE0628478E5,
//                        label=_$!<Work>!$_,
//                        value=<CNPostalAddress: 0x1768790c0:
//                                        street=Commodity Quay\nSt. Katherine Dock,
//                                        city=,
//                                        state=,
//                                        postalCode=E1W 1BF,
//                                        country=United Kingdom,
//                                        countryCode=,
//                                        formattedAddress=(null)>>"
//            )>
//         */
//
//
//        //------------------------------------------------------------------------------------------------
//        //ORGNAME or FIRST/GIVEN Name
//        //------------------------------------------------------------------------------------------------
//
//        print("CNContactProperty.contact.givenName >> CNContact:\r\(self.contactPropertyAddress.contact.givenName)")
//        print("CNContactProperty.contact.familyName >> CNContact:\r\(self.contactPropertyAddress.contact.familyName)")
//        print("CNContactProperty.contact.organizationName >> CNContact:\r\(self.contactPropertyAddress.contact.organizationName)")
//
//
//
//        //---------------------------------------------------------------------
//        //First Name/ Given name
//        //con contacts if you pick person you only want address
//        //
//        //---------------------------------------------------------------------
//        var personNameReturned  = ""
//
//        if self.contactPropertyAddress.contact.givenName == "" {
//            self.log.debug("givenName is empty")
//        }else{
//
//            personNameReturned = personNameReturned + "\(self.contactPropertyAddress.contact.givenName) "
//        }
//        //---------------------------------------------------------------------
//        if self.contactPropertyAddress.contact.familyName == "" {
//            self.log.debug("familyName is empty")
//        }else{
//            //append so can handle blank givenName
//            personNameReturned = personNameReturned + "\(self.contactPropertyAddress.contact.familyName) " //space at end removed by trim() on next line
//        }
//        personNameReturned = personNameReturned.trim()
//
//        dictionaryContactAndAddressReturned[ContactsFrameworkManager.kPersonName] = personNameReturned
//
//
//        //---------------------------------------------------------------------
//        //ORGANISATION name
//        //---------------------------------------------------------------------
//        var orgNameReturned  = ""
//        //---------------------------------------------------------------------
//        if self.contactPropertyAddress.contact.organizationName == "" {
//            self.log.debug("organizationName is empty")
//        }else{
//            orgNameReturned =  "\(self.contactPropertyAddress.contact.organizationName)"
//
//        }
//        //---------------------------------------------------------------------
//
//        dictionaryContactAndAddressReturned[ContactsFrameworkManager.kOrgName] = orgNameReturned
//        //------------------------------------------------------------------------------------------------
//        //------------------------------------------------------------------------------------------------
//
//
//        /*
//        CNContactProperty.contact >> CNContact:
//        <CNContact: 0x7fc28b528880:
//        identifier=18521153-488A-4C46-AA6D-E1AE80BA60C4,
//
//        givenName=John,
//
//        familyName=Appleseed,
//
//        organizationName=,
//
//        phoneNumbers=(
//        "<CNLabeledValue: 0x7fc28b555be0: identifier=D8DBBEDC-6DDC-4F67-A44E-FF81FD2BA683,
//        label=_$!<Mobile>!$_,
//        value=<CNPhoneNumber: 0x7fc28b5571b0: countryCode=us,
//        digits=8885555512>>",
//        "<CNLabeledValue: 0x7fc28b559180: identifier=1C3BE915-6584-4DB3-B5BD-128F15AB2ACF,
//        label=_$!<Home>!$_,
//        value=<CNPhoneNumber: 0x7fc28b557620: countryCode=us,
//        digits=8885551212>>"
//        ),
//
//
//        emailAddresses=(
//
//        "<CNLabeledValue: 0x7fc28b5551c0: identifier=5B2BFEF1-9A8D-43B5-9848-B9CD2A6AAEC5,
//        label=_$!<Work>!$_,
//        value=John-Appleseed@mac.com>"
//
//        ),
//
//        postalAddresses=(
//
//
//        "<CNLabeledValue: 0x7fc28b55a3d0:
//        identifier=F1DC1742-6A61-433F-9B83-2C39661ADFBC,
//        label=_$!<Work>!$_,
//        value=<CNPostalAddress: 0x7fc28b55a670: street=3494 Kuhl Avenue,
//        city=Atlanta,
//        state=GA,
//        postalCode=30303,
//        country=USA,
//        countryCode=ca,
//        formattedAddress=(null)>>",
//
//
//        "<CNLabeledValue: 0x7fc28b55a7c0:
//        identifier=0918DABF-6E33-4B3F-A114-095C41908DD0,
//        label=_$!<Home>!$_,
//        value=<CNPostalAddress: 0x7fc28b55a860: street=1234 Laurel Street,
//        city=Atlanta,
//        state=GA,
//        postalCode=30303,
//        country=USA,
//        countryCode=us,
//        formattedAddress=(null)>>"
//
//        )>
//
//        */
//
//
//        //---------------------------------------------------------------------
//        //CONTACT PROPERTY
//        //---------------------------------------------------------------------
//        //Values wil change depenging on contact property tapped on - so you need to check the key
//        //---------------------------------------------------------------------
//        print("self.contactPropertyAddress.key:\(self.contactPropertyAddress.key)")
//        print("self.contactPropertyAddress.value:\(self.contactPropertyAddress.value)")
//        print("self.contactPropertyAddress.identifier:\(self.contactPropertyAddress.identifier)")
//        print("self.contactPropertyAddress.label:\(self.contactPropertyAddress.label)")
//        //---------------------------------------------------------------------
//        //---------------------------------------------------------------------
//        //CNContactProperty
//        //---------------------------------------------------------------------
//        //Compare this to constants to see which property this is
//        //public var key: String { get }
//
//        //* @abstract The value of the property.
//        //public var value: AnyObject? { get }
//
//        //* @abstract The identifier of the labeled value if the property is an array of labeled values, otherwise is nil.
//        //*/
//        //public var identifier: String? { get }
//
//        //* @abstract The label of the labeled value if the property is an array of labeled values, otherwise is nil.
//        //*/
//        //public var label: String? { get }
//
//
//
//
//
//
//
//        //------------------------------------------------------------------------------------------------
//        //CONTACT PHOTO
//        //------------------------------------------------------------------------------------------------
//
////        public let CNContactImageDataKey: String
////        public let CNContactThumbnailImageDataKey: String
////        public let CNContactImageDataAvailableKey: String
//
//
//        if self.contactPropertyAddress.contact.isKeyAvailable(CNContactThumbnailImageDataKey){
//
//            //try contact = CNContactStore().unifiedContactWithIdentifier(contact.identifier, keysToFetch: [CNContactThumbnailImageDataKey])
//
//
//            if self.contactPropertyAddress.contact.imageDataAvailable{
//                //------------------------------------------------------------------------------------------------
//                //imageData
//                //------------------------------------------------------------------------------------------------
//
//                if let imageData : Data = self.contactPropertyAddress.contact.imageData{
//
//                    if let imageData = UIImage(data: imageData){
//                        print("imageData:\(imageData.size)")
//                        //imageData:(1668.0, 2500.0)
//                    }else{
//                        self.log.error("imageData nil")
//                    }
//
//                }else{
//                    self.log.error("self.contactPropertyAddress.contact.thumbnailImageData is nil")
//                }
//
//                //------------------------------------------------------------------------------------------------
//                //THUMBNAIL
//                //------------------------------------------------------------------------------------------------
//
//                if let thumbnailImageData : Data = self.contactPropertyAddress.contact.thumbnailImageData{
//
//                    if let thumbnailImageData = UIImage(data: thumbnailImageData){
//                        print("thumbnailImageData:\(thumbnailImageData.size)")
//                        //thumbnailImageData:(345.0, 345.0)
//                    }else{
//                        self.log.error("thumbnailImageData: UIImage(data: thumbnailImageData) nil")
//                    }
//
//                }else{
//                    self.log.error("self.contactPropertyAddress.contact.thumbnailImageData is nil")
//                }
//            }else{
//                self.log.error("self.contactPropertyAddress.contact.imageDataAvailable is false")
//            }
//        }
//
//
//
//
//
//
//        //------------------------------------------------------------------------------------------------
//        //ADDRESS
//        //------------------------------------------------------------------------------------------------
//
//        //IF address is tapped on
//        //self.contactPropertyAddress.key:postalAddresses
//        //This is same as the constant
//        print("CNContactPostalAddressesKey:\(CNContactPostalAddressesKey)")
//
//
//        if self.contactPropertyAddress.key == CNContactPostalAddressesKey{
//            //IF address is tapped on
//            //self.contactPropertyAddress.key:postalAddresses
//
//            //------------------------------------------------------------------------------------------------
//            //TITLE
//            //------------------------------------------------------------------------------------------------
//            //---------------------------------------------------------------------
//            print("self.contactPropertyAddress.label:\(self.contactPropertyAddress.label)")
//
//            if let label = self.contactPropertyAddress.label{
//
//                if label == CNLabelWork{
//                    self.log.debug("AddressType: WORK")
//                }
//                else if label == CNLabelHome{
//                    self.log.debug("AddressType: HOME")
//                }
//                else if label == CNLabelOther{
//                    self.log.debug("AddressType: OTHER")
//                }else{
//                    self.log.debug("CUSTOM Address Label:[\(label)]")
//                }
//            }else{
//                self.log.error("self.contactPropertyAddress.label is nil")
//            }
//
//            //------------------------------------------------------------------------------------------------
//            //CNContactPostalAddressesKey  self.contactPropertyAddress.value >> <CNPostalAddress
//            //------------------------------------------------------------------------------------------------
//            if let cnPostalAddress = self.contactPropertyAddress.value as? CNPostalAddress{
//                //        self.contactPropertyAddress.value:Optional(<CNPostalAddress: 0x7f9bd1411020: street=3494 Kuhl Avenue, city=Atlanta, state=GA, postalCode=30303, country=USA, countryCode=ca, formattedAddress=(null)>)
//
//                self.log.debug("cnPostalAddress.street:\(cnPostalAddress.street)")
//                self.log.debug("cnPostalAddress.city:\(cnPostalAddress.city)")
//                self.log.debug("cnPostalAddress.state:\(cnPostalAddress.state)")
//                self.log.debug("cnPostalAddress.postalCode:\(cnPostalAddress.postalCode)")
//                self.log.debug("cnPostalAddress.country:\(cnPostalAddress.country)")
//                self.log.debug("cnPostalAddress.ISOCountryCode:\(cnPostalAddress.isoCountryCode)")
//
//
//                let formatter = CNPostalAddressFormatter()
//                var formattedAddress = formatter.string(from: cnPostalAddress)
//                print("The address is \(formattedAddress)")
//                //    The address is 3494 Kuhl Avenue
//                //    Atlanta GA 30303
//                //    USA
//
//
//
//
//
//                // TODO: - Contact personal
//                // TODO: - Contact personal/org name
//                // TODO: - Contact business/org name
//                //do we show in address?
//                // TODO: - dont return dict = return a CLKContact
//
//
//
//
//
//
//                //this is only what were looking for
//                //kFormattedAddressKey
//                //["city": "London", "ISOCountryCode": "", "country": "United Kingdom", "street": "15 Royal Mint Street", "postalCode": "E1 8LG", "kFormattedAddressKey": "15 Royal Mint Street\nLondon\n\nE1 8LG\nUnited Kingdom", "state": ""]
//                formattedAddress = formattedAddress.replacingOccurrences(of: "\n\n", with: "\n")
//
//                dictionaryContactAndAddressReturned[ContactsFrameworkManager.kFormattedAddressKey] = formattedAddress
//
//                //---------------------------------------------------------------------
//
//                dictionaryContactAndAddressReturned[CNPostalAddressStreetKey] = cnPostalAddress.street
//                dictionaryContactAndAddressReturned[CNPostalAddressCityKey] = cnPostalAddress.city
//                dictionaryContactAndAddressReturned[CNPostalAddressStateKey] = cnPostalAddress.state
//                dictionaryContactAndAddressReturned[CNPostalAddressPostalCodeKey] = cnPostalAddress.postalCode
//                dictionaryContactAndAddressReturned[CNPostalAddressCountryKey] = cnPostalAddress.country
//                dictionaryContactAndAddressReturned[CNPostalAddressISOCountryCodeKey] = cnPostalAddress.isoCountryCode
//
//
//                //return in delegate
//                self.pickContactAddressReturnedWithAddress(dictionaryContactAndAddressReturned)
//
//
////                @available(iOS 9.0, *)
////                public let CNPostalAddressStreetKey: String
////                @available(iOS 9.0, *)
////                public let CNPostalAddressCityKey: String
////                @available(iOS 9.0, *)
////                public let CNPostalAddressStateKey: String
////                @available(iOS 9.0, *)
////                public let CNPostalAddressPostalCodeKey: String
////                @available(iOS 9.0, *)
////                public let CNPostalAddressCountryKey: String
////                @available(iOS 9.0, *)
////                public let CNPostalAddressISOCountryCodeKey: String
//
//
//
//
////                @available(iOS, introduced=2.0, deprecated=9.0, message="use CNContact.postalAddresses")
////                public let kABPersonAddressProperty: ABPropertyID // Street address - kABMultiDictionaryPropertyType
////                @available(iOS, introduced=2.0, deprecated=9.0, message="use CNPostalAddress.street")
////                public let kABPersonAddressStreetKey: CFString!
////                @available(iOS, introduced=2.0, deprecated=9.0, message="use CNPostalAddress.city")
////                public let kABPersonAddressCityKey: CFString!
////                @available(iOS, introduced=2.0, deprecated=9.0, message="use CNPostalAddress.state")
////                public let kABPersonAddressStateKey: CFString!
////                @available(iOS, introduced=2.0, deprecated=9.0, message="use CNPostalAddress.postalCode")
////                public let kABPersonAddressZIPKey: CFString!
////                @available(iOS, introduced=2.0, deprecated=9.0, message="use CNPostalAddress.country")
////                public let kABPersonAddressCountryKey: CFString!
////                @available(iOS, introduced=2.0, deprecated=9.0, message="use CNPostalAddress.ISOCountryCode")
//
//            }else{
//                self.log.error("self.contactPropertyAddress.value is nil or not CNPostalAddress")
//            }
//            //------------------------------------------------------------------------------------------------
//            if let identifier = self.contactPropertyAddress.identifier{
//                self.log.debug("cnPostalAddress.identifier:\(identifier)")
//
//            }else{
//                self.log.error("self.contactPropertyAddress.identifier is nil")
//            }
//            //------------------------------------------------------------------------------------------------
//
//        }else{
//            self.log.error("UNHANDLED CNContactProperty:\r \(contactProperty)")
//        }
