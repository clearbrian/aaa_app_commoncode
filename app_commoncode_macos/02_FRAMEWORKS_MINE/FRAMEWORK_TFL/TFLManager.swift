//
//  TFLManager.swift
//  joyride
//
//  Created by Brian Clear on 01/12/2015.
//  Copyright © 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation
//import UIKit
import SafariServices


class TFLManager {
    let log = MyXCodeEmojiLogger.defaultInstance
    //removed COLCTripPlannerViewController from project clashing with TripPlannerViewController
//    func openTripInSafari(clkTrip : CLKTrip, colcTripPlannerViewController: COLCTripPlannerViewController){
//        
//        let tflTrip = TFLTrip()
//        // TODO: - clkTrip to TFLTrip
//        if let urlString = tflTrip.urlString {
//            
//            let urlwithPercentEscapes = urlString.stringByAddingPercentEncodingWithAllowedCharacters( NSCharacterSet.URLQueryAllowedCharacterSet())
//            
//            if let urlStringwithPercentEscapes = urlwithPercentEscapes{
//                if let url = NSURL(string: urlStringwithPercentEscapes) {
//                    
//                    let safariViewController = SFSafariViewController(URL: url)
//                    
//                    safariViewController.delegate = colcTripPlannerViewController
//                    colcTripPlannerViewController.presentViewController(safariViewController, animated: true, completion: nil)
//                }else{
//                    self.log.error("url is nil for urlString:\r\(urlString)\r\(urlStringwithPercentEscapes)")
//                }
//            }else{
//                self.log.error("urlwithPercentEscapes is nil")
//            }
//        }else{
//            self.log.error("tflTrip.urlString is nil")
//        }
//    }
//    func openTripInSafari(clkTrip : CLKTrip, tripPlannerViewController: TripPlannerViewController){
//        
//        let tflTrip = TFLTrip()
//        // TODO: - clkTrip to TFLTrip
//        if let urlString = tflTrip.urlString {
//            
//            let urlwithPercentEscapes = urlString.stringByAddingPercentEncodingWithAllowedCharacters( NSCharacterSet.URLQueryAllowedCharacterSet())
//            
//            if let urlStringwithPercentEscapes = urlwithPercentEscapes{
//                if let url = NSURL(string: urlStringwithPercentEscapes) {
//                    
//
//
//                    //SafariController button use app tint but has white border so can be hard to see
//                    // AppearanceManager.appColorNavbarAndTabBar
//                    // AppearanceManager.appColorTint
//                    //appDelegate.window?.tintColor = UIColor.redColor()
//                    
//                    appDelegate.window?.tintColor = AppearanceManager.appColorNavbarAndTabBar
//                    //... change back in  afariViewController: safariViewControllerDidFini
//                    
//                    let safariViewController = SFSafariViewController(URL: url)
//                    
//                    safariViewController.delegate = tripPlannerViewController
//                    tripPlannerViewController.presentViewController(safariViewController, animated: true, completion: nil)
//                }else{
//                    self.log.error("url is nil for urlString:\r\(urlString)\r\(urlStringwithPercentEscapes)")
//                }
//            }else{
//                self.log.error("urlwithPercentEscapes is nil")
//            }
//        }else{
//            self.log.error("tflTrip.urlString is nil")
//        }
//    }
    
    //Works with any UIViewController that implements SFSafariViewControllerDelegate
//    func openTripInSafari<T: UIViewController where T: SFSafariViewControllerDelegate>(clkTrip : CLKTrip, viewController: T){
    
    
    func openTripInSafari(_ clkTrip : CLKTrip, openWithViewController: OpenWithViewController){
        
        let tflTrip = TFLTrip(clkTrip:clkTrip)
        // TODO: - clkTrip to TFLTrip
        if let urlString = tflTrip.urlString {
            print("tflTrip.urlString:\r\(urlString)")
            
            let urlwithPercentEscapes = urlString.addingPercentEncoding( withAllowedCharacters: CharacterSet.urlQueryAllowed)
            
            if let urlStringwithPercentEscapes = urlwithPercentEscapes{
                if let url = URL(string: urlStringwithPercentEscapes) {
                    
                    //SafariController button use app tint but has white border so can be hard to see
                    // AppearanceManager.appColorNavbarAndTabBar
                    // AppearanceManager.appColorTint
                    //appDelegate.window?.tintColor = UIColor.redColor()
                    
                    appDelegate.window?.tintColor = AppearanceManager.appColorNavbarAndTabBar
                    //... change back in  afariViewController: safariViewControllerDidFini
                    
                    let safariViewController = SFSafariViewController(url: url)
                    
                    safariViewController.delegate = openWithViewController //as SFSafariViewControllerDelegate
                    openWithViewController.present(safariViewController, animated: true, completion: nil)
                }else{
                    self.log.error("url is nil for urlString:\r\(urlString)\r\(urlStringwithPercentEscapes)")
                }
            }else{
                self.log.error("urlwithPercentEscapes is nil")
            }
        }else{
            self.log.error("tflTrip.urlString is nil")
        }
    }
    
    
}
