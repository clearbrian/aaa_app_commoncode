//
//  TFLApiPlaceCOLCQueryCollection.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 27/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation


class TFLApiPlaceCOLCQueryCollection : COLCQueryCollection{


    override func build_colcQueryDictionary() -> [String: COLCQuery]{
        
        //nearest/Name
        var colcQueryDictionary_ : [String: COLCQuery] = super.build_colcQueryDictionary()
        
        
        //----------------------------------------------------------------------------------------
        //NEAREST
        //----------------------------------------------------------------------------------------
        //we want Nearest first
        colcQueryDictionary_[TFLApiPlace.colcQuery_sortByNearest().title] = TFLApiPlace.colcQuery_sortByNearest()
        colcQueryDictionaryKeys.append(TFLApiPlace.colcQuery_sortByNearest().title)
        
        //----------------------------------------------------------------------------------------
        //EXAMPLE: "NearestJC", "NameJC" adds..."AvailableJC"
        //----------------------------------------------------------------------------------------
        colcQueryDictionary_[TFLApiPlace.colcQuery_sortByCommonName().title] = TFLApiPlace.colcQuery_sortByCommonName()
        colcQueryDictionaryKeys.append(TFLApiPlace.colcQuery_sortByCommonName().title)
        //----------------------------------------------------------------------------------------
        
        return colcQueryDictionary_
        
    }
}



