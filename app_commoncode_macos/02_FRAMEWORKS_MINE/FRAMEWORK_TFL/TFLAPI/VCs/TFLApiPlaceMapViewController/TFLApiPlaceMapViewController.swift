//
//  TFLApiPlaceMapViewController.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 14/01/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps
import AVFoundation
import AVKit
class TFLApiPlaceMapViewController : GenericMapViewController, TFLGeoJSONManagerDelegate{
    
    @IBOutlet weak var switchStations: UISwitch!
    @IBOutlet weak var switchLines: UISwitch!
    @IBOutlet weak var switchRanks: UISwitch!

    //--------------------------------------------------------------
    // MARK: -
    // MARK: - 3D
    //--------------------------------------------------------------
    @IBOutlet weak var switch3D: UISwitch!
    @IBOutlet weak var viewWrapperSwitch3D: UIView!
    
    //--------------------------------------------------------------
    @IBOutlet weak var labelPlaceCount: UILabel!
    
    var tflApiPlaceResponse: TFLApiPlaceResponse? //= TFLApiPlaceResponse()
    
    //--------------------------------------------------------------
    // MARK: - VIEW DID LOAD
    // MARK: -
    //--------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Map"
        
        viewWrapperSwitch3D.layer.cornerRadius = 4.0
        
        //default to traf sq else zoomed right out
        //moveToCurrentLocation()
        
        //----------------------------------------------------------------------------------------
        //LONDON Stations / Lines
        //----------------------------------------------------------------------------------------
        loadTFLStationsAndLines()
        //----------------------------------------------------------------------------------------
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //-----------------------------------------------------------------------------------
        //GET DATA - may be cached
        //-----------------------------------------------------------------------------------
        //ws called once - after that uses cached var - unless you pull to refresh
        self.callWebService(forceNewWSCall: false)
        
    }
    
    
    func callWebService(forceNewWSCall: Bool)
    {
        //super.callWebService()
        
        //----------------------------------------------------------------------------------------
        self.showLoadingOnMain()
        self.labelPlaceCount?.text = ""
        
        //----------------------------------------------------------------------------------------
        //GET APP DATA - may be cached
        //----------------------------------------------------------------------------------------
        appDelegate.log.error("request_app_TFLApiPlaceTypeRequest_via_Delegates IS COMMENTED OUT")
//        appDelegate.tflApiWSController.delegate = self
//        appDelegate.tflApiWSController.request_app_TFLApiPlaceTypeRequest_via_Delegates(forceNewWSCall: forceNewWSCall)
        //----------------------------------------------------------------------------------------
        
        //----------------------------------------------------------------------------------------
        //Jan 2018 - replaced with Unified api - better subclassing
        let tflApiPlace_GetAt_Request = TFLApiPlace_GetAt_Request(tflApiPlaceType:.taxiRank)
        appDelegate.log.info("tflUnifiedApiPlaceRequest:\(String(describing: tflApiPlace_GetAt_Request.urlString))")
        appDelegate.tflUnifiedApiWSController.delegate = self
        appDelegate.tflUnifiedApiWSController.call_WithDelegate(tflApiPlace_GetAt_Request)
        //----------------------------------------------------------------------------------------
        
        
    }
    
    //--------------------------------------------------------------
    // MARK: - ParentWSControllerDelegate
    // MARK: -
    //--------------------------------------------------------------
    
    override func requestStateAlreadyRunning(parentWSController: ParentWSController){
        self.log.error("MAP - requestStateAlreadyRunning - ws in progress - ok if you tap on map/list quickly")
        self.showLoadingOnMain()
    }
    
    override func response_success(parentWSController: ParentWSController, parsedObject: Any){
        // TODO: - add spinner to MAP
        if let tflApiPlaceResponse : TFLApiPlaceResponse = parsedObject as? TFLApiPlaceResponse {
            
            //----------------------------------------------------------------------------------------
            //RESPONSE OK - may be cached - list may load it first
            //----------------------------------------------------------------------------------------
            self.log.debug("tflApiPlaceResponse returned:\(tflApiPlaceResponse.tflApiPlaceArrayProcessed.count)")
            
            //self.genericListViewDataSource = tflApiPlaceResponse
            self.tflApiPlaceResponse = tflApiPlaceResponse
            
            self.addAllToMap_TFLApiPlace()
            self.hideLoadingOnMain()
            
        }else{
            appDelegate.log.error("parsedObject as? TFLApiPlaceResponse is nil")
        }
    }
    
    override func response_failure(parentWSController: ParentWSController, error: Error){
        //----------------------------------------------------------------------------------------
        print(error)
        
        CLKAlertController.showAlertInVC(self,
                                         title: "Error getting Taxi  rank information",
                                         message: "Please check your internet connection.\rIf this problem persists please contact support@cityoflondonconsulting.com with error:\r [\(error.localizedDescription)]")
        //----------------------------------------------------------------------------------------
    }
    

    //--------------------------------------------------------------
    // MARK: - TFLGeoJSONManagerDelegate
    // MARK: -
    //--------------------------------------------------------------
    //too slow
    func tflGeoJSONManagerLinesAndStationsLoaded(tflGeoJSONManager: TFLGeoJSONManager){
        
        //noisy print("LOAD LINES - tflGeoJSONManagerLinesAndStationsLoaded - addTFLLinesToMapFromJSON START")
        
        //OK but slow
        //addTFLStationsToMapFromJSON(tflGeoJSONManager:appDelegate.tflGeoJSONManager)
        
        addTFLLinesToMapFromJSON(tflGeoJSONManager:appDelegate.tflGeoJSONManager)
        //noisy print("LOAD LINES - tflGeoJSONManagerLinesAndStationsLoaded - addTFLLinesToMapFromJSON END")
    }
    
    
    
    
    
    //--------------------------------------------------------------
    // MARK: - DRAW ON MAP
    // MARK: -
    //--------------------------------------------------------------
    
    func addTFLLinesToMapFromJSON(tflGeoJSONManager: TFLGeoJSONManager){
        
        if let tflLineStringCollection = tflGeoJSONManager.tflStations_geoJSONLineStringFeatureCollection {
            if let features : [GeoJSONLineStringFeature] = tflLineStringCollection.features {
                
                /*
                 {
                 "type": "FeatureCollection",
                 "features": [
                 {
                 "geometry": {
                 "type": "LineString",
                 "coordinates": [
                 [
                 -0.059409291146068,
                 51.52391213822392
                 ],
                 ...
                 [
                 -0.057584287095381,
                 51.532416916418214
                 ]
                 ]
                 },
                 "type": "Feature",
                 "properties": {
                 "lines": [
                 {
                 "opened": 2015,
                 "end_sid": "910GBTHNLGR",
                 "colour": "#EE7C0E",
                 "network": "Overground",
                 "start_sid": "910GCAMHTH",
                 "last_wd": 0.0103,
                 "name": "London Overground"
                 }
                 ],
                 "id": "LeaValleyLine1"
                 }
                 },
                 */
                //----------------------------------------------------------------------------------------
                for geoJSONLineStringFeature in features{
                    
                    //print("FEATURE: lineId:\(geoJSONLineStringFeature.lineId) lineCount:\(geoJSONLineStringFeature.lineCount)")
                    
                    //------------------------------------------------------------------------------------------------
                    //GMSPolyline fromm .geometry
                    //------------------------------------------------------------------------------------------------
                    if let gmsPolyLine: GMSPolyline = geoJSONLineStringFeature.gmsPolyLineForTFLFeature(strokeWidth: 3.0) {
                        gmsPolyLine.map = self.gmsMapView
                        
                        //show / hide switches
                        self.arrayPolylines_Lines.append(gmsPolyLine)
                        
                        
                    }else{
                        appDelegate.log.error("geoJSONPointFeature.gmsPolyLine  is nil")
                    }
                    //------------------------------------------------------------------------------------------------
                    
                }//for
                
                //----------------------------------------------------------------------------------------
                appDelegate.log.debug("ADDED LINES:self.arrayPolylines_Lines.count:\(self.arrayPolylines_Lines.count)")
                //----------------------------------------------------------------------------------------
                self.switchLines?.isEnabled = true
                self.switchLines?.setOn(true, animated: true)
                
                //----------------------------------------------------------------------------------------
            }else{
                appDelegate.log.error("tflStationsPointCollection.features is nil")
            }
        }else{
            appDelegate.log.error("tflGeoJSONManager.tflStations_geoJSONPointFeatureCollection is nil")
        }
    }
    
    func addTFLStationsToMapFromJSON(tflGeoJSONManager: TFLGeoJSONManager){
        
        if let tflStationsPointCollection = tflGeoJSONManager.tflStations_geoJSONPointFeatureCollection {
            if let features : [GeoJSONPointFeature] = tflStationsPointCollection.features {
                /*
                 {
                 "geometry": {
                 "type": "Point",
                 "coordinates": [
                 -0.0130713,
                 51.510716
                 ]
                 },
                 "type": "Feature",
                 "properties": {
                 "tfl_intid": 850,
                 "cartography": {
                 "labelX": 25,
                 "labelY": -20
                 },
                 "lines": [
                 {
                 "name": "DLR"
                 }
                 ],
                 "id": "940GZZDLALL",
                 "name": "All Saints"
                 }
                 },
                 */
                for geoJSONPointFeature in features{
                    if let geometry = geoJSONPointFeature.geometry {
                        
                        if let coordinates:[Float] = geometry.coordinates {
                            if coordinates.count == 2{
                                //ITS LNG/LAT
                                
                                let latitude = Double.init(coordinates[1])
                                let longitude = Double.init(coordinates[0])
                                
                                
                                //----------------------------------------------------------------------------------------
                                //ADD MARKER
                                //----------------------------------------------------------------------------------------
                                //let clLocation = CLLocation.init(latitude: CLLocationDegrees(latitude),
                                //                                 longitude: CLLocationDegrees(longitude))
                                //location is ok
                                //let marker = GMSMarker(position: clLocation.coordinate)
                                
                                //marker.title = "UNKNOWN"
                                
                                var idString_ = ""
                                var nameString_ = ""
                                if let geoJSONPointProperty: GeoJSONPointProperty = geoJSONPointFeature.properties {
                                    
                                    
                                    
                                    //---------------------------------------------------------
                                    //Station ID - used as key to get image for icon
                                    //---------------------------------------------------------
                                    if let id = geoJSONPointProperty.id {
                                        idString_ = id
                                        //marker.snippet = markerSnippet()
                                        
                                        
                                        //                                        //---------------------------------------------------------
                                        //                                        //ICON - from dict of images
                                        //                                        //---------------------------------------------------------
                                        //                                        if let imageStation = appDelegate.tflGeoJSONManager.dictionaryStationImages[idString_] {
                                        //                                            marker.icon = imageStation
                                        //                                        }else{
                                        //                                            appDelegate.log.error("dictionaryStationImages[name:'\(idString_)'] is nil")
                                        //
                                        //
                                        //                                            if let imagePin = UIImage.init(named: "Roundel") {
                                        //                                                marker.icon = imagePin
                                        //                                            }else{
                                        //                                                //appDelegate.log.error("UIImage.init(named: imageName:'\(imageName)') is nil")
                                        //                                                marker.icon = nil
                                        //                                            }
                                        //                                        }
                                        //                                        //---------------------------------------------------------
                                        
                                    }else{
                                        appDelegate.log.error("geoJSONPointProperty.name is nil")
                                    }
                                    
                                    //---------------------------------------------------------
                                    //NAME - ICON
                                    //---------------------------------------------------------
                                    if let name = geoJSONPointProperty.name {
                                        //TITLE  - if text is in image then turn off - other wise set it so when you tap on pin the label pops up
                                        //marker.title = name
                                        //marker.snippet = markerSnippet()
                                        
                                        nameString_ = name
                                        
                                    }else{
                                        appDelegate.log.error("geoJSONPointProperty.name is nil")
                                    }
                                    
                                    //---------------------------------------------------------
                                    //gen code
                                    //---------------------------------------------------------
                                    //print("addTFLStation(\"\(nameString_)\",\"\(idString_)\",\(latitude),\(longitude))")
                                    //----------------------------------------------------------------------------------------
                                    
                                }else{
                                    appDelegate.log.error("geoJSONPointFeature.properties is nil")
                                }
                                
                                
                                //------------------------------------------------------------------------------------------------
                                //store the TFLApiPlaceTaxiRank with the marker
                                //marker.userData = geoJSONPointFeature
                                //------------------------------------------------------------------------------------------------
                                
                                
                                //------------------------------------------------------------------------------------------------
                                //marker.map = self.gmsMapView
                                //------------------------------------------------------------------------------------------------
                                
                                
                                //----------------------------------------------------------------------------------------
                                //buildMarker
                                //----------------------------------------------------------------------------------------
                                let _ = buildMarker(latitude: latitude, longitude: longitude, stationId: idString_, title: nameString_, userData: geoJSONPointFeature)
                                //----------------------------------------------------------------------------------------
                            }else{
                                appDelegate.log.error("coordinates.count == 2 FAILED:\(coordinates.count)")
                            }
                            
                        }else{
                            appDelegate.log.error("geometry.coordinates is nil")
                        }
                    }else{
                        appDelegate.log.error("geoJSONPointFeature.geometry  is nil")
                    }
                }
            }else{
                appDelegate.log.error("tflStationsPointCollection.features is nil")
            }
        }else{
            appDelegate.log.error("tflGeoJSONManager.tflStations_geoJSONPointFeatureCollection is nil")
        }
    }
    
    
    //called every time a new device location is recieved - UNLESS user manually moves the map then thats disabled
    //called when you tap on a row in the table
    override func moveMapToCLLocation(_ clLocation : CLLocation){
        //------------------------------------------------------------------------------------------------
        if clLocation.isValid{
            
            
            //-----------------------------------------------------------------------------------
            //zoom in
            //-----------------------------------------------------------------------------------
            //zoom in if very far out - when app starts can be a zoom 4.0
            //    var zoom: Float = self.gmsMapView.camera.zoom
            //    if self.gmsMapView.camera.zoom < 16{
            //        
            //        if self.switch3D.isOn{
            //            //we want some 3d buildings not all
            //            zoom = 17
            //        }else{
            //            zoom = 16
            //        }
            //        
            //    }
            
            var zoom : Float = 16.0
            if let gmsMapView = self.gmsMapView {
                zoom = gmsMapView.camera.zoom
                
                if zoom < 16{
                    zoom = 16
                }
                
            }
            
            //----------------------------------------------------------------------------------------
            //2D
            //----------------------------------------------------------------------------------------
            //    let cameraPosition = GMSCameraPosition.camera(withLatitude: clLocation.coordinate.latitude,
            //                                                  longitude:clLocation.coordinate.longitude,
            //                                                  zoom:zoom)
            //----------------------------------------------------------------------------------------
            //var mapView = GMSMapView.mapWithFrame(CGRectZero, camera:camera)
            //NOISY self.log.warning("LOC: animateToCameraPosition( cameraWithLatitude:[\(clLocation.coordinate.latitude),\(clLocation.coordinate.longitude)")
            //----------------------------------------------------------------------------------------
            
            //----------------------------------------------------------------------------------------
            //3D or 2D
            //----------------------------------------------------------------------------------------
            //            //flat
            //   (51.205, -1.690) bearing:0.000 zoomLevel:4.935 viewingAngle:0.000
            //   3d
            //   didChangeCameraPosition:GMSCameraPosition 0x170e54490:
            //   target:(51.509, -0.072) bearing:348.488 zoomLevel:17.463 viewingAngle:65.000
            //----------------------------------------------------------------------------------------
            //default flat
            var viewingAngle_: Double = 0.0
            
            if self.switch3D.isOn{
                viewingAngle_ = 55.0
            }else{
                viewingAngle_ = 0.0
            }
            let cameraPosition = GMSCameraPosition.camera(withTarget: clLocation.coordinate,
                                                          zoom: zoom,
                                                          bearing: self.gmsMapView.camera.bearing,
                                                          viewingAngle: viewingAngle_)
            
            
            
            //----------------------------------------------------------------------------------------
            self.gmsMapView.animate(to: cameraPosition)
            
            //----------------------------------------------------------------------------------------
            //triggered AFTER move complete in idleAtCameraPosition:
            //self.nearbySearch(currentLocation)
            
            
            //-----------------------------------------------------------------------------------
        }else{
            self.log.debug("LOC: [moveMapToCLLocation(clLocation:\(clLocation))] clLocation.isValid is false - dont call animateToCameraPosition")
        }
    }
  
    public override func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition){
        
        super.mapView(mapView, idleAt: position)
        
        //user may manually swipe off 3d
        if position.viewingAngle <= 0.0{
            
            //causes bounce is already on when you turn on
            if self.switch3D.isOn{
                //print("map changed: ON >> turn OFF")
                self.switch3D.setOn(false, animated: false)
            }else{
                //already off
                //print("map changed: OFF >> OFF - do nothin")
            }
        }else{
            //WRONG it animates from 0.1 .... n so this is triggered all the time
            //the if 0.0 works as it only hits it once
            // MAP IS in 3D MODE
            if self.switch3D.isOn{
                //already on
                //print("map changed: ON >> ON - do nothin")
            }else{
                //print("map changed: OFF >> turn ON")
                self.switch3D.setOn(true, animated: false)
            }
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - MAP
    // MARK: -
    //--------------------------------------------------------------
    
    var arrayMarkers_Stations = [GMSMarker]()
    var arrayMarkers_Ranks = [GMSMarker]()
    
    var arrayPolylines_Lines = [GMSPolyline]()
    
    // TODO: - add to utils as GoogleMappable
    func addAllToMap_TFLApiPlace(){
        appDelegate.log.debug("CALLING addAllToMap_TFLApiPlace")
        
        if let tflPlaceResponse = self.tflApiPlaceResponse {

            if tflPlaceResponse.tflApiPlaceArrayProcessed.count == 0{
                appDelegate.log.error("tflApiPlaceArrayProcessed == 0")

            }else{

                for tflApiPlace in tflPlaceResponse.tflApiPlaceArrayProcessed{

                    if let gmsMarker = tflApiPlace.gmsMarker {
                        gmsMarker.map = self.gmsMapView
                        
                        
                        if let _ = tflApiPlace as? TFLApiPlaceJamCam {
                            //gmsMarker.groundAnchor = CGPoint(x: 0.5,y: 0.5)
                            gmsMarker.groundAnchor = CGPoint(x: 0.5,y: 1.0) //bottom of pole should be in center
                        }else{
                            appDelegate.log.error("self.<#EXPR#> is nil")
                        }

                        arrayMarkers_Ranks.append(gmsMarker)

                    }else{
                        appDelegate.log.error("tfl.gmsMarker  is nil")
                    }
                }
                appDelegate.log.debug("arrayMarkers_Ranks:\(arrayMarkers_Ranks.count)")
            }
        }else{
            appDelegate.log.error("appDelegate.tflPlaceResponse is nil")
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - TFLGeoJSONManager
    // MARK: -
    //--------------------------------------------------------------
    func loadTFLStationsAndLines(){
        
        //pins not configured - load stations from ivar/ lines form json
        self.addBuiltIntStationsToMap()
        
        //TO SLOW - run to dump info
        appDelegate.tflGeoJSONManager.delegate = self
        
        //----------------------------------------------------------------------------------------
        //Comment one or the othe rin but not both
        //----------------------------------------------------------------------------------------
        
        //SLOW
        //appDelegate.tflGeoJSONManager.loadTFLStationsAndLines(loadStationsFromJSON: true)
        // TODO: - MAP: HARDCODED TFL Stations
        appDelegate.tflGeoJSONManager.loadTFLStationsAndLines(loadStationsFromJSON: false)
        
    }
    
    //--------------------------------------------------------------
    // MARK: - GEO JSON - TFL Stations
    // MARK: -
    //--------------------------------------------------------------
    //built in - not parsed json
    func addBuiltIntStationsToMap(){
        appDelegate.log.debug("CALLING addBuiltIntStationsToMap")
        
        for tflStation in appDelegate.tflGeoJSONManager.arrayStations{
            
            let stationMarker = self.buildMarker(latitude: tflStation.lat, longitude: tflStation.lng, stationId: tflStation.idString, title: tflStation.name, userData: tflStation)
            arrayMarkers_Stations.append(stationMarker)
            
            appDelegate.log.debug("[MAP] [\(tflStation.idString)]")
            
        }
        appDelegate.log.debug("arrayMarkers_Stations:\(arrayMarkers_Stations.count)")
    }
    
    //--------------------------------------------------------------
    // MARK: - SWITCH show/hide
    // MARK: -
    //--------------------------------------------------------------
    @IBAction func switchStations_ValueChanged(_ sender: Any) {
        if self.switchStations.isOn{
            self.showOnMap_Stations()
        }else{
            self.hideOnMap_Stations()
        }
    }
    
    @IBAction func switchRanks_ValueChanged(_ sender: Any) {
        if self.switchRanks.isOn{
            self.showOnMap_Ranks()
        }else{
            self.hideOnMap_Ranks()
        }
    }
    
    @IBAction func switchLines_ValueChanged(_ sender: Any) {
        if self.switchLines.isOn{
            self.showOnMap_Lines()
        }else{
            self.hideOnMap_Lines()
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - SHOW HIDE STATIONS
    // MARK: -
    //--------------------------------------------------------------
    func showOnMap_Stations(){
        for gmsMarker in arrayMarkers_Stations{
            gmsMarker.map = self.gmsMapView
        }
    }
    func hideOnMap_Stations(){
        for gmsMarker in arrayMarkers_Stations{
            gmsMarker.map = nil
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - SHOW HIDE STATIONS
    // MARK: -
    //--------------------------------------------------------------
    func showOnMap_Ranks(){
        for gmsMarker in arrayMarkers_Ranks{
            gmsMarker.map = self.gmsMapView
        }
    }
    func hideOnMap_Ranks(){
        for gmsMarker in arrayMarkers_Ranks{
            gmsMarker.map = nil
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - SHOW HIDE LINES
    // MARK: -
    //--------------------------------------------------------------
    func showOnMap_Lines(){
        for gmsPolyline in arrayPolylines_Lines{
            gmsPolyline.map = self.gmsMapView
        }
    }
    func hideOnMap_Lines(){
        for gmsPolyline in arrayPolylines_Lines{
            gmsPolyline.map = nil
        }
    }
    

    
    //--------------------------------------------------------------
    // MARK: - prepareForSegue
    // MARK: -
    //--------------------------------------------------------------
    //OVERRIDE to keep alert in GerneicMapVC - segue in Storyboard inc subclass not Generic
    //--------------------------------------------------------------

    let segueTFLApiPlaceMapViewControllerTOTFLApiPlaceStreetViewController        = "segueTFLApiPlaceMapViewControllerTOTFLApiPlaceStreetViewController"
    let segueTFLApiPlaceMapViewControllerTOTFLApiPlaceMapDirectionsViewController = "segueTFLApiPlaceMapViewControllerTOTFLApiPlaceMapDirectionsViewController"
    let segueTFLApiPlaceMapViewControllerTOAVPlayerViewController = "segueTFLApiPlaceMapViewControllerTOAVPlayerViewController"

    
    override var segueTo_TFLApiPlaceMapDirectionsViewController: String{
        return segueTFLApiPlaceMapViewControllerTOTFLApiPlaceMapDirectionsViewController
    }
    
    override var segueTo_TFLApiPlaceStreetViewController: String{
        return segueTFLApiPlaceMapViewControllerTOTFLApiPlaceStreetViewController
    }
    
    override var segueTo_AVPlayerViewController: String{
        return segueTFLApiPlaceMapViewControllerTOAVPlayerViewController
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == segueTFLApiPlaceMapViewControllerTOTFLApiPlaceMapDirectionsViewController){
            
            if(segue.destination.isMember(of: TFLApiPlaceMapDirectionsViewController.self)){
                
                let tflApiPlaceMapDirectionsViewController = (segue.destination as! TFLApiPlaceMapDirectionsViewController)
                //tflApiPlaceMapDirectionsViewController.delegate = self
                tflApiPlaceMapDirectionsViewController.selectedTFLApiPlace = self.selectedTFLApiPlace
                
            }
            else{
                print("ERROR:not TFLApiPlaceMapDirectionsViewController")
            }
        }
        else if(segue.identifier == segueTFLApiPlaceMapViewControllerTOTFLApiPlaceStreetViewController){

            if(segue.destination.isMember(of: TFLApiPlaceStreetViewController.self)){

                let tflApiPlaceStreetViewController = (segue.destination as! TFLApiPlaceStreetViewController)
                //tflApiPlaceMapViewController.delegate = self
                tflApiPlaceStreetViewController.selectedTFLApiPlace = self.selectedTFLApiPlace

            }else{
                print("ERROR:not TFLApiPlaceMapViewController")
            }
        }
        else if(segue.identifier == segueTo_AVPlayerViewController){
            //import AVFoundation
            //import AVKit
            if(segue.destination.isMember(of: AVPlayerViewController.self)){
                
                let avPlayerViewController = (segue.destination as! AVPlayerViewController)
                
                if let tflApiPlaceJamCam = self.selectedTFLApiPlace as? TFLApiPlaceJamCam {
                    if let videoUrlString = tflApiPlaceJamCam.videoUrlString {
                        if videoUrlString.trim() == "" {
                            appDelegate.log.error("videoUrlString is ''")
                        }else{
                            //https://s3-eu-west-1.amazonaws.com/jamcams.tfl.gov.uk/00001.01409.mp4"
                            if let movieURL = URL(string:videoUrlString){
                                
                                let avPlayer = AVPlayer.init(url: movieURL)
                                avPlayerViewController.player = avPlayer
                                avPlayerViewController.player?.play()
                                
                            }else{
                                self.log.error("URL is nil")
                            }
                        }
                    }else{
                        appDelegate.log.error("tflApiPlaceJamCam.videoUrlString is nil")
                    }
                }else{
                    appDelegate.log.error("self.selectedTFLApiPlace as? TFLApiPlaceJamCam is nil")
                }
            }
            else{
                self.log.error(":not AVPlayerViewController")
            }
        }
        else{
            self.log.error("UNHANDLED SEGUE:\(String(describing: segue.identifier))")
        }
    }
    
}
