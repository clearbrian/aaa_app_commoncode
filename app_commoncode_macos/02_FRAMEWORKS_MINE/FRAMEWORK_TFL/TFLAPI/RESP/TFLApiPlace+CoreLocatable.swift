//
//  TFLApiPlace+CoreLocatable.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 14/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
import CoreLocation

extension TFLApiPlace{
    
    //--------------------------------------------------------------
    // MARK: - GoogleMappable >> CoreLocatable
    // MARK: -
    //--------------------------------------------------------------
    //Declarations in extensions cant override yet - moved back into TFLApiPlace and COLCPlace
    //    //var clLocation: CLLocation?{
    //        
    //    func clLocation() -> CLLocation?{
    //        var locationReturned: CLLocation? = nil
    //        
    //        if let locationWithFloatsReturned = CLLocation.locationWith(latFloat: self.lat, lonFloat: self.lon ){
    //            locationReturned = locationWithFloatsReturned
    //        }else{
    //            appDelegate.log.error("locationWithFloatsReturned is nil")
    //        }
    //        
    //        return locationReturned
    //    }
}
