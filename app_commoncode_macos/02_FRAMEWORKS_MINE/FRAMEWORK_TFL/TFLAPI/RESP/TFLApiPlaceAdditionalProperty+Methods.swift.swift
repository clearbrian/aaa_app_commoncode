//
//  TFLApiPlaceAdditionalProperty+Methods.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 13/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation


extension TFLApiPlaceAdditionalProperty: CustomStringConvertible{
    
    //--------------------------------------------------------------
    // MARK: - CustomStringConvertible
    // MARK: -
    //--------------------------------------------------------------
    
    var description : String {
        var description_ = "TFLApiPlaceAdditionalProperty:"
        description_ = description_ + "             type:\(String(describing: self.type))\r"
        description_ = description_ + "         category:\(String(describing: self.category))\r"
        description_ = description_ + "         modified:\(String(describing: self.modified))\r"
        description_ = description_ + "  sourceSystemKey:\(String(describing: self.sourceSystemKey))\r"
        description_ = description_ + "              key:\(String(describing: self.key))\r"
        description_ = description_ + "            value:\(String(describing: self.value))\r"
        
        return description_
    }
    
    var descriptionSimple : String {
        //----------------------------------------------------------------------------------------
        var category_ = ""
        if let category = self.category {
            category_ = "category:\(category)"
        }else{
            appDelegate.log.error("self.category is nil")
        }
        //----------------------------------------------------------------------------------------
        var key_ = ""
        if let key = self.key {
            key_ = "key:\(key)"
        }else{
            appDelegate.log.error("self.key is nil")
        }
        //----------------------------------------------------------------------------------------
        var value_ = ""
        if let value = self.value {
            value_ = "value:\(value)"
        }else{
            appDelegate.log.error("self.value is nil")
        }
        //----------------------------------------------------------------------------------------
        
        let description_ = "TFLApiPlaceAdditionalProperty:[\(category_) - \(key_) - \(value_)]"
        return description_
    }
    
    var descriptionSimpleCSV : String {
        //----------------------------------------------------------------------------------------
        var category_ = ""
        if let category = self.category {
            category_ = "\(category)"
        }else{
            //appDelegate.log.error("self.category is nil")
        }
        //----------------------------------------------------------------------------------------
        var key_ = ""
        if let key = self.key {
            key_ = "\(key)"
        }else{
            //appDelegate.log.error("self.key is nil")
        }
        //----------------------------------------------------------------------------------------
        var value_ = ""
        if let value = self.value {
            value_ = "\(value)"
        }else{
            //appDelegate.log.error("self.value is nil")
        }
        //----------------------------------------------------------------------------------------
        
        let description_ = "PROP,\(category_),\(key_),\(value_)"
        return description_
    }
    
    
    //--------------------------------------------------------------
    // MARK: - key:value
    // MARK: -
    //--------------------------------------------------------------
    //IN : tflApiPlaceAdditionalProperty.key
    //OUT: tflApiPlaceAdditionalProperty.value
    //--------------------------------------------------------------

    func valueForKey(keyString keyStringToFind: String?) -> String?{
        var valueForKey_ : String? = nil
        
        //if we iterating over all properties checkign all keys - key is String?
        if let keyStringToFindSafe = keyStringToFind {
            if let keySafe = self.key {
                
                //Both value non nil - safe to check
                if keySafe == keyStringToFindSafe{
                    //THIS Property matches the key we want so return the value
                    if let value = self.value {
                        valueForKey_ = value
                        
                    }else{
                        appDelegate.log.error("self.value is nil")
                    }
                }
                
            }else{
                appDelegate.log.error("self.key is nil")
            }
        }else{
            appDelegate.log.error("keyStringToFind is nil")
        }
        
        return valueForKey_
    }
    
    //ok to output "" if nil
    func valueForKeySafe(keyString keyStringToFind: String) -> String{
        
        var valueForKeySafeRet : String = ""
        
        if let valueForKeySafe = self.valueForKey(keyString: keyStringToFind) {
            valueForKeySafeRet = valueForKeySafe
        }else{
            appDelegate.log.error("self.valueForKey(..) returns nil is nil")
        }
        return valueForKeySafeRet
    }
}
