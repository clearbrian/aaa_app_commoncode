//
//  TFLUnifiedApiWSController.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 21/07/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

class TFLUnifiedApiWSController : ParentWSController{
    
    
    
    func call_WithDelegate(_ tflUnifiedApiPlaceRequest: TFLUnifiedApiPlaceRequest)
    {
        //---------------------------------------------------------------------
        //CALL THE WS
        //---------------------------------------------------------------------
        if let _ =  tflUnifiedApiPlaceRequest.urlString{
            
            //---------------------------------------------------------------------
            //NOISY self.log.debug("urlString:\r\(urlString)")
            //---------------------------------------------------------------------
            self.callWS(tflUnifiedApiPlaceRequest,
                        
                        success:{  (parsedObject: Any?)->Void in
                            //----------------------------------------------------------------------------------------
                            //success
                            //----------------------------------------------------------------------------------------
                            
                            let tflApiPlaceResponse = TFLApiPlaceResponse()
                            
                            if let tflApiPlace_GetAt_Request = tflUnifiedApiPlaceRequest as? TFLApiPlace_GetAt_Request {
                                //----------------------------------------------------------------------------------------
                                
                                
                                
                                if tflApiPlace_GetAt_Request.tflApiPlaceType == .taxiRank{
                                    //------------------------------------------------------------------------------------------------
                                    if let tflApiPlaceTaxiRankArray : [TFLApiPlaceTaxiRank] = Mapper<TFLApiPlaceTaxiRank>().mapArray(JSONObject:parsedObject)
                                    {
                                        //---------------------------------------------------------
                                        //PROCESS TAXI RANK VERSION - KEEP ONLY THE LATEST
                                        //---------------------------------------------------------
                                        let tflApiPlaceTaxiRankArrayProcessed: [TFLApiPlaceTaxiRank] = TFLApiPlaceTaxiRank.processTankRankVersions(tflApiPlaceTaxiRankArray: tflApiPlaceTaxiRankArray)
                                        
                                        //tflApiPlaceResponse.tflApiPlaceArrayUnsorted = tflApiPlaceTaxiRankArrayProcessed
                                        tflApiPlaceResponse.tflApiPlaceArrayProcessed = tflApiPlaceTaxiRankArrayProcessed
                                        
                                        //--------------------------------------------------------------------
                                    }
                                    else{
                                        self.log.error("parsedObject is nil or not [TFLApiPlace]")
                                    }
                                    
                                }else if tflApiPlace_GetAt_Request.tflApiPlaceType == .jamCam{
                                    //------------------------------------------------------------------------------------------------
                                    if let tflApiPlaceTFLApiPlaceJamCamArray : [TFLApiPlaceJamCam] = Mapper<TFLApiPlaceJamCam>().mapArray(JSONObject:parsedObject)
                                    {
                                        //tflApiPlaceResponse.tflApiPlaceArrayUnsorted = tflApiPlaceTFLApiPlaceJamCamArray
                                        tflApiPlaceResponse.tflApiPlaceArrayProcessed = tflApiPlaceTFLApiPlaceJamCamArray
                                        
                                    }
                                    else{
                                        self.log.error("parsedObject is nil or not [TFLApiPlaceJamCam]")
                                    }
                                    
                                }else{
                                    
                                    //All other TFLApiPlace subtypes - JamCam etc
                                    
                                    //-------------------------------------------------------------------
                                    //TFLApiPlaceTaxiRank is TFLApiPlace
                                    if let tflApiPlaceArray : [TFLApiPlace] = Mapper<TFLApiPlace>().mapArray(JSONObject:parsedObject)
                                    {
                                        //--------------------------------------------------------------------
                                        //tflApiPlaceResponse.tflApiPlaceArrayUnsorted = tflApiPlaceArray
                                        tflApiPlaceResponse.tflApiPlaceArrayProcessed = tflApiPlaceArray
                                        //--------------------------------------------------------------------
                                        
                                    }else{
                                        self.log.error("parsedObject is nil or not [TFLApiPlace]")
                                    }
                                    //-------------------------------------------------------------------
                                }
                                //----------------------------------------------------------------------------------------
                            }else{
                                appDelegate.log.error("tflUnifiedApiPlaceRequest as? tflApiPlace_GetAt_Request FAILED")
                            }
                            
                            
                            
                            //self.log.debug("tflApiPlaceResponse.tflApiPlaceArrayUnsorted returned:\(tflApiPlaceResponse.tflApiPlaceArrayUnsorted.count)")
                            self.log.debug("tflApiPlaceResponse.tflApiPlaceArrayProcessed returned:\(tflApiPlaceResponse.tflApiPlaceArrayProcessed.count)")
                            //---------------------------------------------------------------------
                            //RESPONSE OK - CACHE IT
                            //---------------------------------------------------------------------
                            //self.tflApiPlaceResponse = tflApiPlaceResponse
                            
                            self.delegate_response_success(parsedObject: tflApiPlaceResponse)
                            //---------------------------------------------------------------------
                            
            },
                        failure:{ (error) -> Void in
                            self.delegate_response_failure(error: error)
            }
            )
            //---------------------------------------------------------------------
        }else{
            self.log.error("urlString is nil - 4733")
        }
    }



    //--------------------------------------------------------------
    // MARK: - Call WS - managed by block - ok for one vc but not when map and list share same response
    // MARK: -
    //--------------------------------------------------------------
    func call_withBlocks_TFLApiPlaceTypeRequest(_ tflApiPlaceTypeRequest: TFLApiPlaceTypeRequest,
                                                success: @escaping (_ tflApiPlaceResponse : TFLApiPlaceResponse) -> Void,
                                                failure: @escaping (_ error: Error) -> Void
        )
    {
        
        //---------------------------------------------------------------------
        //CALL THE WS
        //---------------------------------------------------------------------
        if let _ =  tflApiPlaceTypeRequest.urlString{
            //---------------------------------------------------------------------
            //NOISY self.log.debug("urlString:\r\(urlString)")
            //---------------------------------------------------------------------
            self.callWS(
                tflApiPlaceTypeRequest,
                
                success:{
                    (parsedObject: Any?)->Void in
                    
                    
                    let tflApiPlaceResponse = TFLApiPlaceResponse()
                    
//                    if tflApiPlaceTypeRequest.tflApiPlaceType == .taxiRank{
//                        //------------------------------------------------------------------------------------------------
//                        if let tflApiPlaceTaxiRankArray : [TFLApiPlaceTaxiRank] = Mapper<TFLApiPlaceTaxiRank>().mapArray(JSONObject:parsedObject)
//                        {
//                            //---------------------------------------------------------
//                            //PROCESS TAXI RANK VERSION - KEEP ONLY THE LATEST
//                            //---------------------------------------------------------
//                            let tflApiPlaceTaxiRankArrayProcessed: [TFLApiPlaceTaxiRank] = self.processTankRankVersions(tflApiPlaceTaxiRankArray: tflApiPlaceTaxiRankArray)
//                            
//                            tflApiPlaceResponse.tflApiPlaceArrayProcessed = tflApiPlaceTaxiRankArrayProcessed
//                            
//                            //--------------------------------------------------------------------
//                        }
//                        else{
//                            self.log.error("parsedObject is nil or not [TFLApiPlace]")
//                        }
//                        
//                    }else if tflApiPlaceTypeRequest.tflApiPlaceType == .jamCam{
//                        //------------------------------------------------------------------------------------------------
//                        if let tflApiPlaceTFLApiPlaceJamCamArray : [TFLApiPlaceJamCam] = Mapper<TFLApiPlaceJamCam>().mapArray(JSONObject:parsedObject)
//                        {
//                            tflApiPlaceResponse.tflApiPlaceArrayProcessed = tflApiPlaceTFLApiPlaceJamCamArray
//                            
//                        }
//                        else{
//                            self.log.error("parsedObject is nil or not [TFLApiPlaceJamCam]")
//                        }
//                        
//                    }else{
//                        
//                        //All other TFLApiPlace subtypes - JamCam etc
//                        
//                        //-------------------------------------------------------------------
//                        //TFLApiPlaceTaxiRank is TFLApiPlace
//                        if let tflApiPlaceArray : [TFLApiPlace] = Mapper<TFLApiPlace>().mapArray(JSONObject:parsedObject)
//                        {
//                            //--------------------------------------------------------------------
//                            tflApiPlaceResponse.tflApiPlaceArrayProcessed = tflApiPlaceArray
//                            //--------------------------------------------------------------------
//                            
//                        }else{
//                            self.log.error("parsedObject is nil or not [TFLApiPlace]")
//                        }
//                        //-------------------------------------------------------------------
//                    }
                    
                    
                    self.log.debug("tflApiPlaceResponse.tflApiPlaceArrayUnsorted returned:\(tflApiPlaceResponse.tflApiPlaceArrayProcessed.count)")
                    //---------------------------------------------------------------------
                    //RESPONSE OK
                    //---------------------------------------------------------------------
                    success(tflApiPlaceResponse)
                    //---------------------------------------------------------------------
                    
            },
                failure:{(error) -> Void in
                    failure(error)
            }
            )
            //---------------------------------------------------------------------
        }else{
            self.log.error("urlString is nil - 4744")
        }
    }
}
