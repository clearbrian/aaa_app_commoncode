////
////  TFLApiPlaceRequest.swift
////  app_arkit_gps
////
////  Created by Brian Clear on 15/08/2017.
////  Copyright © 2017 City Of London Consulting. All rights reserved.
////
//
//import Foundation
//
//class TFLUnifiedApiPlaceRequest : TFLUnifiedApiEndpointParentRequest{
//    
//    //var parameters_required_
//    
//    init(){
//        super.init(tflUnifiedEndPoint:.place)
//        
//    }
//}
//
//
///*
// 
//     "/Place/{id}": {
//     "operationId": "Place_Get"
//     
//     "parameters": [
//     {
//     "description": "The id of the place, you can use the /Place/Types/{types} endpoint to get a list of places for a given type including their ids",
//     "required": true,
//     "type": "string",
//     "name": "id",
//     "in": "path"
//     },
//     {
//     "description": "Defaults to false. If true child places e.g. individual charging stations at a charge point while be included, otherwise just the URLs of any child places will be returned",
//     "required": false,
//     "type": "boolean",
//     "name": "includeChildren",
//     "in": "query"
//     }
//     ],
// 
// */
//class TFLApiPlace_GetRequest : TFLUnifiedApiPlaceRequest{
//
//    //    // "required": true,
//    //    var place_id : String
//    //
//    //
//    //    //"required": false
//    //    var place_includeChildren : Bool?
//
//
//    //    init(){
//    //        super.init(tflEndPoint:.place)
//    //
//    //    }
//}
//
//
///*
//     ======== ======== ======== ======== ======== ========
//     paths and their ops
//     ======== ======== ======== ======== ======== ========
//     "/Place": {
//     "operationId": "Place_GetByGeoBox"
//     
//     
//     "/Place/{id}": {
//     "operationId": "Place_Get"
//     
//     "/Place/{type}/At/{Lat}/{Lon}": {
//     "operationId": "Place_GetAt"
//     
//     
//     "/Place/{type}/overlay/{z}/{Lat}/{Lon}/{width}/{height}": {
//     "operationId": "Place_GetOverlay"
//     
//     
//     "/Place/Address/Streets/{Postcode}": {
//     "operationId": "Place_GetStreetsByPostCode"
//     
//     
//     "/Place/Meta/Categories": {
//     "operationId": "Place_MetaCategories"
//     
//     "/Place/Meta/PlaceTypes": {
//     "operationId": "Place_MetaPlaceTypes"
//     
//     "/Place/Search": {
//     "operationId": "Place_Search"
//     
//     "/Place/Type/{types}": {
//     "operationId": "Place_GetByType"
// 
// 
// 
// 
// */

