//
//  TFLEndPoint.swift
//  app_arkit_gps
//
//  Created by Brian Clear on 15/08/2017.
//  Copyright © 2017 City Of London Consulting. All rights reserved.
//

import Foundation

//Swagger operations are grouped by tag
//"tags": [
//"StopPoint"
//],

enum TFLUnifiedEndPoint{
    case accidentStats
    case airQuality
    case bikePoint
    case journey
    case line
    case mode
    case occupancy
    case place
    case road
    case search
    case stopPoint
    case vehicle
    
    func path() -> String{
        switch self{
        case .accidentStats:
            return "AccidentStats"
            
        case .airQuality:
            return "AirQuality"
            
        case .bikePoint:
            return "BikePoint"
            
        case .journey:
            return "Journey"
            
        case .line:
            return "Line"
            
        case .mode:
            return "Mode"
            
        case .occupancy:
            return "Occupancy"
            
        case .place:
            return "Place"
            
        case .road:
            return "Road"
            
        case .search:
            return "Search"
            
        case .stopPoint:
            return "StopPoint"
            
        case .vehicle:
            return "Vehicle"
            
        }
    }
}
