//
//  TFLApiPlaceRequest.swift
//  app_arkit_gps
//
//  Created by Brian Clear on 15/08/2017.
//  Copyright © 2017 City Of London Consulting. All rights reserved.
//

import Foundation

class TFLUnifiedApiPlaceRequest : TFLUnifiedApiEndpointParentRequest{
    
    init(){
        super.init(tflUnifiedEndPoint:.place)
    }
    
}

 /*
 TFL SWAGGER OPERATIONS /Place/<>   : NOT IMPLEMENTED YET SEE SUBCLASSES
     ======== ======== ======== ======== ======== ========
     paths and their ops
     ======== ======== ======== ======== ======== ========
     "/Place": {
     "operationId": "Place_GetByGeoBox"
     
     
     "/Place/{id}": {
     "operationId": "Place_Get"
     
     "/Place/{type}/At/{Lat}/{Lon}": {
     "operationId": "Place_GetAt"
     
     
     "/Place/{type}/overlay/{z}/{Lat}/{Lon}/{width}/{height}": {
     "operationId": "Place_GetOverlay"
     
     
     "/Place/Address/Streets/{Postcode}": {
     "operationId": "Place_GetStreetsByPostCode"
     
     
     "/Place/Meta/Categories": {
     "operationId": "Place_MetaCategories"
     
     "/Place/Meta/PlaceTypes": {
     "operationId": "Place_MetaPlaceTypes"
     
     "/Place/Search": {
     "operationId": "Place_Search"
     
     "/Place/Type/{types}": {
     "operationId": "Place_GetByType"
 
*/
