//
//  TableDisplayable.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 17/01/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

protocol TableDisplayable{
    func textLabelText() -> String
    
    func detailLabelText0() -> String?
    func detailLabelText1() -> String?
    
    func subDetailLabelText0() -> String?
    func subDetailLabelText1() -> String?
    
    
}
