//
//  EmptyListDataSource.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 27/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
class EmptyListDataSource: ParentGenericListViewDataSource{
    
    //Any is String
    //returns Dictionary<String, Array<String>>
    override var dictionaryTableData : Dictionary<String, Array<Any>>{
        return Dictionary<String, Array<Any>>()
    }
}
