//
//  ParentGenericListViewDataSource.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 23/01/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
import UIKit


//protocol ParentGenericListViewDataSourceDelegate {
//    func dataSourceProcessed(parentGenericListViewDataSource: ParentGenericListViewDataSource)
//}

class ParentGenericListViewDataSource: GenericListViewDataSourceProtocol/*, COLCQueryCollectionDelegate*/{
  
    //--------------------------------------------------------------
    // MARK: - GenericListViewDataSourceProtocol
    // MARK: -
    //--------------------------------------------------------------
    var colcQuery : COLCQuery?
    var colcQueryCollectionForType: COLCQueryCollection = TFLApiPlaceType.colcQueryCollectionForType(AppConfig.tflApiPlaceType)
    
    init() {

    }
    
    //--------------------------------------------------------------
    // MARK: - GenericListViewDataSourceProtocol
    // MARK: -
    //--------------------------------------------------------------
    //subclass
    func requery(colcQuery: COLCQuery){
        // TODO: - move code in subclass up COLCQuerably
        appDelegate.log.debug("requery : SUBCLASS - rerun query")
    }
    
    func requeryWithCurrentCOLCQuery(){

        if let currentCOLCQuery = self.colcQueryCollectionForType.currentCOLCQuery{
            self.requery(colcQuery:currentCOLCQuery)
        }else{
            appDelegate.log.error("colcQueryCollectionForType.currentCOLCQuery is nil")
        }
    }
    
    func currentCOLCQuery() -> COLCQuery?{
        
        if let currentCOLCQuery = self.colcQueryCollectionForType.currentCOLCQuery{
            return currentCOLCQuery
        }else{
            appDelegate.log.error("colcQueryCollectionForType.currentCOLCQuery is nil")
            return nil
        }
    }
    
    
    //--------------------------------------------------------------
    // MARK: - dictionaryTableData
    // MARK: -
    //--------------------------------------------------------------
    
    //Any is String
    var dictionaryTableData : Dictionary<String, Array<Any>>{
        return Dictionary<String, Array<Any>>()
    }
    

    //--------------------------------------------------------------
    // MARK: - KEYS
    // MARK: -
    //--------------------------------------------------------------
    var keysArrayUnsorted : Array<String>{
        let keysArrayUnsorted = Array(dictionaryTableData.keys)
        return keysArrayUnsorted
    }
    
    //needed because dictionaryTableData.keys.array is not sorted alphabetically by insertion order
    var keysArraySorted : Array<String>{
        let keysArraySorted = self.keysArrayUnsorted.sorted { (string0, string1) -> Bool in
            if string0.compare(string1, options: .numeric) == .orderedAscending{
                return true
                
            }else{
                return false
            }
        }
        return keysArraySorted
    }
    
    //------------------------------------------------
    // MARK: retrieveSortedKeyForSection
    //------------------------------------------------
    //Sort the keys array before accessing them by section
    //must be done everywhere we get key_section
    func retrieveSortedKeyForSection(_ section: Int) ->String{
        //String
        
        //NOISY print("dictionaryTableData.keys.count:\(dictionaryTableData.keys.count)")
        if self.keysArraySorted.count > section{
            let key_section : String = self.keysArraySorted[section]
            return key_section
        }else{
            return ""
        }
        
    }
    
    //--------------------------------------------------------------
    // MARK: - TOTALCOUNT - for labelc
    // MARK: -
    //--------------------------------------------------------------
    var totalsCount: Int {
         return 0
    }
    
    
    //--------------------------------------------------------------
    // MARK: - SECTIONS
    // MARK: -
    //--------------------------------------------------------------
    func numberOfSections() -> Int {
        let numberOfSections_ = self.keysArrayUnsorted.count
        return numberOfSections_
    }
    
    
    //section header - nil to hide if ungrouped
    func titleForHeaderInSection(_ section: Int) -> String?{
        //---------------------------------------------------------
        //KEY in dictionaryTableData_Grouped in subclass
        //---------------------------------------------------------
        let key_section = self.retrieveSortedKeyForSection(section)
        return key_section
    }
    
    //--------------------------------------------------------------
    // MARK: - ROWS
    // MARK: -
    //--------------------------------------------------------------
    
    func numberOfRowsInSection(_ section: Int) -> Int{
        var numberOfRowsInSection_ = 0
        
        let key_section = retrieveSortedKeyForSection(section)
        
        if let rowDataArray_ = dictionaryTableData[key_section] {
            numberOfRowsInSection_ = rowDataArray_.count
        }else{
            appDelegate.log.error("111 dictionaryTableData[key_section:\(key_section)] is nil or not Array<String> ")
        }

        
        return numberOfRowsInSection_
    }
    
    
    func rowDataArrayForSection (_ section: Int) -> Array<Any>{
        
        var rowDataArray_ : Array<Any> = []
        
        let key_section = retrieveSortedKeyForSection(section)
        
        //Array
        if let rowDataArray = dictionaryTableData[key_section]{
            rowDataArray_ = rowDataArray
            
        }else{
            appDelegate.log.error("ERROR222 dictionaryTableData[key_section:\(key_section)] is nil")
        }
        return rowDataArray_
    }
    
    
    //--------------------------------------------------------------
    // MARK: - SECTION INDEX
    // MARK: -
    //--------------------------------------------------------------
    private lazy var arrayFirstLetters : [String] = {

        var arrayFirstLetters_ = [String]()
        arrayFirstLetters_.append(UITableViewIndexSearch)
        
        for key: String in self.keysArraySorted{
            if key.length >= 1 {
                let firstLetter_ = key.substr(0, length: 1)
                arrayFirstLetters_.append(firstLetter_)
            }else{
                appDelegate.log.error("key.length >= 1 Failed:'\(key)'")
            }
        }
        return arrayFirstLetters_
    }()
    
    public func sectionIndexTitles() -> [String]? {

        //---------------------------------------------------------
        //KEY in dictionaryTableData_Grouped in subclass
        //---------------------------------------------------------
        if let colcQuery = self.colcQuery {
            if colcQuery.colcGroupBy is COLCGroupByAZ {
                //set only for COLCSortByString
                return self.arrayFirstLetters
            }else{
                //NOISY appDelegate.log.error("colcQuery.colcGroupBy is NOT COLCGroupByAZ - sectionIndexTitles >> nil")
                return nil
            }
            
        }else{
            appDelegate.log.debug("self.colcQuery is nil >> sectionIndexTitles is nil")
            return nil
        }
    }
    
    public func sectionForSectionIndexTitle(title: String, at index: Int) -> Int {
        var indexFound_ = -1
        
        if self.arrayFirstLetters.count > 0{
            for index in 0..<self.arrayFirstLetters.count {
                
                let firstLetter : String = self.arrayFirstLetters[index]
                
                if(title == firstLetter){
                    //found
                    indexFound_ = index
                    break
                }else{
                    //no match - skip to next
                }
            }
            if(-1 == indexFound_){
                
                appDelegate.log.error("sectionForSectionIndexTitle indexFound_ NOT FOUND for:title:'\(title)'")
                
            }else{
                //print("FOUND")
            }
        }else{
            appDelegate.log.error("sectionForSectionIndexTitle arrayFirstLetters.count == 0")
        }
        
        return indexFound_
    }
    
}
