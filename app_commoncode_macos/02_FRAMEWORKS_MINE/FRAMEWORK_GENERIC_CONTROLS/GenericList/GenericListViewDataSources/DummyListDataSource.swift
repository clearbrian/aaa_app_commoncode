//
//  DummyGenericListViewDataSourceProtocol.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 23/01/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation



//use EmptyListDataSource - else you get 26 row filled in incorrectly for TFL
class DummyListDataSource: ParentGenericListViewDataSource{
    
    //Any is String
    //returns Dictionary<String, Array<String>>
    override var dictionaryTableData : Dictionary<String, Array<Any>>{
        return [
            "A" : ["R0", "R1", "R3"],
            "B" : ["R0", "R1", "R3"],
            "C" : ["R0", "R1", "R3"],
            "D" : ["R0", "R1", "R3"],
            "E" : ["R0", "R1", "R3"],
            "F" : ["R0", "R1", "R3"],
            "G" : ["R0", "R1", "R3"],
            "H" : ["R0", "R1", "R3"],
            "I" : ["R0", "R1", "R3"],
            "J" : ["R0", "R1", "R3"],
            "K" : ["R0", "R1", "R3"],
            "L" : ["R0", "R1", "R3"],
            "M" : ["R0", "R1", "R3"],
            "N" : ["R0", "R1", "R3"],
            "O" : ["R0", "R1", "R3"],
            "P" : ["R0", "R1", "R3"],
            "Q" : ["R0", "R1", "R3"],
            "R" : ["R0", "R1", "R3"],
            "S" : ["R0", "R1", "R3"],
            "T" : ["R0", "R1", "R3"],
            "U" : ["R0", "R1", "R3"],
            "V" : ["R0", "R1", "R3"],
            "W" : ["R0", "R1", "R3"],
            "X" : ["R0", "R1", "R3"],
            "Y" : ["R0", "R1", "R3"],
            "Z" : ["R0", "R1", "R3"],
        ]
    }
}

