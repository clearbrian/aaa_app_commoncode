//
//  COLCPlace.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 15/03/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class COLCPlace: ParentMappable, TableDisplayable, Alertable, MapPinnable, COLCQueryable, COLCSortable, CustomStringConvertible{
    
    
    var name : String?
    
    
    override init(){
        super.init()
    }
    
    //--------------------------------------------------------------
    // MARK: - ParentMappable
    // MARK: -
    //--------------------------------------------------------------

    required init?(map: Map){
        super.init(map: map)
        
    }
    
    //--------------------------------------------------------------
    //	class func newInstance(map: Map) -> Mappable?{
    //		return TFLTaxiRank()
    //	}
    //	required init?(map: Map){}
    //	private override init(){}
    //--------------------------------------------------------------
    
    override func mapping(map: Map)
    {
        
        appDelegate.log.error("SUBCLASS COLCPlace. mapping(map: Map)")
    }
    
    
    //--------------------------------------------------------------
    // MARK: - TableDisplayable
    // MARK: -
    //--------------------------------------------------------------

    func textLabelText() -> String{
        appDelegate.log.error("SUBCLASS COLCPlace. textLabelText")
        return "COLCPlace>>textLabelText"
    }
    
    func detailLabelText0() -> String?{
        appDelegate.log.error("SUBCLASS COLCPlace. detailLabelText0")
        return nil
    }
    
    func detailLabelText1() -> String?{
        appDelegate.log.error("SUBCLASS COLCPlace. detailLabelText1")
        return nil
    }
    
    func subDetailLabelText0() -> String?{
        appDelegate.log.error("SUBCLASS COLCPlace. subDetailLabelText0")
        return nil
    }
    
    func subDetailLabelText1() -> String?{
        appDelegate.log.error("SUBCLASS COLCPlace. subDetailLabelText1")
        return nil
    }

    //--------------------------------------------------------------
    // MARK: - Alertable
    // MARK: -
    //--------------------------------------------------------------
    var alertTitle: String {
        appDelegate.log.error("SUBCLASS COLCPlace. alertTitle")
        return "COLCPlace>>alertTitle"
    }
    
    var alertMessage: String {
        appDelegate.log.error("SUBCLASS COLCPlace. alertMessage")
        return "COLCPlace>>alertMessage"
    }
    
    //--------------------------------------------------------------
    // MARK: - MapPinnable
    // MARK: -
    //--------------------------------------------------------------
    func markerTitle() -> String{
        //appDelegate.log.error("SUBCLASS COLCPlace. markerTitle")
        return descriptionLineMain
    }
    
    func markerSnippet() -> String{
        //appDelegate.log.error("SUBCLASS COLCPlace. markerSnippet")
        return descriptionOneLine
    }
    

    
    var markerImageName: String? {
        appDelegate.log.error("SUBCLASS COLCPlace. markerImageName")
        return nil
    }
    
    var markerImage: UIImage? {
        appDelegate.log.error("SUBCLASS COLCPlace. markerImage")
        return nil
    }
    
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - Desc
    //--------------------------------------------------------------
    //used by marker but mostly in TFLApiPlace
    var descriptionLineMain : String {
        return "\(Safe.safeString(self.name))"
    }
    
    //used for summary in alert
    //marker snipper
    var descriptionOneLine : String {
        return "\(Safe.safeString(self.name))]"
    }
    
    
    
    
    
    //--------------------------------------------------------------
    // MARK: - MapPinnable >> CoreLocatable
    // MARK: -
    //--------------------------------------------------------------
    var clLocation: CLLocation? {
        appDelegate.log.error("SUBCLASS COLCPlace. clLocation")
        return nil
    }
    
        
    //--------------------------------------------------------------
    // MARK: - CoreLocatable
    // MARK: -
    //--------------------------------------------------------------
    func distanceTo(otherLocation: CLLocation) -> CLLocationDistance {
        var distanceInMeters: CLLocationDistance =  GoogleMapsDistance.NotFound
        
        if let clLocation = self.clLocation {
            distanceInMeters = GoogleMapsDistance.distanceInMeters(from: clLocation,
                                                                   to: otherLocation)
            
        }else{
            appDelegate.log.error("self.clLocation is nil")
        }
        
        return distanceInMeters
    }
    
    func distanceToCurrentLocation() -> CLLocationDistance {
        var distanceInMeters: CLLocationDistance = GoogleMapsDistance.NotFound
        
        if let clLocation = self.clLocation {
            distanceInMeters = GoogleMapsDistance.distanceInMetersFromCurrentLocation(to: clLocation)
            
        }else{
            appDelegate.log.error("self.clLocation is nil")
        }
        
        return distanceInMeters
    }
    
    var distanceToCurrentLocationFormatted: String{
        var distanceToCurrentLocationFormatted_ = ""
        
        let distanceToCurrentLocation : CLLocationDistance = self.distanceToCurrentLocation()
        
        if distanceToCurrentLocation == GoogleMapsDistance.NotFound{
            distanceToCurrentLocationFormatted_ = ""
        }else{
            
            if distanceToCurrentLocation > 1000.0{
                //KM
                let distInKM = distanceToCurrentLocation/1000.0
                let distInKM_2DecimalPlacesString = COLCFormatter.formatToTwoDecimalPlaces(doubleValue: distInKM)
                
                distanceToCurrentLocationFormatted_ = "\(distInKM_2DecimalPlacesString) km"
            }else{
                //METERS
                let distanceToCurrentLocation2DecimalPlaces = COLCFormatter.formatToTwoDecimalPlaces(doubleValue: distanceToCurrentLocation)
                distanceToCurrentLocationFormatted_ = "\(distanceToCurrentLocation2DecimalPlaces) m"
            }
        }
        
        return distanceToCurrentLocationFormatted_
    }
    
    //--------------------------------------------------------------
    // MARK: - CustomStringConvertible
    // MARK: -
    //--------------------------------------------------------------
    public var description: String {
        appDelegate.log.error("SUBCLASS Alertable. CustomStringConvertible")
        return "COLCPlace"
    }
    
    
    //--------------------------------------------------------------
    // MARK: - COLCQueryable
    // MARK: -
    //--------------------------------------------------------------
    //class var colcQueryCollectionForType : COLCQueryCollection
    
    fileprivate static let propertyName_clLocation = "clLocation"
    //    fileprivate static let propertyName_name = "name"

    func value(forPropertyName propertyName: String) -> Any?{
        
        switch propertyName{
        case COLCPlace.propertyName_clLocation:
            return self.clLocation
            
//        case COLCPlace.propertyName_name:
//            return self.name
            
        default:
            appDelegate.log.error("UNHANDLED PROPERTY: value:forPropertyName:\(propertyName) - may be in subclass - COLCPLACE")
            return nil
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - FACTORY COLCQueries
    // MARK: -
    //--------------------------------------------------------------
//    class func colcQuery_sortByNearest() -> COLCQuery{
//        //table header title
//        let colcSort = COLCSortByCLLocation(title: "Nearest Ranks")
//        
//        //action sheet title
//        let colcQuery = COLCQuery(title: "Show Nearest Ranks", colcSort: colcSort)
//        colcQuery.showSearchBar = false
//        return colcQuery
//    }
    
    //moved down to commonName
    //    class func colcQuery_sortByName() -> COLCQuery{
    //        
    //        let colcSort = COLCSortByString(title: "Sorted By Name", propertyName: COLCPlace.propertyName_name)
    //        let colcQuery = COLCQuery(title: "Name", colcSort: colcSort)
    //        colcQuery.showSearchBar = true
    //        return colcQuery
    //    }
    
//    class func colcQuery_sortByDownloaded() -> COLCQuery{
//        
//        let colcSort = COLCSortNone(title: "Downloaded")
//        let colcQuery = COLCQuery(title: "Downloaded", colcSort: colcSort)
//        colcQuery.showSearchBar = false
//        return colcQuery
//    }
    
    
    
    
    
}
