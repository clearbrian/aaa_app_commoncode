//
//  COLCSortByString.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 17/03/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

//--------------------------------------------------------------
// MARK: - COLCSortByString
// MARK: -
//--------------------------------------------------------------
/*
 //----------------------------------------------------------------------------------------
 //PLAYGROUND
 //----------------------------------------------------------------------------------------
 /Users/gbxc/Documents/00_ME/00_BITBUCKET_COLC/app_apptemplatesandnotes/00_CODE/AppWithCodeTemplates_iOS_Swift3/PLAYGROUNDS/Swift3_String_sorted.playground
 
 //----------------------------------------------------------------------------------------
 //OK - LOCALISED - CASE SENSITIVE
 //----------------------------------------------------------------------------------------
 let array7 = ["Cafe B","Café C","Café A", "2 Cafe B","3 Café C","1 Café A", "cafe B","café C","café A"]
 let sorted7 = array7.sorted{$0.localizedCompare($1) == .orderedAscending}
 print(sorted7)
 print("sorted7: \(sorted7)")
 //numbers before letters
 //A and a together
 //é and e the same
 //sorted7: ["1 Café A", "2 Cafe B", "3 Café C", "café A", "Café A", "cafe B", "Cafe B", "café C", "Café C"]
 
 
 //----------------------------------------------------------------------------------------
 //BEST - LOCALISED - CASE INSENSITIVE AaBbCc and Numbers before letters
 //----------------------------------------------------------------------------------------
 let array6 = ["Cafe B","Café C","Café A", "2 Cafe B","3 Café C","1 Café A", "cafe B","café C","café A", "cafe B","cafe C","cafe A", "Dafe B","Dafé D","Dafé A", "2 Dafe B","3 Dafé D","1 Dafé A", "dafe B","dafé D","dafé A", "dafe B","dafe D","dafe A"]
 
 let sorted6 = array6.sorted{$0.localizedCaseInsensitiveCompare($1) == .orderedAscending}
 print("sorted6: \(sorted6)")
 //----------------------------------------------------------------------------------------
 //numbers before letters
 //A and a together
 //é and e the same
 //----------------------------------------------------------------------------------------
 sorted6:
 ["1 Café A", "1 Dafé A",               // .orderedAscending: numbers before letters
 "2 Cafe B", "2 Dafe B",
 "3 Café C", "3 Dafé D",
 
 "cafe A", "Café A", "café A",
 "Cafe B", "cafe B", "cafe B",
 "cafe C", "café C", "Café C",
 "dafe A", "Dafé A", "dafé A",
 
 "Dafe B", "dafe B", "dafe B",          // first letter D, d after c
 "dafe D", "Dafé D", "dafé D"]
 //----------------------------------------------------------------------------------------
 */

class COLCSortByString : COLCSort{
    
    //----------------------------------------------------------------------------------------
    //setting for String sorting
    //----------------------------------------------------------------------------------------
    
    //---------------------------------------------------------------
    //localizedCaseInsensitiveCompare is the best see below
    //---------------------------------------------------------------
    var isCaseInsensitive = true
    var isLocalized = true
    
    //BEST FOR Taxi Rank names - localizedCaseInsensitiveCompare - see comment below
    convenience init(title: String, propertyName: String)
    {
        self.init(title:title, propertyName: propertyName, isSorted: true, isAscending: true, isCaseSensitive: true)
        self.isLocalized = true
    }
    
    
    //--------------------------------------------------------------
    // MARK: - sortResults - overidden calls class method sortByString
    // MARK: -
    //--------------------------------------------------------------
    //protocol COLCQueryable : COLCSortable, CoreLocatable
    override func sortResults(arrayToSort: [COLCQueryable]) -> [COLCQueryable]{
        
        //return unsorted if error
        var arraySorted = arrayToSort
        
        if self.isSorted{
            //[CoreLocatable] >> [COLCSortable]
            arraySorted = COLCSortByString.sortByString(propertyName      : self.propertyName,
                                                        isAscending       : self.isAscending,
                                                        isCaseInsensitive : self.isCaseInsensitive,
                                                        arrayToSort       : arrayToSort)
            
        }else{
            appDelegate.log.error("COLCSortByNearest isSorted is false - should only be false for COLCSortNone")
            //use unsorted
            //arraySorted = arrayToSort
        }
        return arraySorted
    }
    
    override func sortResults(arrayToSort: [COLCQueryable], filterByFirstLetter firstLetter: String) -> [COLCQueryable]{
        //WRONG - return unsorted if error - adds ALL then filtered ones to end so you get too amany
        //var arraySortedAndFiltered = arrayToSort
        var arraySortedAndFiltered = [COLCQueryable]()

        
        if self.isSorted{
            
            //[CoreLocatable] >> [COLCSortable]
            let arraySorted = COLCSortByString.sortByString(propertyName  : self.propertyName,
                                                        isAscending       : self.isAscending,
                                                        isCaseInsensitive : self.isCaseInsensitive,
                                                        arrayToSort       : arrayToSort)
            
            for colcQueryable in arraySorted{
                //find value were sorting on e,g string in commonName
                if let valueString = colcQueryable.value(forPropertyName: self.propertyName) as? String{
                    if valueString.hasPrefix(firstLetter){
                        arraySortedAndFiltered.append(colcQueryable)
                    }else{
                        //
                    }
                }else{
                    appDelegate.log.error("valueString for propertyName:'\(self.propertyName)' is nil")
                }
            }
        }else{
            appDelegate.log.error("COLCSortByNearest isSorted is false - should only be false for COLCSortNone")
            //use unsorted
            //arraySorted = arrayToSort
        }
        return arraySortedAndFiltered
    }
    
    
    
    override func arrayFirstLetters(sortedArray: [COLCQueryable]) -> [String]?{
        var arrayFirstLetters_ = [String]()
        
        
        //unique letters - and not all a.z only ones in the list
        var firstLetterSet = Set<String>()
        
        for colcQueryable: COLCQueryable in sortedArray{
            //varied based on sort e.g. commonName
            if let valueString = colcQueryable.value(forPropertyName: self.propertyName) as? String{
                if valueString.length > 1{
                    let firstLetter_ = valueString.substr(0, length: 1)
                    firstLetterSet.insert(firstLetter_)
                }else{
                    appDelegate.log.error("valueString too short:'\(valueString)'")
                }
            }else{
                appDelegate.log.error("$0.value for propertyName:'\(propertyName)' is nil")
            }
        }
        
        arrayFirstLetters_ = Array(firstLetterSet)

        let arrayFirstLettersSorted_ = arrayFirstLetters_.sorted()
        return arrayFirstLettersSorted_
    }
    
    
    
    
//    public override func sectionIndexTitles() -> [String]?{
//        return nil
//    }
    
    //--------------------------------------------------------------
    // MARK: - sortByString - CLASS METHOD
    // MARK: -
    //--------------------------------------------------------------
    
    class func sortByString(propertyName: String, isAscending: Bool, isCaseInsensitive: Bool, arrayToSort: [COLCQueryable]) -> [COLCQueryable]{
        
        let sortedArray = arrayToSort.sorted {
            var sorted_ = false
            
            //$0 : COLCQueryable
            //$1 : COLCQueryable
            if let valueString0 = $0.value(forPropertyName: propertyName) as? String{
                if let valueString1 = $1.value(forPropertyName: propertyName) as? String{
                    
                    //----------------------------------------------------------------------------------------
                    //SORT
                    //----------------------------------------------------------------------------------------
                    //Case Insensitive - ["Abc", "def", "Ghi"]
                    //---------------------------------------------------------
                    if isAscending{
                        if isCaseInsensitive{
                            //BEST FOR taxi rank names - see comment above method above
                            sorted_ = valueString0.localizedCaseInsensitiveCompare(valueString1) == .orderedAscending
                        }else{
                            sorted_ = valueString0.localizedCompare(valueString1) == .orderedAscending
                        }
                        
                    }else{
                        //---------------------------------------------------------
                        //localizedCaseInsensitiveCompare : orderedDescending
                        //---------------------------------------------------------
                        if isCaseInsensitive{
                            sorted_ = valueString0.localizedCaseInsensitiveCompare(valueString1) == .orderedDescending
                        }else{
                            //Case Sensitive: .orderedDescending
                            sorted_ = valueString0.localizedCompare(valueString1) == .orderedDescending
                        }
                    }
                    //----------------------------------------------------------------------------------------
                }else{
                    appDelegate.log.error("$1.value for propertyName\(propertyName) is nil")
                }
            }else{
                appDelegate.log.error("$0.value for propertyName:'\(propertyName)' is nil")
            }
            return sorted_
        }//sorted{}
        return sortedArray
    }
}
