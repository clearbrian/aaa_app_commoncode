//
//  COLCSortNone.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 17/03/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation


//--------------------------------------------------------------
// MARK: - COLCSortNone
// MARK: -
//--------------------------------------------------------------
class COLCSortNone : COLCSort{
    
    convenience init(title : String)
    {
        self.init(title:title,propertyName: "", isSorted: false, isAscending: false, isCaseSensitive: false)
        
    }
    
    //return results UNSORTED
    //protocol COLCQueryable : COLCSortable, CoreLocatable
    override func sortResults(arrayToSort: [COLCQueryable]) -> [COLCQueryable]{
        return arrayToSort
    }
}
