//
//  COLCQueryable.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 15/03/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
//--------------------------------------------------------------
// MARK: - COLCQueryable
// MARK: -
//--------------------------------------------------------------
//Required Methods for object you can query
//--------------------------------------------------------------
/*
                      class TFLApiPlace: ParentMappable, TableDisplayable, Alertable, MapPinnable, COLCQueryable, CustomStringConvertible{
 class TFLApiPlaceJamCam  : TFLApiPlace{
 class TFLApiPlaceTaxiRank: TFLApiPlace{
 
*/
//--------------------------------------------------------------
//sort by nearest
protocol COLCQueryable : COLCSortable, CoreLocatable{
    
    //Maps json property name > Type
    func value(forPropertyName propertyName: String) -> Any?
    
    //Also required not protocol changes it to static just just here as notes
    //class var colcQueryCollectionForType : COLCQueryCollection
}
