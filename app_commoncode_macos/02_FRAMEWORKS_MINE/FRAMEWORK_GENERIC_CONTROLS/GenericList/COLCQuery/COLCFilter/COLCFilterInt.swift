//
//  COLCFilterInt.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 15/03/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

//Swift book - Int varies by platform 32/64
class COLCFilterInt: COLCFilterNumber{
    
    
    var searchInt: Int
    
    init(propertyName : String,
         searchInt : Int,
         colcFilterNumberComparison : COLCFilterNumberComparison)
    {
        self.searchInt = searchInt
        
        super.init(propertyName: propertyName, colcFilterNumberComparison: colcFilterNumberComparison)
        
    }
    
    //--------------------------------------------------------------
    // MARK: - MATCH rules for Int
    // MARK: -
    //--------------------------------------------------------------
    override func filterMatch(colcQueryable: COLCQueryable) -> Bool{
        var filterMatch = false
        
        if let propertyInt = colcQueryable.value(forPropertyName: self.propertyName) as? Int {
            
            //----------------------------------------------------------------------------------------
            //COLCFilterNumberComparison
            //----------------------------------------------------------------------------------------
            switch self.colcFilterNumberComparison{
            case .equals:
                if propertyInt == self.searchInt{
                    appDelegate.log.debug("[filterMatch? equals] propertyInt:'\(propertyInt)' == searchInt:'\(self.searchInt)' - MATCH")
                    filterMatch = true
                }else{
                    appDelegate.log.debug("[filterMatch? equals] propertyInt:'\(propertyInt)' == searchInt:'\(self.searchInt)' - NO MATCH")
                    filterMatch = false
                }
                
            case .greaterThan:
                if propertyInt > self.searchInt{
                    appDelegate.log.debug("[filterMatch? greaterThan] propertyInt:'\(propertyInt) > searchInt:'\(self.searchInt)' - MATCH")
                    filterMatch = true
                }else{
                    appDelegate.log.debug("[filterMatch? greaterThan] propertyInt:'\(propertyInt)' > searchInt:'\(self.searchInt)' - NO MATCH")
                    filterMatch = false
                }
                
            case .greaterThanOrEquals:
                if propertyInt >= self.searchInt{
                    appDelegate.log.debug("[filterMatch? greaterThanOrEquals] propertyInt:'\(propertyInt)' >= searchInt:'\(self.searchInt)' - MATCH")
                    filterMatch = true
                }else{
                    appDelegate.log.debug("[filterMatch? greaterThanOrEquals] propertyInt:'\(propertyInt)' >= searchInt:'\(self.searchInt)' - NO MATCH")
                    filterMatch = false
                }
                
            case .lessThan:
                if propertyInt < self.searchInt{
                    appDelegate.log.debug("[filterMatch? lessThan] propertyInt:'\(propertyInt)' < searchInt:'\(self.searchInt)' - MATCH")
                    filterMatch = true
                }else{
                    appDelegate.log.debug("[filterMatch? lessThan] propertyInt:'\(propertyInt)' < searchInt:'\(self.searchInt)' - NO MATCH")
                    filterMatch = false
                }
                
            case .lessThanOrEquals:
                if propertyInt <= self.searchInt{
                    appDelegate.log.debug("[filterMatch? lessThanOrEquals] propertyInt:'\(propertyInt)' <= searchInt:'\(self.searchInt)' - MATCH")
                    filterMatch = true
                }else{
                    appDelegate.log.debug("[filterMatch? lessThanOrEquals] propertyInt:'\(propertyInt)' <= searchInt:'\(self.searchInt)' - NO MATCH")
                    filterMatch = false
                }
            }
            //----------------------------------------------------------------------------------------
        }else{
            appDelegate.log.error("[filterMatch? - Int] colcQueryable.value(forPropertyName: colcFilter.propertyName:'\(propertyName)' is nil OR Not Int")
        }
        return filterMatch
    }
}
