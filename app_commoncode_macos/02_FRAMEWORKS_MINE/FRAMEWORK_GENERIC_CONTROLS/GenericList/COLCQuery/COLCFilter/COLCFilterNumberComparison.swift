//
//  COLCFilterNumberComparison.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 15/03/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

enum COLCFilterNumberComparison{
    case equals
    case greaterThan
    case greaterThanOrEquals
    case lessThan
    case lessThanOrEquals
}
