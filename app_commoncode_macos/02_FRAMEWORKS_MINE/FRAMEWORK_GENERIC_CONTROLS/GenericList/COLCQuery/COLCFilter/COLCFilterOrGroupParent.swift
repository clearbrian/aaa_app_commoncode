//
//  COLCFilterOrGroupParent.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 15/03/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

//--------------------------------------------------------------
// MARK: - common parent of COLCFilter or a group of them COLCFilterGroup
// MARK: -
//--------------------------------------------------------------

class COLCFilterOrGroupParent{
    //subclasses should implement
    //func filterMatch(colcQueryable: COLCQueryable) -> Bool
}
