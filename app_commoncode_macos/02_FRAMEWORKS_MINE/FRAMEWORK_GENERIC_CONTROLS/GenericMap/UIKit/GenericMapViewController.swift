//
//  GenericMapViewController.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 18/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//
import Foundation
import UIKit
import GoogleMaps

// TODO: - generic Google Map need apple map and mapbox version
class GenericMapViewController: ParentViewController, CurrentLocationManagerDelegate, GMSMapViewDelegate {
    
    let emailManager = EmailManager()
    
    //delegate may send current position and map jumps back
    var userDidChangeCameraPosition : Bool = false
    var mapMovedOkToCallWS = true
    
    @IBOutlet weak var gmsMapView: GMSMapView!
    
    // TODO: - use - change in IB
    @IBOutlet weak var labelDescription0: UILabel!
    @IBOutlet weak var labelDescription1: UILabel!
    
    //may be nil
    @IBOutlet weak var colcDGActivityIndicatorView: COLCDGActivityIndicatorView!
    
    
    //-------------------------------------------------------------------
    //Move map triggers webservice
    //-------------------------------------------------------------------
    var currentMapLocation: CLLocation?
    //to stop ws called too much get dist in between
    var prevMapLocation: CLLocation?
    
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - TAP ON MARKER
    //--------------------------------------------------------------
    
    // TODO: - make TFLAPIPLACE GEneric
    //didTapOnMarker
    var selectedTFLApiPlace : TFLApiPlace?
    
    //----------------------------------------------------------------------------------------
    //WRONG - do in app delegate or asap - need to generate map pin images for 466 stations - may not be ready in time here - cause Map error
    //    let tflGeoJSONManager = TFLGeoJSONManager()
    //----------------------------------------------------------------------------------------
    
    
    
    //--------------------------------------------------------------
    // MARK: - VIEW DID LOAD
    // MARK: -
    //--------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Map"
        
        
        
        configureMapView()
        styleMap()
        
        //default to traf sq else zoomed right out
        //moveToCurrentLocation()
        
        //---------------------------------------------------------
        //draw on map
        //---------------------------------------------------------
        //CALL IN SUBCLASS addSelectedToMap()
        //---------------------------------------------------------
        
    }
    

    //--------------------------------------------------------------
    // MARK: - SHOW/HIDE
    // MARK: -
    //--------------------------------------------------------------
    
    func showLoadingOnMain(){
        if Thread.isMainThread{
            showLoading()
        }else{
            DispatchQueue.main.async{
                self.showLoading()
            }
        }
    }
    
    private func showLoading(){
        
        if let _ = self.colcDGActivityIndicatorView {
            self.colcDGActivityIndicatorView?.showLoadingOnMain()
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
 
        }else{
            appDelegate.log.error("self.colcDGActivityIndicatorView is nil")
        }
    }
    
    func hideLoadingOnMain(){
        if Thread.isMainThread{
            hideLoading()
        }else{
            DispatchQueue.main.async{
                self.hideLoading()
            }
        }
    }
    
    private func hideLoading(){
        if let _ = self.colcDGActivityIndicatorView {
            self.colcDGActivityIndicatorView?.hideLoadingOnMain()
            UIApplication.shared.isNetworkActivityIndicatorVisible = false

        }else{
            appDelegate.log.error("self.colcDGActivityIndicatorView is nil")
        }
    }
    
    //--------------------------------------------------------------
    
    func styleMap(){
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "mapStyle", withExtension: "json") {
                self.gmsMapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
    }
    
    override func viewDidAppear(_ animated: Bool){
        super.viewDidAppear(animated)
        //default to traf sq else zoomed right out
        moveToCurrentLocation()
    }
    
    func configureMapView(){
        self.gmsMapView.delegate = self
        self.gmsMapView.isMyLocationEnabled = true
        self.gmsMapView.isBuildingsEnabled = true
        self.gmsMapView.isTrafficEnabled = false
        
        self.gmsMapView.settings.compassButton = true;
        self.gmsMapView.settings.myLocationButton = true;
        self.gmsMapView.settings.indoorPicker = true;
        
        //needed for when user taps My location button twice it rotate in direction they are facing
        self.gmsMapView.settings.rotateGestures = true;
    }
    
    //--------------------------------------------------------------
    // MARK: - MAP
    // MARK: -
    //--------------------------------------------------------------
    func addSelectedToMap(){
        if let selectedTFLApiPlace = self.selectedTFLApiPlace {
            if let gmsMarker = selectedTFLApiPlace.gmsMarker {
                gmsMarker.map = self.gmsMapView
                
            }else{
                appDelegate.log.error("tflApiPlace.gmsMarker  is nil")
            }
        }else{
            appDelegate.log.error("self.selectedTFLApiPlace is nil")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func buildMarker(latitude: Double, longitude: Double, stationId: String, title: String, userData: Any?) -> GMSMarker{
        //----------------------------------------------------------------------------------------
        //ADD MARKER
        //----------------------------------------------------------------------------------------
        
        let clLocation = CLLocation.init(latitude: CLLocationDegrees(latitude),
                                         longitude: CLLocationDegrees(longitude))
        
        
        //location is ok
        let marker = GMSMarker(position: clLocation.coordinate)
        
        marker.title = title
        //marker.snippet = markerSnippet()
        
        
        marker.userData = userData
        
        //---------------------------------------------------------
        //ICON - from dict of images
        //---------------------------------------------------------
        //IF YOU GET CoreGraphics errors - make sure you called = tflGeoJSONManager() asap - pref in appD - it generates pins for 
        //Dont call it in this class as ivar - pins may not be generated fast enough
        if let imageStation = appDelegate.tflGeoJSONManager.dictionaryStationImages[stationId] {
            marker.icon = imageStation
        }else{
            appDelegate.log.error("dictionaryStationImages[name:'\(stationId)'] is nil")
            
            
            if let imagePin = UIImage.init(named: "Roundel") {
                marker.icon = imagePin
            }else{
                //appDelegate.log.error("UIImage.init(named: imageName:'\(imageName)') is nil")
                marker.icon = nil
            }
        }
        //------------------------------------------------------------------------------------------------
        
        
        //------------------------------------------------------------------------------------------------
        marker.map = self.gmsMapView
        //------------------------------------------------------------------------------------------------
        return marker
    }
    
    //--------------------------------------------------------------
    // MARK: - CurrentLocationManagerDelegate
    // MARK: -
    //--------------------------------------------------------------
    func currentLocationUpdated(currentLocationManager: CurrentLocationManager, currentLocation: CLLocation, distanceMetersDouble : CLLocationDistance){
        //log.error("TODO currentLocationUpdated currentLocation:\n\(currentLocation)")
        
        //self.gmsMapView.animateToLocation(location: currentLocation, atZoom: GoogleMapsZoomLevel.zoomlevel17_STREET)
        
        moveToCurrentLocation()
    }
    
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - LOCATION/MAP
    // MARK: -
    //--------------------------------------------------------------
    
    //nearbysearch is triggered AFTER move finished in idleAtCameraPosition:
    func moveToCurrentLocation(){
        if self.userDidChangeCameraPosition{
            //NOISY
            //self.log.debug("moveToCurrentLocation - userDidChangeCameraPosition is true DONT move to new currentLocation")
        }else{
            //NOISY
            //self.log.debug("moveToCurrentLocation - userDidChangeCameraPosition is false DO move to new currentLocation")
            //map at default so ok to move to current location
            
            if let currentLocation = appDelegate.currentLocationManager.currentUserPlace.clLocation {
                //NOISY: self.log.debug("LOC: [moveToCurrentLocation:]about to call  moveMapToCLLocation(currentLocation:\(currentLocation))")
                moveMapToCLLocation(currentLocation)
                
            }else{
                self.log.error("currentUserPlace.clLocation is nil")
            }
            
            //            moveToCurrentLocationAlways()
        }
    }
    
    //    //called when we change 2d/3d and FIRST tiem map show
    //    func moveToCurrentLocationAlways(){
    //        if let currentLocation = appDelegate.currentLocationManager.currentUserPlace.clLocation {
    //            //NOISY: self.log.debug("LOC: [moveToCurrentLocation:]about to call  moveMapToCLLocation(currentLocation:\(currentLocation))")
    //            moveMapToCLLocation(currentLocation)
    //
    //        }else{
    //            self.log.error("currentUserPlace.clLocation is nil")
    //        }
    //    }
    
    
    //called every time a new device location is recieved - UNLESS user manually moves the map then thats disabled
    //called when you tap on a row in the table
    func moveMapToCLLocation(_ clLocation : CLLocation){
        //------------------------------------------------------------------------------------------------
        if clLocation.isValid{
            
            
            //-----------------------------------------------------------------------------------
            //zoom in
            //-----------------------------------------------------------------------------------
            //zoom in if very far out - when app starts can be a zoom 4.0
            var zoom : Float = 16.0
            if let gmsMapView = self.gmsMapView {
                zoom = gmsMapView.camera.zoom
                    
                if zoom < 16{
                    zoom = 16
                }
                
            }
            
            //----------------------------------------------------------------------------------------
            //2D
            //----------------------------------------------------------------------------------------
            //    let cameraPosition = GMSCameraPosition.camera(withLatitude: clLocation.coordinate.latitude,
            //                                                  longitude:clLocation.coordinate.longitude,
            //                                                  zoom:zoom)
            //----------------------------------------------------------------------------------------
            //var mapView = GMSMapView.mapWithFrame(CGRectZero, camera:camera)
            //NOISY self.log.warning("LOC: animateToCameraPosition( cameraWithLatitude:[\(clLocation.coordinate.latitude),\(clLocation.coordinate.longitude)")
            //----------------------------------------------------------------------------------------
            
            //----------------------------------------------------------------------------------------
            //3D or 2D
            //----------------------------------------------------------------------------------------
            //            //flat
            //   (51.205, -1.690) bearing:0.000 zoomLevel:4.935 viewingAngle:0.000
            //   3d
            //   didChangeCameraPosition:GMSCameraPosition 0x170e54490:
            //   target:(51.509, -0.072) bearing:348.488 zoomLevel:17.463 viewingAngle:65.000
            //----------------------------------------------------------------------------------------
            //default flat
            var viewingAngle_: Double = 0.0
            
            viewingAngle_ = 0.0
            
            
            if let gmsMapView = self.gmsMapView {
                let cameraPosition = GMSCameraPosition.camera(withTarget: clLocation.coordinate,
                                                              zoom: zoom,
                                                              bearing: gmsMapView.camera.bearing,
                                                              viewingAngle: viewingAngle_)
                
                
                
                //----------------------------------------------------------------------------------------
                gmsMapView.animate(to: cameraPosition)
            }
            
            
            //----------------------------------------------------------------------------------------
            //triggered AFTER move complete in idleAtCameraPosition:
            //self.nearbySearch(currentLocation)
            
            
            //-----------------------------------------------------------------------------------
        }else{
            self.log.debug("LOC: [moveMapToCLLocation(clLocation:\(clLocation))] clLocation.isValid is false - dont call animateToCameraPosition")
        }
    }
    
    func currentLocationFailed(currentLocationManager: CurrentLocationManager, error: Error?)
    {
        self.log.error("currentLocationFailed: error:\(String(describing: error)) - MAP MAY BE STUCK")
    }
    
    
    
    func currentHeadingUpdated(currentLocationManager: CurrentLocationManager, newHeading: CLHeading){
        print("currentHeadingUpdated: newHeading:\r\(newHeading)")
    }
    
    
    
    
    
    
    
    
    @IBAction func switch3D_ValueChanged(_ switch3D: UISwitch) {
        
        //dont use - wont change if user has changed map
        //moveToCurrentLocation()
        //moveToCurrentLocationAlways()
        
        //set after map set or moves
        if let currentGMSCameraPosition = self.currentGMSCameraPosition {
            moveMapToCLLocation(CLLocation.init(latitude: currentGMSCameraPosition.target.latitude, longitude: currentGMSCameraPosition.target.longitude))
            
        }else{
            appDelegate.log.error("self.currentGMSCameraPosition is nil")
        }
    }
    
    
    //--------------------------------------------------------------
    // MARK: - GMSMapViewDelegate - didTap marker
    // MARK: -
    //--------------------------------------------------------------
    public func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool{
        
        appDelegate.log.debug("didTapmarker:\(String(describing: marker.title))")
        
        //TODO: - put the pop up as text fields under the map view with button VIEW
        //does app doesnt display popup? - no > mapView should
        
        if let tflApiPlace = marker.userData as? TFLApiPlace {
            showAlertOptions(tflApiPlace)
            //dont show popup
            return true;
        }else{
            appDelegate.log.error("self.selectedTFLApiPlace is nil")
            // show popup
            //return false;
            return true; //name is in png for station
        }
    }
    
    func showAlertOptions(_ tflApiPlace : TFLApiPlace){
        
        self.selectedTFLApiPlace = tflApiPlace
        
        let alertActionCancel = UIAlertAction.alertActionCancel
        //----------------------------------------------------------------------------------------
        let alertActionDirections = UIAlertAction(title: "Directions", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            
            self.performSegue(withIdentifier: self.segueTo_TFLApiPlaceMapDirectionsViewController, sender: nil)
            
        }
        
        let alertActionStreetView = UIAlertAction(title: "StreetView", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            self.performSegue(withIdentifier: self.segueTo_TFLApiPlaceStreetViewController, sender: nil)
            
        }
        
        //---------------------------------------------------------
        //Watch - Jam Cams only
        //---------------------------------------------------------
        let alertActionWatchMovie = UIAlertAction(title: "Watch", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            self.performSegue(withIdentifier: self.segueTo_AVPlayerViewController, sender: nil)
            
        }
        
        
        //---------------------------------------------------------
        //"Report Problem"
        //---------------------------------------------------------
        let alertReportProblem = UIAlertAction(title: "Report Problem", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            if let selectedTFLApiPlace = self.selectedTFLApiPlace {
                var placeType_ = "Place"
                
                if let placeType = selectedTFLApiPlace.placeType {
                    placeType_ = placeType
                }else{
                    appDelegate.log.error("selectedTFLApiPlace.placeType is nil")
                }
                
                self.emailManager.sendEmailFromVC(self, bodyString: "User Reported Error with \(placeType_):\n['\(selectedTFLApiPlace.idSafe)':'\(Safe.safeString(selectedTFLApiPlace.commonName))']\n Please describe your problem below and we will forward it to TFL.\n")
            }else{
                appDelegate.log.error("self.selectedTFLApiPlace is nil")
            }
        }
        
        
//        //----------------------------------------------------------------------------------------
//        CLKAlertController.showSheetWithActions(vc: self, title: tflApiPlace.alertTitle,
//                                                message: tflApiPlace.alertMessage,
//                                                alertActions: [alertActionCancel, alertActionDirections, alertActionStreetView, alertReportProblem])
//        //----------------------------------------------------------------------------------------
//        
        //---------------------------------------------------------
        //show
        //---------------------------------------------------------
        var alertActions = [UIAlertAction]()
        
        if let _ = self.selectedTFLApiPlace as? TFLApiPlaceJamCam {
            alertActions = [alertActionCancel, alertActionDirections, alertActionStreetView, alertActionWatchMovie, alertReportProblem]
        }else{
            alertActions = [alertActionCancel, alertActionDirections, alertActionStreetView, alertReportProblem]
        }
        
        
        //---------------------------------------------------------
        // TODO: - only add jam cam for movi
        CLKAlertController.showSheetWithActions(          vc: self,
                                                          title: tflApiPlace.alertTitle,
                                                          message: tflApiPlace.alertMessage,
                                                          alertActions: alertActions)
        //---------------------------------------------------------
    }
    
    
    
    //OVERRIDE to keep alert in GenericMapVC
    var segueTo_TFLApiPlaceMapDirectionsViewController: String{
        return   "segueGenericMapViewControllerTOTFLApiPlaceMapDirectionsViewController"
        //return "segueTFLApiPlaceMapViewControllerTOTFLApiPlaceMapDirectionsViewController"
        //... check is not overriden in subclass as SUBCLASS may be used in storyboard so sengue name includes segue<SUBCLASSNAME>TO
    }

    
    var segueTo_TFLApiPlaceStreetViewController: String{
        return "segueTFLApiPlaceMapViewControllerTOTFLApiPlaceStreetViewController"
        //... check is not overriden in subclass as SUBCLASS may be used in storyboard so sengue name includes segue<SUBCLASSNAME>TO
    }

    var segueTo_AVPlayerViewController: String{
        //return "segueTFLApiPlaceMapViewControllerTOAVPlayerViewController"
        return "segueGenericMapViewControllerTOAVPlayerViewController"
        //... check is not overriden in subclass as SUBCLASS may be used in storyboard so sengue name includes segue<SUBCLASSNAME>TO
        
    }
    
    
    
    //--------------------------------------------------------------
    // MARK: - GMSMapViewDelegate
    // MARK: -
    //--------------------------------------------------------------
    //Called before the camera on the map changes, either due to a gesture, animation (e.g., by a user tapping on the “My Location” button)
    //or by being updated explicitly via the camera or a zero-length animation on layer.
    public func mapView(_ mapView: GMSMapView, willMove gesture: Bool){
        
        
        if gesture{
            //OK NOISY - self.log.debug("willMove gesture:\(gesture) self.userDidChangeCameraPosition = true")
            self.userDidChangeCameraPosition = true
            //if user moves map with their hand then should recall ws
            self.mapMovedOkToCallWS = true
        }else{
            
            //or map moving because device location sending notification (and user hasnt manually moved the map to block this)
            ////USER TAPPED ON MYLOCATION BUTTON resets userDidChangeCameraPosition
            //OK NOISY - self.log.debug("willMove gesture:\(gesture) - map moved because device location notif sent or user tapped MY LOC button")
        }
        
    }
    /**
     * Called repeatedly during any animations or gestures on the map (or once, if
     * the camera is explicitly set). This may not be called for all intermediate
     * camera positions. It is always called for the final position of an animation
     * or gesture.
     */
    
    var currentGMSCameraPosition : GMSCameraPosition?
    public func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition){
        
        //NOISY self.log.debug("didChangeCameraPosition:\(position)")
        //delegate may send current position and map jumps back, need flag to prevent this
        self.currentGMSCameraPosition = position
        
        
        //------------------------------------------------------------------------------------------------
        //CALLED MORE THAN ONCE - move to idleAt
        //------------------------------------------------------------------------------------------------
    }
    
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - clLocationForCLLocationCoordinate2D
    // MARK: -
    //--------------------------------------------------------------
    
    func clLocationForCLLocationCoordinate2D(_ clLocationCoordinate2D : CLLocationCoordinate2D) -> CLLocation{
        let location: CLLocation = CLLocation(latitude: clLocationCoordinate2D.latitude, longitude: clLocationCoordinate2D.longitude)
        return location
    }
    
    
    
    //--------------------------------------------------------------
    // MARK: - GMSMapView = idleAt Position
    // MARK: -
    //--------------------------------------------------------------
    
    
    
    /**
     * Called when the map becomes idle, after any outstanding gestures or
     * animations have completed (or after the camera has been explicitly set).
     */
    
    //--------------------------------------------------------------
    // MARK: - GMSMapViewDelegate - MOVE MAP - CALL GOOGLE NEARBY and GEOLOCATE
    // MARK: -
    //--------------------------------------------------------------
    
    public func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition){
        //self.log.warning("LOC: [idleAtCameraPosition] START")
        
        //NOISY self.log.debug("idleAtCameraPosition: position:\(position.target)")
        
        
        
        //------------------------------------------------------------------------------------------------
        //UPDATE CURRENT
        //------------------------------------------------------------------------------------------------
        //position.target: CLLocationCoordinate2D
        let location: CLLocation = clLocationForCLLocationCoordinate2D(position.target)
        
        if location.isValid == false{
            self.log.warning("LOC: [idleAtCameraPosition] location.isValid is FALSE - map pointing at blue equator - move it - calling moveToCurrentLocation")
            self.moveToCurrentLocation()
        }else{
            //self.log.warning("LOC: [idleAtCameraPosition] location.isValid - continue")
            
            //------------------------------------------------
            //                position.bearing        //CLLocationDirection :Double
            //                position.target         //CLLocationCoordinate2D
            //                position.viewingAngle   //Double
            //                position.zoom           //Float
            
            //OK just not needed
            //        if let mapView_projection = self.mapView.projection{
            //            let gmsVisibleRegion = mapView_projection.visibleRegion()
            //
            ////            /** Bottom left corner of the camera. */
            ////            var nearLeft: CLLocationCoordinate2D
            ////
            ////            /** Bottom right corner of the camera. */
            ////            var nearRight: CLLocationCoordinate2D
            ////
            ////            /** Far left corner of the camera. */
            ////            var farLeft: CLLocationCoordinate2D
            ////
            ////            /** Far right corner of the camera. */
            ////            var farRight: CLLocationCoordinate2D
            //
            //
            //            self.log.debug("  farLeft:\(gmsVisibleRegion.farLeft.latitude), \(gmsVisibleRegion.farLeft.longitude)")
            //            self.log.debug(" farRight:\(gmsVisibleRegion.farRight.latitude), \(gmsVisibleRegion.farRight.longitude)")
            //            self.log.debug(" nearLeft:\(gmsVisibleRegion.nearLeft.latitude), \(gmsVisibleRegion.nearLeft.longitude)")
            //            self.log.debug("nearRight:\(gmsVisibleRegion.nearRight.latitude), \(gmsVisibleRegion.nearRight.longitude)")
            //            self.log.debug("")
            //        }else{
            //            self.log.error("self.mapView.projectionis nil")
            //        }colcQueryCollectionForType
            //------------------------------------------------------------------------------------------------
            //PREVENT TOO MUCH WS CALLS if map only moves small bit
            //------------------------------------------------------------------------------------------------
            var mapMovedEnoughToCallWS = false
            //shut up warning
            //print("mapMovedEnoughToCallWS:\(mapMovedEnoughToCallWS)")
            //is this first location retrieved? if not then backup current
            if let currentMapLocation = self.currentMapLocation{
                //---------------------------------------------
                //prev and current set
                //---------------------------------------------
                //Backup current before updating it below
                self.prevMapLocation = currentMapLocation
                
            }else{
                self.log.warning("LOC: [idleAtCameraPosition] currentMapLocation is nil - map first appearing idleAtCameraPosition")
            }
            
            
            //------------------------------------------------------------------------------------------------
            //UPDATE CURRENT
            //------------------------------------------------------------------------------------------------
            //position.target: CLLocationCoordinate2D
            
            //            if location.isValid{
            //                self.log.warning("LOC: [idleAtCameraPosition] location.isValid use as currentMapLocation")
            //            }else{
            //
            //                if let currentLocation = appDelegate.currentLocation {
            //                    self.log.warning("LOC: [idleAtCameraPosition] location.isValid FALSE - using currentLocation:[\(currentLocation.coordinate.latitude),\(currentLocation.coordinate.longitude)")
            //                    location = currentLocation
            //                }else{
            //                    self.log.warning("LOC: [idleAtCameraPosition] location.isValid FALSE - appDelegate.currentLocation is nil - location may be invalid")
            //                }
            //
            //            }
            
            //needed for querysearch / place search - where to center your search from
            self.currentMapLocation = location
            
            //------------------------------------------------------------------------------------------------
            //if dist between prev and current large enough refresh the results
            //------------------------------------------------------------------------------------------------
            
            if let prevMapLocation = self.prevMapLocation{
                if let currentMapLocation = self.currentMapLocation{
                    //------------------------------------------------------------------------------------------------
                    //GET dist between the PREV /CURRENT map locations
                    //------------------------------------------------------------------------------------------------
                    
                    //FROM = e.g. MAP CENTER
                    let from: CLLocationCoordinate2D =  CLLocationCoordinate2D(latitude: prevMapLocation.coordinate.latitude, longitude: prevMapLocation.coordinate.longitude)
                    //print("from:\(from)")
                    
                    //TO = Place result location SELF
                    let to: CLLocationCoordinate2D =  CLLocationCoordinate2D(latitude: currentMapLocation.coordinate.latitude, longitude: currentMapLocation.coordinate.longitude)
                    //print("to:\(from)")
                    //GoogleMaps function - returns meters
                    let distanceMetersDouble : CLLocationDistance =  GMSGeometryDistance(from, to)
                    
                    
                    // TODO: - WHEN ZOOMED IN THIS CAN FAIL TO BE TRIGGERED
                    let maxDist = 10.0
                    if distanceMetersDouble > maxDist{
                        //self.log.debug("distanceMetersDouble:\(distanceMetersDouble) GREATER THAN \(maxDist)m - call WS")
                        mapMovedEnoughToCallWS = true
                    }else{
                        //self.log.debug("distanceMetersDouble:\(distanceMetersDouble) LESS THAN \(maxDist)m - dont call WS")
                        mapMovedEnoughToCallWS = false
                    }
                    //------------------------------------------------------------------------------------------------
                    
                }else{
                    self.log.debug("currentMapLocation is nil - ok to update map")
                    mapMovedEnoughToCallWS = true
                }
            }else{
                self.log.debug("prevMapLocation is nil - ok to update map")
                mapMovedEnoughToCallWS = true
            }
            
            //-----------------------------------------------------------------------------------
            if mapMovedOkToCallWS{
                //update Map and Table
                
                //self.sonarView.startAnimation()
                //self.log.warning("LOC: [idleAtCameraPosition] CALL nearbySearch()")
                //self.nearbySearch(location)
                //-----------------------------------------------------------------------------------
                //move down - always get
                ///Convert lat/lng to address
                //self.geocodeLocation(location.coordinate)
                //-----------------------------------------------------------------------------------
            }else{
                self.log.warning("LOC: [idleAtCameraPosition] mapMovedOkToCallWS is FALSE - DONT CALL NEARBY SEARCH OR GEOCODE LOCATION")
            }
            
            //------------------------------------------------
            //GEOCODE LOCATION
            //------------------------------------------------
            ///Convert lat/lng to address
            //self.geocodeCenterOfMapLocation(location)
            
            //------------------------------------------------
        }
        
    }
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - TAP/PRESS at COOORD
    //--------------------------------------------------------------
    /**
     * Called after a tap gesture at a particular coordinate, but only if a marker
     * was not tapped.  This is called before deselecting any currently selected
     * marker (the implicit action for tapping on the map).
     */
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D){
        self.log.debug("mapView: didTapAtCoordinate")
    }
    /**
     * Called after a long-press gesture at a particular coordinate.
     *
     * @param mapView The map view that was pressed.
     * @param coordinate The location that was pressed.
     */
    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D){
        self.log.debug("mapView: didLongPressAtCoordinate")
    }
    
    
    
    //--------------------------------------------------------------
    // MARK: - prepareForSegue
    // MARK: -
    //--------------------------------------------------------------
//    let segueTFLApiPlaceMapViewControllerTOTFLApiPlaceSingleTableViewController   = "segueTFLApiPlaceMapViewControllerTOTFLApiPlaceSingleTableViewController"
//    let segueTFLApiPlaceMapViewControllerTOTFLApiPlaceStreetViewController        = "segueTFLApiPlaceMapViewControllerTOTFLApiPlaceStreetViewController"
//    let segueTFLApiPlaceMapViewControllerTOTFLApiPlaceMapDirectionsViewController = "segueTFLApiPlaceMapViewControllerTOTFLApiPlaceMapDirectionsViewController"
//    
//    
    
    
    // MARK: -
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        //        if(segue.identifier == segueTFLApiPlaceMapViewControllerTOTFLApiPlaceSingleTableViewController){
        //
        //            if(segue.destination.isMember(of: TFLApiPlaceSingleTableViewController.self)){
        //
        //                let tflApiPlaceSingleTableViewController = (segue.destination as! TFLApiPlaceSingleTableViewController)
        //                //tflApiPlaceMapViewController.delegate = self
        //                tflApiPlaceSingleTableViewController.selectedTFLApiPlace = self.selectedTFLApiPlace
        //
        //
        //            }else{
        //                print("ERROR:not TFLApiPlaceMapViewController")
        //            }
        //        }else if(segue.identifier == segueTFLApiPlaceMapViewControllerTOTFLApiPlaceStreetViewController){
        //
        //            if(segue.destination.isMember(of: TFLApiPlaceStreetViewController.self)){
        //
        //                let tflApiPlaceStreetViewController = (segue.destination as! TFLApiPlaceStreetViewController)
        //                //tflApiPlaceMapViewController.delegate = self
        //                tflApiPlaceStreetViewController.selectedTFLApiPlace = self.selectedTFLApiPlace
        //
        //            }else{
        //                print("ERROR:not TFLApiPlaceMapViewController")
        //            }
        //        }
        //        else if(segue.identifier == segueTFLApiPlaceMapViewControllerTOTFLApiPlaceMapDirectionsViewController){
        //
        //            if(segue.destination.isMember(of: TFLApiPlaceMapDirectionsViewController.self)){
        //
        //                let tflApiPlaceMapDirectionsViewController = (segue.destination as! TFLApiPlaceMapDirectionsViewController)
        //                //tflApiPlaceMapDirectionsViewController.delegate = self
        //                tflApiPlaceMapDirectionsViewController.selectedTFLApiPlace = self.selectedTFLApiPlace
        //
        //            }
        //            else{
        //                print("ERROR:not TFLApiPlaceMapDirectionsViewController")
        //            }
        //        }
        //        else{
        self.log.error("UNHANDLED SEGUE:\(String(describing: segue.identifier))")
        //        }
    }

    
}
