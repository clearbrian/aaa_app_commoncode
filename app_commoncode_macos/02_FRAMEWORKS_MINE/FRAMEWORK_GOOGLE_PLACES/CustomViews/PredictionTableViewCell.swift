//
//  PredictionTableViewCell.swift
//  joyride
//
//  Created by Brian Clear on 18/08/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit

class PredictionTableViewCell : UITableViewCell{
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDetail: UILabel!
    
}
