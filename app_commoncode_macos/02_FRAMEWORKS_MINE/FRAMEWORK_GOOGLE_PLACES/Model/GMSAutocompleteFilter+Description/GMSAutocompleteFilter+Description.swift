//
//  GMSPlacesAutocompleteTypeFilter+Description.swift
//  joyride
//
//  Created by Brian Clear on 31/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation
//given GMSPlacesAutocompleteTypeFilter output description of enum as text

//enum GMSPlacesAutocompleteTypeFilter : Int {
//    
//    case NoFilter
//    case Geocode
//    case Address
//    case Establishment
//    case Region
//    case City
//}


//extension GMSPlacesAutocompleteTypeFilter{
//    
//    public func description() -> String {
//        var description_ = ""
//        
//        switch self {
//        case .NoFilter:
//            description_ = "NoFilter"
//        case .Geocode:
//            description_ = "Geocode"
//        case .Address:
//            description_ = "Address"
//        case .Establishment:
//            description_ = "Establishment"
//        case .Region:
//            description_ = "Region"
//        case .City:
//            description_ = "City"
//        }
//        return description_
//    }
//    
//}
