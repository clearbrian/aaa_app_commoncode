//
//  CLKAddressGoogle.swift
//  joyride
//
//  Created by Brian Clear on 26/10/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
// TODO: - removed to get past app store for txi ranks
//import Contacts
import MapKit
import GoogleMaps
class CLKAddressGoogle: CLKAddress{
    var gmsAddress: GMSAddress?
    
    init(gmsAddress: GMSAddress) {
        self.gmsAddress = gmsAddress
    }
    
    // Street number and name.
    //open var thoroughfare: String? { get }
    override var street: String? {
    
        return self.thoroughfare
    }
    //--------------------------------------------------------------
    // MARK: - GMSAddress.thoroughfare
    // MARK: -
    //--------------------------------------------------------------

    var thoroughfare: String? {
        var thoroughfare_: String? = nil
        if let gmsAddress = self.gmsAddress {
            
            if let thoroughfare = gmsAddress.thoroughfare {
                thoroughfare_ = thoroughfare
                
            }else{
                appDelegate.log.error("gmsAddress.thoroughfare is nil")
            }
            
        }else{
            appDelegate.log.error("self.gmsAddress is nil - cant get thoroughfare")
            
        }
        return thoroughfare_
    }
    
    
    
    //--------------------------------------------------------------
    // MARK: - city/locality
    // MARK: -
    //--------------------------------------------------------------
    
    // Locality or city.
    //open var locality: String? { get }
    override var city: String  {
        if let locality = self.locality {
            return locality
        }else{
            appDelegate.log.error("self.locality is nil - city >> ''")
            return ""
        }
        
    }
    
    //--------------------------------------------------------------
    // MARK: - locality is Optional - need to cancel Uber call is nil
    // MARK: -
    //--------------------------------------------------------------

    
    ///beware - super.locality is an String? can cause SourceKitService to crash if you over ride it with non optional
    override var locality: String?  {
        var locality_: String?
        if let gmsAddress = self.gmsAddress {

            if let locality = gmsAddress.locality {
                locality_ = locality
            }else{
                appDelegate.log.error("gmsAddress.locality is nil")
            }
        }else{
            appDelegate.log.error("self.gmsAddress is nil - cant get locality_")
        }
        return locality_
    }

    // Subdivision of locality, district or park.
    //open var subLocality: String? { get }

    override var subLocality: String  {
        var subLocality_: String = ""
        if let gmsAddress = self.gmsAddress {

            if let subLocality = gmsAddress.subLocality {
                subLocality_ = subLocality
            }else{
                //NOISY appDelegate.log.error("gmsAddress.subLocality is nil")
            }

        }else{
            appDelegate.log.error("self.gmsAddress is nil - cant get city")
        }
        return subLocality_
    }


    //--------------------------------------------------------------
    // MARK: - state/administrativeArea
    // MARK: -
    //--------------------------------------------------------------

    // Region/State/Administrative area.
    //open var administrativeArea: String? { get }

    override var state: String  {
        return self.administrativeArea
    }

    override var administrativeArea: String  {
        var administrativeArea_: String = ""
        if let gmsAddress = self.gmsAddress {

            if let administrativeArea = gmsAddress.administrativeArea {
                administrativeArea_ = administrativeArea
            }else{
                //noisyappDelegate.log.error("gmsAddress.administrativeArea is nil")
            }
        }else{
            appDelegate.log.error("self.gmsAddress is nil - cant get city")

        }
        return administrativeArea_
    }


    //--------------------------------------------------------------
    // MARK: - postalCode: Postal/Zip code.
    // MARK: -
    //--------------------------------------------------------------

    //GMSAddress
    //open var postalCode: String? { get }
    override var postalCode: String  {
        var postalCode_: String = ""
        if let gmsAddress = self.gmsAddress {

            if let postalCode = gmsAddress.postalCode {
                postalCode_ = postalCode
            }else{
                appDelegate.log.error("gmsAddress.postalCode is nil")
            }
        }else{
            appDelegate.log.error("self.gmsAddress is nil - cant get postalCode")
        }
        return postalCode_
    }

    //--------------------------------------------------------------
    // MARK: - country
    // MARK: -
    //--------------------------------------------------------------
    override var country: String  {
        var country_: String = ""
        if let gmsAddress = self.gmsAddress {

            if let country = gmsAddress.country {
                country_ = country
            }else{
                appDelegate.log.error("gmsAddress.country is nil")
            }
        }else{
            appDelegate.log.error("self.gmsAddress is nil - cant get country")
        }
        return country_
    }

    // TODO: - gmsAddress doesnt have iscode
    override var isoCountryCode: String  {
        //if let gmsAddress = self.gmsAddress {
        //
        //    if let isoCountryCode = gmsAddress.isoCountryCode {
        //        return isoCountryCode
        //    }else{
        //        appDelegate.log.error("gmsAddress.isoCountryCode is nil")
        //    }
        //
        //}else{
        //    appDelegate.log.error("self.gmsAddress is nil - cant get isoCountryCode")
        //}
        return ""
    }
    /*
     GMSAddress {
     coordinate: (51.508354, -0.071815)
     lines: , London E1W 1AZ, UK
     locality: London
     postalCode: E1W 1AZ
     country: United Kingdom
     }
     */
    
    override var clLocation: CLLocation?{
        get {
            var clLocation_: CLLocation?
            if let gmsAddress = self.gmsAddress {
                clLocation_ = CLLocation(latitude: gmsAddress.coordinate.latitude, longitude: gmsAddress.coordinate.longitude)

            }else{
                appDelegate.log.error("self.gmsAddress is nil - cant get country")
            }
            return clLocation_
        }
//        set {
//            //clLocation = newValue
//        }
    }
    
    //--------------------------------------------------------------
    // MARK: - addressDictionaryApple
    // MARK: -
    //--------------------------------------------------------------

//     var addressDictionaryApple: [AnyHashable : Any]?{
//        var addressDictionary_:[AnyHashable : Any]? = [:]
//        
//        //------------------------------------------------------------------------------------------------
//        //CNContactPostalAddressesKey  self.contactPropertyAddress.value >> <CNPostalAddress
//        //------------------------------------------------------------------------------------------------
////        if let cnPostalAddress = contactPropertyAddress.value as? CNPostalAddress{
////            
////            //---------------------------------------------------------------------
////            //CoreLocation - CLGeocoder() requires dictionary in Address Book format - Contacts framework used after iOS9 as AB deprecated
////            //open func geocodeAddressDictionary(_ addressDictionary: [AnyHashable : Any]
////            //---------------------------------------------------------------------
////            //<CNPostalAddress: 0x7f9bd1411020: street=3494 Kuhl Avenue, city=Atlanta, state=GA, postalCode=30303, country=USA, countryCode=ca, formattedAddress=(null)>)
////            //---------------------------------------------------------------------
////            //    self.log.debug("cnPostalAddress.street:\(cnPostalAddress.street)")
////            //    self.log.debug("cnPostalAddress.city:\(cnPostalAddress.city)")
////            //    self.log.debug("cnPostalAddress.state:\(cnPostalAddress.state)")
////            //    self.log.debug("cnPostalAddress.postalCode:\(cnPostalAddress.postalCode)")
////            //    self.log.debug("cnPostalAddress.country:\(cnPostalAddress.country)")
////            //    self.log.debug("cnPostalAddress.ISOCountryCode:\(cnPostalAddress.isoCountryCode)")
////            //---------------------------------------------------------------------
////            //  let formatter = CNPostalAddressFormatter()
////            //  let formattedAddress = formatter.string(from: cnPostalAddress)
////            //  print("The address is \(formattedAddress)")
////            //  //    The address is 3494 Kuhl Avenue
////            //  //    Atlanta GA 30303
////            //  //    USA
////            //---------------------------------------------------------------------
////            
////            addressDictionary_?[CNPostalAddressStreetKey] = cnPostalAddress.street
////            addressDictionary_?[CNPostalAddressCityKey] = cnPostalAddress.city
////            addressDictionary_?[CNPostalAddressStateKey] = cnPostalAddress.state
////            addressDictionary_?[CNPostalAddressPostalCodeKey] = cnPostalAddress.postalCode
////            addressDictionary_?[CNPostalAddressCountryKey] = cnPostalAddress.country
////            addressDictionary_?[CNPostalAddressISOCountryCodeKey] = cnPostalAddress.isoCountryCode
////            appDelegate.log.debug("addressDictionary_:\(addressDictionary_)")
////            
////        }else{
////            appDelegate.log.error("self.contactPropertyAddress.value is nil or not CNPostalAddress")
////        }
//        
//        //CNContactOrganizationNameKey
//        //MakKit annotation takes a placemark but cant directly set title
//        addressDictionary_?[CNPostalAddressStreetKey] = self.street
//        addressDictionary_?[CNPostalAddressCityKey] = self.city
//        //addressDictionary_?[CNPostalAddressStateKey] = cnPostalAddress.state
//        addressDictionary_?[CNPostalAddressPostalCodeKey] = self.postalCode
//        addressDictionary_?[CNPostalAddressCountryKey] = self.country
//        //addressDictionary_?[CNPostalAddressISOCountryCodeKey] = cnPostalAddress.isoCountryCode
//        
//        //mkPlacemark.thoroughfare fails cos this is missing
//        // TODO: - format better
//        addressDictionary_?["FormattedAddressLines"] = self.street
//        
//        
//        
//        appDelegate.log.debug("addressDictionaryApple:\(addressDictionary_)")
//        return addressDictionary_
//        
//        
//    }
    
    
    
    override var address_lines: [String]? {
        
//        var address_lines_ = [String]()
//---------------------------------------------------------------------
        //v1 generic CLKAddress - but
//            //---------------------------------------------------------------------
//            if let street = self.street {
//                address_lines_.append(street)
//            }else{
//                appDelegate.log.error("self.street is nil")
//            }
//            //---------------------------------------------------------------------
//            address_lines_.append(self.city)
//        //---------------------------------------------------------------------
//        if let city = self.city {
//            address_lines_.append(city)
//        }else{
//            appDelegate.log.error("self.street is nil")
//        }
//
//            address_lines_.append(self.state)
//            address_lines_.append(self.subLocality)
//            address_lines_.append(self.administrativeArea)
//            address_lines_.append(self.postalCode)
//            //dont add country - dont need to see it in address strings
//            //address_lines_.append(self.country)
//        
//        
//        self.log.error("SUBCLASS SHOULD CREATE address lines varies per apple/google")
        //---------------------------------------------------------------------
        
//        open var thoroughfare: String? { get }
//        /** Locality or city. */
//        open var locality: String? { get }
//        /** Subdivision of locality, district or park. */
//        open var subLocality: String? { get }
//        /** Region/State/Administrative area. */
//        open var administrativeArea: String? { get }
//        /** Postal/Zip code. */
//        open var postalCode: String? { get }
        
       var address_lines_ = [String]()
        
        address_lines_ = safeAppend(address_lines_, lineString: self.thoroughfare)
        address_lines_ = safeAppend(address_lines_, lineString: self.locality)
        address_lines_ = safeAppend(address_lines_, lineString: self.subLocality)
        address_lines_ = safeAppend(address_lines_, lineString: self.administrativeArea)
        address_lines_ = safeAppend(address_lines_, lineString: self.postalCode)
        //address_lines_=  safeAppend(address_lines_, self.country)
        
        return address_lines_
        
    }
    
    
    
    
    
    //if you forward then reverse geocode you end up with a CLKAddressGoogle
    //but Contacts page uses MKMapview so needs a CLPlacemark


    //--------------------------------------------------------------
    // MARK: - MapKit
    // MARK: -
    //--------------------------------------------------------------
    //tried to add to extension but as of Swift3 externtions cant override
    override var mkAnnotation: MKAnnotation?{
        var mkAnnotation_: MKAnnotation?
        
        
        //COLCMKAnnotation.placemark: MKPlacemark internally has no title so need to use COLCMKAnnotation which wraps it but can handle title
        if let clLocation = self.clLocation {
            mkAnnotation_ = COLCMKAnnotation.init(title: self.mkAnnotation_pinTitle(), coordinate: clLocation.coordinate)
            
        }else{
            appDelegate.log.error("self.self.clLocation is nil")
        }
        
        return mkAnnotation_
    }
    
    override func mkAnnotation_pinTitle() -> String{
        //Contact > Belfast > address with only post code 
        var pinTitle = "Google Geocoded Address"
        
        if let clLocation = self.clLocation {
            if let name = self.name {

                if let thoroughfare = self.thoroughfare {
                    
                    if "" == self.city {
                        //city
                        pinTitle =  "\(pinTitle)\r\(name)\r\(thoroughfare)\r(\(clLocation.formattedLatLng()))"
                        
                    }else{
                        pinTitle =  "\(pinTitle)\r\(name)\r\(thoroughfare), \(self.city)\r(\(clLocation.formattedLatLng()))"
                    }
                    
                }else{
                    if "" == self.city {
                        //city
                        pinTitle =  "\(pinTitle)\r\(name)\r(\(clLocation.formattedLatLng()))"
                        
                    }else{
                        pinTitle =  "\(pinTitle)\r\(name)\r\(self.city)\r(\(clLocation.formattedLatLng()))"
                    }
                }

            }else{
                if let thoroughfare = self.thoroughfare {
                    
                    if "" == self.city {
                        //city
                        pinTitle =  "\(pinTitle)\r\(thoroughfare)\r(\(clLocation.formattedLatLng()))"
                        
                    }else{
                        pinTitle =  "\(pinTitle)\r\(thoroughfare), \(self.city)\r(\(clLocation.formattedLatLng()))"
                    }
                    
                }else{
                    if "" == self.city {
                        //city
                        pinTitle =  "\(pinTitle)\r(\(clLocation.formattedLatLng()))"
                        
                    }else{
                        pinTitle =  "\(pinTitle)\r\(self.city)\r(\(clLocation.formattedLatLng()))"
                    }
                }
            }
        }else{
            appDelegate.log.error("self.clLocation is nil")
        }
        
        return pinTitle
    }

}

