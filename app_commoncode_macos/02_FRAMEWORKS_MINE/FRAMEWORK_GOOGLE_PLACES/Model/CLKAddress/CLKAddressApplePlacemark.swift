//
//  CLKAddressApplePlacemark.swift
//  joyride
//
//  Created by Brian Clear on 26/10/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//
import Foundation
import CoreLocation

class CLKAddressApplePlacemark: CLKAddress{
    
    var clPlacemark: CLPlacemark?
    
    init(clPlacemark: CLPlacemark) {
        self.clPlacemark = clPlacemark
    }
    
//    override var name: String? {
//        var name_: String? = nil
//        if let clPlacemark = self.clPlacemark {
//            
//            if let name = clPlacemark.name {
//                name_ = name
//                
//            }else{
//                appDelegate.log.error("clPlacemark.name is nil")
//            }
//            
//        }else{
//            appDelegate.log.error("self.clPlacemark is nil - cant get name")
//            
//        }
//        return name_
//    }
//    
//    ///** Street number and name. */
//    override var street: String? {
//        return self.thoroughfare
//    }
//    
//    //CLPlacemark only
//    var thoroughfare: String? {
//        var thoroughfare_: String? = nil
//        if let clPlacemark = self.clPlacemark {
//            
//            if let thoroughfare = clPlacemark.thoroughfare {
//                thoroughfare_ = thoroughfare
//                
//            }else{
//                appDelegate.log.error("clPlacemark.thoroughfare is nil")
//            }
//            
//        }else{
//            appDelegate.log.error("self.clPlacemark is nil - cant get thoroughfare")
//            
//        }
//        return thoroughfare_
//    }
//    
//    //CLPlacemark only
//    var subThoroughfare: String? {
//        var subThoroughfare_: String? = nil
//        if let clPlacemark = self.clPlacemark {
//            
//            if let subThoroughfare = clPlacemark.subThoroughfare {
//                subThoroughfare_ = subThoroughfare
//                
//            }else{
//                appDelegate.log.error("clPlacemark.subThoroughfare is nil")
//            }
//        }else{
//            appDelegate.log.error("self.clPlacemark is nil - cant get subThoroughfare")
//        }
//        return subThoroughfare_
//    }
//    
//    
//    
//    ///** Locality or city. */
//    //locality: String? { get }
//    override var city: String  {
//        return self.locality
//    }
//    
//    //CLPlacemark
//    override var locality: String  {
//        var locality_: String = ""
//        if let clPlacemark = self.clPlacemark {
//            
//            if let locality = clPlacemark.locality {
//                locality_ = locality
//            }else{
//                appDelegate.log.error("clPlacemark.locality is nil")
//            }
//            
//        }else{
//            appDelegate.log.error("self.clPlacemark is nil - cant get locality")
//        }
//        return locality_
//    }
//    
//    
//    ///** Subdivision of locality, district or park. */
//    //subLocality: String? { get }
//    
//    override var subLocality: String  {
//        var subLocality_: String = ""
//        if let clPlacemark = self.clPlacemark {
//            
//            if let subLocality = clPlacemark.subLocality {
//                subLocality_ = subLocality
//            }else{
//                appDelegate.log.error("clPlacemark.subLocality is nil")
//            }
//            
//        }else{
//            appDelegate.log.error("self.clPlacemark is nil - cant get city")
//        }
//        return subLocality_
//    }
//    
//    //--------------------------------------------------------------
//    // MARK: - state/administrativeArea
//    // MARK: -
//    //--------------------------------------------------------------
//    
//    //CLPlacemark
//    //administrativeArea: String? { get } // state, eg. CA
//    
//    override var state: String  {
//        return self.administrativeArea
//    }
//    
//    override var administrativeArea: String  {
//        var administrativeArea_: String = ""
//        if let clPlacemark = self.clPlacemark {
//            
//            if let administrativeArea = clPlacemark.administrativeArea {
//                administrativeArea_ = administrativeArea
//            }else{
//                appDelegate.log.error("clPlacemark.administrativeArea is nil")
//            }
//        }else{
//            appDelegate.log.error("self.clPlacemark is nil - cant get city")
//            
//        }
//        return administrativeArea_
//    }
//    
//    //CLPlacemark only
//    var subAdministrativeArea: String  {
//        var subAdministrativeArea_: String = ""
//        if let clPlacemark = self.clPlacemark {
//            
//            if let subAdministrativeArea = clPlacemark.subAdministrativeArea {
//                subAdministrativeArea_ = subAdministrativeArea
//            }else{
//                appDelegate.log.error("clPlacemark.subAdministrativeArea is nil")
//            }
//        }else{
//            appDelegate.log.error("self.clPlacemark is nil - cant get city")
//            
//        }
//        return subAdministrativeArea_
//    }
//    //--------------------------------------------------------------
//    // MARK: - postalCode
//    // MARK: -
//    //--------------------------------------------------------------
//    
//    ///** Postal/Zip code. */
//    //postalCode: String? { get } // zip code, eg. 95014
//    override var postalCode: String  {
//        var postalCode_: String = ""
//        if let clPlacemark = self.clPlacemark {
//            
//            if let postalCode = clPlacemark.postalCode {
//                postalCode_ = postalCode
//            }else{
//                appDelegate.log.error("clPlacemark.postalCode is nil")
//            }
//        }else{
//            appDelegate.log.error("self.clPlacemark is nil - cant get postalCode")
//        }
//        return postalCode_
//    }
//    
//    
//    //--------------------------------------------------------------
//    // MARK: - country
//    // MARK: -
//    //--------------------------------------------------------------
//    
//    // country: String? { get } // eg. United States
//    override var country: String  {
//        var country_: String = ""
//        if let clPlacemark = self.clPlacemark {
//            
//            if let country = clPlacemark.country {
//                country_ = country
//            }else{
//                appDelegate.log.error("clPlacemark.country is nil")
//            }
//        }else{
//            appDelegate.log.error("self.clPlacemark is nil - cant get country")
//        }
//        return country_
//    }
//    
//    //   isoCountryCode: String? { get } // eg. US
//    override var isoCountryCode: String  {
//        if let clPlacemark = self.clPlacemark {
//            if let isoCountryCode = clPlacemark.isoCountryCode {
//                return isoCountryCode
//            }else{
//                appDelegate.log.error("clPlacemark.isoCountryCode is nil")
//            }
//            
//        }else{
//            appDelegate.log.error("self.clPlacemark is nil - cant get isoCountryCode")
//        }
//        return ""
//    }
//    
//    //--------------------------------------------------------------
//    // MARK: - CLPlacemark only
//    // MARK: -
//    //--------------------------------------------------------------
//    //inlandWater: String? { get } // eg. Lake Tahoe
//    var inlandWater: String  {
//        
//        if let clPlacemark = self.clPlacemark {
//            if let inlandWater = clPlacemark.inlandWater {
//                return inlandWater
//            }else{
//                appDelegate.log.error("clPlacemark.inlandWater is nil")
//            }
//            
//        }else{
//            appDelegate.log.error("self.clPlacemark is nil - cant get isoCountryCode")
//        }
//    }
//    
//    // ocean: String? { get } // eg. Pacific Ocean
//    var ocean: String  {
//        
//        if let clPlacemark = self.clPlacemark {
//            if let ocean = clPlacemark.ocean {
//                return ocean
//            }else{
//                appDelegate.log.error("clPlacemark.ocean is nil")
//            }
//        }else{
//            appDelegate.log.error("self.clPlacemark is nil - cant get ocean")
//        }
//    }
//    
//    
//    //--------------------------------------------------------------
//    // MARK: - MISC
//    // MARK: -
//    //--------------------------------------------------------------
//    
//    // areasOfInterest: [String]? { get } // eg. Golden Gate Park
//    var areasOfInterest: [String]  {
//        
//        if let clPlacemark = self.clPlacemark {
//            if let areasOfInterest = clPlacemark.areasOfInterest {
//                return areasOfInterest
//            }else{
//                appDelegate.log.error("clPlacemark.areasOfInterest is nil")
//            }
//            
//        }else{
//            appDelegate.log.error("self.clPlacemark is nil - cant get areasOfInterest")
//        }
//    }
//    
//    var timeZone: TimeZone {
//        
//        if let clPlacemark = self.clPlacemark {
//            if let timeZone = clPlacemark.timeZone {
//                return timeZone
//            }else{
//                appDelegate.log.error("clPlacemark. timeZone is nil")
//            }
//            
//        }else{
//            appDelegate.log.error("self.clPlacemark is nil - cant get timeZone")
//        }
//    }
//    //region: CLRegion? { get }
//    var region: CLRegion {
//        
//        if let clPlacemark = self.clPlacemark {
//            if let region = clPlacemark.region {
//                return region
//            }else{
//                appDelegate.log.error("clPlacemark. region is nil")
//            }
//            
//        }else{
//            appDelegate.log.error("self.clPlacemark is nil - cant get region")
//        }
//    }
//    
//    //--------------------------------------------------------------
//    // MARK: - addressDictionary
//    // MARK: -
//    //--------------------------------------------------------------
//    //addressDictionary: [AnyHashable : Any]? { get }
//    var addressDictionary: [AnyHashable : Any] {
//        
//        if let clPlacemark = self.clPlacemark {
//            if let addressDictionary = clPlacemark.addressDictionary {
//                return addressDictionary
//            }else{
//                appDelegate.log.error("clPlacemark.addressDictionary is nil")
//            }
//            
//        }else{
//            appDelegate.log.error("self.clPlacemark is nil - cant get addressDictionary")
//        }
//    }
//    
//    
//    //--------------------------------------------------------------
//    // MARK: - description
//    // MARK: -
//    //--------------------------------------------------------------
//    override var description : String {
//        var description_ = "CLKAddressApple:"
//        description_ = description_ + "CL                  name:\(self.name)\r"
//        description_ = description_ + "                  street:\(self.street)\r"
//        description_ = description_ + "C           thoroughfare:\(self.thoroughfare)\r"
//        description_ = description_ + "CL       subThoroughfare:\(self.subThoroughfare)\r"
//        description_ = description_ + "                    city:\(self.city)\r"
//        description_ = description_ + "CL              locality:\(self.locality)\r"
//        description_ = description_ + "             subLocality:\(self.subLocality)\r"
//        description_ = description_ + "                   state:\(self.state)\r"
//        description_ = description_ + "      administrativeArea:\(self.administrativeArea)\r"
//        
//        description_ = description_ + "CL subAdministrativeArea:\(self.subAdministrativeArea)\r"
//        
//        description_ = description_ + "              postalCode:\(self.postalCode)\r"
//        
//        description_ = description_ + "                 country:\(self.country)\r"
//        description_ = description_ + "          isoCountryCode:\(self.isoCountryCode)\r"
//        description_ = description_ + "CL           inlandWater:\(self.inlandWater)\r"
//        description_ = description_ + "CL                 ocean:\(self.ocean)\r"
//        
//        description_ = description_ + "CL              timeZone:\(self.timeZone)\r"
//        description_ = description_ + "CL                region:\(self.region)\r"
//        description_ = description_ + "              clLocation:\(self.clLocation)\r"
//        description_ = description_ + "CL     areasOfInterest[]:\(self.areasOfInterest)\r"
//        description_ = description_ + "           address_lines:\r\(self.address_lines)\r"
//        description_ = description_ + "CL     addressDictionary:\r\(self.addressDictionary)\r"
//        
//        return description_
//    }
}
