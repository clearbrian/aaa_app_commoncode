//
//  CLKPlaceStartOrEnd.swift
//  joyride
//
//  Created by Brian Clear on 14/10/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation

enum CLKPlaceStartOrEnd: String{
    case Start = "Start"
    case End = "End"
}
