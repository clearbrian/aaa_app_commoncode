//
//  GMPlace+Extensions.swift
//  joyride
//
//  Created by Brian Clear on 05/10/2015.
//  Copyright © 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import GoogleMaps
import GooglePlaces

extension GMSPlace{
    //if you pick a lat/lng as Place the placeId will be invalid and type array will be one row called "synthetic_geocode"
    /*
        Printing description of gmsPlace:
        name: (51.5086515, -0.0724941), id: 0x48760349971657cf, coordinate: (51.508651 -0.072494), open now: 2, phone: (null), address: (null), rating: 0.000000, price level: 0, website: (null), types: (
        "synthetic_geocode"
        )
    */
    func isLatLng_SyntheticGeocode() -> Bool{
        
        var isLatLng = false
        for typeString in self.types{

            if typeString == "synthetic_geocode"{
                isLatLng = true
            }
            
        }
        return isLatLng
    }
}
