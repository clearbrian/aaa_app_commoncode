//
//  CLKGooglePlaceOpeningHours.swift
//  joyride
//
//  Created by Brian Clear on 03/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//


class CLKGooglePlaceOpeningHours: ParentMappable {
    
    //[html_attributions, result, status]
    var open_now: Bool?  /*Boolean*/
    var periods:[CLKGooglePlaceOpeningHoursPeriod]?
    var weekday_text:[String]?
    
    // root calls mapping()
    //    required init?(_ map: Map){
    //        mapping(map: map)
    //    }

    override func mapping(map: Map){
        open_now <- map["open_now"]
        periods <- map["periods"]
        weekday_text <- map["weekday_text"]
    }
}
/*
{
    "open_now" : true,
    "periods" : [
    {
    "close" : {
    "day" : 0,
    "time" : "1600"
    },
    "open" : {
    "day" : 0,
    "time" : "1000"
    }
    },
    {
    "close" : {
    "day" : 1,
    "time" : "2000"
    },
    "open" : {
    "day" : 1,
    "time" : "0800"
    }
    },
    {
    "close" : {
    "day" : 2,
    "time" : "2000"
    },
    "open" : {
    "day" : 2,
    "time" : "0800"
    }
    },
    {
    "close" : {
    "day" : 3,
    "time" : "2000"
    },
    "open" : {
    "day" : 3,
    "time" : "0800"
    }
    },
    {
    "close" : {
    "day" : 4,
    "time" : "2000"
    },
    "open" : {
    "day" : 4,
    "time" : "0800"
    }
    },
    {
    "close" : {
    "day" : 5,
    "time" : "2000"
    },
    "open" : {
    "day" : 5,
    "time" : "0800"
    }
    },
    {
    "close" : {
    "day" : 6,
    "time" : "1700"
    },
    "open" : {
    "day" : 6,
    "time" : "0900"
    }
    }
    ],
    "weekday_text" : [
        "Monday: 8:00 am – 8:00 pm",
        "Tuesday: 8:00 am – 8:00 pm",
        "Wednesday: 8:00 am – 8:00 pm",
        "Thursday: 8:00 am – 8:00 pm",
        "Friday: 8:00 am – 8:00 pm",
        "Saturday: 9:00 am – 5:00 pm",
        "Sunday: 10:00 am – 4:00 pm"
    ]
}
*/
