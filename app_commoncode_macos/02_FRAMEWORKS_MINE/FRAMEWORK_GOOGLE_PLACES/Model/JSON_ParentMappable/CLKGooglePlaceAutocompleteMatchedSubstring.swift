//
//  CLKGooglePlaceAutocompletePrediction.swift
//  joyride
//
//  Created by Brian Clear on 31/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation



class CLKGooglePlaceAutocompleteMatchedSubstring: ParentMappable {
    
    var length :NSNumber?
    var offset :NSNumber?
    
    //---------------------------------------------------------------------

    // root calls mapping()
    //    required init?(_ map: Map){
    //        mapping(map: map)
    //    }
    
    override func mapping(map: Map){
        length <- map["length"]
        offset <- map["offset"]
    }
}

/*
"matched_substrings" : [
    {
        "length" : 3,
        "offset" : 0
    }
],
*/
