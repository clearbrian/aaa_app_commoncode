//
//  CLKGooglePlaceOpeningHoursPeriodOpen.swift
//  joyride
//
//  Created by Brian Clear on 03/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//


class CLKGooglePlaceOpeningHoursPeriodTime: ParentMappable {
    
    var day:String?
    var time:String?
    
    // root calls mapping()
    //    required init?(_ map: Map){
    //        mapping(map: map)
    //    }
    
    override func mapping(map: Map){
        day <- map["day"]
        time <- map["time"]
    }
}
