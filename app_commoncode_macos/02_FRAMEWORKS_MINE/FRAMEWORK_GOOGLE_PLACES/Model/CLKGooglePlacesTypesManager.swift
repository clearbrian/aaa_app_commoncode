//
//  CLKGooglePlacesTypes.swift
//  joyride
//
//  Created by Brian Clear on 24/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation

class CLKGooglePlacesTypesManager : ParentSwiftObject{


    
    //------------------------------------------------------------------------------------------------
    static let kBUSINESS = "BUSINESS"
    static let kBUSINESS_OR_POI = "BUSINESS_OR_POI"
    static let kPOINT_OF_INTEREST = "point_of_interest"
    // static let kLOCATION = "LOCATION"
    //------------------------------------------------------------------------------------------------

    var googlePlacesTypesDictionary: [String : CLKGooglePlacesType] = [String : CLKGooglePlacesType]()
    
    
    func configureTypes(){
        createCLKGooglePlacesType("accounting", googlePlacesGeneralType:.Business, googlePlacesType:.Accounting, isOKToShowTypeName:true)
        createCLKGooglePlacesType("administrative_area_level_1", googlePlacesGeneralType:.Location, googlePlacesType:.Administrative_Area_Level_1, isOKToShowTypeName:false)
        createCLKGooglePlacesType("administrative_area_level_2", googlePlacesGeneralType:.Location, googlePlacesType:.Administrative_Area_Level_2, isOKToShowTypeName:false)
        createCLKGooglePlacesType("administrative_area_level_3", googlePlacesGeneralType:.Location, googlePlacesType:.Administrative_Area_Level_3, isOKToShowTypeName:false)
        createCLKGooglePlacesType("administrative_area_level_4", googlePlacesGeneralType:.Location, googlePlacesType:.Administrative_Area_Level_4, isOKToShowTypeName:false)
        createCLKGooglePlacesType("administrative_area_level_5", googlePlacesGeneralType:.Location, googlePlacesType:.Administrative_Area_Level_5, isOKToShowTypeName:false)
        createCLKGooglePlacesType("airport", googlePlacesGeneralType:.Airport, googlePlacesType:.Airport, isOKToShowTypeName:true)
        createCLKGooglePlacesType("amusement_park", googlePlacesGeneralType:.Entertainment, googlePlacesType:.Amusement_Park, isOKToShowTypeName:true)
        createCLKGooglePlacesType("aquarium", googlePlacesGeneralType:.Business, googlePlacesType:.Aquarium, isOKToShowTypeName:true)
        createCLKGooglePlacesType("art_gallery", googlePlacesGeneralType:.Museum, googlePlacesType:.Art_Gallery, isOKToShowTypeName:true)
        createCLKGooglePlacesType("atm", googlePlacesGeneralType:.Money, googlePlacesType:.Atm, isOKToShowTypeName:true)
        createCLKGooglePlacesType("bakery", googlePlacesGeneralType:.Business, googlePlacesType:.Bakery, isOKToShowTypeName:true)
        createCLKGooglePlacesType("bank", googlePlacesGeneralType:.Money, googlePlacesType:.Bank, isOKToShowTypeName:true)
        createCLKGooglePlacesType("bar", googlePlacesGeneralType:.Food_Drink, googlePlacesType:.Bar, isOKToShowTypeName:true)
        createCLKGooglePlacesType("beauty_salon", googlePlacesGeneralType:.Business, googlePlacesType:.Beauty_Salon, isOKToShowTypeName:true)
        createCLKGooglePlacesType("bicycle_store", googlePlacesGeneralType:.Business, googlePlacesType:.Bicycle_Store, isOKToShowTypeName:true)
        createCLKGooglePlacesType("book_store", googlePlacesGeneralType:.Business, googlePlacesType:.Book_Store, isOKToShowTypeName:true)
        createCLKGooglePlacesType("bowling_alley", googlePlacesGeneralType:.Entertainment, googlePlacesType:.Bowling_Alley, isOKToShowTypeName:true)
        createCLKGooglePlacesType("bus_station", googlePlacesGeneralType:.Transport, googlePlacesType:.Bus_Station, isOKToShowTypeName:true)
        createCLKGooglePlacesType("cafe", googlePlacesGeneralType:.Food_Drink, googlePlacesType:.Cafe, isOKToShowTypeName:true)
        createCLKGooglePlacesType("campground", googlePlacesGeneralType:.Accommodation, googlePlacesType:.Campground, isOKToShowTypeName:true)
        createCLKGooglePlacesType("car_dealer", googlePlacesGeneralType:.Business, googlePlacesType:.Car_Dealer, isOKToShowTypeName:true)
        createCLKGooglePlacesType("car_rental", googlePlacesGeneralType:.Business, googlePlacesType:.Car_Rental, isOKToShowTypeName:true)
        createCLKGooglePlacesType("car_repair", googlePlacesGeneralType:.Business, googlePlacesType:.Car_Repair, isOKToShowTypeName:true)
        createCLKGooglePlacesType("car_wash", googlePlacesGeneralType:.Business, googlePlacesType:.Car_Wash, isOKToShowTypeName:true)
        createCLKGooglePlacesType("casino", googlePlacesGeneralType:.Entertainment, googlePlacesType:.Casino, isOKToShowTypeName:true)
        createCLKGooglePlacesType("cemetery", googlePlacesGeneralType:.Religious, googlePlacesType:.Cemetery, isOKToShowTypeName:true)
        createCLKGooglePlacesType("church", googlePlacesGeneralType:.Religious, googlePlacesType:.Church, isOKToShowTypeName:true)
        createCLKGooglePlacesType("city_hall", googlePlacesGeneralType:.Public_Building, googlePlacesType:.City_Hall, isOKToShowTypeName:true)
        createCLKGooglePlacesType("clothing_store", googlePlacesGeneralType:.Shopping, googlePlacesType:.Clothing_Store, isOKToShowTypeName:true)
        
        createCLKGooglePlacesType("colloquial_area", googlePlacesGeneralType:.Location, googlePlacesType:.Colloquial_Area, isOKToShowTypeName:false)
        
        createCLKGooglePlacesType("convenience_store", googlePlacesGeneralType:.Shopping, googlePlacesType:.Convenience_Store, isOKToShowTypeName:true)
        createCLKGooglePlacesType("country", googlePlacesGeneralType:.Location, googlePlacesType:.Country, isOKToShowTypeName:true)
        createCLKGooglePlacesType("courthouse", googlePlacesGeneralType:.Public_Building, googlePlacesType:.Courthouse, isOKToShowTypeName:true)
        createCLKGooglePlacesType("dentist", googlePlacesGeneralType:.Health, googlePlacesType:.Dentist, isOKToShowTypeName:true)
        createCLKGooglePlacesType("department_store", googlePlacesGeneralType:.Shopping, googlePlacesType:.Department_Store, isOKToShowTypeName:true)
        createCLKGooglePlacesType("doctor", googlePlacesGeneralType:.Health, googlePlacesType:.Doctor, isOKToShowTypeName:true)
        createCLKGooglePlacesType("electrician", googlePlacesGeneralType:.Business, googlePlacesType:.Electrician, isOKToShowTypeName:true)
        createCLKGooglePlacesType("electronics_store", googlePlacesGeneralType:.Shopping, googlePlacesType:.Electronics_Store, isOKToShowTypeName:true)
        createCLKGooglePlacesType("embassy", googlePlacesGeneralType:.Public_Building, googlePlacesType:.Embassy, isOKToShowTypeName:true)
        
        createCLKGooglePlacesType("establishment", googlePlacesGeneralType:.Business, googlePlacesType:.Establishment, isOKToShowTypeName:false)
        
        createCLKGooglePlacesType("finance", googlePlacesGeneralType:.Money, googlePlacesType:.Finance, isOKToShowTypeName:true)
        createCLKGooglePlacesType("fire_station", googlePlacesGeneralType:.Public_Building, googlePlacesType:.Fire_Station, isOKToShowTypeName:true)
        createCLKGooglePlacesType("floor", googlePlacesGeneralType:.Location, googlePlacesType:.Floor, isOKToShowTypeName:true)
        createCLKGooglePlacesType("florist", googlePlacesGeneralType:.Business, googlePlacesType:.Florist, isOKToShowTypeName:true)
        createCLKGooglePlacesType("food", googlePlacesGeneralType:.Food, googlePlacesType:.Food, isOKToShowTypeName:true)
        createCLKGooglePlacesType("funeral_home", googlePlacesGeneralType:.Business, googlePlacesType:.Funeral_Home, isOKToShowTypeName:true)
        createCLKGooglePlacesType("furniture_store", googlePlacesGeneralType:.Shopping, googlePlacesType:.Furniture_Store, isOKToShowTypeName:true)
        createCLKGooglePlacesType("gas_station", googlePlacesGeneralType:.Business, googlePlacesType:.Gas_Station, isOKToShowTypeName:true)
        createCLKGooglePlacesType("general_contractor", googlePlacesGeneralType:.Business, googlePlacesType:.General_Contractor, isOKToShowTypeName:true)
        createCLKGooglePlacesType("geocode", googlePlacesGeneralType:.Location, googlePlacesType:.Geocode, isOKToShowTypeName:true)
        createCLKGooglePlacesType("grocery_or_supermarket", googlePlacesGeneralType:.Shopping, googlePlacesType:.Grocery_Or_Supermarket, isOKToShowTypeName:true)
        createCLKGooglePlacesType("gym", googlePlacesGeneralType:.Business, googlePlacesType:.Gym, isOKToShowTypeName:true)
        createCLKGooglePlacesType("hair_care", googlePlacesGeneralType:.Business, googlePlacesType:.Hair_Care, isOKToShowTypeName:true)
        createCLKGooglePlacesType("hardware_store", googlePlacesGeneralType:.Business, googlePlacesType:.Hardware_Store, isOKToShowTypeName:true)
        createCLKGooglePlacesType("health", googlePlacesGeneralType:.Health, googlePlacesType:.Health, isOKToShowTypeName:true)
        createCLKGooglePlacesType("hindu_temple", googlePlacesGeneralType:.Religious, googlePlacesType:.Hindu_Temple, isOKToShowTypeName:true)
        createCLKGooglePlacesType("home_goods_store", googlePlacesGeneralType:.Shopping, googlePlacesType:.Home_Goods_Store, isOKToShowTypeName:true)
        createCLKGooglePlacesType("hospital", googlePlacesGeneralType:.Health, googlePlacesType:.Hospital, isOKToShowTypeName:true)
        createCLKGooglePlacesType("insurance_agency", googlePlacesGeneralType:.Business, googlePlacesType:.Insurance_Agency, isOKToShowTypeName:true)
        createCLKGooglePlacesType("intersection", googlePlacesGeneralType:.Location, googlePlacesType:.Intersection, isOKToShowTypeName:true)
        createCLKGooglePlacesType("jewelry_store", googlePlacesGeneralType:.Shopping, googlePlacesType:.Jewelry_Store, isOKToShowTypeName:true)
        createCLKGooglePlacesType("laundry", googlePlacesGeneralType:.Business, googlePlacesType:.Laundry, isOKToShowTypeName:true)
        createCLKGooglePlacesType("lawyer", googlePlacesGeneralType:.Business, googlePlacesType:.Lawyer, isOKToShowTypeName:true)
        createCLKGooglePlacesType("library", googlePlacesGeneralType:.Public_Building, googlePlacesType:.Library, isOKToShowTypeName:true)
        createCLKGooglePlacesType("liquor_store", googlePlacesGeneralType:.Shopping, googlePlacesType:.Liquor_Store, isOKToShowTypeName:true)
        createCLKGooglePlacesType("local_government_office", googlePlacesGeneralType:.Public_Building, googlePlacesType:.Local_Government_office, isOKToShowTypeName:true)
        
        
        createCLKGooglePlacesType("locality", googlePlacesGeneralType:.Location, googlePlacesType:.Locality, isOKToShowTypeName:false)
        
        
        createCLKGooglePlacesType("locksmith", googlePlacesGeneralType:.Business, googlePlacesType:.Locksmith, isOKToShowTypeName:true)
        createCLKGooglePlacesType("lodging", googlePlacesGeneralType:.Accommodation, googlePlacesType:.Lodging, isOKToShowTypeName:true)
        createCLKGooglePlacesType("meal_delivery", googlePlacesGeneralType:.Food, googlePlacesType:.Meal_Delivery, isOKToShowTypeName:true)
        createCLKGooglePlacesType("meal_takeaway", googlePlacesGeneralType:.Food, googlePlacesType:.Meal_takeaway, isOKToShowTypeName:true)
        createCLKGooglePlacesType("mosque", googlePlacesGeneralType:.Religious, googlePlacesType:.Mosque, isOKToShowTypeName:true)
        createCLKGooglePlacesType("movie_rental", googlePlacesGeneralType:.Entertainment, googlePlacesType:.Movie_Rental, isOKToShowTypeName:true)
        createCLKGooglePlacesType("movie_theater", googlePlacesGeneralType:.Entertainment, googlePlacesType:.Movie_theater, isOKToShowTypeName:true)
        createCLKGooglePlacesType("moving_company", googlePlacesGeneralType:.Business, googlePlacesType:.Moving_Company, isOKToShowTypeName:true)
        createCLKGooglePlacesType("museum", googlePlacesGeneralType:.Public_Building, googlePlacesType:.Museum, isOKToShowTypeName:true)
        createCLKGooglePlacesType("natural_feature", googlePlacesGeneralType:.Location, googlePlacesType:.Natural_Feature, isOKToShowTypeName:true)
        
        createCLKGooglePlacesType("neighborhood", googlePlacesGeneralType:.Location, googlePlacesType:.Neighborhood, isOKToShowTypeName:false)
        
        createCLKGooglePlacesType("night_club", googlePlacesGeneralType:.Entertainment, googlePlacesType:.Night_Club, isOKToShowTypeName:true)
        createCLKGooglePlacesType("painter", googlePlacesGeneralType:.Business, googlePlacesType:.Painter, isOKToShowTypeName:true)
        createCLKGooglePlacesType("park", googlePlacesGeneralType:.Location, googlePlacesType:.Park, isOKToShowTypeName:true)
        createCLKGooglePlacesType("parking", googlePlacesGeneralType:.Transport, googlePlacesType:.Parking, isOKToShowTypeName:true)
        createCLKGooglePlacesType("pet_store", googlePlacesGeneralType:.Business, googlePlacesType:.Pet_Store, isOKToShowTypeName:true)
        createCLKGooglePlacesType("pharmacy", googlePlacesGeneralType:.Health, googlePlacesType:.Pharmacy, isOKToShowTypeName:true)
        createCLKGooglePlacesType("physiotherapist", googlePlacesGeneralType:.Health, googlePlacesType:.Physiotherapist, isOKToShowTypeName:true)
        createCLKGooglePlacesType("place_of_worship", googlePlacesGeneralType:.Religious, googlePlacesType:.Place_Of_Worship, isOKToShowTypeName:true)
        createCLKGooglePlacesType("plumber", googlePlacesGeneralType:.Business, googlePlacesType:.Plumber, isOKToShowTypeName:true)
        
        //createCLKGooglePlacesType("point_of_interest", googlePlacesGeneralType:.Point_Of_Interest, googlePlacesType:.Point_Of_interest, isOKToShowTypeName:false)
        createCLKGooglePlacesType(CLKGooglePlacesTypesManager.kPOINT_OF_INTEREST, googlePlacesGeneralType:.Point_Of_Interest, googlePlacesType:.Point_Of_interest, isOKToShowTypeName:false)
        
        createCLKGooglePlacesType("police", googlePlacesGeneralType:.Public_Building, googlePlacesType:.Police, isOKToShowTypeName:true)
        
        createCLKGooglePlacesType("political", googlePlacesGeneralType:.Location, googlePlacesType:.Political, isOKToShowTypeName:false)
        
        createCLKGooglePlacesType("post_box", googlePlacesGeneralType:.Post, googlePlacesType:.Post_Box, isOKToShowTypeName:true)
        createCLKGooglePlacesType("post_office", googlePlacesGeneralType:.Post, googlePlacesType:.Post_Office, isOKToShowTypeName:true)
        
        createCLKGooglePlacesType("postal_code", googlePlacesGeneralType:.Location, googlePlacesType:.Postal_Code, isOKToShowTypeName:false)
        createCLKGooglePlacesType("postal_code_prefix", googlePlacesGeneralType:.Location, googlePlacesType:.Postal_Code_Prefix, isOKToShowTypeName:false)
        createCLKGooglePlacesType("postal_code_suffix", googlePlacesGeneralType:.Location, googlePlacesType:.Postal_Code_Suffix, isOKToShowTypeName:false)
        createCLKGooglePlacesType("postal_town", googlePlacesGeneralType:.Location, googlePlacesType:.Postal_town, isOKToShowTypeName:false)
        //?
        createCLKGooglePlacesType("premise", googlePlacesGeneralType:.Location, googlePlacesType:.Premise, isOKToShowTypeName:true)
        
        createCLKGooglePlacesType("real_estate_agency", googlePlacesGeneralType:.Business, googlePlacesType:.Real_Estate_Agency, isOKToShowTypeName:true)
        createCLKGooglePlacesType("restaurant", googlePlacesGeneralType:.Food_Drink, googlePlacesType:.Restaurant, isOKToShowTypeName:true)
        createCLKGooglePlacesType("roofing_contractor", googlePlacesGeneralType:.Business, googlePlacesType:.Roofing_Contractor, isOKToShowTypeName:true)
        createCLKGooglePlacesType("room", googlePlacesGeneralType:.Accommodation, googlePlacesType:.Room, isOKToShowTypeName:true)
        createCLKGooglePlacesType("route", googlePlacesGeneralType:.Location, googlePlacesType:.Route, isOKToShowTypeName:true)
        createCLKGooglePlacesType("rv_park", googlePlacesGeneralType:.Location, googlePlacesType:.Rv_Park, isOKToShowTypeName:true)
        createCLKGooglePlacesType("school", googlePlacesGeneralType:.Public_Building, googlePlacesType:.School, isOKToShowTypeName:true)
        createCLKGooglePlacesType("shoe_store", googlePlacesGeneralType:.Business, googlePlacesType:.Shoe_Store, isOKToShowTypeName:true)
        createCLKGooglePlacesType("shopping_mall", googlePlacesGeneralType:.Shopping, googlePlacesType:.Shopping_Mall, isOKToShowTypeName:true)
        createCLKGooglePlacesType("spa", googlePlacesGeneralType:.Business, googlePlacesType:.Spa, isOKToShowTypeName:true)
        createCLKGooglePlacesType("stadium", googlePlacesGeneralType:.Public_Building, googlePlacesType:.Stadium, isOKToShowTypeName:true)
        createCLKGooglePlacesType("storage", googlePlacesGeneralType:.Business, googlePlacesType:.Storage, isOKToShowTypeName:true)
        createCLKGooglePlacesType("store", googlePlacesGeneralType:.Shopping, googlePlacesType:.Store, isOKToShowTypeName:true)
        
        createCLKGooglePlacesType("street_address", googlePlacesGeneralType:.Location, googlePlacesType:.Street_Address, isOKToShowTypeName:false)
        createCLKGooglePlacesType("street_number", googlePlacesGeneralType:.Location, googlePlacesType:.Street_Number, isOKToShowTypeName:false)
        createCLKGooglePlacesType("sublocality", googlePlacesGeneralType:.Location, googlePlacesType:.Sublocality, isOKToShowTypeName:false)
        createCLKGooglePlacesType("sublocality_level_1", googlePlacesGeneralType:.Location, googlePlacesType:.Sublocality_Level_1, isOKToShowTypeName:false)
        createCLKGooglePlacesType("sublocality_level_2", googlePlacesGeneralType:.Location, googlePlacesType:.Sublocality_Level_2, isOKToShowTypeName:false)
        createCLKGooglePlacesType("sublocality_level_3", googlePlacesGeneralType:.Location, googlePlacesType:.Sublocality_Level_3, isOKToShowTypeName:false)
        createCLKGooglePlacesType("sublocality_level_4", googlePlacesGeneralType:.Location, googlePlacesType:.Sublocality_Level_4, isOKToShowTypeName:false)
        createCLKGooglePlacesType("sublocality_level_5", googlePlacesGeneralType:.Location, googlePlacesType:.Sublocality_Level_5, isOKToShowTypeName:false)
        createCLKGooglePlacesType("subpremise", googlePlacesGeneralType:.Location, googlePlacesType:.SubPremise, isOKToShowTypeName:true)
        
        createCLKGooglePlacesType("subway_station", googlePlacesGeneralType:.Transport, googlePlacesType:.Subway_Station, isOKToShowTypeName:true)
        createCLKGooglePlacesType("synagogue", googlePlacesGeneralType:.Religious, googlePlacesType:.Synagogue, isOKToShowTypeName:true)
        createCLKGooglePlacesType("taxi_stand", googlePlacesGeneralType:.Transport, googlePlacesType:.Taxi_Stand, isOKToShowTypeName:true)
        createCLKGooglePlacesType("train_station", googlePlacesGeneralType:.Transport, googlePlacesType:.Train_Station, isOKToShowTypeName:true)
        createCLKGooglePlacesType("transit_station", googlePlacesGeneralType:.Transport, googlePlacesType:.Transit_Station, isOKToShowTypeName:true)
        createCLKGooglePlacesType("travel_agency", googlePlacesGeneralType:.Business, googlePlacesType:.Travel_Agency, isOKToShowTypeName:true)
        createCLKGooglePlacesType("university", googlePlacesGeneralType:.Public_Building, googlePlacesType:.University, isOKToShowTypeName:true)
        createCLKGooglePlacesType("veterinary_care", googlePlacesGeneralType:.Business, googlePlacesType:.Veterinary_Care, isOKToShowTypeName:true)
        createCLKGooglePlacesType("zoo", googlePlacesGeneralType:.Entertainment, googlePlacesType:.Zoo, isOKToShowTypeName:true)
        
        
        //MY DEFAULTS - NOT IN GOOGLES LIST
        createCLKGooglePlacesType(CLKGooglePlacesTypesManager.kBUSINESS, googlePlacesGeneralType:.Business, googlePlacesType:.Business, isOKToShowTypeName:true)
        
        createCLKGooglePlacesType(CLKGooglePlacesTypesManager.kBUSINESS_OR_POI, googlePlacesGeneralType:.Business_Or_POI, googlePlacesType:.Business_Or_POI, isOKToShowTypeName:true)
        
        
        //neighbourhood etc are already .Locations above
        //        createCLKGooglePlacesType(CLKGooglePlacesTypes.kLOCATION, googlePlacesGeneralType:.Location, googlePlacesType:.Location, isOKToShowTypeName:true)


    }
    override init(){
        super.init()
    
        configureTypes()

    }
    
    func createCLKGooglePlacesType(_ name: String, googlePlacesGeneralType:GooglePlacesType, googlePlacesType:GooglePlacesType, isOKToShowTypeName: Bool){
        
        googlePlacesTypesDictionary[name] = CLKGooglePlacesType(name: name, googlePlacesType:googlePlacesType, googlePlacesGeneralType: googlePlacesGeneralType, isOKToShowTypeName:isOKToShowTypeName)

    }
    
    func contains(_ arrayToSearch : [String], stringToSearchFor: String) -> Bool{
        var found = false
        
        for stringFound in arrayToSearch{
            if stringFound == stringToSearchFor{
                found = true
                break
            }
        }
        return found
    }
    
    
    
    //convert string in types array to enum types / excludeing exceptions
    //place can have more than one type
    //but some are too general 'establishment' so leave out UNLESS its the only type then must include
    
    func finalTypesForGooglePlaceTypes(_ types : [String]) -> [CLKGooglePlacesType]?{
        var clkGooglePlacesTypeArrayReturned : [CLKGooglePlacesType] = [CLKGooglePlacesType]()
        
        if types.count > 0{
            for typeString in types{
                if let clkGooglePlacesType : CLKGooglePlacesType = self.googlePlacesTypesDictionary[typeString]{
                    
                    if clkGooglePlacesType.isOKToShowTypeName{
                        clkGooglePlacesTypeArrayReturned.append(clkGooglePlacesType)
                    }else{
                        //self.log.error("[\(typeString)]clkGooglePlacesType.isOKToShowTypeName is nil - SKIP")
                    }
                }else{
                    self.log.error("CLKGooglePlacesType is nil for typeString:\(typeString)")
                }
            }
        }else{
            self.log.error("types.count > 0 failed - NOT TYPES IN RESULT")
        }
        
        //Now handle cases where types only includes excluded types such as "point_of_interest", "establishment"
        //if this happens then we should have nothing in clkGooglePlacesTypeArrayReturned after first loop
        if clkGooglePlacesTypeArrayReturned.count == 0{
            
            //self.log.debug("TYPES NEED DEFAULTS:\(types)")
            
            //Exceptions
            //    TYPES NEED DEFAULTS:["neighborhood", "political"]
            //    TYPES NEED DEFAULTS:["establishment"]
            //    TYPES NEED DEFAULTS:["point_of_interest", "establishment"]
            //contains(arrayToSearch : [String], stringToSearchFor: String)
            
//            if types.count == 2 && self.contains(types, stringToSearchFor: "neighborhood") && self.contains(types, stringToSearchFor: "political"){
//                self.log.debug("CHANGING:\(types) to 'location'")
//                if let clkGooglePlacesType : CLKGooglePlacesType = self.googlePlacesTypesDictionary[CLKGooglePlacesTypes.kLOCATION]{
//                    //add back in excluded types else place will have no types
//                    clkGooglePlacesTypeArrayReturned.append(clkGooglePlacesType)
//                }else{
//                    self.log.error("CLKGooglePlacesType is nil for typeString:\(CLKGooglePlacesTypes.kLOCATION)")
//                }
//            }
//            else
            
            if types.count == 1 && self.contains(types, stringToSearchFor: "establishment"){
                //"establishment" is flagged as do not show in results but some rwos are only "establishment" - so convert them to business
                //self.log.debug("CHANGING:\(types) to 'business'")
                if let clkGooglePlacesType : CLKGooglePlacesType = self.googlePlacesTypesDictionary[CLKGooglePlacesTypesManager.kBUSINESS]{
                    //add back in excluded types else place will have no types
                    clkGooglePlacesTypeArrayReturned.append(clkGooglePlacesType)
                }else{
                    self.log.error("CLKGooglePlacesType is nil for typeString:\(CLKGooglePlacesTypesManager.kBUSINESS)")
                }
            }
            
            //REMOVE - Buckingham palace became a "Business"
            else if types.count == 2 && self.contains(types, stringToSearchFor: "point_of_interest") && self.contains(types, stringToSearchFor: "establishment")
            {
                //self.log.debug("CHANGING:\(types) to 'point_of_interest business'")
                //---------------------------------------------------------------------
                //CHANGE establishment TO BUSINESS OR POI
                if let clkGooglePlacesType : CLKGooglePlacesType = self.googlePlacesTypesDictionary[CLKGooglePlacesTypesManager.kBUSINESS_OR_POI]{
                    //add back in excluded types else place will have no types
                    clkGooglePlacesTypeArrayReturned.append(clkGooglePlacesType)
                }else{
                    self.log.error("CLKGooglePlacesType is nil for typeString:\(CLKGooglePlacesTypesManager.kBUSINESS_OR_POI)")
                }
                //---------------------------------------------------------------------
//                //add POI as Buckingham Palace
//                if let clkGooglePlacesType : CLKGooglePlacesType = self.googlePlacesTypesDictionary[CLKGooglePlacesTypes.kPOINT_OF_INTEREST]{
//                    //add back in excluded types else place will have no types
//                    clkGooglePlacesTypeArrayReturned.append(clkGooglePlacesType)
//                }else{
//                    self.log.error("CLKGooglePlacesType is nil for typeString:\(CLKGooglePlacesTypes.kPOINT_OF_INTEREST)")
//                }
                //---------------------------------------------------------------------
            }
            else {
                //all the rest: check the types
                for typeString in types{
                    if let clkGooglePlacesType : CLKGooglePlacesType = self.googlePlacesTypesDictionary[typeString]{
                        //add back in excluded types else place will have no types
                        clkGooglePlacesTypeArrayReturned.append(clkGooglePlacesType)
                    }else{
                        self.log.error("CLKGooglePlacesType is nil for typeString:\(typeString)")
                    }
                }
            }

        }else{
            //no error - types successfully set
        }
        
        return clkGooglePlacesTypeArrayReturned
    }
}
