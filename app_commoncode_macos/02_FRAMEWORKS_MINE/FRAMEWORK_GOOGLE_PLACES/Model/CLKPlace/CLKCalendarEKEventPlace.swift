//
//  CLKCalendarEKEventPlace.swift
//  joyride
//
//  Created by Brian Clear on 18/10/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import EventKit


class CLKCalendarEKEventPlace : CLKPlace{
    
    //must be set by init
    //subclass of EKEvent: EKCalendar
    var ekEvent: EKEvent
    
    init(ekEvent: EKEvent) {
        
        self.ekEvent = ekEvent
        
        //set clLocation and name to nil d
        super.init(clLocation:nil, name: nil, clkPlaceWrapperType: .geocodedCalendarEvent)
        
    }
    
    override var clLocation :CLLocation? {
        get {
            //---------------------------------------------------------
            var clLocationReturned :CLLocation?
            
            //---------------------------------------------------------------------
            //this has and address sting but we dont use it
            if let structuredLocation: EKStructuredLocation = ekEvent.structuredLocation {
                if let geoLocation = structuredLocation.geoLocation {
                    if geoLocation.isValid{
                        clLocationReturned = geoLocation
                        
                    }else{
                        appDelegate.log.error("structuredLocation.geoLocation.isValid - failed - required geocode")
                    }
                }else{
                    appDelegate.log.error("structuredLocation.geoLocation is nil - required geocode")
                }
            }else{
                appDelegate.log.error("ekEvent.structuredLocation is nil - required geocode")
            }

            //------------------------------------------------------------------------------------------------
            return clLocationReturned
            //------------------------------------------------------------------------------------------------
        }
        
    }

    
    //--------------------------------------------------------------
    // MARK: - name
    // MARK: -
    //--------------------------------------------------------------
    
    fileprivate var _name: String?
    
    override var name :String? {
        get {
            //---------------------------------------------------------
            var nameReturned :String?
            
            //changed by SET - eg Edit Name
            if let _name = self._name {
                nameReturned = _name
            }else{
                //------------------------------------------------------------------------------------------------
                //GET FROM INTERNAL TYPE
                //------------------------------------------------------------------------------------------------
                //nameReturned = self.ekEvent.title
                //v1 - wrong never show Calendar event name or address title - you get 15 Royal Mint St, 15 Royal Mint street
                
                
                //v2 - address flow
                
                
                if let structuredLocation = self.ekEvent.structuredLocation {
                    if let _ = structuredLocation.geoLocation {
                        appDelegate.log.error("self.ekEvent.structuredLocation is set - should not be in here - should reverseGeodoe this is forward geocode of address string")
                    }else{
                        //----------------------------------------------------------------------------------------
                        // location set but lat/lng blank
                        //----------------------------------------------------------------------------------------
                        /*
                         location = 	Wyndham's Theatre, London, England, United Kingdom;
                         structuredLocation = 	EKStructuredLocation <0x1706bf320> {title = Wyndham's Theatre, London, England, United Kingdom; address = (null); geo = (null); abID = (null); routing = (null); radius = 0.000000;};
                         */
                        //----------------------------------------------------------------------------------------
                        if structuredLocation.title.trim() == "" {
                            appDelegate.log.error("structuredLocation.title is '' - lat/lng is nil - cant forward geocode event place")
                            nameReturned  = nil
                        }else{
                            //THE NAME of the place is the event title e.g. 'Wyndham's Theatre, London, England, United Kingdom'
                            //which MKLocalSearch should find
                            nameReturned = structuredLocation.title
                            
                        }
                        //----------------------------------------------------------------------------------------
                    }
                }else{
                    appDelegate.log.error("self.ekEvent.structuredLocation is nil")
                    nameReturned  = nil
                }
            }
            //------------------------------------------------------------------------------------------------
            return nameReturned
            //------------------------------------------------------------------------------------------------
        }//get
    }
    
    
    //--------------------------------------------------------------
    // formatted_address
    //--------------------------------------------------------------
    //one in base class uses clkGeocodedAddress = self.clkGeocodedAddress correctly
    //    override var formatted_address :String? {
    //
    //}
    //--------------------------------------------------------------


}
/*
EKEvent <0x1653e1b80>
    {
        EKEvent <0x1653e1b80>
            {	 title = 		Jridebc;
                location = 	Brian's Home
                15 Royal Mint Street
                London
                E1 8LG
                United Kingdom;
                calendar = 	EKCalendar <0x165237410> {title = Work; type = CalDAV; allowsModify = YES; color = #CC73E1;};
                alarms = 		(null);
                URL = 			(null);
                lastModified = 2016-11-09 15:18:10 +0000;
                startTimeZone = 	Europe/London (GMT) offset 0;
                startTimeZone = 	Europe/London (GMT) offset 0
        };
        location = 	Brian's Home
        15 Royal Mint Street
        London
        E1 8LG
        United Kingdom;
        structuredLocation = 	EKStructuredLocation <0x163d91690> {title = Brian's Home; address = 15 Royal Mint Street
            London
            E1 8LG
            United Kingdom; geo = <+51.51012600,-0.07133700> +/- 0.00m (speed -1.00 mps / course -1.00) @ 09/11/2016, 16:54:47 Greenwich Mean Time; abID = ab://Brian's%20Home; routing = (null); radius = 0.000000;};
            startDate = 	2016-11-09 15:00:00 +0000; 
            endDate = 		2016-11-09 16:00:00 +0000; 
            allDay = 		0; 
            floating = 	0; 
            recurrence = 	(null); 
            attendees = 	(null); 
            travelTime = 	(null); 
            startLocation = 	(null);
        };
*/
