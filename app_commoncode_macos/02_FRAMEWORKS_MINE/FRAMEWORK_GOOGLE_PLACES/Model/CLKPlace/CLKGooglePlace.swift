//
//  CLKGooglePlace.swift
//  joyride
//
//  Created by Brian Clear on 14/10/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

class CLKGooglePlace: CLKPlace{
   
    //case NearbySearch_CLKGooglePlaceResult_GeoCodeLocation /* used name from Nearby search but address for GeoCodeLocation*/

    fileprivate var clkGooglePlaceResult :CLKGooglePlaceResult?
    
    init(clkGooglePlaceResult: CLKGooglePlaceResult) {
        
        //set name to nil so getName will use CLKGooglePlaceResult.name
        super.init(clLocation: nil, name: nil, clkPlaceWrapperType: .nearbySearch_CLKGooglePlaceResult)
        
        self.clkGooglePlaceResult = clkGooglePlaceResult

    }
    
    //--------------------------------------------------------------
    // MARK: - ivar: place_id
    // MARK: -
    //--------------------------------------------------------------
    fileprivate var _place_id: String?
    var place_id :String? {
        get {
            //---------------------------------------------------------
            var place_idReturned :String?
        
            if let clkGooglePlaceResult = clkGooglePlaceResult{
                place_idReturned = clkGooglePlaceResult.place_id
            }else{
                appDelegate.log.error("clkGooglePlaceResult is nil")
            }
            return place_idReturned
            //---------------------------------------------------------
        }
//        set {
//            //PASS INTO INTERNAL TYPE?
        //Realm constructor needed
        
        
//            //if let clkGooglePlaceResult = clkGooglePlaceResult{
//            //                    clkGooglePlaceResult.place_id = newValue
//            //                }else{
//            //                    appDelegate.log.error("clkGooglePlaceResult is nil")
//            //                }
//            _place_id = newValue
//        }
    }
    
 
    //--------------------------------------------------------------
    // MARK: - name
    // MARK: -
    //--------------------------------------------------------------
    override var name :String? {
        get {
            //---------------------------------------------------------
            var nameReturned :String?
            
            //changed by SET - eg Edit Name
            if let name = super.name {
                nameReturned = name
                
            }else{
                //------------------------------------------------
                //GET FROM INTERNAL TYPE
                //------------------------------------------------
                if let clkGooglePlaceResult = clkGooglePlaceResult{

                    nameReturned = clkGooglePlaceResult.name
                    
                }else{
                    appDelegate.log.error("[NearbySearch_CLKGooglePlaceResult] clkGooglePlaceResult is nil - cant return nameReturned")
                }
            }
            //-----------------------------------------------------
            return nameReturned
            //----------------------------------------------------------
        }
        //        set {
        //            /*
        //            //------------------------------------------------------------------------------------------------
        //            //PASS INTO INTERNAL TYPE
        //            //------------------------------------------------------------------------------------------------
        //            //---------------------------------------------------------------------
        //            //CHANGED BY EDIT
        //            _name = newValue
        //            //------------------------------------------------------------------------------------------------
        //        }
    }
    
    
    var clkGoogleDirectionsLocation : CLKGoogleDirectionsLocation?{
        var clkGoogleDirectionsLocation : CLKGoogleDirectionsLocation?
        
 // TODO: - needed?
//        switch clkPlaceWrapperType{
//        case .clkPlace_Default:
//            //------------------------------------------------
//            if let place_id = self.place_id{
//                if place_id == "" {
//                    appDelegate.log.error("place_id is nil")
//                }else{
//                    
//                    clkGoogleDirectionsLocation = CLKGoogleDirectionsLocation(placeId: place_id)
//                }
//                
//            }
//            else if let clLocation = self.clLocation{
//                clkGoogleDirectionsLocation = CLKGoogleDirectionsLocation(clLocation: clLocation)
//            }
//            else if let formatted_address = self.formatted_address{
//                clkGoogleDirectionsLocation = CLKGoogleDirectionsLocation(addressString: formatted_address)
//            }
//            else
//            {
//                appDelegate.log.error("UNABLE TO CONVERT CLKPlace to CLKGoogleDirectionsLocation")
//            }
//        //------------------------------------------------
//        case .geoCodeLocation:
//            //------------------------------------------------
//            if let clLocation = self.clLocation{
//                clkGoogleDirectionsLocation = CLKGoogleDirectionsLocation(clLocation: clLocation)
//            }
//            else
//            {
//                appDelegate.log.error("UNABLE TO CONVERT CLKPlace to CLKGoogleDirectionsLocation")
//            }
//            //------------------------------------------------
//            
//        case .nearbySearch_CLKGooglePlaceResult:
//            //--------------------------------------------------
//            if let place_id = self.place_id{
//                clkGoogleDirectionsLocation = CLKGoogleDirectionsLocation(placeId: place_id)
//            }
//            else
//            {
//                appDelegate.log.error("UNABLE TO CONVERT CLKPlace to CLKGoogleDirectionsLocation")
//            }
//        //--------------------------------------------------
//        case .nearbySearch_CLKGooglePlaceResult_GeoCodeLocation:
//            //--------------------------------------------------
//            //------------------------------------------------
//            if let place_id = self.place_id{
//                clkGoogleDirectionsLocation = CLKGoogleDirectionsLocation(placeId: place_id)
//            }
//            else if let clLocation = self.clLocation{
//                clkGoogleDirectionsLocation = CLKGoogleDirectionsLocation(clLocation: clLocation)
//            }
//            else
//            {
//                appDelegate.log.error("UNABLE TO CONVERT CLKPlace to CLKGoogleDirectionsLocation")
//            }
//            //--------------------------------------------------
//            
//            //        case .placesPicker_GMSPlace:
//            //            if let place_id = self.place_id{
//            //                clkGoogleDirectionsLocation = CLKGoogleDirectionsLocation(placeId: place_id)
//            //            }
//            //            else
//            //            {
//            //                appDelegate.log.error("UNABLE TO CONVERT CLKPlace to CLKGoogleDirectionsLocation")
//            //            }
//            //            //--------------------------------------------------
//            
//        case .savedPlace_DBPlace:
//            if let clLocation = self.clLocation{
//                clkGoogleDirectionsLocation = CLKGoogleDirectionsLocation(clLocation: clLocation)
//            }
//            else
//            {
//                appDelegate.log.error("UNABLE TO CONVERT CLKPlace to CLKGoogleDirectionsLocation")
//            }
//        case .savedPlace_CLKGooglePlaceResult:
//            if let place_id = self.place_id{
//                clkGoogleDirectionsLocation = CLKGoogleDirectionsLocation(placeId: place_id)
//            }
//            else
//            {
//                appDelegate.log.error("UNABLE TO CONVERT CLKPlace to CLKGoogleDirectionsLocation")
//            }
//        //--------------------------------------------------
//        case .geocodedContact:
//            if let clLocation = self.clLocation{
//                clkGoogleDirectionsLocation = CLKGoogleDirectionsLocation(clLocation: clLocation)
//            }
//            else
//            {
//                appDelegate.log.error("UNABLE TO CONVERT CLKPlace to CLKGoogleDirectionsLocation")
//            }
//        //--------------------------------------------------
//        case .geocodedCalendarEvent:
//            if let clLocation = self.clLocation{
//                clkGoogleDirectionsLocation = CLKGoogleDirectionsLocation(clLocation: clLocation)
//            }
//            else
//            {
//                appDelegate.log.error("UNABLE TO CONVERT CLKPlace to CLKGoogleDirectionsLocation")
//            }
//            
//            
//            
//            //--------------------------------------------------
//            //        case .colcFacebookEvent:
//            //            if let clLocation = self.clLocation{
//            //                clkGoogleDirectionsLocation = CLKGoogleDirectionsLocation(clLocation: clLocation)
//            //            }
//            //            else
//            //            {
//            //                appDelegate.log.error("UNABLE TO CONVERT CLKPlace to CLKGoogleDirectionsLocation")
//            //            }
//            //--------------------------------------------------
//            //        case .colcFacebookEvent_GeoCodeLocation:
//            //            if let clLocation = self.clLocation{
//            //                clkGoogleDirectionsLocation = CLKGoogleDirectionsLocation(clLocation: clLocation)
//            //            }
//            //            else
//            //            {
//            //                appDelegate.log.error("UNABLE TO CONVERT CLKPlace to CLKGoogleDirectionsLocation")
//        //            }
//        default:
//            appDelegate.log.error("CLKPLACE SUBCLASS clkGoogleDirectionsLocation")
//        }
        // TODO: - cleanup - I create think when I create DirectioNRequest but here it uses a placeId which may be more accurate
        if let place_id = self.place_id{
            clkGoogleDirectionsLocation = CLKGoogleDirectionsLocation(placeId: place_id)
        }
        else
        {
            appDelegate.log.error("UNABLE TO CONVERT CLKPlace to CLKGoogleDirectionsLocation")
        }

        
        return clkGoogleDirectionsLocation
    }

    
    //--------------------------------------------------------------
    // MARK: - FORMATTED ADDRESS
    // MARK: -
    //--------------------------------------------------------------
    
//    var formatted_address_street : String = ""
//    
//    //--------------------------------------------------------------
//    
    
    
    // TODO: - setter used this but is setter read only subclass and parent both have this var _formatted_address
//    fileprivate var _formatted_address: String?
    override var formatted_address :String? {
        get {
            //---------------------------------------------------------
            var formatted_addressReturned :String?

            if let clkGooglePlaceResult = clkGooglePlaceResult{
                
                //dont use - will have more in but bus stop "London" >> "London E1W" - better to use nearest street address
                
                // TODO: - hwy do we get address detail AND Nearest street address
                
                //    if let address_components = clkGooglePlaceResult.address_components {
                //        formatted_addressReturned = "\(address_components)"
                //    }else{
                //        appDelegate.log.error("self.address_components is nil")
                //    }
                
                //note: for bus stops may still return just 'London'
                formatted_addressReturned = clkGooglePlaceResult.formatted_address
                
            }else{
                appDelegate.log.error("[NearbySearch_CLKGooglePlaceResult] clkGooglePlaceResult is nil - cant return formatted_addressReturned")
            }
            
            // TODO: - EDIT ADDRESS - call super.name if nil then no changed by EDIT i think
            
            
            //------------------------------------------------------------------------------------------------
            return formatted_addressReturned
            //------------------------------------------------------------------------------------------------
        }
        set {
            // TODO: - RealDB load from DB - sets address
            
            // TODO: - ADDRESS
            
//            //------------------------------------------------------------------------------------------------
//            //PASS INTO INTERNAL TYPE
//            //------------------------------------------------------------------------------------------------
//            switch clkPlaceWrapperType{
//            case .clkPlace_Default:
//                //-------------------------------------------------
//                //no internal object - just store in private ivar
//                _formatted_address = newValue
//                //-------------------------------------------------
//                
//            case .nearbySearch_CLKGooglePlaceResult:
//                //--------------------------------------------------
//                if let clkGooglePlaceResult = clkGooglePlaceResult{
//                    clkGooglePlaceResult.formatted_address = newValue
//                }else{
//                    appDelegate.log.error("clkGooglePlaceResult is nil")
//                }
//            //--------------------------------------------------
//            case .nearbySearch_CLKGooglePlaceResult_GeoCodeLocation:
//                //--------------------------------------------------
//                if let clkGooglePlaceResult = clkGooglePlaceResult{
//                    clkGooglePlaceResult.formatted_address = newValue
//                }else{
//                    appDelegate.log.error("clkGooglePlaceResult is nil")
//                }
//                //--------------------------------------------------
//                
//                //            case .placesPicker_GMSPlace:
//                //                //--------------------------------------------------
//                //                if let _ = gmsPlace{
//                //                    appDelegate.log.error("gmsPlace.placeID is readonly - newValue stores in ivar but may be overwritte when we call get again")
//                //                    //gmsPlace.placeID = newValue
//                //
//                //                }else{
//                //                    appDelegate.log.error("clkGooglePlaceResult is nil")
//                //                }
//                
//            case .geoCodeLocation:
//                _formatted_address = newValue
//                //--------------------------------------------------
//                //            case .savedPlace_DBPlace:
//                //                _formatted_address = newValue
//                
//            case .savedPlace_CLKGooglePlaceResult:
//                //--------------------------------------------------
//                if let clkGooglePlaceResult = clkGooglePlaceResult{
//                    clkGooglePlaceResult.formatted_address = newValue
//                }else{
//                    appDelegate.log.error("clkGooglePlaceResult is nil")
//                }
//            //--------------------------------------------------
//            case .geocodedContact:
//                _formatted_address = newValue
//            case .geocodedCalendarEvent:
//                _formatted_address = newValue
//                //--------------------------------------------------
//                //            case .colcFacebookEvent:
//                //                _formatted_address = newValue
//                //            case .colcFacebookEvent_GeoCodeLocation:
//            //                _formatted_address = newValue
//            default:
//                appDelegate.log.error("CLKPLACE SUBCLASS _formatted_address")
//            }
            
            // TODO: - readonly?
            //_formatted_address = newValue
            //store in private _formatted_address in parent
            super.formatted_address = newValue
            //------------------------------------------------------------------------------------------------
        }
    }
    
    
    //------------------------------------------------------------------------------------------------
    //clLocation CLLocation
    //------------------------------------------------------------------------------------------------
    
    // TODO: - in pareent as well!
    fileprivate var _clLocation: CLLocation?
    
    // TODO: - remove set - change to init
    override var clLocation :CLLocation? {
        get {
            //---------------------------------------------------------
            var clLocationReturned :CLLocation?
            
//            //------------------------------------------------------------------------------------------------
//            //GET FROM INTERNAL TYPE
//            //------------------------------------------------------------------------------------------------
//            switch clkPlaceWrapperType{
//            case .clkPlace_Default:
//                //-------------------------------------------------
//                clLocationReturned = _clLocation
//                //-------------------------------------------------
//                
//            case .geoCodeLocation:
//                //-------------------------------------------------
//                clLocationReturned = _clLocation
//                //-------------------------------------------------
//                
//            case .nearbySearch_CLKGooglePlaceResult:
//                //--------------------------------------------------
//                if let clkGooglePlaceResult = clkGooglePlaceResult{
//                    clLocationReturned = clkGooglePlaceResult.clLocation
//                }else{
//                    appDelegate.log.error("clkGooglePlaceResult is nil")
//                }
//            //--------------------------------------------------
//            case .nearbySearch_CLKGooglePlaceResult_GeoCodeLocation:
//                //--------------------------------------------------
//                if let clkGooglePlaceResult = clkGooglePlaceResult{
//                    clLocationReturned = clkGooglePlaceResult.clLocation
//                }else{
//                    appDelegate.log.error("clkGooglePlaceResult is nil")
//                }
//                //--------------------------------------------------
//                
//                //            case .placesPicker_GMSPlace:
//                //                //--------------------------------------------------
//                //                if let gmsPlace = gmsPlace{
//                //
//                //                    //coordinate: CLLocationCoordinate2D
//                //                    let clLocationFromCoord = CLLocation(latitude: gmsPlace.coordinate.latitude,
//                //                        longitude: gmsPlace.coordinate.longitude)
//                //
//                //
//                //                    clLocationReturned = clLocationFromCoord
//                //                }else{
//                //                    appDelegate.log.error("clkGooglePlaceResult is nil")
//                //                }
//                
//                //            case .savedPlace_DBPlace:
//                //                //--------------------------------------------------
//                //                if let dbPlace = self.dbPlace{
//                //
//                //                    //coordinate: CLLocationCoordinate2D
//                //                    let clLocationFromCoord = CLLocation(latitude: dbPlace.latitude,
//                //                                                        longitude: dbPlace.longitude)
//                //
//                //                    clLocationReturned = clLocationFromCoord
//                //
//                //                }else{
//                //                    appDelegate.log.error("self.dbPlace is nil")
//                //                }
//            //--------------------------------------------------
//            case .savedPlace_CLKGooglePlaceResult:
//                //--------------------------------------------------
//                if let clkGooglePlaceResult = clkGooglePlaceResult{
//                    clLocationReturned = clkGooglePlaceResult.clLocation
//                }else{
//                    appDelegate.log.error("clkGooglePlaceResult is nil")
//                }
//            //--------------------------------------------------
//            case .geocodedContact:
//                if let colcContact = self.colcContact{
//                    if let clLocation = colcContact.clLocation{
//                        clLocationReturned = clLocation
//                    }else{
//                        appDelegate.log.error("colcContact.clLocation is nil")
//                    }
//                }else{
//                    appDelegate.log.error("self.colcContact is nil")
//                }
//                
//            case .geocodedCalendarEvent:
//                if let colcGeocodedCalendarEvent = self.colcGeocodedCalendarEvent{
//                    if let clLocation = colcGeocodedCalendarEvent.clLocation{
//                        clLocationReturned = clLocation
//                    }else{
//                        appDelegate.log.error("colcGeocodedCalendarEvent.clLocation is nil")
//                    }
//                }else{
//                    appDelegate.log.error("self.colcContact is nil")
//                }
//                
//                //            case .colcFacebookEvent:
//                //                if let colcFacebookEvent = self.colcFacebookEvent{
//                //                    if let clLocation = colcFacebookEvent.clLocation {
//                //                        clLocationReturned = clLocation
//                //                    }else{
//                //                        appDelegate.log.error("colcFacebookEvent.clLocation is nil")
//                //                    }
//                //                }else{
//                //                    appDelegate.log.error("self.colcFacebookEvent is nil")
//                //                }
//                //            case .colcFacebookEvent_GeoCodeLocation:
//                //                if let colcFacebookEvent = self.colcFacebookEvent{
//                //                    //ok should be same cllocation as geoocoded address
//                //                    if let clLocation = colcFacebookEvent.clLocation {
//                //                        clLocationReturned = clLocation
//                //                    }else{
//                //                        appDelegate.log.error("colcFacebookEvent.clLocation is nil")
//                //
//                //                        if let googleGeocodedGMSAddress = self.googleGeocodedGMSAddress {
//                //                            clLocationReturned = CLLocation(latitude: googleGeocodedGMSAddress.coordinate.latitude, longitude: googleGeocodedGMSAddress.coordinate.longitude)
//                //                        }else{
//                //                            appDelegate.log.error("colcFacebookEvent.clLocation and self.googleGeocodedGMSAddress is nil - cant return CLKPlace.clLocation")
//                //                        }
//                //                    }
//                //
//                //                }else{
//                //                    appDelegate.log.error("self.colcFacebookEvent is nil")
//            //                }
//            default:
//                appDelegate.log.error("CLKPLACE SUBCLASS clkGoogleDirectionsLocation")
//            }
            
            if let clkGooglePlaceResult = clkGooglePlaceResult{
                clLocationReturned = clkGooglePlaceResult.clLocation
            }else{
                appDelegate.log.error("clkGooglePlaceResult is nil")
            }
            
            
            
            //------------------------------------------------------------------------------------------------
            return clLocationReturned
            //------------------------------------------------------------------------------------------------
        }
        // TODO: - readonly
        //        set {
        //
        //            //------------------------------------------------------------------------------------------------
        //            //PASS INTO INTERNAL TYPE
        //            //------------------------------------------------------------------------------------------------
        //            switch clkPlaceWrapperType{
        //            case .clkPlace_Default:
        //                //-------------------------------------------------
        //                //no internal object - just store in private ivar
        //                _clLocation = newValue
        //                //-------------------------------------------------
        //            case .geoCodeLocation:
        //                //-------------------------------------------------
        //                //no internal object - just store in private ivar
        //                _clLocation = newValue
        //                //-------------------------------------------------
        //
        //            case .nearbySearch_CLKGooglePlaceResult:
        //                //--------------------------------------------------
        //                if let _ = clkGooglePlaceResult{
        //                    appDelegate.log.error("clkGooglePlaceResult.clLocation is readonly - newValue stores in ivar but may be overwritte when we call get again")
        //                    //clkGooglePlaceResult.clLocation = newValue
        //                }else{
        //                    appDelegate.log.error("clkGooglePlaceResult is nil")
        //                }
        //                //--------------------------------------------------
        //            case .nearbySearch_CLKGooglePlaceResult_GeoCodeLocation:
        //                //--------------------------------------------------
        //                if let _ = clkGooglePlaceResult{
        //                    appDelegate.log.error("clkGooglePlaceResult.clLocation is readonly - newValue stores in ivar but may be overwritte when we call get again")
        //                    //clkGooglePlaceResult.clLocation = newValue
        //                }else{
        //                    appDelegate.log.error("clkGooglePlaceResult is nil")
        //                }
        //                //--------------------------------------------------
        //
        //            case .placesPicker_GMSPlace:
        //                //--------------------------------------------------
        //                if let _ = gmsPlace{
        //                    appDelegate.log.error("gmsPlace.placeID is readonly - newValue stores in ivar but may be overwritte when we call get again")
        //                    //gmsPlace.placeID = newValue
        //
        //                }else{
        //                    appDelegate.log.error("clkGooglePlaceResult is nil")
        //                }
        //                //--------------------------------------------------
        //            case .savedPlace_DBPlace:
        //                //--------------------------------------------------
        //                if let _ = self.dbPlace{
        //                    appDelegate.log.error("TODO set place.clLocation")
        ////                    if newValue ?.isMemberOfClass(aClass: AnyClass)
        ////                    _clLocation = = CLLocation(latitude: place.latitude, longitude: place.longitude)
        //
        //                }else{
        //                    appDelegate.log.error("self.place is nil")
        //                }
        //                //--------------------------------------------------
        //            case .savedPlace_CLKGooglePlaceResult:
        //                //--------------------------------------------------
        //                if let _ = clkGooglePlaceResult{
        //                    appDelegate.log.error("clkGooglePlaceResult.clLocation is readonly - newValue stores in ivar but may be overwritte when we call get again")
        //                    //clkGooglePlaceResult.clLocation = newValue
        //                }else{
        //                    appDelegate.log.error("clkGooglePlaceResult is nil")
        //                }
        //            case .geocodedContact:
        //                //                if let colcContact = self.colcContact{
        //                //
        //                //                    //                    //coordinate: CLLocationCoordinate2D
        //                //                    //                    let clLocationFromCoord = CLLocation(latitude: dbPlace.latitude,
        //                //                    //                        longitude: dbPlace.longitude)
        //                //                    //
        //                //                    //                    clLocationReturned = clLocationFromCoord
        //                //
        //                //                }else{
        //                //                    appDelegate.log.error("self.dbPlace is nil")
        //                //                }
        //                appDelegate.log.error("TODO  self.colcContact.location - get CLLocation from inner Contact")
        //            case .geocodedCalendarEvent:
        //                //                if let colcContact = self.colcContact{
        //                //
        //                //                    //                    //coordinate: CLLocationCoordinate2D
        //                //                    //                    let clLocationFromCoord = CLLocation(latitude: dbPlace.latitude,
        //                //                    //                        longitude: dbPlace.longitude)
        //                //                    //
        //                //                    //                    clLocationReturned = clLocationFromCoord
        //                //
        //                //                }else{
        //                //                    appDelegate.log.error("self.dbPlace is nil")
        //                //                }
        //                appDelegate.log.error("TODO  self .GeocodedCalendarEvent .location - get CLLocation from inner Cal")
        ////            case .colcFacebookEvent:
        ////                //                if let colcContact = self.colcContact{
        ////                //
        ////                //                    //                    //coordinate: CLLocationCoordinate2D
        ////                //                    //                    let clLocationFromCoord = CLLocation(latitude: dbPlace.latitude,
        ////                //                    //                        longitude: dbPlace.longitude)
        ////                //                    //
        ////                //                    //                    clLocationReturned = clLocationFromCoord
        ////                //
        ////                //                }else{
        ////                //                    appDelegate.log.error("self.dbPlace is nil")
        ////                //                }
        ////                appDelegate.log.error("TODO  self.colcContact.location - get CLLocation from inner COLCFacebookEvent")
        ////            case .colcFacebookEvent_GeoCodeLocation:
        ////                if let googleGeocodedGMSAddress = self.googleGeocodedGMSAddress   {
        ////                    _clLocation = CLLocation.init(latitude: googleGeocodedGMSAddress.coordinate.latitude, longitude: googleGeocodedGMSAddress.coordinate.latitude)
        ////
        ////                }else{
        ////                    appDelegate.log.error("[COLCFacebookEvent_GeoCodeLocation] self.googleGeocodedGMSAddress is nil")
        ////                }
        ////            }
        //                
        //            default:
        //                appDelegate.log.error("CLKPLACE SUBCLASS SET _clLocation - remove should")
        //            //------------------------------------------------------------------------------------------------
        //        }
    }

    
    
    
    
    
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - GOOGLE WS: GET PLACE DETAIL - gets better address for CLKGooglePlaceResult
    // MARK: -
    //--------------------------------------------------------------
    
    //takes CLKGooglePlaceResult returns same result but address is better
    func get_place_detail(_ clkGooglePlaceResult: CLKGooglePlaceResult,
                          success: @escaping (_ clkGooglePlaceResultWithDetail: CLKGooglePlaceResult) -> Void,
                          failure: @escaping (_ error: Error?) -> Void
        )
    {
        
        if let place_id = clkGooglePlaceResult.place_id{
            
            self.get_place_detail(place_id,
                success:{ (clkPlaceSearchResponse: CLKGooglePlaceSearchResponse?)->Void in
                    //------------------------------------------------------------------------------------------------
                    //SUCCESS - better address with detail found
                    //------------------------------------------------------------------------------------------------

                    if let clkPlaceSearchResponse = clkPlaceSearchResponse{
                        
                        appDelegate.log.debug("clkPlaceSearchResponse returned")
                        
                        if let clkGooglePlaceResultWithDetailReturned = clkPlaceSearchResponse.result{
                            //store it with this CLKPlace so formatted_address gives better result
                            //NOTE: for bus stops with only addresss 'London' the formatted_address may still be "London"
                            self.clkGooglePlaceResult = clkGooglePlaceResultWithDetailReturned
                            
                            success(clkGooglePlaceResultWithDetailReturned)
                            
                        }
                        else if let results = clkPlaceSearchResponse.results{
                            appDelegate.log.error("more than one result found for get_place_detail:\(results)")
                        }
                        else
                        {
                            appDelegate.log.error("clkPlaceSearchResponse.result/results is nil")
                        }
                    }else{
                        appDelegate.log.error("clkPlaceSearchResponse is nil or not CLKGooglePlaceSearchResponse")
                    }
                    //---------------------------------------------------------------------
                },
                failure:{ (error) -> Void in
                    //appDelegate.log.error("error:\(error)")
                    failure(error)
                }
            )
        }else{
            appDelegate.log.error("clkGooglePlaceResult.place_id is nil")
        }
    }
    
    
    //--------------------------------------------------------------
    // MARK: - GOOGLE PLACES
    // MARK: -
    //--------------------------------------------------------------
    //moved in from APPD
    var googlePlacesController = GooglePlacesController()

    
    // TODO: - ADDRESS also RealmDb needs this too
    //get better address
    func get_place_detail(_ placeIdString: String,
                          success: @escaping (_ clkPlaceSearchResponse: CLKGooglePlaceSearchResponse?) -> Void,
                          failure: @escaping (_ error: Error?) -> Void){
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        //---------------------------------------------------------------------
        self.googlePlacesController.googlePlacesWSController.get_place_detail(placeIdString,
                                                                                     success:{
                                                                                        (clkPlaceSearchResponse: CLKGooglePlaceSearchResponse?)->Void in
                                                                                        //---------------------------------------------------------------------
                                                                                        if let clkPlaceSearchResponse = clkPlaceSearchResponse{
                                                                                            appDelegate.log.debug("clkPlaceSearchResponse returned")
                                                                                            
                                                                                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                                                                            success(clkPlaceSearchResponse)
                                                                                            
                                                                                        }else{
                                                                                            appDelegate.log.error("clkPlaceSearchResponse is nil or not CLKGooglePlaceSearchResponse")
                                                                                            
                                                                                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                                                                            
                                                                                            // TODO: - CREATE ERROR
                                                                                            
                                                                                            failure(nil)
                                                                                        }
                                                                                        //---------------------------------------------------------------------
            },
                                                                                     failure:{ (error) -> Void in
                                                                                        //appDelegate.log.error("error:\(error)")
                                                                                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                                                                        failure(error)
            }
        )
        //---------------------------------------------------------------------
    }
}
