//
//  CLKAddressContact.swift
//  joyride
//
//  Created by Brian Clear on 26/10/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
// TODO: - OK BUT COMMENTED out 'import Contacts' - as app store demanded I add plist permisssion message for Contacts framework
//import Contacts
//
////--------------------------------------------------------------
//// MARK: - CNPostalAddress >> CLKAddressContact
//// MARK: -
////--------------------------------------------------------------
////CNPostalAddress
////@available(iOS 9.0, *)
////public let CNPostalAddressStreetKey: String
////@available(iOS 9.0, *)
////public let CNPostalAddressCityKey: String
////@available(iOS 9.0, *)
////public let CNPostalAddressStateKey: String
////@available(iOS 9.0, *)
////public let CNPostalAddressPostalCodeKey: String
////@available(iOS 9.0, *)
////public let CNPostalAddressCountryKey: String
////@available(iOS 9.0, *)
////public let CNPostalAddressISOCountryCodeKey: String
//class CLKAddressContact: CLKAddress{
//    
//    var cnPostalAddress: CNPostalAddress?
//    
//    init(cnPostalAddress: CNPostalAddress) {
//        self.cnPostalAddress = cnPostalAddress
//    }
//    
//    override var street: String {
//        if let cnPostalAddress = self.cnPostalAddress {
//            return cnPostalAddress.street
//            
//        }else{
//            appDelegate.log.error("self.cnPostalAddress is nil - cant get street")
//            return ""
//        }
//    }
//    
//    override var city: String  {
//        if let cnPostalAddress = self.cnPostalAddress {
//            return cnPostalAddress.city
//            
//        }else{
//            appDelegate.log.error("self.cnPostalAddress is nil - cant get city")
//            return ""
//        }
//    }
//    
//    override var state: String  {
//        if let cnPostalAddress = self.cnPostalAddress {
//            return cnPostalAddress.state
//            
//        }else{
//            appDelegate.log.error("self.cnPostalAddress is nil - cant get state")
//            return ""
//        }
//    }
//    
//    override var postalCode: String  {
//        if let cnPostalAddress = self.cnPostalAddress {
//            return cnPostalAddress.postalCode
//            
//        }else{
//            appDelegate.log.error("self.cnPostalAddress is nil - cant get postalCode")
//            return ""
//        }
//    }
//    
//    override var country: String  {
//        if let cnPostalAddress = self.cnPostalAddress {
//            return cnPostalAddress.country
//            
//        }else{
//            appDelegate.log.error("self.cnPostalAddress is nil - cant get country")
//            return ""
//        }
//    }
//    
//    override var isoCountryCode: String  {
//        if let cnPostalAddress = self.cnPostalAddress {
//            return cnPostalAddress.isoCountryCode
//            
//        }else{
//            appDelegate.log.error("self.cnPostalAddress is nil - cant get isoCountryCode")
//            return ""
//        }
//    }
//    
//}
