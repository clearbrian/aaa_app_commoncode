//
//  UberPlace.swift
//  joyride
//
//  Created by Brian Clear on 15/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation

/*
uber://?client_id=YOUR_CLIENT_ID
&action=setPickup

&pickup[latitude]=37.775818
&pickup[longitude]=-122.418028
&pickup[nickname]=UberHQ
&pickup[formatted_address]=1455 Market St, San Francisco, CA 94103

&dropoff[latitude]=37.802374
&dropoff[longitude]=-122.405818
&dropoff[nickname]=Coit Tower
&dropoff[formatted_address]=1 Telegraph Hill Blvd, San Francisco, CA 94133
&product_id=a1111c8c-c720-46c3-8534-2fcdd730040d


*/

enum UberPlaceType : String {
    case Pickup = "pickup"  //used in deeplink url
    case Dropoff = "dropoff" //used in deeplink url
}

class UberPlace : OpenInAppPlace{
    
    var latitude : String?
    var longitude : String?
    var nickname : String?
    var formatted_address : String?
    var uberPlaceType : UberPlaceType?
    
    override init(){
        
    }
    init(uberPlaceType : UberPlaceType){
        self.uberPlaceType = uberPlaceType
    }
    
    //--------------------------------------------------------------
    // MARK: - STATIC
    // MARK: -
    //--------------------------------------------------------------

    static func convert_CLKPlace_To_UberPlace(_ clkPlace: CLKPlace, uberPlaceType: UberPlaceType) -> UberPlace?{
        
        var uberPlaceTypeReturned: UberPlace?
        if let clLocation = clkPlace.clLocation{
            
            let lat = clLocation.coordinate.latitude
            let lng = clLocation.coordinate.longitude
            
            //------------------------------------------------------------------------------------------------
            //NAME
            //------------------------------------------------------------------------------------------------
            //if geocode then name not set
            //.GeoCodeLocation name is blank so put on in then overwrite if it is set
            
            //------------------------------------------
            //let nameToShow = OpenInAppPlace.nameForCLKPlace(clkPlace, nameToShowFallback: uberPlaceType.rawValue.uppercased())
            // TODO: - removed fallback: method use .name only
            //let nameToShow = clkPlace.name(fallbackNameString: uberPlaceType.rawValue.uppercased())
            
            var nameToShow = ""
            if let name = clkPlace.name {
                nameToShow = name
            }else  if let formatted_address_first_line = clkPlace.formatted_address_first_line {
                nameToShow = formatted_address_first_line
            }
            else{
                appDelegate.log.error("clkPlace.name is nil: using '\(uberPlaceType.rawValue.uppercased())'")
                nameToShow = uberPlaceType.rawValue.uppercased()
            }
            
            //---------------------------------------------------------------------
            
            
            //------------------------------------------------------------------------------------------------
            //LOCALITY
            //------------------------------------------------------------------------------------------------
            // TODO: - test this in unknown locality
            if let locality = OpenInAppPlace.localityForCLKPlace(clkPlace){
                if let formatted_address = clkPlace.formatted_address{
                    
                    uberPlaceTypeReturned = createUberPlace(
                        lat: lat,
                        lng: lng,
                        locality: locality, //e.g. London
                        formatted_address: formatted_address,
                        name : nameToShow,
                        uberPlaceType: uberPlaceType
                    )
                    
                }else{
                    //cant use log ivar in init
                    print("clkPlace.formatted_address is nil")
                }
            }else{
                //cant use log ivar in init
                print("locality is nil cant create Uber Place - OpenInAppPlace.localityForCLKPlace failed ")
            }
            
            //-----------------------------------------------------------------------------------
            
        }else{
            //cant use log ivar in init
            print("clkPlace.clLocation is nil")
        }
        return uberPlaceTypeReturned
        
    }
    //needed as place may be created from picking a place or pickign a Lat/Lng which is geocoded to address
    static func createUberPlace(lat:Double, lng:Double, locality: String, formatted_address: String, name : String, uberPlaceType: UberPlaceType) -> UberPlace?
    {
        
        //------------------------------------------------------------------------------------------------
        //&pickup[latitude]=37.775818
        //&pickup[longitude]=-122.418028
        //&pickup[nickname]=UberHQ
        //&pickup[formatted_address]=1455 Market St, San Francisco, CA 94103
        //------------------------------------------------------------------------------------------------
        
        let uberPlace :UberPlace = UberPlace()
        
        uberPlace.latitude = "\(lat)"
        uberPlace.longitude = "\(lng)"
        uberPlace.nickname = name
        uberPlace.formatted_address = formatted_address
        uberPlace.uberPlaceType = uberPlaceType
        
        return uberPlace
    }
    
    
    
    //type : "dropoff"/"pickup"
    func paramsForUberPlace() -> String{
        
        var urlString = ""
        
        if let uberPlaceType = self.uberPlaceType {
            
            let uberPlaceType_name = uberPlaceType.rawValue
            
            //=======================================================================================================================
            if let latitude = self.latitude{
                urlString = urlString + "&\(uberPlaceType_name)[latitude]=\(latitude)"
            }else{
                self.log.error("uberPlace.latitude is nil")
            }
            //---------------------------------------------------------------------
            if let longitude = self.longitude{
                urlString = urlString + "&\(uberPlaceType_name)[longitude]=\(longitude)"
            }else{
                self.log.error("uberPlace.longitude is nil")
            }
            //---------------------------------------------------------------------
            if let nickname = self.nickname{
                
                //if let nickname_Encoded = nickname.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding){
                if let nickname_Encoded = nickname.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed){
                    urlString = urlString + "&\(uberPlaceType_name)[nickname]=\(nickname_Encoded)"
                    
                }else{
                    self.log.error("formatted_address.stringByAddingPercentEncodingWithAllowedCharacters is nil")
                }
                
            }else{
                self.log.error("uberPlace.nickname is nil")
            }
            //---------------------------------------------------------------------
            if let formatted_address = self.formatted_address{
                //if let formatted_addressEncoded = formatted_address.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding){
                if let formatted_addressEncoded = formatted_address.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed){
                    
                    urlString = urlString + "&\(uberPlaceType_name)[formatted_address]=\(formatted_addressEncoded))"
                    
                }else{
                    self.log.error("formatted_address.stringByAddingPercentEncodingWithAllowedCharacters is nil")
                }
            }else{
                self.log.error("uberPlace.formatted_address is nil")
            }
            //=======================================================================================================================

        }else{
            self.log.error("self.uberPlaceType is nil")
        }
        
        return urlString
    }
}
