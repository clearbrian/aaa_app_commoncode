//
//  CLKPlace.swift
//  joyride
//
//  Created by Brian Clear on 06/11/2015.
//  Copyright © 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import CoreLocation
//import GoogleMaps
//import GooglePlaces


//This is a common wrapper for Place passed around the app
//class CLKPlace : ParentSwiftObject{
class CLKPlace {
    
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - INITIALIZERS/CONVERTERS
    //--------------------------------------------------------------
    convenience init() {
        
        self.init(clLocation:nil,
                        name:nil,
         clkPlaceWrapperType: .clkPlace_Default)
        
    }
    
    convenience init(clLocation: CLLocation) {
        self.init(clLocation: clLocation)

    }
    
    //DESIGNATED INITIALIZER - in base class should have the most params
    //DESIGNATED INITIALIZER - in sub classclass should have the most params
    //https://developer.apple.com/library/prerelease/content/documentation/Swift/Conceptual/Swift_Programming_Language/Initialization.html#//apple_ref/doc/uid/TP40014097-CH18-ID222
    
    init(clLocation: CLLocation?,
               name: String?,
         clkPlaceWrapperType: CLKPlaceWrapperType)
    {
        //can be nil
        //if not nil get will use these values
        //if nil - will get from inner class Contact, FBEvent
        
        _clLocation = clLocation
        _name = name
        
        self.clkPlaceWrapperType = clkPlaceWrapperType
    }
    
    //--------------------------------------------------------------
    // MARK: - EXTERNAL
    // MARK: -
    //--------------------------------------------------------------
    //default to blank CLKPlace
    var clkPlaceWrapperType : CLKPlaceWrapperType = .clkPlace_Default
    var placeType: PlaceType?
    
    //--------------------------------------------------------------
    // MARK: - CLKAddress >
    // MARK: -
    //--------------------------------------------------------------
    //returned from geocoder
    var clkGeocodedAddressesCollection: CLKGeocodedAddressesCollection?
    
    var clkGeocodedAddress: CLKAddress?{
        var clkGeocodedAddress_: CLKAddress?
        if let clkGeocodedAddressesCollection = self.clkGeocodedAddressesCollection {
            clkGeocodedAddress_ = clkGeocodedAddressesCollection.bestOrFirstCLKAddress
        }else{
            //noisy appDelegate.log.error("self.clkGeocodedAddressesGoogleCollection is nil - clkGeocodedAddress is nil")
        }
        return clkGeocodedAddress_
    }
    
    //--------------------------------------------------------------
    //to show switchUseStreet
    var isGeocoded:Bool{
        if let _ = self.clkGeocodedAddress {
            return true
        }else{
            return false
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - EXTERNAL - OpenWithViewController/OpenAppConfigClipboard/TripPlannerViewController/EditCLKPlaceAddressViewController
    // MARK: -
    //--------------------------------------------------------------
    
    //NAME ADDRESS - no optional
    fileprivate var _name_formatted_address: String?
    
    var name_formatted_address :String {
        get {
            //---------------------------------------------------------
            var name_formatted_addressReturned :String = ""
            name_formatted_addressReturned = "CLKPLACE 76767"

            //set by Edit screen
            if let _name_formatted_address = self._name_formatted_address {
                name_formatted_addressReturned = _name_formatted_address
            }else{

                    if let name = self.name{
                        if let formatted_address = self.formatted_address{
  
                            if self.clkPlaceWrapperType == .geocodedCalendarEvent{
                                name_formatted_addressReturned = "\(formatted_address)"
                            }
                            else  if self.clkPlaceWrapperType == .colcFacebookEvent{
                                
                                if let formatted_address = self.formatted_address{
                                    name_formatted_addressReturned = "\(name), \(formatted_address)"
                                }else{
                                    //both nil
                                    name_formatted_addressReturned = "\(name)"
                                }
                            }else{
                                //when you edit an address name is set to ""
                                if name == ""{
                                    name_formatted_addressReturned = "\(formatted_address)"
                                }else{
                                    name_formatted_addressReturned = "\(name), \(formatted_address)"
                                }
                                
                            }
                            
                        }else{
                            name_formatted_addressReturned = "\(name)"
                        }
                    }else{
                        if let formatted_address = self.formatted_address{
                            name_formatted_addressReturned = "\(formatted_address)"
                        }else{
                            //both nil
                            name_formatted_addressReturned = ""
                        }
                    }
//                    //---------------------------------------------------------------------
//                }
//                //-----------------------------------------------------------------------------------
            }
            //------------------------------------------------------------------------------------------------
            return name_formatted_addressReturned
            //------------------------------------------------------------------------------------------------
        }
        set {
            //store in private var
            //_name_formatted_address = name_formatted_address
            _name_formatted_address = newValue
        }
    }
    
    // TODO: - needed? from RealmDb??
    //contact needs to check it but then if not changed by edit should use 
    private var _name: String?
    var name :String? {
        get {
            //---------------------------------------------------------
            var nameReturned :String?
            
            //changed by SET - eg Edit Name
            if let _name = self._name {
                nameReturned = _name
            }else{
                //---------------------------------------------------------------------
                //Contact - dont in subclass
                //
                
                if let clkGeocodedAddress = self.clkGeocodedAddress {
                    
                    if let formatted_address_first_line = self.formatted_address_first_line {
                        nameReturned = formatted_address_first_line
                        
                    }
                    else if let addressString = clkGeocodedAddress.addressString {
                        nameReturned = addressString
                        
                    }
                    else{
                        appDelegate.log.error("clkGoogleGeocoderResult.addressString is nil")
                    }
                }else{
                    appDelegate.log.error("self.clkGeocodedAddress is nil - name is nil")
                }
            }
            
            //------------------------------------------------------------------------------------------------
            return nameReturned
            //------------------------------------------------------------------------------------------------
        }
        //   set {
        //       /*
        //       //------------------------------------------------------------------------------------------------
        //       //PASS INTO INTERNAL TYPE
        //       //------------------------------------------------------------------------------------------------
        //       //---------------------------------------------------------------------
        //       //CHANGED BY EDIT
        //       _name = newValue
        //       //------------------------------------------------------------------------------------------------
        //   }
    }
    
    
    //--------------------------------------------------------------
    // MARK: - locality
    // MARK: -
    //--------------------------------------------------------------
    //used by Uber - if location turned off should get from CLKPlace GMSAddress
    fileprivate var _locality:String?
    
    var locality:String? {
        get {
            var localityReturned: String?
            
            if let private_locality = self._locality {
                localityReturned = private_locality
                
            }else{
                //-----------------------------------------------------
                //GET FROM Geocoded
                //-----------------------------------------------------
                if let clkGeocodedAddress = self.clkGeocodedAddress {
                    if let locality = clkGeocodedAddress.locality {
                        localityReturned = locality
                    }else{
                        appDelegate.log.error("CLKPLace googleGeocodedGMSAddress.locality is nil")
                    }
                }else{
                    appDelegate.log.error("self.clkGoogleGeocoderResult is nil - cant get LOCALITY - Reverse geocode not run")
                }
            }
        
            return localityReturned
        }
        
        set {
            //store it in the private var
            _locality = newValue
            
            appDelegate.log.debug("locality.set - not saved")
        }
    }

    
    //--------------------------------------------------------------
    // MARK: - FORMATTED ADDRESS - FIRST LINE
    // MARK: -
    //--------------------------------------------------------------
    // TODO: - ADDRESS - address varies should have generic CLKAddress class mapkit address/google address
    var formatted_address_first_line:String?{
        var formatted_address_first_line_:String?

        if let clkGeocodedAddress = self.clkGeocodedAddress {
            formatted_address_first_line_ = clkGeocodedAddress.address_lines_first
            
        }else{
            appDelegate.log.error("self.clkGeocodedAddress is nil")
        }
        
        return formatted_address_first_line_
    }
    
    //in Contact this can be blank/personname or orgname
    var formatted_name :String? {
        get {
            //---------------------------------------------------------
            var formatted_nameReturned :String?
            
            formatted_nameReturned = self.name
            //------------------------------------------------------------------------------------------------
            return formatted_nameReturned
            //------------------------------------------------------------------------------------------------
        }
    }


    
    //--------------------------------------------------------------
    // MARK: - FORMATTED ADDRESS
    // MARK: -
    //--------------------------------------------------------------
    // TODO: - use? first_line
    var formatted_address_street : String = ""
    
    //--------------------------------------------------------------

    private var _formatted_address: String?
    var formatted_address :String? {
        get {
            //---------------------------------------------------------
            var formatted_addressReturned :String?
        
            if let clkGeocodedAddress = self.clkGeocodedAddress {
                if let addressString = clkGeocodedAddress.addressString {
                    formatted_addressReturned = addressString
                }else{
                    appDelegate.log.error("clkGoogleGeocoderResult.addressString is nil")
                }
            }else{
                appDelegate.log.error("self.clkGoogleGeocoderResult is nil: place for reversegeocoded yet")
            }
        
            //------------------------------------------------------------------------------------------------
            return formatted_addressReturned
            //------------------------------------------------------------------------------------------------
        }
        set {
            // TODO: - RealDB load from DB - sets address
            
            //------------------------------------------------------------------------------------------------
            //PASS INTO INTERNAL TYPE
            //------------------------------------------------------------------------------------------------
            switch clkPlaceWrapperType{
            case .clkPlace_Default:
                //-------------------------------------------------
                //no internal object - just store in private ivar
                _formatted_address = newValue
                //-------------------------------------------------

            case .geoCodeLocation:
                _formatted_address = newValue
                //--------------------------------------------------

            case .geocodedContact:
                _formatted_address = newValue
                
            case .geocodedCalendarEvent:
                _formatted_address = newValue
                //--------------------------------------------------

            default:
                appDelegate.log.error("CLKPLACE SUBCLASS _formatted_address")
            }
            //------------------------------------------------------------------------------------------------
        }
    }
    
    
    var formatted_address_geocoded :String?{
        var formatted_address_geocoded_ :String = " "
        
        if self.isGeocoded {
            if let clkGeocodedAddress = self.clkGeocodedAddress   {
                
                if let addressString = clkGeocodedAddress.addressString {
                    formatted_address_geocoded_ = addressString
                }else{
                    appDelegate.log.error("clkGoogleGeocoderResult.addressString is nil")
                }
            }else{
                appDelegate.log.error("sclkPlaceSelected.formatted_address is nil")
            }
            
        }else{
            formatted_address_geocoded_ = "NOT GEOCODED YET"
            appDelegate.log.error("useGeocodedAddress is YES but NOT GEOCODED YET isGeocoded is false")
        }

        return formatted_address_geocoded_
    }
    
    //------------------------------------------------------------------------------------------------
    //clLocation CLLocation
    //------------------------------------------------------------------------------------------------
    fileprivate var _clLocation: CLLocation?
    
    //MUST ALWAYS BE Optional as app may call it before location retrieved
    // TODO: - remove set - change to init
    var clLocation :CLLocation? {
        get {
            //---------------------------------------------------------
            var clLocationReturned :CLLocation?
            
            //------------------------------------------------------------------------------------------------
            //GET FROM INTERNAL TYPE
            //------------------------------------------------------------------------------------------------
            switch clkPlaceWrapperType{
            case .clkPlace_Default:
                //-------------------------------------------------
                clLocationReturned = _clLocation
                //-------------------------------------------------
                
            case .geoCodeLocation:
                //-------------------------------------------------
                clLocationReturned = _clLocation
                //-------------------------------------------------
 // TODO: - CLEANUP after test moved to CLKGooglePlace
//            case .nearbySearch_CLKGooglePlaceResult:
//                //--------------------------------------------------
//                if let clkGooglePlaceResult = clkGooglePlaceResult{
//                    clLocationReturned = clkGooglePlaceResult.clLocation
//                }else{
//                    appDelegate.log.error("clkGooglePlaceResult is nil")
//                }
//                //--------------------------------------------------
//            case .nearbySearch_CLKGooglePlaceResult_GeoCodeLocation:
//                //--------------------------------------------------
//                if let clkGooglePlaceResult = clkGooglePlaceResult{
//                    clLocationReturned = clkGooglePlaceResult.clLocation
//                }else{
//                    appDelegate.log.error("clkGooglePlaceResult is nil")
//                }
                //--------------------------------------------------
                
//            case .placesPicker_GMSPlace:
//                //--------------------------------------------------
//                if let gmsPlace = gmsPlace{
//                    
//                    //coordinate: CLLocationCoordinate2D
//                    let clLocationFromCoord = CLLocation(latitude: gmsPlace.coordinate.latitude,
//                        longitude: gmsPlace.coordinate.longitude)
//                    
//                    
//                    clLocationReturned = clLocationFromCoord
//                }else{
//                    appDelegate.log.error("clkGooglePlaceResult is nil")
//                }

//            case .savedPlace_DBPlace:
//                //--------------------------------------------------
//                if let dbPlace = self.dbPlace{
//                    
//                    //coordinate: CLLocationCoordinate2D
//                    let clLocationFromCoord = CLLocation(latitude: dbPlace.latitude,
//                                                        longitude: dbPlace.longitude)
//                    
//                    clLocationReturned = clLocationFromCoord
//                    
//                }else{
//                    appDelegate.log.error("self.dbPlace is nil")
//                }
                //--------------------------------------------------
                
// TODO: - used?
//            case .savedPlace_CLKGooglePlaceResult:
//                //--------------------------------------------------
//                if let clkGooglePlaceResult = clkGooglePlaceResult{
//                    clLocationReturned = clkGooglePlaceResult.clLocation
//                }else{
//                    appDelegate.log.error("clkGooglePlaceResult is nil")
//                }
            //--------------------------------------------------
                
                
// TODO: - remove - moved down
//            case .geocodedContact:
//                if let colcContact = self.colcContact{
//                    if let clLocation = colcContact.clLocation{
//                        clLocationReturned = clLocation
//                    }else{
//                        appDelegate.log.error("colcContact.clLocation is nil")
//                    }
//                }else{
//                    appDelegate.log.error("self.colcContact is nil")
//                }
//                
//            case .geocodedCalendarEvent:
//                if let colcGeocodedCalendarEvent = self.colcGeocodedCalendarEvent{
//                    if let clLocation = colcGeocodedCalendarEvent.clLocation{
//                        clLocationReturned = clLocation
//                    }else{
//                        appDelegate.log.error("colcGeocodedCalendarEvent.clLocation is nil")
//                    }
//                }else{
//                    appDelegate.log.error("self.colcContact is nil")
//                }
                
//            case .colcFacebookEvent:
//                if let colcFacebookEvent = self.colcFacebookEvent{
//                    if let clLocation = colcFacebookEvent.clLocation {
//                        clLocationReturned = clLocation
//                    }else{
//                        appDelegate.log.error("colcFacebookEvent.clLocation is nil")
//                    }
//                }else{
//                    appDelegate.log.error("self.colcFacebookEvent is nil")
//                }
//            case .colcFacebookEvent_GeoCodeLocation:
//                if let colcFacebookEvent = self.colcFacebookEvent{
//                    //ok should be same cllocation as geoocoded address
//                    if let clLocation = colcFacebookEvent.clLocation {
//                        clLocationReturned = clLocation
//                    }else{
//                        appDelegate.log.error("colcFacebookEvent.clLocation is nil")
//                        
//                        if let googleGeocodedGMSAddress = self.googleGeocodedGMSAddress {
//                            clLocationReturned = CLLocation(latitude: googleGeocodedGMSAddress.coordinate.latitude, longitude: googleGeocodedGMSAddress.coordinate.longitude)
//                        }else{
//                            appDelegate.log.error("colcFacebookEvent.clLocation and self.googleGeocodedGMSAddress is nil - cant return CLKPlace.clLocation")
//                        }
//                    }
//                    
//                }else{
//                    appDelegate.log.error("self.colcFacebookEvent is nil")
//                }
            default:
                appDelegate.log.error("CLKPLACE SUBCLASS clkGoogleDirectionsLocation")
            }
            //------------------------------------------------------------------------------------------------
            return clLocationReturned
            //------------------------------------------------------------------------------------------------
        }
        // TODO: - readonly
//        set {
//            
//            //------------------------------------------------------------------------------------------------
//            //PASS INTO INTERNAL TYPE
//            //------------------------------------------------------------------------------------------------
//            switch clkPlaceWrapperType{
//            case .clkPlace_Default:
//                //-------------------------------------------------
//                //no internal object - just store in private ivar
//                _clLocation = newValue
//                //-------------------------------------------------
//            case .geoCodeLocation:
//                //-------------------------------------------------
//                //no internal object - just store in private ivar
//                _clLocation = newValue
//                //-------------------------------------------------
//                
//            case .nearbySearch_CLKGooglePlaceResult:
//                //--------------------------------------------------
//                if let _ = clkGooglePlaceResult{
//                    appDelegate.log.error("clkGooglePlaceResult.clLocation is readonly - newValue stores in ivar but may be overwritte when we call get again")
//                    //clkGooglePlaceResult.clLocation = newValue
//                }else{
//                    appDelegate.log.error("clkGooglePlaceResult is nil")
//                }
//                //--------------------------------------------------
//            case .nearbySearch_CLKGooglePlaceResult_GeoCodeLocation:
//                //--------------------------------------------------
//                if let _ = clkGooglePlaceResult{
//                    appDelegate.log.error("clkGooglePlaceResult.clLocation is readonly - newValue stores in ivar but may be overwritte when we call get again")
//                    //clkGooglePlaceResult.clLocation = newValue
//                }else{
//                    appDelegate.log.error("clkGooglePlaceResult is nil")
//                }
//                //--------------------------------------------------
//                
//            case .placesPicker_GMSPlace:
//                //--------------------------------------------------
//                if let _ = gmsPlace{
//                    appDelegate.log.error("gmsPlace.placeID is readonly - newValue stores in ivar but may be overwritte when we call get again")
//                    //gmsPlace.placeID = newValue
//                    
//                }else{
//                    appDelegate.log.error("clkGooglePlaceResult is nil")
//                }
//                //--------------------------------------------------
//            case .savedPlace_DBPlace:
//                //--------------------------------------------------
//                if let _ = self.dbPlace{
//                    appDelegate.log.error("TODO set place.clLocation")
////                    if newValue ?.isMemberOfClass(aClass: AnyClass)
////                    _clLocation = = CLLocation(latitude: place.latitude, longitude: place.longitude)
//                    
//                }else{
//                    appDelegate.log.error("self.place is nil")
//                }
//                //--------------------------------------------------
//            case .savedPlace_CLKGooglePlaceResult:
//                //--------------------------------------------------
//                if let _ = clkGooglePlaceResult{
//                    appDelegate.log.error("clkGooglePlaceResult.clLocation is readonly - newValue stores in ivar but may be overwritte when we call get again")
//                    //clkGooglePlaceResult.clLocation = newValue
//                }else{
//                    appDelegate.log.error("clkGooglePlaceResult is nil")
//                }
//            case .geocodedContact:
//                //                if let colcContact = self.colcContact{
//                //
//                //                    //                    //coordinate: CLLocationCoordinate2D
//                //                    //                    let clLocationFromCoord = CLLocation(latitude: dbPlace.latitude,
//                //                    //                        longitude: dbPlace.longitude)
//                //                    //
//                //                    //                    clLocationReturned = clLocationFromCoord
//                //
//                //                }else{
//                //                    appDelegate.log.error("self.dbPlace is nil")
//                //                }
//                appDelegate.log.error("TODO  self.colcContact.location - get CLLocation from inner Contact")
//            case .geocodedCalendarEvent:
//                //                if let colcContact = self.colcContact{
//                //
//                //                    //                    //coordinate: CLLocationCoordinate2D
//                //                    //                    let clLocationFromCoord = CLLocation(latitude: dbPlace.latitude,
//                //                    //                        longitude: dbPlace.longitude)
//                //                    //
//                //                    //                    clLocationReturned = clLocationFromCoord
//                //
//                //                }else{
//                //                    appDelegate.log.error("self.dbPlace is nil")
//                //                }
//                appDelegate.log.error("TODO  self .GeocodedCalendarEvent .location - get CLLocation from inner Cal")
////            case .colcFacebookEvent:
////                //                if let colcContact = self.colcContact{
////                //
////                //                    //                    //coordinate: CLLocationCoordinate2D
////                //                    //                    let clLocationFromCoord = CLLocation(latitude: dbPlace.latitude,
////                //                    //                        longitude: dbPlace.longitude)
////                //                    //
////                //                    //                    clLocationReturned = clLocationFromCoord
////                //
////                //                }else{
////                //                    appDelegate.log.error("self.dbPlace is nil")
////                //                }
////                appDelegate.log.error("TODO  self.colcContact.location - get CLLocation from inner COLCFacebookEvent")
////            case .colcFacebookEvent_GeoCodeLocation:
////                if let googleGeocodedGMSAddress = self.googleGeocodedGMSAddress   {
////                    _clLocation = CLLocation.init(latitude: googleGeocodedGMSAddress.coordinate.latitude, longitude: googleGeocodedGMSAddress.coordinate.latitude)
////
////                }else{
////                    appDelegate.log.error("[COLCFacebookEvent_GeoCodeLocation] self.googleGeocodedGMSAddress is nil")
////                }
////            }
//                
//            default:
//                appDelegate.log.error("CLKPLACE SUBCLASS SET _clLocation - remove should")
//            //------------------------------------------------------------------------------------------------
//        }
    }
    

    
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - EXTERNAL - CLKPlace > RealmPlace
    // MARK: -
    //--------------------------------------------------------------
    //generate RealmDB obejc Place.swift from CLKPlace
   
//    var realmPlace: RealmPlace?{
//        get {
//            let _realmPlace = RealmPlace()
//            //---------------------------------------------------------------------
//            //if we edit place name is set to "" but messed up Recent Places so try first line of address
//            
//            //var nameToSet = self.name(fallbackNameString:"")
//            var nameToSet = ""
//            if let name = self.name{
//                nameToSet = name
//            }else{
//                // TODO: - REMOVE - do in name method
//                if nameToSet == ""{
//                    if let formatted_address_first_line = self.formatted_address_first_line{
//                        nameToSet = formatted_address_first_line
//                    }else{
//                        appDelegate.log.error("self.formatted_address_first_line is nil")
//                    }
//                }else{
//                    
//                }
//            }
//            
//            //---------------------------------------------------------------------
//             _realmPlace.name = nameToSet
//            //-----------------------------------------------------------------------------------
//            if let formatted_address = self.formatted_address {
//                _realmPlace.address = formatted_address
//            }else{
//                appDelegate.log.error("self.formatted_address is nil")
//                _realmPlace.name = ""
//            }
//            //-----------------------------------------------------------------------------------
//            
//            if let locality = self.locality {
//                _realmPlace.locality = locality
//            }else{
//                appDelegate.log.error("self.locality is nil")
//                _realmPlace.locality = ""
//            }
//            //-----------------------------------------------------------------------------------
//
//// TODO: - SUBCLASS - parent return nil
////            if let place_id = self.place_id {
////                _realmPlace.place_id = place_id
////            }else{
////                appDelegate.log.error("self.place_id is nil")
////                _realmPlace.place_id = ""
////            }
//            //-----------------------------------------------------------------------------------
//            if let clLocation = self.clLocation {
//                _realmPlace.latitude = clLocation.coordinate.latitude
//                _realmPlace.longitude = clLocation.coordinate.longitude
//            }else{
//                appDelegate.log.error("self.latitude or longitude is nil")
//                _realmPlace.latitude = -1.0
//                _realmPlace.longitude = -1.0
//            }
//            
//            //-----------------------------------------------------------------------------------
//            _realmPlace.clkPlaceWrapperTypeId = clkPlaceWrapperType.rawValue
//            
//            return _realmPlace
//        }
//// TODO: - removed?
//        //Converted to an init(realmPlace)
//        //    set {
//        //        
//        //        _realmPlace = newValue
//        //        
//        //      
//        //        if let newValue = newValue {
//        //            //self.name = newValue.name
//        //            self.name = newValue.name
//        //            self.formatted_address = newValue.address
//        //            
//        //            self.locality = newValue.locality
//        //            self.place_id = newValue.place_id
//        //            
//        //            self.clLocation = CLLocation(latitude: newValue.latitude, longitude: newValue.longitude)
//        //
//        //            self.clkPlaceWrapperType = CLKPlaceWrapperType.clkPlaceWrapperTypeForRaw(newValue.clkPlaceWrapperTypeId)
//        //        }else{
//        //             self.clLocation = nil
//        //        }
//        //       
//        //    }
//    }

    //--------------------------------------------------------------
    // MARK: - Init - from RealmPlace in DB
    // MARK: -
    //--------------------------------------------------------------
//    init(realmPlace: RealmPlace){
//        
////REMOVE ParentSwiftObject        super.init()
//        print("realmPlace in init:\(realmPlace)")
//        
//        //self.realmPlace = realmPlace
//        //_realmPlace = realmPlace
//
//        _name = realmPlace.name
//        self.formatted_address = realmPlace.address
//
//        self.locality = realmPlace.locality
//        
//  appDelegate.log.error("[ERROR self.place_id should be in parent as well for RealmDB")
//// TODO: - COMMENT
////        self.place_id = realmPlace.place_id
//        // TODO: - COMMENT
//appDelegate.log.error("[ERROR GEOCODE IS OFF  453453")
////        self.clLocation = CLLocation(latitude: realmPlace.latitude, longitude: realmPlace.longitude)
//
//        self.clkPlaceWrapperType = CLKPlaceWrapperType.clkPlaceWrapperTypeForRaw(realmPlace.clkPlaceWrapperTypeId)
// 
//
//    }
    
    
    //--------------------------------------------------------------
    // MARK: - GEOCODE CLLocation
    // MARK: -
    //--------------------------------------------------------------
// TODO: - CLEANUP after test
    
    //    func updateLocationAndReverseGeocode(clLocation: CLLocation,
//                        success: @escaping (_ clkGeocodedAddressesGoogleCollection: CLKGeocodedAddressesGoogleCollection) -> Void,
//                        failure: @escaping (_ error: Error?) -> Void
//        )
//    {
//        if clLocation.isValid{
//            
//            //------------------------------------------------------------------------------------------------
//            //STORE NEW LOCATION in private var
//            //------------------------------------------------------------------------------------------------
//            _clLocation = clLocation
//            
//            //------------------------------------------------------------------------------------------------
//            //CALL REVERSE GEOCODE - alway if you user this methof
//            //------------------------------------------------------------------------------------------------
//            self.reverseGeocodePlace( success:{(clkGeocodedAddressesGoogleCollection: CLKGeocodedAddressesGoogleCollection) -> Void in
//                                            //---------------------------------------------------------------------
//                                            //store the collection on the result in this CLKPlace
//                                            //important all address come from this
//                                            self.clkGeocodedAddressesCollection = clkGeocodedAddressesGoogleCollection
//                                            //--------------------------------------------
//                                            success(clkGeocodedAddressesGoogleCollection)
//                                            //--------------------------------------------
//                                      },
//                                      failure:{ (error: Error?) -> Void in
//                                            failure(error)
//                                      }
//            )
//            //------------------------------------------------------------------------------------------------
//            
//        }else{
//            appDelegate.log.debug("clLocation.isValid is false - skip geocode of new location")
//        }
//    }
    
    
    func updateLocation(clLocation: CLLocation)
    {
        if clLocation.isValid{
            
            //------------------------------------------------------------------------------------------------
            //STORE NEW LOCATION in private var
            //------------------------------------------------------------------------------------------------
            _clLocation = clLocation
            
        }else{
            appDelegate.log.debug("clLocation.isValid is false - skip geocode of new location")
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - REVERSE GEOCODE
    // MARK: -
    //--------------------------------------------------------------
// TODO: - CLEANUP after test
    //    var isReverseGeocodeInProgress = false
//
//    uses
//    func reverseGeocodePlace1(success: @escaping (_ clkGeocodedAddressesGoogleCollection: CLKGeocodedAddressesGoogleCollection) -> Void,
//                             failure: @escaping (_ error: Error?) -> Void
//        )
//    {
//        
//
//        //prevent multiple calls
//        if isReverseGeocodeInProgress{
//            appDelegate.log.debug("isReverseGeocodeInProgress - skip geocode till current call returns")
//            
//        }else{
//            if let clLocation = self.clLocation {
//                //---------------------------------------------------------------------
//                if clLocation.isValid{
//                    //------------------------------------------------------------------------------------------------
//                    self.isReverseGeocodeInProgress = true
//                    appDelegate.clkGoogleGeocoder.reverseGeocode(clLocation,
//                                                                 success:{
//                                                                    (clkGeocodedAddressesGoogleCollection: CLKGeocodedAddressesGoogleCollection) -> Void in
//                                                                    
//                                                                    self.isReverseGeocodeInProgress = false
//                                                                    success(clkGeocodedAddressesGoogleCollection)
//                                                                    
//                        },
//                                                                 failure:{ (error: Error?) -> Void in
//                                                                    //---------------------------------------------------------------------
//                                                                    //appDelegate.log.error("[GEOCODE CURRENT LOCATION FAILED] currentLocation(\(currentLocation)) error:\(error)")
//                                                                    //---------------------------------------------------------------------
//                                                                    self.isReverseGeocodeInProgress = false
//                                                                    failure(error)
//                                                                    //---------------------------------------------------------------------
//                        }
//                    )
//                    //-----------------------------------------------------------------------------------------------
//                }else{
//                    appDelegate.log.debug("clLocation.isValid is false - skip geocode of new location")
//                    
//                }//clLocation.isValid
//                //---------------------------------------------------------------------
//            }else{
//                appDelegate.log.error("self.clLocation is nil")
//                failure(NSError.appError(.geocode_reverse_google_failed))
//            }
//            
//        }//isReverseGeocodeInProgress
//    }
}
