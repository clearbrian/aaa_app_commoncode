//
//  TFLTaxisViewController.swift
//  joyride
//
//  Created by Brian Clear on 13/04/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit



class TFLTaxisRankListViewController: GenericListViewController, TFLGeoJSONManagerDelegate{
    
    
    let tflApiWSController = TFLApiWSController()
    
    let emailManager = EmailManager()
    
    @IBOutlet weak var labelRankCount: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.genericListViewDataSource = TFLPlaceTaxiRankResponse()
        
        self.title = "Taxi Ranks"
        
        configureNavBar()
        
        //-----------------------------------------------------------------------------------
        //DYNAMIC TEXT and TABLE ROW HEIGHT
        //-----------------------------------------------------------------------------------
        self.config_tableView_dynamicHeight(estimatedRowHeight: 80.0)
        
        self.config_callWS()
    }
    
    
    //SUBLCASS
    override func configureNavBar() {
        super.configureNavBar()
        
        //----------------------------------------------------------------------------------------
        //Menu
        //----------------------------------------------------------------------------------------
        //        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Menu",
        //                                                           style: .plain,
        //                                                           target: self,
        //                                                           action: #selector(TFLTaxisRankListViewController.leftBarButtonItem_Action))
        //        //----------------------------------------------------------------------------------------
        //        //Map
        //        //----------------------------------------------------------------------------------------
        //        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Map",
        //                                                            style: .plain,
        //                                                            target: self,
        //                                                            action: #selector(TFLTaxisRankListViewController.rightBarButtonItem_Action))
        //----------------------------------------------------------------------------------------
        //NOTE IMAGES WILL BE DISPLAY B&W and in neegative so avid colored icons
        //icon_info_triangle
        
//        if let imageInfoTriangle = UIImage.init(named: "info2_30x30") {
//            navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: imageInfoTriangle,
//                                                                     style: .plain,
//                                                                     target: self,
//                                                                     action: #selector(TFLTaxisRankListViewController.leftBarButtonItem_Action))
//            
//            //navigationItem.leftBarButtonItem?.tintColor = UIColor.darkGray
//            navigationItem.leftBarButtonItem?.tintColor = AppearanceManager.appColorLight0
//            //navigationItem.leftBarButtonItem?.tintColor = UIColor.white
//        }else{
//            appDelegate.log.error("imageInfoTriangle is nil")
//        }
        //----------------------------------------------------------------------------------------
        //now in tab
        //    if let imageMap = UIImage.init(named: "icon_map_simple") {
        //        navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: imageMap,
        //                                                                 style: .plain,
        //                                                                 target: self,
        //                                                                 action: #selector(TFLTaxisRankListViewController.rightBarButtonItem_Action))
        //        
        //        navigationItem.rightBarButtonItem?.tintColor = AppearanceManager.appColorLight0
        //        
        //    }else{
        //        appDelegate.log.error("imageMap is nil")
        //    }
        //----------------------------------------------------------------------------------------
        
        if let imageInfoTriangle = UIImage.init(named: "info2_30x30") {
            navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: imageInfoTriangle,
                                                                    style: .plain,
                                                                    target: self,
                                                                    action: #selector(TFLTaxisRankListViewController.rightBarButtonItem_Action))
            
            //navigationItem.rightBarButtonItem?.tintColor = UIColor.darkGray
            navigationItem.rightBarButtonItem?.tintColor = AppearanceManager.appColorLight0
            //navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        }else{
            appDelegate.log.error("imageInfoTriangle is nil")
        }
    }
    
    
    
    
    override func leftBarButtonItem_Action() {
        //self.performSegue(withIdentifier: segueTFLTaxisRankListViewControllerTOSettingsTableViewController, sender: nil)
        
    }
    
    override func rightBarButtonItem_Action() {
        //self.performSegue(withIdentifier: segueTFLTaxisRankListViewControllerTOTFLTaxisRankMapViewController, sender: nil)
        self.performSegue(withIdentifier: segueTFLTaxisRankListViewControllerTOSettingsTableViewController, sender: nil)
      
    }

    
    
    override func config_callWS()
    {
        super.config_callWS()
        
        //----------------------------------------------------------------------------------------
        self.showLoadingOnMain()
        self.labelRankCount.text = ""
        //---------------------------------------------------------------------
        self.tflApiWSController.call_tflApiPlaceTypeRequest_TaxiRank(
            success:{
                                        (tflPlaceTaxiRankResponse : TFLPlaceTaxiRankResponse)->Void in
                                                
                                                //---------------------------------------------------------------------
                                                self.log.info("tflApiPlaceTaxiRankArray returned:\(tflPlaceTaxiRankResponse.tflApiPlaceTaxiRankArrayUnsorted.count)")
                                                
                                                self.labelRankCount.text = "\(tflPlaceTaxiRankResponse.tflApiPlaceTaxiRankArrayUnsorted.count)\rRanks"
                                                //-------------------------------------------------------------------
                                                self.genericListViewDataSource = tflPlaceTaxiRankResponse
                                                
                                                //store in AppD for map view
                                                appDelegate.tflPlaceTaxiRankResponse = tflPlaceTaxiRankResponse
                                                
                                                //self.tflPlaceTaxiRankResponse.dumpTaxiRankOneLine()
                                                //print("\r\r\r\r")
                                                //self.tflPlaceTaxiRankResponse.dumpTaxiRankOneLine_VersionsOnly()
                                                //-------------------------------------------------------------------
                                                self.hideLoadingOnMain()
                                                self.reloadTableOnMain()
                                                //-------------------------------------------------------------------
                                                
                                                //preload the json
                                                appDelegate.tflGeoJSONManager.delegate = self
                                                appDelegate.tflGeoJSONManager.loadTFLStationsAndLines(loadStationsFromJSON: false)
                                                
                                                
                                                
            },
            failure:{
                                                (error) -> Void in
                                                print(error)
                                                
                                                CLKAlertController.showAlertInVC(self, title: "Error getting Taxi  rank information", message: "Please check your internet connection.\rIf this problem persists please contact support@cityoflondonconsulting.com with error:\r [\(error.localizedDescription)]")
                                                
            }
        )
        
    }
    //--------------------------------------------------------------
    // MARK: - TFLGeoJSONManagerDelegate
    // MARK: -
    //--------------------------------------------------------------
    func tflGeoJSONManagerLinesAndStationsLoaded(tflGeoJSONManager: TFLGeoJSONManager){
        appDelegate.log.info("tflGeoJSONManagerLinesAndStationsLoaded - do nothing - map will but quicker now")
    }
    
    
    //--------------------------------------------------------------
    // MARK: - UITableViewDataSource
    // MARK: -
    //--------------------------------------------------------------
    
    //--------------------------------------------------------------
    // MARK: - TABLEVIEW: CELL FOR ROW
    //--------------------------------------------------------------
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cellIdentifier = "GenericListTableViewCell"
        
        let genericListTableViewCell : GenericListTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! GenericListTableViewCell
        
        //----------------------------------------------------------------------------------------
        //genericListTableViewCell.applyCustomFont()
        //----------------------------------------------------------------------------------------
        
        //Array<Any> >> Array<TFLApiPlaceTaxiRank>
        if var rowDataArray_ : Array<TFLApiPlaceTaxiRank> = self.genericListViewDataSource.rowDataArrayForSection(indexPath.section) as? Array<TFLApiPlaceTaxiRank>{
            
            let tflApiPlaceTaxiRank : TFLApiPlaceTaxiRank = rowDataArray_[indexPath.row]
            //----------------------------------------------------------------------------------------
            genericListTableViewCell.labelTitle?.text = tflApiPlaceTaxiRank.textLabelText()
            
            genericListTableViewCell.labelDetail0?.text = tflApiPlaceTaxiRank.detailLabelText0()
            genericListTableViewCell.labelDetail1?.text = tflApiPlaceTaxiRank.detailLabelText1()
            
            genericListTableViewCell.labelSubDetail0?.text = tflApiPlaceTaxiRank.subDetailLabelText0()
            genericListTableViewCell.labelSubDetail1?.text = tflApiPlaceTaxiRank.subDetailLabelText1()
            
            //----------------------------------------------------------------------------------------
        }else{
            self.log.error("rowDataArrayForSection as? Array<TFLApiPlaceTaxiRank> failed")
        }
        
        return genericListTableViewCell
    }
    
    //--------------------------------------------------------------
    // MARK: - TABLEVIEW: didSelectRowAt
    //--------------------------------------------------------------
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        //----------------------------------------------------------------------------------------
        //GENERIC row Any from dict
        //----------------------------------------------------------------------------------------
        if var rowDataArray_ : Array<TFLApiPlaceTaxiRank> = self.genericListViewDataSource.rowDataArrayForSection(indexPath.section) as? Array<TFLApiPlaceTaxiRank>{
            
            self.selectedRowItemAny = rowDataArray_[indexPath.row]
            //----------------------------------------------------------------------------------------
            
            if let selectedRowItemAny = self.selectedRowItemAny {
                
                self.log.info("didSelectRowAt:\r\(selectedRowItemAny)")
                
                //self.performSegue(withIdentifier: self.segueTFLTaxisRankListViewControllerTOTFLTaxisRankSingleTableViewController, sender: nil)
                
                showAlertOptions(selectedRowItemAny as! TFLApiPlaceTaxiRank)
                
            }else{
                appDelegate.log.error("sself.selectedTFLApiPlaceTaxiRank is nil")
            }
            //----------------------------------------------------------------------------------------
        }else{
            self.log.error("rowDataArrayForSection as? Array<TFLApiPlaceTaxiRank> failed")
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - didTapOnMarker
    // MARK: -
    //--------------------------------------------------------------
    //didTapOnMarker
    var selectedTFLApiPlaceTaxiRank : TFLApiPlaceTaxiRank?
      // MARK: -
    
    func showAlertOptions(_ tflApiPlaceTaxiRank : TFLApiPlaceTaxiRank){
        
        self.selectedTFLApiPlaceTaxiRank = tflApiPlaceTaxiRank
        
        //---------------------------------------------------------
        //CANCEL
        //---------------------------------------------------------
        let alertActionCancel = UIAlertAction.alertActionCancel
        //---------------------------------------------------------
        //Directions
        //---------------------------------------------------------

        let alertActionDirections = UIAlertAction(title: "Directions", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            
            self.performSegue(withIdentifier: "segueTFLTaxisRankListViewControllerTOTFLTaxisRankMapDirectionsViewController", sender: nil)
            
        }
        
        //---------------------------------------------------------
        //StreetView
        //---------------------------------------------------------

        let alertActionStreetView = UIAlertAction(title: "StreetView", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            self.performSegue(withIdentifier: "segueTFLTaxisRankListViewControllerTOTFLTaxisRankStreetViewController", sender: nil)
            
        }
        
        
        let alertReportProblem = UIAlertAction(title: "Report Problem", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            if let selectedTFLApiPlaceTaxiRank = self.selectedTFLApiPlaceTaxiRank {
                self.emailManager.sendEmailFromVC(self, bodyString: "User Reported Error with Taxi Rank:\n['\(selectedTFLApiPlaceTaxiRank.idSafe)':'\(selectedTFLApiPlaceTaxiRank.commonNameSafe)']\n Please describe your problem below and we will forward it to TFL.\n")
            }else{
                appDelegate.log.error("self.selectedTFLApiPlaceTaxiRank is nil")
            }
            
        }
        
        
        //---------------------------------------------------------
        //show
        //---------------------------------------------------------

        CLKAlertController.showSheetWithActions(vc: self, title: tflApiPlaceTaxiRank.alertTitle,
                                                message: tflApiPlaceTaxiRank.alertMessage,
                                                alertActions: [alertActionCancel, alertActionDirections, alertActionStreetView, alertReportProblem])
    }
    

    //--------------------------------------------------------------
    // MARK: -
    //--------------------------------------------------------------

    //------------------------------------------------
    // MARK: UITableViewDataSource - SECTIONS
    //------------------------------------------------
    //Handled by parent and genericListViewDataSource
    //    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?{
    //
    //    override public func numberOfSections(in tableView: UITableView) -> Int {
    //    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    //    //------------------------------------------------

    //    //------------------------------------------------
    //    override public func sectionIndexTitles(for tableView: UITableView) -> [String]? {
    //    override public func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {

    //--------------------------------------------------------------
    // MARK: - prepare
    // MARK: -
    //--------------------------------------------------------------
    //RightBarButtion: Rank List to Settings
    let segueTFLTaxisRankListViewControllerTOSettingsTableViewController = "segueTFLTaxisRankListViewControllerTOSettingsTableViewController"
    
    //Map
    let segueTFLTaxisRankListViewControllerTOTFLTaxisRankMapViewController = "segueTFLTaxisRankListViewControllerTOTFLTaxisRankMapViewController"
    
    //Single Rank
    //let segueTFLTaxisRankListViewControllerTOTFLTaxisRankSingleTableViewController = "segueTFLTaxisRankListViewControllerTOTFLTaxisRankSingleTableViewController"

    let segueTFLTaxisRankListViewControllerTOTFLTaxisRankStreetViewController = "segueTFLTaxisRankListViewControllerTOTFLTaxisRankStreetViewController"
    let segueTFLTaxisRankListViewControllerTOTFLTaxisRankMapDirectionsViewController = "segueTFLTaxisRankListViewControllerTOTFLTaxisRankMapDirectionsViewController"

    
    //--------------------------------------------------------------
    // MARK: - prepare
    // MARK: -
    //--------------------------------------------------------------
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == segueTFLTaxisRankListViewControllerTOTFLTaxisRankMapViewController){
            
            if(segue.destination.isMember(of: TFLTaxisRankMapViewController.self)){
                print("ERROR:segue to map broken use resp in delegate TFLTaxisRankMapViewController")
                //let tflTaxisRankMapViewController = (segue.destination as! TFLTaxisRankMapViewController)
                
                if let tflPlaceTaxiRankResponse = self.genericListViewDataSource as? TFLPlaceTaxiRankResponse {
                    //tflTaxisRankMapViewController.tflPlaceTaxiRankResponse = tflPlaceTaxiRankResponse
                    // ERROR: map view is in tab - may never be opened by segue now
                   
                    appDelegate.tflPlaceTaxiRankResponse = tflPlaceTaxiRankResponse
                }else{
                    appDelegate.log.error("self.genericListViewDataSource as? TFLPlaceTaxiRankResponse is nil")
                }
                
            }else{
                print("ERROR:not TFLTaxisRankMapViewController")
            }
        }
//        else if(segue.identifier == segueTFLTaxisRankListViewControllerTOTFLTaxisRankSingleTableViewController){
//            
//            if(segue.destination.isMember(of: TFLTaxisRankSingleTableViewController.self)){
//                
//                let tflTaxisRankSingleTableViewController = (segue.destination as! TFLTaxisRankSingleTableViewController)
//                
//                if let selectedTFLApiPlaceTaxiRank = self.selectedRowItemAny as? TFLApiPlaceTaxiRank {
//                    tflTaxisRankSingleTableViewController.selectedTFLApiPlaceTaxiRank = selectedTFLApiPlaceTaxiRank
//                }else{
//                    appDelegate.log.error("self.selectedRowItemAny as? TFLApiPlaceTaxiRank is nil")
//                }
//
//            }else{
//                print("ERROR:not TFLTaxisRankSingleTableViewController")
//            }
//        }
        else if(segue.identifier == segueTFLTaxisRankListViewControllerTOTFLTaxisRankStreetViewController){
            
            if(segue.destination.isMember(of: TFLTaxisRankStreetViewController.self)){
                
                let tflTaxisRankStreetViewController = (segue.destination as! TFLTaxisRankStreetViewController)
                //tflTaxisRankMapViewController.delegate = self
                tflTaxisRankStreetViewController.selectedTFLApiPlaceTaxiRank = self.selectedTFLApiPlaceTaxiRank
                
            }else{
                print("ERROR:not TFLTaxisRankMapViewController")
            }
        }
        else if(segue.identifier == segueTFLTaxisRankListViewControllerTOTFLTaxisRankMapDirectionsViewController){
            
            if(segue.destination.isMember(of: TFLTaxisRankMapDirectionsViewController.self)){
                
                let tflTaxisRankMapDirectionsViewController = (segue.destination as! TFLTaxisRankMapDirectionsViewController)
                //tflTaxisRankMapDirectionsViewController.delegate = self
                tflTaxisRankMapDirectionsViewController.selectedTFLApiPlaceTaxiRank = self.selectedTFLApiPlaceTaxiRank
                
            }
            else{
                print("ERROR:not TFLTaxisRankMapDirectionsViewController")
            }
        }
        else{
            print("UNHANDLED SEGUE:\(segue.identifier)")
        }
    }
}
