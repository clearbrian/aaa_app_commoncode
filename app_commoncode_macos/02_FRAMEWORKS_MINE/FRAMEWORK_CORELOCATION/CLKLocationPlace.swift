//
//  CLKNearestStreetPlace.swift
//  joyride
//
//  Created by Brian Clear on 18/10/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
class CLKLocationPlace : CLKPlace{

    //PLACE PICKER - center of map
    // TODO: - CLEANUP after test
//    init(clLocation: CLLocation, name: String) {
//        
//        // TODO: - is this class needed - we set the super init then here we see subclass specific info but there is none
//        // TODO: - remove geoCodeLocation
//        super.init(clLocation:clLocation, name: name, clkPlaceWrapperType: .geoCodeLocation)
//    }
    
    
    //------------------------------------------------------------------------------------------------
    //"Nearest Street Address"
    //"User Location"
    //------------------------------------------------------------------------------------------------
    //Geocode - Current location > Geocoded to street address > locality used
    //Geocode - Place Picker map center > Geocoded to street address - dont show name only address
    init() {
        
        // TODO: - is this class needed - we set the super init then here we see subclass specific info but there is none
        // TODO: - remove geoCodeLocation
        super.init(clLocation:nil, name: nil, clkPlaceWrapperType: .geoCodeLocation)
        
        
    }
    
    var clLocationIsValid: Bool{
        var clLocationIsValid_ = false
        
        if let clLocation = self.clLocation {
            clLocationIsValid_ = clLocation.isValid
        }else{
            appDelegate.log.error("self.clLocation is nil - clLocation.isValid not set yet")
        }
        return clLocationIsValid_
        
    }
    
    override var name :String? {
        get {
            //---------------------------------------------------------
            var nameReturned :String?
            //Use Case: Nearby > Street Address > name should be nil to prevent "14 East Smithfield, 14 East Smithfield, London E1W 1AP, UK"
            //Use Case: Nearby > Street Address > TRIPVC > Start pin should be self.formatted_address_first_line - done in TripVC addPointToToMapForPlace
            
            //DO NOT Fallback to self.formatted_address_first_line here - OK to return nil to prevent "14 East Smithfield, 14 East Smithfield, London E1W 1AP, UK"
            //---------------------------------------------------------------------
            // TODO: - save to DB needs a name - do when we save RealmPlace
            nameReturned = nil
            
            
            //------------------------------------------------------------------------------------------------
            return nameReturned
            //------------------------------------------------------------------------------------------------
        }
        // TODO: - CLEANUP after test
        //        set {
        //            /*
        //            //------------------------------------------------------------------------------------------------
        //            //PASS INTO INTERNAL TYPE
        //            //------------------------------------------------------------------------------------------------
       
        //            //---------------------------------------------------------------------
        //            //CHANGED BY EDIT
        //            _name = newValue
        //            //------------------------------------------------------------------------------------------------
        //        }
    }
    
}
