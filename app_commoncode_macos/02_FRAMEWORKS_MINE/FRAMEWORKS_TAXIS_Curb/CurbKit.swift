//
//  CurbKit.swift
//  joyride
//
//  Created by Brian Clear on 16/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation

class OpenAppConfigcurb: OpenAppConfig{
    
}

/*
 https://developer.curb.com/docs/deeplinking
 
 https://docs.google.com/document/d/1Gn3Fo5-jWkGZumJLlEgK1nAizxCCMm5T0X4qTF7yeLE/edit
 //make sure you add this to Info.plist LSApplicationQueriesSchemes
 //    <key>CFBundleURLSchemes</key>
 //    <array>
 //    <string>fb1624443627822246</string>
 //    </array>
 */

class CurbKit : ParentSwiftObject{
    
    //https://itunes.apple.com/gb/app/curb-the-taxi-app/id299226386?mt=8
    //-----------------------------------------------------------------------------------
    static let openAppConfig = OpenAppConfigClipboard(openWithType : OpenWithType.openWithType_Curb,
                                      appSchemeString     : "gocurb://",
                                      appWebsiteURLString : "https://gocurb.com/",
                                      appStoreIdUInt      : 299226386)
    //-----------------------------------------------------------------------------------
    
}
