//
//	GeoJSONPointProperty.swift
//
//	Create by Brian Clear on 26/1/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
//import ObjectMapper


class GeoJSONPointProperty : ParentMappable{

	var cartography : GeoJSONPointCartography?
	var id : String?
	var lines : [GeoJSONPointLine]?
	var name : String?
	var tflIntid : Int?


	class func newInstance(map: Map) -> Mappable?{
		return GeoJSONPointProperty()
	}
	/*required init?(map: Map){}*/
	/*private override init(){}*/

	override func mapping(map: Map)
	{
		cartography <- map["cartography"]
		id <- map["id"]
		lines <- map["lines"]
		name <- map["name"]
		tflIntid <- map["tfl_intid"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
   /* @objc required init(coder aDecoder: NSCoder)
	{
         cartography = aDecoder.decodeObject(forKey: "cartography") as? GeoJSONPointCartography
         id = aDecoder.decodeObject(forKey: "id") as? String
         lines = aDecoder.decodeObject(forKey: "lines") as? [GeoJSONPointLine]
         name = aDecoder.decodeObject(forKey: "name") as? String
         tflIntid = aDecoder.decodeObject(forKey: "tfl_intid") as? Int

	}*/

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    /*@objc func encode(with aCoder: NSCoder)
	{
		if cartography != nil{
			aCoder.encode(cartography, forKey: "cartography")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if lines != nil{
			aCoder.encode(lines, forKey: "lines")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if tflIntid != nil{
			aCoder.encode(tflIntid, forKey: "tfl_intid")
		}

	}*/
	var description: String
	{
		var description_ = "**** \(type(of: self)) *****\r"
		description_ = description_ + "cartography: \(String(describing: cartography))\r"
		description_ = description_ + "id: \(String(describing: id))\r"
		description_ = description_ + "lines: \(String(describing: lines))\r"
		description_ = description_ + "name: \(String(describing: name))\r"
		description_ = description_ + "tflIntid: \(String(describing: tflIntid))\r"
		return description_
	}

}
