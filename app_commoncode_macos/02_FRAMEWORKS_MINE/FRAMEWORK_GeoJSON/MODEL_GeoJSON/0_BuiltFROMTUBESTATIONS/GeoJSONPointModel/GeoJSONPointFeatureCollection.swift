//
//	GeoJSONPointFeatureCollection.swift
//
//	Create by Brian Clear on 26/1/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
//import ObjectMapper


class GeoJSONPointFeatureCollection : ParentMappable{

	var features : [GeoJSONPointFeature]?
	var type : String?


	class func newInstance(map: Map) -> Mappable?{
		return GeoJSONPointFeatureCollection()
	}
	/*required init?(map: Map){}*/
	/*private override init(){}*/

	override func mapping(map: Map)
	{
		features <- map["features"]
		type <- map["type"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
   /* @objc required init(coder aDecoder: NSCoder)
	{
         features = aDecoder.decodeObject(forKey: "features") as? [GeoJSONPointFeature]
         type = aDecoder.decodeObject(forKey: "type") as? String

	}*/

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    /*@objc func encode(with aCoder: NSCoder)
	{
		if features != nil{
			aCoder.encode(features, forKey: "features")
		}
		if type != nil{
			aCoder.encode(type, forKey: "type")
		}

	}*/
	var description: String
	{
		var description_ = "**** \(type(of: self)) *****\r"
		description_ = description_ + "features: \(String(describing: features))\r"
		description_ = description_ + "type: \(String(describing: type))\r"
		return description_
	}

}
