//
//  GeoJSONLineStringFeature+Google.swift
//  mac_app_applemaps_tfl
//
//  Created by Brian Clear on 11/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
import GoogleMaps

extension GeoJSONLineStringFeature{
    
    var lineId :String{
        var lineIdString_ = ""
        
        if let geoJSONLineStringFeature: GeoJSONLineStringProperty = self.properties {
            
            if let id = geoJSONLineStringFeature.id {
                lineIdString_ = id
                
            }else{
                appDelegate.log.error("geoJSONPointProperty.name is nil")
            }
        }else{
            appDelegate.log.error("geoJSONPointFeature.properties is nil")
        }
        return lineIdString_
    }
    
    var lineCount: Int{
        var linesCount = 0
        
        if let geoJSONLineStringFeature: GeoJSONLineStringProperty = self.properties {
            
            if let lines: [GeoJSONLineStringLine] = geoJSONLineStringFeature.lines {
                linesCount = lines.count
                
            }else{
                appDelegate.log.error("geoJSONPointProperty.lines is nil")
            }
        }else{
            appDelegate.log.error("geoJSONPointFeature.properties is nil")
        }
        return linesCount
    }
    
    func gmsPolyLineForTFLFeature(strokeWidth: CGFloat) -> GMSPolyline?{
        var gmsPolyline_ : GMSPolyline?
        
        if let geometry: GeoJSONLineStringGeometry = self.geometry {
            
            if let gmsPolyLineFromGeometry = geometry.gmsPolyLineForGeometry(strokeWidth: strokeWidth, strokeColor: self.colorActiveForFeature) {
                gmsPolyline_ = gmsPolyLineFromGeometry
                
                //gmsPolyLine.map = self.gmsMapView
                
            }else{
                appDelegate.log.error("self.EXPR is nil")
            }
            
        }else{
            appDelegate.log.error("geoJSONPointFeature.geometry  is nil")
        }
        
        
        return gmsPolyline_
    }
    
    
    var colorActiveForFeature: UIColor {
        var colorActiveForFeature_: UIColor = .cyan //if something wrong cant use red thats the central line
        
        if let properties = self.properties {
            
            colorActiveForFeature_ = properties.colorActiveLine
            
        }else{
            appDelegate.log.error("self.properties is nil")
        }
        
        return colorActiveForFeature_
    }
    
    
    
    func gmsPolyLineForFeature(strokeWidth: CGFloat, strokeColor: UIColor) -> GMSPolyline?{
        var gmsPolyline_ : GMSPolyline?
        
        
        //---------------------------------------------------------
        //THIN
        //---------------------------------------------------------
        if let properties = self.properties {
            let strokeColor = properties.colorActiveLine
            
            if let geometry: GeoJSONLineStringGeometry = self.geometry {
                
                if let gmsPolyLineFromGeometry = geometry.gmsPolyLineForGeometry(strokeWidth: strokeWidth, strokeColor: strokeColor) {
                    gmsPolyline_ = gmsPolyLineFromGeometry
                    
                    //gmsPolyLine.map = self.gmsMapView
                    
                }else{
                    appDelegate.log.error("self.EXPR is nil")
                }
                
            }else{
                appDelegate.log.error("geoJSONPointFeature.geometry  is nil")
            }
            
        }else{
            appDelegate.log.error("self.EXPR is nil")
        }
        
        
        return gmsPolyline_
    }
}
