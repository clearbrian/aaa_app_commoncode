//
//	GeoJSONLineStringLine.swift
//
//	Create by Brian Clear on 26/1/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
//import ObjectMapper
//import UIKit
extension GeoJSONLineStringLine{
    
    var isClosed: Bool{
        var isClosed_ = false
        
        if let _ = self.closed {
            //non nil
            isClosed_ = true
        }else{
            isClosed_ = false
        }
        return isClosed_
    }
    
    //--------------------------------------------------------------
    // MARK: - opened
    // MARK: -
    //--------------------------------------------------------------
    //    "opened": 2005,
    //    "opened": 2007,
    //    "opened": 2008,
    //    "opened": 2009,
    //    "opened": 2010,
    //    "opened": 2011,
    //    "opened": 2012,
    //    "opened": 2015,
    //2016 - if app run in jan but line opened in aug no way to tell
        
    //    "opened": 2018,
    //    "opened": 2019,
    //    "opened": 2020,
    //    "opened": 9999,
    
    
    var currentYear: Int{
        
        let dateNow = Date()
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: dateNow)
        //let month = calendar.component(.month, from: dateNow)
        //let day = calendar.component(.day, from: dateNow)
        return year
    }
    
    
    //--------------------------------------------------------------
    // MARK: - isActive
    // MARK: -
    //--------------------------------------------------------------
    //Line doenst have "closed"
    //line has "opned" with value > currentYear
    //Werid case - tube lines may have no "opened" or "closed"
    var isActive: Bool{
        var isActive = false
        
        //"closed" - json exists
        if self.isClosed{
            //DEFINATELY CLOSED
            isActive = false
        }else{
            //"closed" - json doesnt exists - check value in "opened"
            if let opened = self.opened {
                if opened > currentYear{
                    isActive = false
                }else{
                    isActive = true
                }
            }else{
                //has no "opened" json entry - special case - some tube lines dont have "closed" or "opened" e.g. Embankment1/MetJub5 etc need to distinguish from opened but in future CrossrailEastAB
                //isActive = false
                isActive = true
            }
        }
        return isActive
    }
    
    
}

/*
 
 "colour": "#000000",
 "colour": "#003688",
 "colour": "#00782A",
 "colour": "#0098D4",
 "colour": "#00A4A7",
 "colour": "#7156A5",
 "colour": "#84B817",
 "colour": "#95CDBA",
 "colour": "#9B0056",
 "colour": "#A0A5A9",
 "colour": "#B36305",
 "colour": "#E32017",
 "colour": "#E51836",
 "colour": "#EE7C0E",
 "colour": "#F3A9BB",
 "colour": "#FFA300",
 "colour": "#FFD300",
 
 
 
 //EXAMPLES
 "closed" : 2005,
 "colour" : "#000000",
 "end_sid" : "910GACTNCTL",
 "last_wd" : 0,
 "name" : "Bakerloo",
 "network" : "Crossrail"
 "night" : true,
 "opened" : 2005,
 
  "ot2end_sid": "910GDALSKLD",
 "otend_sid" : "910GBHILLPK",
 "start_sid" : "910GACTNCTL",
 
 */
class GeoJSONLineStringLine : ParentMappable{

    var closed : Int?
	var colour : String?
    

    
    var end_sid : String?
    var last_wd : Int?
	var name : String?
    
    //comparing lines
    var nameSafe : String{
        if let name = self.name {
            return name
        }else{
            return ""
        }
    }
    
    
    var network : String?
    var night : Bool?
    var opened : Int?
    
    var ot2end_sid : String?
    var otend_sid : String?
    var start_sid : String?


	class func newInstance(map: Map) -> Mappable?{
		return GeoJSONLineStringLine()
	}
	/*required init?(map: Map){}*/
	/*private override init(){}*/

	override func mapping(map: Map)
	{
        closed <- map["closed"]
        colour <- map["colour"]
        end_sid <- map["end_sid"]
        last_wd <- map["last_wd"]
        name <- map["name"]
		network <- map["network"]
        night <- map["night"]
        opened <- map["opened"]
        ot2end_sid <- map["ot2end_sid"]
        otend_sid <- map["otend_sid"]
        start_sid <- map["start_sid"]
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
   /* @objc required init(coder aDecoder: NSCoder)
	{
         colour = aDecoder.decodeObject(forKey: "colour") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         network = aDecoder.decodeObject(forKey: "network") as? String
         opened = aDecoder.decodeObject(forKey: "opened") as? Int

	}*/

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    /*@objc func encode(with aCoder: NSCoder)
	{
		if colour != nil{
			aCoder.encode(colour, forKey: "colour")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if network != nil{
			aCoder.encode(network, forKey: "network")
		}
		if opened != nil{
			aCoder.encode(opened, forKey: "opened")
		}

	}*/
	var description: String
	{
		var description_ = "**** \(type(of: self)) *****\r"
		description_ = description_ + "colour: \(String(describing: colour)))\r"
		description_ = description_ + "name: \(String(describing: name))\r"
		description_ = description_ + "network: \(String(describing: network))\r"
		description_ = description_ + "opened: \(String(describing: opened))\r"
		return description_
	}

}
