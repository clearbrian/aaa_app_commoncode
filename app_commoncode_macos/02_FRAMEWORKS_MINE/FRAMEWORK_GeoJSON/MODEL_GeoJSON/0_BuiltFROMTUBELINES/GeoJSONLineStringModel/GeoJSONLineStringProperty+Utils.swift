//
//  GeoJSONLineStringProperty+iOS.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 11/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

extension GeoJSONLineStringProperty{
    
    var arrayActiveLines : [GeoJSONLineStringLine]{
        var arrayActiveLines_ = [GeoJSONLineStringLine]()
        
        if let lines = self.lines {
            
            for line: GeoJSONLineStringLine in lines{
                if line.isActive {
                    arrayActiveLines_.append(line)
                }else{
                    //appDelegate.log.error("self.isActive is false")
                }
                
            }//for
        }else{
            //appDelegate.log.error(" self.lines is false")
        }
        return arrayActiveLines_
    }
    
    //--------------------------------------------------------------
    // MARK: - isSharedActiveBranch
    // MARK: -
    //--------------------------------------------------------------
    //Shared line Picadilly/District share same line up to Earls court
    //--------------------------------------------------------------
    //    linesCountActive_ > 1  >>> LINE:id:JubDLRAA linesCountAll_:2 linesCountActive_:2
    //    linesCountActive_ > 1  >>> LINE:id:WestBrompton linesCountAll_:2 linesCountActive_:2
    //    linesCountActive_ > 1  >>> LINE:id:MetJub1 linesCountAll_:2 linesCountActive_:2
    //    linesCountActive_ > 1  >>> LINE:id:MetJub2 linesCountAll_:2 linesCountActive_:2
    //--------------------------------------------------------------
    //note - these branches also have shared lines but not active yet
    //WEIRD: linesCountActive_ > 1  >>> LINE:id:CrossrailEastAB linesCountAll_:2 linesCountActive_:0
    //WEIRD: linesCountActive_ > 1  >>> LINE:id:StratBethLink linesCountAll_:2 linesCountActive_:0
    //--------------------------------------------------------------
    var isSharedActiveBranch: Bool{
        var isSharedActiveBranch_: Bool = false
        
        //not > 0  you need more than one line for shared branch
        if self.arrayActiveLines.count > 1{
            
            var currentName_ : String?
            //---------------------------------------------------------
            for line: GeoJSONLineStringLine in self.arrayActiveLines{
                
                if let currentName = currentName_ {
                    if line.nameSafe != currentName{
                        //TWO active lines have different name for same set of lat/lng
                        //so this branch is shared
                        isSharedActiveBranch_ = true
                        break
                    }else{
                        
                    }
                }else{
                    //currentName_is nil - this is row 0
                    //need [0] [1] to start compare
                }
                //store for next loop
                currentName_ = line.nameSafe
                
            }
            //---------------------------------------------------------
        }else{
            isSharedActiveBranch_ = false
        }
        
        return isSharedActiveBranch_
    }

}
