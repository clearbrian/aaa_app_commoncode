//
//  TFLGeoJSONManager+iOS.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 11/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
import AppKit

class TFLGeoJSONManagerParentOS: GeoJSONManager{
    var dictionaryStationImages = [String : NSImage]()
}
