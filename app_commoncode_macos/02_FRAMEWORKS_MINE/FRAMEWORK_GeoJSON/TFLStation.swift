//
//  TFLStation.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 11/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
class TFLStation{
    var name = ""
    var idString = ""
    
    var lat = 0.0
    var lng = 0.0
    
    init(name: String, idString:String, lat: Double, lng: Double){
        self.name = name
        self.idString = idString
        self.lat = lat
        self.lng = lng
    }
}
