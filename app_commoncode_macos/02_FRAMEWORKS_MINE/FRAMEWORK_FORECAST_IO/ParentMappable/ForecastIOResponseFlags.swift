//
//  ForecastIOResponseFlags.swift
//  joyride
//
//  Created by Brian Clear on 13/04/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
/*
flags: {
    sources: [],
    darksky-stations: [],
    datapoint-stations: [],
    isd-stations: [],
    madis-stations: [],
    units: "us"
}
*/


class ForecastIOResponseFlags: ParentMappable {
    
    
    var sources: [AnyObject]?
    var darksky_stations: [AnyObject]?
    var datapoint_stations: [AnyObject]?
    var isd_stations: [AnyObject]?
    var madis_stations: [AnyObject]?
    var units: NSString?

    
    //---------------------------------------------------------------------
    // root calls mapping()
    //    required init?(_ map: Map){
    //        mapping(map: map)
    //    }
    //---------------------------------------------------------------------
    
    //https://github.com/Hearst-DD/ObjectMapper
    override func mapping(map: Map){
        
        sources <- map["sources"]
        darksky_stations <- map["darksky-stations"]
        datapoint_stations <- map["datapoint-stations"]
        isd_stations <- map["isd-stations"]
        madis_stations <- map["madis-stations"]
        units <- map["units"]
        
    }
    
}
