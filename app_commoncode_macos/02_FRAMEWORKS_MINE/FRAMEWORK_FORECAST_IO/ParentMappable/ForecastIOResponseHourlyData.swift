//
//  ForecastIOResponseHourlyData.swift
//  joyride
//
//  Created by Brian Clear on 13/04/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
class ForecastIOResponseHourlyData: ParentMappable {
    
    
    
    var time: NSNumber? // 1460559600,
    var summary: NSString? // "Partly Cloudy",
    var icon: NSString? // "partly-cloudy-day",
    var precipIntensity: NSNumber? // 0,
    var precipProbability: NSNumber? // 0,
    var precipType: NSString? // 0,
    var temperature: NSNumber? // 62.43,
    var apparentTemperature: NSNumber? // 62.43,
    var dewPoint: NSNumber? // 35.12,
    var humidity: NSNumber? // 0.36,
    var windSpeed: NSNumber? // 6.18,
    var windBearing: NSNumber? // 250,
    var visibility: NSNumber? // 10,
    var cloudCover: NSNumber? // 0.32,
    var pressure: NSNumber? // 1008.14,
    var ozone: NSNumber? // 387.7
    
 
    
    //---------------------------------------------------------------------
    // root calls mapping()
    //    required init?(_ map: Map){
    //        mapping(map: map)
    //    }
    //---------------------------------------------------------------------
    
    //https://github.com/Hearst-DD/ObjectMapper
    override func mapping(map: Map){
        
        time <- map["time"]
        summary <- map["summary"]
        icon <- map["icon"]
        precipIntensity <- map["precipIntensity"]
        precipProbability <- map["precipProbability"]
        precipType <- map["precipType"]
        
        temperature <- map["temperature"]
        apparentTemperature <- map["apparentTemperature"]
        dewPoint <- map["dewPoint"]
        humidity <- map["humidity"]
        windSpeed <- map["windSpeed"]
        windBearing <- map["windBearing"]
        visibility <- map["visibility"]
        cloudCover <- map["cloudCover"]
        pressure <- map["pressure"]
        ozone <- map["ozone"]
    }
    
}
/*
{
    time: 1460559600,
    summary: "Partly Cloudy",
    icon: "partly-cloudy-day",
    precipIntensity: 0,
    precipProbability: 0,
    temperature: 62.43,
    apparentTemperature: 62.43,
    dewPoint: 35.12,
    humidity: 0.36,
    windSpeed: 6.18,
    windBearing: 250,
    visibility: 10,
    cloudCover: 0.32,
    pressure: 1008.14,
    ozone: 387.7
},
 */
