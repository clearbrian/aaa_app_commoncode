//
//  ForecastIOResponseHourly.swift
//  joyride
//
//  Created by Brian Clear on 13/04/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
class ForecastIOResponseHourly: ParentMappable {
    
    
    var summary: NSString?  //"Partly Cloudy for the hour",
    var icon: NSString? //"partly-cloudy-day",
    var data: [ForecastIOResponseHourlyData]?
    
    //---------------------------------------------------------------------
    // root calls mapping()
    //    required init?(_ map: Map){
    //        mapping(map: map)
    //    }
    //---------------------------------------------------------------------
    
    //https://github.com/Hearst-DD/ObjectMapper
    override func mapping(map: Map){
        
        summary <- map["summary"]
        icon <- map["icon"]
        data <- map["data"]
        
    }
    
}
//summary: "Partly cloudy for the hour.",
//icon: "partly-cloudy-day",
//data: []
