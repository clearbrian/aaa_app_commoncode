//
//  ForecastIOController.swift
//  joyride
//
//  Created by Brian Clear on 13/04/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
class ForecastIOController : ParentSwiftObject{
    
    let forecastIOWSController = ForecastIOWSController()

    
    override init(){
        super.init()
        
    }
    
    func getForecast(_ forecastIOForecastRequest: ForecastIOForecastRequest,
                              success: @escaping (_ forecastIOForecastResponse: ForecastIOForecastResponse?) -> Void,
                              failure: @escaping (_ error: Error?) -> Void
        )
    {
        //---------------------------------------------------------------------
        self.forecastIOWSController.get_forecast(forecastIOForecastRequest,
            success:{
                (forecastIOForecastResponse: ForecastIOForecastResponse?)->Void in
                //---------------------------------------------------------------------
                if let forecastIOForecastResponse = forecastIOForecastResponse{
                    self.log.info("forecastIOForecastResponse returned:\(forecastIOForecastResponse)")
                    
                    success(forecastIOForecastResponse)
                }else{
                    self.log.error("clkForecastIOResponse is nil")
                }
                
            },
            failure:{
                (error) -> Void in
                return failure(error)
            }
        )
        //---------------------------------------------------------------------
    }
}
