//
//  ForecastIOPlace.swift
//  joyride
//
//  Created by Brian Clear on 15/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation

/*
uber://?client_id=YOUR_CLIENT_ID
&action=setPickup

&pickup[latitude]=37.775818
&pickup[longitude]=-122.418028
&pickup[nickname]=ForecastIOHQ
&pickup[formatted_address]=1455 Market St, San Francisco, CA 94103

&dropoff[latitude]=37.802374
&dropoff[longitude]=-122.405818
&dropoff[nickname]=Coit Tower
&dropoff[formatted_address]=1 Telegraph Hill Blvd, San Francisco, CA 94133
&product_id=a1111c8c-c720-46c3-8534-2fcdd730040d


*/

enum ForecastIOPlaceType : String {
    case Pickup = "pickup"
    case Dropoff = "dropoff"
}

class ForecastIOPlace : ParentSwiftObject{
    
    var latitude : String?
    var longitude : String?
    var nickname : String?
    var formatted_address : String?
    var uberPlaceType : ForecastIOPlaceType?
    
    override init(){
        
    }
    init(uberPlaceType : ForecastIOPlaceType){
        self.uberPlaceType = uberPlaceType
    }
}
