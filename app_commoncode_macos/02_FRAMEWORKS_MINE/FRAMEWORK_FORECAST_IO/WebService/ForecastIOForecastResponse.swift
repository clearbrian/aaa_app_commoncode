//
//  ForecastIOForecastResponse.swift
//  joyride
//
//  Created by Brian Clear on 13/04/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation


class ForecastIOForecastResponse: ParentMappable {

    var latitude: NSNumber?
    var longitude: NSNumber?
    var timezone: NSString?
    var offset: NSNumber?
    var currently: ForecastIOResponseCurrently?
    var minutely: ForecastIOResponseMinutely?
    var hourly: ForecastIOResponseHourly?
    var daily: ForecastIOResponseDaily?
    var flags: ForecastIOResponseFlags?
    
    //---------------------------------------------------------------------
    // root calls mapping()
    //    required init?(_ map: Map){
    //        mapping(map: map)
    //    }
    //---------------------------------------------------------------------
    
    //https://github.com/Hearst-DD/ObjectMapper
    override func mapping(map: Map){
        
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        timezone <- map["timezone"]
        offset <- map["offset"]
        currently <- map["currently"]
        minutely <- map["minutely"]
        hourly <- map["hourly"]
        daily <- map["daily"]
        flags <- map["flags"]
        
    }
    
}
/*
latitude: 51.5080776,
longitude: -0.0717707,
timezone: "Europe/London",
offset: 1,
currently: {},
minutely: {},
hourly: {},
daily: {},
flags: {}
}
 */
