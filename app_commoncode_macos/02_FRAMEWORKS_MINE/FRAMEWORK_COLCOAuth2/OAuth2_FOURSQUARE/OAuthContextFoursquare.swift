//
//  OAuthContextFOURSQUARE.swift
//  joyride
//
//  Created by Brian Clear on 07/07/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

//
//  OAuthContextFOURSQUARE.swift
//  joyride
//
//  Created by Brian Clear on 06/07/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
//--------------------------------------------------------------
// MARK: - OAuthContextFOURSQUARE
// MARK: -
//--------------------------------------------------------------
/*
https://developer.foursquare.com/overview/auth.html
joyrider://oauth/callback_foursquare

Client id
E3HMBZNK40ZQC1WMWGJTCES2MAIA1ALI51WLTVUSL3CTHCIL
Client secret
N454WAYJNAPCJHPHFFLYPXQV0YFPU0VIVSFY0MR3YH1JK0K2
===============================================================================================================================================
1. GRANT AUTH
===============================================================================================================================================
https://foursquare.com/oauth2/authenticate?client_id=YOUR_CLIENT_ID&response_type=code&redirect_uri=YOUR_REGISTERED_REDIRECT_URI

 ------------------------------------------------------------------------------------------
 ------------------------------------------------------------------------------------------
 ------------------------------------------------------------------------------------------
 ------------------------------------------------------------------------------------------
 ------------------------------------------------------------------------------------------
//WORKING - OPEN IN BROWSER - Safari best shows alert with redirect
https://foursquare.com/oauth2/authenticate?client_id=E3HMBZNK40ZQC1WMWGJTCES2MAIA1ALI51WLTVUSL3CTHCIL&response_type=code&redirect_uri=joyrider://oauth/callback_foursquare
 ------------------------------------------------------------------------------------------
 ------------------------------------------------------------------------------------------
 ------------------------------------------------------------------------------------------
 ------------------------------------------------------------------------------------------
 ------------------------------------------------------------------------------------------
REDIRECTS TO
joyrider://oauth/callback_foursquare?code=EEWOOR2GG3WSP12SZM00GBIPSTRLFLILCEDTGXYEDZXSG1RD#_=_.
joyrider://oauth/callback_foursquare?code=YV4UGCS1VZ4UDFQ2GQPUL1RUAKIVDBCSBQ2R2SZFVWNHMCOF#_=_.
joyrider://oauth/callback_foursquare?code=VEEN4EEWZIH3DMBZE2ZSDVKQTTEFT3BHJUM4KIJ23ZVNFGR1#_=_.   <<<< COPY THE CODE TO STEP 2

------------------------------------------------------------------------------------------


===============================================================================================================================================
2. REQUEST ACCESS TOKEN
===============================================================================================================================================

https://foursquare.com/oauth2/access_token
?client_id=YOUR_CLIENT_ID
&client_secret=YOUR_CLIENT_SECRET
&grant_type=authorization_code
&redirect_uri=YOUR_REGISTERED_REDIRECT_URI
&code=CODE

https://foursquare.com/oauth2/access_token?client_id=YOUR_CLIENT_ID&client_secret=YOUR_CLIENT_SECRET&grant_type=authorization_code&redirect_uri=YOUR_REGISTERED_REDIRECT_URI&code=CODE



WORKING  - CHANGE THE CODE!!
https://foursquare.com/oauth2/access_token?client_id=E3HMBZNK40ZQC1WMWGJTCES2MAIA1ALI51WLTVUSL3CTHCIL&client_secret=N454WAYJNAPCJHPHFFLYPXQV0YFPU0VIVSFY0MR3YH1JK0K2&grant_type=authorization_code&redirect_uri=joyrider://oauth/callback_foursquare&code=VEEN4EEWZIH3DMBZE2ZSDVKQTTEFT3BHJUM4KIJ23ZVNFGR1#_=_.

in
open /Users/gbxc/Documents/00_ME/00_BITBUCKET_COLC/app_github_v3_oauth_tester/02_FOURSQUARE
PHASE2_AuthGetToken_FOURSQUARE.httpreq

------------------------------------------------------------------------------------------
200
{
    "access_token": "ZPCT1JPZB3RQXM0Q3M5BAALVYNPASFK22IW40CVO0OBX5NQN"
}

{"access_token": "ZPCT1JPZB3RQXM0Q3M5BAALVYNPASFK22IW40CVO0OBX5NQN"}
    ------------------------------------------------------------------------------------------
WRONG or old code=
    400 - Bad Request
        {
            "error": "invalid_grant"
}
//wrong clientid or clientsecret
401 - Bad Request
    
    {
        "error": "invalid_client"
}
400
&grant_type=xxauthorization_code
//should be
&grant_type=authorization_code
    {
        "error": "unsupported_grant_type"
}
400
&redirect_uri=xxjoyrider://oauth/callback_foursquare
&redirect_uri=joyrider://oauth/callback_foursquare
{
    "error": "redirect_uri_mismatch"
    }
 
 ===============================================================================================
 NOTE - in foursquare MUST use a GET to get token
 /Users/gbxc/Documents/00_ME/00_BITBUCKET_COLC/app_github_v3_oauth_tester/02_FOURSQUARE/PHASE2_AuthGetToken_FOURSQUARE.httpreq
 
 OK - when you update code=
 https://foursquare.com/oauth2/access_token?client_id=E3HMBZNK40ZQC1WMWGJTCES2MAIA1ALI51WLTVUSL3CTHCIL&client_secret=N454WAYJNAPCJHPHFFLYPXQV0YFPU0VIVSFY0MR3YH1JK0K2&grant_type=authorization_code&redirect_uri=joyrider://oauth/callback_foursquare&code=Z1BIC44ZYOJXSQYYNRLWJBCULBK4UANXIXAU5XTUFQVCFJUX#_=_.
 
 FAILS
 Content-Type
 application/json; charset=utf-8
 {
 "client_id": "E3HMBZNK40ZQC1WMWGJTCES2MAIA1ALI51WLTVUSL3CTHCIL",
	"client_secret": "N454WAYJNAPCJHPHFFLYPXQV0YFPU0VIVSFY0MR3YH1JK0K2",
	"grant_type": "authorization_code",
	"redirect_uri": "joyrider://oauth/callback_foursquare",
	"code": "Z1BIC44ZYOJXSQYYNRLWJBCULBK4UANXIXAU5XTUFQVCFJUX#_=_."
 }
 401 - Bad Request
 {
 "error": "unsupported_grant_type"
 }

 
 
    
===============================================================================================
3. CALL API WITH ACCESS TOKEN
===============================================================================================
 
{"access_token": "ZPCT1JPZB3RQXM0Q3M5BAALVYNPASFK22IW40CVO0OBX5NQN"}

ENDPOINTS
https://developer.foursquare.com/docs/


https://developer.foursquare.com/docs/users/users

https://api.foursquare.com/v2/users/USER_ID

curl https://api.foursquare.com/v2/users/self/checkins?oauth_token=ACCESS_TOKEN&v=YYYYMMDD

https://api.foursquare.com/v2/users/self/checkins?oauth_token=ZPCT1JPZB3RQXM0Q3M5BAALVYNPASFK22IW40CVO0OBX5NQN&v=20160707

{
    "notifications": [
    {
    "item": {
    "unreadCount": 0
    },
    "type": "notificationTray"
    }
    ],
    "meta": {
        "code": 200,
        "requestId": "577e88ab498e694bbc46ac2c"
    },
    "response": {
        "checkins": {
            "items": [
            {
            "isMayor": false,
            "source": {
            "url": "http://www.cityoflondonconsulting.com",
            "name": "TestApp"
            },
            "timeZoneOffset": 0,
            "likes": {
            "count": 0,
            "groups": [
            
            ]
            },
            "createdAt": 1454931147,
            "type": "checkin",
            "id": "56b87ccb498e76e3b896f2d1",
            "posts": {
            "count": 0,
            "textCount": 0
            },
            "venue": {
            "location": {
            "country": "United Kingdom",
            "formattedAddress": [
            "Piccadilly Circus",
            "London",
            "Greater London",
            "W1D 1NN"
            ],
            "lng": -0.1344752311706543,
            "city": "London",
            "cc": "GB",
            "state": "Greater London",
            "postalCode": "W1D 1NN",
            "lat": 51.50990434225196,
            "address": "Piccadilly Circus"
            },
            "contact": {
            
            },
            "name": "Piccadilly Circus",
            "verified": false,
            "storeId": "",
            "id": "4c9c6a059c48236a1cb14dee",
            "stats": {
            "usersCount": 70610,
            "tipCount": 475,
            "checkinsCount": 121077
            },
            "like": false,
            "categories": [
            {
            "primary": true,
            "icon": {
            "suffix": ".png",
            "prefix": "https://ss3.4sqi.net/img/categories_v2/parks_outdoors/plaza_"
            },
            "name": "Plaza",
            "id": "4bf58dd8d48988d164941735",
            "shortName": "Plaza",
            "pluralName": "Plazas"
            }
            ]
            },
            "photos": {
            "items": [
            
            ],
            "count": 0
            },
            "comments": {
            "count": 0
            },
            "like": false
            }
            ],
            "count": 1
        }
    }
}


*/




class OAuthContextFoursquare: OAuthContext{
    
    //---------------------------------------------------------------------
    let oauthAPI: OAuth2API = .Foursquare
    //---------------------------------------------------------------------
    //params required by foursquare thats arent in oauth2 spec
    var foursquare_response_type: String?
    
    var foursquare_grant_type: String?
    

    //---------------------------------------------------------------------
    //ACCESS TOKEN
    //---------------------------------------------------------------------
    //access_token is in here
    var oauth2AccessTokenFoursquareResponse: OAuth2AccessTokenFoursquareResponse?
    
    
    //---------------------------------------------------------------------
    override var access_token: String? {
        var access_token_: String? = nil
        
        if let oauth2AccessTokenFoursquareResponse = self.oauth2AccessTokenFoursquareResponse {
            if let access_token = oauth2AccessTokenFoursquareResponse.access_token {
                access_token_ = access_token
                
            }else{
                self.log.error("oauth2AccessTokenFoursquareResponse.access_token is nil")
            }
            
        }else{
            self.log.error("self.oauth2AccessTokenFoursquareResponse is nil")
        }
        return access_token_
    }
    
    //--------------------------------------------------------------
    // MARK: - init
    // MARK: -
    //--------------------------------------------------------------
    init(oauth_redirect_uri  : String,
         oauth_client_id : String,
         oauth_client_secret : String)
    {
        //---------------------------------------------------------------------
        //pass up non github settings
        super.init(oauth_url_authorize : "https://foursquare.com/oauth2/authenticate",
                   oauth_client_id : oauth_client_id,
                   oauth_client_secret : oauth_client_secret,
                   oauth_redirect_uri : oauth_redirect_uri,
                   oauth_url_request_access_token : "https://foursquare.com/oauth2/access_token",
                   stateIsRequired: false)

        //---------------------------------------------------------------------
        //AUTHORIZE
        //&response_type=code
        self.foursquare_response_type = "code"
        //---------------------------------------------------------------------
        //RQUEST
        //&grant_type=authorization_code
        //this is an oauth setting but named differently depending on the api
        self.foursquare_grant_type = "authorization_code"
    }
    
    //--------------------------------------------------------------
    // MARK: - urlStringAuthorize
    // MARK: -
    //--------------------------------------------------------------
    
    //================================================================================================================================
    // 1. GRANT AUTH
    //========================================================================================================================
    //    https://foursquare.com/oauth2/authenticate?client_id=YOUR_CLIENT_ID&response_type=code&redirect_uri=YOUR_REGISTERED_REDIRECT_URI
    //
    //    ------------------------------------------------------------------------------------------
    //    OPEN IN BROWSER - Safari best shows alert with redirect
    //    https://foursquare.com/oauth2/authenticate?client_id=E3HMBZNK40ZQC1WMWGJTCES2MAIA1ALI51WLTVUSL3CTHCIL&response_type=code&redirect_uri=joyrider://oauth/callback_foursquare
    //---------------------------------------------------------------------
    override var urlStringAuthorize : String?{
        
        var urlStringAuthorizeReturned : String? = nil
        

        //if any param missing then super.urlStringAuthorize will be nil
        //if any in subclass missing we should also return nil
        if let urlStringAuthorizeFromSuper = super.urlStringAuthorize{
            
            //String easier to append
            var urlStringAuthorizeFoursquare = urlStringAuthorizeFromSuper
            
            //---------------------------------------------------------------------
            //&response_type=code
            if let foursquare_response_type = self.foursquare_response_type {
                urlStringAuthorizeFoursquare = urlStringAuthorizeFoursquare + "&response_type=\(foursquare_response_type)"
                
            }else{
                self.log.error("foursquare_response_type is nil")
            }
            
            //---------------------------------------------------------------------
            //    //&state=8E7F9061
            //    if let github_state = self.github_state {
            //        urlStringAuthorizeFoursquare = urlStringAuthorizeFoursquare + "&state=\(github_state)"
            //        
            //    }else{
            //        self.log.error("github_state is nil")
            //    }
            //---------------------------------------------------------------------
            
            //only add if all params are set - if not then should be left nil
            urlStringAuthorizeReturned = urlStringAuthorizeFoursquare
            
        }else{
            self.log.error("super.urlStringAuthorize is nil")
            urlStringAuthorizeReturned = nil
        }
        
        //To test copy URL into Safari
        return urlStringAuthorizeReturned
    }
    
    
    override var bodyStringRequestAccessToken : String?{
        
        var bodyStringReturned : String? = nil
        
        //        if self.isValid(){
        
        //===============================================================================================================================================
        //2. REQUEST ACCESS TOKEN
        //===============================================================================================================================================
        //
        //https://foursquare.com/oauth2/access_token
        //?client_id=YOUR_CLIENT_ID
        //&client_secret=YOUR_CLIENT_SECRET
        //&grant_type=authorization_code
        //&redirect_uri=YOUR_REGISTERED_REDIRECT_URI
        //&code=CODE
        //
        //https://foursquare.com/oauth2/access_token?client_id=YOUR_CLIENT_ID&client_secret=YOUR_CLIENT_SECRET&grant_type=authorization_code&redirect_uri=YOUR_REGISTERED_REDIRECT_URI&code=CODE
        //
        //
        //
        //WORKING  - CHANGE THE CODE!!
        //https://foursquare.com/oauth2/access_token?client_id=E3HMBZNK40ZQC1WMWGJTCES2MAIA1ALI51WLTVUSL3CTHCIL&client_secret=N454WAYJNAPCJHPHFFLYPXQV0YFPU0VIVSFY0MR3YH1JK0K2&grant_type=authorization_code&redirect_uri=joyrider://oauth/callback_foursquare&code=VEEN4EEWZIH3DMBZE2ZSDVKQTTEFT3BHJUM4KIJ23ZVNFGR1#_=_.
        //---------------------------------------------------------------------
        ///super.bodyStringRequestAccessToken returns "" added so its future proof
        if let bodyStringRequestAccessTokenFromSuper = super.bodyStringRequestAccessToken{
            
            var bodyStringRequestAccessTokenFoursquare = bodyStringRequestAccessTokenFromSuper
            
            //---------------------------------------------------------------------
            //ALL REQUIRED SO NESTED - IF ANY MISSING THEN ERRORS
            //---------------------------------------------------------------------
            
            //---------------------------------------------------------------------
            //client_id=be45feb27af1729e6312
            //---------------------------------------------------------------------
            if let oauth_client_id = self.oauth_client_id {
                bodyStringRequestAccessTokenFoursquare = bodyStringRequestAccessTokenFoursquare + "client_id=\(oauth_client_id)"
                
                //---------------------------------------------------------------------
                //&client_secret=bbf4bde86a4a3fa193b821cc4a6c610079c306d2
                //---------------------------------------------------------------------
                if let oauth_client_secret = self.oauth_client_secret {
                    bodyStringRequestAccessTokenFoursquare = bodyStringRequestAccessTokenFoursquare + "&client_secret=\(oauth_client_secret)"
                    
                    //---------------------------------------------------------------------
                    //&grant_type
                    //never changes
                    //---------------------------------------------------------------------
                    if let foursquare_grant_type = self.foursquare_grant_type {
                        bodyStringRequestAccessTokenFoursquare = bodyStringRequestAccessTokenFoursquare + "&grant_type=\(foursquare_grant_type)"
                        

                        //---------------------------------------------------------------------
                        // &redirect_uri=joyrider://oauth/callback_github
                        //---------------------------------------------------------------------
                        if let oauth_redirect_uri = self.oauth_redirect_uri {
                            bodyStringRequestAccessTokenFoursquare = bodyStringRequestAccessTokenFoursquare + "&redirect_uri=\(oauth_redirect_uri)"
                            
                            
                            //---------------------------------------------------------------------
                            //&code=5c11a5a28052e8acc149
                            //generated by /authorize step
                            //---------------------------------------------------------------------
                            if let oauth_authorize_code = self.oauth_authorize_code {
                                bodyStringRequestAccessTokenFoursquare = bodyStringRequestAccessTokenFoursquare + "&code=\(oauth_authorize_code)"
                                
                                //---------------------------------------------------------------------
                                // TODO: - add state? - not in spec
                                //&state=8E7F9061
                                //---------------------------------------------------------------------
                                // if let github_state = self.github_state {
                                //     bodyStringRequestAccessTokenFoursquare = bodyStringRequestAccessTokenFoursquare + "&state=\(github_state)"
                                //
                                //     //---------------------------------------------------------------------
                                //     //ALL REQUIRED PARAMETERS SET
                                //     //---------------------------------------------------------------------
                                //     bodyStringReturned = bodyStringRequestAccessTokenFoursquare
                                //     //---------------------------------------------------------------------
                                // }else{
                                //     self.log.error("github_state is nil")
                                // }
                                //

                                //---------------------------------------------------------------------
                                //ALL REQUIRED PARAMETERS SET
                                //---------------------------------------------------------------------
                                bodyStringReturned = bodyStringRequestAccessTokenFoursquare
                                //---------------------------------------------------------------------
                            }else{
                                self.log.error("oauth_authorize_code is nil - should be set after step1 /authorize returns with code=")
                            }
                            
                            
                        }else{
                            self.log.error("oauth_redirect_uri is nil")
                        }
                    }else{
                        self.log.error("foursquare_grant_type is nil")
                    }
                }else{
                    self.log.error("oauth_client_secret is nil")
                }
            }else{
                self.log.error("oauth_client_id is nil")
            }
        }else{
            self.log.error("super.bodyStringRequestAccessToken is nil")
            bodyStringReturned = nil
        }
        
        //To test copy URL into Safari
        return bodyStringReturned
    }
    
    override func buildAccessTokenRequest() -> OAuth2ParentWSRequest?{
        var oauth2ParentWSRequestReturned: OAuth2ParentWSRequest?
        
        if let oauth_url_request_access_token = self.oauth_url_request_access_token {
            
            //------------------------------------------------
            let oauth2AccessTokenFoursquareRequest = OAuth2AccessTokenFoursquareRequest()
            
            oauth2AccessTokenFoursquareRequest.webServiceEndpoint = oauth_url_request_access_token
            
            //GET ACCESS TOKEN in FS is always a get I tried a POST with client= &..& and json body both got 401
            oauth2AccessTokenFoursquareRequest.colcHTTPMethod = .Get
            
            //---------------------------------------------------------------------
            //GET  //POST and non json so can use budy string in url
            //---------------------------------------------------------------------
            //https://foursquare.com/oauth2/access_token?client_id=E3HMBZNK40ZQC1WMWGJTCES2MAIA1ALI51WLTVUSL3CTHCIL&client_secret=N454WAYJNAPCJHPHFFLYPXQV0YFPU0VIVSFY0MR3YH1JK0K2&grant_type=authorization_code&redirect_uri=joyrider://oauth/callback_foursquare&code=Z1BIC44ZYOJXSQYYNRLWJBCULBK4UANXIXAU5XTUFQVCFJUX#_=_.
            //---------------------------------------------------------------------
            
            //---------------------------------------------------------------------
            //GET so no BODY
            //---------------------------------------------------------------------
            //    //---------------------------------------------------------------------
            //    if let bodyStringRequestAccessToken = self.bodyStringRequestAccessToken{
            //        //---------------------------------------------------------------------
            //        //BODY
            //        //---------------------------------------------------------------------
            //        oauth2AccessTokenFoursquareRequest.bodyString = bodyStringRequestAccessToken
            //        //---------------------------------------------------------------------
            //        
            //
            //        
            //        //---------------------------------------------------------------------
            //    }else{
            //        self.log.error("oAuthContext.bodyStringRequestAccessToken is nil")
            //    }
            //---------------------------------------------------------------------
            
            //GET and non json so can use bodyStringRequestAccessToken in url
            oauth2AccessTokenFoursquareRequest.bodyString = nil
            //"client_id=E3HMBZNK40ZQC1WMWGJTCES2MAIA1ALI51WLTVUSL3CTHCIL&client_secret=N454WAYJNAPCJHPHFFLYPXQV0YFPU0VIVSFY0MR3YH1JK0K2&grant_type=authorization_code&oauth_redirect_uri=joyrider://oauth/callback_foursquare"
            if let bodyStringRequestAccessToken = bodyStringRequestAccessToken {
                
                oauth2AccessTokenFoursquareRequest.query = "\(bodyStringRequestAccessToken)"
                
                oauth2ParentWSRequestReturned = oauth2AccessTokenFoursquareRequest
                
            }else{
                self.log.error("Foursquare:bodyStringRequestAccessToken is nil")
            }

        }else{
            self.log.error("oAuthContext.oauth_url_request_access_token is nil")
        }
        
        return oauth2ParentWSRequestReturned
    }
    
    
    //{"access_token":"ZPCT1JPZB3RQXM0Q3M5BAALVYNPASFK22IW40CVO0OBX5NQN"}
    override func parseAccessTokenResponse(_ parsedObject: Any,
                                           success: (_ access_token: String) -> Void,
                                           failure: (_ error: Error) -> Void)
    {
        //---------------------------------------------------------------------
        switch parsedObject{
        case let number as NSNumber:
            self.log.error("UNEXPECTED JSON RESPONSE: NSNumber:\(number)")
            
        case let array as NSArray:
            self.log.error("UNEXPECTED JSON RESPONSE: NSArray:\(array)")
            
        case let nsDictionary as NSDictionary:
            //---------------------------------------------------------------------
            self.log.info("NSDictionary:\(nsDictionary)")
            //---------------------------------------------------------------------
            if let _ = nsDictionary["error"] {
                //---------------------------------------------------------------------
                //ERROR DICTIONARY
                //---------------------------------------------------------------------
                //    NSDictionary:{
                //        error = "bad_verification_code";
                //        "error_description" = "The code passed is incorrect or expired.";
                //        "error_uri" = "https://developer.github.com/v3/oauth/#bad-verification-code";
                //    }
                //---------------------------------------------------------------------
                self.log.error("Error NSDictionary:\(nsDictionary)")
                
                if let oauth2AccessTokenFoursquareErrorResponse : OAuth2AccessTokenFoursquareErrorResponse = Mapper<OAuth2AccessTokenFoursquareErrorResponse>().map(JSONObject:parsedObject){
                    
                    self.log.info("oauth2AccessTokenFoursquareResponse returned:[\r\(oauth2AccessTokenFoursquareErrorResponse)]")
                    
                    //---------------------------------------------------------------------
                    //check response status
                    if let error = oauth2AccessTokenFoursquareErrorResponse.error{
                        switch error{
                            
                        case OAuth2AccessTokenFoursquareError.error.rawValue:
                            
                            self.log.error(OAuth2AccessTokenFoursquareError.error.rawValue)
                            
                            failure(OAuth2AccessTokenFoursquareError.error.nsError())
                            
                        default:
                            self.log.error("UNHANDLED oauth2AccessTokenFoursquareErrorResponse: \(oauth2AccessTokenFoursquareErrorResponse)")
                            
                            failure(NSError.unexpectedResponseObject("UNHANDLED oauth2AccessTokenFoursquareErrorResponse: \(oauth2AccessTokenFoursquareErrorResponse)"))
                        }
                    }else{
                        self.log.error("oauth2AccessTokenFoursquareErrorResponse.error is nil")
                        
                        failure(NSError.unexpectedResponseObject("oauth2AccessTokenFoursquareErrorResponse.error is nil"))
                    }
                    //---------------------------------------------------------------------
                }else{
                    self.log.error("clkPlaceSearchResponse is nil or not CLKGooglePlaceSearchResponse")
                }
                
            }else if let _ = nsDictionary["access_token"] {
                //---------------------------------------------------------------------
                //OK: Parsed Dictionary contains "access_token"
                //---------------------------------------------------------------------
                //{"access_token":"ZPCT1JPZB3RQXM0Q3M5BAALVYNPASFK22IW40CVO0OBX5NQN"}
                self.log.error("OK NSDictionary:\(nsDictionary)")
                
                if let oauth2AccessTokenFoursquareResponse : OAuth2AccessTokenFoursquareResponse = Mapper<OAuth2AccessTokenFoursquareResponse>().map(JSONObject:parsedObject){
                    //------------------------------------------------------------------------------------------------
                    self.log.info("oauth2AccessTokenFoursquareResponse returned:\(oauth2AccessTokenFoursquareResponse.description)")
                    //------------------------------------------------------------------------------------------------
                    // TODO: - save access_token to keychain using key "com.github"
                    
                    //---------------------------------------------------------------------
                    //STEP 2 - get access_token complete - you may now call any API calls using the token
                    //---------------------------------------------------------------------
                    self.oauth2AccessTokenFoursquareResponse = oauth2AccessTokenFoursquareResponse
                    
                    if let access_token = self.access_token {
                        self.log.info("access_token returned:\(access_token)")
                        
                        success(access_token)
                        
                    }else{
                        self.log.error("self.access_token is nil")
                    }
                }else{
                    self.log.error("clkPlaceSearchResponse is nil or not CLKGooglePlaceSearchResponse")
                }
                
                
            }else{
                self.log.error("Parsed Object doesnt contain key 'error' or 'access_token' cant map to ParentMappable: NSDictionary:\(nsDictionary)")
            }
        //---------------------------------------------------------------------
        default:
            self.log.error("UNEXPECTED JSON RESPONSE: UNKNOWN TYPE")
        }
    }
}
