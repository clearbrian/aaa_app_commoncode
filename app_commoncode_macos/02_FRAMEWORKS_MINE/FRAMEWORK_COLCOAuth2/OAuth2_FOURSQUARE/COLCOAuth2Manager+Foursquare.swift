//
//  COLCOAuth2Manager+Foursquare.swift
//  joyride
//
//  Created by Brian Clear on 08/07/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
/*
    ===============================================================================================
    3. CALL API WITH ACCESS TOKEN
    ===============================================================================================
        
    {"access_token": "ZPCT1JPZB3RQXM0Q3M5BAALVYNPASFK22IW40CVO0OBX5NQN"}

    ENDPOINTS
    https://developer.foursquare.com/docs/


    https://developer.foursquare.com/docs/users/users

    https://api.foursquare.com/v2/users/USER_ID

    https://api.foursquare.com/v2/users/self?oauth_token=ACCESS_TOKEN&v=YYYYMMDD
 
 ZPCT1JPZB3RQXM0Q3M5BAALVYNPASFK22IW40CVO0OBX5NQN

    curl https://api.foursquare.com/v2/users/self/checkins?oauth_token=ACCESS_TOKEN&v=YYYYMMDD

    https://api.foursquare.com/v2/users/self/checkins?oauth_token=ZPCT1JPZB3RQXM0Q3M5BAALVYNPASFK22IW40CVO0OBX5NQN&v=20160707
*/



extension COLCOAuth2Manager{

    func callApi_FoursquareUser(
        success: @escaping (_ foursquareUser : FoursquareUser) -> Void,
                failure: @escaping (_ error: Error) -> Void
        )
    {
        
        if let access_token = self.accessTokenForOAuth2API(.Foursquare) {
            
            //---------------------------------------------------------------------
            let foursquareUserRequest = FoursquareUserRequest(access_token:access_token)
            
            //DEBUG
            //let foursquareUserRequest = FoursquareUserRequest(access_token:"CRAP_TOKEN")
            //---------------------------------------------------------------------
            self.callApi_Foursquare(foursquareUserRequest,
                                    responseNSDictionary: { (nsDictionary) in
                                        //---------------------------------------------------------------------
                                        //200 and no key "error"
                                        //---------------------------------------------------------------------
                                        //response =     {
                                        //    user =         {
                                        if let userDictionary = nsDictionary["user"] as? NSDictionary{
                                            
                                            if let foursquareUser : FoursquareUser = Mapper<FoursquareUser>().map(JSONObject:userDictionary){
                                                //--------------------------------------------------------------------
                                                self.log.info("foursquareUser returned:\(foursquareUser.description)")
                                                //--------------------------------------------------------------------
                                                success(foursquareUser)
                                                
                                            }else{
                                                self.log.error("parsedObject is nil or not FoursquareUser")
                                            }
                                            
                                            
                                        }else{
                                            failure(NSError.errorWithDescription("Got 200 but 'response' missing", code: 864953))
                                        }
                                        
                                        
                                        
                                        //---------------------------------------------------------------------
                },
                                    responseNSArray:{ (nsArray: NSArray)->Void in
                                        self.log.error("UNEXPECTED NSArray should be single FoursquareUser")
                },
                                    failure: { (error: Error)-> Void in
                                        failure(error)
                                        
            })
            //---------------------------------------------------------------------
            
        }else{
            self.log.error("self.accessTokenForOAuth2API(.Foursquare) is nil")
        }
    }
    
    func callApi_FoursquareVenuesSearch(
        success: @escaping (_ foursquareVenuesSearch : FoursquareVenuesSearch) -> Void,
                failure: @escaping (_ error: Error) -> Void
        )
    {
        
        if let access_token = self.accessTokenForOAuth2API(.Foursquare) {
            
            //---------------------------------------------------------------------
            let foursquareVenuesSearchRequest = FoursquareVenuesSearchRequest(access_token:access_token)
            
            //DEBUG
            //let foursquareVenuesSearchRequest = FoursquareVenuesSearchRequest(access_token:"CRAP_TOKEN")
            //---------------------------------------------------------------------
            self.callApi_Foursquare(foursquareVenuesSearchRequest,
                                    responseNSDictionary: { (nsDictionary) in
                                        //---------------------------------------------------------------------
                                        //200 and no key "error"
                                        //---------------------------------------------------------------------
                                        //response =     {
                                        //    user =         {
                                        if let userDictionary = nsDictionary["user"] as? NSDictionary{
                                            
                                            if let foursquareVenuesSearch : FoursquareVenuesSearch = Mapper<FoursquareVenuesSearch>().map(JSONObject:userDictionary){
                                                //--------------------------------------------------------------------
                                                self.log.info("foursquareVenuesSearch returned:\(foursquareVenuesSearch.description)")
                                                //--------------------------------------------------------------------
                                                success(foursquareVenuesSearch)
                                                
                                            }else{
                                                self.log.error("parsedObject is nil or not FoursquareVenuesSearch")
                                            }
                                            
                                            
                                        }else{
                                            failure(NSError.errorWithDescription("Got 200 but 'response' missing", code: 864953))
                                        }
                                        
                                        
                                        
                                        //---------------------------------------------------------------------
                },
                                    responseNSArray:{ (nsArray: NSArray)->Void in
                                        self.log.error("UNEXPECTED NSArray should be single FoursquareVenuesSearch")
                },
                                    failure: { (error: Error)-> Void in
                                        failure(error)
                                        
            })
            //---------------------------------------------------------------------
            
        }else{
            self.log.error("self.accessTokenForOAuth2API(.Foursquare) is nil")
        }
    }
    
    
    
    //Call any Foursquare API call - handle common errors such as 401 etc
    func callApi_Foursquare(_ oauth2ParentWSRequest: OAuth2ParentWSRequest,
                             responseNSDictionary: @escaping (_ nsDictionary : NSDictionary) -> Void,
                                  responseNSArray: @escaping (_ nsArray : NSArray) -> Void,
                            failure: @escaping (_ error: Error) -> Void
        )
    {
        
        self.callOAuthWS(oauth2ParentWSRequest,
                         responseNSDictionary:{ (nsDictionary: NSDictionary, statusCode: Int) -> Void in
                            
                            //---------------------------------------------------------------------
                            //Responses and errors vary by api call so handled at lowest level  e.g. Github Foursquare classes
                            //---------------------------------------------------------------------
                            //Foursquare wraps all calls in meta/
                            /*
                             {"meta":{"code":410,...","requestId":"577fc2dd498e7c4752543822"},"response":{}}
                             "meta": { "code": 200,"requestId": "577fbc0f498eac86cece9ee8"
                             },
                             */
                            //---------------------------------------------------------------------
                            if let metaDictionary = nsDictionary["meta"] as? NSDictionary
                            {
                                if let statusCodeNSNumber = metaDictionary["code"] as? NSNumber
                                {
                                    if statusCodeNSNumber.intValue == 200
                                    {
                                        //---------------------------------------------------------------------
                                        //information is in "response"
                                        //---------------------------------------------------------------------
                                        if let responseDictionary = nsDictionary["response"] as? NSDictionary{
                                            
                                            responseNSDictionary(responseDictionary)
                                            
                                        }else{
                                            failure(NSError.errorWithDescription("Got 200 but 'response' missing", code: 864953))
                                        }
                                    }
                                    else{
                                        //---------------------------------------------------------------------
                                        //ERRORS
                                        //---------------------------------------------------------------------
                                        //Access token may have expired or been revoked
                                        if statusCodeNSNumber.intValue == 400
                                        {
                                            //---------------------------------------------------------------------
                                            //400 - Foursquare {"error":"invalid_grant"}
                                            //---------------------------------------------------------------------
                                            // TODO: - could have 401 AND valid json
                                            //{"message":"Bad credentials","documentation_url":"https://developer.github.com/v3"}
                                            
                                            failure(NSError.appError(AppError.oAuth2_400_access_token_error))
                                            
                                        }
                                            //else if statusCodeNSNumber.integerValue == 410
                                            //{
                                            //    //---------------------------------------------------------------------
                                            //    //410 - FS - version missing from end of access token
                                            //    //WRONG - 410
                                            //    //https://api.foursquare.com/v2/users/self?oauth_token=ZPCT1JPZB3RQXM0Q3M5BAALVYNPASFK22IW40CVO0OBX5NQN
                                            //    //OK 200
                                            //    //https://api.foursquare.com/v2/users/self?oauth_token=ZPCT1JPZB3RQXM0Q3M5BAALVYNPASFK22IW40CVO0OBX5NQN&v=20160707
                                            //
                                            //
                                            
                                            //
                                            //    failure(NSError.appError(AppError.OAuth2_Github_401_not_authorized))
                                            //
                                            //}
                                        else{
                                            //---------------------------------------------------------------------
                                            //FOURSQUARE ERRORS - 410
                                            //---------------------------------------------------------------------
                                            //{"meta":{"code":410,"errorType":"param_error","errorDetail":"The Foursquare API no longer supports requests that do not pass in a version parameter. For more details see https:\/\/developer.foursquare.com\/overview\/versioning","requestId":"577fc2dd498e7c4752543822"},"response":{}}
                                            
                                            
                                            //---------------------------------------------------------------------
                                            //error is in "errorType" "errorDetail"
                                            //---------------------------------------------------------------------
                                            if let errorTypeString: String = metaDictionary["errorType"] as? String{
                                                if let errorDetailString: String = metaDictionary["errorDetail"] as? String{
                                                    self.log.error("ERROR statusCode:\(statusCodeNSNumber.intValue)")
                                                    
                                                    failure(NSError.errorWithDescription("[\(statusCodeNSNumber.intValue)][\(errorTypeString)][\(errorDetailString)]", code: statusCodeNSNumber.intValue))
                                                }else{
                                                    failure(NSError.errorWithDescription("Got Foursquare error but 'errorDetail' missing or not String", code: 2343))
                                                }
                                            }else{
                                                failure(NSError.errorWithDescription("Got Foursquare error  but 'errorType' missing or not String", code: 2342))
                                            }
                                        }
                                    }
                                    
                                }else{
                                    self.log.error("Parsed Object statusCodeNSNumber = metaDictionary['code'] as? NSNumber FAILED: NSDictionary:\(nsDictionary)")
                                }
                            }
                            else{
                                self.log.error("Parsed Object doesnt contain key 'error' or 'access_token' cant map to ParentMappable: NSDictionary:\(nsDictionary)")
                            }
                            
                            
            },
                         responseNSArray:{ (nsArray: NSArray, statusCode: Int)->Void in
                            //---------------------------------------------------------------------
                            //NSArray
                            //---------------------------------------------------------------------
                            if statusCode == 200
                            {
                                //if error then would be NSDictionary above
                                responseNSArray(nsArray)
                            }
                            else{
                                //---------------------------------------------------------------------
                                //not a 200 - definately an error
                                //---------------------------------------------------------------------
                                //Access token may have expired or been revoked
                                if statusCode == 400
                                {
                                    failure(NSError.appError(AppError.oAuth2_400_access_token_error))
                                }
                                else if statusCode == 401{
                                    failure(NSError.appError(AppError.oAuth2_Foursquare_401_not_authorized))
                                }
                                else{
                                    failure(NSError.errorWithDescription("UNHANDLED Foursquare statusCode:\(statusCode):[\(nsArray)]", code: 2342))
                                }
                            }
                            //---------------------------------------------------------------------
                            
                            
            },
                         //SWIFT3
            //            failure:{ (parsedObject: Any) -> Void in
            //                            failure(self.parseErrorFoursquare(parsedObject))
            //            }
            
            failure:{ (error: Error) -> Void in
                failure(error)
            }
        )
    }
    
    
    
    func parseErrorFoursquare(_ parsedObject: Any) -> NSError{
        //---------------------------------------------------------------------
        //ERROR DICTIONARY
        //---------------------------------------------------------------------

        //---------------------------------------------------------------------
        if let oauth2AccessTokenFoursquareErrorResponse : OAuth2AccessTokenFoursquareErrorResponse = Mapper<OAuth2AccessTokenFoursquareErrorResponse>().map(JSONObject:parsedObject){
            
            self.log.info("oauth2AccessTokenFoursquareResponse returned:[\r\(oauth2AccessTokenFoursquareErrorResponse)]")
            
            //---------------------------------------------------------------------
            //check response status
            if let error = oauth2AccessTokenFoursquareErrorResponse.error{
                switch error{
                    
                case OAuth2AccessTokenFoursquareError.error.rawValue:
                    
                    self.log.error(OAuth2AccessTokenFoursquareError.error.rawValue)
                    return OAuth2AccessTokenFoursquareError.error.nsError()
                    
                default:
                    self.log.error("UNHANDLED oauth2AccessTokenFoursquareErrorResponse: \(oauth2AccessTokenFoursquareErrorResponse)")
                    return NSError.unexpectedResponseObject("UNHANDLED oauth2AccessTokenFoursquareErrorResponse: \(oauth2AccessTokenFoursquareErrorResponse)")
                }
            }else{
                self.log.error("oauth2AccessTokenFoursquareErrorResponse.error is nil")
                return NSError.unexpectedResponseObject("oauth2AccessTokenFoursquareErrorResponse.error is nil")
                
            }
            //---------------------------------------------------------------------
        }else{
            self.log.error("parsedObject is nil or not OAuth2AccessTokenFoursquareErrorResponse")
            return NSError.unexpectedResponseObject("parsedObject is nil or not OAuth2AccessTokenFoursquareErrorResponse")
        }
        //---------------------------------------------------------------------
        //---------------------------------------------------------------------
    }
}



