//
//  FoursquareViewController.swift
//  joyride
//
//  Created by Brian Clear on 20/07/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//
import Foundation
import UIKit

protocol FoursquareViewControllerDelegate {
    func controllerDidSelectCOLCFoursquareEvent(_ foursquareViewController: FoursquareViewController, colcFoursquareEvent: COLCFoursquareEvent)
    func foursquareViewControllerCancelled(_ foursquareViewController: FoursquareViewController)
}

//removed from project and storyboard backed up AND from bridging header

class FoursquareViewController: ParentViewController, UITableViewDataSource, UITableViewDelegate, COLCOAuth2ManagerDelegate
{
    
    var delegate: FoursquareViewControllerDelegate?
    
    var arrayCOLCFoursquareEvent = [COLCFoursquareEvent]()
    
    
    @IBOutlet weak var tableView: UITableView!

    @IBOutlet weak var buttonLogin: UIButton!
    
    @IBOutlet weak var labelNearestAddress: UILabel!
    
    func preferredContentSizeChanged(_ notification: Notification){
        self.tableView.invalidateIntrinsicContentSize()
        self.tableView.reloadData()
        
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //------------------------------------------------------------------------------------------------
        //to call applicationDidBecomeActive_refreshTableView
        //preferredContentSizeChanged only works sometimes
        appDelegate.topParentViewController = self
        //-----------------------------------------------------------------------------------------------
        //NSNotificationCenter.defaultCenter().addObserver(self,
        //                                                 selector: #selector(FoursquareViewController.preferredContentSizeChanged(_:)),
        //                                                 name: UIContentSizeCategoryDidChangeNotification, object: nil)
        //------------------------------------------------------------------------------------------------
        //Foursquare
        //------------------------------------------------------------------------------------------------
        //works but not sure how to ask for events persmission - use loginbutton
        //OK but tried it in IB but wasnt sure how to set non default permissions
        //UNTESTED loginButton.readPermissions = ["public_profile", "email", "user_events", "user_likes", "user_tagged_places", "user_hometown", "user_videos"]
        //let loginButton = FoursquareSDKLoginButton()
        //loginButton.delegate = self
        //loginButton.center = self.view.center
        //self.view.addSubview(loginButton)
        
        //-----------------------------------------------------------------------------------
        //DYNAMIC TEXT and TABLE ROW HEIGHT
        //-----------------------------------------------------------------------------------
        ////can done in DataSource methods
        //self.tableView.estimatedRowHeight = 170.0
        ////also done in heightForRowAtIndexPath
        //self.tableView.rowHeight = UITableViewAutomaticDimension;
        
        //also done in heightForRowAtIndexPath
        //self.tableView.rowHeight = 130.0
        
        //        self.tableView.sectionHeaderHeight = UITableViewAutomaticDimension;
        //        self.tableView.estimatedSectionHeaderHeight = 80.0f;
        //------------------------------------------------------------------------------------
        
        self.buttonLogin.layer.cornerRadius = 4.0
        
        
        //------------------------------------------------------------------------------------
        //if logged into Foursquare then get events immediately
        //------------------------------------------------------------------------------------
// TODO: - <#COMMENT#>
        //        if let _ = FoursquareSDKAccessToken.currentAccessToken(){
//            changeButtonToLogout()
//            
//            dumpCurrentAccessToken()
//            
//            self.getUserEvents()
//            
//        }else{
//            self.log.info("No Foursquare token - tap button to login")
//        }
        
        //------------------------------------------------------------------------------------
        
        
    }
    
    //dynamic type - if changed in iOS setting need to reload the table
    override func applicationDidBecomeActive_refreshTableView(){
        self.tableView.reloadData()
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //--------------------------------------------------------------
    // MARK: - FoursquareSDKLoginButtonDelegate
    // MARK: -
    //--------------------------------------------------------------
    
    func clearTable(){
        DispatchQueue.main.async{
            self.arrayCOLCFoursquareEvent = [COLCFoursquareEvent]()
            self.tableView.reloadData()
        }
    }
    func changeButtonToLogout(){
        DispatchQueue.main.async{
            self.buttonLogin.setTitle("Logout", for: UIControlState())
        }
    }
    func changeButtonToLogin(){
        DispatchQueue.main.async{
            self.buttonLogin.setTitle("Login to Foursquare", for: UIControlState())
        }
    }
    
    @IBAction func buttonLogin_TouchUpInside(_ sender: AnyObject) {
    
        
        appDelegate.colcOAuth2Manager.delegate = self
        //---------------------------------------------------------------------
        //Comment in ONLY one
        //---------------------------------------------------------------------
        //        appDelegate.colcOAuth2Manager.authorizeWithExternalSafari(.Github)
        //        appDelegate.colcOAuth2Manager.authorizeWithSafariInViewController(self, oauth2API: .Github)
        //---------------------------------------------------------------------
        //        appDelegate.colcOAuth2Manager.authorizeWithExternalSafari(.Foursquare)
        appDelegate.colcOAuth2Manager.authorizeWithSafariInViewController(self, oauth2API: .Foursquare)
        //---------------------------------------------------------------------
        
    }
    

    
    
    //--------------------------------------------------------------
    // MARK: - COLCOAuth2ManagerDelegate
    // MARK: -
    //--------------------------------------------------------------
    
    func accessTokenReceivedOK(_ colcOAuth2Manager: COLCOAuth2Manager, access_token: String, oAuthContext: OAuthContext){
        
        if let _ = oAuthContext as? OAuthContextGithub {
            //callApi_GithubUser()
            callApi_GithubUserRepos()
            
        }
        else if let _ = oAuthContext as? OAuthContextFoursquare {
            //OK callApi_FoursquareUser()
            callApi_FoursquareVenuesSearch()
            
            
        }else{
            self.log.error("self.OAuthContextGithub is nil")
        }
        
        
        
        
        
        //        todo - fix JSONExport app
        //
        //        use SFSafariController
        //
        //        foursquare
        //        https://github.com/search?l=Objective-C&p=2&q=foursquare&type=Repositories&utf8=%E2%9C%93
        //        yelp
        //        meetup
        //        do meetup api - meetup requires http referer not joyride://
        //
        //        eventbrite
        //        tripadvisor
        //        google calendar
        //        lanyrd
        //        eventjoy
        //
        //        http://www.mashery.com/blog/meeting-hotel-industry-challenges-apis-check
        //        expedia
        //        orbitz
        //        KAYAK
        //        hipmunk
    }
    
    
    func callApi_FoursquareUser(){
        
        //---------------------------------------------------------------------
        //not asynch
        //appDelegate.colcOAuth2Manager.delegate = self
        //---------------------------------------------------------------------
        appDelegate.colcOAuth2Manager.callApi_FoursquareUser(
            
            success:{ (foursquareUser: FoursquareUser)->Void in
                self.log.info("foursquareUser:\(foursquareUser.firstName) \(foursquareUser.lastName)")
                
                
            },
            failure:{ (error) -> Void in
                self.handleError(error)
            }
        )
    }
    func callApi_FoursquareVenuesSearch(){
        
        //---------------------------------------------------------------------
        //not asynch
        //appDelegate.colcOAuth2Manager.delegate = self
        //---------------------------------------------------------------------
        appDelegate.colcOAuth2Manager.callApi_FoursquareVenuesSearch(
            
            success:{ (foursquareVenuesSearch: FoursquareVenuesSearch)->Void in
                self.log.info("foursquareVenuesSearch:TODO DISPLAY RESULTS")
                
                
            },
            failure:{ (error) -> Void in
                self.handleError(error)
            }
        )
    }
    
    func callApi_GithubUserRepos(){
        //not asynch
        //appDelegate.colcOAuth2Manager.delegate = self
        
        
        appDelegate.colcOAuth2Manager.callApi_GithubUserRepos(
            
            success:{ (githubUserRepoArray: [GithubUserRepo])->Void in
                self.log.info("githubUserRepoArray returned:\(githubUserRepoArray.count)")
                for githubUserRepo in githubUserRepoArray{
                    self.log.info("REPO:\(githubUserRepo.name)")
                }
                
            },
            failure:{ (error) -> Void in
                self.handleError(error)
            }
        )
    }
    
    
    
    func accessTokenFailed(_ colcOAuth2Manager: COLCOAuth2Manager, error: Error){
        self.handleError(error)
    }
    
    func handleError(_ error: Error){
        var showErrorAlert = true
        // TODO: - SWIFT3 - this is done multiple times refactor so its only done once use AppError
        switch error{

        case AppError.googlePlacesResponseStatus_ZERO_RESULTS_DISTANCE:
            self.log.info("status: ZERO_RESULTS")
            //------------------------------------------------------------------------------------------------
            //DONT SHOW ERROR ALERT FOR ZERO RESULTS
            showErrorAlert = false

            //                        //remove all results
            //                        self.nearbySearch_CLKGooglePlaceResultsArray = [CLKGooglePlaceResult]()
            //                        self.tableViewPlacesResults.reloadData()
            //
            //                        //------------------------------------------------------------------------------------------------
            //                        self.labelAddress.text = ""
            //                        //------------------------------------------------------------------------------------------------

            //------------------------------------------------------------------------------------------------

        case AppError.googlePlacesResponseStatus_OVER_QUERY_LIMIT:
            self.log.info("status: OVER_QUERY_LIMIT")

        case AppError.googlePlacesResponseStatus_REQUEST_DENIED:
            self.log.info("status: REQUEST_DENIED")

        case AppError.googlePlacesResponseStatus_INVALID_REQUEST:

            self.log.info("status: INVALID_REQUEST")

        default:
            self.log.error("UNKNOWN error: \(error)")
        }
        
        if showErrorAlert{
            //if let parentViewController = self.parentViewController {
            CLKAlertController.showAlertInVCWithCallback(self, title: "Error", message: error.localizedDescription)
            { () -> Void in
                self.log.info("Ok tapped = alert closed appDelegate.doLogout()")
            }
            //}else{
            //    self.log.error("parentViewController is nil")
            //}
        }
    }
    
    
    
    
    
    
    
    
    func getUserEvents() {
        // TODO: - <#COMMENT#>
//        if FoursquareSDKAccessToken.currentAccessToken() != nil {
//            // TODO: -
//            //            if FoursquareSDKAccessToken.currentAccessToken().hasGranted("email") && FoursquareSDKAccessToken.currentAccessToken().hasGranted("public_profile") {
//            //                35
//            //                36	        }
//            
//            
//            //print(FoursquareSDKAccessToken.currentAccessToken())
//            
//            FoursquareSDKGraphRequest(graphPath: "me", parameters: ["fields": "events"]).startWithCompletionHandler({ (connection, result, error) -> Void in
//                if error == nil {
//                    //print("----------------------")
//                    print(result)
//                    //print("----------------------")
//                    print(result as! NSDictionary)
//                    
//                    if let resultNSDictionary = result as? NSDictionary {
//                        //print("resultNSDictionary:\(resultNSDictionary)")
//                        //print("resultNSDictionary.allKeys:\(resultNSDictionary.allKeys)")
//                        //resultNSDictionary.allKeys:[id, events]
//                        //print("----------------------")
//                        
//                        if let colcFoursquareEventSearchResult : COLCFoursquareEventSearchResult = Mapper<COLCFoursquareEventSearchResult>().map(JSONObject:resultNSDictionary){
//                            
//                            
//                            for (key, value) in colcFoursquareEventSearchResult.events {
//                                //print("key[\(key)]")
//                                //print("value[\(value)]")
//                                
//                                switch value{
//                                case let number as NSNumber:
//                                    print("NSNumber:\(number)")
//                                case let array as NSArray:
//                                    //print("NSArray:\(array)")
//                                    
//                                    for valueInArray in array {
//                                        
//                                        //print("valueInArray[\(valueInArray)]")
//                                        
//                                        switch valueInArray{
//                                        case let number as NSNumber:
//                                            print("NSNumber:\(number)")
//                                        case let array as NSArray:
//                                            print("NSArray:\(array)")
//                                            
//                                            
//                                        case let nsDictionary as NSDictionary:
//                                            //print("NSDictionary:\(nsDictionary)")
//                                            //print("nsDictionary.allKeys:\(nsDictionary.allKeys)")
//                                            //nsDictionary.allKeys:[end_time, id, start_time, rsvp_status, description, place, name]
//                                            //print("")
//                                            
//                                            if let colcFoursquareEvent : COLCFoursquareEvent = Mapper<COLCFoursquareEvent>().map(JSONObject:nsDictionary){
//                                                
//                                                self.arrayCOLCFoursquareEvent.append(colcFoursquareEvent)
//                                                
//                                                //print("colcFoursquareEvent:\(colcFoursquareEvent)")
//                                            }else{
//                                                self.log.error("clkPlaceSearchResponse is nil or not CLKGooglePlaceSearchResponse")
//                                            }
//                                        default:
//                                            print("UNKNOWN TYPE")
//                                        }
//                                        
//                                        //print("*****************")
//                                    }
//                                    
//                                case let nsDictionary as NSDictionary:
//                                    print("NSDictionary:\(nsDictionary)")
//                                default:
//                                    print("UNKNOWN TYPE")
//                                }
//                                
//                                //print("*****************")
//                            }
//                            
//                        }else{
//                            self.log.error("clkPlaceSearchResponse is nil or not CLKGooglePlaceSearchResponse")
//                        }
//                        
//                    }else{
//                        self.log.error("result as? NSDictionary is nil")
//                    }
//                    
//                    //print("----------------------")
//                    
//                    self.tableView.reloadData()
//                }else{
//                    self.log.error("FoursquareSDKGraphRequest FAILED:\(error)")
//                }
//            })
//        }else{
//            self.log.error("FoursquareSDKAccessToken.currentAccessToken() is nil")
//        }
    }
    
    
    func dumpCurrentAccessToken(){
        //        public var appID: String! { get }
        //
        //        /*!
        //        @abstract Returns the known declined permissions.
        //        */
        //        public var declinedPermissions: Set<NSObject>! { get }
        //
        //        /*!
        //        @abstract Returns the expiration date.
        //        */
        //        @NSCopying public var expirationDate: NSDate! { get }
        //
        //        /*!
        //        @abstract Returns the known granted permissions.
        //        */
        //        public var permissions: Set<NSObject>! { get }
        //
        //        /*!
        //        @abstract Returns the date the token was last refreshed.
        //        */
        //        @NSCopying public var refreshDate: NSDate! { get }
        //
        //        /*!
        //        @abstract Returns the opaque token string.
        //        */
        //        public var tokenString: String! { get }
        //
        //        /*!
        //        @abstract Returns the user ID.
        //        */
        //        public var userID: String! { get }
        //
        //
 // TODO: - <#COMMENT#>
//        if let currentAccessToken : FoursquareSDKAccessToken = FoursquareSDKAccessToken.currentAccessToken(){
//            var tokenValues = "\rcurrentAccessToken *******\r"
//            tokenValues = tokenValues + "tokenString   :[\(currentAccessToken.tokenString)]\r"
//            tokenValues = tokenValues + "userID        :[\(currentAccessToken.userID)]\r"
//            tokenValues = tokenValues + "appID         :[\(currentAccessToken.appID)]\r"
//            tokenValues = tokenValues + "refreshDate   :[\(currentAccessToken.refreshDate)][Date last refreshed]\r"
//            tokenValues = tokenValues + "expirationDate:[\(currentAccessToken.expirationDate)]\r"
//            tokenValues = tokenValues + "permissions   :\r[\(currentAccessToken.permissions)]\r"
//            tokenValues = tokenValues + "declinedPermissions   :\r[\(currentAccessToken.declinedPermissions)]\r"
//            tokenValues = tokenValues + "currentAccessToken *******\r"
//            self.log.info("\(tokenValues)")
//            
//        }else{
//            self.log.error("currentAccessToken is nil")
//        }
    }
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - TABLEVIEW
    //--------------------------------------------------------------
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat{
        return 170.0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return UITableViewAutomaticDimension
        //        return 170.0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCOLCFoursquareEvent.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cellIdentifier = "COLCFoursquareEventTableViewCell"
        
        let colcFoursquareEventTableViewCell : COLCFoursquareEventTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! COLCFoursquareEventTableViewCell
        
        colcFoursquareEventTableViewCell.applyCustomFont()
        //------------------------------------------------
//        let colcFoursquareEvent : COLCFoursquareEvent = arrayCOLCFoursquareEvent[indexPath.row]
        //------------------------------------------------
// TODO: - Foursquare
        //        colcFoursquareEventTableViewCell.labelId?.text = "..."
//        if let id = colcFoursquareEvent.id as? String{
//            colcFoursquareEventTableViewCell.labelId?.text = id
//            
//        }else{
//            self.log.error("colcFoursquareEvent.id as? String is nil")
//        }
//        //------------------------------------------------
//        colcFoursquareEventTableViewCell.labelName?.text = "..."
//        if let name = colcFoursquareEvent.name as? String{
//            colcFoursquareEventTableViewCell.labelName?.text = name
//            
//        }else{
//            self.log.error("colcFoursquareEvent.name as? String is nil")
//        }
//        //------------------------------------------------
//        colcFoursquareEventTableViewCell.labelPlace?.text = "..."
//        if let colcFoursquarePlace : COLCFoursquarePlace = colcFoursquareEvent.place{
//            if let name = colcFoursquarePlace.name as? String{
//                
//                if colcFoursquarePlace.formattedAddress == "" {
//                    colcFoursquareEventTableViewCell.labelPlace?.text = name
//                    
//                }else{
//                    colcFoursquareEventTableViewCell.labelPlace?.text = "\(name), \(colcFoursquarePlace.formattedAddress)"
//                }
//            }else{
//                self.log.error("colcFoursquarePlace.name is nil")
//            }
//        }else{
//            self.log.error("colcFoursquareEvent.place is nil")
//        }
//        //------------------------------------------------
//        if let rsvp_status = colcFoursquareEvent.rsvp_status as? String{
//            colcFoursquareEventTableViewCell.labelRSVPStatus?.text = rsvp_status
//            
//        }else{
//            self.log.error("colcFoursquareEvent.rsvp_status as? String is nil")
//            colcFoursquareEventTableViewCell.labelRSVPStatus?.text = "..."
//        }
//        //------------------------------------------------
//        if let start_time = colcFoursquareEvent.start_time as? String{
//            
//            //"2016-05-03T00:00:00"
//            if let nsDate =  UTCStringToNSDate(start_time) {
//                if let dateString =  dateStringInDisplayFormat(nsDate) {
//                    colcFoursquareEventTableViewCell.labelStartTime?.text = dateString as String
//                }else{
//                    colcFoursquareEventTableViewCell.labelStartTime?.text = ""
//                }
//            }else{
//                colcFoursquareEventTableViewCell.labelStartTime?.text = ""
//            }
//            
//        }else{
//            self.log.error("colcFoursquareEvent.start_time as? String is nil")
//            colcFoursquareEventTableViewCell.labelStartTime?.text = "..."
//        }
//        //------------------------------------------------
//        if let end_time = colcFoursquareEvent.end_time as? String{
//            
//            //"2016-05-03T00:00:00"
//            if let nsDate =  UTCStringToNSDate(end_time) {
//                if let dateString =  dateStringInDisplayFormat(nsDate) {
//                    colcFoursquareEventTableViewCell.labelEndTime?.text = dateString as String
//                }else{
//                    colcFoursquareEventTableViewCell.labelEndTime?.text = ""
//                }
//            }else{
//                colcFoursquareEventTableViewCell.labelEndTime?.text = ""
//            }
//            
//        }else{
//            //not always set self.log.error("colcFoursquareEvent.end_time as? String is nil")
//            colcFoursquareEventTableViewCell.labelEndTime?.text = "..."
//        }
//        //------------------------------------------------
        colcFoursquareEventTableViewCell.labelDescription?.text = "..."
        return colcFoursquareEventTableViewCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let colcFoursquareEvent : COLCFoursquareEvent = arrayCOLCFoursquareEvent[(indexPath as NSIndexPath).row]
        //print("\(colcFoursquareEvent.name)")
        
        if let delegate = self.delegate {
            
            
            delegate.controllerDidSelectCOLCFoursquareEvent(self, colcFoursquareEvent: colcFoursquareEvent)
            
        }else{
            self.log.error("self.delegate is nil")
        }
        
    }
    
    
    //--------------------------------------------------------------
    // MARK: - FACEBOOK DATE
    // MARK: -
    //--------------------------------------------------------------
    
    func UTCStringToNSDate(_ utcDateString: String) -> Date?{
        
        var dateReturned : Date? = nil
        
        let dateFormatterIN = DateFormatter()
        //"2016-06-13T19:45:00+0100"
        //dateFormatterIN.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        //dateFormatterIN.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZ"
        dateFormatterIN.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZ"
        
        //dateFormatterIN.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
        let dateInUserTimeZone = dateFormatterIN.date(from: utcDateString)
        
        if let dateInUserTimeZone = dateInUserTimeZone{
            ////print("dateInUserTimeZone:[\(dateInUserTimeZone)")
            dateReturned = dateInUserTimeZone
        }else{
            self.log.error("dateFromStringUTC is nil")
        }
        
        return dateReturned
    }
    
    
    func dateStringInDisplayFormat(_ dateIN: Date) -> NSString?{
        
        var dateStringReturned : NSString? = nil
        
        let dateFormatterIN = DateFormatter()
        
        //"11 Feb 2016"
        dateFormatterIN.dateFormat = "dd MMM yyyy HH:mm"
        
        // TODO: - Fixtures do we need to support timeZone
        //dateFormatterIN.timeZone = self.userTimeZone()
        dateStringReturned = dateFormatterIN.string(from: dateIN) as NSString?
        
        return dateStringReturned
        
    }
    
    @IBAction func buttonCancel_Action(_ sender: AnyObject) {
        
        //self.navigationController?.popViewControllerAnimated(true)
        
        self.dismiss(animated: true, completion: {
            
            //------------------------------------------------
            if let delegate_ = self.delegate {
                //containerView is hidden by the delegate
                delegate_.foursquareViewControllerCancelled(self)
            }else{
                self.log.error("self.delegate is nil")
            }
            //------------------------------------------------
        })
    }
    
}


