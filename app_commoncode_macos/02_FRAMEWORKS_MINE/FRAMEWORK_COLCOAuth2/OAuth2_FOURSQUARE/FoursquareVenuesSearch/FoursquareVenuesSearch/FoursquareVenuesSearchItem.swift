//
//	FoursquareVenuesSearchItem.swift
//
//	Create by Brian Clear on 19/7/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class FoursquareVenuesSearchItem : ParentMappable{

	var unreadCount : Int?



	override func mapping(map: Map)
	{
		unreadCount <- map["unreadCount"]
		
	}
	var description: String
	{
		var description_ = "**** \(type(of: self)) *****\r"
		description_ = description_ + "unreadCount: \(unreadCount)\r"
		return description_
	}

}
