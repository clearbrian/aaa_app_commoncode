//
//	FoursquareVenuesSearchMenu.swift
//
//	Create by Brian Clear on 19/7/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class FoursquareVenuesSearchMenu : ParentMappable{

	var anchor : String?
	var label : String?
	var mobileUrl : String?
	var type : String?
	var url : String?



	override func mapping(map: Map)
	{
		anchor <- map["anchor"]
		label <- map["label"]
		mobileUrl <- map["mobileUrl"]
		type <- map["type"]
		url <- map["url"]
		
	}
	var description: String
	{
		var description_ = "**** \(type(of: self)) *****\r"
		description_ = description_ + "anchor: \(anchor)\r"
		description_ = description_ + "label: \(label)\r"
		description_ = description_ + "mobileUrl: \(mobileUrl)\r"
		description_ = description_ + "type: \(type)\r"
		description_ = description_ + "url: \(url)\r"
		return description_
	}

}
