//
//	FoursquareVenuesSearchStat.swift
//
//	Create by Brian Clear on 19/7/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class FoursquareVenuesSearchStat : ParentMappable{

	var checkinsCount : Int?
	var tipCount : Int?
	var usersCount : Int?



	override func mapping(map: Map)
	{
		checkinsCount <- map["checkinsCount"]
		tipCount <- map["tipCount"]
		usersCount <- map["usersCount"]
		
	}
	var description: String
	{
		var description_ = "**** \(type(of: self)) *****\r"
		description_ = description_ + "checkinsCount: \(checkinsCount)\r"
		description_ = description_ + "tipCount: \(tipCount)\r"
		description_ = description_ + "usersCount: \(usersCount)\r"
		return description_
	}

}
