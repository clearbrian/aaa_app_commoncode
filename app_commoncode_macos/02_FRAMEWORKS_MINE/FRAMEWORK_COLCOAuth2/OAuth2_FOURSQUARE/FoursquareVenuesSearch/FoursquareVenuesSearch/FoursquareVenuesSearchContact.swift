//
//	FoursquareVenuesSearchContact.swift
//
//	Create by Brian Clear on 19/7/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class FoursquareVenuesSearchContact : ParentMappable{




	override func mapping(map: Map)
	{
		
	}
	var description: String
	{
		let description_ = "**** \(type(of: self)) *****\r"
		return description_
	}

}
