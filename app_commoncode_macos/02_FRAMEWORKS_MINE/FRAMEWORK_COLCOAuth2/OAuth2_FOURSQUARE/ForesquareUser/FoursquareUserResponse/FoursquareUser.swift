//
//	FoursquareUser.swift
//
//	Create by Brian Clear on 8/7/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class FoursquareUser : ParentMappable{

	var bio : String?
	var birthday : Int?
	var blockedStatus : String?
	var canonicalUrl : String?
	var checkinPings : String?
	var checkins : FoursquareCheckin?
	var contact : FoursquareContact?
	var createdAt : Int?
	var firstName : String?
	var friends : FoursquareFriend?
	var gender : String?
	var homeCity : String?
	var id : String?
	var lastName : String?
	var lenses : [String]?
	var lists : FoursquareFriend?
	var mayorships : FoursquarePhoto?
	var photo : FoursquarePhoto?
	var photos : FoursquarePhoto?
	var pings : Bool?
	var referralId : String?
	var relationship : String?
	var requests : FoursquareComment?
	var tips : FoursquareComment?
	var type : String?



	override func mapping(map: Map)
	{
		bio <- map["bio"]
		birthday <- map["birthday"]
		blockedStatus <- map["blockedStatus"]
		canonicalUrl <- map["canonicalUrl"]
		checkinPings <- map["checkinPings"]
		checkins <- map["checkins"]
		contact <- map["contact"]
		createdAt <- map["createdAt"]
		firstName <- map["firstName"]
		friends <- map["friends"]
		gender <- map["gender"]
		homeCity <- map["homeCity"]
		id <- map["id"]
		lastName <- map["lastName"]
		lenses <- map["lenses"]
		lists <- map["lists"]
		mayorships <- map["mayorships"]
		photo <- map["photo"]
		photos <- map["photos"]
		pings <- map["pings"]
		referralId <- map["referralId"]
		relationship <- map["relationship"]
		requests <- map["requests"]
		tips <- map["tips"]
		type <- map["type"]
		
	}
	var description: String
	{
		var description_ = "**** \(type(of: self)) *****\r"
		description_ = description_ + "bio: \(bio)\r"
		description_ = description_ + "birthday: \(birthday)\r"
		description_ = description_ + "blockedStatus: \(blockedStatus)\r"
		description_ = description_ + "canonicalUrl: \(canonicalUrl)\r"
		description_ = description_ + "checkinPings: \(checkinPings)\r"
		description_ = description_ + "checkins: \(checkins)\r"
		description_ = description_ + "contact: \(contact)\r"
		description_ = description_ + "createdAt: \(createdAt)\r"
		description_ = description_ + "firstName: \(firstName)\r"
		description_ = description_ + "friends: \(friends)\r"
		description_ = description_ + "gender: \(gender)\r"
		description_ = description_ + "homeCity: \(homeCity)\r"
		description_ = description_ + "id: \(id)\r"
		description_ = description_ + "lastName: \(lastName)\r"
		description_ = description_ + "lenses: \(lenses)\r"
		description_ = description_ + "lists: \(lists)\r"
		description_ = description_ + "mayorships: \(mayorships)\r"
		description_ = description_ + "photo: \(photo)\r"
		description_ = description_ + "photos: \(photos)\r"
		description_ = description_ + "pings: \(pings)\r"
		description_ = description_ + "referralId: \(referralId)\r"
		description_ = description_ + "relationship: \(relationship)\r"
		description_ = description_ + "requests: \(requests)\r"
		description_ = description_ + "tips: \(tips)\r"
		description_ = description_ + "type: \(type)\r"
		return description_
	}

}
