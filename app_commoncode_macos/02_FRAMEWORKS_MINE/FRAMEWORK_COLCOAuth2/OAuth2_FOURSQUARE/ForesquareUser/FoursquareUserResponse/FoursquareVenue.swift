//
//	FoursquareVenue.swift
//
//	Create by Brian Clear on 8/7/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class FoursquareVenue : ParentMappable{

	var categories : [FoursquareCategory]?
	var contact : FoursquareContact?
	var id : String?
	var like : Bool?
	var location : FoursquareVenuesSearchLocation?
	var name : String?
	var stats : FoursquareStat?
	var storeId : String?
	var verified : Bool?



	override func mapping(map: Map)
	{
		categories <- map["categories"]
		contact <- map["contact"]
		id <- map["id"]
		like <- map["like"]
		location <- map["location"]
		name <- map["name"]
		stats <- map["stats"]
		storeId <- map["storeId"]
		verified <- map["verified"]
		
	}
	var description: String
	{
		var description_ = "**** \(type(of: self)) *****\r"
		description_ = description_ + "categories: \(categories)\r"
		description_ = description_ + "contact: \(contact)\r"
		description_ = description_ + "id: \(id)\r"
		description_ = description_ + "like: \(like)\r"
		description_ = description_ + "location: \(location)\r"
		description_ = description_ + "name: \(name)\r"
		description_ = description_ + "stats: \(stats)\r"
		description_ = description_ + "storeId: \(storeId)\r"
		description_ = description_ + "verified: \(verified)\r"
		return description_
	}

}
