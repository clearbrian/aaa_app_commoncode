//
//	FoursquareComment.swift
//
//	Create by Brian Clear on 8/7/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class FoursquareComment : ParentMappable{

	var count : Int?



	override func mapping(map: Map)
	{
		count <- map["count"]
		
	}
	var description: String
	{
		var description_ = "**** \(type(of: self)) *****\r"
		description_ = description_ + "count: \(count)\r"
		return description_
	}

}
