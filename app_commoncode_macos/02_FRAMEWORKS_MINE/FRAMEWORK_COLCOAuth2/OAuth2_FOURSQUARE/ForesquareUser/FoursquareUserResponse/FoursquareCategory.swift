//
//	FoursquareCategory.swift
//
//	Create by Brian Clear on 8/7/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class FoursquareCategory : ParentMappable{

	var icon : FoursquareIcon?
	var id : String?
	var name : String?
	var pluralName : String?
	var primary : Bool?
	var shortName : String?



	override func mapping(map: Map)
	{
		icon <- map["icon"]
		id <- map["id"]
		name <- map["name"]
		pluralName <- map["pluralName"]
		primary <- map["primary"]
		shortName <- map["shortName"]
		
	}
	var description: String
	{
		var description_ = "**** \(type(of: self)) *****\r"
		description_ = description_ + "icon: \(icon)\r"
		description_ = description_ + "id: \(id)\r"
		description_ = description_ + "name: \(name)\r"
		description_ = description_ + "pluralName: \(pluralName)\r"
		description_ = description_ + "primary: \(primary)\r"
		description_ = description_ + "shortName: \(shortName)\r"
		return description_
	}

}
