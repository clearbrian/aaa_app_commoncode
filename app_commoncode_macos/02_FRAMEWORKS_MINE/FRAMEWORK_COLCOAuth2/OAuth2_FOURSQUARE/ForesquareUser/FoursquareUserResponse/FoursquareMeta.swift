//
//	FoursquareMeta.swift
//
//	Create by Brian Clear on 8/7/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class FoursquareMeta : ParentMappable{

	var code : Int?
	var requestId : String?



	override func mapping(map: Map)
	{
		code <- map["code"]
		requestId <- map["requestId"]
		
	}
	var description: String
	{
		var description_ = "**** \(type(of: self)) *****\r"
		description_ = description_ + "code: \(code)\r"
		description_ = description_ + "requestId: \(requestId)\r"
		return description_
	}

}
