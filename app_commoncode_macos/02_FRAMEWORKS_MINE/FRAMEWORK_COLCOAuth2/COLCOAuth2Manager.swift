//
//  COLCOAuth2Manager.swift
//  colcoauth2
//
//  Created by Brian Clear on 05/07/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit
import SafariServices

enum OAuth2API : String{
    case Github = ".Github"
    case Foursquare = ".Foursquare"
}

protocol COLCOAuth2ManagerDelegate {
    func accessTokenReceivedOK(_ colcOAuth2Manager: COLCOAuth2Manager, access_token: String, oAuthContext: OAuthContext)
    func accessTokenFailed(_ colcOAuth2Manager: COLCOAuth2Manager, error: Error)
}

//SFSafariViewControllerDelegate requries NSObject so ParentNSObject
class COLCOAuth2Manager : ParentNSObject, SFSafariViewControllerDelegate {
    
    var delegate: COLCOAuth2ManagerDelegate?

    let colcWebServicesManager = COLCWebServicesManager()
    
    //---------------------------------------------------------------------
    //Info,plist - used to reopen app after authorize
    static let kAppScheme = "joyrider"
    //---------------------------------------------------------------------
    
    
    var dictionaryOAuth2API = [OAuth2API : OAuthContext]()
    
    
    //---------------------------------------------------------------------
    //https://developer.github.com/v3/oauth/#web-application-flow
    //---------------------------------------------------------------------
    //redirect_uri/client_id set up here
    //https://github.com/settings/applications/380231
    //--------------------------------------------------------------
    //GITHUB authorize OAuth2
    //"https://github.com/login/oauth/authorize?client_id=be45feb27af1729e6312&redirect_uri=joyrider://oauth/callback_github&scope=user&state=8E7F9061&allow_signup=false"
   
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //GITHUB
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    let oauthContextGithub = OAuthContextGithub(oauth_redirect_uri : "joyrider://oauth/callback_github",          /*     OAuth2 : redirect_uri         */
                                                   oauth_client_id : "be45feb27af1729e6312",                      /*     OAuth2 : Client id of the app */
                                                oauth_client_secret: "bbf4bde86a4a3fa193b821cc4a6c610079c306d2",  /*     OAuth2 : Client secret        */
                                                      github_scope : "user",                                      /* GITHUB API : how much access app requires to github api */
                                               github_allow_signup : "false"                                      /* GITHUB API : allow_signup; user has to log in to get token - if true then they can also sign up */
    )
    
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //FOURSQUARE
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //https://developer.foursquare.com/overview/auth.html
    
    let oAuthContextFoursquare = OAuthContextFoursquare(oauth_redirect_uri : "joyrider://oauth/callback_foursquare",              /* OAuth2 : redirect_uri         */
                                                           oauth_client_id : "E3HMBZNK40ZQC1WMWGJTCES2MAIA1ALI51WLTVUSL3CTHCIL",  /* OAuth2 : Client id of the app */
                                                       oauth_client_secret : "N454WAYJNAPCJHPHFFLYPXQV0YFPU0VIVSFY0MR3YH1JK0K2"   /* OAuth2 : Client secret        */
     )
    
    //--------------------------------------------------------------
    // MARK: - init
    // MARK: -
    //--------------------------------------------------------------
    override init(){
        super.init()
        
        dictionaryOAuth2API[.Github] = self.oauthContextGithub
        dictionaryOAuth2API[.Foursquare] = self.oAuthContextFoursquare
        
    }
    
    func accessTokenForOAuth2API(_ oauth2API: OAuth2API) -> String?{
        var access_token_ : String? = nil
        
        if let oauthContext: OAuthContext = oauthContextForOAuth2API(oauth2API) {
            
            if let access_token = oauthContext.access_token {
                access_token_ = access_token
                
            }else{
                self.log.error("oauthContext.access_token is nil")
            }
        }else{
            self.log.error("dictionaryOAuth2API[oauth2API] is nil")
        }
        return access_token_
    }
    
    
    func oauthContextForOAuth2API(_ oauth2API: OAuth2API) -> OAuthContext?{
        var oauthContext_ : OAuthContext? = nil
        
        if let oauthContext: OAuthContext = dictionaryOAuth2API[oauth2API] {
            
            oauthContext_ = oauthContext
            
        }else{
            self.log.error("dictionaryOAuth2API[oauth2API] is nil")
        }
        return oauthContext_
    }
    
    func oauthContextFor_redirect_uri(_ oauth_redirect_uri: String) -> OAuthContext?{
        var oauthContext_: OAuthContext?
        
        
        //---------------------------------------------------------------------
        //GITHUB
        //---------------------------------------------------------------------
        if let oauthContextGithub_oauth_redirect_uri = self.oauthContextGithub.oauth_redirect_uri {
            
            if oauth_redirect_uri.hasPrefix(oauthContextGithub_oauth_redirect_uri){
                oauthContext_ = self.oauthContextGithub
                
            }else{
                //no match try next
            }
        }else{
            //self.oauthContextGithub.oauth_redirect_uri is nil try next
        }
        
        //---------------------------------------------------------------------
        //Foursquare
        //---------------------------------------------------------------------
        if let oauthContextFoursquare_oauth_redirect_uri = self.oAuthContextFoursquare.oauth_redirect_uri {
            
            if oauth_redirect_uri.hasPrefix(oauthContextFoursquare_oauth_redirect_uri){
                oauthContext_ = self.oAuthContextFoursquare
                
            }else{
                //no match try next
            }
        }else{
            //self.oauthContextGithub.oauth_redirect_uri is nil try next
        }
        return oauthContext_
    }
    
    
    //--------------------------------------------------------------
    static func isOAuthAppScheme(_ url: URL) -> Bool{
        if url.scheme == kAppScheme{
            return true
        }else{
            return false
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - STEP 1 - AUTHORIZE - User log in and grants authorization
    // MARK: -
    //--------------------------------------------------------------
    //GET https://github.com/login/oauth/authorize
    //private var urlStringAuthorize:String = "https://github.com/login/oauth/authorize?client_id=be45feb27af1729e6312&redirect_uri=joyrider://oauth/callback_github&scope=user&state=8E7F9061&allow_signup=false"
    
    //---------------------------------------------------------------------
    //Must add joyrider:// to Info.plist
    //CFBundleDisplayName shoudl be set so Open 'Joyrider' instead of  Open 'joyrider'
    //---------------------------------------------------------------------
    /*
    <key>CFBundleURLTypes</key>
    <array>
    <dict>
        <key>CFBundleURLSchemes</key>
        <array>
        <string>joyrider</string>
        </array>
    </dict>
    </array>
    <key>CFBundleDisplayName</key>
    <string>Joyrider</string>
     */
    
    //--------------------------------------------------------------
    // MARK: - STEP 1a - AUTHORIZE using external Safari
    // MARK: -
    //--------------------------------------------------------------
    func authorizeWithExternalSafari(_ oauth2API: OAuth2API){
        
        if let oauthContext = appDelegate.colcOAuth2Manager.oauthContextForOAuth2API(oauth2API) {
            //---------------------------------------------------------------------
            //return nil if any params missing isValid() false
            if let urlStringAuthorize = oauthContext.urlStringAuthorize {
                //------------------------------------------------
                self.log.info("AUTHORIZE:\(urlStringAuthorize)")
                if let urlAuthorize = URL(string: urlStringAuthorize) {
                    
                    UIApplication.shared.openURL(urlAuthorize)
                    //First time - will open Safari as another app
                    //go to /authorize url
                    //User may cancel or login and grant access
                    //Website will try and open redirect_url
                    //joyrider://oauth/callback_github?code=fcb5061a442f8d584193&state=8E7F9061
                    //so this code will return to the app via
                    ////This will trigger AppDelegate...openURL
                    //which will come back into this class in
                    //handleOauth2CallbackURL(..)
                    
                }else{
                    self.log.error("urlAuthorize is not valid URL")
                }
                //------------------------------------------------
            }else{
                self.log.error("self.oauthContextGITHUB.urlStringAuthorize is nil")
            }
        }else{
            self.log.error("appDelegate.colcOAuth2Manager.oauthContextForOAuth2API(\(oauth2API.rawValue)) is nil")
        }

    }
    
    //--------------------------------------------------------------
    // MARK: - STEP 1b - AUTHORIZE using Embedded SafariViewController
    // MARK: -
    //--------------------------------------------------------------

    var sfSafariViewController : SFSafariViewController?
    func authorizeWithSafariInViewController(_ presentingViewController: UIViewController, oauth2API: OAuth2API){
        

        if let oauthContext = appDelegate.colcOAuth2Manager.oauthContextForOAuth2API(oauth2API) {
            //---------------------------------------------------------------------
            //return nil if any params missing isValid() false
            if let urlStringAuthorize = oauthContext.urlStringAuthorize {
                //------------------------------------------------
                self.log.info("AUTHORIZE:\(urlStringAuthorize)")
                if let urlAuthorize = URL(string: urlStringAuthorize) {
                    
                    //UIApplication.sharedApplication().openURL(urlAuthorize)
                    
                    self.sfSafariViewController = SFSafariViewController(url: urlAuthorize)
                    if let sfSafariViewController = self.sfSafariViewController {
                        sfSafariViewController.delegate = self
                        presentingViewController.present(sfSafariViewController, animated: true, completion: nil)
                        
                        //First time - will open Safari as embedded SFSafariViewController in this app
                        //SFSafariViewController will go to /authorize url
                        //User may cancel or login and grant access
                        //Website will try and open redirect_url
                        //joyrider://oauth/callback_github?code=fcb5061a442f8d584193&state=8E7F9061
                        //This acts the same way as external app calling joyride://
                        //it will be passed to this app
                        //and will trigger AppDelegate...openURL
                        //which will come back into this class in
                        //handleOauth2CallbackURL(..)
                        
                    }else{
                        self.log.error("self.sfSafariViewController is nil")
                    }
                }else{
                    self.log.error("urlAuthorize is not valid URL")
                }
                //------------------------------------------------
            }else{
                self.log.error("self.oauthContextGITHUB.urlStringAuthorize is nil")
            }
        }else{
            self.log.error("appDelegate.colcOAuth2Manager.oauthContextForOAuth2API(\(oauth2API.rawValue)) is nil")
        }
        
        
        
    }
    
    //func safariViewController(controller: SFSafariViewController, activityItemsForURL URL: NSURL, title: String?) -> [UIActivity]{
    //
    //}
    
    /*! @abstract Delegate callback called when the user taps the Done button. Upon this call, the view controller is dismissed modally. */
    func safariViewControllerDidFinish(_ controller: SFSafariViewController){
        self.log.info("safariViewControllerDidFinish")
        
        //do nothing
        //Website will try and open redirect_url
        //joyrider://oauth/callback_github?code=fcb5061a442f8d584193&state=8E7F9061
        //This acts the same way as external app calling joyride://
        //it will be passed to this app
        //and will trigger AppDelegate...openURL
        //which will come back into this class in
        //handleOauth2CallbackURL(..)
        //this will dismiss the SFSafariViewController if its visible
    }
    
    /*! @abstract Invoked when the initial URL load is complete.
     @param success YES if loading completed successfully, NO if loading failed.
     @discussion This method is invoked when SFSafariViewController completes the loading of the URL that you pass
     to its initializer. It is not invoked for any subsequent page loads in the same SFSafariViewController instance.
     */
    func safariViewController(_ controller: SFSafariViewController, didCompleteInitialLoad didLoadSuccessfully: Bool){
        self.log.error("didLoadSuccessfully")
        //do nothing
        //Website will try and open redirect_url
        //joyrider://oauth/callback_github?code=fcb5061a442f8d584193&state=8E7F9061
        //This acts the same way as external app calling joyride://
        //it will be passed to this app
        //and will trigger AppDelegate...openURL
        //which will come back into this class in
        //handleOauth2CallbackURL(..)
        //this will dismiss the SFSafariViewController if its visible
    }
    

    //--------------------------------------------------------------
    // MARK: - STEP 2 - APP REQUESTS ACCESS TOKEN
    // MARK: -
    //--------------------------------------------------------------
    //User has logged into website we want access to and granted access.
    //Website redirect to webpage using redirect_url
    //joyrider://oauth/callback_github?code=cbdecb65c641c9c7523b&state=8E7F9061
    //Which reopens this app  AppDelegate.openURL
    //which calls this method
    //---------------------------------------------------------------------
    func handleOauth2CallbackURL(_ url: URL){

        //------------------------------------------------
        //Check app opened with URLScheme joyride://
        //------------------------------------------------
        if COLCOAuth2Manager.isOAuthAppScheme(url){
            //------------------------------------------------
            //If app authorized with embedded SFSafariViewController then it will be still open so close it
            //------------------------------------------------
            if let _ = self.sfSafariViewController {
                self.sfSafariViewController?.dismiss(animated: true, completion: {
                    self.handleOauth2CallbackURL_requestAccessToken(url)
                })
            }else{
                //Used external Safari - nothing to close
                
                self.handleOauth2CallbackURL_requestAccessToken(url)
            }
            
        }else{
            self.log.error("App opened with unexpected url [didnt start with \(url.scheme)]:\(url)")
        }
    }
    
    func handleOauth2CallbackURL_requestAccessToken(_ url: URL){
        
        //------------------------------
        //DEBUG print(url.dumpProperties())
        //------------------------------
        
        /*
         ---------------------------------------
         joyrider://oauth/callback_github?code=cbdecb65c641c9c7523b&state=8E7F9061
         ---------------------------------------
         absoluteString:joyrider://oauth/callback_github?code=cbdecb65c641c9c7523b&state=8E7F9061
         absoluteURL:joyrider://oauth/callback_github?code=cbdecb65c641c9c7523b&state=8E7F9061
         baseURL:nil
         fileSystemRepresentation:0x00007f954c804000
         fragment:
         host:oauth
         lastPathComponent:callback
         parameterString:
         password:
         path:/callback
         pathComponents:Optional(["/", "callback"])
         pathExtension:
         port:nil
         query:code=cbdecb65c641c9c7523b&state=8E7F9061
         relativePath:/callback
         relativeString:joyrider://oauth/callback_github?code=cbdecb65c641c9c7523b&state=8E7F9061
         resourceSpecifier://oauth/callback_github?code=cbdecb65c641c9c7523b&state=8E7F9061
         scheme:colcoauth2
         standardizedURL:Optional(joyrider://oauth/callback_github?code=cbdecb65c641c9c7523b&state=8E7F9061)
         user:
         ------------------------------------------------ */
        
        //------------------------------------------------
        //Check app opened with URLScheme joyride://
        //------------------------------------------------
        if COLCOAuth2Manager.isOAuthAppScheme(url){
            
            
            if let nsURLComponents = URLComponents(url: url, resolvingAgainstBaseURL: true){
                //------------------------------------------------
                if let compQuery = nsURLComponents.query , compQuery.characters.count > 0 {
                    if let percentEncodedQuery = nsURLComponents.percentEncodedQuery {
                        //------------------------------------------------
                        let query = OAuth2Utils.paramsFromQuery(percentEncodedQuery)
                        
                        //------------------------------------------------
                        //joyrider://oauth/callback_github?code=cbdecb65c641c9c7523b&state=8E7F9061
                        //------------------------------------------------
                        
                        let urlString = "\(url)"
                        if let oauthContext : OAuthContext = oauthContextFor_redirect_uri(urlString){
                            
                            
                            if oauthContext.stateIsRequired{
                                //---------------------------------------------------------------------
                                //Check state
                                //---------------------------------------------------------------------
                                //state=<GUID>
                                //must be in callback and must match
                                //---------------------------------------------------------------------
                                if let state = query["state"] {
                        
                                    self.log.info("state:\(state)")
                                    
                                    if state == oauthContext.state{
                                        
                                        if let code = query["code"] {
                                            
                                            //---------------------------------------------------------------------
                                            //STEP 1 - succesful - User has logged in and granted access
                                            //---------------------------------------------------------------------
                                            self.log.info("AUTHORIZATION GRANTED: code:\(code) >> requesting access_token..")
                                            
                                            oauthContext.oauth_authorize_code = code
                                            
                                            //---------------------------------------------------------------------
                                            //OAUTH STEP 2 - USING code= retrieved in /authorize call /request access token
                                            //---------------------------------------------------------------------
                                            self.requestAccessToken(oauthContext)
                                            
                                        }
                                    }else{
                                        self.log.error("state[\(state)] == oauthContext.github_state[\(oauthContext.state)] FAILED ")
                                    }
                                }else{
                                    self.log.error("query['state'] is nil")
                                }
                            }else{
                                //---------------------------------------------------------------------
                                //state= is not required - only check for code
                                //---------------------------------------------------------------------
                                if let code = query["code"] {
                                    
                                    //---------------------------------------------------------------------
                                    //STEP 1 - succesful - User has logged in and granted access
                                    //---------------------------------------------------------------------
                                    self.log.info("AUTHORIZATION GRANTED: code:\(code) >> requesting access_token..")
                                    
                                    oauthContext.oauth_authorize_code = code
                                    
                                    //---------------------------------------------------------------------
                                    //OAUTH STEP 2 - USING code= retrieved in /authorize call /request access token
                                    //---------------------------------------------------------------------
                                    self.requestAccessToken(oauthContext)
                                    
                                }
                            }

                            //---------------------------------------------------------------------
                        }else{
                            self.log.error("oauthContext = oauthContextFor_redirect_uri(urlString)")
                        }

                    }else{
                        self.log.error("nsURLComponents.percentEncodedQuery is nil")
                    }
                }else{
                    self.log.error("where compQuery.characters.count > 0 is nil")
                }
                //------------------------------------------------
            }else{
                self.log.error("NSURLComponents(URL: url ) FAILED")
            }
        }else{
            self.log.error("App opened with unexpected url [didnt start with \(url.scheme)]:\(url)")
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - STEP 2 - User has logged in and granted access - now request a token
    // MARK: -
    //--------------------------------------------------------------

    func requestAccessToken(_ oAuthContext: OAuthContext){
        //---------------------------------------------------------------------
        //POST https://github.com/login/oauth/access_token
        //GET https://foursquare.com/oauth2/access_token
        //---------------------------------------------------------------------
        //OAuth2AccessTokenGithubRequest
        //OAuth2AccessTokenFoursquareRequest
        if let oauth2ParentWSRequest: OAuth2ParentWSRequest = oAuthContext.buildAccessTokenRequest() {
            //---------------------------------------------------------------------
            colcWebServicesManager.callOAuthWS(oauth2ParentWSRequest,
                                          parsedResponse:{ (parsedObject: Any, statusCode: Int) -> Void in

                                                oAuthContext.parseAccessTokenResponse(parsedObject,
                                                    success:{
                                                        (access_token: String) -> Void in
                                                        
                                                        //---------------------------------------------------------------------
                                                        self.log.info("[access_token] access_token:\(access_token)")
                                                        //---------------------------------------------------------------------
                                                        
                                                        if let delegate = self.delegate{
                                                            //------------------------------------------------
                                                            delegate.accessTokenReceivedOK(self, access_token: access_token, oAuthContext: oAuthContext)
                                                            //------------------------------------------------
                                                            
                                                        }else{
                                                            self.log.error("delegate is nil")
                                                        }
                                                        //---------------------------------------------------------------------
                                                    },
                                                    failure:{(error: Error) -> Void in
                                                        self.returnNSErrorToDelegate(error)
                                                        
                                                    }
                                                )

                },
                                          failure:{ (error) -> Void in
                                            //NO WS ERROR but may be status errors see success:
                                            self.log.error("error:\(error)")
                }
            )
            //---------------------------------------------------------------------
        }else{
            self.log.error("oAuthContext.buildAccessTokenRequest() is nil")
        }
    }
    //--------------------------------------------------------------
    // MARK: - CALL COLCOAuth2ManagerDelegate
    // MARK: -
    //--------------------------------------------------------------
    func returnNSErrorToDelegate(_ error: Error){
        
        if let delegate = self.delegate{
            
            //------------------------------------------------
            delegate.accessTokenFailed(self, error: error)
            //------------------------------------------------
            
        }else{
            self.log.error("delegate is nil")
        }
    }
    
    //---------------------------------------------------------------------
    /*
     <key>CFBundleURLTypes</key>
     <array>
     <dict>
     <key>CFBundleURLSchemes</key>
     <array>
     <string>colcoauth2</string>
     </array>
     </dict>
     </array>
     <key>CFBundleDisplayName</key>
     <string>My Auth App</string>
     */
    //---------------------------------------------------------------------
    //UNUSED
    //I was going to check url joyrider:// against in in Info.plist but  order isnt guaranteed in plist
    func checkURLScheme(){
        //[[NSBundle mainBundle] objectForInfoDictionaryKey:key_name];
        if let arrayOfSchemeDictionaries: NSArray = Bundle.main.object(forInfoDictionaryKey: "CFBundleURLTypes") as? NSArray{
            //------------------------------------------------
            print("arrayOfSchemeDictionaries:\(arrayOfSchemeDictionaries.count)")
            
            if arrayOfSchemeDictionaries.count > 0 {
                if let firstSchemeDictionary: NSDictionary = arrayOfSchemeDictionaries.object(at: 0) as? NSDictionary{
                    print("firstSchemeDictionary:\(firstSchemeDictionary)")
                    
                    let bundleURLSchemesArray = firstSchemeDictionary["CFBundleURLSchemes"]
                    print("bundleURLSchemesArray:\(bundleURLSchemesArray)")
                    
                }else{
                    self.log.error(" CFBundleURLTypes not NSArray")
                    
                }
            }else{
                self.log.error(" arrayOfSchemeDictionaries count is 0")
            }
            
            //------------------------------------------------
        }else{
            self.log.error("CFBundleURLTypes not NSArray")
        }
        //---------------------------------------------------------------------
        
    }
    
    //---------------------------------------------------------------------
    //used by Extensions
    //---------------------------------------------------------------------
    func callOAuthWS(_ oauth2ParentWSRequest: OAuth2ParentWSRequest,
                      responseNSDictionary: @escaping (_ nsDictionary: NSDictionary, _ statusCode: Int) -> Void,
                           responseNSArray: @escaping (_ nsArray: NSArray, _ statusCode: Int) -> Void,
                                   failure: (_ error: Error) -> Void
        )
    {
        
        colcWebServicesManager.callOAuthWS(oauth2ParentWSRequest,
                                      parsedResponse:{ (parsedObject: Any, statusCode: Int)->Void in
                                            //---------------------------------------------------------------------
                                            //OK - response could be valid json and 200 but may be an error
                                            //---------------------------------------------------------------------
                                            switch parsedObject{
                                            case let number as NSNumber:
                                                self.log.error("UNEXPECTED JSON RESPONSE: NSNumber:\(number)")
                                                
                                            case let nsArray as NSArray:
                                                //---------------------------------------------------------------------
                                                //GET REPOS returns array
                                                //self.log.error(" JSON RESPONSE: NSArray:\(parsedObjects)")
                                                //---------------------------------------------------------------------
                                                responseNSArray(nsArray, statusCode)
                                                //---------------------------------------------------------------------
                                                
                                            case let nsDictionary as NSDictionary:
                                                self.log.info("NSDictionary:\(nsDictionary)")
                                                //---------------------------------------------------------------------
                                                responseNSDictionary(nsDictionary, statusCode)
                                                //---------------------------------------------------------------------
                                            default:
                                                self.log.error("UNEXPECTED JSON RESPONSE: UNKNOWN TYPE")
                                            }
                                            //---------------------------------------------------------------------
            },
                                      failure:{ (error) -> Void in
                                        //not valid json
                                        self.log.error("error:\(error)")
            }
        )//callWS
        //---------------------------------------------------------------------
    }
    
}
