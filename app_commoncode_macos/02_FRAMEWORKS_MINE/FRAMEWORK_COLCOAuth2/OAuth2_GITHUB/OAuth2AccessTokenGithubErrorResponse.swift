//
//  OAuth2AccessTokenGithubErrorResponse.swift
//  joyride
//
//  Created by Brian Clear on 06/07/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation

//https://developer.github.com/v3/oauth/#common-errors-for-the-access-token-request

enum OAuth2AccessTokenGithubError : String {
    
    //https://developer.github.com/v3/oauth/#bad-verification-code
    case bad_verification_code = "bad_verification_code"

    func nsError() -> NSError{
        
        switch self{
        case .bad_verification_code:
            return NSError.appError(AppError.oAuth2_Github_bad_verification_code)
        }
    }
}


//ADD HEADER [Accept]:[application/json]
//{"error":"bad_verification_code","error_description":"The code passed is incorrect or expired.","error_uri":"https://developer.github.com/v3/oauth/#bad-verification-code"}
class OAuth2AccessTokenGithubErrorResponse: ParentMappable {
    
    var error: String?
    var error_description: String?
    var error_uri: String?
    
    // root calls mapping()
    //    required init?(_ map: Map){
    //        mapping(map: map)
    //    }
    
    override func mapping(map: Map){
        error <- map["error"]
        error_description <- map["error_description"]
        error_uri <- map["error_uri"]
    }
    var description: String
    {
        var description_ = "**** \(type(of: self)) *****\r"
        description_ = description_ + "error: \(error)\r"
        description_ = description_ + "errorDescription: \(error_description)\r"
        description_ = description_ + "errorUri: \(error_uri)\r"
        return description_
    }
}
