//
//  OAuth2AccessTokenGithubResponse.swift
//  joyride
//
//  Created by Brian Clear on 06/07/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation

//{"access_token":"5e493186a5c9f4bdbfab1bcf68eb05780731790d","token_type":"bearer","scope":"user"}
class OAuth2AccessTokenGithubResponse: ParentMappable, CustomStringConvertible {
    
    var access_token: String?
    var token_type: String?
    var scope: String?
    
    // root calls mapping()
    //    required init?(_ map: Map){
    //        mapping(map: map)
    //    }
    
    override func mapping(map: Map){
        access_token <- map["access_token"]
        token_type <- map["token_type"]
        scope <- map["oauth_redirect_uri"]
    }
    
    var description: String {
        var description_ = "**** \(type(of: self)) *****\r"
        description_ = description_ + "accessToken: \(access_token)\r"
        description_ = description_ + "scope: \(scope)\r"
        description_ = description_ + "tokenType: \(token_type)\r"
        return description_
    }
}
