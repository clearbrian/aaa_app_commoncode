
//
//	GithubUserRepo.swift
//
//	Create by Brian Clear on 6/7/2016
//	Copyright © 2016. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GithubUserRepo : ParentMappable{
    
    var archiveUrl : String?
    var assigneesUrl : String?
    var blobsUrl : String?
    var branchesUrl : String?
    var cloneUrl : String?
    var collaboratorsUrl : String?
    var commentsUrl : String?
    var commitsUrl : String?
    var compareUrl : String?
    var contentsUrl : String?
    var contributorsUrl : String?
    var createdAt : String?
    var defaultBranch : String?
    var deploymentsUrl : String?
    var descriptionField : String?
    var downloadsUrl : String?
    var eventsUrl : String?
    var fork : Bool?
    var forks : Int?
    var forksCount : Int?
    var forksUrl : String?
    var fullName : String?
    var gitCommitsUrl : String?
    var gitRefsUrl : String?
    var gitTagsUrl : String?
    var gitUrl : String?
    var hasDownloads : Bool?
    var hasIssues : Bool?
    var hasPages : Bool?
    var hasWiki : Bool?
    var homepage : String?
    var hooksUrl : String?
    var htmlUrl : String?
    var id : Int?
    var issueCommentUrl : String?
    var issueEventsUrl : String?
    var issuesUrl : String?
    var keysUrl : String?
    var labelsUrl : String?
    var language : String?
    var languagesUrl : String?
    var mergesUrl : String?
    var milestonesUrl : String?
    var mirrorUrl : String?
    var name : String?
    var notificationsUrl : String?
    var openIssues : Int?
    var openIssuesCount : Int?
    var owner : GithubUserRepoOwner?
    var permissions : GithubUserRepoPermission?
    var privateField : Bool?
    var pullsUrl : String?
    var pushedAt : String?
    var releasesUrl : String?
    var size : Int?
    var sshUrl : String?
    var stargazersCount : Int?
    var stargazersUrl : String?
    var statusesUrl : String?
    var subscribersUrl : String?
    var subscriptionUrl : String?
    var svnUrl : String?
    var tagsUrl : String?
    var teamsUrl : String?
    var treesUrl : String?
    var updatedAt : String?
    var url : String?
    var watchers : Int?
    var watchersCount : Int?
    
    
    
    override func mapping(map: Map)
    {
        archiveUrl <- map["archive_url"]
        assigneesUrl <- map["assignees_url"]
        blobsUrl <- map["blobs_url"]
        branchesUrl <- map["branches_url"]
        cloneUrl <- map["clone_url"]
        collaboratorsUrl <- map["collaborators_url"]
        commentsUrl <- map["comments_url"]
        commitsUrl <- map["commits_url"]
        compareUrl <- map["compare_url"]
        contentsUrl <- map["contents_url"]
        contributorsUrl <- map["contributors_url"]
        createdAt <- map["created_at"]
        defaultBranch <- map["default_branch"]
        deploymentsUrl <- map["deployments_url"]
        descriptionField <- map["description"]
        downloadsUrl <- map["downloads_url"]
        eventsUrl <- map["events_url"]
        fork <- map["fork"]
        forks <- map["forks"]
        forksCount <- map["forks_count"]
        forksUrl <- map["forks_url"]
        fullName <- map["full_name"]
        gitCommitsUrl <- map["git_commits_url"]
        gitRefsUrl <- map["git_refs_url"]
        gitTagsUrl <- map["git_tags_url"]
        gitUrl <- map["git_url"]
        hasDownloads <- map["has_downloads"]
        hasIssues <- map["has_issues"]
        hasPages <- map["has_pages"]
        hasWiki <- map["has_wiki"]
        homepage <- map["homepage"]
        hooksUrl <- map["hooks_url"]
        htmlUrl <- map["html_url"]
        id <- map["id"]
        issueCommentUrl <- map["issue_comment_url"]
        issueEventsUrl <- map["issue_events_url"]
        issuesUrl <- map["issues_url"]
        keysUrl <- map["keys_url"]
        labelsUrl <- map["labels_url"]
        language <- map["language"]
        languagesUrl <- map["languages_url"]
        mergesUrl <- map["merges_url"]
        milestonesUrl <- map["milestones_url"]
        mirrorUrl <- map["mirror_url"]
        name <- map["name"]
        notificationsUrl <- map["notifications_url"]
        openIssues <- map["open_issues"]
        openIssuesCount <- map["open_issues_count"]
        owner <- map["owner"]
        permissions <- map["permissions"]
        privateField <- map["private"]
        pullsUrl <- map["pulls_url"]
        pushedAt <- map["pushed_at"]
        releasesUrl <- map["releases_url"]
        size <- map["size"]
        sshUrl <- map["ssh_url"]
        stargazersCount <- map["stargazers_count"]
        stargazersUrl <- map["stargazers_url"]
        statusesUrl <- map["statuses_url"]
        subscribersUrl <- map["subscribers_url"]
        subscriptionUrl <- map["subscription_url"]
        svnUrl <- map["svn_url"]
        tagsUrl <- map["tags_url"]
        teamsUrl <- map["teams_url"]
        treesUrl <- map["trees_url"]
        updatedAt <- map["updated_at"]
        url <- map["url"]
        watchers <- map["watchers"]
        watchersCount <- map["watchers_count"]
        
    }
    var description: String
    {
        var description_ = "**** \(type(of: self)) *****\r"
        description_ = description_ + "archiveUrl: \(archiveUrl)\r"
        description_ = description_ + "assigneesUrl: \(assigneesUrl)\r"
        description_ = description_ + "blobsUrl: \(blobsUrl)\r"
        description_ = description_ + "branchesUrl: \(branchesUrl)\r"
        description_ = description_ + "cloneUrl: \(cloneUrl)\r"
        description_ = description_ + "collaboratorsUrl: \(collaboratorsUrl)\r"
        description_ = description_ + "commentsUrl: \(commentsUrl)\r"
        description_ = description_ + "commitsUrl: \(commitsUrl)\r"
        description_ = description_ + "compareUrl: \(compareUrl)\r"
        description_ = description_ + "contentsUrl: \(contentsUrl)\r"
        description_ = description_ + "contributorsUrl: \(contributorsUrl)\r"
        description_ = description_ + "createdAt: \(createdAt)\r"
        description_ = description_ + "defaultBranch: \(defaultBranch)\r"
        description_ = description_ + "deploymentsUrl: \(deploymentsUrl)\r"
        description_ = description_ + "descriptionField: \(descriptionField)\r"
        description_ = description_ + "downloadsUrl: \(downloadsUrl)\r"
        description_ = description_ + "eventsUrl: \(eventsUrl)\r"
        description_ = description_ + "fork: \(fork)\r"
        description_ = description_ + "forks: \(forks)\r"
        description_ = description_ + "forksCount: \(forksCount)\r"
        description_ = description_ + "forksUrl: \(forksUrl)\r"
        description_ = description_ + "fullName: \(fullName)\r"
        description_ = description_ + "gitCommitsUrl: \(gitCommitsUrl)\r"
        description_ = description_ + "gitRefsUrl: \(gitRefsUrl)\r"
        description_ = description_ + "gitTagsUrl: \(gitTagsUrl)\r"
        description_ = description_ + "gitUrl: \(gitUrl)\r"
        description_ = description_ + "hasDownloads: \(hasDownloads)\r"
        description_ = description_ + "hasIssues: \(hasIssues)\r"
        description_ = description_ + "hasPages: \(hasPages)\r"
        description_ = description_ + "hasWiki: \(hasWiki)\r"
        description_ = description_ + "homepage: \(homepage)\r"
        description_ = description_ + "hooksUrl: \(hooksUrl)\r"
        description_ = description_ + "htmlUrl: \(htmlUrl)\r"
        description_ = description_ + "id: \(id)\r"
        description_ = description_ + "issueCommentUrl: \(issueCommentUrl)\r"
        description_ = description_ + "issueEventsUrl: \(issueEventsUrl)\r"
        description_ = description_ + "issuesUrl: \(issuesUrl)\r"
        description_ = description_ + "keysUrl: \(keysUrl)\r"
        description_ = description_ + "labelsUrl: \(labelsUrl)\r"
        description_ = description_ + "language: \(language)\r"
        description_ = description_ + "languagesUrl: \(languagesUrl)\r"
        description_ = description_ + "mergesUrl: \(mergesUrl)\r"
        description_ = description_ + "milestonesUrl: \(milestonesUrl)\r"
        description_ = description_ + "mirrorUrl: \(mirrorUrl)\r"
        description_ = description_ + "name: \(name)\r"
        description_ = description_ + "notificationsUrl: \(notificationsUrl)\r"
        description_ = description_ + "openIssues: \(openIssues)\r"
        description_ = description_ + "openIssuesCount: \(openIssuesCount)\r"
        description_ = description_ + "owner: \(owner)\r"
        description_ = description_ + "permissions: \(permissions)\r"
        description_ = description_ + "privateField: \(privateField)\r"
        description_ = description_ + "pullsUrl: \(pullsUrl)\r"
        description_ = description_ + "pushedAt: \(pushedAt)\r"
        description_ = description_ + "releasesUrl: \(releasesUrl)\r"
        description_ = description_ + "size: \(size)\r"
        description_ = description_ + "sshUrl: \(sshUrl)\r"
        description_ = description_ + "stargazersCount: \(stargazersCount)\r"
        description_ = description_ + "stargazersUrl: \(stargazersUrl)\r"
        description_ = description_ + "statusesUrl: \(statusesUrl)\r"
        description_ = description_ + "subscribersUrl: \(subscribersUrl)\r"
        description_ = description_ + "subscriptionUrl: \(subscriptionUrl)\r"
        description_ = description_ + "svnUrl: \(svnUrl)\r"
        description_ = description_ + "tagsUrl: \(tagsUrl)\r"
        description_ = description_ + "teamsUrl: \(teamsUrl)\r"
        description_ = description_ + "treesUrl: \(treesUrl)\r"
        description_ = description_ + "updatedAt: \(updatedAt)\r"
        description_ = description_ + "url: \(url)\r"
        description_ = description_ + "watchers: \(watchers)\r"
        description_ = description_ + "watchersCount: \(watchersCount)\r"
        return description_
    }
    
}
