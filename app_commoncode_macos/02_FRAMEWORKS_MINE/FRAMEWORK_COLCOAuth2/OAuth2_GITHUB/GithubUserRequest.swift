//
//  OAuth2GithubUserRequest.swift
//  joyride
//
//  Created by Brian Clear on 06/07/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation

class GithubUserRequest : OAuth2ParentAccessTokenWSRequest{
    
    override init(access_token : String) {
        super.init(access_token: access_token)
        
        self.webServiceEndpoint = "https://api.github.com/user"

    }
}
