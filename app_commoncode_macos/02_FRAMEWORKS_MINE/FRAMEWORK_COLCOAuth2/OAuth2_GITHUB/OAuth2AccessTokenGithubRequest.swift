//
//  OAuth2AccessTokenGithubRequest.swift
//  joyride
//
//  Created by Brian Clear on 06/07/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation

class OAuth2AccessTokenGithubRequest : OAuth2ParentWSRequest{
    
    override init() {
        super.init()
        

        
//        //---------------------------------------------------------------------
//        //This is format of the BODY SENT
//        //---------------------------------------------------------------------
//        //application/x-www-form-urlencoded; charset=utf-8
//        //client_id=be45feb27af1729e6312&client_secret=bbf4bde86a4a3fa193b821cc4a6c610079c306d2&oauth_redirect_uri=joyrider://oauth/callback_github&state=8E7F9061
//        //---------------------------------------------------------------------
//        request.setValue("application/json;charset=UTF-8", forHTTPHeaderField: "Content-Type")
//        
//        //This is format of the RESPONSE RETURNED
//        //doent work with github
//        //request.setValue("application/json;charset=UTF-8", forHTTPHeaderField: "Accept")
//        request.setValue("application/json", forHTTPHeaderField: "Accept")
//        //------------------------------------------------
        
        
        //---------------------------------------------------------------------
        //This is format of the BODY SENT
        //---------------------------------------------------------------------
        //application/x-www-form-urlencoded; charset=utf-8
        //client_id=be45feb27af1729e6312&client_secret=bbf4bde86a4a3fa193b821cc4a6c610079c306d2&oauth_redirect_uri=joyrider://oauth/callback_github&state=8E7F9061
        //---------------------------------------------------------------------
        self.headers["Content-Type"] = "application/x-www-form-urlencoded; charset=utf-8"
        self.headers["Accept"] = "application/json"
    }
}
