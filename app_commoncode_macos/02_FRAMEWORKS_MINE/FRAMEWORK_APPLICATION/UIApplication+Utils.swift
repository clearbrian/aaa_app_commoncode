//
//  NSApplication+Utils.swift
//  mac_app_applemaps_tfl
//
//  Created by Brian Clear on 11/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

extension UIApplication{
    
    func showNetworkActivity(){
        
        DispatchQueue.main.async{
            self.isNetworkActivityIndicatorVisible = false
        }
        
    }
    
    func hideNetworkActivity(){
        
        DispatchQueue.main.async{
            self.isNetworkActivityIndicatorVisible = false
        }
        
    }
    
    func sendLocationToTopController(currentLocationManager: CurrentLocationManager,
                                     currentLocation : CLLocation, distanceMetersDouble : CLLocationDistance)
    {
    
        if let window = appDelegate.window {
            if let rootUINavigationController = window.rootViewController as? UINavigationController{
                //-------------------------------------------------------------------------
                //UINavigationController
                //-------------------------------------------------------------------------

                if let topViewController = rootUINavigationController.topViewController {
                    
                    self.sendLocationToTopVC(topViewController: topViewController,
                                        currentLocationManager: currentLocationManager,
                                               currentLocation: currentLocation,
                                               distanceMetersDouble : distanceMetersDouble)
                    
                }else{
                    appDelegate.log.error("rootUINavigationController.topViewController is nil")
                }
            }
            else if let rootUITabBarController = window.rootViewController as? UITabBarController
            {
                if let selectedViewController = rootUITabBarController.selectedViewController {
                    //-------------------------------------------------------------------------
                    //UITabBarController >> UINavigationController
                    //-------------------------------------------------------------------------

                    if let rootUINavigationController = selectedViewController as? UINavigationController{

                        //UITabBarController >> UINavigationController >> VC<CurrentLocationManagerDelegate>
                        if let topViewController = rootUINavigationController.topViewController {
                            
                            self.sendLocationToTopVC(topViewController : topViewController,
                                                currentLocationManager : currentLocationManager,
                                                       currentLocation : currentLocation,
                                                  distanceMetersDouble : distanceMetersDouble)
                            
                        }else{
                            appDelegate.log.error("rootUINavigationController.topViewController is nil")
                        }
                    }else{
                        appDelegate.log.error("window.rootViewController as? UINavigationController is nil:window.rootViewController \(String(describing: window.rootViewController))")
                    }
                    //-------------------------------------------------------------------
                    

                }else{
                    appDelegate.log.error("rootUINavigationController.topViewController is nil")
                }
            }
                //else if let viewController = window.rootViewController as? ViewController
                //else if let viewController = window.rootViewController as? BasicViewController
            else if let viewController: UIViewController = window.rootViewController// as? UIViewController
            {
                
                //tracking gps app
                self.sendLocationToTopVC(topViewController : viewController,
                                         currentLocationManager : currentLocationManager,
                                         currentLocation : currentLocation,
                                         distanceMetersDouble : distanceMetersDouble)
            
            }else
            {
                appDelegate.log.error("window.rootViewController as? UINavigationController is nil:window.rootViewController \(String(describing: window.rootViewController))")
            }
        }else{
            appDelegate.log.error("self.window is nil")
        }
    }
    
    func sendLocationToTopVC(topViewController : UIViewController,
                        currentLocationManager : CurrentLocationManager,
                               currentLocation : CLLocation,
                          distanceMetersDouble : CLLocationDistance)
    {
        if topViewController is CurrentLocationManagerDelegate{
            if let topViewControllerCurrentLocationManagerDelegate = topViewController as? CurrentLocationManagerDelegate {
                topViewControllerCurrentLocationManagerDelegate.currentLocationUpdated(currentLocationManager: currentLocationManager,
                                                                                              currentLocation: currentLocation,
                                                                                         distanceMetersDouble: distanceMetersDouble)
            }else{
                appDelegate.log.error("topViewController as? CurrentLocationManagerDelegate  is nil")
            }
        }else{
            //noisy
            //appDelegate.log.error("topViewController:[\(topViewController)] doesnt implement CurrentLocationManagerDelegate - cant forward current location")
        }
    }
    
    
    //--------------------------------------------------------------
    // MARK: - HEADING
    // MARK: -
    //--------------------------------------------------------------

    func sendHeadingToTopController(currentLocationManager: CurrentLocationManager,
                                    currentHeading : CLHeading)
    {
        
        if let window = appDelegate.window {
            if let rootUINavigationController = window.rootViewController as? UINavigationController{
                //-------------------------------------------------------------------------
                //UINavigationController
                //-------------------------------------------------------------------------
                
                if let topViewController = rootUINavigationController.topViewController {
                    
                    self.sendHeadingToTopVC(topViewController: topViewController,
                                            currentLocationManager: currentLocationManager,
                                            currentHeading: currentHeading)
                    
                }else{
                    appDelegate.log.error("rootUINavigationController.topViewController is nil")
                }
            }
            else if let rootUITabBarController = window.rootViewController as? UITabBarController
            {
                if let selectedViewController = rootUITabBarController.selectedViewController {
                    //-------------------------------------------------------------------------
                    //UITabBarController >> UINavigationController
                    //-------------------------------------------------------------------------
                    
                    if let rootUINavigationController = selectedViewController as? UINavigationController{
                        
                        //UITabBarController >> UINavigationController >> VC<currentLocationManagerDelegate>
                        if let topViewController = rootUINavigationController.topViewController {
                            
                            self.sendHeadingToTopVC(topViewController : topViewController,
                                                    currentLocationManager : currentLocationManager,
                                                    currentHeading : currentHeading)
                            
                        }else{
                            appDelegate.log.error("rootUINavigationController.topViewController is nil")
                        }
                    }else{
                        appDelegate.log.error("window.rootViewController as? UINavigationController is nil:window.rootViewController \(String(describing: window.rootViewController))")
                    }
                    //-------------------------------------------------------------------
                    
                    
                }else{
                    appDelegate.log.error("rootUINavigationController.topViewController is nil")
                }
            }
                //else if let viewController = window.rootViewController as? ViewController
                //else if let viewController = window.rootViewController as? BasicViewController
            else if let viewController: UIViewController = window.rootViewController// as? UIViewController
            {
                
                //tracking gps app
                self.sendHeadingToTopVC(topViewController : viewController,
                                        currentLocationManager : currentLocationManager,
                                        currentHeading : currentHeading)
                
            }else
            {
                appDelegate.log.error("window.rootViewController as? UINavigationController is nil:window.rootViewController \(String(describing: window.rootViewController))")
            }
        }else{
            appDelegate.log.error("self.window is nil")
        }
    }
    
    func sendHeadingToTopVC(topViewController : UIViewController,
                            currentLocationManager : CurrentLocationManager,
                            currentHeading : CLHeading)
    {
        if topViewController is CurrentLocationManagerDelegate{
            if let topViewControllercurrentLocationManagerDelegate = topViewController as? CurrentLocationManagerDelegate {
                
                topViewControllercurrentLocationManagerDelegate.currentHeadingUpdated(currentLocationManager: currentLocationManager,
                                                                                      newHeading: currentHeading)
            }else{
                appDelegate.log.error("topViewController as? currentLocationManagerDelegate  is nil")
            }
        }else{
            //noisy
            //appDelegate.log.error("topViewController:[\(topViewController)] doesnt implement currentLocationManagerDelegate - cant forward current location")
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - currentLocationFailed
    // MARK: -
    //--------------------------------------------------------------

    func sendToTopController_currentLocationFailed(currentLocationManager: CurrentLocationManager,
                                                   error: Error)
    {
        
        if let window = appDelegate.window {
            if let rootUINavigationController = window.rootViewController as? UINavigationController{
                //-------------------------------------------------------------------------
                //UINavigationController
                //-------------------------------------------------------------------------
                
                if let topViewController = rootUINavigationController.topViewController {
                    
                    self.sendToTopVC_currentLocationFailed(topViewController: topViewController,
                                                           currentLocationManager: currentLocationManager,
                                                           error: error)
                    
                }else{
                    appDelegate.log.error("rootUINavigationController.topViewController is nil")
                }
            }
            else if let rootUITabBarController = window.rootViewController as? UITabBarController
            {
                if let selectedViewController = rootUITabBarController.selectedViewController {
                    //-------------------------------------------------------------------------
                    //UITabBarController >> UINavigationController
                    //-------------------------------------------------------------------------
                    
                    if let rootUINavigationController = selectedViewController as? UINavigationController{
                        
                        //UITabBarController >> UINavigationController >> VC<currentLocationManagerDelegate>
                        if let topViewController = rootUINavigationController.topViewController {
                            
                            self.sendToTopVC_currentLocationFailed(topViewController : topViewController,
                                                                   currentLocationManager : currentLocationManager,
                                                                   error : error)
                            
                        }else{
                            appDelegate.log.error("rootUINavigationController.topViewController is nil")
                        }
                    }else{
                        appDelegate.log.error("window.rootViewController as? UINavigationController is nil:window.rootViewController \(String(describing: window.rootViewController))")
                    }
                    //-------------------------------------------------------------------
                    
                    
                }else{
                    appDelegate.log.error("rootUINavigationController.topViewController is nil")
                }
            }
                //else if let viewController = window.rootViewController as? ViewController
                //else if let viewController = window.rootViewController as? BasicViewController
            else if let viewController: UIViewController = window.rootViewController// as? UIViewController
            {
                
                //tracking gps app
                self.sendToTopVC_currentLocationFailed(topViewController : viewController,
                                                       currentLocationManager : currentLocationManager,
                                                       error : error)
                
            }else
            {
                appDelegate.log.error("window.rootViewController as? UINavigationController is nil:window.rootViewController \(String(describing: window.rootViewController))")
            }
        }else{
            appDelegate.log.error("self.window is nil")
        }
    }
    
    func sendToTopVC_currentLocationFailed(topViewController : UIViewController,
                                           currentLocationManager : CurrentLocationManager,
                                           error : Error)
    {
        if topViewController is CurrentLocationManagerDelegate{
            if let topViewControllercurrentLocationManagerDelegate = topViewController as? CurrentLocationManagerDelegate {
                
                topViewControllercurrentLocationManagerDelegate.currentLocationFailed(currentLocationManager: currentLocationManager,
                                                                                      error: error)
            }else{
                appDelegate.log.error("topViewController as? currentLocationManagerDelegate  is nil")
            }
        }else{
            //noisy
            //appDelegate.log.error("topViewController:[\(topViewController)] doesnt implement currentLocationManagerDelegate - cant forward current location")
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - applicationDidBecomeActive_refreshTableViewToTopController
    // MARK: -
    //--------------------------------------------------------------

    
    func send_applicationDidBecomeActive_refreshTableViewToTopController()
    {
        
        if let window = appDelegate.window {
            if let rootUINavigationController = window.rootViewController as? UINavigationController{
                //-------------------------------------------------------------------------
                //UINavigationController
                //-------------------------------------------------------------------------
                
                if let topViewController = rootUINavigationController.topViewController {
                    
                    self.send_applicationDidBecomeActive_refreshTableViewToTopVC(topViewController: topViewController)
                    
                }else{
                    appDelegate.log.error("rootUINavigationController.topViewController is nil")
                }
            }
            else if let rootUITabBarController = window.rootViewController as? UITabBarController
            {
                if let selectedViewController = rootUITabBarController.selectedViewController {
                    //-------------------------------------------------------------------------
                    //UITabBarController >> UINavigationController
                    //-------------------------------------------------------------------------
                    
                    if let rootUINavigationController = selectedViewController as? UINavigationController{
                        
                        //UITabBarController >> UINavigationController >> VC<CurrentLocationManagerDelegate>
                        if let topViewController = rootUINavigationController.topViewController {
                            
                            self.send_applicationDidBecomeActive_refreshTableViewToTopVC(topViewController : topViewController)
                            
                        }else{
                            appDelegate.log.error("rootUINavigationController.topViewController is nil")
                        }
                    }else{
                        appDelegate.log.error("window.rootViewController as? UINavigationController is nil:window.rootViewController \(String(describing: window.rootViewController))")
                    }
                    //-------------------------------------------------------------------
                    
                    
                }else{
                    appDelegate.log.error("rootUINavigationController.topViewController is nil")
                }
            }else{
                appDelegate.log.error("window.rootViewController as? UINavigationController is nil:window.rootViewController \(String(describing: window.rootViewController))")
            }
        }else{
            appDelegate.log.error("self.window is nil")
        }
    }
    
    func send_applicationDidBecomeActive_refreshTableViewToTopVC(topViewController : UIViewController)
    {
        if topViewController is CurrentLocationManagerDelegate{
            if let parentViewController = topViewController as? ParentViewController {
                parentViewController.applicationDidBecomeActive_refreshTableView()
                
            }else if let parentTableViewController = topViewController as? ParentTableViewController {
                parentTableViewController.applicationDidBecomeActive_refreshTableView()
            }else{
                appDelegate.log.error("topViewController not ParentViewController, ParentTableViewController")
            }
        }else{
            //noisy
            //appDelegate.log.error("topViewController:[\(topViewController)] doesnt implement CurrentLocationManagerDelegate - send_applicationDidBecomeActive_refreshTableViewToTopVC")
        }
    }
    
}
