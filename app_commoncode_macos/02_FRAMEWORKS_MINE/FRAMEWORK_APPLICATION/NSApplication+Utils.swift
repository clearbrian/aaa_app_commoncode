//
//  NSApplication+Utils.swift
//  mac_app_applemaps_tfl
//
//  Created by Brian Clear on 11/02/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
import AppKit
import CoreLocation

extension NSApplication{
    
    //var isNetworkActivityIndicatorVisible: Bool
    func showNetworkActivity(){
        
        DispatchQueue.main.async{
            //self.isNetworkActivityIndicatorVisible = false
        }
    }
    
    func hideNetworkActivity(){
        
        DispatchQueue.main.async{
            //self.isNetworkActivityIndicatorVisible = false
        }
    }
    
    func sendLocationToTopController(currentLocationManager: CurrentLocationManager,
                                     currentLocation : CLLocation)
    {
        if let mainWindow = sharedApplication.mainWindow {
            //-------------------------------------------------------------------
            if let contentViewController = mainWindow.contentViewController {
                if contentViewController is CurrentLocationManagerDelegate{
                    if let currentLocationManagerDelegate = contentViewController as? CurrentLocationManagerDelegate {
                        
                        currentLocationManagerDelegate.currentLocationUpdated(currentLocationManager: currentLocationManager,
                                                                              currentLocation: currentLocation)
                        
                    }else{
                        appDelegate.log.error("topViewController as? CurrentLocationManagerDelegate  is nil")
                    }
                }else{
                    //noisy
                    appDelegate.log.error("topViewController:[\(contentViewController)] doesnt implement CurrentLocationManagerDelegate - cant forward current location")
                }
            }
            //-------------------------------------------------------------------
            
        }else{
            appDelegate.log.error("sharedApplication.mainWindow is nil - did you call")
        }
    }
}
