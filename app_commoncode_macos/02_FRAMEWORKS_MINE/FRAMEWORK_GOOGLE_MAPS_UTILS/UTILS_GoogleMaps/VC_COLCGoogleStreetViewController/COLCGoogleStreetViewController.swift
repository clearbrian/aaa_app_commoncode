//
//  COLCGoogleStreetViewController.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 18/01/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

import UIKit
import GoogleMaps
import CoreLocation

/*
 https://developers.google.com/maps/documentation/ios-sdk/reference/interface_g_m_s_panorama_view
 */
class COLCGoogleStreetViewController: ParentViewController, GMSPanoramaViewDelegate {
    
    @IBOutlet weak var gmsPanoramaView: GMSPanoramaView!
    
    @IBOutlet weak var viewCurtain: UIView!
    @IBOutlet weak var colcDGActivityIndicatorView: COLCDGActivityIndicatorView!
    @IBOutlet weak var labelStreetViewUnavailable: UILabel!
    
    
    @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var labelDetail0: UILabel!
    @IBOutlet weak var labelDetail1: UILabel!
    @IBOutlet weak var labelSubDetail0: UILabel!
    @IBOutlet weak var labelSubDetail1: UILabel!
    
    
    
    @IBOutlet weak var imageViewCompass: UIImageView!
    
    func degreesToRadians(_ degrees: CGFloat) -> CGFloat{
        //return degrees / 180.0 * CGFloat(M_PI)
        return degrees / 180.0 * CGFloat(Double.pi)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.labelTitle.applyCustomFontForCurrentTextStyle()
        self.labelDetail0.applyCustomFontForCurrentTextStyle()
        self.labelDetail1.applyCustomFontForCurrentTextStyle()
        self.labelSubDetail0.applyCustomFontForCurrentTextStyle()
        self.labelSubDetail1.applyCustomFontForCurrentTextStyle()
        
        self.labelTitle?.text = ""
        self.labelDetail0?.text = ""
        self.labelDetail1?.text = ""
        self.labelSubDetail0?.text = ""
        self.labelSubDetail1?.text = ""

        self.buttonDirections.layer.cornerRadius = 4.0
        
        //rotateCompassToHeading(45.0)
        
        configureGMSPanoramaView()
        showStreetViewAtLocation()
        
        //showMarker() should return true to see the marker
        overlayMarkerForCoordinate()
        configureNavBar()
    }
    
    func configureNavBar() {
        //----------------------------------------------------------------------------------------
        //Menu
        //----------------------------------------------------------------------------------------
         navigationItem.leftBarButtonItem = UIBarButtonItem(title: "< Back",
                                                           style: .plain,
                                                           target: self,
                                                           action: #selector(COLCGoogleStreetViewController.leftBarButtonItem_Action))
        //----------------------------------------------------------------------------------------
        //Map
        //----------------------------------------------------------------------------------------
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Directions",
                                                            style: .plain,
                                                            target: self,
                                                            action: #selector(COLCGoogleStreetViewController.rightBarButtonItem_Action))
        //----------------------------------------------------------------------------------------
    }
    
    func leftBarButtonItem_Action() {
       
        //self.dismiss(animated: true, completion: {
        //
        //})
        
        if let navigationController = self.navigationController {
            navigationController.popViewController(animated: true)
        }else{
            appDelegate.log.error("self.navigationController is nil")
        }
        
    }
    
    func rightBarButtonItem_Action() {
        self.showAlertOptions()
    }
    
    func showAlertOptions(){
        appDelegate.log.error("SUBCLASS")
    }
    
    
    
    
    
    
    //--------------------------------------------------------------
    // MARK: - COMPASS
    // MARK: -
    //--------------------------------------------------------------

    func rotateCompassToHeading(_ degrees: CGFloat){
        imageViewCompass.transform = CGAffineTransform(rotationAngle: degreesToRadians(degrees))
    }
    
    
    func configureGMSPanoramaView(){
        self.gmsPanoramaView.delegate = self
    }
    
    func showStreetViewAtLocation(){
        
        self.viewCurtain.isHidden = false
        self.colcDGActivityIndicatorView?.isHidden = false
        
        self.labelStreetViewUnavailable.isHidden = true
        self.buttonShowDirections.isHidden = true
        
        if let clLocation = self.clLocation {
            
            //London Bride Bus Station is underground
            //            let clLocationStreetView = CLLocation.init(coordinate: clLocation.coordinate,
            //                                                       altitude: 100.0,
            //                                                       horizontalAccuracy: 0.0,
            //                                                       verticalAccuracy: 0.0, timestamp: Date())
            
            
            //print("[showStreetViewAtLocation] moveNearCoordinate(\(clLocation))")
            self.gmsPanoramaView.moveNearCoordinate(clLocation.coordinate)
        }else{
            appDelegate.log.error("self.clLocation is nil - cannot moveNearCoordinate")
        }
        
    }
    
    func showStreetViewUnavaiable(){
        self.viewCurtain.isHidden = false
        self.colcDGActivityIndicatorView?.isHidden = true
        
        self.labelStreetViewUnavailable.isHidden = false
        self.buttonShowDirections.isHidden = false
    }
    
    func hideCurtain(){
        self.viewCurtain.isHidden = true
        self.colcDGActivityIndicatorView?.isHidden = true
        
        self.labelStreetViewUnavailable.isHidden = true
        self.buttonShowDirections.isHidden = true
        
        
    }
    
    
    //--------------------------------------------------------------
    // MARK: - BUTTONS : Directions - there 2 - one on security curtain
    // MARK: -
    //--------------------------------------------------------------

    @IBOutlet weak var buttonDirections: UIButton!
    
    @IBAction func buttonDirections_Action(_ sender: Any) {
        self.showDirections()
    }
    
    //Street view not avaialable for some places e.g. Canary Wharf for security reason let then redirect to Directions
    @IBOutlet weak var buttonShowDirections: UIButton!
    
    @IBAction func buttonShowDirections_Action(_ sender: Any) {
        showDirections()
    }
    
    
    //called by subclass but with diff segue
    func showDirections(){
        self.performSegue(withIdentifier: self.segueTo_TFLApiPlaceMapDirectionsViewController, sender: nil)
        
    }
    
    var segueTo_TFLApiPlaceMapDirectionsViewController: String{
        //subclass
        return "segueCOLCGoogleStreetViewControllerTOTFLApiPlaceMapDirectionsViewController"
    }
    
  
    
    //--------------------------------------------------------------
    // MARK: - Override - provide location for streetview
    // MARK: -
    //--------------------------------------------------------------
    //SUBCLASS - Override to provide your own location
    var clLocation: CLLocation?{
        
        // Create a marker at the Eiffel Tower
        return CLLocation(latitude: -33.732, longitude: 150.312)
    }
    
    //showMarker() should return true to see the marker
    func overlayMarkerForCoordinate(){
        if showMarker(){
            if let clLocation = self.clLocation {
                
                let position = CLLocationCoordinate2D(latitude: clLocation.coordinate.latitude, longitude: clLocation.coordinate.longitude)
                let marker = GMSMarker(position: position)
                
                //----------------------------------------------------------------------------------------
                //overiddable properties
                marker.title = self.markerTitle
                marker.snippet = self.markerSnippet
                marker.icon = self.imageForMarker
                //----------------------------------------------------------------------------------------
                
                marker.panoramaView = self.gmsPanoramaView
            }else{
                appDelegate.log.error("self.clLocation is nil - cannot moveNearCoordinate")
            }
        }else{
            self.log.debug("showMarker - false - dont add marker")
        }
    }
    
    //--------------------------------------------------------------
    // MARK: - SUBCLASS - fill from the object
    // MARK: -
    //--------------------------------------------------------------
    //if true overlat submarker GMSMarker
    func showMarker() -> Bool{
        return true
    }
    
    //Subclass - Text label popup when you tap on marker - fill from the subclass object - e.g. taxiRank.name
    var markerTitle : String{
        return "marker.title"
    }
    
    //Subclass - Detail label on marker - optional
    var markerSnippet : String?{
        return "marker.snippet"
    }
    
    //--------------------------------------------------------------
    // MARK: - IMAGE - PIN is Overlayed on street view - rotates it
    // MARK: -
    //--------------------------------------------------------------
    //Doesnt seem to support animated images
    var imageForMarker: UIImage?{
        var imageForMarker: UIImage? = nil
//        if let animatedImage = self.animatedMarkerImage() {
//            imageForMarker = animatedImage
//            
//        }else if let namedImage = self.singleMarkerImage(named: self.singleMarkerImageName) {
//            imageForMarker = namedImage
//            
//        }else{
//            appDelegate.log.error("animatedImage()/namedImage both returned nil")
//        }
        if let namedImage = self.singleMarkerImage(named: self.singleMarkerImageName) {
            imageForMarker = namedImage
            
        }else{
            appDelegate.log.error("animatedImage()/namedImage both returned nil")
        }
        return imageForMarker
    }
    
    //--------------------------------------------------------------
    // MARK: - MARKER - Single Marker
    // MARK: -
    //--------------------------------------------------------------

    //Subclass - provide image name for overlay pin icon
    var singleMarkerImageName: String{
        return "taxipin"
        
    }
    
    //UIImage(named: "taxipin")
    func singleMarkerImage(named: String) -> UIImage?{
        
        if let namedImage_ = UIImage(named: named) {
            return namedImage_
        }else{
            appDelegate.log.error("UIImage(named: '\(named)')  is nil")
        }
        return nil
    }
    
    //--------------------------------------------------------------
    // MARK: - MARKER - Animated Marker - not working on StreetView
    // MARK: -
    //--------------------------------------------------------------
    func animatedMarkerImage() -> UIImage?{
        var animatedImageReturned : UIImage? = nil
        
        if let animatedImage = UIImage.animatedImageWithNames(self.animatedMarkerImageNames, duration: self.animatedMarkerDuration){
            animatedImageReturned = animatedImage
        }else{
            appDelegate.log.error("animatedImageWithNames is nil")
        }
        
        return animatedImageReturned
    }
    
    //Subclass
    var animatedMarkerImageNames: [String]{
        return ["loading_1", "loading_2", "loading_3"]
    }
    
    var animatedMarkerDuration: TimeInterval{
        return 3.0
    }

    //--------------------------------------------------------------
    // MARK: - lifecycle
    // MARK: -
    //--------------------------------------------------------------

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //--------------------------------------------------------------
    // MARK: - GMSPanoramaViewDelegate
    // MARK: -
    //--------------------------------------------------------------
    
    /**
     * Called when starting a move to another panorama.
     * This can be the result of interactive navigation to a neighbouring panorama.
     * At the moment this method is called, the |view|.panorama is still
     * pointing to the old panorama, as the new panorama identified by |panoID|
     * is not yet resolved. panoramaView:didMoveToPanorama: will be called when the
     * new panorama is ready.
     */
    func panoramaView(_ view: GMSPanoramaView, willMoveToPanoramaID panoramaID: String){
        //when you click on the arrows on the street it moves to another streetview/panorama
        //print("willMoveToPanoramaID:\(panoramaID) - GMSPanoramaView:[\(view)]")
    }
    
    /**
     * This is invoked every time the |view|.panorama property changes.
     */
    func panoramaView(_ view: GMSPanoramaView, didMoveTo panorama: GMSPanorama?){
        //changed panorama/streetview - ususally by tapping the white < > in streetview
        //print("panoramaView:didMoveTo panorama:GMSPanorama [\(panorama)]")
    }
    
    /**
     * Called when the panorama change was caused by invoking
     * moveToPanoramaNearCoordinate:. The coordinate passed to that method will also
     * be passed here.
     */
    func panoramaView(_ view: GMSPanoramaView, didMoveTo panorama: GMSPanorama, nearCoordinate coordinate: CLLocationCoordinate2D){
        //Called once - when streetview loaded ok
        //print("didMoveTo panorama:      nearCoordinate:[\(coordinate)]")
        //print("didMoveTo panorama: panorama.coordinate:[\(panorama.coordinate)]")
        
        //Street view panorama is a point NEAR to the CLLocattio
        if let clLocation = self.clLocation {
            let headingToTaxiRank: CLLocationDirection = GMSGeometryHeading(panorama.coordinate, clLocation.coordinate )
            
            //print("HEADING TO headingToTaxiRank:\(headingToTaxiRank)")
            
            //tilt down in case its v close
            let cameraPointingAtStop = GMSPanoramaCamera.init(heading: headingToTaxiRank, pitch: -10.0, zoom: 1.0)
            view.animate(to: cameraPointingAtStop, animationDuration: 2.0)
            
            
        }else{
            appDelegate.log.error("self.clLocation is nil - cannot get GMSGeometryHeading")
        }
        
        self.hideCurtain()
    }
    
    /**
     * Called when moveNearCoordinate: produces an error.
     */
    func panoramaView(_ view: GMSPanoramaView, error: Error, onMoveNearCoordinate coordinate: CLLocationCoordinate2D){
        //1 cabot sq has no streetview
        //onMoveNearCoordinate:error:Error Domain=com.google.maps.GMSPanoramaService Code=0 "(null)"
        //print("onMoveNearCoordinate:error:\(error)")

        //CLKAlertController.showAlertInVC(self, title: "Streetview unavailable", message: "Street view is unavailable for that location")
        self.showStreetViewUnavaiable()
    }
    
    /**
     * Called when moveToPanoramaID: produces an error.
     */
    func panoramaView(_ view: GMSPanoramaView, error: Error, onMoveToPanoramaID panoramaID: String){
        //print("onMoveToPanoramaID")
    }
    
    /**
     * Called repeatedly during changes to the camera on GMSPanoramaView. This may
     * not be called for all intermediate camera values, but is always called for
     * the final position of the camera after an animation or gesture.
     */
    func panoramaView(_ panoramaView: GMSPanoramaView, didMove camera: GMSPanoramaCamera){
        //called repeatedly as you rotate the view
        ////print("didMove: GMSPanoramaCamera:\(camera)")
       
        //CLLocationDirection is Double alias
        let clLocationDirection : CLLocationDirection = camera.orientation.heading
        
        self.rotateCompassToHeading(CGFloat(clLocationDirection))
    }
    
    /**
     * Called when a user has tapped on the GMSPanoramaView, but this tap was not
     * consumed (taps may be consumed by e.g., tapping on a navigation arrow).
     */
    func panoramaView(_ panoramaView: GMSPanoramaView, didTap point: CGPoint){
        ////print("didTap")
    }
    
    /**
     * Called after a marker has been tapped.  May return YES to indicate the event
     * has been fully handled and suppress any default behavior.
     */
    func panoramaView(_ panoramaView: GMSPanoramaView, didTap marker: GMSMarker) -> Bool{
        
        //print("didTap marker")
        return false
    }
    
    
    /**
     * Called when the panorama tiles for the current view have just been requested
     * and are beginning to load.
     */
    func panoramaViewDidStartRendering(_ panoramaView: GMSPanoramaView){
        //print("panoramaViewDidStartRendering")
    }
    
    /**
     * Called when the panorama tiles have been loaded (or permanently failed to load)
     * and rendered on screen.
     */
    func panoramaViewDidFinishRendering(_ panoramaView: GMSPanoramaView){
        //called everytime you move the map - scroll/spin has stopped
        //print("panoramaViewDidFinishRendering")
        
    }
}
