//
//  GoogleMappable.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 17/01/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation
import CoreLocation
import GoogleMaps

//MapsPinnable includes CoreLocatable
protocol GoogleMappable: MapPinnable{
    
    //-------------------------------------------------------------------
    //Swift 3 - cant override yet in extensions for var OR func so move into main class
    var gmsMarker : GMSMarker? {get}
    //-------------------------------------------------------------------
    

//    func markerTitle() -> String
//    func markerSnippet() -> String
    
}
