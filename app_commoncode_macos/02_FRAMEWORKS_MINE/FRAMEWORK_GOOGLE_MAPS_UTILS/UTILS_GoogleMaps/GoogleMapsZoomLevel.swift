//
//  GoogleMapsZoomLevel.swift
//  app_pubtranslon_taxis
//
//  Created by Brian Clear on 17/01/2017.
//  Copyright © 2017 Brian Clear. All rights reserved.
//

import Foundation

//--------------------------------------------------------------
// MARK: - GoogleMapsZoomLevel
// MARK: -
//--------------------------------------------------------------
//    self.log.debug("kGMSMinZoomLevel:\(kGMSMinZoomLevel)")
//    self.log.debug("kGMSMaxZoomLevel:\(kGMSMaxZoomLevel)")
//    kGMSMinZoomLevel:2.0
//    kGMSMaxZoomLevel:21.0
//--------------------------------------------------------------

enum GoogleMapsZoomLevel : Int {
    //    case ZoomLevel0_WORLD = 0
    //    case ZoomLevel1_ = 1
    case zoomlevel2_WORLD = 2            /* kGMSMinZoomLevel: 2.0 */
    case zoomlevel3_CONTINENT = 3
    case zoomlevel4_CONTINENT1 = 4
    case zoomlevel5_COUNTRY = 5
    case zoomlevel6_COUNTRY1 = 6
    case zoomlevel7_COUNTIES = 7
    case zoomlevel8_COUNTY = 8
    case zoomlevel9_OUTER_CITY = 9
    case zoomlevel10_OUTER_CITY1 = 10
    case zoomlevel11_TOWN_LOCALITY = 11
    case zoomlevel12_TOWN_AND_STREETS = 12
    case zoomlevel13_TOWN_MAIN_STREETS = 13
    case zoomlevel14_TOWN_POSTCODE_AREA = 14
    case zoomlevel15_NEARBY_STREETS = 15
    case zoomlevel16_POSTCODE = 16
    case zoomlevel17_STREET = 17
    case zoomlevel18_BLOCK = 18
    case zoomlevel19_BUILDING = 19
    case zoomlevel20_BUILDING = 20
    case zoomlevel21_EXACT_LOCATION = 21 /* kGMSMaxZoomLevel: 2.0 */
    
    
    func doubleValue() -> Double{
        return Double.init(self.rawValue)
    }
    func floatValue() -> Float{
        return Float.init(self.rawValue)
    }

}
//--------------------------------------------------------------
