//
//  NSColor+Clarksons.swift
//  SandPforiOS
//
//  Created by Brian Clear on 10/06/2015.
//  Copyright (c) 2015 CLARKSONS RESEARCH SERVICES LIMITED. All rights reserved.
//

import Foundation
import AppKit

extension NSColor {
    
    
    public class func appBaseColor() -> NSColor { 
        
        //return NSColor(red: 26.0/255.0,  green:36.0/255.0,  blue:41.0/255.0,  alpha:1.0)
        return NSColor.appColorCYAN()
    }
    
    public class func appColorMIDNIGHTBLUE() -> NSColor { 
        //clarksons base color - Midnight blue from website
        //http://site.clarksons.com/images/background.png
        return NSColor(red: 26.00/255.0,  green:36.00/255.0,  blue:41.00/255.0,  alpha:1.00)
    }
    //    public class func appColorCYAN() -> NSColor { 
    //        return NSColor(red: 29.00/255.0,  green:181.00/255.0,  blue:204.00/255.0,  alpha:1.00)
    //    }
    public class func appColorCYAN() -> NSColor {
        return NSColor(red: 55.00/255.0,  green:142.00/255.0,  blue:218.00/255.0,  alpha:1.00)
    }

    public class func appColorYELLOW() -> NSColor {
        return NSColor(red: 208.00/255.0,  green:197.00/255.0,  blue:42.00/255.0,  alpha:1.00)
    }
    
    public class func appColorREDFLAT() -> NSColor { 
        return NSColor(red: 207.00/255.0,  green:28.00/255.0,  blue:29.00/255.0,  alpha:1.00)
    }
    
    public class func appColorGREENLIGHT() -> NSColor { 
        return NSColor(red: 88.00/255.0,  green:169.00/255.0,  blue:45.00/255.0,  alpha:1.00)
    }
    
    public class func appColorGREEN() -> NSColor { 
        return NSColor(red: 68.00/255.0,  green:170.00/255.0,  blue:66.00/255.0,  alpha:1.00)
    }
    public class func appColorORANGE() -> NSColor { 
        return NSColor(red: 252.00/255.0,  green:108.00/255.0,  blue:33.00/255.0,  alpha:1.00)
    }
    public class func appColorPURPLE() -> NSColor { 
        return NSColor(red: 246.00/255.0,  green:26.00/255.0,  blue:162.00/255.0,  alpha:1.00)
    }
    
    //taken from Clarksons annual report - not on official site but
    public class func appColorBLUEDARK() -> NSColor { 
        return NSColor(red: 3.00/255.0,  green:49.00/255.0,  blue:76.00/255.0,  alpha:1.00)
    }
    public class func appColorBLUELIGHT() -> NSColor { 
        return NSColor(red: 8.00/255.0,  green:81.00/255.0,  blue:111.00/255.0,  alpha:1.00)
    }
    public class func appColorBLUEGREEN() -> NSColor { 
        return NSColor(red: 19.00/255.0,  green:133.00/255.0,  blue:151.00/255.0,  alpha:1.00)
    }
    
    public class func appColorGray() -> NSColor {
        return NSColor(red: 226.00/255.0,  green:228.00/255.0,  blue:229.00/255.0,  alpha:1.00)
    }
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - https://flatNSColors.com/
    //--------------------------------------------------------------

    public class func appColorFlatPinkRed() -> NSColor {
        return NSColor(red: 255.00/255.0,  green:38.00/255.0,  blue:82.00/255.0,  alpha:1.00)
    }
    
    public class func appColorFlat_Turquoise() -> NSColor {
        return NSColor(red: 26.00/255.0,  green:188.00/255.0,  blue:156.00/255.0,  alpha:1.00)
    }
    
    public class func appColorFlat_Purple_SEANCE() -> NSColor {
        return NSColor(red: 154.0/255.0,  green:18.0/255.0,  blue:179.0/255.0,  alpha:1.00)
    }

    
    //Emerald
    public class func appColorFlat_Emerald() -> NSColor {
        return NSColor(red: 46.00/255.0,  green:204.00/255.0,  blue:113.00/255.0,  alpha:1.00)
    }

    
    //PETER RIVER
    //rgb(52, 152, 219)
    public class func appColorFlat_Peter_River() -> NSColor {
        return NSColor(red: 52.00/255.0,  green:152.00/255.0,  blue:219.00/255.0,  alpha:1.00)
    }
    
    //AMETHYST/purple
    //rgb(155, 89, 182)
    public class func appColorFlat_Amethyst() -> NSColor {
        return NSColor(red: 155.00/255.0,  green:89.00/255.0,  blue:182.00/255.0,  alpha:1.00)
    }
    
    //WET ASPHALT/grey
    //rgb(52, 73, 94)
    public class func appColorFlat_WetAsphalt() -> NSColor {
        return NSColor(red: 52.00/255.0,  green:73.00/255.0,  blue:94.00/255.0,  alpha:1.00)
    }
    //CARROT
    //rgb(230, 126, 34)
    public class func appColorFlat_Carrot() -> NSColor {
        return NSColor(red: 230.00/255.0,  green:126.00/255.0,  blue:34.00/255.0,  alpha:1.00)
    }
    
    
    //pink red
    //rgb(231, 76, 60)
    public class func appColorFlat_Alizarin_Red() -> NSColor {
        return NSColor(red: 231.00/255.0,  green:76.00/255.0,  blue:60.00/255.0,  alpha:1.00)
    }
    
    //concrete
    //rgb(127, 140, 141)
    public class func appColorFlat_Concrete() -> NSColor {
        return NSColor(red: 127.00/255.0,  green:140.00/255.0,  blue:141.00/255.0,  alpha:1.00)
    }
    
    //orange
    //rgb(243, 156, 18)
    public class func appColorFlat_Orange() -> NSColor {
        return NSColor(red: 243.00/255.0,  green:156.00/255.0,  blue:18.00/255.0,  alpha:1.00)
    }
    //rgb(241, 196, 15)
    public class func appColorFlat_Sunflower() -> NSColor {
        return NSColor(red: 241.00/255.0,  green:196.00/255.0,  blue:15.00/255.0,  alpha:1.00)
    }
    //pomegranate
    //rgb(192, 57, 43)
    public class func appColorFlat_Pomegranate() -> NSColor {
        return NSColor(red: 192.00/255.0,  green:57.00/255.0,  blue:43.00/255.0,  alpha:1.00)
    }
    
    
    //http://www.flatNSColorpicker.com/
//    MEDIUM PURPLE
//    BF55EC
//    191, 85, 236
    public class func appColorFlat_Medium_Purple() -> NSColor {
        return NSColor(red: 191.00/255.0,  green:85.00/255.0,  blue:236.00/255.0,  alpha:1.00)
    }
//    PICTON BLUE
//    22A7F0
//    34, 167, 240
    public class func appColorFlat_Picton_Blue() -> NSColor {
        return NSColor(red: 34.00/255.0,  green:167.00/255.0,  blue:240.00/255.0,  alpha:1.00)
    }
    
//    GOSSIP
//    87D37C
//    135, 211, 124
    public class func appColorFlat_Gossip() -> NSColor {
        return NSColor(red: 135.00/255.0,  green:211.00/255.0,  blue:124.00/255.0,  alpha:1.00)
    }
    
//    BUTTERCUP
//    F39C12
//    243, 156, 18
    public class func appColorFlat_Buttercup() -> NSColor {
        return NSColor(red: 243.00/255.0,  green:156.00/255.0,  blue:18.00/255.0,  alpha:1.00)
    }
    
//    SUNSET ORANGE
//    F64747
//    246, 71, 71
    
    public class func appColorFlat_SunsetOrange() -> NSColor {
        return NSColor.rgb(246, green:71, blue:71)
    }

    public class func appColorFlat_YellowOrange_California() -> NSColor {
        return NSColor.rgb(248, green:148, blue:6)
    }
    
    
    public class func rgb(_ red:Int, green:Int, blue:Int) -> NSColor {
        return NSColor(red: CGFloat(red)/255.0,  green:CGFloat(green)/255.0,  blue:CGFloat(blue)/255.0,  alpha:1.00)
    }
    
    //--------------------------------------------------------------
    // MARK: - GoogleMapUtils
    // MARK: -
    //--------------------------------------------------------------

    public class func appColorMapLabelStroke() -> NSColor {
        return NSColor.rgb(5, green:24, blue:76)
        
    }
    public class func appColorMapLabelFill() -> NSColor {
        return NSColor.rgb(240, green:237, blue:239)
        
    }
    //--------------------------------------------------------------

}
