//
//  NSError+Convenience.swift
//  SandPforiOS
//
//  Created by Brian Clear on 09/06/2015.
//  Copyright (c) 2015 CLARKSONS RESEARCH SERVICES LIMITED. All rights reserved.
//

import Foundation

var supportEmailAddress = "support@cityoflondonconsulting.com"

public let AppErrorTitleKey: String = "AppErrorTitleKey"
public let AppErrorDescriptionKey: String = "AppErrorDescriptionKey"


public enum AppError : Int {
    
    //------------------------------------------------------------------------------------------------
    //DEFINE ERROR CODES
    //------------------------------------------------------------------------------------------------
    
    //-----------------------------------------------
    //1000+ Webservice Errors
    //-----------------------------------------------
    
    case webserviceError_Status_400 = 1400
    
    //-----------------------------------------------
    //WebserviceManager
    //-----------------------------------------------
    case loginFailed_Username_Is_Blank = 2000
    case loginFailed_Password_Is_Blank = 2001
    
    //-----------------------------------------------
    //ARC GooglePlaces - Status Codes
    //-----------------------------------------------
    //https://developers.google.com/places/webservice/search
    //    Status Codes
    
    //    The "status" field within the search response object contains the status of the request, and may contain debugging information to help you track down why the request failed. The "status" field may contain the following values:
    
    //    OK indicates that no errors occurred; the place was successfully detected and at least one result was returned.
    //    ZERO_RESULTS indicates that the search was successful but returned no results. This may occur if the search was passed a latlng in a remote location.
    //    OVER_QUERY_LIMIT indicates that you are over your quota.
    //    REQUEST_DENIED indicates that your request was denied, generally because of lack of an invalid key parameter.
    //    INVALID_REQUEST generally indicates that a required query parameter (location or radius) is missing.
    
    //GooglePlacesResponseStatus_OK >> no error

    case googlePlacesResponseStatus_ZERO_RESULTS_PLACES = 3000
    case googlePlacesResponseStatus_ZERO_RESULTS_DISTANCE = 3004
    
    case googlePlacesResponseStatus_OVER_QUERY_LIMIT = 3001
    case googlePlacesResponseStatus_REQUEST_DENIED = 3002
    case googlePlacesResponseStatus_INVALID_REQUEST = 3003

    //-----------------------------------------------
    //OAuth2 - Github
    //-----------------------------------------------
    case oAuth2_Github_bad_verification_code = 3100
    //token is CRAP_TOKEN
    case oAuth2_Github_401_not_authorized = 3401
    
    case oAuth2_400_access_token_error = 3402

    //-----------------------------------------------
    //OAuth2 - Foursquare
    //-----------------------------------------------
    case oAuth2_Foursquare_error = 4666
    case oAuth2_Foursquare_401_not_authorized = 4401

    //-----------------------------------------------
    //9000+ WebserviceManager/JSON
    //-----------------------------------------------
    
    case webserviceManager_CantParseJson_NSData = 9050
    case webserviceManager_CantParseJson_NString_Returned_Not_NSArray_Or_NSDictionary = 9051
    //-----------------------------------------------
    //9000+ WebserviceManager/JSON
    //-----------------------------------------------
    
    case geocode_reverse_google_failed = 10050
    case geocode_forward_apple_failed = 10150

    
    
    
    
    //------------------------------------------------------------------------------------------------
    //DEFINE ERROR MESSAGES
    //------------------------------------------------------------------------------------------------
    //error message displayed to user is handled here
    public func description() -> String {
        switch self {
            
            //1000+ Webservice Errors
        case .webserviceError_Status_400:
            return "Cannot login. You do not have permission.\rPlease contact \(supportEmailAddress). [Error: \(self.rawValue)]"
            
        case .loginFailed_Username_Is_Blank:
            return "Cannot login. Username is blank."
            
        case .loginFailed_Password_Is_Blank:
            return "Cannot login. Password is blank."
            
        case .webserviceManager_CantParseJson_NSData:
            return "An error has occured parsing the response.\rPlease contact \(supportEmailAddress). [Error: \(self.rawValue)]"
            
        case .webserviceManager_CantParseJson_NString_Returned_Not_NSArray_Or_NSDictionary:
            return "An error has occured parsing the response.\rPlease contact \(supportEmailAddress). [Error: \(self.rawValue)]"
            
            /* =========================== ZERO_RESULTS ===========================*/
        case .googlePlacesResponseStatus_ZERO_RESULTS_PLACES:
            return "There are no Google places or businesses at that location. Try moving the map to a more populated area"
            
        case .googlePlacesResponseStatus_ZERO_RESULTS_DISTANCE:
            //getting distance from new york to london - geocode will return ZERO_RESULTS but error me
            return "There are no no Google places or businesses at that location."
            /* =========================== ZERO_RESULTS ===========================*/
            
        case .googlePlacesResponseStatus_OVER_QUERY_LIMIT:
            // TODO: - FLAG THIS SOME HOW - see to warn me!
            return "A search error has occured.\rPlease contact \(supportEmailAddress).[Error:\(self.rawValue) - OVER_QUERY_LIMIT]"
            
        case .googlePlacesResponseStatus_REQUEST_DENIED:
            return "An login error has occured.\rPlease contact \(supportEmailAddress).[Error:\(self.rawValue) - REQUEST_DENIED]"
            
        case .googlePlacesResponseStatus_INVALID_REQUEST:
            return "There was a unexpected problem with this request.\rPlease contact \(supportEmailAddress).[Error:\(self.rawValue) - INVALID_REQUEST]"
            
        case .oAuth2_Github_bad_verification_code:
            return "Could not retrieve an access token.\rPlease contact \(supportEmailAddress).[App Error:\(self.rawValue) - 'bad_verification_code']"
            
        case .oAuth2_Github_401_not_authorized:
            return "Could not retrieve an access token.\rPlease contact \(supportEmailAddress).[App Error:\(self.rawValue) - '401 token issue']"
            
        case .oAuth2_400_access_token_error:
            return "Could not retrieve an access token.\rPlease contact \(supportEmailAddress).[App Error:\(self.rawValue) - '400 token issue']"
            
        case .oAuth2_Foursquare_error:
            return "Could not retrieve an access token.\rPlease contact \(supportEmailAddress).[App Error:\(self.rawValue) - 'error']"
  
        case .oAuth2_Foursquare_401_not_authorized:
            return "Could not retrieve an access token.\rPlease contact \(supportEmailAddress).[App Error:\(self.rawValue) - '401 token issue']"
            
        case .geocode_reverse_google_failed:
            return "Could not retrieve an exact address for that location.\rPlease contact \(supportEmailAddress).[App Error:\(self.rawValue) - 'Geocode Google Failed']"
            
        case .geocode_forward_apple_failed:
            return "Could not retrieve an exact location for that address.\rPlease contact \(supportEmailAddress).[App Error:\(self.rawValue) - 'Apple Forward Geocode Failed']"
            
            
            
        //dont comment in - forces me to add error message for new enum error
        //    default:
        //        return "An unexpected error has occured.\rPlease contact \(supportEmailAddress) if this problem persists."
        }
    }
}


//--------------------------------------------------------------
// MARK: - appError NSError
// MARK: -
//--------------------------------------------------------------

extension NSError {
    
    //--------------------------------------------------------------
    // MARK: - appError
    // MARK: -
    //--------------------------------------------------------------

    //Usage:
    //return errorWithAppError(.LoginFailed_Username_Is_Blank)
    public class func appError(_ appError: AppError) -> NSError {
        
        return errorWithMessage(appError.description(), code: appError.rawValue)
    }
    
    
    //--------------------------------------------------------------
    // MARK: - AppErrorDomain
    // MARK: -
    //--------------------------------------------------------------

    class func AppErrorDomain() -> String{
        var bundleIdReturned = "com.cityoflondonconsulting.AppErrorDomain"
        
        if let bundleID_ = Bundle.main.bundleIdentifier {
            bundleIdReturned = bundleID_
            
        }else{
            appDelegate.log.error("Bundle.main.bundleIdentifier is nil")
        }
        return bundleIdReturned
    }
    
    
    //--------------------------------------------------------------
    // MARK: - errorWithMessage
    // MARK: -
    //--------------------------------------------------------------

    //Avoid using - create and enum - keeps the ID in order
    class func errorWithMessage(_ message: String, code: Int) -> NSError {
        let userInfo = [NSLocalizedDescriptionKey : message]
        
        let err = NSError(domain: NSError.AppErrorDomain(), code: code, userInfo: userInfo)
        
        return err
    }
    
    fileprivate class func errorWithTitle(_ title: String, andMessage message: String, code: Int) -> NSError {
        
        var userInfo = [AnyHashable : Any]()
        
        //Alerts
        userInfo[AppErrorTitleKey] = title
        userInfo[AppErrorDescriptionKey] = message
        
        userInfo[NSLocalizedDescriptionKey] = message

        let err = NSError(domain: NSError.AppErrorDomain(), code: code, userInfo: userInfo)
        
        return err
    }
    
    
    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - v1 - TODO remove
    //--------------------------------------------------------------
    
    //corrupt web address - not query
    public class func webserviceError404() -> NSError {
        return errorWithMessage("There was a problem with that query.\rPlease try again or if problem persists please contact support\r [error code: 404 - bad url parameters]", code: 999404)
    }
    
    public class func cannotParseJsonResponse() -> NSError {
        return errorWithMessage("Cannot parse result returned from server. Please contact support if this problem persists", code: 9993)
    }
    public class func noDataInReponse() -> NSError {
        return errorWithMessage("Cannot parse result returned from server. Data is empty. Please contact support if this problem persists", code: 9995)
    }
    public class func unhandledStatusCode(_ statusCode: Int) -> NSError {
        return errorWithMessage("Cannot parse result returned from server. Unhandled statusCode:\(statusCode). Please contact support if this problem persists", code: 9996)
    }
    public class func notNSHTTPURLResponse() -> NSError {
        return errorWithMessage("Cannot parse result returned from server. Not NSHTTPURLResponse. Please contact support if this problem persists", code: 9997)
    }
    
    public class func unexpectedResponseObject(_ expectedClass:String) -> NSError {
        return errorWithMessage("Cannot parse result returned from server. Not \(expectedClass). Please contact support if this problem persists", code: 9997)
    }

    
    //--------------------------------------------------------------
    // MARK: -
    // MARK: - GOOGLE PLACES ERRORS
    //--------------------------------------------------------------
    //used for unknown google errors but there shouldnt be any
    public class func googlePlacesAPIStatusError(_ status:String) -> NSError {
        return errorWithMessage("Cannot retrieve location from server. Please contact support if this problem persists. \r[status:\(status)]", code: 9998)
    }
    public class func genericError(_ description:String) -> NSError {
        return errorWithMessage(description, code: 9980)
    }
    
    
    
}
