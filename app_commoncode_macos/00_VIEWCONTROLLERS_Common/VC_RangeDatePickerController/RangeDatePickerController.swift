//
//  RangeDatePickerController.swift
//  ClarksonsArc
//
//  Created by Brian Clear on 11/05/2016.
//  Copyright © 2016 Clarksons. All rights reserved.
//

import Foundation
import UIKit

protocol RangeDatePickerControllerDelegate {
    func datesUpdated(_ rangeDatePickerController: RangeDatePickerController, dateFrom: Date, dateTo: Date)
}

class RangeDatePickerController: ParentViewController  {
    //set by segue
    var dateFrom : Date?
    var dateTo : Date?
    
    var delegate : RangeDatePickerControllerDelegate?
    
    
    @IBOutlet weak var labelFrom: UILabel!
    
    @IBOutlet weak var labelTo: UILabel!
    
    @IBOutlet weak var datePickerFrom: UIDatePicker!
    
    @IBOutlet weak var datePickerTo: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //------------------------------------------------
//        let rightBarButtonItem = UIBarButtonItem(title: "Update", style: .Plain, target: self, action: #selector(RangeDatePickerController.rightBarButtonItem_Update_Action))
//        navigationItem.rightBarButtonItem = rightBarButtonItem
        //------------------------------------------------
        if let dateFrom = dateFrom {
            self.datePickerFrom.date = dateFrom
            self.labelFrom.text = "From: \(dateStringInDisplayFormat(dateFrom))"
        }else{
            self.log.error("dateFrom is nil - cant set picker")
        }
        
        if let dateTo = dateTo {
            self.datePickerTo.date = dateTo
            self.labelTo.text = "To: \(dateStringInDisplayFormat(dateTo))"
        }else{
            self.log.error("dateTo is nil - cant set picker")
        }
    }
    
//    override func viewWillAppear(animated: Bool){
//        super.viewWillAppear(animated)
//        
//        self.navigationController?.navigationBarHidden = false
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    


    func dateStringInDisplayFormat(_ dateIN: Date) -> NSString{
        
        var dateStringReturned : NSString = ""
        
        let dateFormatterIN = DateFormatter()
        
        //"11 Feb 2016"
        dateFormatterIN.dateFormat = "dd MMM yyyy"
        
        // TODO: - Fixtures do we need to support timeZone
        //dateFormatterIN.timeZone = self.userTimeZone()
        dateStringReturned = dateFormatterIN.string(from: dateIN) as NSString
        
        return dateStringReturned
        
    }
    
    
    

    
    //--------------------------------------------------------------
    // MARK: - TODAY
    // MARK: -
    //--------------------------------------------------------------
    //set it to MIDNIGHT of the date chosen
    //issue we cant set it to NSDate() because if you set TO/FROM to NSDate() they may be in wrong order and out by seconds
    //so wont return .OrderedSame
    //TO
    //2016-05-11 11:34:03 +0000
    //FROM
    //2016-05-11 11:34:04 +0000 // same date but out by a second so not .OrderedSame
    
    @IBAction func buttonTodayFrom_Action(_ sender: AnyObject) {
        //to search for a price on a single day the TO and FROM cant both be a midnight so we set
        //FROM : today : 00:00
        //TO   : today : 23:59
        //so we can filter for any prices on that date
        if let dateFrom_AtMidnight = RangeDatePickerController.dateAtMidnight(Date()){
            self.datePickerFrom.setDate(dateFrom_AtMidnight, animated: true)
        }else{
            self.log.error("self.dateAtMidnight(NSDate()) is nil")
        }
    }
    
    @IBAction func buttonTodayTo_Action(_ sender: AnyObject) {
        //to search for a price on a single day the TO and FROM cant both be a midnight so we set
        //FROM : today : 00:00
        //TO   : today : 23:59
        //so we can filter for any prices on that date
        if let dateTo_dateAt2359 = RangeDatePickerController.dateAt2359(Date()){
            self.datePickerTo.setDate(dateTo_dateAt2359, animated: true)
        }else{
            self.log.error("self.dateAtMidnight(NSDate()) is nil")
        }
    }
    
    //-----------------------------------------------------------------------------------
    //to search for a price on a single day the TO and FROM cant both be a midnight so we set 
    //FROM : today : 00:00
    //TO   : today : 23:59
    //so we can filter for any prices on that date
    static func dateAtMidnight(_ dateIN: Date) -> Date?{
        
        var dateReturned : Date?
        //-----------------------------------------------------------------------------------
        //NDDate to String "yyyy-MM-dd'T'HH:mm"
        //-----------------------------------------------------------------------------------
        let dateFormatterIN = DateFormatter()
        
        //"11 Feb 2016"
        //dateFormatterIN.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        //dateFormatterIN.dateFormat = "yyyy-MM-dd'T'HH:mm"
        //midnight and handled BST - hack but we only need the date pickers to be set to SAME datetime of you push the today buttons 
        //so we can accurately check if TO if BEFORE FROM
        
        //dateFormatterIN.dateFormat = "yyyy-MM-dd'T'01:01:00.000'Z'"
        dateFormatterIN.dateFormat = "yyyy-MM-dd'T'00:00:00.000'Z'" //wrong we need 00:00 as dates in json use 00:00 so if you do FROM/TO at same date you need 00:00 for it to be included
        
        // TODO: - Fixtures do we need to support timeZone
        //dateFormatterIN.timeZone = self.userTimeZone()
        
        let dateStringAtSetTime = dateFormatterIN.string(from: dateIN)
        
        //-----------------------------------------------------------------------------------
        //"2016-05-11T01:01:00.000Z"
        // TO 
        //'2016-05-11 00:01:00 +0000' 2016-05-11 00:01:00 UTC
        //-----------------------------------------------------------------------------------

        let dateFormatterOUT = DateFormatter()
        dateFormatterOUT.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        dateReturned = dateFormatterOUT.date(from: dateStringAtSetTime)
        
        return dateReturned
        
    }
    //to search for a price on a single day the TO and FROM cant both be a midnight so we set
    //FROM : today : 00:00
    //TO   : today : 23:59
    //so we can filter for any prices on that date
    static func dateAt2359(_ dateIN: Date) -> Date?{
        
        var dateReturned : Date?
        //-----------------------------------------------------------------------------------
        //NDDate to String "yyyy-MM-dd'T'HH:mm"
        //-----------------------------------------------------------------------------------
        let dateFormatterIN = DateFormatter()
        
        //"11 Feb 2016"
        //dateFormatterIN.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        //dateFormatterIN.dateFormat = "yyyy-MM-dd'T'HH:mm"
        //midnight and handled BST - hack but we only need the date pickers to be set to SAME datetime of you push the today buttons
        //so we can accurately check if TO if BEFORE FROM
        
        dateFormatterIN.dateFormat = "yyyy-MM-dd'T'23:59:00.000'Z'"
        
        // TODO: - Fixtures do we need to support timeZone
        //dateFormatterIN.timeZone = self.userTimeZone()
        
        let dateStringAtSetTime = dateFormatterIN.string(from: dateIN)
        
        //-----------------------------------------------------------------------------------
        //"2016-05-11T01:01:00.000Z"
        // TO
        //'2016-05-11 00:01:00 +0000' 2016-05-11 00:01:00 UTC
        //-----------------------------------------------------------------------------------
        
        let dateFormatterOUT = DateFormatter()
        dateFormatterOUT.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        dateReturned = dateFormatterOUT.date(from: dateStringAtSetTime)
        
        return dateReturned
        
    }
    //fixtures - Reported Date - pick a FROM Date - uses 12:00 on website
    static func dateAt1200(_ dateIN: Date) -> Date?{
        
        var dateReturned : Date?
        //-----------------------------------------------------------------------------------
        //NDDate to String "yyyy-MM-dd'T'HH:mm"
        //-----------------------------------------------------------------------------------
        let dateFormatterIN = DateFormatter()
        
        //"11 Feb 2016"
        //dateFormatterIN.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        //dateFormatterIN.dateFormat = "yyyy-MM-dd'T'HH:mm"
        //midnight and handled BST - hack but we only need the date pickers to be set to SAME datetime of you push the today buttons
        //so we can accurately check if TO if BEFORE FROM
        
        dateFormatterIN.dateFormat = "yyyy-MM-dd'T'12:00:00.000'Z'"
        
        // TODO: - Fixtures do we need to support timeZone
        //dateFormatterIN.timeZone = self.userTimeZone()
        
        let dateStringAtSetTime = dateFormatterIN.string(from: dateIN)
        
        //-----------------------------------------------------------------------------------
        //"2016-05-11T01:01:00.000Z"
        // TO
        //'2016-05-11 00:01:00 +0000' 2016-05-11 00:01:00 UTC
        //-----------------------------------------------------------------------------------
        
        let dateFormatterOUT = DateFormatter()
        dateFormatterOUT.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        dateReturned = dateFormatterOUT.date(from: dateStringAtSetTime)
        
        return dateReturned
        
    }
    
    @IBAction func buttonCancel_Action(_ sender: AnyObject) {
        self.dismiss(animated: true) { 
            //call delegate
        }
    }
//    func rightBarButtonItem_Update_Action() {
//        
//        
//        
//    }
    
    
    @IBAction func buttonUpdate_Action(_ sender: AnyObject) {
        
        var isTOAfterOrSameAsFROM = false
        
        if self.datePickerFrom.date.compare(self.datePickerTo.date) == .orderedDescending {
            print("OrderedDescending")
            isTOAfterOrSameAsFROM = false
            
        }else if self.datePickerFrom.date.compare(self.datePickerTo.date) ==  .orderedAscending {
            print("OrderedAscending")
            isTOAfterOrSameAsFROM = true
            
        }else{
            //OrderedSame - sort by bunkerName
            print("OrderedSame")
            isTOAfterOrSameAsFROM = true
        }
        
        if isTOAfterOrSameAsFROM{
            if let delegate = self.delegate{
                //---------------------------------------------------------------------
                //self.dismissViewControllerAnimated(true, completion: nil)
                if let navigationController = self.navigationController {
                    navigationController.popViewController(animated: true)
                }else{
                    appDelegate.log.error("self.navigationController is nil")
                }
                //---------------------------------------------------------------------
                
                
                //------------------------------------------------
                //errorIN may be nil
                delegate.datesUpdated(self, dateFrom: self.datePickerFrom.date, dateTo: self.datePickerTo.date)
                //------------------------------------------------
                
            }else{
                self.log.error("delegate is nil")
            }
        }else{
            //------------------------------------------------------------------------------------------------
            let alertController = UIAlertController(title: "Error", message: "TO date is before FROM date", preferredStyle: UIAlertControllerStyle.alert)
            
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            //            alertController.addAction(UIAlertAction(title: "Yes, delete it.", style: UIAlertActionStyle.Default, handler: {
            //                (action) -> Void in
            //                self.log.debug("Delete PRESSED:\(section)")
            //                self.calculatorViewController.calculatorManager.deleteRow(section)
            //                self.tableViewMain.reloadData()
            //                self.calculatorViewController.doCalculate()
            //
            //            }))
            self.present(alertController, animated: true, completion: nil)
            //------------------------------------------------------------------------------------------------
        }
        
        
        self.dismiss(animated: true) {
            //call delegate
        }
    }
    
    
    
}
