//
//  OpenWithViewController.swift
//  joyride
//
//  Created by Brian Clear on 31/01/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//
import Foundation
import UIKit
import SafariServices

//protocol OpenWithViewControllerDelegate {
//    func openWithViewControllerReturned(openWithViewController: OpenWithViewController, openWithItem : OpenWithItem?)
//    func openWithViewControllerCancelled(colcPlacePickerViewController: OpenWithViewController)
//    
//}

//--------------------------------------------------------------
// MARK: - OpenWithViewControllerv
// MARK: -
//--------------------------------------------------------------

class OpenWithViewController: ParentViewController, UITableViewDelegate, UITableViewDataSource, SFSafariViewControllerDelegate{
    
    //---------------------------------------------------------------------
    //OpenWithController
    //---------------------------------------------------------------------
    let openWithController = OpenWithController()
    
    //--------------------------------------------------------------
    // MARK: - TABLE DATA
    // MARK: -
    //--------------------------------------------------------------

    //    //section keys in dictionary aren't sorted - put them
    //    var arrayTableDataSections_OpenWithAppGroup: [OpenWithAppGroup] = [.OpenWithAppGroup_RideSharing, .OpenWithAppGroup_Maps, .OpenWithAppGroup_Transit]
    //
    //    
    //    var arrayTableOpenAppConfig_RideSharing: [OpenAppConfig] = [
    //        
    //        OpenWithType.openWithType_Uber.openAppConfig(),
    //        /*
    //        OpenWithType.OpenWithType_Lyft.openAppConfig(),
    //        OpenWithType.OpenWithType_AddisonLee.openAppConfig(),
    //        OpenWithType.OpenWithType_DidiChuxing.openAppConfig(),
    //        OpenWithType.OpenWithType_Curb.openAppConfig(),
    //        //OpenWithType.OpenWithType_BlaBlaCar.openAppConfig(),
    //        OpenWithType.OpenWithType_Hailo.openAppConfig(),
    //        OpenWithType.OpenWithType_Gett.openAppConfig(),
    //        OpenWithType.OpenWithType_Grab.openAppConfig(),
    //        OpenWithType.OpenWithType_OlaCabs.openAppConfig(),
    // */
    //        ]
    //    
    //    var arrayTableOpenAppConfig_Maps: [OpenAppConfig]  = [
    //        OpenWithType.openWithType_AppleMaps.openAppConfig(),
    //        OpenWithType.openWithType_GoogleMaps.openAppConfig(),
    //        OpenWithType.openWithType_Waze.openAppConfig(),
    //        OpenWithType.openWithType_Navigon.openAppConfig(),
    //    ]
    //    var arrayTableOpenAppConfig_Transit: [OpenAppConfig]  = [
    //        OpenWithType.openWithType_CityMapper.openAppConfig(),
    //        OpenWithType.openWithType_Transit.openAppConfig(),
    //        OpenWithType.openWithType_TFL.openAppConfig(),
    //        ]
    
    //----------------------------------------------------------------------------------------
    var arrayTableDataSections_OpenWithAppGroup: [OpenWithAppGroup] = [.OpenWithAppGroup_Maps]
    //----------------------------------------------------------------------------------------

    var arrayTableOpenAppConfig_Maps: [OpenAppConfig]  = [
        OpenWithType.openWithType_AppleMaps.openAppConfig(),
        OpenWithType.openWithType_GoogleMaps.openAppConfig(),
        OpenWithType.openWithType_Waze.openAppConfig()
        /*,
        OpenWithType.openWithType_Navigon.openAppConfig(),(*/
    ]
    //----------------------------------------------------------------------------------------

    
    
    var dictTableDataItems1: [OpenWithAppGroup: [OpenAppConfig]] =  [:]
    //--------------------------------------------------------------
    @IBOutlet weak var tableView: UITableView!
    
    var selectedOpenAppConfig : OpenAppConfig?
    
    // TODO: - passTrip around
    var clkPlaceStart: CLKPlace?
    var clkPlaceEnd: CLKPlace?
    var openWithTravelMode: OpenWithTravelMode = .driving
    
    //-----------------------------------------------------------------------------------
    @IBOutlet weak var labelAddressStart: UILabel!
    @IBOutlet weak var labelAddressEnd: UILabel!
    
    //--------------------------------------------------------------
    // MARK: - IMPLEMENTATION
    // MARK: -
    //--------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //----------------------------------------------------------------------------------------
        //        self.dictTableDataItems1 =
        //            [.OpenWithAppGroup_RideSharing: arrayTableOpenAppConfig_RideSharing,
        //             .OpenWithAppGroup_Maps: arrayTableOpenAppConfig_Maps,
        //             .OpenWithAppGroup_Transit: arrayTableOpenAppConfig_Transit]
        //----------------------------------------------------------------------------------------
        self.dictTableDataItems1 =
            [.OpenWithAppGroup_Maps: arrayTableOpenAppConfig_Maps]
        //----------------------------------------------------------------------------------------
        
        self.view.backgroundColor = AppearanceManager.appColorNavbarAndTabBar
        
        fillTextFieldAddress()
        
        //----------------------------------------------------------------------------------------
        //DYNAMIC TEXT and TABLE ROW HEIGHT
        //-----------------------------------------------------------------------------------
        self.tableView.estimatedRowHeight = 80.0
        //also done in heightForRowAtIndexPath
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        //----------------------------------------------------------------------------------------
        //self.tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        //self.tableView.estimatedSectionHeaderHeight = 80.0
        //----------------------------------------------------------------------------------------
    }
    
    func fillTextFieldAddress(){

        //------------------------------------------------
        //Start
        //------------------------------------------------
        if let clkPlaceStart = self.clkPlaceStart {
            self.labelAddressStart?.text = "\(clkPlaceStart.name_formatted_address)"
            
        }else{
            //user pressed CLEAR
            self.labelAddressStart?.text = ""
        }
        //------------------------------------------------
        //End
        //------------------------------------------------
        if let clkPlaceEnd = self.clkPlaceEnd {
            self.labelAddressEnd?.text = "\(clkPlaceEnd.name_formatted_address)"
            
        }else{
            //user pressed CLEAR
            self.labelAddressEnd?.text = ""
        }
    }
    
    
    
    @IBAction func buttonCancel_Action(_ sender: AnyObject) {
        self.dismiss(animated: true) { () -> Void in
            //            if let delegate = self.delegate {
            //                delegate.openWithViewControllerCancelled(self)
            //                
            //            }else{
            //                self.log.error("self.delegate is nil")
            //            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //--------------------------------------------------------------
    // MARK: - UITableViewDataSource/Delegate
    // MARK: -
    //--------------------------------------------------------------
    
    //--------------------------------------------------------------
    // MARK: - TABLE VIEW HEADER
    // MARK: -
    //--------------------------------------------------------------
    
    internal let sectionHeaderHeight: CGFloat = 37.0
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        //return UITableViewAutomaticDimension
        return sectionHeaderHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        var cell = UITableViewCell()
        
        //-----------------------------------------------------------------------------------
        let openWithViewControllerTableViewCellHeader : OpenWithViewControllerTableViewCellHeader = tableView.dequeueReusableCell(withIdentifier: "OpenWithViewControllerTableViewCellHeader") as! OpenWithViewControllerTableViewCellHeader!
        
        openWithViewControllerTableViewCellHeader.applyCustomFont()
        
        //-----------------------------------------------------------------------------------
        let openWithAppGroup = arrayTableDataSections_OpenWithAppGroup[section]
        openWithViewControllerTableViewCellHeader.labelName.text = openWithAppGroup.rawValue
        //-----------------------------------------------------------------------------------
        
        cell = openWithViewControllerTableViewCellHeader
        //-----------------------------------------------------------------------------------
        return cell
    }


    func numberOfSections(in tableView: UITableView) -> Int{
        return arrayTableDataSections_OpenWithAppGroup.count
    }
    
    //--------------------------------------------------------------
    // MARK: - HELPER METHOD TO get array from Table Data dictionary
    // MARK: -
    //--------------------------------------------------------------

    func arrayOpenAppConfigsForSection(_ section: Int) -> [OpenAppConfig]{
        var arrayOpenAppConfigs_: [OpenAppConfig] = []
        let openWithAppGroup = arrayTableDataSections_OpenWithAppGroup[section]
        
        if let arrayTableOpenAppConfig: [OpenAppConfig] = dictTableDataItems1[openWithAppGroup]{
            arrayOpenAppConfigs_ = arrayTableOpenAppConfig
            
        }else{
            self.log.error("dictTableDataItems[openWithAppGroup] failed")
            
        }
        return arrayOpenAppConfigs_
    }
    //--------------------------------------------------------------
    // MARK: - Table
    // MARK: -
    //--------------------------------------------------------------

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let arrayOpenAppConfigs = arrayOpenAppConfigsForSection(section)
        return arrayOpenAppConfigs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        var cell = UITableViewCell()
        
        if let openWithViewControllerTableViewCell = tableView.dequeueReusableCell(withIdentifier: "OpenWithViewControllerTableViewCell") as? OpenWithViewControllerTableViewCell {
            
            //openWithViewControllerTableViewCell.textLabel?.applyCustomFontForCurrentTextStyle()
            openWithViewControllerTableViewCell.applyCustomFont()
            //------------------------------------------------
            let arrayOpenAppConfigs = arrayOpenAppConfigsForSection((indexPath as NSIndexPath).section)
            let openAppConfig : OpenAppConfig = arrayOpenAppConfigs[(indexPath as NSIndexPath).row]
            
            openWithViewControllerTableViewCell.labelName?.text = openAppConfig.openWithType.name()
            //openWithViewControllerTableViewCell.labelSource?.text = openAppConfig.openWithSource.rawValue
            openWithViewControllerTableViewCell.labelSource?.text = openAppConfig.openWithType.openInDescription()
            
            
            //------------------------------------------------
            // TODO: -
            //            if openWithItem.isSelected{
            //                openWithViewControllerTableViewCell.accessoryType = .Checkmark
            //            }else{
            //                openWithViewControllerTableViewCell.accessoryType = .None
            //
            //            }
            
            cell = openWithViewControllerTableViewCell
        }else{
            self.log.error("dequeueReusableCellWithIdentifier'OpenWithViewControllerTableViewCell' FAILED")
        }
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
//        unselectAll(indexPath.section)
        let arrayOpenAppConfigs = arrayOpenAppConfigsForSection((indexPath as NSIndexPath).section)
        self.selectedOpenAppConfig = arrayOpenAppConfigs[(indexPath as NSIndexPath).row]
        
        //-----------------------------------------------------------------------------------
        //check/uncheck - may need for user to turn on / off ones they dont use
//        arrayTableOpenItems[indexPath.row].isSelected = true
//        self.tableView.reloadData()
        //-----------------------------------------------------------------------------------

        
        self.openWithController.open(self.selectedOpenAppConfig,
                                     clkPlaceStart: self.clkPlaceStart,
                                     clkPlaceEnd: self.clkPlaceEnd,
                                     openWithTravelMode: self.openWithTravelMode,
                                     presentingViewController: self)
        
        
        
        
    }
    
//    func unselectAll(section: Int){
//        var arrayTableOpenItems = arrayTableOpenItemsForSection(section)
//        for count in 0..<arrayTableOpenItems.count{
//            arrayTableOpenItems[count].isSelected = false
//        }
//    }
    
//    //--------------------------------------------------------------
//    // MARK: -
//    //-------------------------------------------------------------------
//    openWithTravelMode: OpenWithTravelMode,
//    func open(openAppConfig : OpenAppConfig?,
//              clkPlaceStart: CLKPlace?,
//              clkPlaceEnd: CLKPlace?){
//
//        if let openAppConfig = openAppConfig{
//            self.log.debug("openWithViewControllerReturned openAppConfig.openWithType.name():\(openAppConfig.openWithType.name())")
//
//            //------------------------------------------------
//            //SAVE TO DB
//            //------------------------------------------------
//            self.saveTripToDB(openAppConfig)
//            
//            
//            // TODO: - clkPlaceEnd might not be set
//            //------------------------------------------------
//            if let clkPlaceStart = clkPlaceStart {
//                if let clkPlaceEnd = clkPlaceEnd {
//                    //------------------------------------------------------------------------------------------------
//                    openAppConfig.open(self,
//                                                clkPlaceStart: clkPlaceStart,
//                                                clkPlaceEnd: clkPlaceEnd,
//                                                success:{() -> Void in
//                                                    // TODO: - <#COMMENT#>
//                                                    
//                        },
//                                                failure:{(errMsg) -> Void in
//
//                                                    CLKAlertController.showAlertInVC(self, title: "Error opening application", message: errMsg)
//                                                    
//                    })
//                    //------------------------------------------------------------------------------------------------
//                    
//                }else{
//                    self.log.error("self.clkPlaceEnd is nil")
//                }
//            }else{
//                self.log.error("self.clkPlaceStart is nil")
//            }
//            //------------------------------------------------
//            
//        }else{
//            self.log.debug("openWithViewControllerReturned openWithItem is nil")
//        }
//    }
//        
//        
//    // TODO: - move into CLKTrip
//    func saveTripToDB(openAppConfig: OpenAppConfig){
//        //------------------------------------------------------------------------------------------------
//        //START
//        //------------------------------------------------------------------------------------------------
//        if let clkPlaceStart = self.clkPlaceStart{
//            if let clkPlaceEnd = self.clkPlaceEnd{
//                
//                if let clkTrip = CLKTrip(name: "\(openAppConfig.openWithType.name()) Trip", openWithType: openAppConfig.openWithType, dateTime: NSDate(), clkPlaceStart: clkPlaceStart, clkPlaceEnd: clkPlaceEnd){
//                    
//                    appDelegate.colcDBManager.saveCLKTrip(clkTrip, completion: { (dbResultType) -> Void in
//                        switch dbResultType {
//                        case .success:
//                            //-------------------
//                            //SUCCESS
//                            //-------------------
//                            
//                            self.log.debug("trip saved ok")
//                            
//                            
//                        case .error(let errorType):
//                            //--------------------
//                            //ERROR
//                            //--------------------
//                            self.log.error("\(errorType)")
//                            
//                        }
//                    })
//                }else{
//                    self.log.error("FAILED TO CREATE CLKTrip from clkPlaceStart/clkPlaceEnd")
//                }
//            }else{
//                self.log.error("self.clkPlaceEnd is nil")
//            }
//        }else{
//            self.log.error("clkPlaceStart is nil")
//        }
//        
//    }

}
