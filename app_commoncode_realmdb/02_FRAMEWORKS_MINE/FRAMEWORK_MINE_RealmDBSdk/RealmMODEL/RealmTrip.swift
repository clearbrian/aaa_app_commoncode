//
//  Trip.swift
//  TestRealDB
//
//  Created by Brian Clear on 18/02/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import RealmSwift

class RealmTrip : Object {
    dynamic var name = ""
    dynamic var openWithTypeId: Int = -1    //OpenWithType.rawvalue
    dynamic var dateTime: Date = Date()
    dynamic var realmPlaceStart : RealmPlace?
    dynamic var realmPlaceDestination : RealmPlace?
    
}
