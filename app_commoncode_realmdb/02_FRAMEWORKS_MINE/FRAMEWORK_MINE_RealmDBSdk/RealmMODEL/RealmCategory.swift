//
//  Category.swift
//  TestRealDB
//
//  Created by Brian Clear on 18/02/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import RealmSwift
class RealmCategory : Object {
    dynamic var name = ""
}