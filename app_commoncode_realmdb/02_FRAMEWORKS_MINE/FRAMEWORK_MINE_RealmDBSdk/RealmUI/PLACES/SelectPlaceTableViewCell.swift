//
//  SelectPlaceTableViewCell.swift
//  joyride
//
//  Created by Brian Clear on 09/06/2016.
//  Copyright © 2016 City Of London Consulting Limited. All rights reserved.
//

//
//  SelectPlaceTableViewCell.swift
//  joyride
//
//  Created by Brian Clear on 18/08/2015.
//  Copyright (c) 2015 City Of London Consulting Limited. All rights reserved.
//

import Foundation
import UIKit

class SelectPlaceTableViewCell : UITableViewCell{
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelAddress: UILabel!
    @IBOutlet weak var labelType: UILabel!
    @IBOutlet weak var labelLocality: UILabel!

    
    var customFontApplied = false
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //print("SelectPlaceTableViewCell init coder")
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        super.init(style: UITableViewCellStyle.subtitle, reuseIdentifier: reuseIdentifier)
        
    }
    
    func setup(){
        
    }
    
    override func draw(_ rect: CGRect) {
        
        //print("drawRect")
        //didnt work here only for cells off screen
        //applyCustomFont()
    }
    
    func applyCustomFont(){
        if customFontApplied{
            //cell created already - BODY is replace with customfont - if we reuse it and call this again it will try and convert font again
        }else{
            
            self.labelName?.applyCustomFontForCurrentTextStyle()
            self.labelAddress?.applyCustomFontForCurrentTextStyle()
            self.labelType?.applyCustomFontForCurrentTextStyle()
            self.labelLocality?.applyCustomFontForCurrentTextStyle()

            customFontApplied = true
        }
    }
}
